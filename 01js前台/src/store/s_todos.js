import { uid } from 'quasar'

const state={
  todos: [
    {
      id: 1,
      name: "吃饭",
      到期日期: "2022.6.4",
      到期时间: "15:14",
      负责人: "自己",
      done: false
    },
    {
      id: 2,
      name: "学习",
      到期日期: "2022.6.4",
      到期时间: "15:14",
      负责人: "自己",
      done: true
    },
    {
      id: 3,
      name: "睡觉",
      到期日期: "2022.6.4",
      到期时间: "15:14",
      负责人: "自己",
      done: false
    },
  ]
}
const actions={
  增加一项(context,拟增todo){
    // console.log('拟增todo')
    let id=uid()
    拟增todo.id=id
    context.commit("m增加一项",拟增todo)
  },
}
const mutations={
  切换完成状态(state,id){
		// console.log('切换完成状态被调用了')
		// console.log(state)
    state.todos.forEach((todo)=>{
      if(todo.id === id) todo.done = !todo.done
    })
  },

  deleteTodo(state,id){
    state.todos = state.todos.filter( todo => todo.id !== id )
  },
  m增加一项(state,拟增todo){
    state.todos.unshift(拟增todo)
  },

}

const getters={
  todos:(state)=>{
    return state.todos
  }
}

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
