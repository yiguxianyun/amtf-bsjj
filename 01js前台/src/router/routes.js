const routes = [
    {
        path: '/',
        component: () => import('layouts/MainLayout.vue'),
        children: [
            // {
            //   path: '/sw',
            //   component: () => import('src/pages/sw.vue')
            // },
            {
                path: '',
                component: () => import('pages/尺寸与位置.vue')
            },
            {
                path: '/动态组件',
                component: () => import('pages/动态组件.vue')
            },
            {
                path: '/生产',
                component: () => import('pages/生产.vue')
            }
            // { path: '/动态组件', component: () => import('pages/动态组件.vue') }
        ]
    },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: '/:catchAll(.*)*',
        component: () => import('pages/ErrorNotFound.vue')
    }
];

export default routes;
