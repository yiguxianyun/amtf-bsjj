var start = function () {
  inc = document.getElementById("incomming");
  window.指定图层 = document.getElementById("指定图层");
  window.开启显示边界框尺寸el = document.getElementById("开启显示边界框尺寸");
  window.x尺寸el = document.getElementById("x尺寸");
  window.y尺寸el = document.getElementById("y尺寸");
  window.z尺寸el = document.getElementById("z尺寸");

  旧值 = localStorage.getItem("指定图层");
  // alert(旧值);
  if (旧值) {
    指定图层.value = 旧值;
  }
};

window.onload = start;

window.onkeydown = function (e) {
  // alert(x尺寸el);
  if (e.keyCode === 13) {
    console.log(e);
    if (e.target == x尺寸el) {
      // alert(e);
    }
    e.target.blur();
  }
};
window.onbeforeunload = function (ev) {
  localStorage.setItem("指定图层", 指定图层.value);
};

// <!-- 调用ruby👇 -->
function 缩放移动对象(e) {
  e.value = exp_result(e.value);

  边界框尺寸 = [x尺寸el.value, y尺寸el.value, z尺寸el.value];
  边界框尺寸.push($("input[name='x基准']:checked").val());
  边界框尺寸.push($("input[name='y基准']:checked").val());
  边界框尺寸.push($("input[name='z基准']:checked").val());
  边界框尺寸 = 边界框尺寸.map(function (item) {
    item = parseFloat(item);
    if (isNaN(item)) {
      item = 0;
    }
    return item;
  });

  sketchup.缩放移动对象(JSON.stringify(边界框尺寸));
}
function 开启显示边界框尺寸(e) {
  // 调式模式 = document.getElementById('调式模式');
  arg = e.checked;
  // alert(arg)
  // window.location = 'skp:开启显示边界框尺寸@' + arg
  sketchup.开启显示边界框尺寸(arg);
}
function 重新连接amtf() {
  sketchup.重新连接amtf();
}
function 尺寸标注放到指定图层() {
  // arg = "你好，世界"
  // alert(arg)
  // window.location = 'skp:测试@' + arg
  sketchup.尺寸标注放到指定图层(指定图层.value);
  // 发送调试模式();
}
function hotkey() {
  var a = window.event.keyCode;
  if (a == 65 && event.ctrlKey) {
    alert("你按了ctrl+a键吧");
  }
} // end hotkey

document.onkeydown = hotkey;
function 测试() {
  // arg = "你好，世界"
  // alert(arg)
  // window.location = 'skp:测试@' + arg
  sketchup.测试();
  // 发送调试模式();
}
function 关于amtf() {
  arg = "你好，关于amtf";
  // alert(arg)
  // window.location = 'skp:关于amtf@' + arg
  sketchup.关于amtf(arg);
}

function 开启amtf_dxf() {
  arg = "你好，世界";
  inc = document.getElementById("incomming");
  if (inc.innerHTML == "") {
    // alert(arg)
    // window.location = 'skp:开启amtf_dxf@' + arg
    sketchup.开启amtf_dxf(arg);
    start();
  } else {
  }
}

function 孤立选中对象() {
  // window.location = 'skp:孤立@' + arg
  sketchup.孤立选中对象();
}
function 还原原显示状态() {
  // window.location = 'skp:孤立@' + arg
  sketchup.还原原显示状态();
}
function 选择同材质组件() {
  // arg = document.getElementById("选择同材质组件").value
  // window.location = 'skp:选择同材质组件@' + arg
  sketchup.选择同材质组件();
  // alert(window.location)
}
function 移除组件and子级材质() {
  // window.location = 'skp:移除组件and子级材质@' + arg
  sketchup.移除组件and子级材质();
  // alert(window.location)
}
function 更改背板厚度() {
  arg = document.getElementById("背板厚度").value;
  // window.location = 'skp:更改背板厚度@' + arg
  sketchup.更改背板厚度(arg);
  // alert(window.location)
}
function 刷新amtf() {
  // alert(window.location)
  arg = "刷新amtf";
  // window.location = 'skp:刷新amtf@' + arg
  sketchup.刷新amtf(arg);
  // alert(window.location)
}

function 清除未使用() {
  arg = "清除未使用";
  // window.location = 'skp:清除未使用@' + arg
  sketchup.清除未使用(arg);
  // try {
  //   var Message = "请选择文件夹"; //选择框提示信息
  //   var Shell = new ActiveXObject("Shell.Application");
  //   var Folder = Shell.BrowseForFolder(0, Message, 0x0040, 0x11); //起始目录为：我的电脑
  //   //var Folder = Shell.BrowseForFolder(0,Message,0); //起始目录为：桌面
  //   if (Folder != null) {
  //     Folder = Folder.items(); // 返回 FolderItems 对象
  //     Folder = Folder.item(); // 返回 Folderitem 对象
  //     Folder = Folder.Path; // 返回路径
  //     if (Folder.charAt(Folder.length - 1) != "\\") {
  //       Folder = Folder + "\\";
  //     }
  //     document.all.savePath.value = Folder;
  //     return Folder;
  //   }
  // } catch (e) {
  //   alert(e.message);
  // }
}

function 匹配尺寸() {
  arg = "匹配尺寸";
  // window.location = 'skp:匹配尺寸@' + arg
  sketchup.匹配尺寸(arg);
}

function 对齐() {
  // window.location = 'skp:对齐@' + arg
  sketchup.对齐(arg);
}

function 多余组件处理() {
  // window.location = 'skp:多余组件处理@' + arg
  sketchup.多余组件处理(arg);
}

function 另存为排版模型() {
  // window.location = 'skp:另存为排版模型@' + arg
  sketchup.另存为排版模型(arg);

  // alert(window.location)
}

function 删除指定图层() {
  // window.location = 'skp:删除指定图层@' + arg
  sketchup.删除指定图层(arg);
}

function 删除隐藏项目() {
  window.location = "skp:删除隐藏项目@" + arg;
}

function 删除or炸开无板字组件() {
  window.location = "skp:删除or炸开无板字组件@" + arg;
}

function 炸开所有子组件() {
  // window.location = 'skp:炸开所有子组件@' + arg
  sketchup.炸开所有子组件(arg);

  // alert(window.location)
}

function 组件改名() {
  // window.location = 'skp:组件改名@' + arg
  sketchup.组件改名(arg);
}

function 组件转群组() {
  // window.location = 'skp:组件转群组@' + arg
  sketchup.组件转群组(arg);
}

// function 一键排版预处理() {
//   // window.location = 'skp:一键排版预处理@' + arg
//   sketchup.一键排版预处理(arg);
// }
function 干涉检查() {
  // window.location = 'skp:干涉检查@' + arg
  sketchup.干涉检查();
}

function 延伸背板() {
  arg = document.getElementById("延伸值").value;
  // alert(arg)
  // window.location = 'skp:延伸背板@' + arg
  sketchup.延伸背板(arg);
}

function 文件改名() {
  // window.location = 'skp:文件改名@' + arg
  sketchup.文件改名(arg);
}

function 画柜子(arg) {
  // alert(arg)
  // window.location = 'skp:画柜子@' + arg
  sketchup.画柜子(arg);
}

// function 重载ABF() {
//   arg = "重载ABF？";
//   // window.location = 'skp:重载ABF@' + arg
//   sketchup.重载ABF(arg);
// }

// <!-- 供ruby调用V2👇 -->
function 显示边界框尺寸(params) {
  // alert(params);
  const result = JSON.parse(params);
  x尺寸el.value = result["x"];
  y尺寸el.value = result["y"];
  z尺寸el.value = result["z"];
}
