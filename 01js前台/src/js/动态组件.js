// <!-- 调用ruby👇 -->
$(document).ready(function () {
  $("#生成基准盒").on("click", function () {
    // alert("段落被点击了。");
    // console.log(this.id);
    sketchup.生成单组件({ 组件名: this.value });
  });
  // $("#生成左侧板,#生成右侧板").on("click", function () {
  //   sketchup.生成单组件({ 组件名: this.value, xl: 18 });
  // });
  $("#生成左侧板").on("click", function () {
    sketchup.生成单组件({ 组件名: this.value, xl: 18 });
  });
  $("#生成右侧板").on("click", function () {
    sketchup.生成单组件({ 组件名: this.value, xl: 18, x: 1000 });
  });
  $("#生成顶板").on("click", function () {
    sketchup.生成单组件({ 组件名: this.value, zl: 18, z: 1000 });
  });
  $("#生成底板").on("click", function () {
    sketchup.生成单组件({ 组件名: this.value, zl: 18 });
  });
  $("#生成面板").on("click", function () {
    sketchup.生成单组件({ 组件名: this.value, yl: 18 });
  });
  $("#生成背板").on("click", function () {
    sketchup.生成单组件({ 组件名: this.value, yl: 18, y: 1000 });
  });
  // $("#生成右侧板").on("click", function () {
  //   sketchup.生成单组件([this.value, 18]);
  // });

  $("#生成装配件").on("click", function () {
    // sketchup.生成装配件(e.value);
    sketchup.生成装配件({ 组件名: this.value });
  });
});
