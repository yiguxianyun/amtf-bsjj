import { defineStore } from 'pinia';

export const useAmtfStore = defineStore('amtf-store', {
  state: () => ({
    counter: 0,
    baseUrl: 'https://www.baidu.com/',
    su连接状态: "0",
    主组件属性: {},
    父组件属性: {},
    sw边界尺寸: {},
    相关组件属性: {},
    调用amtf_dxf2结果: [],
    检查板厚_处理结果: "",
    返回结果: "",
    生产: {
      成品柜名: "amtf",
      单元柜名: "amtf",
      板名: "不批量修改",
      订单: "amtf",
      客户: "amtf",
      启用: true,
      每块板数量: 1,
      小于多少毫米不封边: 12,
      封边厚度: [1, 1, 1, 1]
    },
  }),
  getters: {
    doubleCount: (state) => state.counter * 2,
    su连接状态显示文字: (state) => state.su连接状态 == 0 ? 'su未连接' : 'su已连接',
    调用amtf_dxf2结果显示: (state) => {
      let returnVal = state.调用amtf_dxf2结果.join("\n\n");
      return returnVal;
    }
  },
  actions: {
    increment() {
      this.counter++;
    },
    changeState(params) {
      // console.log('接收到的参数===>', params)
      this.baseUrl = params
    }
    ,
    排序() {
      // var arr5 = [{id:10},{id:5},{id:6},{id:9},{id:2},{id:3}];
      this.主组件属性.其他属性.sort(function (a, b) {
        return a.id - b.id
      })

      if (this.父组件属性.组件定义名 != "顶层组件") {
        this.父组件属性.其他属性.sort(function (a, b) {
          return a.id - b.id
        })
      }
      // console.log(this.组件属性.其他属性);
    },
  },
});
