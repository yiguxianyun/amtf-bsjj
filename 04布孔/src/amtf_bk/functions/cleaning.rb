# encoding: UTF-8
module AMTF
	module BUKONG
		module CleaningModule
			def self.cleaning(options)
				entities = Sketchup.active_model.selection
				query = AmtfQuery.new
				parts = query.query_sheets(entities)
				return unless parts && parts.size > 0
				pb = ExtraFuntion::ProgressBar.new(parts.size, OB[:cleaning])
				i = 0
				timer =
					UI.start_timer(0.000000000005, true) do
						if i == parts.size
							UI.stop_timer(timer)
							Sketchup.set_status_text(OB[:completed])
						end
						if i < parts.size
							begin
								part = parts[i]
								sheet_id = part.sheet_id
								frame = Utilities.get_frame_of_part(part)
								if frame
									if options['all'] || options['connector']
										frame.clean_connector(sheet_id)
									end
									if options['all'] || options['groove']
										frame.remove_groove(sheet_id)
									end
									if options['all'] || options['poperties']
										frame.clean_attributes(sheet_id)
									end
									if options['all'] || options['edge_banding']
										edge_face = part.edge_face
										face_index_arr = []
										edge_face.each do |f|
											face_index =
												f.get_attribute 'amtf_dict',
												                'face_index'
											if face_index
												face_index_arr << face_index
											end
										end
										frame.remove_multi_edging(
											sheet_id,
											face_index_arr
										)
									end
								end
								Utilities
									.definition(part.comp)
									.entities
									.each do |item|
										next if item.is_a?(Sketchup::Edge)
										if item.is_a?(Sketchup::Face)
											if options['all'] ||
													options['edge_banding']
												item.delete_attribute 'amtf_dict',
												                      'edge_banding'
												item.delete_attribute 'amtf_dict',
												                      'edge_type_id'
												item.delete_attribute 'amtf_dict',
												                      'edge_type_name'
												item.delete_attribute 'amtf_dict',
												                      'edge_type_thick'
												item.material =
													part.comp.material
											end
											next
										end
										type =
											item.get_attribute(
												'amtf_dict',
												'type'
											)
										if options['all']
											item.erase!
											next
										end
										if !options['connector']
											if item.get_attribute(
													'amtf_dict',
													'cabineo_cap'
											   )
												next
											end
											if %w[
													oc_cam
													chot_cam
													rafix_d
													rafix_d2
													rafix_d1
													dogbone_male
													dogbone_female
													chot_dot
													chot_go3
													chot_go2
													chot_cam
													cabineo_d
													cabineo_d1
													cabineo_d2
													cabineo_d3
													cabineo_cap
													khoan_moi
													patV
													drawer_fitting_1
													drawer_fitting_2
											   ].include?(type)
												next
											end
										end
										if !options['groove']
											if type == 'tool_path' ||
													type == 'curve_groove'
												next
											end
											next if type == 'chanel'
										end
										item.erase!
									end
								if options['poperties']
									part.comp.set_attribute 'amtf_dict',
									                        'custom_grain',
									                        'no'
									part.comp.set_attribute 'amtf_dict',
									                        'comp_type',
									                        'normal_sheet'
									part.comp.set_attribute 'amtf_dict',
									                        'amtf_bukong_id',
									                        ''
								end
								i += 1
								pb.update(i)
							rescue => error
								i += 1
								Utilities.print_exception(error, false)
							end
						end
					end
			end
		end
	end
end