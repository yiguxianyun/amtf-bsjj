# encoding: UTF-8
module AMTF
	module BUKONG
		module Worker_function
			def self.auto_hole
				url = "#{ServerOptions.get_server_url}/auto_hole"
				if !AMTF_Dialog.dialog.nil? && AMTF_Dialog.dialog.visible?
					AMTF_Dialog.dialog.bring_to_front
					AMTF_Dialog.dialog.set_url(url)
				else
					AMTF_Dialog.create_dialog
					AMTF_Dialog.dialog.set_url(url)
					AMTF_Dialog.dialog.show
				end
				AMTF_Dialog.dialog.on('auto_hole_execute') do |deferred, config_data|
						begin
							execute_auto_hole(config_data)
							deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
			end
			def self.execute_auto_hole(config_data)
				Sketchup.active_model.start_operation(OB[:automation], true)
				amtf_query = AmtfQuery.new
				parts = amtf_query.query_sheets(Sketchup.active_model.selection)
				bounds = Geom::BoundingBox.new
				parts.each { |item| bounds.add(item.bounds) }
				octree = AmtfOctree.new(1, bounds)
				config =Utilities.get_setting(
						'edge_banding',
						config_data['config'],
						config_data['edge_auto_id']
					)
				pb =ExtraFuntion::ProgressBar.new(
						parts.size,
						OB[:build_part_list]
					)
				i = 0
				timer =
					UI.start_timer(0.0000000000005, true) do
						if i == parts.size
							UI.stop_timer(timer)
							do_connector(parts, config_data)
						end
						if i < parts.size
							begin
								octree.insert(parts[i])
								if parts[i].comp.get_attribute(
										'amtf_dict',
										'no_auto_edge_banding'
								   ) != true &&
										config_data['auto_edge_banding'] == true
									no_edge_thick =
										config_data['nesting_config'][
											'no_edge_thick'
										]
									parts[i].edge_banding(
										config,
										config_data['edge_auto_id'],
										no_edge_thick
									)
								end
								i += 1
								pb.update(i)
							rescue => error
								i += 1
								Utilities.print_exception(error, false)
							end
						end
					end
				Sketchup.active_model.commit_operation
			end

			def self.do_connector(parts, config_data)
				pb = ExtraFuntion::ProgressBar.new(parts.size, OB[:automation])
				i = 0
				j = 0
				result = {}
				amtf_query = AmtfQuery.new
				frame_list = AMTF_STORE.frame_premitives
				pairs_list = {}
				connector_type = config_data['conn_auto_type']
				frame_list.each do |k, frame|
					next unless frame && frame.frame && frame.frame.valid?
					unless Sketchup.active_model.selection.include?(frame.frame)
						next
					end
					attrs = frame.attrs
					next unless attrs && attrs['root']
					if attrs['root'][connector_type]
						if !attrs['root'][connector_type].empty?
							pairs_list[frame] = attrs['root'][connector_type]
						end
					end
					frame.create_connector
				end
				timer =UI.start_timer(0.0000000000005, true) do
						UI.stop_timer(timer) if i == parts.size
						if i < parts.size
							begin
								part = parts[i]
								result = part.relatedPart
								if result.size > 0
									part_j = result.shift
									if part_j[1].relatedPart.key?(part)
										part_j[1].relatedPart.delete(part)
									end
									part_frame =
										Utilities.get_frame_of_part(part)
									part1_frame =
										Utilities.get_frame_of_part(part_j[1])
									check_pair =
										FittingUtils.check_pair(
											part,
											part_j[1],
											pairs_list,
											part_frame,
											part1_frame
										)
									if check_pair != true
										amtf_frame = nil
										if part_frame && part1_frame &&
												part1_frame == part_frame
											amtf_frame = part_frame
										end
										make_hole_data = {
											pick_data: {
												pick_component: part.comp,
												parent_matrix: part.parent_tr,
												trans: part.trans
											},
											last_pick: {
												pick_component: part_j[1].comp,
												parent_matrix:
													part_j[1].parent_tr,
												trans: part_j[1].trans
											},
											frame: amtf_frame
										}
										maker = AmtfMakeHole.new(make_hole_data)
										maker.set_config(config_data)
										maker.make_hole
									end
								end
								i += 1 if result.size == 0
								pb.update(i)
							rescue => error
								Utilities.print_exception(error, false)
								i += 1
							end
						end
					end
			end
		end
	end
end