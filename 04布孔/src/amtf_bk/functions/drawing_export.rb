# encoding: UTF-8
module AMTF
    module BUKONG
        module Drawing_Export   
           def self.execute_command
                # Lấy d�?liệu
                @part_list = []     
                entities = Sketchup.active_model.entities
                entities.each do |entity|               
                    next unless entity.is_a?(Sketchup::Group)                
                    explode = Utilities.get_comp_attribute(entity, 'amtflogic_exploded', 'amtf_dict')
                    if explode != 'true'  || Utilities.hasParent?(entity)
                    UI.messagebox OB[:explode_required], MB_OK
                    return
                    else             
                        next unless Utilities.solid?(entity)     
                        sheetpart = SheetPart.new entity, entity.transformation, IDENTITY
                        @part_list << sheetpart
                    end
                end     
                return unless @part_list.size > 0
                #Lọc và lấy tên, nếu tên khác nhau dấu cách, hoặc ch�?hoa, ch�?thường thì đưa v�?cùng một tên
                name_arr = []
                no_name_found = false
                hidden_layer =  Sketchup.active_model.layers.add("amtflogic_hidden_layer")
                for i in 0..@part_list.size - 1
                    part = @part_list[i]
                    grp_name = Utilities.get_comp_attribute(part.comp, 'prefix', 'amtf_dict')
                    unless grp_name.nil?
                        item = {
                            :grp_name => grp_name, 
                            :id => Utilities.fj_uniq_name(grp_name), 
                            :comp => part.comp
                        } 
                        name_arr << item  unless grp_name.empty?
                    end 
                    if grp_name.nil?   
                        part.comp.layer = hidden_layer  
                        no_name_found = true       
                    elsif  grp_name.empty?
                        no_name_found = true
                    end    
                end
                UI.messagebox OB[:no_name_found], MB_OK if no_name_found 
                id_arr = []
                @scene_data = []
                for i in 0..name_arr.size - 1
                    unless id_arr.include?name_arr[i][:id]
                        id_arr << name_arr[i][:id] 
                        comp_array = get_comp_in_group_name(@part_list, name_arr[i][:id]) 
                        layer = Sketchup.active_model.layers.add(name_arr[i][:id])                      
                        comp_array.each {
                            |grp|                            
                            grp.layer = layer
                        }
                        @scene_data << {
                            :grp_name_id => name_arr[i][:id], 
                            :grp_name_title => name_arr[i][:grp_name], 
                            :pages => get_pages_with_grp_name_id(name_arr[i][:id]),
                            :comp_array => comp_array,
                            :layer => layer
                        }
                    end
                end
                @scene_data << {
                    :grp_name_id => 'amtflogic_custom_scene', 
                    :grp_name_title => "Custom Scene", 
                    :pages => get_custom_pages(),
                    :comp_array => [],
                    :layer => nil
                }
                begin
                    @filename_project_view = File.join(PLUGIN_DIR, "/styles/hidden_line.style")
                    @filename_axo_view = File.join(PLUGIN_DIR, "/styles/shaded_texture.style")
                    view_type1 = Sketchup.read_default('AmtflogicBuKong', 'view_type1' , 'wireframe'),
                    view_type2 = Sketchup.read_default('AmtflogicBuKong', 'view_type2' , 'shaded_texture')
                    case view_type1
                        when 'wireframe'
                            @filename_project_view = File.join(PLUGIN_DIR, "/styles/wireframe.style")
                        when 'hidden_line'
                            @filename_project_view = File.join(PLUGIN_DIR, "/styles/hidden_line.style")
                        when 'shaded_mode'
                            @filename_project_view = File.join(PLUGIN_DIR, "/styles/shaded_mode.style")
                        else
                            @filename_project_view = File.join(PLUGIN_DIR, "/styles/hidden_line.style")
                        end
                        case view_type2
                        when 'wireframe'
                            @filename_axo_view = File.join(PLUGIN_DIR, "/styles/shaded_texture.style")
                        when 'hidden_line'
                            @filename_axo_view = File.join(PLUGIN_DIR, "/styles/hidden_line.style")
                        when 'shaded_mode'
                            @filename_axo_view = File.join(PLUGIN_DIR, "/styles/shaded_mode.style") 
                        when 'shaded_texture'
                            @filename_axo_view = File.join(PLUGIN_DIR, "/styles/shaded_texture.style")                   
                        else
                            @filename_axo_view = File.join(PLUGIN_DIR, "/styles/shaded_texture.style")
                    end   
                 rescue => error
                    Utilities.print_exception(error, false)
                    Sketchup.set_status_text(error)                   
                    return
                 end
                model = Sketchup.active_model  
                pages = Sketchup.active_model.pages 
                if !pages["amtflogic_origin"].nil? && pages["amtflogic_origin"].valid?  
                    pages.selected_page = pages['amtflogic_origin']                  
                    pages.erase(pages["amtflogic_origin"])
                end                 
                @origin_scene = pages.add "amtflogic_origin"     
                pages.selected_page = pages['amtflogic_origin']
                @origin_scene.transition_time = 0.1
                @origin_scene.update
                @dialog = AMTF_Dialog.dialog
                @dialog.set_size(300, 650)
                @dialog.execute_script('activate_drawing_export()')
                @dialog.on('drawing_export_request_data'){
                    |deferred|
                    begin              
                        deferred.resolve(@scene_data)  
                      rescue => error
                        Utilities.print_exception(error, false)
                        Sketchup.set_status_text(error)
                        deferred.resolve(false)                         
                      end 
                }
                @dialog.on("dimension_tool"){
                    |deferred, page_name, grp_name_id| 
                    return unless page_name && grp_name_id
                    Sketchup.active_model.tools.pop_tool                   
                    params = {
                        :dialog => @dialog,
                        :scene_data => @scene_data,
                        :filename_project_view => @filename_project_view,
                        :filename_axo_view =>  @filename_axo_view,
                        :part_list => @part_list,
                        :page_name => page_name,
                        :grp_name_id => grp_name_id
                    }
                    dim_tool = DimToolModule::DimTool.new params
                    Sketchup.active_model.select_tool(dim_tool)
                    # Attach the observer.
                    # Sketchup.active_model.tools.add_observer(FujjiToolsObserver.new(@dialog))
                }
                @dialog.on('delete_page') { |deferred, page_name, grp_name_id|   
                    pages = Sketchup.active_model.pages 
                    begin  
                        if !pages[page_name].nil? && pages[page_name].valid? && pages[page_name] != 'amtflogic_origin'
                            #Xoa di
                            pages.erase(pages[page_name])
                            delete_page_scene_data(@scene_data, grp_name_id, page_name)   
                            if !pages["amtflogic_origin"].nil? && pages["amtflogic_origin"].valid?                    
                                pages.selected_page = pages['amtflogic_origin']
                            end                          
                            deferred.resolve(true)
                        else
                            deferred.resolve(false)
                        end                          
                    rescue => error
                        Utilities.print_exception(error, false)
                        Sketchup.set_status_text(error)
                        deferred.resolve(false)                         
                    end                    
                }
                @dialog.on('auto_generate_pages') { |deferred, grp_name_id|  
                    pages = Sketchup.active_model.pages
                    begin
                        styles = Sketchup.active_model.styles
                        page_name = grp_name_id + "-amtf-topView"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Z_AXIS.reverse, grp_name_id, @scene_data)
                        page_name = grp_name_id + "-amtf-bottomView"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Z_AXIS, grp_name_id, @scene_data)                   
                        page_name = grp_name_id + "-amtf-frontView"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Y_AXIS, grp_name_id, @scene_data)                    
                        page_name = grp_name_id + "-amtf-backView"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Y_AXIS.reverse, grp_name_id, @scene_data)
                        page_name = grp_name_id + "-amtf-leftView"
                        create_project_scene(@part_list, page_name,  @filename_project_view,X_AXIS, grp_name_id, @scene_data)
                        page_name = grp_name_id + "-amtf-rightView"
                        create_project_scene(@part_list, page_name,  @filename_project_view, X_AXIS.reverse, grp_name_id, @scene_data)
                        page_name = grp_name_id + "-amtf-topView-no-door"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Z_AXIS.reverse, grp_name_id, @scene_data, no_door: true)
                        page_name = grp_name_id + "-amtf-bottomView-no-door"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Z_AXIS, grp_name_id, @scene_data, no_door: true)                    
                        page_name = grp_name_id + "-amtf-frontView-no-door"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Y_AXIS, grp_name_id, @scene_data, no_door: true)                    
                        page_name = grp_name_id + "-amtf-backView-no-door"
                        create_project_scene(@part_list, page_name,  @filename_project_view, Y_AXIS.reverse, grp_name_id, @scene_data, no_door: true)
                        page_name = grp_name_id + "-amtf-leftView-no-door"
                        create_project_scene(@part_list, page_name,  @filename_project_view, X_AXIS, grp_name_id, @scene_data, no_door: true)
                        page_name = grp_name_id + "-amtf-rightView-no-door"
                        create_project_scene(@part_list, page_name,  @filename_project_view, X_AXIS.reverse, grp_name_id, @scene_data, no_door: true)
                        styles.add_style(@filename_axo_view, true)
                        page_name = grp_name_id + "-amtf-isoView"
                        if !pages[page_name].nil? && pages[page_name].valid?
                            pages.erase(pages[page_name])
                        end  
                        isoView = pages.add page_name                  
                        isoView.transition_time = 0.1
                        Sketchup.active_model.layers.each{
                            |la|
                            if la.name == grp_name_id
                                isoView.set_visibility la, true
                            else
                                isoView.set_visibility la, false
                            end
                        }                       
                        selection = Sketchup.active_model.selection
                        selection.clear
                        comp_array = get_comp_in_group_name(@part_list, grp_name_id) 
                        comp_array.each {
                            |ent|
                            selection.add ent
                        }
                        direction=Geom::Vector3d.new -0.666667, 0.666667, -0.333333
                        model.active_view.camera.set( ORIGIN, direction, direction.axes.y)
                        model.active_view.camera.perspective = true
                        model.active_view.zoom selection
                        isoView.update
                        update_scene_data(@scene_data, grp_name_id, page_name)
                        data_to_js = {
                            :grp_name_id => grp_name_id,
                            :scene_data => get_needed_scene(@scene_data)
                        }
                        json_str = data_to_js.to_json                        
                        deferred.resolve(json_str)
                        pages.selected_page = pages['amtflogic_origin']
                    rescue => error
                        pages.selected_page = pages['amtflogic_origin']
                        Utilities.print_exception(error, false)
                        Sketchup.set_status_text(error)
                        deferred.resolve(false)
                    end
                }  
                @dialog.on("drawing_export_setView") { 
                    | deferred , page_name| 
                    begin                       
                        pages = Sketchup.active_model.pages
                        if !pages[page_name].nil? && pages[page_name].valid?                            
                            pages.selected_page = pages[page_name]
                        end  
                        deferred.resolve(true)   
                    rescue => error
                        Utilities.print_exception(error, false)
                        Sketchup.set_status_text(error)
                    end
                    nil
                }                
                @dialog.on("drawing_export_rangeChange") { 
                    | deferred , range|  
                    begin                       
                        exploded_view_by_range(range)
                        deferred.resolve(true)   
                    rescue => error
                        Utilities.print_exception(error, false)
                        Sketchup.set_status_text(error)
                    end
                    nil
                 }
                 @dialog.on("drawing_call_drawing_tool") { |deferred , tool_name|  
                    Sketchup.active_model.tools.pop_tool
                    if tool_name == "custom_scene"
                        capture_custom_scene
                    else 
                        params = {
                            :dialog => @dialog,
                            :scene_data => @scene_data,
                            :filename_project_view => @filename_project_view,
                            :filename_axo_view =>  @filename_axo_view,
                            :part_list => @part_list,
                            :tool_name => tool_name
                        }
                        @drw_tool = DrawingTool::DrawingExport.new params
                        Sketchup.active_model.select_tool(@drw_tool)
                    end
                    nil
                 }
                @dialog.on('update_scene') { |deferred, page_name|  
                    begin
                        if !pages[page_name].nil? && pages[page_name].valid? 
                            status = pages[page_name].update
                            deferred.resolve('ok')
                        end 
                    rescue => error
                        deferred.resolve(false)
                        Utilities.print_exception(error, false)
                        Sketchup.set_status_text(error)
                    end
                    nil
                }
            end 
            def self.update_scene_data(scene_data, grp_name_id, page_id)
                for i in 0..scene_data.size - 1
                    if scene_data[i][:grp_name_id] == grp_name_id && !scene_data[i][:pages].include?(page_id)
                        scene_data[i][:pages] << page_id
                    end
                end
            end
            def self.delete_page_scene_data(scene_data, grp_name_id, page_id)
                for i in 0..scene_data.size - 1
                    if scene_data[i][:grp_name_id] == grp_name_id && scene_data[i][:pages].include?(page_id)
                        scene_data[i][:pages].delete(page_id)
                    end
                end
            end
            def self.create_project_scene(part_list, page_name,  filename, direction, grp_name_id, scene_data, no_door: false, slice_tool: false)
                pages = Sketchup.active_model.pages                
                model = Sketchup.active_model
                pages.selected_page = pages['amtflogic_origin']
                styles = Sketchup.active_model.styles
                styles.add_style(filename, true)
                if !pages[page_name].nil? && pages[page_name].valid?                    
                    pages.erase(pages[page_name])
                end 
                page = pages.add page_name                 
                page.transition_time = 0.1
                if slice_tool
                    page.set_attribute "amtf_dict", "direction", direction.to_json
                end
                Sketchup.active_model.layers.each{
                    |la|
                    if la.name == grp_name_id
                        page.set_visibility la, true
                    else
                        page.set_visibility la, false
                    end
                }          
                hidden_arr = []
                if no_door                    
                    for i in 0..@part_list.size - 1
                        part = @part_list[i]
                        comp_type = Utilities.get_comp_attribute(part.comp, 'comp_type', 'amtf_dict')
                        grp_name = Utilities.get_comp_attribute(part.comp, 'prefix', 'amtf_dict')
                        id = Utilities.fj_uniq_name(grp_name)
                        if comp_type == 'door' && grp_name_id == id
                            part.comp.hidden = true
                            hidden_arr << part.comp
                        end
                    end
                end
                selection = Sketchup.active_model.selection
                selection.clear
                comp_array = get_comp_in_group_name(@part_list, grp_name_id) 
                comp_array.each {
                    |ent|
                    selection.add ent
                }
                model.active_view.camera.set( ORIGIN, direction, direction.axes.y)
                model.active_view.camera.perspective = false
                model.active_view.zoom selection
                page.update        
                update_scene_data(scene_data, grp_name_id, page_name)   
                pages.selected_page = pages[page_name]
                hidden_arr.each do
                    |canh|
                    canh.hidden = false
                end
            end
            def self.unhide_list(comp_list)
                for i in 0..comp_list.size - 1
                    comp_list[i].hidden = false   
                end
            end
            def self.get_comp_in_group_name(part_list, grp_name_id)
                return unless part_list.size > 0 && !grp_name_id.empty?
                tempParr = []
                for i in 0..part_list.size - 1
                    part = part_list[i]
                    grp_name = Utilities.get_comp_attribute(part.comp, 'prefix', 'amtf_dict')
                    next if grp_name.nil?
                    id = Utilities.fj_uniq_name(grp_name)
                    tempParr << part.comp if grp_name_id == id
                end
                return tempParr
            end
            def self.get_pages_with_grp_name_id(grp_name_id)
                #Get all pages
                arr = []
                pages = Sketchup.active_model.pages 
                pages.each {
                    |page| 
                    n = page.name.rindex(/\-amtf\-[^\n]*View/)
                    if !n.nil?                        
                        split_name = page.name.slice(0..n-1)
                        arr << page.name if split_name == grp_name_id
                    end
                }
                return arr
            end
            def self.get_custom_pages
                #Get all pages
                arr = []
                pages = Sketchup.active_model.pages 
                pages.each {
                    |page| 
                    n = page.name.rindex(/c-amtf\-[^\n]*View/)
                    if !n.nil?    
                        arr << page.name 
                    end
                }
                return arr
            end
            def self.get_needed_scene(scene_data)
                sorted_data = []
                for i in 0..scene_data.size - 1
                    item = {
                        :grp_name_id => scene_data[i][:grp_name_id], 
                        :grp_name_title => scene_data[i][:grp_name_title], 
                        :pages => scene_data[i][:pages]
                    }
                    sorted_data << item
                end
                return sorted_data
            end
            def self.exploded_view_by_range(range)               
                selection = Sketchup.active_model.selection        
                if selection.empty?
                    UI.messagebox OB[:selection_empty], MB_OK
                end
                # return unless selection[0].is_a?(Sketchup::Group) && selection[0].layer == range["current_exploded_layer"]
                Sketchup.active_model.start_operation "Exploded View"
                originGroup = Sketchup.active_model.entities.add_group(selection)		       
                centre_selec = originGroup.bounds.center
                selection = originGroup.explode
                #cent_selec là tâm ban đầu
                cent_selec_x = centre_selec[0].to_i   # determination
                cent_selec_y = centre_selec[1].to_i   # du centre de
                cent_selec_z = centre_selec[2].to_i   # la selection
                # Tâm sau khi dịch chuyển
                deplac_pivot_x = cent_selec_x
                deplac_pivot_y = cent_selec_y
                deplac_pivot_z = cent_selec_z
                #Coef là h�?s�?di chuyển (Đơn v�?là Inch)
                coef_trans_x = range["rangeX"].to_i
                coef_trans_y = range["rangeY"].to_i
                coef_trans_z = range["rangeZ"].to_i
                # Dịch trục theo đơn v�?
                convertit = []
                convertit[0]= 1.inch
                convertit[1]= 1.feet
                convertit[2]= 1.mm
                convertit[3]= 1.cm
                convertit[4]= 1.m/100
                uniteCourante = Sketchup.active_model.options["UnitsOptions"]["LengthUnit"]
                coef_trans_x = coef_trans_x.to_f * convertit[uniteCourante]
                coef_trans_y = coef_trans_y.to_f * convertit[uniteCourante]
                coef_trans_z = coef_trans_z.to_f * convertit[uniteCourante]
                # Tính toán khoảng cách giữa tâm ban đầu của vùng chọn và tâm mới
                nouv_pivot_x =  cent_selec_x-deplac_pivot_x
                nouv_pivot_y =  cent_selec_y-deplac_pivot_y
                nouv_pivot_z =  cent_selec_z-deplac_pivot_z
                nouv_pivot_x =  - nouv_pivot_x
                nouv_pivot_y =  - nouv_pivot_y
                nouv_pivot_z =  - nouv_pivot_z
                selection.each do |e| # debut de la transformation
                    next unless e.is_a?(Sketchup::Group)
                    centre_objet = e.bounds.center
                    centre_objet_x = centre_objet[0].to_i  # Determination
                    centre_objet_y = centre_objet[1].to_i  # du centre
                    centre_objet_z = centre_objet[2].to_i  # de l'objet
                    if centre_objet_x == cent_selec_x
                        nouv_centobj_x = nouv_pivot_x
                    else
                        nouv_centobj_x = ((centre_objet_x-cent_selec_x) * coef_trans_x) + nouv_pivot_x
                    end
                   if centre_objet_y == cent_selec_y
                        nouv_centobj_y = nouv_pivot_y
                    else
                        nouv_centobj_y = ((centre_objet_y-cent_selec_y) * coef_trans_y) + nouv_pivot_y
                    end
                   if centre_objet_z == cent_selec_z
                        nouv_centobj_z = nouv_pivot_z
                   else 
                        nouv_centobj_z = ((centre_objet_z-cent_selec_z) * coef_trans_z) + nouv_pivot_z
                   end
                    point_trans = Geom::Point3d.new nouv_centobj_x,nouv_centobj_y,nouv_centobj_z
                    t = Geom::Transformation.new point_trans
                    e.transform!(t)                    
                    Sketchup.active_model.selection.add e
                end
                Sketchup.active_model.commit_operation
            end # exploded_view
            # def self.capture_exploded_scene(page_name, exploded_layer, selection)
            #     if selection.size == 0
            #         UI.messagebox OB[:selection_empty], MB_OK
            #         return
            #     end
            #     pages = Sketchup.active_model.pages  
            #     existing_pages = []  
            #     @scene_data.each do
            #       |scene|
            #       existing_pages.push(*scene[:pages]) if scene[:pages].size > 0
            #     end
            #     if page_name.nil?
            #         page_name = '';  
            #         while page_name == ''
            #         promt_str = OB[:name]
            #         prompts = [promt_str]
            #         defaults = [""]
            #         inputs = UI.inputbox( prompts, defaults ,OB[:name])                  
            #         return if !inputs
            #         name = Utilities.fj_uniq_name(inputs[0])                
            #         page_name = name if !existing_pages.include?(name)
            #         end
            #     end               
            #     styles = Sketchup.active_model.styles
            #     styles.add_style(@filename_axo_view, true)
            #     if !pages[page_name].nil? && pages[page_name].valid?
            #         pages.erase(pages[page_name])
            #     end  
            #     hidden_arr = []
            #     for i in 0..@part_list.size - 1                     
            #         comp = @part_list[i].comp
            #         next unless comp.is_a?(Sketchup::Group) && comp.valid?
            #         if !selection.include?(comp) && !comp.hidden?
            #               hidden_arr << comp
            #               comp.hidden = true
            #         end
            #     end
            #     isoView = pages.add page_name   
            #     isoView.set_visibility exploded_layer, true
            #     isoView.transition_time = 0.1
            #     isoView.update
            #     for i in 0..hidden_arr.size - 1
            #           hidden_arr[i].hidden = false
            #     end
            #     grp_name_id = "amtflogic_custom_scene"
            #     Drawing_Export.update_scene_data(@scene_data, grp_name_id, page_name)            
            #     data_to_js = {
            #         :grp_name_id => grp_name_id,
            #         :scene_data =>Drawing_Export.get_needed_scene(@scene_data) 
            #     }
            #     json_str = data_to_js.to_json
            #     js_command = "update_drawing_list('" + json_str +"');"
            #     @dialog.execute_script(js_command)
            # end
            def self.capture_custom_scene(page_name = nil)
                selection = Sketchup.active_model.selection
                if selection.size == 0
                    UI.messagebox OB[:selection_empty], MB_OK
                    return
                end
                pages = Sketchup.active_model.pages  
                existing_pages = []  
                @scene_data.each do
                  |scene|
                  existing_pages.push(*scene[:pages]) if scene[:pages].size > 0
                end
                if page_name.nil?
                    page_name = '';  
                    while page_name == ''
                        promt_str = OB[:name]
                        prompts = [promt_str]
                        defaults = [""]
                        inputs = UI.inputbox( prompts, defaults ,OB[:name])                  
                        return if !inputs
                        name = "c-amtf-#{Utilities.fj_uniq_name(inputs[0])}View"                
                        page_name = name if !existing_pages.include?(name)
                    end
                end               
                styles = Sketchup.active_model.styles
                styles.add_style(@filename_axo_view, true)
                if !pages[page_name].nil? && pages[page_name].valid?
                    pages.erase(pages[page_name])
                end  
                hidden_arr = []
                for i in 0..@part_list.size - 1                     
                    comp = @part_list[i].comp
                    next unless comp.is_a?(Sketchup::Group) && comp.valid?
                    if !selection.include?(comp) && !comp.hidden?
                          hidden_arr << comp
                          comp.hidden = true
                    end
                end
                isoView = pages.add page_name                 
                isoView.transition_time = 0.1
                isoView.update
                pages.selected_page = pages[page_name]
                for i in 0..hidden_arr.size - 1
                      hidden_arr[i].hidden = false
                end
                grp_name_id = "amtflogic_custom_scene"
                Drawing_Export.update_scene_data(@scene_data, grp_name_id, page_name)            
                data_to_js = {
                    :grp_name_id => grp_name_id,
                    :scene_data =>Drawing_Export.get_needed_scene(@scene_data) 
                }
                json_str = data_to_js.to_json
                js_command = "update_drawing_list('" + json_str +"');"
                @dialog.execute_script(js_command)
          end
        end
    end
end