# encoding: UTF-8
module AMTF
    module BUKONG
    module DrawSheetModule
      def self.make_smartsheet
        sel = Sketchup.active_model.selection
        face = nil
        count = 0
        sel.each{
          |ent|
          if ent.is_a?(Sketchup::Face )
            face = ent 
            count = count + 1
          end
        } 
        if count !=1
          Sketchup.status_text = OB[:selection_inccorect]
          return
        else          
          oloop = face.outer_loop                      
          points = []      
          oloop.vertices.each{ |v| points.push(v.position) }           
          points = Utilities.uniq_points(points) 
          if points.size !=4
            Sketchup.status_text = 'Error 01'
            return
          else
            # transform = this.element.matrixWorld.clone().elements.slice();
            doorGroup_id = Utilities.CreatUniqueid();
            args = {
              'inset' =>  0,
              'gapLeft' =>  0,
              'gapRight' =>  0,
              'gapTop' =>  0,
              'gapBottom' =>  0,
              'safeGap' =>  0,
              'hingeType' =>  'hingeType',
              'splitType' =>  'vertical',
              'width' => 500, 
              'height' => 500, 
              'transform' => IDENTITY,
              'materials_thick' =>  17,
              'dist_array' => {'type' =>  "equal", 'data' =>  2},
              'doorGroup_id' => doorGroup_id , 
              'doorType'  => 'flat',
              'door_params' => {},
            }
            door_grp = AmtfDoorGroup.new( args )
          end
        end
      end
      # Procedure to make sure a Face is 
      def self.check_face
       sel = Sketchup.active_model.selection
       ok = sel.find{ |e| e.is_a?( Sketchup::Face ) }
       ok ? MF_ENABLED : MF_GRAYED
      end
    end#  OffsetToolModule
    end # BuKong
  end # 