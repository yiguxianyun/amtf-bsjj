# encoding: UTF-8
module AMTF
	module BUKONG
		module Name_functions
			def self.create_dialog(title, name = nil)
				options = {
					dialog_title: title,
					preferences_key: 'bukongcabinet.net',
					style: UI::HtmlDialog::STYLE_DIALOG,
					# Set a fixed size now that we know the content size.
					resizable: false,
					scrollable: false,
					width: 500,
					height: 150
				}
				dialog = UI::HtmlDialog.new(options)
				dialog.set_size(options[:width], options[:height]) # Ensure size is set.
				dialog.set_file(PLUGIN_DIR + '/ui/name.html')
				dialog.center
				dialog
			end
			def self.show_group_name_dialog
				@dialog.close if !@dialog.nil?
				@dialog = self.create_dialog(OB[:name_group])
				Bridge.decorate(@dialog)
				@dialog.on('request_data') do |deferred|
					begin
						deferred.resolve({ name: '', name_title: OB[:name] })
					rescue => error
						Utilities.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.add_action_callback(
					'save_name'
				) do |action_context, params|
					command_name_group(params, @dialog)
					nil
				end
				@dialog.show
			end
			def self.command_name_group(params, dialog)
				# Lấy mảng các chi tiết t�?selection
				entities = Sketchup.active_model.selection
				query = AmtfQuery.new
				parts = query.query_sheets(entities)
				parts.each do |part|
					if params
						part.comp.set_attribute(
							'amtf_dict',
							'prefix',
							params.to_s
						)
					end
					subGroup =
						Utilities
							.definition(part.comp)
							.entities
							.grep(Sketchup::Group)
					if subGroup.size > 0
						for i in 0..subGroup.size - 1
							next unless subGroup[i]
							next if subGroup[i].nil?
							next unless subGroup[i].valid?
							group_type =
								subGroup[i].get_attribute('amtf_dict', 'type')
							if group_type == 'bukong_label'
								subGroup[i].erase!
								break
							end
						end
					end
					part.drawArrow(part.mat_gan_nhan)
				end
				dialog.close
			end
		end
	end
end