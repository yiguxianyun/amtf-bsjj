# encoding: UTF-8
module AMTF
	module BUKONG
		TOOL_SCALE = 21_236
		TOOL_MOVE = 21_048
		class AmtfToolsObserver < Sketchup::ToolsObserver
			def initialize
				# @cache = []
				# @active_tool = nil
			end
			def onActiveToolChanged(tools, tool_name, tool_id)
				# FUJI_GLOBAL_TOOL_OBSERVER[:tool_name] = tool_name
				# FUJI_GLOBAL_TOOL_OBSERVER[:tool_id] = tool_id
				# @active_tool = tool_id
				# close_condition = true
				# if tool_id == TOOL_SCALE
				#     @cache.clear
				# end
			end
			def onToolStateChanged(tools, tool_name, tool_id, tool_state)
				# FUJI_GLOBAL_TOOL_OBSERVER[:tool_name] = tool_name
				# FUJI_GLOBAL_TOOL_OBSERVER[:tool_id] = tool_id
				# FUJI_GLOBAL_TOOL_OBSERVER[:tool_state] = tool_state
				# puts "onToolStateChanged tool_state #{tool_state}"
				# return false if @active_tool != tool_id
				# if tool_id == TOOL_SCALE
				# 	if tool_state == 0
				# 		return false if @cache.empty?
				# 		while @cache.size > 0
				# 			entity = @cache.pop
				# 			next unless entity.valid?
				# 			comp_type =
				# 				entity.get_attribute 'amtf_dict', 'comp_type'
				# 			comp_id =
				# 				entity.get_attribute 'amtf_dict', 'comp_id'
				# 			if (comp_type == 'AmtfFrame')
				# 				frame = AMTF_STORE.frame_premitives[comp_id]
				# 				root = frame.root
				# 				if !root.nil?
				# 					tr = frame.transformation
				# 					org =
				# 						frame.origin_cpoint.position.transform(
				# 							tr
				# 						)
				# 					x_point =
				# 						frame.x_cpoint.position.transform(tr)
				# 					y_point =
				# 						frame.y_cpoint.position.transform(tr)
				# 					z_point =
				# 						frame.z_cpoint.position.transform(tr)
				# 					frame_width = org.distance(x_point)
				# 					frame_height = org.distance(z_point)
				# 					frame_depth = org.distance(y_point)
				# 					frame.frame.transform!(
				# 						frame.frame.transformation.inverse
				# 					)
				# 					frame.frame.move!(
				# 						Geom::Transformation.new(org)
				# 					)
				# 					frame_adjust_attrs = {
				# 						unit: AMTF_Dialog.get_model_units,
				# 						root: root,
				# 						frame_width: frame_width.to_f,
				# 						frame_height: frame_height.to_f,
				# 						frame_depth: frame_depth.to_f,
				# 						translate: IDENTITY,
				# 						frame_ID: frame.id,
				# 						frame: frame
				# 					}
				# 					AMTF_Dialog.dialog.execute_script(
				# 						"scale_frame_adjust_command('#{frame_adjust_attrs.to_json}');"
				# 					)
				# 				end
				# 			end
				# 		end
				# 		selection = Sketchup.active_model.selection
				# 		selection.clear
				# 		@cache.clear
				# 	elsif tool_state == 1
				# 		selection = Sketchup.active_model.selection
				# 		selection.each { |ent| @cache.push(ent) }
				# 	end
				# end
			end
		end
		class BuKongEntityObserver < Sketchup::EntityObserver
			def onEraseEntity(entity)
				#  puts "onEraseEntity: #{entity}"
			end
			def onChangeEntity(entity)
				#  puts entity
			end
		end
		class BuKongModelObserver < Sketchup::ModelObserver
			# def onPreSaveModel(model)
			#     saver = AmtfSaver.new
			#     saver.save_json
			# end
			#   def onTransactionUndo(model)
			#     puts "onTransactionUndo: #{model}"
			#   end
			#  def onTransactionStart(model)
			#     puts "onTransactionStart: #{model}"
			#   end
			#   def onTransactionEmpty(model)
			#     puts "onTransactionEmpty: #{model}"
			#   end
			#   def onTransactionCommit(model)
			#     puts "onTransactionCommit: #{model}"
			#   end
			#   def onTransactionAbort(model)
			#     puts "onTransactionAbort: #{model}"
			#   end
		end
		class BuKongAppObserver < Sketchup::AppObserver
			def onNewModel(model)
				model.add_observer(BuKongModelObserver.new)
				model.entities.add_observer(BuKongEntitiesObserver.new)
			end
			def onOpenModel(model)
				AMTF_STORE.reset
				model.entities.add_observer(BuKongEntitiesObserver.new)
			end
			def onQuit()
				saver = AmtfSaver.new
				saver.save_json
			end
			def onUnloadExtension(extension_name)
				# puts "onUnloadExtension: #{extension_name}"
			end
		end # class BuKongAppObserver
		class BuKongEntitiesObserver < Sketchup::EntitiesObserver
			# def onElementModified(entities, entity)
			#   puts "onElementModified entity: #{entity}"
			#    puts "onElementModified entities: #{entities}"
			# end
			# def onElementRemoved(entities, entity_id)
			#   puts "onElementRemoved: #{entity_id}"
			# end
			# def onEraseEntities(entities)
			#   puts "onEraseEntities: #{entities}"
			# end
			def onElementAdded(entities, entity)
				return unless entity.valid?
				comp_type = entity.get_attribute 'amtf_dict', 'comp_type'
				if (comp_type == 'AmtfFrame')
					entity.make_unique
					saver = AmtfSaver.new
					attrs = saver.open_comp_json(entity)
					if attrs
						attrs['root']['id'] = Utilities.CreatUniqueid
						frm = AmtfFrame.new
						frm.init(attrs, entity)
						saver.save_comp_json(frm.frame, attrs)
					end
				end
			end
		end
	end
end