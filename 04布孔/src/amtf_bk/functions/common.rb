# encoding: UTF-8
module AMTF
	module BUKONG
		module Utilities
			def self.print_exception(exception, explicit)
				puts "[#{explicit ? 'EXPLICIT' : 'INEXPLICIT'}] #{exception.class}: #{exception.message}"
				puts exception.backtrace.join("\n")
			end
			def self.definition(instance)
				# Returns the definition for a +Group+, +ComponentInstance+ or +Image+
				# @param [Sketchup::ComponentInstance, Sketchup::Group, Sketchup::Image] instance
				# @return [Sketchup::ComponentDefinition]
				# @since 2.0.0
				#https://sketchucation.com/forums/viewtopic.php?f=180&t=19765
				if instance.is_a?(Sketchup::ComponentInstance)
					# ComponentInstance
					return instance.definition
				elsif instance.is_a?(Sketchup::Group)
					# Group
					#
					# (i) group.entities.parent should return the definition of a group.
					# But because of a SketchUp bug we must verify that group.entities.parent returns
					# the correct definition. If the returned definition doesn't include our group instance
					# then we must search through all the definitions to locate it.
					# 这段代码是在讨论SketchUp的一个bug。
					# 在理论上，`group.entities.parent`应该返回一个组的定义，但是由于这个bug，有时候会返回错误的定义。
					# 因此，需要通过搜索所有定义来定位正确的定义。
					if instance.entities.parent.instances.include?(instance)
						return instance.entities.parent
					else
						Sketchup.active_model.definitions.each do |definition|
							if definition.instances.include?(instance)
								return definition
							end
						end
					end
				elsif instance.is_a?(Sketchup::Image)
					Sketchup.active_model.definitions.each do |definition|
						if definition.image? &&
								definition.instances.include?(instance)
							return definition
						end
					end
				end
				return nil # Error. We should never exit here.
			end
			def self.within_face?(point, face, on_boundary = true)
				# Test if point is inside of a face.
				# @param point [Geom::Point3d]
				# @param face [Sketchup::Face]
				# @param on_boundary [Boolean] Value to return if point is on the boundary
				# (edge/vertex) of face.
				# @return [Boolean]
				return false if !point || !face
				pc = face.classify_point(point)
				if [
						Sketchup::Face::PointOnEdge,
						Sketchup::Face::PointOnVertex
				   ].include?(pc)
					return on_boundary
				end
				outerloop = face.outer_loop
				vertices = outerloop.vertices
				pc == Sketchup::Face::PointInside
			end
			def self.on_boundary?(point, face)
				# Test if point is on boundary of a face.
				#
				# @param point [Geom::Point3d]
				# @param face [Sketchup::Face]
				# @return [Boolean]
				return false if !point || !face
				pc = face.classify_point(point)
				[
					Sketchup::Face::PointOnEdge,
					Sketchup::Face::PointOnVertex
				].include?(pc)
			end
			def self.uniq_points(points)
				# Remove duplicate points from array.
				#
				# Ruby's own `Array#uniq` don't remove duplicated points as they are regarded
				# separate objects, based on #eql? and #hash. Without modifying the SketchUp
				# API classes this method can remove duplicated points based on == comparison.
				#
				# @param points [Array<Geom::Point3d>]
				#
				# @return [Array<Geom::Point3d>]
				return if !points
				pos = []
				for i in 0..points.size - 2
					for j in i + 1..points.size - 1
						if points[j].x == points[i].x &&
								points[j].y == points[i].y &&
								points[j].z == points[i].z
							pos << j
						end
					end
				end
				temp = []
				for i in 0..points.size - 1
					temp << points[i] if !pos.include?(i)
				end
				return temp
			end
			def self.within?(
				point,
				container,
				on_boundary = true,
				verify_solid = true
			)
				# Test if point is inside of container.
				#
				# @param point [Geom::Point3d] Point in the same coordinate system as the
				#   container, not its internal coordinate system.
				# @param container [Sketchup::Group, Sketchup::ComponentInstance]
				# @param on_boundary [Boolean] Value to return if point is on the boundary
				#   (surface) itself.
				# @param verify_solid [Boolean] Test whether container is a solid, and return
				#   false if it isn't. This test can be omitted if the container is known to
				#   be a solid.
				#
				# @return [Boolean]
				return false if verify_solid && !solid?(container)
				point = point.transform(container.transformation.inverse)
				# Cast ray from point and count how many times it intersect mesh to
				# determine what side it is on.
				vector = Geom::Vector3d.new(234, 1343, 345)
				ray = [point, vector]
				intersections = []
				definition(container)
					.entities
					.grep(Sketchup::Face) do |face|
						return on_boundary if within_face?(point, face)
						intersection =
							Geom.intersect_line_plane(ray, face.plane)
						next unless intersection
						next if intersection == point
						# Check that intersection is on ray, and not in the other direction along
						# the line.
						unless (intersection - point).samedirection?(vector)
							next
						end
						next unless within_face?(intersection, face)
						intersections << intersection
					end
				# Ray may have hit right on an edge, intersecting two adjacent faces, or
				# even a vertex and intersected many more.
				# Remove duplicated points.
				intersections = uniq_points(intersections)
				intersections.size.odd?
			end

			def self.solid?(container)
				return false unless instance?(container)
				if container.get_attribute('amtf_dict', 'dogbone_modal')
					return false
				end
				if container.bounds.width * container.bounds.height *
						container.bounds.depth == 0
					return false
				end
				edges = definition(container).entities.grep(Sketchup::Edge)
				return false if edges.size == 0
				found = false
				broken_edges = []
				edges.each do |e|
					if e.faces.size.odd? || e.faces.size == 0
						broken_edges << e
						found = true
						break
					end
				end
				return true if !found
				type = container.get_attribute('amtf_dict', 'type')
				comp_type = container.get_attribute('amtf_dict', 'comp_type')
				if comp_type != 'amtf_axes_group' && type != 'tool_path'
					# Sketchup.active_model.rendering_options['EdgeColorMode'] = 0
					broken_edges.each do |e|
						e.vertices.each do |v|
							Utilities
								.definition(container)
								.entities
								.add_cpoint(v.position)
						end
					end
					definition(container).entities.grep(Sketchup::Edge).each { |e| e.material = 'red' }
					container.material = 'green'
				end
				return false
			end
			def self.is_hole_exception?(container)
				# Kiểm tra xem đối tượng có phải là b�?qua không thực hiện khoan l�?
				dict = container.attribute_dictionary 'part_type_dict'
				if dict
					dict.each do |key, value|
						if key == 'type' && value == 'hole_exception'
							return true
						end
					end
				end
				return false
			end
			def self.instance?(entity)
				# Test if entity is either group or component instance.
				# @param entity [Sketchup::Entity]
				# @return [Boolean]
				entity.is_a?(Sketchup::Group) ||
					entity.is_a?(Sketchup::ComponentInstance)
			end
			def self.point_at_face(face)
				# Find an arbitrary point at a face.
				# @param face [Sketchup::Face]
				# @return [Geom::Point3d, nil] nil is returned for zero area faces.
				# Sometimes invalid faces gets created when intersecting.
				# These are removed when validity check run.
				return if face.area.zero?
				# PolygonMesh.polygon_points in rare situations return points on a line,
				# which would lead to a point on the edge boundary being returned rather
				# than one within face.

				# 在一个面上找到一个任意的点。
				# @param face [Sketchup::Face] 面对象
				# @return [Geom::Point3d, nil] 如果面积为0，则返回nil。
				# 当截面相交时，有时会创建无效的面。
				# 在进行有效性检查时，这些面将被移除。
				# 如果面积为0就返回。
				# 在罕见情况下，PolygonMesh.polygon_points会返回一条直线上的点，
				# 这会导致返回位于边界上的点而不是面内的点。

				index = 1
				begin
					points = face.mesh.polygon_points_at(index)
					index += 1
				end while points[0].on_line?(points[1], points[2])
				#三、线性组合到底在说什么 👇
				# 其实是在分析一组向量中，哪些向量是冗余的，哪些是无可替代的
				Geom.linear_combination(
					0.3458,
					Geom.linear_combination(
						0.412365,
						points[0],
						0.587635,
						points[1]
					),
					0.6542,
					points[2]
				)
			end

			def self.face_contains_points(trf, trp, points, faces_array)
				# @params: points là mảng các điểm cần kiểm tra
				# @ params: faces_aray là mảng các face cần kiểm tra
				# @Output: face là mặt chứa tất c�?các điểm
				return unless points.size > 0 && faces_array.size > 0
				faces_array.uniq
				result =
					faces_array.select do |face|
						points.all? do |point|
							within_face?(
								point_context_convert(point, trp, trf),
								face,
								on_boundary = true
							)
						end
					end
				return result
			end
			def self.the_points_on_face?(p0, p1, face)
				#Same context
				return true if within_face?(p0, face) && within_face?(p1, face)
				false
			end
			def self.same_plane(points)
				# Kiểm tra xem các điểm có cùng đồng phẳng không
				points = uniq_points(points)
				return false if points.size < 3
				return true if points.size == 3
				plane = [points[0], points[1], points[2]]
				for i in 3..points.size - 1
					return false if !points[i].on_plane? plane
				end
				return true
			end

			def self.find_point_online_with_distance(p0, vector, d)
				# Tìm điểm trên đường thẳng line, cách điểm p0 trên đường thẳng đó một khoảng cách d, theo phương của vector ch�?phương
				# Đường thẳng line có dạng phương trình đường thẳng đi qua p0, có vector ch�?phương n(a,b,c)
				# T�?phương trình đường thẳng
				# x = x0 + a*t
				# y = y0 + b*t
				# x = z0 + c*t
				# khoảng cách d = sqrt((x1 - x0)**2 + (y1 - y0)**2 + (z1 - z0)**2)
				return unless p0 && vector && d > 0
				a = vector.x
				b = vector.y
				c = vector.z
				x0 = p0.x
				y0 = p0.y
				z0 = p0.z
				x1 = x0 - a * d / Math.sqrt(a**2 + b**2 + c**2)
				y1 = y0 - b * d / Math.sqrt(a**2 + b**2 + c**2)
				z1 = z0 - c * d / Math.sqrt(a**2 + b**2 + c**2)
				x2 = x0 + a * d / Math.sqrt(a**2 + b**2 + c**2)
				y2 = y0 + b * d / Math.sqrt(a**2 + b**2 + c**2)
				z2 = z0 + c * d / Math.sqrt(a**2 + b**2 + c**2)
				point1 = Geom::Point3d.new(x1, y1, z1)
				point2 = Geom::Point3d.new(x2, y2, z2)
				if p0.vector_to(point1).dot(vector) < 0
					return point2
				else
					return point1
				end
			end
			def self.find_2points_online_with_distance(p0, vector, d)
				# Tìm điểm trên đường thẳng line, cách điểm p0 trên đường thẳng đó một khoảng cách d, theo phương của vector ch�?phương
				# Đường thẳng line có dạng phương trình đường thẳng đi qua p0, có vector ch�?phương n(a,b,c)
				# T�?phương trình đường thẳng
				# x = x0 + a*t
				# y = y0 + b*t
				# x = z0 + c*t
				# khoảng cách d = sqrt((x1 - x0)**2 + (y1 - y0)**2 + (z1 - z0)**2)
				return unless p0 && vector && d > 0
				a = vector.x
				b = vector.y
				c = vector.z
				x0 = p0.x
				y0 = p0.y
				z0 = p0.z
				x1 = x0 - a * d / Math.sqrt(a**2 + b**2 + c**2)
				y1 = y0 - b * d / Math.sqrt(a**2 + b**2 + c**2)
				z1 = z0 - c * d / Math.sqrt(a**2 + b**2 + c**2)
				x2 = x0 + a * d / Math.sqrt(a**2 + b**2 + c**2)
				y2 = y0 + b * d / Math.sqrt(a**2 + b**2 + c**2)
				z2 = z0 + c * d / Math.sqrt(a**2 + b**2 + c**2)
				point1 = Geom::Point3d.new(x1, y1, z1)
				point2 = Geom::Point3d.new(x2, y2, z2)
				return point1, point2
			end
			def self.get_midpoint(p1, p2)
				return unless p1 && p2
				# Tìm trung điểm của đoạn thẳng đi qua 2 điểm p1, p2
				x = (p2.x + p1.x) / 2
				y = (p2.y + p1.y) / 2
				z = (p2.z + p1.z) / 2
				return Geom::Point3d.new x, y, z
			end
			def self.get_axes_trans(z_axis, p0, p1)
				return unless z_axis && p0 && p1
				# Xác định transformation của h�?trục tọa đ�?có trục OZ đi qua điểm p0 và trục OY đi qua p1
				y_axis = p0.vector_to(p1)
				cross = y_axis * z_axis
				x_axis = cross.normalize
				return Geom::Transformation.axes(p0, x_axis, y_axis, z_axis)
			end
			def self.find_long_egde(points)
				# Mô t�? Cho các đỉnh của đa giác, tìm ra hai cạnh dài nhất.
				# Lấy các cạnh của đa giác nạp vào mảng.
				# Thực hiện sắp xếp các mảng theo th�?t�?tăng đần v�?kích thước.
				# Lấy ra 2 cạnh nằm cuối dẫy
				return unless points
				return unless points.size >= 4
				egde_array = []
				for i in 0..points.size - 1
					if i == points.size - 1
						egde_array << {
							p1: points[i],
							p2: points[0],
							distance: points[i].distance(points[0])
						}
					else
						egde_array << {
							p1: points[i],
							p2: points[i + 1],
							distance: points[i].distance(points[i + 1])
						}
					end
				end
				size = egde_array.size
				out = egde_array.sort_by { |a| a[:distance] }
				# Kiểm tra xem 2 cạnh dài có song song với nhau không
				return out[size - 1], out[size - 2]
			end
			def self.find_short_egde(points)
				# Mô t�? Cho các đỉnh của đa giác, tìm ra cạnh ngắn nhất.
				# Lấy các cạnh của đa giác nạp vào mảng.
				# Thực hiện sắp xếp các mảng theo th�?t�?tăng đần v�?kích thước.
				# Lấy ra cạnh nằm đầu dãy
				return unless points
				return unless points.size >= 4
				egde_array = []
				for i in 0..points.size - 1
					if i == points.size - 1
						egde_array << {
							p1: points[i],
							p2: points[0],
							distance: points[i].distance(points[0])
						}
					else
						egde_array << {
							p1: points[i],
							p2: points[i + 1],
							distance: points[i].distance(points[i + 1])
						}
					end
				end
				size = egde_array.size
				out = egde_array.sort_by { |a| a[:distance] }
				# Kiểm tra xem 2 cạnh dài có song song với nhau không
				return out[0]
			end
			#--------------------------------------------------------------------------------------------------
			# Thuật toán sắp xếp các điểm theo chiều ngược kim đồng h�?của đỉnh một đa giác
			#https://stackoverflow.com/questions/14370636/sorting-a-list-of-3d-coplanar-points-to-be-clockwise-or-counterclockwise?noredirect=1&lq=1
			# Thuật toán bao gồm:
			# 1) get_arbitrary_point: lấy trung tâm các điểm
			# 2) Xác định vetor pháp tuyến của mặt chứa các điểm
			# 3) You have the center C and the normal n. To determine whether point B is clockwise or counterclockwise from point A,
			#  calculate dot(n, cross(A-C, B-C)). If the result is positive, B is counterclockwise from A; if it's negative, B is clockwise from A.
			# 4) Sắp xếp các điểm theo chiều (ccw_order)
			def self.get_arbitrary_point(points)
				return unless points && points.size >= 3
				mesh = Geom::PolygonMesh.new
				polygon_index = mesh.add_polygon points
				index = 1
				begin
					points = mesh.polygon_points_at(index)
					index += 1
				end while points[0].on_line?(points[1], points[2])
				return(
					Geom.linear_combination(
						0.3458,
						Geom.linear_combination(
							0.412365,
							points[0],
							0.5485,
							points[1]
						),
						0.4495,
						points[2]
					)
				)
			end
			def self.calcArea(poly)
				return unless poly
				return if poly.size < 3
				en = poly.size - 1
				sum = poly[en].x * poly[0].y - poly[0].x * poly[en].y
				for i in 0..en - 1
					n = i + 1
					sum += poly[i].x * poly[n].y - poly[n].x * poly[i].y
				end
				return sum
			end
			def self.isClockwise?(poly)
				return false unless poly
				return false if poly.size < 3
				return calcArea(poly) > 0
			end
			def self.orientation_ccw2d?(p1, p2, p3)
				# 	if (o == 0)
				#   document.write("Linear");
				# else if (o == 1)
				#   document.write("Clockwise");
				# else
				#   document.write("CounterClockwise");
				val =
					(p2.y - p1.y) * (p3.x - p2.x) -
						(p2.x - p1.x) * (p3.y - p2.y)
				return 0 if val == 0
				return 1 if val > 0
				return 2
			end
			def self.ccw_order?(a, b, center, normal)
				return unless a && b && center && normal
				vectorCA = center.vector_to(a)
				vectorCB = center.vector_to(b)
				cross = vectorCA * vectorCB
				cross.normalize!
				dot = normal.dot(cross)
				return dot > 0 ? true : false
			end
			def self.ccwSort2D(points)
				points = points.sort_by { |p| p.y }
				# Get center y
				cy = (points.first.y + points.last.y) / 2
				# Sort from right to left
				points = points.sort_by { |p| p.x }
				cx = (points.first.x + points.last.x) / 2
				center = Geom::Point3d.new(cx, cy, 0)
				#     // // Pre calculate the angles as it will be slow in the sort
				#     // // As the points are sorted from right to left the first point
				#     // // is the rightmost
				#     // // Starting angle used to reference other angles
				startAng = nil
				tempHash = []
				points.each do |item|
					ang = Math.atan2(item.y - center.y, item.x - center.x)
					if startAng.nil?
						startAng = ang
					elsif ang < startAng
						# ensure that all points are clockwise of the start point
						ang += Math::PI * 2
					end
					tempHash << { point: item, angle: ang }
				end
				# first sort clockwise
				tempHash.sort_by { |item| item[:angle] }
				# then reverse the order
				tempHash = tempHash.reverse
				# tempHash.unshift(tempHash.pop())
				ccwPoints = []
				tempHash.each { |item| ccwPoints << item[:point] }
				ccwPoints
			end
			def self.get_sorted_points_ccw(points)
				return unless points
				size = points.size
				return unless points.size >= 3
				center = find_center_of_points(points)
				plane = Geom.fit_plane_to_points(points)
				normal = Geom::Vector3d.new(plane[0], plane[1], plane[2])
				loop do
					# we need to create a variable that will be checked so that we don't run into an infinite loop scenario.
					swapped = false
					# subtract one because Ruby arrays are zero-index based
					(size - 1).times do |i|
						if ccw_order?(points[i + 1], points[i], center, normal)
							tmp =
								Geom::Point3d.new points[i].x,
								                  points[i].y,
								                  points[i].z
							points[i] = points[i + 1]
							points[i + 1] = tmp
							swapped = true
						end
					end
					break if not swapped
				end
				points
			end
			def self.getSnapMesh(entity, list, transformation = IDENTITY)
				if (
						entity.is_a?(Sketchup::Selection) ||
							entity.is_a?(Sketchup::Entities) ||
							entity.is_a?(Array)
				   )
					entity.each do |child_entity|
						getSnapMesh(child_entity, list, transformation)
					end
				elsif entity.is_a?(Sketchup::Group)
					comp_type =
						Utilities.get_comp_attribute(
							entity,
							'comp_type',
							'amtf_dict'
						)
					if comp_type === 'Snap_Mesh'
						instance_tr = transformation * entity.transformation
						list.push({ entity: entity, trans: instance_tr })
					end
				end
			end
			def self.traverseArray(entity, list, transformation = IDENTITY)
				# Here, do something with this entity. Check its type when calling methods that not all entities have.
				# To convert points to global coordinates, transform them with `point.transform(transformation)`
				# If this node can have children, traverse its children.
				if (
						entity.is_a?(Sketchup::Selection) ||
							entity.is_a?(Sketchup::Entities) ||
							entity.is_a?(Array)
				   )
					entity.each do |child_entity|
						traverseArray(child_entity, list, transformation)
					end
				elsif Utilities.instance?(entity)
					# Multiply the outer coordinate system with the group's/component's local coordinate system.
					entity.make_unique
					# .make_unique đ�?khi thao tác sau này không làm thay đổi các chi tiết khác một cách không kiểm soát được
					instance_tr = transformation * entity.transformation
					if has_sub_com(entity)
						Utilities
							.definition(entity)
							.entities
							.each do |child_entity|
								next unless Utilities.instance?(child_entity)
								traverseArray(child_entity, list, instance_tr)
							end
					elsif Utilities.solid?(entity)
						sheetpart =
							SheetPart.new entity,
							              instance_tr,
							              transformation,
							              entity.persistent_id
						list << sheetpart
					end
				end
			end
			def self.transform_decompose(m)
				m = m.clone
				# Extract translation
				translation = m.values_at(12, 13, 14)
				# Extract scaling, considering uniform scale factor (last matrix element)
				scaling = Array.new(3)
				scaling[0] = m[15] * Math.sqrt(m[0]**2 + m[1]**2 + m[2]**2)
				scaling[1] = m[15] * Math.sqrt(m[4]**2 + m[5]**2 + m[6]**2)
				scaling[2] = m[15] * Math.sqrt(m[8]**2 + m[9]**2 + m[10]**2)
				# Remove scaling to prepare for extraction of rotation
				[0, 1, 2].each { |i| m[i] /= scaling[0] } unless scaling[0] ==
					0.0
				[4, 5, 6].each { |i| m[i] /= scaling[1] } unless scaling[1] ==
					0.0
				[8, 9, 10].each { |i| m[i] /= scaling[2] } unless scaling[2] ==
					0.0
				m[15] = 1.0
				# Verify orientation, if necessary invert it.
				tmp_z_axis =
					Geom::Vector3d
						.new(m[0], m[1], m[2])
						.cross(Geom::Vector3d.new(m[4], m[5], m[6]))
				if tmp_z_axis.dot(Geom::Vector3d.new(m[8], m[9], m[10])) < 0
					scaling[0] *= -1
					m[0] = -m[0]
					m[1] = -m[1]
					m[2] = -m[2]
				end
				# Extract rotation
				# Source: Extracting Euler Angles from a Rotation Matrix, Mike Day, Insomniac Games
				# http://www.insomniacgames.com/mike-day-extracting-euler-angles-from-a-rotation-matrix/
				theta1 = Math.atan2(m[6], m[10])
				c2 = Math.sqrt(m[0]**2 + m[1]**2)
				theta2 = Math.atan2(-m[2], c2)
				s1 = Math.sin(theta1)
				c1 = Math.cos(theta1)
				theta3 =
					Math.atan2(s1 * m[8] - c1 * m[4], c1 * m[5] - s1 * m[9])
				rotation = [-theta1, -theta2, -theta3]
				return translation, scaling, rotation
			end
		#region
			# def self.traverseMinifix(entities)
			#     return unless entities
			#     list = []
			#     for i in 0..entities.size - 1
			#         entity = entities[i]
			#         next unless Utilities.instance?(entity)
			#         boundingbox = entity.bounds
			#         volume = boundingbox.width * boundingbox.height * boundingbox.depth
			#         next if volume === 0
			#         grep_face = entity.entities.grep(Sketchup::Face)
			#         next if grep_face.size == 0
			#         explode = Utilities.get_comp_attribute(entity, 'amtflogic_exploded', 'amtf_dict')
			#         if explode != 'true' || Utilities.hasParent?(entity)
			#             UI.messagebox OB[:explode_required], MB_OK
			#             return
			#         end
			#         if instance?(entity)
			#             entity.make_unique
			#             subGroup = entity.entities.grep(Sketchup::Group)
			#             next if subGroup.size == 0
			#             transformation = entity.transformation
			#             for j in 0..subGroup.size - 1
			#                 mini = subGroup[j]
			#                 instance_tr = transformation * mini.transformation
			#                 type = Utilities.get_comp_attribute(mini,'type', 'amtf_dict')
			#                 path_id = Utilities.get_comp_attribute(mini,'path_id', 'amtf_dict')
			#                 path_name = Utilities.get_comp_attribute(mini,'path_name', 'amtf_dict')
			#                 if type == 'oc_cam' ||
			#                     type == 'chot_cam' ||
			#                     type == 'chot_go' ||
			#                     type == 'chanel' ||
			#                     type == 'custom_groove' ||
			#                     type == 'hinge' ||
			#                     type == 'tool_path' ||
			#                     type == 'curve_groove' ||
			#                     type == 'chot_dot' ||
			#                     type == 'khoan_moi' ||
			#                     type == 'patV' ||
			#                     type == 'rafix' ||
			#                     type == 'lockdowel' ||
			#                     type == 's_hinge' ||
			#                     type == 'rafix_D' ||
			#                     type == 'rafix_D1' ||
			#                     type == 'rafix_D2'
			#                     minifixID = Utilities.get_comp_attribute(mini,'minifixID', 'amtf_dict')
			#                     partner_sheet_id = Utilities.get_comp_attribute(mini,'partner_sheet_id', 'amtf_dict')
			#                     data = {
			#                         :instance => mini,
			#                         :trans => instance_tr,
			#                         :parent_tr => transformation,
			#                         :parent => entity,
			#                         :type => type,
			#                         :minifixID => minifixID,
			#                         :path_id => path_id,
			#                         :path_name => path_name,
			#                         :partner_sheet_id => partner_sheet_id
			#                     }
			#                     list << data
			#                 end
			#             end
			#         end
			#     end
			#     return list
			# end
		#end region
			def self.buildMinifixList(entities)
				#Đ�?sinh hash chuyển sang Node
				return unless entities
				list = Hash.new
				for i in 0..entities.size - 1
					entity = entities[i]
					next unless Utilities.instance?(entity)
					if instance?(entity)
						subGroup =
							definition(entity).entities.grep(Sketchup::Group)
						next if subGroup.size == 0
						transformation = entity.transformation
						for j in 0..subGroup.size - 1
							mini = subGroup[j]
							instance_tr = transformation * mini.transformation
							type =
								Utilities.get_comp_attribute(
									mini,
									'type',
									'amtf_dict'
								)
							path_id =
								Utilities.get_comp_attribute(
									mini,
									'path_id',
									'amtf_dict'
								)
							path_name =
								Utilities.get_comp_attribute(
									mini,
									'path_name',
									'amtf_dict'
								)
							partner_sheet_id =
								Utilities.get_comp_attribute(
									mini,
									'partner_sheet_id',
									'amtf_dict'
								)
							minifixID =
								Utilities.get_comp_attribute(
									mini,
									'minifixID',
									'amtf_dict'
								)
							if type == 'oc_cam' || type == 'chot_cam' ||
									type == 'chot_go' || type == 'chanel' ||
									type == 'custom_groove' ||
									type == 'hinge' || type == 'tool_path' ||
									type == 'curve_groove' ||
									type == 'chot_dot' || type == 'khoan_moi' ||
									type == 'patV' || type == 'rafix_D' ||
									type == 'rafix_D1' || type == 'rafix_D2' ||
									type == 'lockdowel'
								data = {
									instance: mini,
									trans: instance_tr,
									parent_tr: transformation,
									parent: entity,
									type: type,
									path_id: path_id,
									path_name: path_name,
									partner_sheet_id: partner_sheet_id,
									minifixID: minifixID
								}
								list[mini.persistent_id.to_s] = data
							end
						end
					end
				end
				return list
			end
			def self.minifixHash(entities)
				return unless entities
				list = Hash.new
				for i in 0..entities.size - 1
					entity = entities[i]
					next unless Utilities.instance?(entity)
					explode =
						Utilities.get_comp_attribute(
							entity,
							'amtflogic_exploded',
							'amtf_dict'
						)
					if explode != 'true' || Utilities.hasParent?(entity)
						UI.messagebox OB[:explode_required], MB_OK
						return
					end
					if instance?(entity)
						entity.make_unique
						subGroup = entity.entities.grep(Sketchup::Group)
						next if subGroup.size == 0
						transformation = entity.transformation
						for j in 0..subGroup.size - 1
							mini = subGroup[j]
							instance_tr = transformation * mini.transformation
							type =
								Utilities.get_comp_attribute(
									mini,
									'type',
									'amtf_dict'
								)
							path_id =
								Utilities.get_comp_attribute(
									mini,
									'path_id',
									'amtf_dict'
								)
							path_name =
								Utilities.get_comp_attribute(
									mini,
									'path_name',
									'amtf_dict'
								)
							if type == 'oc_cam' || type == 'chot_cam' ||
									type == 'chot_go' || type == 'rafix_D' ||
									type == 'rafix_D1' || type == 'rafix_D2'
								minifixID =
									Utilities.get_comp_attribute(
										mini,
										'minifixID',
										'amtf_dict'
									)
								data = {
									instance: mini,
									trans: instance_tr,
									parent_tr: transformation,
									parent: entity,
									type: type,
									minifixID: minifixID,
									path_id: path_id,
									path_name: path_name
								}
								if list.has_key?(minifixID)
									list[minifixID] << data
								else
									list[minifixID] = [data]
								end
							end
						end
					end
				end
				return list
			end
			def self.getMinifixByType(entities, miniType)
				return unless entities
				list = []
				for i in 0..entities.size - 1
					entity = entities[i]
					explode =
						Utilities.get_comp_attribute(
							entity,
							'amtflogic_exploded',
							'amtf_dict'
						)
					if explode != 'true' || Utilities.hasParent?(entity)
						UI.messagebox OB[:explode_required], MB_OK
						return
					end
					if instance?(entity)
						entity.make_unique
						subGroup = entity.entities.grep(Sketchup::Group)
						next if subGroup.size == 0
						transformation = entity.transformation
						for j in 0..subGroup.size - 1
							mini = subGroup[j]
							instance_tr = transformation * mini.transformation
							type =
								Utilities.get_comp_attribute(
									mini,
									'type',
									'amtf_dict'
								)
							path_id =
								Utilities.get_comp_attribute(
									mini,
									'path_id',
									'amtf_dict'
								)
							path_name =
								Utilities.get_comp_attribute(
									mini,
									'path_name',
									'amtf_dict'
								)
							if type == miniType
								minifixID =
									Utilities.get_comp_attribute(
										mini,
										'minifixID',
										'amtf_dict'
									)
								data = {
									instance: mini,
									trans: instance_tr,
									parent_tr: transformation,
									parent: entity,
									type: type,
									minifixID: minifixID,
									path_id: path_id,
									path_name: path_name
								}
								list << data
							end
						end
					end
				end
				return list
			end
			def self.get_parrent_trans(comp, entity, transformation, found)
				return transformation if found[:found]
				# Trong đ�?quy, return trong chương trình nó ch�?tr�?lại cho vòng đ�?quy trước đó
				# truyền tham chiếu đến found bên ngoài đ�?lấy được giá tr�?khi dừng chương trình
				if (
						entity.is_a?(Sketchup::Selection) ||
							entity.is_a?(Sketchup::Entities)
				   )
					entity.each do |child_entity|
						get_parrent_trans(
							comp,
							child_entity,
							transformation,
							found
						)
					end
				elsif Utilities.instance?(entity)
					instance_tr = transformation * entity.transformation
					if has_sub_com(entity)
						Utilities
							.definition(entity)
							.entities
							.each do |child_entity|
								next unless Utilities.instance?(child_entity)
								get_parrent_trans(
									comp,
									child_entity,
									instance_tr,
									found
								)
							end
					elsif comp == entity
						found[:found] = true
						found[:parent_trans] = transformation
						found[:trans] = instance_tr
					end
				end
			end
			def self.get_comp_by_id(compID, entity, found)
				return entity if found[:found]
				# Trong đ�?quy, return trong chương trình nó ch�?tr�?lại cho vòng đ�?quy trước đó
				# truyền tham chiếu đến found bên ngoài đ�?lấy được giá tr�?khi dừng chương trình
				if (
						entity.is_a?(Sketchup::Selection) ||
							entity.is_a?(Sketchup::Entities)
				   )
					entity.each do |child_entity|
						get_comp_by_id(compID, child_entity, found)
					end
				elsif Utilities.instance?(entity)
					the_id =
						Utilities.get_comp_attribute(
							entity,
							'compID',
							'amtf_dict'
						)
					if has_sub_com(entity)
						Utilities
							.definition(entity)
							.entities
							.each do |child_entity|
								next unless Utilities.instance?(child_entity)
								get_comp_by_id(compID, child_entity, found)
							end
					elsif the_id == compID
						found[:found] = true
						found[:comp] = entity
					end
				end
			end
			def self.has_sub_com(container)
				# Kiểm tra xem trong Group hoặc Component có chứa bất k�?Group hoặc Component nào không
				return false unless Utilities.instance?(container)
				found = false
				Utilities
					.definition(container)
					.entities
					.each do |obj|
						# Trường hợp 1: obj là solid, true nếu như nó là instance
						# Trường hợp 2: nếu obj không solid
						#   2.1 Con của nó không có instance
						#   2.2 Con của nó có instance
						if Utilities.instance?(obj)
							if Utilities.solid?(obj)
								found = true
							else
								found = true if Utilities
									.definition(obj)
									.entities
									.any? do |item|
										Utilities.instance?(item) &&
											Utilities.get_comp_attribute(
												obj,
												'type',
												'amtf_dict'
											) != 'bukong_label'
									end
							end
						end
					end
				return found
			end
			# def self.buil_sheet_part_list(entities, explode_required = true)
			#     # Lấy mảng các chi tiết t�?selection
			#     return unless entities
			#     entities = entities.to_a
			#     list = []
			#     #chia các nhóm 50 đ�?giảm b�?nh�?khi đ�?quy
			#     size = entities.size
			#     if size > 50
			#         entities = entities.each_slice(50).to_a
			#     else
			#         entities = [entities]
			#     end
			#     length = entities.size
			#     progress = ExtraFuntion::ProgressBar.new(length,OB[:build_part_list])
			#     for i in 0..length - 1
			#         traverseArray(entities[i], list, transformation = IDENTITY)
			#         progress.update(i)
			#         if(i == length - 1)
			#             Sketchup.set_status_text(OB[:completed])
			#         end
			#     end
			#     return list
			# end
			def self.buil_sheet_part_list(entities)
				# Lấy mảng các chi tiết t�?selection
				return unless entities
				entities = entities.to_a
				list = []
				#chia các nhóm 50 đ�?giảm b�?nh�?khi đ�?quy
				size = entities.size
				if size > 50
					entities = entities.each_slice(50).to_a
				else
					entities = [entities]
				end
				length = entities.size
				progress =
					ExtraFuntion::ProgressBar.new(length, OB[:build_part_list])
				for i in 0..length - 1
					traverseArray(entities[i], list, transformation = IDENTITY)
					progress.update(i)
					if (i == length - 1)
						Sketchup.set_status_text(OB[:completed])
					end
				end
				return list
			end
			def self.sheet_part_list(entities)
				list = []
				entities.each do |entity|
					next unless entity.is_a?(Sketchup::Group)
					next unless solid?(entity)
					boundingbox = entity.bounds
					volume =
						boundingbox.width * boundingbox.height *
							boundingbox.depth
					next if volume === 0
					explode =
						Utilities.get_comp_attribute(
							entity,
							'amtflogic_exploded',
							'amtf_dict'
						)
					if explode != 'true' || Utilities.hasParent?(entity)
						UI.messagebox OB[:explode_required], MB_OK
						return
					else
						persistent_id = entity.persistent_id
						sheetpart =
							SheetPart.new entity,
							              entity.transformation,
							              IDENTITY,
							              persistent_id
						list << sheetpart
					end
				end
				return list
			end
			def self.sheet_comp_list(entities)
				list = []
				entities.each do |entity|
					next unless entity.is_a?(Sketchup::Group)
					explode =
						Utilities.get_comp_attribute(
							entity,
							'amtflogic_exploded',
							'amtf_dict'
						)
					if explode != 'true' || Utilities.hasParent?(entity)
						UI.messagebox OB[:explode_required], MB_OK
						return
					else
						next unless solid?(entity)
						list << entity
					end
				end
				return list
			end
			# def self.drawing_sheet_part_list
			#     # Lấy mảng các chi tiết t�?selection
			#     entities = Sketchup.active_model.entities
			#     entities = entities.to_a
			#     list = []
			#     #chia các nhóm 50 đ�?giảm b�?nh�?khi đ�?quy
			#     size = entities.size
			#     if size > 50
			#         entities = entities.each_slice(50).to_a
			#     else
			#         entities = [entities]
			#     end
			#     length = entities.size
			#     progress = ExtraFuntion::ProgressBar.new(length,OB[:build_part_list])
			#     i = 0
			#     timer = UI.start_timer(0.1,true){
			#         if i == length
			#             UI.stop_timer(timer)
			#             Sketchup.set_status_text(OB[:completed])
			#             Drawing_Export.execute_command(list)
			#         end
			#         if i < length
			#             begin
			#                 traverseArray(entities[i], list, transformation = IDENTITY)
			#                 progress.update(i)
			#                 i +=1
			#                 progress.update(i)
			#             rescue => error
			#                 Utilities.print_exception(error, false)
			#             end
			#         end
			#     }
			# end
			# def self.nesting_sheet_part_list(entities, dialog, callback)
			#     # Lấy mảng các chi tiết t�?selection
			#     return unless entities
			#     entities = entities.to_a
			#     list = []
			#     #chia các nhóm 50 đ�?giảm b�?nh�?khi đ�?quy
			#     size = entities.size
			#     if size > 50
			#         entities = entities.each_slice(50).to_a
			#     else
			#         entities = [entities]
			#     end
			#     length = entities.size
			#     progress = ExtraFuntion::ProgressBar.new(length,OB[:build_part_list])
			#     i = 0
			#     timer = UI.start_timer(0.1,true){
			#         if i == length
			#             UI.stop_timer(timer)
			#             Sketchup.set_status_text(OB[:completed])
			#             callback.call(list, dialog)
			#         end
			#         if i < length
			#             begin
			#                 traverseArray(entities[i], list, transformation = IDENTITY)
			#                 progress.update(i)
			#                 i +=1
			#                 progress.update(i)
			#             rescue => error
			#                 Utilities.print_exception(error, false)
			#             end
			#         end
			#     }
			# end
			def self.sheet_part_hash(entities)
				# Hash 1: comp chứa những mini nào
				# Hash 2: mini này thì cha nó là ai
				# Hash 3: comp này chứa custom path nào
				# Hash 4: mini này thì nó là loại gì (cam chot, ban l�? ..)
				# Hash 5:
				#  Chung quy lại là có 2 Hash
				# Hash lấy comp làm key: chứa thông tin liên quan đến:
				# - Loop
				# - path
				# - Cam chốt
				# - Bản l�?
				# - Khoan mồi
				#  - vv
				#  Hash lấy mini làm key
				# - Parent
				# - type
				# -
				#  Hash được lông trong hash
				comp_hash = Hash.new
				mini_hash = Hash.new
				material_hash = Hash.new
				entities.each do |entity|
					next unless entity.is_a?(Sketchup::Group)
					explode =
						Utilities.get_comp_attribute(
							entity,
							'amtflogic_exploded',
							'amtf_dict'
						)
					if explode != 'true' || Utilities.hasParent?(entity)
						UI.messagebox OB[:explode_required], MB_OK
						return
					else
						next unless entity.valid?
						next unless Utilities.solid?(entity)
						entity.make_unique
						material = entity.material
						if material_hash.has_key?(material)
							material_hash[material] << entity
						else
							material_hash[material] = [entity]
						end
						mini_list = []
						oc_cam_list = []
						chot_go_list = []
						chot_cam_list = []
						khoan_moi_list = []
						chot_dot_list = []
						patV_list = []
						rafix_list = []
						lockdowel_list = []
						hinge_list = []
						ranh_list = []
						tool_path_list = []
						comp_id =
							entity.get_attribute('amtf_dict', 'pathcomp_id')
						if !comp_id.nil?
							big_faces = Utilities.get_big_faces(entity)
							big_faces.each do |f|
								loops = f.loops
								loops.each do |lo|
									path_json =
										lo.get_attribute(
											'amtf_dict',
											'path_json'
										)
									unless path_json.nil?
										mini_hash[lo] = {
											parent: entity,
											type: 'loop',
											path_json: path_json,
											trans: entity.transformation,
											edges: lo.edges
										}
										tool_path_list << lo
									end
								end
							end
						end
						ents = entity.entities
						ents.each do |obj|
							next unless obj.is_a?(Sketchup::Group)
							mini_list << obj
							type = obj.get_attribute('amtf_dict', 'type')
							if type == 'oc_cam'
								oc_cam_list << obj
							elsif type == 'chot_cam'
								chot_cam_list << obj
							elsif type == 'chot_go'
								chot_go_list << obj
							elsif type == 'patV'
								patV_list << obj
							elsif type == 'khoan_moi'
								khoan_moi_list << obj
							elsif type == 'chot_dot'
								chot_dot_list << obj
							elsif type == 'rafix'
								rafix_list << obj
							elsif type == 'lockdowel'
								lockdowel_list << obj
							elsif type == 'hinge'
								hinge_list << obj
							elsif type == 'chanel' || type == 'custom_groove'
								ranh_list << obj
							elsif type == 'tool_path' || type == 'curve_groove'
								tool_path_list << obj
							end
							path_json =
								obj.get_attribute('amtf_dict', 'path_json')
							mini_hash[obj] = {
								parent: entity,
								type: type,
								path_json: path_json,
								trans:
									entity.transformation * obj.transformation,
								edges: obj.entities.grep(Sketchup::Edge)
							}
						end
						comp_hash[entity] = {
							mini_list: mini_list,
							oc_cam_list: oc_cam_list,
							chot_go_list: chot_go_list,
							chot_cam_list: chot_cam_list,
							khoan_moi_list: khoan_moi_list,
							chot_dot_list: chot_dot_list,
							patV_list: patV_list,
							rafix_list: rafix_list,
							lockdowel_list: lockdowel_list,
							hinge_list: hinge_list,
							ranh_list: ranh_list,
							tool_path_list: tool_path_list
						}
					end
				end
				return(
					{
						comp_hash: comp_hash,
						mini_hash: mini_hash,
						material_hash: material_hash
					}
				)
			end
			def self.get_part_by_comp(part_list, comp)
				part_list.each { |part| return part if part.comp == comp }
				nil
			end
			def self.same_array?(arr1, arr2)
				return false unless arr1 && arr2 && arr1.size == arr2.size
				arr2 & arr1 == arr2
			end
			def self.coplanar_edges?(e1, e2)
				# Kiểm tra xem 2 edges có đồng phẳng không
				return false unless e1 && e2
				pts1 = []
				e1.vertices.each { |v| pts1 << v.position }
				pts2 = []
				e2.vertices.each { |v| pts2 << v.position }
				plane = [pts1[0], pts1[1], pts2[0]]
				return true if pts2[1].on_plane?(plane)
				return false
			end
			def self.coplanar_3edges?(e1, e2, e3)
				# Kiểm tra xem 3 edges có đồng phẳng không
				return false unless e1 && e2 && e3
				return false unless coplanar_edges?(e1, e2)
				pts1 = []
				e1.vertices.each { |v| pts1 << v.position }
				pts2 = []
				e2.vertices.each { |v| pts2 << v.position }
				pts3 = []
				e3.vertices.each { |v| pts3 << v.position }
				plane = [pts1[0], pts1[1], pts2[0]]
				if pts3[0].on_plane?(plane) && pts3[1].on_plane?(plane)
					return true
				end
				return false
			end
			def self.find_near_point(p, p_array)
				# Tìm điểm trong mảng p_array, có khoảng cách tới điểm p là ngắn nhất
				return unless p && p_array.size > 0
				temp = []
				p_array.each do |point|
					temp << { point: point, distance: p.distance(point) }
				end
				sorted = temp.sort_by { |obj| obj[:distance] }
				return sorted[0][:point]
			end
			def self.generate_id()
				t = Time.now
				id = t.strftime('%Y%m%d%k%M%S%L') # Get current date to the milliseconds
				id = id.to_i.to_s(36)
			end
			def self.point3d_to_string(pt)
				x = pt.x.to_inch.to_s
				y = pt.y.to_inch.to_s
				z = pt.z.to_inch.to_s
				point_s = x + ',' + y + ',' + z
			end
			def self.string_to_point3d(point_s)
				array = point_s.split(',').map { |s| s.to_f }
				x = array[0]
				y = array[1]
				z = array[2]
				pt = Geom::Point3d.new(x, y, z)
			end
			def self.points_json_encode(points)
				arr = []
				points.each { |pt| arr << point3d_to_string(pt) }
				arr.to_json
			end
			def self.points_json_decode(str)
				arr = JSON.parse(str)
				arr = arr.map { |point_s| string_to_point3d(point_s) }
			end
			def self.string_vector_decode(vector_s)
				array =
					vector_s
						.split(',')
						.map do |s|
							num = s.scan(/\d+\.*\d*/)
							num[0].to_f
						end
				x = array[0]
				y = array[1]
				z = array[2]
				pt = Geom::Vector3d.new(x, y, z)
			end
			def self.get_comp_attribute(entity, attrs, dict_name)
				# return unless  entity.is_a?(Sketchup::Group)
				return unless entity.valid?
				dict = entity.attribute_dictionary dict_name
				if dict
					dict.each { |key, value| return value.to_s if key == attrs }
				end
			end
			def self.points_in_root_context(points, tr1)
				arr = []
				points.each do |pt|
					arr << point_context_convert(pt, tr1, IDENTITY)
				end
				return arr
			end
			def self.points_array_context_convert(points, tr1, tr2)
				arr = []
				points.each { |pt| arr << point_context_convert(pt, tr1, tr2) }
				return arr
			end
			def self.puts_matrix(t)
				# @param [Geom::Transformation]
				# In ra ma trận transformation cho mục đích debug
				a = t.to_a
				printf "%8.03f %8.03f %8.03f %8.03f\n", a[0], a[4], a[8], a[12]
				printf "%8.03f %8.03f %8.03f %8.03f\n", a[1], a[5], a[9], a[13]
				printf "%8.03f %8.03f %8.03f %8.03f\n", a[2], a[6], a[10], a[14]
				printf "%8.03f %8.03f %8.03f %8.03f\n", a[3], a[7], a[11], a[15]
			end
			def self.transY(y, container_height)
				return container_height - y
			end
			def self.transLenY(height)
				return 0 - height
			end
			def self.CreatUniqueid()
				t = Time.now
				id = t.strftime('%Y%m%d%k%M%S%L') # Get current date to the milliseconds
				# puts id
				id = id.to_i.to_s(36) # will generate somthing like "5i0sp1h4tkc"
				# Reverse it
				# puts id
				id.to_i(36)
				# puts id
				str = get_random_string(5)
				id += str
				id = 'id' + id
				return id
			end
			def self.get_random_string(length = 5)
				source =('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a + %w[_ - .]
				key = ''
				length.times { key += source[rand(source.size)].to_s }
				return key
			end
			def self.CreatFileName()
				t = Time.now
				id = t.strftime('%Y%m%d%k%M%S%L') # Get current date to the milliseconds
				id = id.to_i.to_s(36) # will generate somthing like "5i0sp1h4tkc"
				# Reverse it
				id.to_i(36)
				str = get_random_string_filename(5)
				id += str
				id = 'id' + id
				return id
			end
			def self.get_random_string_filename(length = 5)
				source = ('a'..'z').to_a + ('A'..'Z').to_a
				key = ''
				length.times { key += source[rand(source.size)].to_s }
				return key
			end
			def self.distance_X(points, preMax)
				#preMax là x lớn nhất trước đó
				distance = 0
				max = 0
				points.each do |p|
					if p.x.abs > max
						max = p.x.abs
						if (p.x < 0)
							distance = 2 * p.x.abs
						else
							distance = p.x
						end
					end
				end
				return distance.to_l + 2.to_l + preMax.to_l
			end
			def self.max_X(points)
				# Tìm x lớn nhất trong các points
				max = 0
				points.each { |p| max = p.x.abs if p.x.abs > max }
				return max.to_l
			end
			def self.offset_vector(edge, face, d, matcanh)
				# Xác đinh vector ch�?phương đ�?offset cho cạnh
				other_edge = face.edges.select { |e| e != edge }
				p1 = nil
				line = edge.line
				other_edge.each do |ent|
					vertex0 = ent.vertices[0].position
					vertex1 = ent.vertices[1].position
					if !vertex0.on_line?(line)
						p1 = vertex0
						break
					elsif !vertex1.on_line?(line)
						p1 = vertex1
						break
					end
				end
				p0 = p1.project_to_line(line)
				# Tìm điểm p trên đoạn thẳng, nối giữa 2 điểm p0(x0, y0, z0) và p1(x1, y1, z1), cách điểm p1 trên đường thẳng đó một khoảng cách d
				# Đường thẳng đi qua p1(x0,y0,z0) có vector ch�?phương n = [a,b,c]
				# a = x1 - x0
				# b = y1 - y0
				# c = z1 - z0
				# T�?phương trình đường thẳng
				# x = x0 + a*t
				# y = y0 + b*t
				# x = z0 + c*t
				# khoảng cách d = sqrt((x1 - x0)**2 + (y1 - y0)**2 + (z1 - z0)**2)
				a = p1.x - p0.x
				b = p1.y - p0.y
				c = p1.z - p0.z
				x0 = p0.x
				y0 = p0.y
				z0 = p0.z
				return nil if Math.sqrt(a**2 + b**2 + c**2) == 0
				x2 = x0 - a * d / Math.sqrt(a**2 + b**2 + c**2)
				y2 = y0 - b * d / Math.sqrt(a**2 + b**2 + c**2)
				z2 = z0 - c * d / Math.sqrt(a**2 + b**2 + c**2)
				x3 = x0 + a * d / Math.sqrt(a**2 + b**2 + c**2)
				y3 = y0 + b * d / Math.sqrt(a**2 + b**2 + c**2)
				z3 = z0 + c * d / Math.sqrt(a**2 + b**2 + c**2)
				point1 = Geom::Point3d.new(x2, y2, z2)
				point2 = Geom::Point3d.new(x3, y3, z3)
				vector1 = p0.vector_to(point1)
				vector2 = p0.vector_to(point2)
				vector3 = matcanh.normal
				if vector1.dot(vector3) < 0
					return vector1
				else
					return vector2
				end
			end
			def self.moving_vector(
					clickPoint,
					chot_cam_center,
					chot_go_center,
					distance
				)
				#S�?dụng lại
				p0 = chot_cam_center
				p1 = chot_go_center
				d = distance
				a = p1.x - p0.x
				b = p1.y - p0.y
				c = p1.z - p0.z
				x0 = p0.x
				y0 = p0.y
				z0 = p0.z
				x2 = x0 - a * d / Math.sqrt(a**2 + b**2 + c**2)
				y2 = y0 - b * d / Math.sqrt(a**2 + b**2 + c**2)
				z2 = z0 - c * d / Math.sqrt(a**2 + b**2 + c**2)
				x3 = x0 + a * d / Math.sqrt(a**2 + b**2 + c**2)
				y3 = y0 + b * d / Math.sqrt(a**2 + b**2 + c**2)
				z3 = z0 + c * d / Math.sqrt(a**2 + b**2 + c**2)
				point1 = Geom::Point3d.new(x2, y2, z2)
				point2 = Geom::Point3d.new(x3, y3, z3)
				vetor1 = chot_cam_center.vector_to(clickPoint)
				vector2 = chot_cam_center.vector_to(chot_go_center)
				if vetor1.dot(vector2) < 0
					return chot_cam_center.vector_to(point1)
				else
					return chot_cam_center.vector_to(point2)
				end
			end
			def self.hinge_moving_vector(
					origin_point,
					clickPoint,
					nearest_edge,
					distance
				)
				line = nearest_edge.line
				projected_point = clickPoint.project_to_line(line)
				second_point =
					find_point_between_two_points(
						projected_point,
						clickPoint,
						get_option('hinge_L')
					)
				destination =
					find_point_between_two_points(
						origin_point,
						second_point,
						distance
					)
				return origin_point.vector_to(destination)
			end
			def self.hole_moving_vector(
					origin_point,
					clickPoint,
					line,
					distance
				)
				#Đường thẳng song song đi qua origin_point
				para_line = [origin_point, line[1]]
				#Hình chiếu của clickPoint tren para_line
				projected_point = clickPoint.project_to_line(para_line)
				return if point_compare?(projected_point, origin_point)
				vector = origin_point.vector_to(projected_point).normalize
				vector = vector_multiply(distance.to_l, vector)
				return vector
			end
			# def self.extendGroove(points)
			#     return points if points.size < 2
			#     points.push(points[0])
			#     h = []
			#     for i in 0..points.size - 1
			#         h << {:p => points[i], :dist => points[i].distance(points[i+1])}
			#     end
			#     h.sort_by{|p| p[:dist] }
			# end
			def self.get_option(str)
				if str == 'ban_kinh_cam'
					return(
						0.5 *
							Utilities.normalize(
								Sketchup.read_default(
									'AmtflogicBuKong',
									'duong_kinh_cam',
									15
								).to_s
							)
					)
				elsif str == 'vi_tri_oc_cam'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'vi_tri_oc_cam',
								50
							).to_s
						)
					)
				elsif str == 'khoang_cach_cam_chot'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'khoang_cach_cam_chot',
								32
							).to_s
						)
					)
				elsif str == 'khoang_chia_lo_cam_min'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'khoang_chia_lo_cam_min',
								90
							).to_s
						)
					)
				elsif str == 'khoang_chia_lo_cam_max'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'khoang_chia_lo_cam_max',
								1000
							).to_s
						)
					)
				elsif str == 'mini_fix_big_radius'
					return(
						0.5 *
							Utilities.normalize(
								Sketchup.read_default(
									'AmtflogicBuKong',
									'mini_fix_big_diameter',
									10
								).to_s
							)
					)
				elsif str == 'mini_fix_small_radius'
					return(
						0.5 *
							Utilities.normalize(
								Sketchup.read_default(
									'AmtflogicBuKong',
									'mini_fix_small_diameter',
									8
								).to_s
							)
					)
				elsif str == 'do_dai_cam'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'do_dai_cam',
								34
							).to_s
						)
					)
				elsif str == 'do_sau_cam'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'do_sau_cam',
								9
							).to_s
						)
					)
				elsif str == 'do_sau_chot_cam'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'do_sau_cam',
								9
							).to_s
						)
					)
				elsif str == 'hinge_d'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'hinge_d',
								35
							).to_s
						)
					)
				elsif str == 'hinge_L'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'hinge_L',
								21.5
							).to_s
						)
					)
				elsif str == 'hinge_max_distance'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'hinge_max_distance',
								1000
							).to_s
						)
					)
				elsif str == 'hinge_min_distance'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'hinge_min_distance',
								90
							).to_s
						)
					)
				elsif str == 'vi_tri_hinge'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'vi_tri_hinge',
								100
							).to_s
						)
					)
				elsif str == 'no_dowel'
					return(
						Sketchup.read_default(
							'AmtflogicBuKong',
							'no_dowel',
							false
						)
					)
				elsif str == 'day_nep_dan_canh'
					return(
						Utilities.normalize(
							Sketchup.read_default(
								'AmtflogicBuKong',
								'day_nep_dan_canh',
								1
							).to_s
						)
					)
				elsif str == 'all_edge_banding'
					return(
						Sketchup.read_default(
							'AmtflogicBuKong',
							'all_edge_banding',
							false
						)
					)
				end
			end
			def self.points_with_distance(p0, line, d)
				# Tìm điểm trên đường thẳng line, cách điểm p0 trên đường thẳng đó một khoảng cách d, theo phương của vector ch�?phương
				# Đường thẳng line có dạng phương trình đường thẳng đi qua p0, có vector ch�?phương n(a,b,c)
				# T�?phương trình đường thẳng
				# x = x0 + a*t
				# y = y0 + b*t
				# x = z0 + c*t
				# khoảng cách d = sqrt((x1 - x0)**2 + (y1 - y0)**2 + (z1 - z0)**2)
				return unless p0 && line.size > 0 && d > 0
				a = line[1].x
				b = line[1].y
				c = line[1].z
				x0 = p0.x
				y0 = p0.y
				z0 = p0.z
				x1 = x0 - a * d / Math.sqrt(a**2 + b**2 + c**2)
				y1 = y0 - b * d / Math.sqrt(a**2 + b**2 + c**2)
				z1 = z0 - c * d / Math.sqrt(a**2 + b**2 + c**2)
				x2 = x0 + a * d / Math.sqrt(a**2 + b**2 + c**2)
				y2 = y0 + b * d / Math.sqrt(a**2 + b**2 + c**2)
				z2 = z0 + c * d / Math.sqrt(a**2 + b**2 + c**2)
				point1 = Geom::Point3d.new(x1, y1, z1)
				point2 = Geom::Point3d.new(x2, y2, z2)
				return point1, point2
			end
			# def self.inside_direction(face, point0, point1)
			#     mid_point = get_midpoint(point0,point1)
			#     perpend = find_perpendicular_at_point(point0, mid_point, point1, face)
			#     return unless perpend && perpend.size > 0
			#     #Khoảng cách đ�?nh�?
			#     d = 10.mm
			#     points = points_with_distance(mid_point, perpend[0], d)
			#     return unless points && points.size == 2
			#     vector0 = mid_point.vector_to(points[0])
			#     vector1 = mid_point.vector_to(points[1])
			#     center = find_center_face(face)
			#     vector_to_center = mid_point.vector_to(center)
			#     result = nil
			#     if vector0.dot(vector_to_center) > 0
			#         result = vector0.normalize
			#     elsif vector1.dot(vector_to_center) > 0
			#         result = vector1.normalize
			#     end
			#     return result
			#     #Tr�?v�?vector đơn v�?ch�?phương vào phía trong của mặt
			# end
			# def self.inside_direction_vector(point0, point1, comp, d)
			#     return unless point0 && point1 && comp && d
			#     side_face = get_edge_faces(comp)
			#     return unless side_face
			#     foundEdge = nil
			#     foundFace = nil
			#     side_face.each do | face |
			#        face.edges.each do |e|
			#             if point0.on_line?(e.line) && point1.on_line?(e.line)
			#                 foundEdge = e
			#                 foundFace = face
			#                 break
			#             end
			#        end
			#     end
			#     p1 = nil
			#     return unless foundFace
			#     foundFace.edges.each do |e|
			#         v0 = e.vertices[0].position
			#         v1 = e.vertices[1].position
			#         if !v0.on_line?(foundEdge.line)
			#             p1 = v0
			#             break
			#         end
			#         if !v1.on_line?(foundEdge.line)
			#             p1 = v1
			#             break
			#         end
			#     end
			#     line = foundEdge.line
			#     p0 = p1.project_to_line(line)
			#     # Tìm điểm p trên đoạn thẳng, nối giữa 2 điểm p0(x0, y0, z0) và p1(x1, y1, z1), cách điểm p1 trên đường thẳng đó một khoảng cách d
			#     # Đường thẳng đi qua p1(x0,y0,z0) có vector ch�?phương n = [a,b,c]
			#     # a = x1 - x0
			#     # b = y1 - y0
			#     # c = z1 - z0
			#     # T�?phương trình đường thẳng
			#     # x = x0 + a*t
			#     # y = y0 + b*t
			#     # x = z0 + c*t
			#     # khoảng cách d = sqrt((x1 - x0)**2 + (y1 - y0)**2 + (z1 - z0)**2)
			#     a = p1.x - p0.x
			#     b = p1.y - p0.y
			#     c = p1.z - p0.z
			#     x0 = p0.x
			#     y0 = p0.y
			#     z0 = p0.z
			#     x2 = x0 - a*d/Math.sqrt(a**2 + b**2 + c**2)
			#     y2 = y0 - b*d/Math.sqrt(a**2 + b**2 + c**2)
			#     z2 = z0 - c*d/Math.sqrt(a**2 + b**2 + c**2)
			#     x3 = x0 + a*d/Math.sqrt(a**2 + b**2 + c**2)
			#     y3 = y0 + b*d/Math.sqrt(a**2 + b**2 + c**2)
			#     z3 = z0 + c*d/Math.sqrt(a**2 + b**2 + c**2)
			#     pts1 =  Geom::Point3d.new(x2, y2, z2)
			#     pts2 =  Geom::Point3d.new(x3, y3, z3)
			#     vector1 = p0.vector_to(pts1)
			#     vector2 = p0.vector_to(pts2)
			#     vector3 = p0.vector_to(p1)
			#     if vector1.dot(vector3) > 0
			#         return vector1
			#     else
			#         return vector2
			#     end
			# end
			def self.find_perpendicular_at_point(p1, p2, p3, face)
				#Đường thẳng đi qua p1 và p3, điểm h�?vuông góc là p2
				return unless p1 && p2 && p3 && face # && within_face?(p1, face, on_boundary = true) && within_face?(p3, face, on_boundary = true)
				# Tìm phương trình đường thẳng đi qua p2, thuộc mặt phẳng face chứa p1 và p3
				# vuông góc với đường thẳng đi qua 2 điểm p1 và p3
				# Lấy một điểm bất k�?p4 thuộc mặt phẳng face, dựng đường thẳng line đi qua p4, có vector ch�?phương của đường thẳng p1-p3
				# Tìm điểm p5 là hình chiếu của p2 trên đường thằng line vừa dựng
				# Đường thẳng đi qua p2-p5 chính là đường thẳng cần tìm
				# Tr�?v�?2 phương trình đường thẳng với vector ch�?phương ngược chiều
				p4 = point_at_face(face)
				vector = p1.vector_to(p3)
				line = [p4, vector]
				p5 = p2.project_to_line(line)
				vector_a = p2.vector_to(p5)
				vector_b = p5.vector_to(p2)
				return [p2, vector_a], [p2, vector_b]
			end
			def self.point_of_perpendicular_at_point(p1, p2, p3, face)
				return unless p1 && p2 && p3 && face # && within_face?(p1, face, on_boundary = true) && within_face?(p3, face, on_boundary = true)
				# Tìm phương trình đường thẳng đi qua p2, thuộc mặt phẳng face chứa p1 và p3
				# vuông góc với đường thẳng đi qua 2 điểm p1 và p3
				# Lấy một điểm bất k�?p4 thuộc mặt phẳng face, dựng đường thẳng line đi qua p4, có vector ch�?phương của đường thẳng p1-p3
				# Tìm điểm p5 là hình chiếu của p2 trên đường thằng line vừa dựng
				# Đường thẳng đi qua p2-p5 chính là đường thẳng cần tìm
				# Tr�?v�?2 phương trình đường thẳng với vector ch�?phương ngược chiều
				p4 = point_at_face(face)
				vector = p1.vector_to(p3)
				line = [p4, vector]
				p5 = p2.project_to_line(line)
				return p5
			end
			def self.pocket_distance(face)
				# Tính khoảng cách t�?đường ngoài vào đường trong
				# Lấy một cạnh, dựng đường vuông góc, tìm giao điểm với  đường của loop kia, nếu có nhiều điểm cắt thì lây điểm gần nhất
				loops = face.loops
				return if loops.size != 2
				edges_0 = loops[0].edges
				edges_1 = loops[1].edges
				p1 = edges_0[0].start.position
				p3 = edges_0[0].end.position
				perpend_line =
					Utilities.find_perpendicular_at_point(p1, p1, p3, face)
				distArr = []
				edges_1.each do |edge|
					pt = intersect_line_edge(perpend_line[0], edge)
					next unless pt
					dist = p1.distance(pt)
					distArr << dist
				end
				distArr = distArr.sort
				distArr[0]
			end
			def self.update_face_data(attrs)
				data = JSON.parse(attrs[:json])
				tempData = []
				if attrs[:type] == 'edge_hole'
					data.each do |item|
						if item['faceCompID'].to_s === attrs[:faceCompID].to_s
							next
						end
						tempData << item
					end
					if attrs[:points]
						edge_hole = {
							faceCompID: attrs[:faceCompID],
							edgeCompID: attrs[:edgeCompID],
							the_big_faceID: attrs[:the_big_faceID],
							the_side_faceID: attrs[:the_side_faceID],
							points: attrs[:points]
						}
						tempData << edge_hole
					end
					attrs[:face].set_attribute 'amtf_dict',
					                           'edge_hole',
					                           tempData.to_json
				elsif attrs[:type] == 'face_hole'
					data.each do |item|
						next if item['edgeCompID'] == attrs[:edgeCompID]
						tempData << item
					end
					if attrs[:points]
						face_hole = {
							faceCompID: attrs[:faceCompID],
							edgeCompID: attrs[:edgeCompID],
							the_big_faceID: attrs[:the_big_faceID],
							the_side_faceID: attrs[:the_side_faceID],
							points: attrs[:points]
						}
						tempData << face_hole
					end
					attrs[:face].set_attribute 'amtf_dict',
					                           'face_hole',
					                           tempData.to_json
				end
			end
			def self.find_face_in_comp(compID, faceID)
				findComp = { found: false, comp: nil }
				entities = Sketchup.active_model.entities
				get_comp_by_id(compID, entities, findComp)
				comp = findComp[:comp]
				faces = get_big_faces(comp)
				faces.each do |face|
					id =
						Utilities.get_comp_attribute(
							face,
							'faceID',
							'amtf_dict'
						)
					if faceID == id
						return face
						break
					end
				end
			end
			def self.delete_minifix(container, minifixID)
				# Delete minifix
				definition(container)
					.entities
					.each do |ent|
						if ent.is_a?(Sketchup::Group)
							minifix =
								get_comp_attribute(
									ent,
									'minifixID',
									'amtf_dict'
								)
							ent.erase! if minifix == minifixID
						end
					end
			end
			def self.parse_input(inputs)
				inputs = inputs.map { |input| normalize(input) }
				handle_error if inputs.empty?
				inputs[0]
			end
			def self.isDivive?(input)
				is_div = %r{^\/\d+}.match(input)
				return true if !is_div.nil? # Nếu không phải là s�?
				false
			end
			def self.isNumber?(input)
				return false if input.empty? || input.nil?
				is_num = /[^0-9\.\-\+]/.match(input)
				return true if is_num.nil? # Nếu không phải là s�?
				false
			end
			def self.normalize(input)
				return if !isNumber?(input)
				# unit = Utilities.get_model_units()
				# input = (input.to_s.to_f.mm) if unit == 'MILLIMETERS'
				# input = (input.to_s.to_f.feet) if unit == 'FEET'
				# input = (input.to_s.to_f.yard) if unit == 'DEFAULT'
				# input = (input.to_s.to_f.cm) if unit == 'CENTIMETERS'
				# input = (input.to_s.to_f.m) if unit == 'METERS'
				return input.to_s.to_l
			end
			def self.toDegrees(input)
				return if input.match(/[^0-9\.]\-\+/) # Nếu không phải là s�?
				return input.to_f.degrees
			end
			def self.unitConvert(input)
				return 0 if input === 0
				unit = Utilities.get_model_units
				return input.mm if unit == 'MILLIMETERS'
				return input.feet if unit == 'FEET'
				return input.yard if unit == 'DEFAULT'
				return input.cm if unit == 'CENTIMETERS'
				return input.m if unit == 'METERS'
				return input if unit == 'INCHES'
			end
			def self.handle_error
				# you could raise an error or just return the default
				puts 'error'
			end
			def self.update_compFaceData(
				faceComp,
				the_big_face,
				the_big_faceID,
				minifixID,
				translationVector,
				action_key
			)
				compFaceData =
					Utilities.get_comp_attribute(
						the_big_face,
						'face_hole',
						'amtf_dict'
					)
				compFaceData = JSON.parse(compFaceData)
				# Duyệt qua compFaceData, khi gặp được minifix thì thực hiện x�?lý
				Utilities.delete_minifix(faceComp, minifixID)
				found = false
				compFaceData.each do |item|
					item['points'].each do |obj|
						if obj['minifixID'].to_s == minifixID.to_s
							found = true
							if action_key == 'delete'
								item['points'].delete(obj)
							elsif action_key == 'move'
								#Translate chot cam
								translation =
									Geom::Transformation.translation translationVector
								chot_cam =
									Utilities.string_to_point3d(obj['chot_cam'])
								#Translate chot go
								chot_go =
									Utilities.string_to_point3d(obj['chot_go'])
								#Luu
								obj['chot_cam'] =
									point3d_to_string(
										chot_cam.transform(translation)
									)
								obj['chot_go'] =
									point3d_to_string(
										chot_go.transform(translation)
									)
								plasticBase =
									Utilities
										.definition(faceComp)
										.entities
										.add_group
								plasticBase.set_attribute 'amtf_dict',
								                          'type',
								                          'minifix'
								plasticBase.set_attribute 'amtf_dict',
								                          'minifixID',
								                          minifixID
								circle_vector = the_big_face.normal.normalize!
								plasticBase.entities.add_circle chot_cam
										.transform(translation),
								                                circle_vector,
								                                Utilities
										.get_option('mini_fix_big_radius')
								plasticBase.entities.add_circle chot_go
										.transform(translation),
								                                circle_vector,
								                                Utilities
										.get_option('mini_fix_small_radius')
							end
						end
					end
					compFaceData.delete(item) if item['points'].size == 0
				end
				if found
					the_big_face.set_attribute 'amtf_dict',
					                           'face_hole',
					                           compFaceData.to_json
				end
			end
			def self.removeRelatedCompAttributes(entity)
				# If this node can have children, traverse its children.
				if entity.is_a?(Sketchup::Model)
					entity.entities.each do |child_entity|
						removeRelatedCompAttributes(child_entity)
					end
				elsif entity.is_a?(Sketchup::Group) ||
						entity.is_a?(Sketchup::ComponentInstance)
					entity.definition.entities.each do |child_entity|
						removeRelatedCompAttributes(child_entity)
					end
				end
			end
			def self.find_center_face(face)
				return unless face
				edges = face.edges
				vertices = []
				for i in 1..edges.length - 1
					vertices << edges[i].vertices[0]
					vertices << edges[i].vertices[1]
				end
				vertices.uniq!
				vertices[vertices.length] = vertices[0] ### =loop
				sum_x = 0
				sum_y = 0
				sum_z = 0
				for i in (0...vertices.length - 1)
					sum_x += vertices[i].position.x
					sum_y += vertices[i].position.y
					sum_z += vertices[i].position.z
				end ###for
				x = sum_x / (vertices.length - 1)
				y = sum_y / (vertices.length - 1)
				z = sum_z / (vertices.length - 1)
				centroid = Geom::Point3d.new x, y, z
				return centroid
			end
			def self.find_center_of_points(v)
				return unless v.size > 0
				vertices = v.map { |p| Geom::Point3d.new p.x, p.y, p.z }
				vertices[vertices.size] = vertices[0] ### =loop
				sum_x = 0
				sum_y = 0
				sum_z = 0
				for i in (0...vertices.size - 1)
					sum_x += vertices[i].x
					sum_y += vertices[i].y
					sum_z += vertices[i].z
				end ###for
				x = sum_x / (vertices.size - 1)
				y = sum_y / (vertices.size - 1)
				z = sum_z / (vertices.size - 1)
				centroid = Geom::Point3d.new x, y, z
				# vertices.pop # Tr�?lại nguyên trạng vertices
				return centroid
			end
			def self.make_uniq_array(arr)
				return if !arr
				pos = []
				for i in 0..arr.size - 2
					for j in i + 1..arr.size - 1
						pos << j if arr[i].to_l == arr[j].to_l
					end
				end
				temp = []
				for i in 0..arr.size - 1
					temp << arr[i] if !pos.include?(i)
				end
				return temp
			end
			def self.amtfFileWrite(filename, data)
				path = File.join(PLUGIN_DIR, 'db/', filename)
				f = File.open path, 'w+'
				f.write(data)
				f.close
			end
			def self.amtfFileRead(filename)
				data = []
				path = File.join(PLUGIN_DIR, 'db/', filename)
				if File.exists?(path)
					f = File.open path, 'r'
					for line in f.readlines
						data << line
					end
					f.close
					return data
				else
					return []
				end
			end
			def self.viewMinifix
				selection = Sketchup.active_model.selection
				minifixList = []
				traverseMinifix(selection, minifixList)
				for i in 0..minifixList.size - 1
					begin
						item = minifixList[i]
						center = find_centroid(item[:instance])
						#Đưa ra context của group cha (part)
						center =
							point_context_convert(
								center,
								item[:trans],
								item[:parent_tr]
							)
						#Xác định pháp tuyến, kiểm tra xem nó thuộc mặt nào của cha
						faces =
							definition(item[:parent])
								.entities
								.grep(Sketchup::Face)
						find_face = nil
						for j in 0..faces.size - 1
							if within_face?(center, faces[j])
								find_face = faces[j]
								break
							end
						end
						circle_vector = find_face.normal.normalize!
						if item[:type] == 'oc_cam'
							# Check nếu như đã tồn tại
							found = false
							item[:parent]
								.entities
								.grep(Sketchup::Group)
								.each do |model|
									if get_comp_attribute(
											model,
											'type',
											'amtf_dict'
									   ) == 'occam_model'
										found = true if get_comp_attribute(
											model,
											'minifixID',
											'amtf_dict'
										) == item[:minifixID]
										break
									end
								end
							if !found
								circle_vector = circle_vector.reverse
								minifixModel =
									definition(item[:parent]).entities.add_group
								minifixModel.set_attribute 'amtf_dict',
								                           'type',
								                           'occam_model'
								minifixModel.set_attribute 'amtf_dict',
								                           'minifixID',
								                           item[:minifixID]
								circle =
									minifixModel.entities.add_circle center,
									                                 circle_vector,
									                                 get_option(
											'ban_kinh_cam'
									                                 )
								mod_face =
									minifixModel.entities.add_face(circle)
								mod_face.pushpull(
									get_option('do_sau_cam') +
										get_option('mini_fix_big_radius')
								)
								minifixModel.material = 'red'
							end
						elsif item[:type] == 'chot_cam'
							circle_vector = circle_vector.reverse
							va_arr = circle_vector.to_a
							#vector dịch chuyển bằng phép nhân khoảng cách cần dịch chuyển với vector pháp tuyến �?đây đ�?normalize
							heso = get_option('do_sau_chot_cam')
							translation =
								Geom::Vector3d.new [
										heso * va_arr[0],
										heso * va_arr[1],
										heso * va_arr[2]
								                   ]
							transformation =
								Geom::Transformation.new translation
							center = center.transform(transformation)
							minifixModel =
								definition(item[:parent]).entities.add_group
							minifixModel.set_attribute 'amtf_dict',
							                           'type',
							                           'chotcam_model'
							minifixModel.set_attribute 'amtf_dict',
							                           'minifixID',
							                           item[:minifixID]
							circle =
								minifixModel.entities.add_circle center,
								                                 circle_vector,
								                                 get_option(
										'mini_fix_big_radius'
								                                 )
							mod_face = minifixModel.entities.add_face(circle)
							mod_face.reverse!
							mod_face.pushpull(heso + get_option('do_dai_cam'))
							minifixModel.material = 'red'
						elsif item[:type] == 'chot_go'
							circle_vector = circle_vector.reverse
							va_arr = circle_vector.to_a
							heso = get_option('do_sau_chot_cam')
							translation =
								Geom::Vector3d.new [
										heso * va_arr[0],
										heso * va_arr[1],
										heso * va_arr[2]
								                   ]
							transformation =
								Geom::Transformation.new translation
							center = center.transform(transformation)
							minifixModel =
								definition(item[:parent]).entities.add_group
							minifixModel.set_attribute 'amtf_dict',
							                           'type',
							                           'chotgo_model'
							minifixModel.set_attribute 'amtf_dict',
							                           'minifixID',
							                           item[:minifixID]
							circle =
								minifixModel.entities.add_circle center,
								                                 circle_vector,
								                                 get_option(
										'mini_fix_small_radius'
								                                 )
							mod_face = minifixModel.entities.add_face(circle)
							mod_face.reverse!
							mod_face.pushpull(2 * get_option('do_sau_cam'))
							minifixModel.material = 'red'
						end
					rescue => error
						Utilities.print_exception(error, false)
					end
				end
			end
			def self.minifixModel(item, point)
				center = find_centroid(item[:instance])
				#Đưa ra context của group cha (part)
				center =
					point_context_convert(
						center,
						item[:trans],
						item[:parent_tr]
					)
				#Xác định pháp tuyến, kiểm tra xem nó thuộc mặt nào của cha
				faces = definition(item[:parent]).entities.grep(Sketchup::Face)
				find_face = nil
				for j in 0..faces.size - 1
					if within_face?(center, faces[j])
						find_face = faces[j]
						break
					end
				end
				circle_vector = find_face.normal.normalize!
				if item[:type] == 'oc_cam'
					# Check nếu như đã tồn tại
					found = false
					item[:parent]
						.entities
						.grep(Sketchup::Group)
						.each do |model|
							if get_comp_attribute(model, 'type', 'amtf_dict') ==
									'occam_model'
								found = true if get_comp_attribute(
									model,
									'minifixID',
									'amtf_dict'
								) == item[:minifixID]
								break
							end
						end
					if !found
						circle_vector = circle_vector.reverse
						minifixModel =
							definition(item[:parent]).entities.add_group
						minifixModel.set_attribute 'amtf_dict',
						                           'type',
						                           'occam_model'
						minifixModel.set_attribute 'amtf_dict',
						                           'minifixID',
						                           item[:minifixID]
						circle =
							minifixModel.entities.add_circle center,
							                                 circle_vector,
							                                 get_option(
									'ban_kinh_cam'
							                                 )
						mod_face = minifixModel.entities.add_face(circle)
						mod_face.pushpull(
							get_option('do_sau_cam') +
								get_option('mini_fix_big_radius')
						)
						minifixModel.material = 'red'
					end
				elsif item[:type] == 'chot_cam'
					circle_vector = circle_vector.reverse
					va_arr = circle_vector.to_a
					#vector dịch chuyển bằng phép nhân khoảng cách cần dịch chuyển với vector pháp tuyến �?đây đ�?normalize
					heso = get_option('do_sau_chot_cam')
					translation =
						Geom::Vector3d.new [
								heso * va_arr[0],
								heso * va_arr[1],
								heso * va_arr[2]
						                   ]
					transformation = Geom::Transformation.new translation
					center = center.transform(transformation)
					minifixModel = definition(item[:parent]).entities.add_group
					minifixModel.set_attribute 'amtf_dict',
					                           'type',
					                           'chotcam_model'
					minifixModel.set_attribute 'amtf_dict',
					                           'minifixID',
					                           item[:minifixID]
					circle =
						minifixModel.entities.add_circle center,
						                                 circle_vector,
						                                 get_option(
								'mini_fix_big_radius'
						                                 )
					mod_face = minifixModel.entities.add_face(circle)
					mod_face.reverse!
					mod_face.pushpull(heso + get_option('do_dai_cam'))
					minifixModel.material = 'red'
				elsif item[:type] == 'chot_go'
					circle_vector = circle_vector.reverse
					va_arr = circle_vector.to_a
					heso = get_option('do_sau_chot_cam')
					translation =
						Geom::Vector3d.new [
								heso * va_arr[0],
								heso * va_arr[1],
								heso * va_arr[2]
						                   ]
					transformation = Geom::Transformation.new translation
					center = center.transform(transformation)
					minifixModel = definition(item[:parent]).entities.add_group
					minifixModel.set_attribute 'amtf_dict',
					                           'type',
					                           'chotgo_model'
					minifixModel.set_attribute 'amtf_dict',
					                           'minifixID',
					                           item[:minifixID]
					circle =
						minifixModel.entities.add_circle center,
						                                 circle_vector,
						                                 get_option(
								'mini_fix_small_radius'
						                                 )
					mod_face = minifixModel.entities.add_face(circle)
					mod_face.reverse!
					mod_face.pushpull(2 * get_option('do_sau_cam'))
					minifixModel.material = 'red'
				end
			end
			def self.between?(a, b, c, on_point = true)
				return false unless c.on_line?([a, b])
				v1 = c.vector_to(a)
				v2 = c.vector_to(b)
				if on_point
					return true if !v1.valid? || !v2.valid?
				else
					return false if !v1.valid? || !v2.valid?
				end
				!v1.samedirection?(v2)
			end
			def self.point_on_edge?(point, edge)
				a = edge.start.position
				b = edge.end.position
				between?(a, b, point, true)
			end
			def self.intersect_line_edge(line, edge)
				point = Geom.intersect_line_line(line, edge.line)
				return nil if point.nil?
				return point if self.point_on_edge?(point, edge)
				# return nil
			end
			def self.deactivate_all_tool
				tools = Sketchup.active_model.tools #.active_tool
				tool = tools.pop_tool
			end
			def self.get_model_units()
				opts = Sketchup.active_model.options['UnitsOptions']
				unit_format = opts['LengthFormat']
				if unit_format != Length::Decimal
					if unit_format == Length::Fractional ||
							unit_format == Length::Architectural
						'INCHES' # Architectural or Fractional
					elsif unit_format == Length::Engineering
						'FEET' # Engineering
					else
						'DEFAULT'
					end
				else
					# Decimal ( unit_format == 0 )
					case opts['LengthUnit']
					when Length::Inches
						'INCHES'
					when Length::Feet
						'FEET'
					when Length::Centimeter
						'CENTIMETERS'
					when Length::Millimeter
						'MILLIMETERS'
					when Length::Meter
						'METERS'
					else
						'DEFAULT'
					end
				end
			end
			def self.getNormal(point, face)
				plane = face.plane
				projected_point = point.project_to_plane(plane)
				vector = projected_point.vector_to(point)
				normal = vector.normalize!
				return normal
			end
			def self.isOverlap?(aBox, bBox)
				aMaxX = maxbyAxes(aBox)[:x]
				aMinX = minbyAxes(aBox)[:x]
				aMaxY = maxbyAxes(aBox)[:y]
				aMinY = minbyAxes(aBox)[:y]
				aMaxZ = maxbyAxes(aBox)[:z]
				aMinZ = minbyAxes(aBox)[:z]
				bMaxX = maxbyAxes(bBox)[:x]
				bMinX = minbyAxes(bBox)[:x]
				bMaxY = maxbyAxes(bBox)[:y]
				bMinY = minbyAxes(bBox)[:y]
				bMaxZ = maxbyAxes(bBox)[:z]
				bMinZ = minbyAxes(bBox)[:z]
				return(
					(aMinX <= bMaxX && aMaxX >= bMinX) &&
						(aMinY <= bMaxY && aMaxY >= bMinY) &&
						(aMinZ <= bMaxZ && aMaxZ >= bMinZ)
				)
			end
			def self.maxbyAxes(bb)
				tempX = []
				tempY = []
				tempZ = []
				for i in 0..7
					tempX << bb.corner(i).to_a
					tempY << bb.corner(i).to_a
					tempZ << bb.corner(i).to_a
				end
				maxX = deepClonePointArray(tempX).max_by { |obj| obj.x }
				maxY = deepClonePointArray(tempY).max_by { |obj| obj.y }
				maxZ = deepClonePointArray(tempZ).max_by { |obj| obj.z }
				return { x: maxX.x, y: maxY.y, z: maxZ.z }
			end
			def self.minbyAxes(bb)
				tempX = []
				tempY = []
				tempZ = []
				for i in 0..7
					tempX << bb.corner(i).to_a
					tempY << bb.corner(i).to_a
					tempZ << bb.corner(i).to_a
				end
				minX = deepClonePointArray(tempX).min_by { |obj| obj.x }
				minY = deepClonePointArray(tempY).min_by { |obj| obj.y }
				minZ = deepClonePointArray(tempZ).min_by { |obj| obj.z }
				return { x: minX.x, y: minY.y, z: minZ.z }
			end
			def self.bound_points(bound)
				if bound.width == 0
					[0, 2, 6, 4].map { |i| bound.corner(i) }
				elsif bound.height == 0
					[0, 1, 5, 4].map { |i| bound.corner(i) }
				elsif bound.depth == 0
					[0, 1, 3, 2].map { |i| bound.corner(i) }
				else
					(0..7).map { |i| bound.corner(i) }
				end
			end
			def self.cleaning
				# Default code, use or delete...
				mod = Sketchup.active_model # Open model
				ent = mod.entities # All entities in model
				sel = mod.selection # Current selection
				parts = buil_sheet_part_list(ent)
				parts.each do |part|
					definition(part.comp)
						.entities
						.each do |item|
							next if item.is_a?(Sketchup::Edge)
							next if item.is_a?(Sketchup::Face)
							item.erase!
						end
				end
			end
			def self.deepClone(point)
				newPoint = Geom::Point3d.new
				newPoint.x = point.x
				newPoint.y = point.y
				newPoint.z = point.z
				return newPoint
			end
			def self.deepClonePointArray(arr)
				newArray = []
				for i in 0..arr.size - 1
					newArray.push(deepClone(arr[i]))
				end
				return newArray
			end
			# def self.get_edge_banding_type(str)
			#     edge_type_name = 'V'
			#     EdgeOptions.convertEdgeData.each do
			#         |item|
			#         edge_type_name = item['edge_type_name'] if str.to_s == item['edge_type_number'] && item['edge_type_default'] == 'false'
			#     end
			#     return edge_type_name
			# end
			def self.bound_is_2d?(bound)
				bound.width == 0 || bound.height == 0 || bound.depth == 0
			end
			def self.bound_points(bound)
				if bound.width == 0
					[0, 2, 6, 4].map { |i| bound.corner(i) }
				elsif bound.height == 0
					[0, 1, 5, 4].map { |i| bound.corner(i) }
				elsif bound.depth == 0
					[0, 1, 3, 2].map { |i| bound.corner(i) }
				else
					(0..7).map { |i| bound.corner(i) }
				end
			end
			def self.getEdgeCurve(loop)
				#Đi một vòng, gặp curve thì nạp vào mảng
				curveEdges = []
				curve = nil
				loop.edges.each do |e|
					if e.curve.nil?
						curveEdges << e
					else
						if e.curve != curve
							curve = e.curve
							curveEdges << curve
						end
					end
				end
				return curveEdges
			end
			# Normalize radius to be between 0 and 2PI, e.g. -PI/4 becomes 7PI/4 and 5PI becomes PI.
			def self.normalizeRadians(alpha)
				pi_2 = Math::PI * 2
				return alpha if (alpha >= 0 && alpha <= pi_2)
				return alpha - pi_2 * ((alpha / pi_2).floor)
			end
			def self.angleBetween(p0, p1, center)
				v0 = center.vector_to(p0)
				v1 = center.vector_to(p1)
				angle = v0.angle_between(v1)
			end
			def self.getBulge(p0, p1, center)
				angle = normalizeRadians(angleBetween(p0, p1, center))
				angle = -angle if ccw?(center, p0, p1)
				Math.tan(angle / 4)
			end
			def self.ccw?(center, p0, p1)
				v0 = center.vector_to(p0)
				v1 = center.vector_to(p1)
				return v0.cross(v1).z > 0
			end
			def self.vector_ccw?(v0, v1)
				return v0.cross(v1).z > 0
			end
			def self.amtfBoundingBox(grp, grp_trans)
				# tr�?v�?8 đỉnh  của amtfBoundingBox
				big_face = get_big_faces(grp, grp_trans)[0]
				zaxis = big_face.normal.normalize!
				origin = big_face.edges[0].vertices[0].position
				trans = Geom::Transformation.new origin, zaxis
				edges = Utilities.definition(grp).entities.grep(Sketchup::Edge)
				points = []
				for i in 0..edges.size - 1
					points << edges[i].vertices[0].position
					points << edges[i].vertices[1].position
				end
				points =
					points.map do |point|
						point = point_context_convert(point, grp_trans, trans)
					end
				pointX = points.sort_by { |point| point.x }
				maxX = pointX.last.x
				minX = pointX.first.x
				pointY = points.sort_by { |point| point.y }
				maxY = pointY.last.y
				minY = pointY.first.y
				pointZ = points.sort_by { |point| point.z }
				maxZ = pointZ.last.z
				minZ = pointZ.first.z
				ptr0 = Geom::Point3d.new minX, minY, minZ
				ptr1 = Geom::Point3d.new maxX, minY, minZ
				ptr2 = Geom::Point3d.new maxX, maxY, minZ
				ptr3 = Geom::Point3d.new minX, maxY, minZ
				ptr4 = Geom::Point3d.new minX, minY, maxZ
				ptr5 = Geom::Point3d.new maxX, minY, maxZ
				ptr6 = Geom::Point3d.new maxX, maxY, maxZ
				ptr7 = Geom::Point3d.new minX, maxY, maxZ
				corners = [ptr0, ptr1, ptr2, ptr3, ptr4, ptr5, ptr6, ptr7]
				corners =
					corners.map do |point|
						point = point_context_convert(point, trans, grp_trans)
					end
				return corners
			end
			# def self.amtf2DBoundingBox(points)
			#     # tr�?v�?4 đỉnh  của bounding box các điểm points
			#     points = deepClonePointArray(points)
			#    pointX = points.sort_by{|point| point.x}
			#    maxX = pointX.last.x
			#    minX = pointX.first.x
			#    pointY = points.sort_by{|point| point.y}
			#    maxY = pointY.last.y
			#    minY = pointY.first.y
			#    pointZ = points.sort_by{|point| point.z}
			#    maxZ = pointZ.last.z
			#    minZ = pointZ.first.z
			#    ptr0 = Geom::Point3d.new minX, minY, minZ
			#    ptr1 = Geom::Point3d.new maxX, minY, minZ
			#    ptr2 = Geom::Point3d.new maxX, maxY, minZ
			#    ptr3 = Geom::Point3d.new minX, maxY, minZ
			#    ptr4 = Geom::Point3d.new minX, minY, maxZ
			#    ptr5 = Geom::Point3d.new maxX, minY, maxZ
			#    ptr6 = Geom::Point3d.new maxX, maxY, maxZ
			#    ptr7 = Geom::Point3d.new minX, maxY, maxZ
			#    corners = [ptr0, ptr1, ptr2, ptr3, ptr4, ptr5, ptr6, ptr7]
			#     corners = Utilities.uniq_points(corners)
			#    return corners
			# end
			# def self.amtfDimension(entity, trans)
			#     corners = amtfBoundingBox(grp, grp_trans)
			#     width = corners[0].distance(corners[1])
			#     height = corners[0].distance(corners[4])
			#     depth = corners[0].distance(corners[1])
			#     return {:width => width, :height => height, :depth => depth }
			# end
			# def self.isBoundPositive?(grp)
			#     return unless instance?(grp)
			#     # tr�?v�?8 đỉnh  của amtfBoundingBox
			#     big_faces = get_big_faces(grp)
			#     return if big_faces.nil?
			#     return if big_faces.size == 0
			#     big_face = big_faces[0]
			#     zaxis = big_face.normal.normalize!
			#     origin = big_face.edges[0].vertices[0].position
			#     trans = Geom::Transformation.new origin, zaxis
			#     edges = grp.entities.grep(Sketchup::Edge)
			#    points = []
			#    for i in 0..edges.size - 1
			#         points << edges[i].vertices[0].position
			#         points << edges[i].vertices[1].position
			#    end
			#    points = points.map{
			#        |point|
			#        point = point.transform!(trans) #point_context_convert(point, grp.transformation, trans)
			#    }
			#    pointX = points.sort_by{|point| point.x}
			#    maxX = pointX.last.x
			#    minX = pointX.first.x
			#    pointY = points.sort_by{|point| point.y}
			#    maxY = pointY.last.y
			#    minY = pointY.first.y
			#    pointZ = points.sort_by{|point| point.z}
			#    maxZ = pointZ.last.z
			#    minZ = pointZ.first.z
			#    ptr0 = Geom::Point3d.new minX, minY, minZ
			#    ptr1 = Geom::Point3d.new maxX, minY, minZ
			#    ptr2 = Geom::Point3d.new maxX, maxY, minZ
			#    ptr3 = Geom::Point3d.new minX, maxY, minZ
			#    ptr4 = Geom::Point3d.new minX, minY, maxZ
			#    ptr5 = Geom::Point3d.new maxX, minY, maxZ
			#    ptr6 = Geom::Point3d.new maxX, maxY, maxZ
			#    ptr7 = Geom::Point3d.new minX, maxY, maxZ
			#    corners = [ptr0, ptr1, ptr2, ptr3, ptr4, ptr5, ptr6, ptr7]
			#     width = corners[0].distance(corners[1])
			#     height = corners[0].distance(corners[4])
			#     depth = corners[0].distance(corners[1])
			#     return width*height*depth.to_f > 0
			# end
			def self.fj_uniq_name(str)
				return str if str.nil? || str.empty?
				str = str.downcase
				str.strip!
				return str if str.nil?
				return str if str.empty?
				str = str.gsub(/\s\s/, ' ') while str.match(/\s\s/)
				str = str.gsub(/\s/, '-')
			end
			def self.layer_exist(name)
				layers = Sketchup.active_model.layers
				found = false
				layers.each { |la| found = true if la.name.to_s == name }
				found
			end
			def self.point_compare?(p1, p2)
				return (p1.x == p2.x) && (p1.y == p2.y) && (p1.z == p2.z)
			end
			def self.vector_multiply(k, v)
				return unless k && v
				Geom::Vector3d.new k * v[0], k * v[1], k * v[2]
			end
			def self.vector_multiply_2d(k, v)
				return unless k && v
				Geom::Vector2d.new k * v[0], k * v[1]
			end
			#    def self.amtfIntersect(e1, e2, tr1, parent_tr2)
			#         # e1_copy = e1.parent.entities.add_instance(e1.entities.parent, e1.transformation)
			#         # e2_copy = e2.parent.entities.add_instance(e2.entities.parent, e2.transformation)
			#         # e1_copy.make_unique
			#         # e2_copy.make_unique
			#         begin
			#             es1 = e1.entities
			#             es2 = e2.entities
			#             # es1.each do |ent|
			#             #     ent.erase! if ent.is_a?(Sketchup::Group)
			#             # end
			#             # es2.each do |ent|
			#             #     ent.erase! if ent.is_a?(Sketchup::Group)
			#             # end
			#             ens = Sketchup.active_model.entities
			#             intersect_grp = ens.add_group()
			#             ges = intersect_grp.entities
			#             invt2 = parent_tr2.inverse
			#             gt = invt2 * intersect_grp.transformation
			#             et = invt2 * tr1
			#             es1.intersect_with(true, et, ges, gt, true, e2)
			#             result = false
			#             result = true if ges.size > 0
			#             # e1_copy.erase! if e1_copy.valid?
			#             # e2_copy.erase! if e2_copy.valid?
			#             intersect_grp.erase!
			#             return result
			#         rescue => error
			#             puts error
			#             # e1_copy.erase! if e1_copy.valid?
			#             # e2_copy.erase! if e2_copy.valid?
			#         end
			#    end
			def self.get_comp_trans(part_list, comp)
				trans = nil
				for i in 0..part_list.size - 1
					trans = part_list[i].transformation if comp ===
						part_list[i].comp
				end
				return trans
			end
			def self.hasParent?(entity)
				group = entity.parent.instances[-1] if entity &&
					entity.parent.is_a?(Sketchup::ComponentDefinition)
				return !group.nil?
			end
			def self.within_polygon?(p1, edges)
				return false unless p1 && edges && edges.size > 0
				# Kiểm tra một điểm nằm bên trong polygon tạo bới các edges
				test_point = edges[0].vertices[0].position
				test_vector = p1.vector_to(test_point)
				test_line = [p1, test_vector]
				intersect = []
				for i in 0..edges.size - 1
					point = Utilities.intersect_line_edge(test_line, edges[i])
					unless point.nil?
						vector1 = p1.vector_to(point)
						intersect << point if vector1.dot(test_vector) < 0
					end
				end
				intersect = Utilities.uniq_points(intersect)
				return intersect.size.odd?
			end
			def self.get_sheet_vertext(comp)
				faces = comp.entities.grep(Sketchup::Face)
				loops = []
				for i in 0..faces.size - 1
					loops.push(*faces[i].loops)
				end
				vertices = []
				for i in 0..loops.size - 1
					vts = loops[i].vertices
					for j in 0..vts.size - 1
						vertex = vts[j]
						vertices << vertex if vertex.faces.size === 3
					end
				end
				points = []
				for i in 0..vertices.size - 1
					points << {
						point: vertices[i].position,
						vertex: vertices[i]
					}
				end
				result = []
				for i in 0..points.size - 1
					pt =
						Utilities.point_context_convert(
							points[i][:point],
							comp.transformation,
							IDENTITY
						)
					result << {
						point: pt,
						comp: comp,
						vertex: points[i][:vertex]
					}
				end
				result
			end
			def self.get_minifix_vertext(ent, parent)
				edges = ent.entities.grep(Sketchup::Edge)
				points = []
				for i in 0..edges.size - 1
					points << edges[i].start.position
					points << edges[i].end.position
				end
				points = Utilities.uniq_points(points)
				result = []
				for i in 0..points.size - 1
					pt =
						Utilities.point_context_convert(
							points[i],
							parent.transformation * ent.transformation,
							IDENTITY
						)
					result << { point: pt, comp: ent }
				end
				result
			end
			def self.vectorMinus(v1, v2)
				v3 = Geom::Vector3d.new(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z)
			end
			def self.vectorPlus(v1, v2)
				v3 = Geom::Vector3d.new(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z)
			end
			def self.vectorMultiplyvector(v0, v1)
				v3 = Geom::Vector3d.new(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z)
			end
			def self.vectorMultiply(v, t)
				v3 = Geom::Vector3d.new(t * v.x, t * v.y, t * v.z)
			end
			def self.unitPerp(v)
				v3 = Geom::Vector2d.new(-v.y, v.x)
				v3.normalize!
			end
			def self.findSectionByID(sec_arr, id, selected_sections)
				sec_arr.each do |sec|
					if sec['root']['id'] === id &&
							sec['root']['type'] == 'section'
						selected_sections[:found] = sec
						return
					else
						findSectionByID(
							sec['root']['sections'],
							id,
							selected_sections
						)
					end
				end
			end
			def self.findSheetByID(root, id, found_sheet)
				if root['sheets'].key?(id)
					found_sheet[:found] = {
						sheet: root['sheets'][id],
						parent: root
					}
					return
				else
					root['sections'].each do |sec|
						findSheetByID(sec['root'], id, found_sheet)
					end
				end
			end
			def self.export_json()
				model = Sketchup.active_model
				data = {}
				model.entities.each do |ent|
					if ent.get_attribute('amtf_dict', 'comp_type') ==
							'AmtfFrame'
						if AMTF_STORE.frame_premitives[ent.persistent_id]
							data[ent.persistent_id] =
								AMTF_STORE.frame_premitives[ent.persistent_id]
									.root
						end
					end
				end
				model_path = Sketchup.active_model.path
				file_full_name = ''
				if model_path.empty? || Sketchup.active_model.modified?
					UI.messagebox(OB[:File_not_saved], MB_OK)
					return nil
				else
					name = Sketchup.active_model.title
					path = File.dirname(model_path)
					file_full_name = slashify(File.join(path, "#{name}.amtf"))
				end
				return if file_full_name == ''
				File.open(file_full_name, 'w') do |f|
					f.write(Base64.encode64(JSON.generate(data)))
					f.close
				end
			end
			def self.slashify(path)
				path.gsub(/\\\\|\\/, '/')
			end
			def self.import_json()
				model = Sketchup.active_model
				model_path = Sketchup.active_model.path
				name = Sketchup.active_model.title
				path = File.dirname(model_path)
				file_full_name = UI.openpanel('Open File', path, '*.amtf')
				if (file_full_name)
					if (File.exists?(file_full_name))
						file_data = ''
						open(file_full_name, 'r') do |f|
							file_data = f.read
							f.close
						end
						data_hash = JSON.parse(Base64.decode64(file_data))
						model.entities.each do |ent|
							if ent.get_attribute('amtf_dict', 'comp_type') ==
									'AmtfFrame'
								if data_hash.key?(ent.persistent_id.to_s)
									model_data =
										data_hash[ent.persistent_id.to_s]
									frm = AmtfFrame.new(model_data, ent)
								end
							end
						end
					end
				end
			end
			def self.getParentGroup(entity)
				group = entity.parent.instances[-1] if entity &&
					entity.valid? &&
					entity.parent.is_a?(Sketchup::ComponentDefinition)
				return group
			end
			def self.getTransPath(entity)
				path = [entity]
				parent = getParentGroup(entity)
				while !parent.nil? && parent.is_a?(Sketchup::Group)
					path.push(parent)
					parent = getParentGroup(parent)
				end
				return path
			end
			##################################FROM VERSION 3.0#########################################
			def self.correct_scale(comp)
				#   matrix = comp.transformation.to_a
				#   if matrix[0] !=1 || matrix[5] != 1 || matrix[10] != 1 || matrix[15] != 1
				#     if comp && comp.valid?
				#       new_group = nil
				#       parent = comp.parent
				#       new_group = parent.entities.add_group
				#     #   org = comp.transformation.origin
				#     #   z_axis = comp.transformation.zaxis
				#     #   tr = Geom::Transformation.new(org, z_axis)
				#     #   new_group.transform!(tr)
				#       inst = Utilities.definition(new_group).entities.add_instance(Utilities.definition(comp), comp.transformation )
				#       inst.explode
				#       new_group.name = comp.name
				#       copy_attribute(comp, new_group)
				#       comp.erase!
				#       return new_group
				#     end
				#   end
				return comp
			end
			def self.inside_direction(point, comp)
				big_faces = Utilities.get_big_faces(comp)
				if Utilities.within_face?(point, big_faces[0])
					return big_faces[1].normal
				elsif Utilities.within_face?(point, big_faces[1])
					return big_faces[0].normal
				end
				nil
			end
			def self.get_edge_faces(comp)
				faces = Utilities.definition(comp).entities.grep(Sketchup::Face)
				return unless faces && faces.size > 0
				faces.compact!
				out = faces.sort_by { |obj| obj.area }
				result = out.slice(0, out.size - 2)
				return result
			end

			def self.解读编号(名称)
				编号=""
				编号match=/__(\d+)./.match(名称)
				if 编号match
				  编号=编号match[1]
				end
				return 编号
			end

			def self.point_context_convert(point, tr1, tr2)
				return unless !point.nil? && !tr1.nil? && !tr2.nil?
				#Chuyển bối cảnh của point có trong đối tượng có transformation tr1
				#sang bối cảnh nằm trong đối tượng có transformation tr2
				return point.transform(tr2.inverse * tr1)
			end
			# 这是一个Ruby类的静态方法（类方法），它用于将一个向量从一个坐标系转换到另一个坐标系。它接受三个参数：原始向量、原始坐标系的变换矩阵、目标坐标系的变换矩阵。
			# 为了实现这个转换，我们需要将向量从原始坐标系转换到原点（使用`tr1`变换矩阵），然后再从原点转换到目标坐标系（使用`tr2.inverse`变换矩阵的逆矩阵）。最后，我们将转换后的向量返回。
			# 需要注意的是，这个方法并没有修改原始向量，而是返回了一个新的向量。
			def self.vector_context_convert(vector, tr1, tr2)
				#Chuyển bối cảnh của point có trong đối tượng có transformation tr1
				#sang bối cảnh nằm trong đối tượng có transformation tr2
				# 将具有变换tr1的对象中的点的背景转换为具有变换tr2的对象中的背景。
				return vector.transform(tr2.inverse * tr1) if vector
			end
			def self.transform_context_convert(tr1, tr2)
				#Chuyển bối cảnh của đối tượng có transformation tr1
				#sang bối cảnh nằm trong đối tượng có transformation tr2
				tr2.inverse * tr1
			end
			def self.get_big_faces(comp, trans)
				return if !instance?(comp)
				faces = Utilities.definition(comp).entities.grep(Sketchup::Face)
				return unless faces && faces.size > 0
				faces = faces.sort_by { |f| AmtfMath.face_area(f, trans) }
				bigFace = [faces[faces.size - 1], faces[faces.size - 2]]
			end #big_faces
			def self.find_big_faces(tre, tr1, tr2, egde_array, e1, e2)
				# Tìm ra mặt lớn của part mà có chứa ít nhất 1 điểm giữa edge trong edge_aray
				# S�?dụng khi đã biết part khi intersect, xác định xem part nào chứa mặt lớn
				#Kiểm tra luôn trường hợp c�?e1 và e2 có tất c�?các đỉnh thuộc cũng một mặt lớn
				big_faces = get_big_faces(e1)
				arr = []
				egde_array.each do |e|
					pts0 = e.vertices[0].position
					pts1 = e.vertices[1].position
					mid_point =
						point_context_convert(
							get_midpoint(pts0, pts1),
							tre,
							tr1
						)
					if within_face?(mid_point, big_faces[0], false)
						arr << { bigface: big_faces[0], edge: e }
					end
					if within_face?(mid_point, big_faces[1], false)
						arr << { bigface: big_faces[1], edge: e }
					end
				end
				side1 = []
				side0 = []
				big_face2 = get_big_faces(e2)
				#Kiểm tra xem edge thuộc mặt nào của e2, đưa vào side1 hay là side2
				arr.each do |obj|
					if edge_on_face?(tre, tr2, obj[:edge], big_face2[0])
						side0 << obj
					end
					if edge_on_face?(tre, tr2, obj[:edge], big_face2[1])
						side1 << obj
					end
				end
				# Nếu như e2 cũng có mặt chứa tất c�?các đỉnh thì là trường hợp 2 mặt lớn tiếp xúc
				# �?đây hơi loằng ngoằng, c�?cái th�?tục này, check với trường hợp gi�?định e1 chứa mặt lớn, cho nên nếu e2 cũng là mặt lớn thì s�?cho tr�?v�?mảng rỗng
				mang0 = []
				mang1 = []
				for i in 0..egde_array.size - 1
					e = egde_array[i]
					pts0 =
						point_context_convert(e.vertices[0].position, tre, tr2)
					pts1 =
						point_context_convert(e.vertices[1].position, tre, tr2)
					if within_face?(pts0, big_face2[0])
						mang0 << pts0
					elsif within_face?(pts0, big_face2[1])
						mang1 << pts0
					end
					if within_face?(pts1, big_face2[0])
						mang0 << pts1
					elsif within_face?(pts1, big_face2[1])
						mang1 << pts1
					end
				end
				if mang0.size == 2 * egde_array.size ||
						mang1.size == 2 * egde_array.size
					return []
				end
				return side0 if side0.size > 0
				return side1 if side1.size > 0
				return []
			end
			# 重心(centroid)
			def self.find_centroid(group)
				return unless group
				edges = definition(group).entities.grep(Sketchup::Edge)
				return unless edges.size > 0
				vertices = []
				for i in 0..edges.length - 1
					vertices << edges[i].vertices[0]
					vertices << edges[i].vertices[1]
				end
				vertices.uniq!
				# 为了实现闭合循环(loop)，将数组(vertices)的最后一个顶点(vertices)设置为数组(vertices)的第一个顶点(vertices)。
				vertices[vertices.length] = vertices[0] ### =loop
				return vertices[0].position if vertices.length == 1
				sum_x = 0
				sum_y = 0
				sum_z = 0
				for i in (0...vertices.length - 1)
					sum_x += vertices[i].position.x
					sum_y += vertices[i].position.y
					sum_z += vertices[i].position.z
				end ###for
				x = sum_x / (vertices.length - 1)
				y = sum_y / (vertices.length - 1)
				z = sum_z / (vertices.length - 1)
				centroid = Geom::Point3d.new x, y, z
				return centroid
			end

			# 计算三合一孔位置 cam==凸轮
			def self.get_cam_hole_locations(p1, p2, config_data, enter_qty = 0)
				distance_l = config_data['distance_l'].to_s.to_l #凸轮孔距离边缘
				distance_l1 = config_data['distance_l1'].to_s.to_l #螺杆距离边缘
				distance_l2 = config_data['distance_l2'].to_s.to_l
				distance_l3 = config_data['distance_l3'].to_s.to_l
				# 阵列 = [
				# 	{ 总长: 200.mm, 数量: 1 },
				# 	{ 总长: 400.mm, 数量: 2 },
				# 	{ 总长: 1000.mm, 数量: 3 }
				# ]

				# if config_data['阵列'] &&config_data['阵列'].size > 0
				# 	阵列 = []
				# 	config_data['阵列'].each do |obj|
				# 		阵列 << {
				# 			总长: obj['总长'].to_s.to_l,
				# 			数量: obj['数量'].to_i
				# 		}
				# 	end
				# end
				return unless p1 && p2
				len = p1.distance(p2)
				候选阵列 = config_data['候选阵列']
				#先计算数量↓
				# if enter_qty == 0 
				# 	if len <= 阵列.first[:总长]
				# 		enter_qty = 阵列.first[:数量]
				# 	elsif len > 阵列.last[:总长]
				# 		enter_qty = 阵列.last[:数量]
				# 	else
				# 		for i in 1..阵列.size - 1
				# 			if len <= 阵列[i][:总长]
				# 				enter_qty = 阵列[i][:数量]
				# 				break
				# 			end
				# 		end
				# 	end
				# end
				if enter_qty == 0 
					区间序号=候选阵列.size - 1 #初定为最大区间
					# puts "len 👉 #{len}" 
					for i in 0..区间序号
						总长=候选阵列[i]["总长"].mm
						# 总长=Sketchup.parse_length(总长)
						# puts "总长 👉 #{总长}" 
						# puts "len.class 👉 #{len.class}" 
						# puts "总长.class 👉 #{总长.class}" 
						if len <= 总长
							区间序号=i
							break
						end
					end
					# puts "区间序号 👉 #{区间序号}" 
					# for i in 0...5
					# 	puts i
					# end
                    # 阵列 = Hash[]
					# puts "候选阵列[区间序号] 👉 #{候选阵列[区间序号]}" 
					# 候选阵列[区间序号].each{|k,v|
					# 	阵列[k]=v
					# }
					enter_qty = 候选阵列[区间序号]["数量"]
					端距 = 候选阵列[区间序号]["端距"].mm
					# puts "	端距 👉 #{端距}" 
					# puts "	端距 👉 #{端距.class}" 
				end
				# amtf.nil
				#先计算数量↑
				# 再计算位置↓
				tpoint = []
				if enter_qty == 1
					chot_cam = get_midpoint(p1, p2)
					chot_go2 =find_point_between_two_points(chot_cam, p2, distance_l3)
					chot_go1 =find_point_between_two_points(chot_cam, p1, distance_l2) #木销位置
					tpoint << {
						chot_cam: chot_cam,
						chot_go1: chot_go1,
						chot_go2: chot_go2
					}
				else #数量==2 或者更多↓
					point_1 =Utilities.find_point_between_two_points(p1,p2,端距)
					point_2 =Utilities.find_point_between_two_points(p2,p1,端距)
					tpoint[0] = {
						chot_cam: point_1,
						chot_go2:find_point_between_two_points(point_1,p2,distance_l3),
						chot_go1:find_point_between_two_points(point_1,p1,distance_l2),
					}
					tpoint[enter_qty - 1] = { #末端点
						chot_cam: point_2,
						chot_go2:find_point_between_two_points(point_2,p1,distance_l3),
						chot_go1:find_point_between_two_points(point_2,p2,distance_l2),
					}
					if enter_qty == 2
						# 等于2，什么也不做
					else #大于2，计算中间的点
						range = point_1.distance(point_2) / (enter_qty - 1)
						for i in 1..enter_qty - 2
							tpi =Utilities.find_point_between_two_points(tpoint[i - 1][:chot_cam],p2,range)
							chot_go2_i =find_point_between_two_points(tpi, p2, distance_l3)
							chot_go1_i =find_point_between_two_points(tpi, p1, distance_l2)
							tpoint[i] = {chot_cam: tpi,chot_go1: chot_go1_i,chot_go2: chot_go2_i
							}
						end
					end
				end#数量==2 或者更多↑
				# puts "tpoint 👉 #{tpoint}"
				return tpoint
				nil
			end

			def self.get_dogbone_hole_locations(
				p1,
				p2,
				config_data,
				enter_qty = 0
			)
				distance_l =
					config_data['distance_l'].to_s.to_l +
						0.5 * config_data['distance_l1'].to_s.to_l
				distance_l3 =
					0.5 * config_data['distance_l1'].to_s.to_l +
						config_data['distance_l3'].to_s.to_l
				distance_range = [
					{ distance: 200.mm, quantity: 1 },
					{ distance: 400.mm, quantity: 2 },
					{ distance: 1000.mm, quantity: 3 }
				]
				if config_data['distance_range'] &&
						config_data['distance_range'].size > 0
					distance_range = []
					config_data['distance_range'].each do |obj|
						distance_range << {
							distance: obj['distance'].to_s.to_l,
							quantity: obj['quantity'].to_i
						}
					end
				end
				return unless p1 && p2
				len = p1.distance(p2)
				if enter_qty == 0
					if len <= distance_range.first[:distance]
						enter_qty = distance_range.first[:quantity]
					elsif len > distance_range.last[:distance]
						enter_qty = distance_range.last[:quantity]
					else
						for i in 1..distance_range.size - 1
							if len <= distance_range[i][:distance]
								enter_qty = distance_range[i][:quantity]
								break
							end
						end
					end
				end
				if enter_qty == 1
					out_array = []
					chot_cam = get_midpoint(p1, p2)
					chot_go2 =
						find_point_between_two_points(chot_cam, p2, distance_l3)
					out_array << { chot_cam: chot_cam, chot_go2: chot_go2 }
					return out_array
				elsif enter_qty == 2
					point_1 =
						Utilities.find_point_between_two_points(
							p1,
							p2,
							distance_l
						)
					point_2 =
						Utilities.find_point_between_two_points(
							p2,
							p1,
							distance_l
						)
					tpoint = []
					tpoint[0] = {
						chot_cam: point_1,
						chot_go2:
							find_point_between_two_points(
								point_1,
								p2,
								distance_l3
							)
					}
					tpoint[1] = {
						chot_cam: point_2,
						chot_go2:
							find_point_between_two_points(
								point_2,
								p1,
								distance_l3
							)
					}
					return tpoint
				else
					point_1 =
						Utilities.find_point_between_two_points(
							p1,
							p2,
							distance_l
						)
					point_2 =
						Utilities.find_point_between_two_points(
							p2,
							p1,
							distance_l
						)
					tpoint = []
					range = point_1.distance(point_2) / (enter_qty - 1)
					tpoint[0] = {
						chot_cam: point_1,
						chot_go2:
							find_point_between_two_points(
								point_1,
								p2,
								distance_l3
							)
					}
					tpoint[enter_qty - 1] = {
						chot_cam: point_2,
						chot_go2:
							find_point_between_two_points(
								point_2,
								p1,
								distance_l3
							)
					}
					for i in 1..enter_qty - 2
						tpi =
							Utilities.find_point_between_two_points(
								tpoint[i - 1][:chot_cam],
								p2,
								range
							)
						chot_go2_i =
							find_point_between_two_points(tpi, p2, distance_l3)
						tpoint[i] = { chot_cam: tpi, chot_go2: chot_go2_i }
					end
					return tpoint
				end
				nil
			end
			def self.get_hole_locations(p1, p2, config_data, enter_qty = 0)
				distance_l = config_data['distance_l'].to_s.to_l
				distance_range = [
					{ distance: 200.mm, quantity: 1 },
					{ distance: 400.mm, quantity: 2 },
					{ distance: 1000.mm, quantity: 3 }
				]
				if config_data['distance_range'] &&
						config_data['distance_range'].size > 0
					distance_range = []
					config_data['distance_range'].each do |obj|
						distance_range << {
							distance: obj['distance'].to_s.to_l,
							quantity: obj['quantity'].to_i
						}
					end
				end
				return unless p1 && p2
				len = p1.distance(p2)
				if enter_qty == 0
					if len <= distance_range.first[:distance]
						enter_qty = distance_range.first[:quantity]
					elsif len > distance_range.last[:distance]
						enter_qty = distance_range.last[:quantity]
					else
						for i in 1..distance_range.size - 1
							if len <= distance_range[i][:distance]
								enter_qty = distance_range[i][:quantity]
								break
							end
						end
					end
				end
				if enter_qty == 1
					out_array = []
					chot_cam = get_midpoint(p1, p2)
					out_array << { chot_cam: chot_cam }
					return out_array
				elsif enter_qty == 2
					point_1 =
						Utilities.find_point_between_two_points(
							p1,
							p2,
							distance_l
						)
					point_2 =
						Utilities.find_point_between_two_points(
							p2,
							p1,
							distance_l
						)
					tpoint = []
					tpoint[0] = { chot_cam: point_1 }
					tpoint[1] = { chot_cam: point_2 }
					return tpoint
				else
					point_1 =
						Utilities.find_point_between_two_points(
							p1,
							p2,
							distance_l
						)
					point_2 =
						Utilities.find_point_between_two_points(
							p2,
							p1,
							distance_l
						)
					tpoint = []
					range = point_1.distance(point_2) / (enter_qty - 1)
					tpoint[0] = { chot_cam: point_1 }
					tpoint[enter_qty - 1] = { chot_cam: point_2 }
					for i in 1..enter_qty - 2
						tpi =
							Utilities.find_point_between_two_points(
								tpoint[i - 1][:chot_cam],
								p2,
								range
							)
						tpoint[i] = { chot_cam: tpi }
					end
					return tpoint
				end
				nil
			end
			def self.get_lockdowel_locations(locParams)
				l1 = locParams[:distance_l1].to_s.to_l
				l2 = locParams[:distance_l2].to_s.to_l
				lockdowel_L = locParams[:distance_l].to_s.to_l
				p1 = locParams[:p1]
				p2 = locParams[:p2]
				enter_qty = locParams[:enter_qty]
				distance_range = [
					{ distance: 200.mm, quantity: 1 },
					{ distance: 400.mm, quantity: 2 },
					{ distance: 1000.mm, quantity: 3 }
				]
				if locParams[:distance_range] &&
						locParams[:distance_range].size > 0
					distance_range = []
					locParams[:distance_range].each do |obj|
						distance_range << {
							distance: obj['distance'].to_s.to_l,
							quantity: obj['quantity'].to_i
						}
					end
				end
				return unless p1 && p2
				len = p1.distance(p2)
				if enter_qty == 0
					if len <= distance_range.first[:distance]
						enter_qty = distance_range.first[:quantity]
					elsif len > distance_range.last[:distance]
						enter_qty = distance_range.last[:quantity]
					else
						for i in 1..distance_range.size - 1
							if len <= distance_range[i][:distance]
								enter_qty = distance_range[i][:quantity]
								break
							end
						end
					end
				end
				point_1 =
					Utilities.find_point_between_two_points(p1, p2, lockdowel_L)
				point_2 =
					Utilities.find_point_between_two_points(
						p2,
						p1,
						lockdowel_L - l2 + l1
					)
				distance = point_1.distance(point_2)
				vector_1 = p1.vector_to(point_1)
				vector_2 = point_1.vector_to(point_2)
				uVector = vector_1.normalize
				if enter_qty == 1
					c3 = Utilities.get_midpoint(point_1, point_2)
					v1 = Utilities.vector_multiply(l1, uVector)
					v2 =
						Utilities.vector_multiply(
							(l2.to_l - l1.to_l),
							uVector.reverse
						)
					tr1 = Geom::Transformation.new v1
					tr2 = Geom::Transformation.new v2
					c1 = c3.transform(tr1)
					c2 = c3.transform(tr2)
					return [{ center1: c1, center2: c2, center3: c3 }]
				else
					center1 = point_1
					tr1 =
						Geom::Transformation.new Utilities.vector_multiply(
								l1.to_l,
								uVector
						                         )
					tr2 =
						Geom::Transformation.new Utilities.vector_multiply(
								l2.to_l,
								uVector
						                         )
					center2 = center1.transform(tr2)
					center3 = center1.transform(tr1)
					tp = [
						{ center1: center1, center2: center2, center3: center3 }
					]
					for i in 1..enter_qty - 1
						c1 =
							Utilities.find_point_between_two_points(
								tp[i - 1][:center1],
								point_2,
								(distance - l2.to_l) / (enter_qty - 1)
							)
						c2 = c1.transform(tr2)
						c3 = c1.transform(tr1)
						tp << { center1: c1, center2: c2, center3: c3 }
					end
					return tp
				end
				nil
			end

			# 该代码定义了一个函数 find_point_between_two_points，它接收三个参数 p0, p1, d。函数的目标是找到两个给定点 p0 和 p1 之间的某一点，在这两个点的连线上，距离起始点 p0 为 d 的位置。
			# 在函数的主体中，首先计算了 p1 和 p0 在 x、y 和 z 方向上的差值，即 a = p1.x - p0.x，b = p1.y - p0.y，c = p1.z - p0.z。
			# 然后，根据这些差值以及给定的距离 d，计算了两个点的坐标(x2, y2, z2)和(x3, y3, z3)：
			def self.find_point_between_two_points(p0, p1, d)
				a = p1.x - p0.x
				b = p1.y - p0.y
				c = p1.z - p0.z
				x0 = p0.x
				y0 = p0.y
				z0 = p0.z
				x2 = x0 - a * d / Math.sqrt(a**2 + b**2 + c**2)
				y2 = y0 - b * d / Math.sqrt(a**2 + b**2 + c**2)
				z2 = z0 - c * d / Math.sqrt(a**2 + b**2 + c**2)
				x3 = x0 + a * d / Math.sqrt(a**2 + b**2 + c**2)
				y3 = y0 + b * d / Math.sqrt(a**2 + b**2 + c**2)
				z3 = z0 + c * d / Math.sqrt(a**2 + b**2 + c**2)
				point1 = Geom::Point3d.new(x2, y2, z2)
				point2 = Geom::Point3d.new(x3, y3, z3)
				
				if point_within_ab(point1, p0, p1)
					return point1
				else
					return point2
				end
			end

			# 通过计算向量 vector_pa 和 vector_pb 的点积来判断点 p 是否在直线段 a 和 b 之间。
			# 如果点积小于 0，则表示点 p 在直线段之间，函数返回 true，否则返回 false：
			def self.point_within_ab(p, a, b)
				vector_pa = Geom::Vector3d.new(p.x - a.x, p.y - a.y, p.z - a.z)
				vector_pb = Geom::Vector3d.new(p.x - b.x, p.y - b.y, p.z - b.z)
				return vector_pa.dot(vector_pb) < 0
			end

			def self.is_back_comp?(tre, tr, egde_array, e)
				# Xác định xem e có chứa trung điểm của edge trên c�?2 mặt không, nếu có thì nó là back
				big_faces = get_big_faces(e, tr)
				on_big_face0 = false
				on_big_face1 = false
				egde_array.each do |e|
					pts0 = e.vertices[0].position
					pts1 = e.vertices[1].position
					mid_point =
						point_context_convert(get_midpoint(pts0, pts1), tre, tr)
					on_big_face0 = true if within_face?(mid_point, big_faces[0])
					on_big_face1 = true if within_face?(mid_point, big_faces[1])
				end
				return on_big_face0 && on_big_face1
			end
			def self.copy_attribute(entity1, entity2)
				return unless entity1.valid? && entity2.valid?
				attrdicts = entity1.attribute_dictionaries
				attrdicts.each do |dict|
					if dict
						dict.each do |key, value|
							entity2.set_attribute dict.name, key, value
						end
					end
				end
			end
			def self.get_groove_depth(bigface,faceTrans,edgeComp,edgeComTrans)
				edgeFaces = Utilities.get_edge_faces(edgeComp)
				edgeCenter = []
				edgeFaces.each do |f|
					edgeCenter <<
						Utilities.point_context_convert(
							Utilities.find_center_face(f),
							edgeComTrans,
							faceTrans
						)
				end
				edgeCenter =
					edgeCenter.map do |p|
						center =
							Utilities.point_context_convert(
								p,
								faceTrans,
								IDENTITY
							)
						project =
							Utilities.point_context_convert(
								p.project_to_plane(bigface.plane),
								faceTrans,
								IDENTITY
							)
						center.distance(project)
					end
				edgeCenter.sort!
				return edgeCenter[0]
			end
			def self.create_boundingbox(path)
				box = Hash.new
				box[:minX] = Float::INFINITY
				box[:minY] = Float::INFINITY
				box[:maxX] = -Float::INFINITY
				box[:maxY] = -Float::INFINITY
				for i in 0..path.size - 1
					x = path[i].x
					y = path[i].y
					box[:minX] = [box[:minX], x].min
					box[:minY] = [box[:minY], y].min
					box[:maxX] = [box[:maxX], x].max
					box[:maxY] = [box[:maxY], y].max
				end
				if path.size == 0
					box[:minX] = 0
					box[:minY] = 0
					box[:maxX] = 0
					box[:maxY] = 0
				end
				return box
			end
			def self.points_of_longe_edges(entities,faceComponent,trans,groove_type_extend,
					groove_type_extend_width
				)
				# Cho mục đích lảm rãnh hậu
				return unless entities
				groove_type_extend = 0 if !groove_type_extend
				edges = entities.grep(Sketchup::Edge)
				return unless edges
				big_faces = get_big_faces(faceComponent, trans)
				edges_0 = []
				edges_1 = []
				edges.each do |edg|
					test_face0 =
						edge_on_face?(IDENTITY, trans, edg, big_faces[0])
					test_face1 =
						edge_on_face?(IDENTITY, trans, edg, big_faces[1])
					edges_0.push(edg) if test_face0 == true
					edges_1.push(edg) if test_face1 == true
				end
				# Kiểm tra xong mặt nào chứa
				return if edges_0.size > 0 && edges_1.size > 0
				return if edges_0.size == 0 && edges_1.size == 0
				if edges_0.size > 0
					selected_face = big_faces[0]
					edges = edges_0
				elsif edges_1.size > 0
					selected_face = big_faces[1]
					edges = edges_1
				end
				# Lấy một cạnh dài nhất làm trục Y
				edges =
					edges.sort do |a, b|
						b.start.position.distance(b.end.position) <=>
							a.start.position.distance(a.end.position)
					end
				yaxis =
					edges
						.last
						.start
						.position
						.vector_to(edges.last.end.position)
						.normalize
				vertices = []
				# chuyển v�?mặt đ�?lấy bound
				zaxis =
					point_context_convert(selected_face.normal, trans, IDENTITY)
						.normalize
				xaxis = yaxis.cross(zaxis).normalize
				origin =
					point_context_convert(
						point_at_face(selected_face),
						trans,
						IDENTITY
					)
				tr = Geom::Transformation.new(xaxis, yaxis, zaxis, origin)
				plane =
					AmtfMath.plane_context_convert(
						selected_face.plane,
						trans,
						IDENTITY
					)
				edges.each do |edg|
					p0 = edg.start.position
					p1 = edg.end.position
					p0 = p0.project_to_plane(plane)
					p1 = p1.project_to_plane(plane)
					vertices.push(point_context_convert(p0, IDENTITY, tr))
					vertices.push(point_context_convert(p1, IDENTITY, tr))
				end
				box = create_boundingbox(vertices)
				return unless box
				d1 = box[:maxX] - box[:minX]
				d2 = box[:maxY] - box[:minY]
				p10 = Geom::Point3d.new(box[:minX], box[:minY], 0)
				p11 = Geom::Point3d.new(box[:minX], box[:maxY], 0)
				p20 = Geom::Point3d.new(box[:maxX], box[:minY], 0)
				p21 = Geom::Point3d.new(box[:maxX], box[:maxY], 0)
				if d1 > d2
					p10 = Geom::Point3d.new(box[:minX], box[:minY], 0)
					p11 = Geom::Point3d.new(box[:maxX], box[:minY], 0)
					p20 = Geom::Point3d.new(box[:minX], box[:maxY], 0)
					p21 = Geom::Point3d.new(box[:maxX], box[:maxY], 0)
				end
				p10 = point_context_convert(p10, tr, trans)
				p11 = point_context_convert(p11, tr, trans)
				p20 = point_context_convert(p20, tr, trans)
				p21 = point_context_convert(p21, tr, trans)
				vector_1 = p10.vector_to(p11).normalize
				vector_2 = p20.vector_to(p21).normalize
				tr101 =
					Geom::Transformation.new(
						vector_multiply(groove_type_extend.to_s.to_l, vector_1)
					)
				tr110 =
					Geom::Transformation.new(
						vector_multiply(
							groove_type_extend.to_s.to_l,
							vector_1.reverse
						)
					)
				p10.transform!(tr110)
				p11.transform!(tr101)
				tr201 =
					Geom::Transformation.new(
						vector_multiply(groove_type_extend.to_s.to_l, vector_2)
					)
				tr210 =
					Geom::Transformation.new(
						vector_multiply(
							groove_type_extend.to_s.to_l,
							vector_2.reverse
						)
					)
				p20.transform!(tr210)
				p21.transform!(tr201)
				ext_p = p20.project_to_line([p10, vector_1])
				ext_v2to1 =
					vector_multiply(
						groove_type_extend_width.to_s.to_l,
						p20.vector_to(ext_p).normalize
					)
				ext_tr1 = Geom::Transformation.new(ext_v2to1)
				ext_tr2 = Geom::Transformation.new(ext_v2to1.reverse)
				p10.transform!(ext_tr1)
				p11.transform!(ext_tr1)
				p20.transform!(ext_tr2)
				p21.transform!(ext_tr2)
				output = []
				if vector_1.dot(vector_2) < 0
					output = [p10, p11, p20, p21]
				elsif vector_1.dot(vector_2) == 0
					output = []
				else
					output = [p10, p11, p21, p20]
				end
				output =
					output.map! do |p|
						Utilities.point_context_convert(p, trans, IDENTITY)
					end
				return { points: output, selected_face: selected_face }
			end
			def self.get_setting(type, config, conID)
				data = {}
				return data unless config[type]
				config[type].each do |obj|
					if obj['id'] == conID
						data = obj
						break
					end
				end
				return {} unless !data.empty?
				hash = {}
				data['params1'].each do |item|
					hash[item['key']] = item['value']
				end
				data['params2'].each do |item|
					hash[item['key']] = item['value']
				end
				if data.key?('tool_database')
					hash['tool_profile_id'] =data['tool_database']['tool_profile_id']
					hash['alignX_id'] = data['tool_database']['alignX_id']
					hash['alignY_id'] = data['tool_database']['alignY_id']
				end
				return hash
			end
			def self.edge_on_face?(tre, trf, edge, face)
				# Kiểm tra xem edge có nằm trong face không
				pts0 = edge.vertices[0].position
				pts1 = edge.vertices[1].position
				p0 = point_context_convert(pts0, tre, trf)
				p1 = point_context_convert(pts1, tre, trf)
				return true if within_face?(p0, face) && within_face?(p1, face)
				false
			end
			# Counter-clockwise angle from vector2 to vector1, as seen from normal.
			def self.planar_angle(vector1, vector2, normal = Z_AXIS)
				Math.atan2((vector2 * vector1) % normal, vector1 % vector2)
			end
			def self.find_mesh_geometry(entities)
				entities.select do |e|
					[Sketchup::Face, Sketchup::Edge].include?(e.class)
				end
			end
			def self.get_parent_group(entity)
				group = entity.parent.instances[-1] if entity &&
					entity.parent.is_a?(Sketchup::ComponentDefinition) &&
					entity.parent.group?
			end
			def self.get_frame_of_group(entity)
				frame = nil
				condition = true
				while condition
					papa = entity.parent
					if papa.is_a?(Sketchup::Model)
						condition = false
					else
						parent_group = get_parent_group(entity)
						if parent_group
							comp_type =
								parent_group.get_attribute 'amtf_dict',
								                           'comp_type'
							if (comp_type == 'AmtfFrame')
								frame =
									AMTF_STORE.frame_premitives[
										parent_group.persistent_id
									]
								condition = false
							end
							entity = parent_group
						else
							condition = false
						end
					end
				end
				return frame
			end
			def self.get_frame_of_part(part)
				get_frame_of_group(part.comp)
			end
		end #Utilities
	end #BuKong
end #Amtflogi.