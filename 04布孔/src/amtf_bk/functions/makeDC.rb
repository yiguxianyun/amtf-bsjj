# encoding: UTF-8
module AMTF
    module BUKONG
      module DynamicComponent
        class DCObserver < Sketchup::EntityObserver
          def initialize
            # @touched=0
            # @animator=TwistAnimation.new
          end
          def onEraseEntity(entity)
          end
          def onChangeEntity(entity)    # changes to the dynamic_attributes dictionary
            return if entity.deleted?
            # width = entity.bounds.width
            DynamicComponent.build_component(200)
          end
        end
        def self.build_component(size)
          component = Sketchup.active_model.definitions.add("TwistCube");
          component.entities.erase_entities(component.entities.to_a) # erase any pre-existing entities
          points = Array.new  
          points[0] = ORIGIN 
          points[1] = [size, 0 , 0]; points[2] = [size, size, 0]; points[3] = [0 , size, 0]
          face = component.entities.add_face(points)
          face.reverse! if face.normal.z < 0
          face.pushpull(size)
          return component
        end
        def self.initDynamicComponent(entity)
          entity.set_attribute "dynamic_attributes","_formatversion",1.0
          da=entity.attribute_dictionary("dynamic_attributes")
          da["onclick"]="Animate(_touched,0,1)"
          da["_touched"]="0"
          return da
        end
        def self.addDynamicAttribute(dattr,attribute, value, units: "INCHES",access: false)
          dattr[attribute]=value
          dattr["_"+attribute+"_label"]=attribute
          if access
            dattr["_"+attribute+"_access"]="TEXTBOX" 
            dattr["_"+attribute+"_formlabel"]=attribute
          end 
          ["","formula"].each{|x| dattr["_"+attribute+"_"+x+"units"]=units}
        end
   def wrap_selection(ents)
      actents = Sketchup.active_model.active_entities
      grp_inner = actents.add_group(ents)
      grp_dados = grp_inner.to_component
      grp_atrib = grp_dados.definition
      grp_atrib.set_attribute "dynamic_attributes","_lengthunits","CENTIMETERS"
      grp_atrib.set_attribute "dynamic_attributes","_name","camada"
      grp_atrib.set_attribute "dynamic_attributes","name",""
      grp_atrib.set_attribute "dynamic_attributes","description",""
      grp_atrib.set_attribute "dynamic_attributes","lenx",""
      grp_atrib.set_attribute "dynamic_attributes","leny",""
      grp_atrib.set_attribute "dynamic_attributes","lenz",""
      grp_atrib.set_attribute "dynamic_attributes","x",""
      grp_atrib.set_attribute "dynamic_attributes","y",""
      grp_atrib.set_attribute "dynamic_attributes","z",""
      grp_atrib.set_attribute "dynamic_attributes","x",""
      grp_atrib.set_attribute "dynamic_attributes","y",""
      grp_atrib.set_attribute "dynamic_attributes","z",""
      grp_atrib.set_attribute "dynamic_attributes","material",""
    end
        def self.createDC
        model = Sketchup.active_model
        entities = model.entities
        def1 = Sketchup.active_model.definitions.add("Sheet1");        
        points1 = Array.new  
        points1[0] = ORIGIN 
        points1[1] = [100, 0 , 0]; points1[2] = [100, 17, 0]; points1[3] = [0 , 17, 0]
        face1 = def1.entities.add_face(points1)
        face1.reverse! if face1.normal.z < 0
        face1.pushpull(100)
        def2 = Sketchup.active_model.definitions.add("Sheet2");    
        points2 = Array.new  
        points2[0] = [0, 100 , 0] 
        points2[1] = [100, 100 , 0]; points2[2] = [100, 117, 0]; points2[3] = [0 , 117, 0]
        face2 = def2.entities.add_face(points2)
        face2.reverse! if face2.normal.z < 0
        face2.pushpull(100)
        group_def = Sketchup.active_model.definitions.add("Frame"); 
        inst1=  group_def.entities.add_instance(def1, Geom::Transformation.new)
        inst2=  group_def.entities.add_instance(def2, Geom::Transformation.new)
        frame = Sketchup.active_model.entities.add_instance(group_def, Geom::Transformation.new)
        cmd = "#{200/2}*2"
        frame.definition.set_attribute "dynamic_attributes", "_lenz_formula", cmd
        frame.definition.set_attribute "dynamic_attributes","_lengthunits","MILLIMETERS"
        inst1.definition.set_attribute "dynamic_attributes", "_lenz_formula", "parent!LenZ - 30"
        inst1.definition.set_attribute "dynamic_attributes","_lengthunits","MILLIMETERS"
        # inst1.definition.set_attribute "dynamic_attributes","_inst_copies","5"
        # inst1.definition.set_attribute "dynamic_attributes","_inst__z_formula","50+COPY*LenZ"
        # inst1.definition.set_attribute "dynamic_attributes","_z_formula","50+COPY*(LenZ+50)"
        inst2.definition.set_attribute "dynamic_attributes","_x_formula","parent!X + 100"
        dcs = $dc_observers.get_latest_class
        dcs.redraw_with_undo(inst1)
        dcs.redraw_with_undo(frame)
          # 
          # # component = build_component 100
          # # cube = Sketchup.active_model.active_entities.add_instance(component, Geom::Transformation.new)
          # # group = Sketchup.active_model.active_entities.add_group(cube)
          # # group.name = "TwistCube"
          # # da=initDynamicComponent(group)
          # # addDynamicAttribute(da,"size",100,access: true) # starting size of cube
          # # da.add_observer(DCObserver.new)
          #  # Assumes that sang is the 1st entity in model.
          # sang = Sketchup.active_model.entities[0]
          # sang_def = sang.definition
          # # Override sang's shirt color to red. ("material"
          # # is a special attribute that requires
          # # you to set a formula to "take control"
          # # over the default material the user has painted.)
          # sang_def.set_attribute 'dynamic_attributes',
          #   'material', 'red'
          # sang_def.set_attribute 'dynamic_attributes',
          #   '_material_formula', '"red"'
          #   sang_def.set_attribute 'dynamic_attributes',
          #   '_lenx_formula', 'X+200'
          # # Add a new configurable option to Sang.
          # # (Any attribute that starts with an underscore
          # # is a "meta attribute" that describes behavior.)
          # sang_def.set_attribute 'dynamic_attributes',
          #   'weight', '145'
          # sang_def.set_attribute 'dynamic_attributes',
          #   '_weight_label', 'weight'
          # sang_def.set_attribute 'dynamic_attributes',
          #   '_weight_formlabel', 'My Weight'
          # sang_def.set_attribute 'dynamic_attributes',
          #   '_weight_units', 'STRING'
          # sang_def.set_attribute 'dynamic_attributes',
          #   '_weight_access', 'TEXTBOX'
          # # Change the description that shows
          # # up in the configure box with a custom
          # # formula.
          # sang_def.set_attribute 'dynamic_attributes',
          #   '_description_formula',
          #   '"Sang is now red and weighs " & weight'
          # # There is a global handle into the plugin that
          # # allows you to make the same calls that the
          # # plugin does, like so...
          # dcs = $dc_observers.get_latest_class
          # dcs.redraw_with_undo(sang)
        end
      end
    end
end
