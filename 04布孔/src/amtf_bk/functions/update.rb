# encoding: UTF-8
require 'zlib'
require 'fileutils'
module AMTF
	module BUKONG
		module UpdateModule
			def self.fetch_server
				# begin
				form = { version: "#{EXTENSION.version}" }
				url = "#{ServerOptions.get_server_url}/api/check_version"
				request = Sketchup::Http::Request.new(url, Sketchup::Http::POST)
				request.body = URI.encode_www_form(form)
				request.start do |req, res|
					if req.status == Sketchup::Http::STATUS_SUCCESS
						check_version = JSON.parse(res.body)
						file_name = check_version['file_name']
						if file_name
							update_url =
								"#{ServerOptions.get_update_server_url}/update/#{file_name}"
							self.download_modules(update_url, file_name)
						end
					else
						puts 'Request was not sucessful!'
					end
				end
				# rescue StandardError
				# 	puts 'fetch server error'
				# end
			end ###
			def self.download_modules(url, file_name)
				# begin
				req = Sketchup::Http::Request.new(url, Sketchup::Http::GET)
				req.headers = { 'content-type': 'application/octet-stream' }
				req.set_download_progress_callback do |current, total|
					current_kb = (current / 1024).to_i
					total_kb = (total / 1024).to_i
					Sketchup.set_status_text 'Dowloading update ...' + ' ' +
							current_kb.to_s + '/' + total_kb.to_s
				end
				req.start do |req, res|
					if req.status == Sketchup::Http::STATUS_SUCCESS
						puts 'Request update success!'
						dirname = File.dirname(PLUGIN_DIR) + '/bukong_temp'
						FileUtils.mkdir_p(dirname) unless File.exists?(dirname)
						file_path = File.join(dirname, file_name)
						File.binwrite(file_path, res.body)
						unpack(file_path)
					else
						puts "can't download"
					end
				end
				# rescue StandardError
				# 	puts 'download error'
				# end
			end
			def self.unpack(src)
				# begin
				folder = File.dirname(PLUGIN_DIR)
				puts 'Unpacking ...'
				FileUtils.rm_rf(PLUGIN_DIR) if File.directory?(PLUGIN_DIR)
				FileUtils.mkdir_p(PLUGIN_DIR) unless File.directory?(PLUGIN_DIR)
				Zlib::GzipReader.open(src) do |file|
					num_of_file = file.read(32).to_i(2)
					num_of_file.times do
						name_len = file.read(32).to_i(2)
						file_path = file.read(name_len)
						file_size = file.read(32).to_i(2)
						file_data = file.read(file_size)
						parent = folder
						dirname = parent + '/' + File.dirname(file_path)
						unless File.directory?(dirname)
							FileUtils.mkdir_p(dirname)
						end
						file_name = parent + '/' + file_path
						File.write(file_name, file_data, mode: 'wb')
					end
					file.close
				end
				if File.exist?(src)
					if File.directory?(File.dirname(src))
						FileUtils.rm_rf(File.dirname(src))
					end
				end
				Sketchup.set_status_text OB[:update_completed]
				# UI.messagebox(OB[:update_completed])
				# rescue StandardError
				# 	puts 'unpack error'
				# end
			end
		end
	end
end