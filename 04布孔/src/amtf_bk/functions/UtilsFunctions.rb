# encoding: UTF-8
module AMTF
    module BUKONG   
      module UtilsFunctions
        def self.refresh(entity)
          return unless entity.valid?
          comp_type = entity.get_attribute 'amtf_dict', 'comp_type' 
          if( comp_type == 'AmtfFrame')  
            entity.make_unique   
            entity.add_observer(BuKongEntityObserver.new)    
            saver =  AmtfSaver.new
            attrs = saver.open_comp_json(entity)
            if attrs               
              frm =  AmtfFrame.new
              frm.init( attrs, entity )
              frm.update(attrs)
            end
          end  
        end
        def self.swap(arr, xp, yp)
          temp = arr[xp];
          arr[xp] = arr[yp];
          arr[yp] = temp;
        end
        def self.sort3dPoints(points) 
          dir = points[0].vector_to(points[1])
          i = 0
          j = 0
          n = points.size
          for i in 0..n - 2          
              for j in 0..n-i-2              
                  vect = points[ j ].vector_to(points[j+1]);
                  swap(points, j, j+1 ) if vect.dot(dir) < 0
              end    
          end
          return points;
        end
        def self.get_edges_hash_world(face, tr)
          edges = {}
          face.edges.each{
            |edg|
            p1 = edg.start.position
            p2 = edg.end.position
            p1 = p1.transform(tr.inverse)
            p2 = p2.transform(tr.inverse)
            edges[edg] = {:p1 => p1, :p2 => p2}
          }
          return edges
        end
        def self.on_boundary?( face, point )
          edges = face.edges
          on_bounds = false
          edges.each{
            |edge|
            if self.point_on_edge?(point, edge)
              on_bounds = true
              break
            end
          }
          return on_bounds
        end
        def self.between?(a, b, c, on_point = true)
          return false unless c.on_line?([a,b])
          v1 = c.vector_to(a)
          v2 = c.vector_to(b)
          if on_point
            return true  if !v1.valid? || !v2.valid?
          else
            return false if !v1.valid? || !v2.valid?
          end
          !v1.samedirection?(v2)
        end
        def self.point_on_edge?(point, edge)
          a = edge.start.position
          b = edge.end.position
          self.between?(a, b, point, true)
        end
        def self.amtf_intersect(e1, e2)
          # bb1 = e1.bounds
          # bb2 = e2.bounds
          # boundingbox = bb1.intersect(bb2)
          # return [] unless boundingbox.valid?
          bigFaces = Utilities.get_big_faces(e1)
          edge_faces = Utilities.get_edge_faces(e1)
          partnerFaces = Utilities.get_big_faces(e2)
          partnerEdgeFaces = Utilities.get_edge_faces(e2)
          build_hash = [];
          visited = [];
          for i in 0..bigFaces.size - 1 do
            myFace = bigFaces[i];            
            myPlane = myFace.plane            
            for j in 0..partnerFaces.size - 1 do
              yourFace = partnerFaces[j]
              yourPlane = yourFace.plane
              intersect_line = Geom.intersect_plane_plane(myPlane, yourPlane)
              next unless intersect_line
              lines = []
              edge_faces.each{
                |f|
                ePlane = f.plane
                itr_point = Geom.intersect_line_plane(intersect_line, ePlane)                        
                  if itr_point 
                      copy_intr1 = itr_point.clone
                      copy_intr2 = itr_point.clone
                      copy_intr1.transform!(e2.transformation.inverse)                      
                      copy_intr2.transform!(e1.transformation.inverse)          
                    if Utilities.within?(copy_intr2, e1) && Utilities.within?(copy_intr1, e2)
                      lines.push( itr_point )
                    end                        
                  end       
              }
              partnerEdgeFaces.each{ 
                |f|
                ePlane = f.plane
                itr_point = Geom.intersect_line_plane(intersect_line, ePlane)   
                if itr_point 
                    copy_intr1 = itr_point.clone
                    copy_intr2 = itr_point.clone
                    copy_intr1.transform!(e2.transformation.inverse)                      
                    copy_intr2.transform!(e1.transformation.inverse)          
                  if Utilities.within?(copy_intr2, e1) && Utilities.within?(copy_intr1, e2)
                    lines.push( itr_point )
                  end                        
                end        
              }
              lines = Utilities.uniq_points(lines)
              next if lines.size < 2
              trVector = Utilities.vectorMultiply(lines[0].vector_to(lines[1]).normalize(), 5.mm)
              translate = Geom::Transformation.translation(trVector)
              mid = lines[0].transform(translate)
              copy_mid1 = mid.clone
              copy_mid2 = mid.clone
              copy_mid1.transform!(e1.transformation.inverse)                      
              copy_mid2.transform!(e2.transformation.inverse)
              tempHash = {}
              hashByEdge = {}
              if !self.on_boundary?( myFace, copy_mid1 )
                  if !visited.include?( myFace ) && !visited.include?( yourFace ) 
                      lines = Utilities.uniq_points(lines)
                      tempHash = {
                          :bigFace => myFace,
                          :edgeFace => yourFace,
                          :lines => lines,
                          :bigComp => e1,
                          :edgeComp => e2
                      }
                      visited.push(myFace, yourFace )
                  end
              elsif !self.on_boundary?( yourFace, copy_mid2 )
                if !visited.include?( myFace ) && !visited.include?( yourFace ) 
                    lines = Utilities.uniq_points(lines)
                    tempHash = {
                        :bigFace => yourFace,
                        :edgeFace => myFace,
                        :lines => lines,
                        :bigComp => e2,
                        :edgeComp => e1
                    }
                    visited.push(myFace, yourFace )
                end
              else
                next
              end
              next if tempHash.empty?()
              if lines.size === 2              
                    build_hash.push(tempHash);
              elsif lines.size > 2
                  egdes = self.get_edges_hash_world(tempHash[:edgeFace], tempHash[:edgeComp].transformation)
                  #  Có 2 trường hợp
                  # 1) Các điểm trên cùng một cạnh
                  # 2) Các điểm trên các cạnh khác nhau
                  egdes.each do |k, edg|            
                    for i in 0..tempHash[:lines].size - 1                         
                      if self.between?(edg[:p1], edg[ :p2 ], tempHash[:lines][ i ])
                        if hashByEdge.key?(k)          
                          hashByEdge[ k ][:lines].push( tempHash[:lines][ i ] )                        
                        else
                          hashByEdge[ k ] = {
                              :lines => [ tempHash[:lines][ i ] ],
                              :bigFace => tempHash[:bigFace],
                              :edgeFace => tempHash[:edgeFace],
                              :edgeComp => tempHash[:edgeComp],
                              :faceComp => tempHash[:faceComp]
                          }
                        end
                      end
                    end
                  end
                  # Sắp xếp lại các điểm theo th�?t�? các thành phần có dưới 2 điểm thì loại b�?
                  hashByEdge.each{
                    |key, item|
                    if item[:lines].size === 2                       
                      build_hash.push( item )                      
                    elsif item[:lines].size > 2 
                      sort3dPoints(item[:lines]); 
                      temp = [];
                      jj = 0;
                      inEdge = true;
                      while jj < item[:lines].size - 1                       
                          mid = Utilities.get_midpoint(item[:lines][ jj ], item[:lines][ jj + 1 ]);  
                          mid.transform!( item[:bigComp].transformation.inverse )
                          if Utilities.within_face?(mid, item[:bigFace])
                              temp.push(item[:lines][ jj ]);
                              temp.push(item[:lines][ jj + 1 ]);
                              inEdge = true;
                          else 
                              inEdge = false;
                          end
                          jj +=1
                          if !inEdge || jj === item[:lines].size - 1
                            tempLines = Utilities.uniq_points( temp );
                            if tempLines.size >=2
                              tempLines = [ tempLines[ 0 ], tempLines[ tempLines.size - 1 ] ]                                        
                              build_hash.push( {
                                  :lines => tempLines,
                                  :bigFace => tempHash[:bigFace],
                                  :edgeFace => tempHash[:edgeFace],
                                  :edgeComp => tempHash[:edgeComp],
                                  :faceComp => tempHash[:faceComp]
                              })
                            end 
                            temp = []
                          end
                      end#while
                    end
                  }
              end
            end#end for             
          end #end for
          build_hash.each{
            |item|
            model = Sketchup.active_model
            entities = model.active_entities
            point1 = item[:lines][ 0 ]
            point2 = item[:lines][ 1 ]
            cline = entities.add_cline(point1, point2)
            cpoint = entities.add_cpoint(point1)
          }
          return build_hash
        end        
      end
    end
end