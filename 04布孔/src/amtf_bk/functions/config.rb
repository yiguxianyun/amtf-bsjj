# encoding: UTF-8
module AMTF
	module BUKONG
		module Config
			def self.create_dialog(data)
				options = {
					dialog_title: OB[:setting],
					preferences_key: 'bukongcabinet.net',
					style: UI::HtmlDialog::STYLE_DIALOG,
					# Set a fixed size now that we know the content size.
					resizable: false,
					scrollable: true,
					width: 700,
					height: 640
				}
				url =
					"#{ServerOptions.get_server_url}/setting?data=#{data.to_json}"
				dialog = UI::HtmlDialog.new(options)
				dialog.set_size(options[:width], options[:height]) # Ensure size is set.
				dialog.set_url(url)
				dialog.center
				dialog
			end
			def self.setting
				data = get_setting
				@dialog.close if !@dialog.nil?
				@dialog = self.create_dialog(data)
				Bridge.decorate(@dialog)
				@dialog.on('save_config_data') do |deferred, params|
					begin
						params.each do |key, value|
							Sketchup.write_default(
								'AmtflogicBuKong',
								key.to_s,
								value
							)
						end
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('config_backup_json') do |deferred|
					begin
						cnc_data = {
							toolData:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'toolData',
									''
								),
							ppData:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'ppData',
									''
								),
							datumXY:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'datumXY',
									'1'
								),
							originZ:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'originZ',
									'machine_bed'
								),
							plungeZ:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'plungeZ',
									5
								),
							clearanceZ:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'clearanceZ',
									30
								),
							toolGroup:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'toolGroup',
									''
								),
							cutting_tool_path:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'cutting_tool_path',
									''
								),
							tool_group_spindle_off:
								Sketchup.read_default(
									'AmtflogicBuKong',
									'tool_group_spindle_off',
									false
								)
						}
						config_data = get_setting
						deferred.resolve(
							{ cnc_data: cnc_data, config_data: config_data }
						)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('save_from_json') do |deferred, json_data|
					begin
						cnc_data = json_data['cnc_data']
						config_data = json_data['config_data']
						config_data.each do |key, value|
							Sketchup.write_default(
								'AmtflogicBuKong',
								key.to_s,
								value
							)
						end
						cnc_data.each do |key, value|
							Sketchup.write_default(
								'AmtflogicBuKong',
								key.to_s,
								value
							)
						end
						deferred.resolve(true)
						@dialog.close
						AMTF_Dialog.no_closed = true
						AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
						AMTF_Dialog.no_closed = false
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.show
			end
			def self.get_setting
				data = {
					language: OB.saved_lang,
					spacing:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'spacing',
							13
						),
					binOffset:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'binOffset',
							4
						),
					binWidth:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'binWidth',
							1220
						),
					binHeight:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'binHeight',
							2440
						),
					rotations:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'rotations',
							4
						),
					useHoles:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'useHoles',
							true
						),
					corporateName:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'corporateName',
							''
						),
					labelWidth:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'labelWidth',
							60
						),
					labelHeight:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'labelHeight',
							40
						),
					printUnit:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'printUnit',
							'mm'
						),
					do_dai_cam:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'do_dai_cam',
							34
						),
					do_sau_cam:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'do_sau_cam',
							9
						),
					duong_kinh_cam:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'duong_kinh_cam',
							15
						),
					khoang_cach_cam_chot:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'khoang_cach_cam_chot',
							32
						),
					khoang_chia_lo_cam_max:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'khoang_chia_lo_cam_max',
							1000
						),
					khoang_chia_lo_cam_min:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'khoang_chia_lo_cam_min',
							90
						),
					mini_fix_big_diameter:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'mini_fix_big_diameter',
							10
						),
					mini_fix_small_diameter:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'mini_fix_small_diameter',
							8
						),
					no_dowel:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'no_dowel',
							false
						),
					vi_tri_hinge:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'vi_tri_hinge',
							100
						),
					vi_tri_oc_cam:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'vi_tri_oc_cam',
							50
						),
					day_nep_dan_canh:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'day_nep_dan_canh',
							1
						),
					all_edge_banding:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'all_edge_banding',
							false
						),
					hingeData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'hingeData',
							''
						),
					edgeTypeData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'edgeTypeData',
							''
						),
					noDrill_machine:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'noDrill_machine',
							false
						),
					khoanmoiData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'khoanmoiData',
							''
						),
					chotdotData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'chotdotData',
							''
						),
					patVData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'patVData',
							''
						),
					rafixData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'rafixData',
							''
						),
					lockdowelData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'lockdowelData',
							''
						),
					view_type1:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'view_type1',
							'wireframe'
						),
					view_type2:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'view_type2',
							'shaded_texture'
						),
					coonection_type:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'coonection_type',
							'cam_chot'
						),
					grooveTypeData:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'grooveTypeData',
							''
						),
					hinge_max_distance:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'hinge_max_distance',
							1000
						),
					hinge_min_distance:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'hinge_min_distance',
							90
						),
					screen_height:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'screen_height',
							838
						),
					screen_width:
						Sketchup.read_default(
							'AmtflogicBuKong',
							'screen_width',
							1550
						)
				}
				return data
			end
		end #Config
	end #BuKong
end #Amtflogic