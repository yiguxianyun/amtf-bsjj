# encoding: UTF-8
module AMTF
	module BUKONG
		module Nesting_functions
			def self.nesting
				query = AmtfQuery.new
				sheets = query.query_sheets(Sketchup.active_model.selection)
				return unless sheets && sheets.size > 0
				pb =
					ExtraFuntion::ProgressBar.new(
						sheets.size,
						OB[:build_part_list]
					)
				counter = 0
				executing_error = []
				if sheets && sheets.size > 0
					sheets.compact!
					data = []
					timer =
						UI.start_timer(0.005, true) do
							if counter == sheets.size
								UI.stop_timer(timer)
								if executing_error.size > 0
									Sketchup.set_status_text(executing_error[0])
									next
								end
								transX = 0.to_l
								preMax = 0.to_l
								data.compact!
								data.each do |d|
									next if d.nil?
									transX +=
										Utilities.distance_X(
											d[:top][:points],
											preMax
										)
									preMax = Utilities.max_X(d[:top][:points])
									translate =
										Geom::Transformation.new [transX, 0, 0]
									d[:top][:points].map! do |item|
										item.transform(translate)
									end
									if d[:top][:holePoints].size > 0
										d[:top][:holePoints].map! do |hole|
											{
												center:
													hole[:center].transform(
														translate
													),
												type: hole[:type],
												radius: hole[:radius],
												config_data: hole[:config_data],
												connector_type_id:
													hole[:connector_type_id],
												minifixID: hole[:minifixID]
											}
										end
									end
									if d[:top][:dogboneMaleHoles].size > 0
										d[:top][:dogboneMaleHoles]
											.map! do |hole|
											{
												center1:
													hole[:center1].transform(
														translate
													),
												center2:
													hole[:center2].transform(
														translate
													),
												type: hole[:type],
												radius: hole[:radius],
												config_data: hole[:config_data],
												connector_type_id:
													hole[:connector_type_id],
												minifixID: hole[:minifixID]
											}
										end
									end
									if d[:top][:dogboneFemaleHoles].size > 0
										d[:top][:dogboneFemaleHoles]
											.map! do |hole|
											{
												center1:
													hole[:center1].transform(
														translate
													),
												center2:
													hole[:center2].transform(
														translate
													),
												type: hole[:type],
												radius: hole[:radius],
												config_data: hole[:config_data],
												connector_type_id:
													hole[:connector_type_id],
												minifixID: hole[:minifixID]
											}
										end
									end
									# d[:top][:curveEdges][:points].each{ |item|
									#     item[:point].transform!(translate)
									# }
									if d[:top][:chanelPoints].size > 0
										d[:top][:chanelPoints].each do |rabbet|
											rabbet[:points].map! do |point|
												point.transform(translate)
											end
											rabbet[:offset_points]
												.map! do |point|
												point.transform(translate)
											end
										end
									end
									if d[:top][:custom_path].size > 0
										d[:top][:custom_path].each do |rabbet|
											rabbet[:points].map! do |point|
												point.transform(translate)
											end
										end
									end
									if d[:top][:lockdowelPoints].size > 0
										d[:top][:lockdowelPoints] =
											d[:top][:lockdowelPoints]
												.map! do |lockdowel|
												lockdowel = {
													p1:
														lockdowel[:p1]
															.transform(
															translate
														),
													p2:
														lockdowel[:p2]
															.transform(
															translate
														),
													p3:
														lockdowel[:p3]
															.transform(
															translate
														),
													config_data:
														lockdowel[:config_data]
												}
											end
									end
									if d[:top][:tool_path].size > 0
										d[:top][:tool_path].each do |tool_path|
											tool_path[:points].map! do |point|
												point.transform(translate)
											end
										end
									end
									if d[:top][:pocket_path].size > 0
										d[:top][:pocket_path]
											.each do |pocket_path|
											pocket_path[:outer_points]
												.map! do |point|
												point.transform(translate)
											end
											pocket_path[:inner_points]
												.map! do |point|
												point.transform(translate)
											end
										end
									end
									#################################
									#Vex ra màn hình
									# Sketchup.active_model.entities.add_face(
									# 	d[:top][:points]
									# )
									# d[:top][:chanelPoints].each do |pts|
									# 	if pts.size > 0
									# 		Sketchup
									# 			.active_model
									# 			.entities
									# 			.add_face(pts[:points])
									# 	end
									# end
									# d[:top][:custom_path].each do
									#   |tool_path|
									#   for i in 0..tool_path[:points].size - 2
									#     Sketchup.active_model.entities.add_line tool_path[:points][i], tool_path[:points][i+1]
									#   end
									# end
									# d[:top][:lockdowelPoints].each do
									#   |lockwowel|
									#     vector = Geom::Vector3d.new 0,0,1
									#     vector2 = vector.normalize!
									#     Sketchup.active_model.entities.add_circle lockwowel[:p1], vector2, 5.mm
									#     Sketchup.active_model.entities.add_circle lockwowel[:p2], vector2, 5.mm
									#     Sketchup.active_model.entities.add_circle lockwowel[:p3], vector2, 10.mm
									#     Sketchup.active_model.entities.add_line lockwowel[:p1],lockwowel[:p2]
									#     Sketchup.active_model.entities.add_line lockwowel[:p3],lockwowel[:p2]
									# end
									# # ##################################
									# d[:top][:holePoints].each do |hole|
									# 	# # vẽ ra
									# 	vector = Geom::Vector3d.new 0, 0, 1
									# 	vector2 = vector.normalize!
									# 	Sketchup.active_model
									# 		.entities.add_circle hole[:center],
									# 	                                          vector2,
									# 	                                          hole[
									# 			:radius
									# 	                                          ]
									# 			.to_s.to_l
									# end
									# puts d[:top][:dogboneMaleHoles].size
									# d[:top][:dogboneMaleHoles].each do |hole|
									# 	# # vẽ ra
									# 	vector = Geom::Vector3d.new 0, 0, 1
									# 	vector2 = vector.normalize!
									# 	puts hole[:radius]
									# 	Sketchup.active_model
									# 		.entities.add_circle hole[:center1],
									# 	                                          vector2,
									# 	                                          hole[
									# 			:radius
									# 	                                          ]
									# 	Sketchup.active_model
									# 		.entities.add_circle hole[:center2],
									# 	                                          vector2,
									# 	                                          hole[
									# 			:radius
									# 	                                          ]
									# end
									#################################################
									# inner loop
									if d[:top][:innerLoop].size > 0
										d[:top][:innerLoop].each do |loops|
											loops.map! do |item|
												item.transform(translate)
											end
										end
										d[:top][:curveEdges_inner]
											.each do |loops|
											loops[:points].map! do |item|
												item = {
													point:
														item[:point].transform(
															translate
														),
													curve: item[:curve],
													bulge: item[:bulge]
												}
											end
										end
									end
									#Xử lý chỉ định nẹp dán cạnh
									if d[:top][:edgeBandingMark].size > 0
										d[:top][:edgeBandingMark]
											.map! do |item|
											{
												startPoint:
													item[:startPoint].transform(
														translate
													),
												endPoint:
													item[:endPoint].transform(
														translate
													),
												banding: item[:banding]
											}
										end
									end
									if d[:top][:edgeBandingMark_inner].size > 0
										d[:top][:edgeBandingMark_inner]
											.each do |loops|
											loops.map! do |item|
												{
													startPoint:
														item[:startPoint]
															.transform(
															translate
														),
													endPoint:
														item[:endPoint]
															.transform(
															translate
														),
													banding: item[:banding]
												}
											end
										end
									end
									if d[:both_side]
										#Cùng sử dụng translate của top side cho mục đích gia công hai mặt
										if d[:bottom][:holePoints].size > 0
											d[:bottom][:holePoints]
												.map! do |hole|
												{
													center:
														hole[:center].transform(
															translate
														),
													type: hole[:type],
													radius: hole[:radius],
													config_data:
														hole[:config_data],
													connector_type_id:
														hole[
															:connector_type_id
														],
													minifixID: hole[:minifixID]
												}
											end
										end
										if d[:bottom][:dogboneFemaleHoles]
												.size > 0
											d[:bottom][:dogboneFemaleHoles]
												.map! do |hole|
												{
													center1:
														hole[:center1]
															.transform(
															translate
														),
													center2:
														hole[:center2]
															.transform(
															translate
														),
													type: hole[:type],
													radius: hole[:radius],
													config_data:
														hole[:config_data],
													connector_type_id:
														hole[
															:connector_type_id
														],
													minifixID: hole[:minifixID]
												}
											end
										end
										if d[:bottom][:chanelPoints].size > 0
											d[:bottom][:chanelPoints]
												.each do |rabbet|
												rabbet[:points].map! do |point|
													point.transform(translate)
												end
												rabbet[:offset_points]
													.map! do |point|
													point.transform(translate)
												end
											end
										end
										if d[:bottom][:custom_path].size > 0
											d[:bottom][:custom_path]
												.each do |rabbet|
												rabbet[:points].map! do |point|
													point.transform(translate)
												end
											end
										end
										if d[:bottom][:lockdowelPoints].size > 0
											d[:bottom][:lockdowelPoints] =
												d[:bottom][:lockdowelPoints]
													.map! do |lockdowel|
													lockdowel = {
														p1:
															lockdowel[:p1]
																.transform(
																translate
															),
														p2:
															lockdowel[:p2]
																.transform(
																translate
															),
														p3:
															lockdowel[:p3]
																.transform(
																translate
															),
														config_data:
															lockdowel[
																:config_data
															]
													}
												end
										end
										#   d[:bottom][:lockdowelPoints].each do
										#   |lockwowel|
										#     vector = Geom::Vector3d.new 0,0,1
										#     vector2 = vector.normalize!
										#     Sketchup.active_model.entities.add_circle lockwowel[:p1], vector2, 5.mm
										#     Sketchup.active_model.entities.add_circle lockwowel[:p2], vector2, 5.mm
										#     Sketchup.active_model.entities.add_circle lockwowel[:p3], vector2, 10.mm
										#     Sketchup.active_model.entities.add_line lockwowel[:p1],lockwowel[:p2]
										#     Sketchup.active_model.entities.add_line lockwowel[:p3],lockwowel[:p2]
										# end
										if d[:bottom][:tool_path].size > 0
											d[:bottom][:tool_path]
												.each do |tool_path|
												tool_path[:points]
													.map! do |point|
													point.transform(translate)
												end
											end
										end
										if d[:bottom][:pocket_path].size > 0
											d[:bottom][:pocket_path]
												.each do |pocket_path|
												pocket_path[:outer_points]
													.map! do |point|
													point.transform(translate)
												end
												pocket_path[:inner_points]
													.map! do |point|
													point.transform(translate)
												end
											end
										end
									end
								end
								# Tách các loại material khác nhau thành các mảng
								# Đối với mỗi mảng cùng material, lại tách riêng ra thành các mảng có độ dày khác nhau
								materials = []
								data.each do |item|
									mat = ''
									mat += item[:material] if item[:material]
									mat += item[:color].to_s if item[:color]
									materials << mat
								end
								materials.uniq!
								data_by_materials = []
								materials.each do |mat|
									data_m =
										data.select do |obj|
											mat1 = ''
											mat1 += obj[:material] if obj[
												:material
											]
											mat1 += obj[:color].to_s if obj[
												:color
											]
											mat1 == mat
										end
									data_by_materials << data_m
								end
								final_data = []
								data_by_materials.each do |item|
									thickness = []
									item.each do |obj|
										thickness.push(obj[:thick].to_l)
									end
									thickness =
										Utilities.make_uniq_array(thickness)
									# Ở đây phải dùng make_uniq_aray tự viết vì các giá trị của mảng ở dạng float, không phải lengh, có giá trị khác nhau nhưng to_l lại bằng nhau
									thickness.each do |th|
										data_thick =
											item.select do |obj|
												obj[:thick].to_l == th
											end
										final_data << data_thick
									end
								end
								@nest_data = { data: final_data }
								# puts "v=#{@nest_data.to_json}"
								url = "#{ServerOptions.get_server_url}/nesting"
								if !AMTF_Dialog.dialog.nil? &&
										AMTF_Dialog.dialog.visible?
									AMTF_Dialog.dialog.bring_to_front
									AMTF_Dialog.dialog.set_url(url)
									AMTF_Dialog.dialog.set_position(30, 30)
								else
									AMTF_Dialog.create_dialog
									AMTF_Dialog.dialog.set_url(url)
									AMTF_Dialog.dialog.show
									AMTF_Dialog.dialog.set_position(30, 30)
								end
								AMTF_Dialog
									.dialog
									.on('request_nesting_data') do |deferred|
										begin
											deferred.resolve(@nest_data.to_json)
										rescue => error
											Utilities.print_exception(
												error,
												false
											)
											Sketchup.set_status_text(error)
											deferred.resolve(false)
										end
									end
								Sketchup.set_status_text(OB[:completed])
							else
								begin
									part = sheets[counter]
									comp_type =
										part.comp.get_attribute(
											'amtf_dict',
											'comp_type'
										)
									if comp_type != 'curve_toebase'
										data <<
											part.get_nesting_data(
												executing_error
											)
									end
									counter += 1
									if counter / sheets.size <= 0.9
										pb.update(counter)
									end
								rescue => error
									Utilities.print_exception(error, false)
									counter += 1
								end
							end
						end
				end #sheets.size > 0
			end
		end #Nesting_functions
	end #WoodPro
end #