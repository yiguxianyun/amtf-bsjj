# encoding: UTF-8
module AMTF
	module BUKONG
		module Point_to_point
			def self.execute_pp
				url = "#{ServerOptions.get_server_url}/point_to_point"
				if !AMTF_Dialog.dialog.nil? && AMTF_Dialog.dialog.visible?
					AMTF_Dialog.dialog.bring_to_front
					AMTF_Dialog.dialog.set_url(url)
					AMTF_Dialog.dialog.set_position(30, 30)
				else
					AMTF_Dialog.create_dialog
					AMTF_Dialog.dialog.set_url(url)
					AMTF_Dialog.dialog.show
					AMTF_Dialog.dialog.set_position(30, 30)
				end
				AMTF_Dialog
					.dialog
					.on('point_to_point_execute') do |deferred, config|
						begin
							query = AmtfQuery.new
							sheets =
								query.query_sheets(
									Sketchup.active_model.selection
								)
							return unless sheets && sheets.size > 0
							pb =
								ExtraFuntion::ProgressBar.new(
									sheets.size,
									OB[:build_part_list]
								)
							counter = 0
							executing_error = []
							if sheets && sheets.size > 0
								sheets.compact!
								data = []
								timer =
									UI.start_timer(0.005, true) do
										if counter == sheets.size
											UI.stop_timer(timer)
											if executing_error.size > 0
												Sketchup.set_status_text(
													executing_error[0]
												)
												next
											end
											deferred.resolve(data)
											Sketchup.set_status_text(
												OB[:completed]
											)
										else
											begin
												part = sheets[counter]
												comp_type =
													part.comp.get_attribute(
														'amtf_dict',
														'comp_type'
													)
												if comp_type != 'curve_toebase'
													data <<
														part.get_point_to_point(
															executing_error,
															config
														)
												end
												counter += 1
												if counter / sheets.size <= 0.9
													pb.update(counter)
												end
											rescue => error
												Utilities.print_exception(
													error,
													false
												)
												counter += 1
											end
										end
									end
							end #sheets.size > 0
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
				return
			end
		end
	end
end