# encoding: UTF-8

module AMTF
	module BUKONG
		module AmtfMath
			def self.between?(a, b, c, on_point = true)
				return false unless c.on_line?([a, b])
				v1 = c.vector_to(a)
				v2 = c.vector_to(b)
				if on_point
					return true if !v1.valid? || !v2.valid?
				else
					return false if !v1.valid? || !v2.valid?
				end
				!v1.samedirection?(v2)
			end
			def self.planar_points?(points)
				points = Utilities.uniq_points(points)
				return false if points.size < 3
				plane = Geom.fit_plane_to_points(points)
				points.all? { |pt| pt.on_plane?(plane) }
			end
			def self.normalize_plane(plane)
				return plane if plane.length == 2
				a, b, c, d = plane
				v = Geom::Vector3d.new(a, b, c)
				p = ORIGIN.offset(v.reverse, d)
				return p, v
			end
			def self.plane_context_convert(plane, tr1, tr2)
				plane1 = normalize_plane(plane)
				tot_trans = Utilities.transform_context_convert(tr1, tr2)
				return [
					plane1[0].transform(tot_trans),
					plane1[1].transform(tot_trans)
				]
			end
			def self.line_context_convert(line, tr1, tr2)
				tot_trans = Utilities.transform_context_convert(tr1, tr2)
				return [
					line[0].transform(tot_trans),
					line[1].transform(tot_trans)
				]
			end
			def self.grep_points(egdes)
				points = []
				egdes.each do |e|
					points << e.start.position
					points << e.end.position
				end
				points = Utilities.uniq_points(points)
				points = Utilities.get_sorted_points_ccw(points)
			end
			def self.arbitrary_non_parallel_vector(vector)
				vector.parallel?(Z_AXIS) ? X_AXIS : Z_AXIS
			end
			# Find an arbitrary unit vector that is perpendicular to given vector.
			#
			# @param [Geom::Vector3d]
			# @return [Geom::Vector3d]
			def self.arbitrary_perpendicular_vector(vector)
				(vector * arbitrary_non_parallel_vector(vector)).normalize
			end
			def self.plane_normal(plane)
				return plane[1].normalize if plane.size == 2
				a, b, c, _ = plane
				Geom::Vector3d.new(a, b, c).normalize
			end
			# Compute the area scale factor from transformation at a plane.
			#
			# @param [Array(Geom::Point3d, Geom::Vector3d), Array(Float, Float, Float, Float)]
			# @return [Float]
			def self.scale_factor_in_plane(plane, transformation)
				normal = plane_normal(plane)
				plane_vector0 = arbitrary_perpendicular_vector(normal)
				plane_vector1 = plane_vector0 * normal
				(
					plane_vector0.transform(transformation) *
						plane_vector1.transform(transformation)
				).length.to_f
			end
			def self.face_area(entity, transformation)
				area =
					entity.area *
						scale_factor_in_plane(entity.plane, transformation)
			end
			def self.thickness(ent, tr)
				faces = Utilities.definition(ent).entities.grep(Sketchup::Face)
				faces = faces.sort_by { |f| AmtfMath.face_area(f, tr) }
				faces = [faces[faces.size - 1], faces[faces.size - 2]]
				p1 = faces[0].vertices[0].position
				p2 = p1.project_to_plane(faces[1].plane)
				p1 = Utilities.point_context_convert(p1, tr, IDENTITY)
				p2 = Utilities.point_context_convert(p2, tr, IDENTITY)
				return p1.distance(p2)
			end
			def self.get_font
				# saver = AMTF::BUKONG::AmtfSaver.new
				saver = AmtfSaver.new
				config = saver.open_config
				return '楷体' if !config
				# return '楷体' 
				config=
				{
					origin: "边界中心点",
					标签长: "标签长",
				}
				puts "config"
				puts config
				# config = JSON.parse(config)
				config = JSON.parse(config)
				# amtf.nil
				general_setting = config['general_setting']
				return unless general_setting
				return unless general_setting[0]
				params1 = general_setting[0]['params1']
				return unless params1
				font = params1.select { |o| o['key'] == 'font_setting' }
				return unless font.size > 0
				return font[0]['value']
			end
			def self.get_parent_group(entity)
				parent_group = entity.parent.instances[-1] if entity &&
					entity.parent.is_a?(Sketchup::ComponentDefinition) &&
					entity.parent.group?
			end
			def self.get_material(entity)
				materials = []
				materials.push(entity.material) if entity.material
				condition = true
				while condition
					papa = entity.parent
					if papa.is_a?(Sketchup::Model)
						condition = false
					else
						parent_group = get_parent_group(entity)
						comp_type =
							parent_group.get_attribute 'amtf_dict', 'comp_type'
						if (
								comp_type == 'AmtfFrame' ||
									comp_type == 'AmtfSubFrame'
						   )
							if parent_group.material && parent_group.material
								materials.push(parent_group.material)
							end
						end
						entity = parent_group
					end
				end
				return materials[0] if materials.size > 0
				nil
			end
			def self.get_line(points)
				return points[0], points[0].vector_to(points[1]).normalize
			end
		end
	end

end