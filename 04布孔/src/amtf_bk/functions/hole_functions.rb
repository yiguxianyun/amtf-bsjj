# encoding: UTF-8
module AMTF
	module BUKONG
		module Hole_functions
			def self.fixed_solid(comp, trans)
				return if Utilities.solid?(comp)
				bigfaces = Utilities.get_big_faces(comp, trans)
				p1 = bigfaces[0].vertices[0].position
				p2 = p1.project_to_plane(bigfaces[1].plane)
				distance = p1.distance(p2)
				normal = p1.vector_to(p2).normalize
				oloop = bigfaces[0].outer_loop
				outer_points = []
				oloop.vertices.each { |v| outer_points << v.position }
				loop_0 = bigfaces[0].loops
				inloop_0 = loop_0.select { |lp| !lp.outer? }
				innerPoints = []
				inloop_0.each do |l|
					point1 = []
					l.vertices.each { |v| point1 << v.position }
					innerPoints << point1
				end
				comp.entities.each do |e|
					if e.is_a?(Sketchup::Face) || e.is_a?(Sketchup::Edge)
						e.erase!
					end
				end
				face =
					comp.entities.add_face(Utilities.uniq_points(outer_points))
				innerPoints.each do |point1|
					comp.entities.add_face(Utilities.uniq_points(point1)).erase!
				end
				face.reverse! if face.normal.dot(normal) < 0
				face.pushpull(distance)
			end
			def self.repair_face
				selection = Sketchup.active_model.selection
				selection.each { |ent| fixed_solid(ent, ent.transformation) }
			end
			def self.show_sheet
				selection = Sketchup.active_model.selection
				query = AmtfQuery.new
				板件obj = query.query_sheets(selection, false)
				板件obj.each do |ent|
					ent.comp.visible = true if !ent.comp.visible?
				end
				selection.each do |entity|
					next unless Utilities.instance?(entity)
					comp_type = entity.get_attribute 'amtf_dict', 'comp_type'
					Utilities
						.definition(entity)
						.entities
						.each do |ent|
							if ent.is_a?(Sketchup::Group)
								if !ent.visible?
									node_id =
										ent.get_attribute 'amtf_dict', 'node_id'
									sheet_id =
										ent.get_attribute 'amtf_dict',
										                  'sheet_id'
									ent.visible = true if node_id || sheet_id
								end
							end
						end
				end
			end
			
			def self.打标签
				query = AmtfQuery.new
				板件objs = query.query_sheets(Sketchup.active_model.entities)
				puts "板件objs.size 👉 #{板件objs.size}" 
				return unless 板件objs.size > 0
				有标签组件s = [] # erase
				无标签组件s = [] #not erase
				for i in 0..板件objs.size - 1
					begin
						# Lấy ra mảng các phần t�?có nhãn
						板件obj = 板件objs[i]
						pick_face = 板件obj.big_faces[0]
						标签=板件obj.获取标签
						found = false
						if 标签.nil?
							# result = UI.messagebox("没找到标签对象！")
							# return
							无标签组件s << { 板件obj: 板件obj, pick_face: pick_face }
						else
							有标签组件s << {板件obj: 板件obj,sub: 标签,pick_face: pick_face}
							found = true
						end
						# subGroup =Utilities.definition(板件obj.comp).entities.grep(Sketchup::Group)
						# if subGroup.size > 0
						# 	found = false
						# 	for k in 0..subGroup.size - 1
						# 		group_type =subGroup[k].get_attribute('amtf_dict','type')
						# 		if group_type == 'bukong_label'
						# 			center = subGroup[k].bounds.center
						# 			pick_face = 板件obj.big_faces[1] if Utilities
						# 				.within_face?(center, 板件obj.big_faces[1])
						# 			有标签组件s << {
						# 				板件obj: 板件obj,
						# 				sub: subGroup[k],
						# 				pick_face: pick_face
						# 			}
						# 			found = true
						# 		end
						# 	end
						# 	if found
						# 	else
						# 		无标签组件s << { 板件obj: 板件obj, pick_face: pick_face }
						# 	end
						# else #没有子组件
						# 	无标签组件s << { 板件obj: 板件obj, pick_face: pick_face }
						# end
					rescue => error
						puts error
					end
				end #遍历完每块板 零件
				
				pb =ExtraFuntion::ProgressBar.new(有标签组件s.size + 1,OB[:label_remove])
				for i in 0..有标签组件s.size - 1
					无标签组件s << {
						板件obj: 有标签组件s[i][:板件obj],
						pick_face: 有标签组件s[i][:pick_face]
					}
					有标签组件s[i][:sub].erase!
				end
				draw_name(无标签组件s)
			end
			
			def self.draw_name(arr)
				# puts "e 👉 #{arr[0]}" 
				# amtf.nil
				i = 0
				pb = ExtraFuntion::ProgressBar.new(arr.size, OB[:打标签])
				timer =
					UI.start_timer(0.000005, true) do
						if i == arr.size
							UI.stop_timer(timer)
							Sketchup.set_status_text(OB[:completed])
						end
						if i < arr.size
							begin
								pick_face = arr[i][:pick_face]
								板件obj = arr[i][:板件obj]
								板件obj.comp.set_attribute 'amtf_dict',
								                        'amtf_bukong_id',
								                        i
								板件obj.add_id_to_name(i)
								板件obj.drawArrow(pick_face)
							rescue => exception
								puts exception
							end
						end
						pb.update(i)
						i += 1
					end
			end
			
			def self.labelRemove
				entities = Sketchup.active_model.entities
				query = AmtfQuery.new
				板件objs = query.query_sheets(entities)
				return unless 板件objs.size > 0
				Sketchup.active_model.start_operation OB[:label_remove], true
				pb =
					ExtraFuntion::ProgressBar.new(板件objs.size, OB[:label_remove])
				begin
					for i in 0..板件objs.size - 1
						sheet = 板件objs[i].comp
						subGroup =
							Utilities
								.definition(sheet)
								.entities
								.grep(Sketchup::Group)
						subGroup.each do |grp|
							group_type = grp.get_attribute('amtf_dict', 'type')
							grp.erase! if group_type == 'bukong_label'
						end
					end
				rescue => error
					puts error
				end
				Sketchup.active_model.commit_operation
				Sketchup.set_status_text(OB[:completed])
			end
		end #Hole_functions
	end #BuKong
end #Amtflogic