# encoding: UTF-8
module AMTF
    module BUKONG
      module BuKongFunction
        def self.command_bukong 
            screen_width = 1550;
            screen_height = 838;
            material_thick = 17;
            door_safeGap = 0.8;
            door_gapLeft = 0;
            door_gapRight = 0;
            door_gapTop = 0;
            door_gapBottom = 0;
            @sheets_part = {}
            options = {
                :dialog_title => "ONECLICK Cabinet",
                :preferences_key => "bukongcabinet.net",
                :scrollable => true,
                :resizable => false,
                :width => screen_width,
                :height =>screen_height,
                :left => 0,
                :top => 0,
                :min_width => 50,
                :min_height => 50,
                :max_width =>1920,
                :max_height => 1080,
                :style => UI::HtmlDialog::STYLE_DIALOG
            }  
            khoanmoiData = Sketchup.read_default('AmtflogicBuKong', 'khoanmoiData' , '') 
            chotdotData = Sketchup.read_default('AmtflogicBuKong', 'chotdotData' , '')
            patVData = Sketchup.read_default('AmtflogicBuKong', 'patVData' , '')
            rafixData = Sketchup.read_default('AmtflogicBuKong', 'rafixData' , '')
            lockdowelData = Sketchup.read_default('AmtflogicBuKong', 'lockdowelData' , '');
            hingeData = Sketchup.read_default('AmtflogicBuKong', 'hingeData' , nil)
            data = {
              :language => OB.saved_lang,      
              :unit => Utilities.get_model_units(),
              :screen_width => screen_width,
              :screen_height => screen_height,
              :material_thick => material_thick,
              :door_parameters =>
              {
                :door_safeGap => door_safeGap,
                :door_gapLeft => door_gapLeft,
                :door_gapRight => door_gapRight,
                :door_gapTop => door_gapTop,
                :door_gapBottom => door_gapBottom
              },                     
              :khoanmoiData => ConnectorOptions.convert_khoanMoiData( khoanmoiData ),
              :chotdotData => ConnectorOptions.convert_chotdotData( chotdotData ),
              :patVData => ConnectorOptions.convert_patVData( patVData ),
              :rafixData => ConnectorOptions.convert_rafixData( rafixData ),
              :lockdowelData => ConnectorOptions.convert_lockdowelData( lockdowelData ),
              :hingeData => HingeOptions.convert_hingeData(hingeData),
              :camChotData => [
                {
                  :vi_tri_oc_cam => Sketchup.read_default('AmtflogicBuKong', 'vi_tri_oc_cam' , 50),
                  :khoang_cach_cam_chot => Sketchup.read_default('AmtflogicBuKong', 'khoang_cach_cam_chot' , 32),
                  :khoang_chia_lo_cam_max => Sketchup.read_default('AmtflogicBuKong', 'khoang_chia_lo_cam_max' , 1000),
                  :khoang_chia_lo_cam_min => Sketchup.read_default('AmtflogicBuKong', 'khoang_chia_lo_cam_min' , 90),
                  :do_sau_cam =>  Sketchup.read_default('AmtflogicBuKong', 'do_sau_cam' , 9),
                  :do_dai_cam => Sketchup.read_default('AmtflogicBuKong', 'do_dai_cam' , 34),
                  :duong_kinh_cam => Sketchup.read_default('AmtflogicBuKong', 'duong_kinh_cam' , 15),
                  :mini_fix_big_diameter => Sketchup.read_default('AmtflogicBuKong', 'mini_fix_big_diameter' , 10),
                  :mini_fix_small_diameter => Sketchup.read_default('AmtflogicBuKong', 'mini_fix_small_diameter' , 8),
                  :no_dowel => Utilities.get_option('no_dowel'),
                }
              ]
            } 
            url = "#{ServerOptions.get_server_url}:#{DRAW_PORT}/design?data=#{data.to_json}"
            dialog = UI::HtmlDialog.new(options)
            dialog.set_size(options[:width], options[:height]) # Ensure size is set.
            dialog.set_url(url)
            dialog.show()
            dialog.add_action_callback("confirm_good_connect") { 
              |action_context|
              dialog.set_can_close { 
                js_command = "document.getElementById('CloseWidow').click()"
                dialog.execute_script(js_command)
                false
              }
            }
            Bridge.decorate(dialog)            
            dialog.on("close_window"){
              | deferred |
              dialog.set_can_close { true }
              dialog.close()
            }
            dialog.on("setsize"){
              |deferred|
              dialog.set_size(200, 800)
            }
            dialog.on("save_data"){
              |deferred, data|
              begin
                model = Sketchup.active_model                
                file_name = model.get_attribute('Amtflogic', 'BuKongFile', nil)
                if( !file_name || file_name.empty? )                
                  file_name = Utilities.CreatFileName()
                  file_name.gsub('.','')
                  file_name = "/working/#{file_name}.json"
                end
                file_full_name = File.join(PLUGIN_DIR, file_name )
                model.set_attribute('Amtflogic', 'BuKongFile', file_name )
                File.open( file_full_name, 'w') { 
                  |f|
                  f.write( JSON.generate( data ) )
                  f.close()
                }
                deferred.resolve(true )
              rescue => error
                Utilities.print_exception(error, false)
                Sketchup.set_status_text(error)
                deferred.resolve(false)  
              end
            }    
            dialog.on("open_file"){
              | deferred |
              begin
                model = Sketchup.active_model                
                file_name = model.get_attribute('Amtflogic', 'BuKongFile', nil)
                if( file_name )  
                  file_full_name = File.join(PLUGIN_DIR, file_name ) 
                  if( File.exists?( file_full_name ) )                
                    file_data = ''
                    open( file_full_name, 'r') { 
                      |f|
                      file_data = f.read
                      f.close()
                    }
                    data_hash = JSON.parse(file_data)
                    deferred.resolve(data_hash )
                  else
                    deferred.resolve('No file' )
                  end
                else
                  deferred.resolve('No file' )
                end
              rescue => error
                Utilities.print_exception(error, false)
                Sketchup.set_status_text(error)
                deferred.resolve(false)  
              end
            }    
            dialog.on("send_model_data"){
              |deferred, data|
              begin
                model_data = JSON.parse( data.to_json )
                Sketchup.active_model.selection.clear
                noneFrame = JSON.parse( model_data['noneFrame'].to_json )
                sheet_id_map = {} 
                temp_sheet_part = {}               
                new_connector_list = []
                pb = ExtraFuntion::ProgressBar.new(noneFrame.size, OB[:in_progress])
                i = 0         
                timer = UI.start_timer(0.0000000000005,true){
                    if i == noneFrame.size
                        UI.stop_timer(timer)   
                        @sheets_part = temp_sheet_part
                        deferred.resolve( true ) 
                    end 
                    if i < noneFrame.size
                        begin                          
                            sheet_persistent_id = noneFrame[i]['sheet_id']
                            raw_data = noneFrame[ i ]['sheet_data']
                            sheet_name = noneFrame[ i ]['sheet_name']
                            sheet_type = noneFrame[ i ]['sheet_type']
                            no_hole =  noneFrame[ i ]['no_hole']
                            face_sheet_data = raw_data['faces']
                            paths_sheet_data = raw_data['paths']
                            group = Sketchup.active_model.active_entities.add_group();
                            Sketchup.active_model.selection.add( group )
                            temp_sheet_part[ group.persistent_id ] = SheetPart.new group, group.transformation, IDENTITY, group.persistent_id
                            group.set_attribute 'amtf_dict', 'amtflogic_exploded', 'true'
                            group.set_attribute 'amtf_dict', 'comp_type', 'no_hole' if no_hole
                            group.set_attribute 'amtf_dict', 'comp_type', 'back' if sheet_type === 'BOTTOM_DRAWER'
                            group.name = sheet_name if sheet_name
                            # Tạo các path của door
                            paths_sheet_data.each{
                              |key, path |
                              ptr_arr = path['polyline']
                              temp_arr = []
                              for j in 0..ptr_arr.size - 1    
                                p = Geom::Point3d.new(Utilities.unitConvert( ptr_arr[j]['x']), 
                                                      Utilities.unitConvert( ptr_arr[j]['y']), 
                                                      Utilities.unitConvert( ptr_arr[j]['z']))  
                                ptr = Utilities.point_context_convert( p, IDENTITY, group.transformation)                    
                                temp_arr << ptr
                              end
                              curve_group = group.entities.add_group 
                              temp_arr = temp_arr.map{|pt| pt = pt.transform( curve_group.transformation.inverse ) }
                              for j in 0..temp_arr.size - 2
                                curve_group.entities.add_line temp_arr[j], temp_arr[j+1]
                              end
                              curve_group.set_attribute 'amtf_dict', 'type', 'curve_groove' 
                              curve_group.set_attribute 'amtf_dict', 'closed', path['closed']
                              curve_group.set_attribute 'amtf_dict', 'minifixID', Utilities.CreatUniqueid();
                            }
                            # sheet_id_map đ�?đánh lại ID cho các connector
                            sheet_id_map[ sheet_persistent_id ] = group.persistent_id;                                          
                            if @sheets_part[ sheet_persistent_id ] && 
                              @sheets_part[ sheet_persistent_id ].comp.valid? &&   
                              !@sheets_part[ sheet_persistent_id ].comp.deleted?
                              group.material = @sheets_part[ sheet_persistent_id ].comp.material
                              @sheets_part[ sheet_persistent_id ].comp.erase! 
                              @sheets_part.delete( sheet_persistent_id )
                            end
                            face_sheet_data.each{
                              |face_id, f|
                              innerloop = f['innerloop']
                              outerloop = f['outerloop']
                              connector = f['connector']
                              points = []   
                                outerloop.each do |point|               
                                  points.push( 
                                    Geom::Point3d.new(Utilities.unitConvert( point['x']), Utilities.unitConvert( point['y']), Utilities.unitConvert( point['z'])))
                                end
                              face = group.entities.add_face( Utilities.uniq_points(points))
                              innerloop.each {
                                |l|
                                point1 = []
                                l.each do |point|               
                                  point1.push( 
                                    Geom::Point3d.new(Utilities.unitConvert( point['x']), Utilities.unitConvert( point['y']), Utilities.unitConvert( point['z'])))
                                end
                                group.entities.add_face( Utilities.uniq_points(point1)).erase!
                              }
                              connector.each {
                                |id, con |
                                next if con.empty?()
                                if con["connect_type"] === 'chanel'
                                  points = con['outerloop'].map{ 
                                    |point| 
                                    the_trans = group.transformation
                                    point = Geom::Point3d.new   Utilities.unitConvert( point['x'] ), 
                                                                Utilities.unitConvert( point['y'] ), 
                                                                Utilities.unitConvert( point['z'] )
                                    Utilities.point_context_convert(point, IDENTITY, the_trans) 
                                  }            
                                  chanel_grp = group.entities.add_group
                                  group.set_attribute('amtf_dict', 'has_groove', 'yes')
                                  chanel_type = con["chanel_type"];
                                  groove_type_name = con["groove_name"]
                                  groove_type_id = con[ "connector_type_id"]
                                  minifixID = con['minifixID']
                                  if( !groove_type_name )
                                    default = GroooveOptions.get_default
                                    default = GroooveOptions.groove_attrs[0] if default.nil?
                                    groove_type_id = default[:groove_type_id]
                                    groove_type_name = default[:groove_type_name]
                                    chanel_type = 'chanel'
                                  end
                                  if !minifixID 
                                    minifixID = Utilities.CreatUniqueid()
                                  end
                                  chanel_face = chanel_grp.entities.add_face(points)
                                  chanel_grp.set_attribute 'amtf_dict', 'type', chanel_type
                                  chanel_grp.set_attribute 'amtf_dict', 'groove_type_id', groove_type_id
                                  chanel_grp.set_attribute 'amtf_dict', 'minifixID', minifixID
                                  chanel_grp.set_attribute 'amtf_dict', 'groove_name', groove_type_name
                                elsif  con["connect_type"] === 'oc_cam' ||
                                    con["connect_type"] === 'chot_cam' ||
                                    con["connect_type"] === 'chot_go'
                                    center = Geom::Point3d.new Utilities.unitConvert(con['center']['x']), 
                                                                  Utilities.unitConvert(con['center']['y']), 
                                                                  Utilities.unitConvert(con['center']['z'])
                                    data_oc_cam = {
                                        :center => center,
                                        :radius => Utilities.unitConvert(con['radius']),
                                        :minifixID => con['minifixID'],
                                        :partner_sheet_id => con['partner_sheet_id'],
                                        :type => con["connect_type"],
                                        :connector_type => OB[:oc_cam]
                                    }
                                    oc_cam = AmtfCircle.new(nil, nil, nil)
                                    oc_cam.setData(data_oc_cam)
                                    oc_cam.drawCircle(group, face)
                                    new_connector_list.push( oc_cam.instance )
                                elsif con["connect_type"] === 'chan_rafix' ||
                                      con["connect_type"] === 'oc_rafix' ||
                                      con["connect_type"] === 'chan_rafixD2'
                                      connector_id = con[ 'connector_type_id' ];
                                      config_data =  Sketchup.read_default('AmtflogicBuKong', 'rafixData' , '')
                                      config_data = ConnectorOptions.get_rafix_params_by_id( connector_id , config_data)
                                    center = Geom::Point3d.new Utilities.unitConvert(con['center']['x']), 
                                                                  Utilities.unitConvert(con['center']['y']), 
                                                                  Utilities.unitConvert(con['center']['z'])
                                    rafix_type = 'rafix_D'
                                    radius = 0.5*config_data['rafix_diameter'].to_l
                                    if con["connect_type"] === 'oc_rafix' 
                                      rafix_type = 'rafix_D'
                                      radius = 0.5*config_data['rafix_diameter'].to_l
                                    elsif con["connect_type"] === 'chan_rafix' 
                                      rafix_type = 'rafix_D1'
                                      radius = 0.5*config_data['rafix_diameter1'].to_l
                                    elsif con["connect_type"] === 'chan_rafixD2' 
                                      rafix_type = 'rafix_D2'
                                      radius = 0.5*config_data['rafix_diameter2'].to_l
                                    end
                                    data_oc_cam = {
                                          :center => center,
                                          :radius => radius,
                                          :minifixID => con['minifixID'],
                                          :type => rafix_type, 
                                          :type_id => connector_id,
                                          :connector_type => config_data["rafix_type_name"],
                                          :partner_sheet_id => con['partner_sheet_id']
                                      }
                                      oc_cam = AmtfCircle.new(nil, nil, nil)
                                      oc_cam.setData(data_oc_cam)
                                      oc_cam.drawCircle(group, face)
                                      new_connector_list.push( oc_cam.instance )
                                elsif con["connect_type"] === 'lockdowel'
                                  center_1 = Geom::Point3d.new Utilities.unitConvert(con['center_1']['x']), 
                                                                  Utilities.unitConvert(con['center_1']['y']), 
                                                                  Utilities.unitConvert(con['center_1']['z'])
                                  center_2 = Geom::Point3d.new Utilities.unitConvert(con['center_2']['x']), 
                                                                  Utilities.unitConvert(con['center_2']['y']), 
                                                                  Utilities.unitConvert(con['center_2']['z'])
                                  center_3 = Geom::Point3d.new Utilities.unitConvert(con['center_3']['x']), 
                                                                  Utilities.unitConvert(con['center_3']['y']), 
                                                                  Utilities.unitConvert(con['center_3']['z'])
                                  connector_id = con[ 'connector_type_id' ];
                                  config_data = Sketchup.read_default('AmtflogicBuKong', 'lockdowelData', '')                    
                                  config_data = ConnectorOptions.get_lockdowel_params_by_id(connector_id, config_data)
                                  data_lockdowel = {
                                      :center_1 => center_1,
                                      :center_2 => center_2,
                                      :center_3 => center_3,
                                      :radius => 4.mm,    
                                      :minifixID => con['minifixID'],
                                      :type => 'lockdowel',
                                      :connector_type => config_data["lockdowel_type_name"],
                                      :type_id => connector_id,
                                      :partner_sheet_id => con['partner_sheet_id']
                                  }
                                  lockdowel = AmtfLockdowel.new(data_lockdowel)
                                  lockdowel.drawCircle(group, face)
                                  new_connector_list.push( lockdowel.instance )
                                elsif  con["connect_type"] === 'khoan_moi'
                                    connector_id = con[ 'connector_type_id' ];
                                    config_data =  Sketchup.read_default('AmtflogicBuKong', 'rafixData' , '')
                                    config_data = ConnectorOptions.get_rafix_params_by_id( connector_id , config_data)
                                    center = Geom::Point3d.new Utilities.unitConvert(con['center']['x']), 
                                                                  Utilities.unitConvert(con['center']['y']), 
                                                                  Utilities.unitConvert(con['center']['z'])
                                    data_oc_cam = {
                                        :center => center,
                                        :radius => Utilities.unitConvert(con['radius']),
                                        :minifixID => con['minifixID'],
                                        :partner_sheet_id => con['partner_sheet_id'],
                                        :type => con["connect_type"],
                                        :type_id => connector_id,
                                        :connector_type => config_data['khoanmoi_type_name']
                                    }
                                    oc_cam = AmtfCircle.new(nil, nil, nil)
                                    oc_cam.setData(data_oc_cam)
                                    oc_cam.drawCircle(group, face)
                                    new_connector_list.push( oc_cam.instance )
                                  elsif  con["connect_type"] === 'chot_dot'
                                    connector_id = con[ 'connector_type_id' ];
                                    config_data =  Sketchup.read_default('AmtflogicBuKong', 'chotdotData' , '')
                                    config_data = ConnectorOptions.get_chot_dot_params_by_id( connector_id, config_data)
                                    center = Geom::Point3d.new Utilities.unitConvert(con['center']['x']), 
                                                                  Utilities.unitConvert(con['center']['y']), 
                                                                  Utilities.unitConvert(con['center']['z'])
                                    data_oc_cam = {
                                        :center => center,
                                        :radius => Utilities.unitConvert(con['radius']),
                                        :minifixID => con['minifixID'],
                                        :partner_sheet_id => con['partner_sheet_id'],
                                        :type => con["connect_type"],
                                        :type_id => connector_id,
                                        :connector_type => config_data['chotdot_type_name']
                                    }
                                    oc_cam = AmtfCircle.new(nil, nil, nil)
                                    oc_cam.setData(data_oc_cam)
                                    oc_cam.drawCircle(group, face)
                                    new_connector_list.push( oc_cam.instance )
                                  elsif  con["connect_type"] === 'patV'
                                    connector_id = con[ 'connector_type_id' ];
                                    config_data =  Sketchup.read_default('AmtflogicBuKong', 'patVData' , '')
                                    config_data = ConnectorOptions.get_patV_params_by_id( connector_id, config_data)
                                    center = Geom::Point3d.new Utilities.unitConvert(con['center']['x']), 
                                                                  Utilities.unitConvert(con['center']['y']), 
                                                                  Utilities.unitConvert(con['center']['z'])
                                    data_oc_cam = {
                                        :center => center,
                                        :radius => Utilities.unitConvert(con['radius']),
                                        :minifixID => con['minifixID'],
                                        :partner_sheet_id => con['partner_sheet_id'],
                                        :type => con["connect_type"],
                                        :type_id => connector_id,
                                        :connector_type => config_data["patV_type_name"],
                                    }
                                    oc_cam = AmtfCircle.new(nil, nil, nil)
                                    oc_cam.setData(data_oc_cam)
                                    oc_cam.drawCircle(group, face)
                                    new_connector_list.push( oc_cam.instance )
                                  elsif  con["connect_type"] === 'hinge'
                                    connector_id = con[ 'connector_type_id' ];
                                    hinge_data = HingeOptions.get_hinge_params(connector_id)
                                    hinge_type_name = hinge_data["hinge_type_name"]
                                    hinge_M = hinge_data['hinge_M'].to_l
                                    hinge_d = hinge_data['hinge_diameter'].to_l
                                    hinge_d1 = hinge_data['hinge_diameter1'].to_l
                                    hinge_L2 = 0.5*hinge_data['hinge_L2'].to_l
                                    hinge_L3 = hinge_data['hinge_L3'].to_l
                                    hinge_r = hinge_d/2
                                    hinge_r1 = hinge_d1/2
                                    center = Geom::Point3d.new Utilities.unitConvert(con['center']['x']), 
                                                                  Utilities.unitConvert(con['center']['y']), 
                                                                  Utilities.unitConvert(con['center']['z'])
                                    lo_35 = group.entities.add_group
                                    lo_35.set_attribute 'amtf_dict', 'type', 'hinge'
                                    lo_35.set_attribute 'amtf_dict', 'minifixID', con['minifixID'] 
                                    lo_35.set_attribute 'amtf_dict', 'hinge_type', hinge_type_name 
                                    lo_35.set_attribute 'amtf_dict', 'radius', hinge_r 
                                    big_center = center.transform(lo_35.transformation)
                                    lo_35.entities.add_circle(big_center , face.normal, hinge_r.to_l)
                                  elsif  con["connect_type"] === 's_hinge'
                                    center = Geom::Point3d.new Utilities.unitConvert(con['center']['x']), 
                                                                Utilities.unitConvert(con['center']['y']), 
                                                                Utilities.unitConvert(con['center']['z'])
                                    connector_id = con[ 'connector_type_id' ];
                                    hinge_data = HingeOptions.get_hinge_params(connector_id)
                                    hinge_type_name = hinge_data["hinge_type_name"]
                                    hinge_M = hinge_data['hinge_M'].to_l
                                    hinge_d = hinge_data['hinge_diameter'].to_l
                                    hinge_d1 = hinge_data['hinge_diameter1'].to_l
                                    hinge_L2 = 0.5*hinge_data['hinge_L2'].to_l
                                    hinge_L3 = hinge_data['hinge_L3'].to_l
                                    hinge_r = hinge_d/2
                                    hinge_r1 = hinge_d1/2
                                    lo_vit1 =  group.entities.add_group
                                    lo_vit1.set_attribute 'amtf_dict', 'type', 's_hinge'
                                    lo_vit1.set_attribute 'amtf_dict', 'minifixID', con['minifixID']  
                                    lo_vit1.set_attribute 'amtf_dict', 'hinge_type', hinge_type_name 
                                    lo_vit1.set_attribute 'amtf_dict', 'radius', hinge_r1 
                                    vit1 = center.transform(lo_vit1.transformation)
                                    lo_vit1.entities.add_circle(vit1 , face.normal, hinge_r1.to_l)
                                  end
                              }      
                            }
                            @sheets_part.each do 
                              |key, part |
                              part.comp.erase! if part.comp.valid?
                              @sheets_part.delete( key );
                            end
                            new_connector_list.each {
                              |con|
                              partner_sheet_id = Utilities.get_comp_attribute( con,'partner_sheet_id', 'amtf_dict' )
                              if(sheet_id_map.key?(partner_sheet_id))
                                con.set_attribute 'amtf_dict', 'partner_sheet_id', sheet_id_map[ partner_sheet_id ]
                              end
                            }
                            i +=1
                            pb.update(i)   
                        rescue => error      
                            i +=1                             
                            Utilities.print_exception(error, false)   
                        end
                    end
                }
              rescue => error
                Utilities.print_exception(error, false)
                Sketchup.set_status_text(error)
                deferred.resolve(false)  
              end
            }
            dialog.on("get_model_data"){
              |deferred|
              begin              
                deferred.resolve( generateData())
              rescue => error
                Utilities.print_exception(error, false)
                Sketchup.set_status_text(error)
                deferred.resolve(false)  
              end
            }    
        end   
        def self.generateData
            selection = Sketchup.active_model.selection   
            @sheets_part = Hash.new
            sheets = []
            Utilities.traverseArray( selection, sheets )
            minifixList = Utilities.buildMinifixList(selection)
            object = {}    
            colors = []
            sheets.each do
              |sheet|
              connector = {}
              children = sheet.entities.grep(Sketchup::Group)
              children.each {
                | mini|
                if minifixList.key?( mini.persistent_id.to_s )
                  partner_sheet_id = mini.get_attribute('amtf_dict', 'partner_sheet_id')
                  minifixID = mini.get_attribute('amtf_dict', 'minifixID')
                  minifixType = mini.get_attribute('amtf_dict', 'type')
                  miniTrans = sheet.transformation*mini.transformation
                  if minifixType === 'oc_cam' || minifixType === 'chot_cam' || minifixType === 'chot_go' ||
                    minifixType === 'rafix_D' || minifixType === 'rafix_D1' || minifixType === 'rafix_D2' ||
                    minifixType === 'khoan_moi' || minifixType === 'chot_dot'  || minifixType === 'patV'
                    center = Utilities.point_context_convert( Utilities.find_centroid(mini), miniTrans, sheet.transformation )
                    connector[ mini.persistent_id ] = {
                      :minifixType => minifixType,
                      :center =>  Utilities.point_context_convert( center, sheet.transformation, IDENTITY),
                      :partner_sheet_id => partner_sheet_id,
                      :minifixID => minifixID,
                      :miniID => mini.persistent_id,
                      :center_origin => center,
                      :type_id => mini.get_attribute('amtf_dict', 'type_id'),
                      :sheet_id => sheet.comp.persistent_id
                    }
                  elsif minifixType === 'lockdowel'
                      cirlces = mini.entities.grep(Sketchup::Group)                        
                      ctr1 = nil
                      ctr2 = nil
                      ctr3 = nil
                      cirlces.each{
                          |cir|
                          ctr = Utilities.find_centroid(cir)
                          ctr = Utilities.point_context_convert(ctr, miniTrans*cir.transformation, sheet.transformation )
                          cirlce_type = cir.get_attribute('amtf_dict', 'type')                          
                          ctr1 = ctr  if cirlce_type == 'cirlce_1' 
                          ctr2 = ctr  if cirlce_type == 'cirlce_2' 
                          ctr3 = ctr  if cirlce_type == 'cirlce_3' 
                      }
                      connector[ mini.persistent_id ] = {
                        :minifixType => minifixType,
                        :center_1 =>  Utilities.point_context_convert( ctr1, sheet.transformation, IDENTITY),
                        :center_2 =>  Utilities.point_context_convert( ctr2, sheet.transformation, IDENTITY),
                        :center_3 =>  Utilities.point_context_convert( ctr3, sheet.transformation, IDENTITY),
                        :partner_sheet_id => partner_sheet_id,
                        :minifixID => minifixID,
                        :miniID => mini.persistent_id,
                        :center1_origin => ctr1,
                        :center2_origin => ctr2,
                        :center3_origin => ctr3,
                        :type_id => mini.get_attribute('amtf_dict', 'type_id'),
                        :sheet_id => sheet.comp.persistent_id
                      }
                    elsif minifixType == 'chanel' || minifixType == 'custom_groove'
                        face = mini.entities.grep(Sketchup::Face)                     
                        oloop = face[0].outer_loop                        
                        points = []      
                        oloop.vertices.each{ |v| points.push(v.position) }      
                        shape = points.map{ |point| Utilities.point_context_convert(point, miniTrans, IDENTITY ) } 
                        shape0 = Utilities.find_centroid( mini )
                        shape0 = Utilities.point_context_convert( shape0, miniTrans, sheet.transformation )
                        connector[ mini.persistent_id ] = {
                          :minifixType => minifixType,                         
                          :minifixID => minifixID,
                          :miniID => mini.persistent_id,
                          :type_id => mini.get_attribute('amtf_dict', 'groove_type_id'),
                          :type => mini.get_attribute('amtf_dict', 'type'),
                          :groove_name => mini.get_attribute('amtf_dict', 'groove_name'),
                          :sheet_id => sheet.comp.persistent_id,
                          :shape => shape,
                          :shape0 => shape0
                        }
                    end
                  end
              }
              faces = sheet.entities.grep(Sketchup::Face)   
              face_data = {}       
              faces.each do          
                |f|
                innerLoop = f.loops.select { |l| !l.outer? }
                inner_pots = []
                innerLoop.each {
                  |lo|
                  verts = lo.vertices.map { 
                    | vertex | 
                    pt = vertex.position
                    pt.transform!(sheet.transformation)
                  }
                  inner_pots.push(verts)
                }
                shape = f.outer_loop.vertices.map { 
                  | vertex | 
                  pt = vertex.position
                  pt.transform!(sheet.transformation)
                }
                face_connector = {}
                connector.each {
                  | id, con |
                  if  con[:minifixType] === 'oc_cam' || con[:minifixType] === 'chot_cam' || con[:minifixType] === 'chot_go' ||
                      con[:minifixType] === 'rafix_D' || con[:minifixType] === 'rafix_D1' || con[:minifixType] === 'rafix_D2' ||
                      con[:minifixType] === 'khoan_moi' || con[:minifixType] === 'chot_dot' || con[:minifixType] === 'patV'
                      if Utilities.within_face?(con[:center_origin], f)
                        face_connector[ id ]  = {
                          :face_id => f.persistent_id,
                          :center => con[ :center ],
                          :partner_sheet_id => con[:partner_sheet_id],
                          :minifixType => con[:minifixType],
                          :minifixID => con[:minifixID],
                          :miniID => con[ :miniID ],
                          :type_id => con[ :type_id ],
                          :sheet_id => con[ :sheet_id ]
                        }
                      end
                  elsif con[:minifixType] === 'lockdowel'
                    if Utilities.within_face?(con[:center1_origin], f)
                      face_connector[ id ]  = {
                        :face_id => f.persistent_id,
                        :center_1 => con[ :center_1 ],
                        :center_2 => con[ :center_2 ],
                        :center_3 => con[ :center_3 ],
                        :minifixType => con[:minifixType],
                        :minifixID => con[:minifixID],
                        :miniID => con[ :miniID ],
                        :type_id => con[ :type_id ],
                        :partner_sheet_id => con[:partner_sheet_id],
                        :sheet_id => con[ :sheet_id ]
                      }
                    end 
                  elsif con[:minifixType] === 'chanel' || con[:minifixType] == 'custom_groove'
                    if Utilities.within_face?(con[:shape0 ], f)
                      face_connector[ id ] = {
                          :face_id => f.persistent_id,
                          :shape => con[:shape ],
                          :minifixType => con[:minifixType],
                          :minifixID => con[:minifixID],
                          :miniID => con[ :miniID ],
                          :type_id => con[ :type_id ],
                          :sheet_id => con[ :sheet_id ],
                          :groove_name => con[:groove_name],
                          :type => con[:type]
                      }
                    end                    
                  end
                }
                face_data[ f.persistent_id ] = {
                  :shape => shape,
                  :holes => inner_pots,
                  :face_connector => face_connector
                }
              end
              colors.push(sheet.color)
              object[ sheet.sheet_persistent_id ] = {
                :faces => face_data,
                :color => sheet.color,
                :sheet_persistent_id => sheet.sheet_persistent_id,
                :comp_type => sheet.comp.get_attribute('amtf_dict', 'comp_type'),
                :sheet_name => sheet.comp.name
              }
              @sheets_part[ sheet.sheet_persistent_id.to_s ] = sheet
            end
            mesh_list = []
            Utilities.getSnapMesh( selection, mesh_list )
            mesh_edges = Hash.new
            mesh_list.each{
              |me|
              edgs = me[:entity].entities.grep(Sketchup::Edge )
              edges = Hash.new
              edgs.each{
                |e|
                p1 = e.start.position
                p2 = e.end.position
                p1.transform!(me[:trans])
                p2.transform!(me[:trans])
                edges[e.persistent_id] = { :p1 => p1, :p2 => p2 } 
              }
              mesh_edges[me[:entity].persistent_id] = edges
            }
            return { :colors => colors, :modelData => object, :mesh_edges => mesh_edges }.to_json()
        end
      end
    end
end