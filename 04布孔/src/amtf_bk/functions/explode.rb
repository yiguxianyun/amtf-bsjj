# encoding: UTF-8
module AMTF
	module BUKONG
		module Explode
			def self.command_explode
				selection = Sketchup.active_model.selection
				query = AmtfQuery.new
				sheets = query.query_sheets(selection, false)
				size = sheets.size
				i = 0
				done = false
				progress =
					ExtraFuntion::ProgressBar.new(
						size,
						OB[:exploding_inprogress]
					)
				Sketchup.active_model.start_operation 'explode', true
				timer =
					UI.start_timer(0.0001, true) do
						progress.update(i) if 100 * i / size < 96
						if i == size
							UI.stop_timer(timer)
							progress.update(size)
							Sketchup.set_status_text(OB[:explode_completed])
							materials = Sketchup.active_model.materials
							materials.purge_unused
							selection.to_a.each do |ent|
								ent.erase! if ent.valid?
							end
						else
							begin
								element = sheets[i].comp
								new_group =
									Sketchup.active_model.entities.add_group
								name = element.name
								tr =
									Utilities.transform_context_convert(
										sheets[i].trans,
										IDENTITY
									)
								inst =
									Utilities
										.definition(new_group)
										.entities
										.add_instance(
											Utilities.definition(element),
											tr
										)
								inst.explode
								new_group.name = name
								dict = element.attribute_dictionary 'amtf_dict'
								if dict
									dict.each do |key, value|
										new_group.set_attribute 'amtf_dict',
										                        key,
										                        value
									end
								end
								new_group.material = element.material
								comp_type =
									element.get_attribute 'amtf_dict',
									                      'comp_type'
								new_group.visible = true if comp_type ==
									'flating_curve'
							rescue => error
								puts error
								Sketchup.set_status_text('Error')
								UI.stop_timer(timer)
							end
						end
						i += 1
					end
				Sketchup.active_model.commit_operation
			end
		end
	end
end