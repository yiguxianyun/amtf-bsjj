# encoding: UTF-8
module AMTF
    module BUKONG   
       module LayoutDrawings        
        def self.page_builder
          @part_list = []     
            entities = Sketchup.active_model.entities
            entities.each do |entity|               
                next unless entity.is_a?(Sketchup::Group)                
                explode = Utilities.get_comp_attribute(entity, 'amtflogic_exploded', 'amtf_dict')
                if explode != 'true'  || Utilities.hasParent?(entity)
                UI.messagebox OB[:explode_required], MB_OK
                return
                else             
                    next unless Utilities.solid?(entity)     
                    sheetpart = SheetPart.new entity, entity.transformation, IDENTITY
                    @part_list << sheetpart
                end
            end     
            return unless @part_list.size > 0
            #Lọc và lấy tên, nếu tên khác nhau dấu cách, hoặc chữ hoa, chữ thường thì đưa về cùng một tên
            name_arr = []
            no_name_found = false
            hidden_layer =  Sketchup.active_model.layers.add("amtflogic_hidden_layer")
            for i in 0..@part_list.size - 1
                part = @part_list[i]
                grp_name = Utilities.get_comp_attribute(part.comp, 'prefix', 'amtf_dict')
                unless grp_name.nil?
                    item = {
                        :grp_name => grp_name, 
                        :id => Utilities.fj_uniq_name(grp_name), 
                        :comp => part.comp
                    } 
                    name_arr << item  unless grp_name.empty?
                end 
                if grp_name.nil?   
                    part.comp.layer = hidden_layer  
                    no_name_found = true       
                elsif  grp_name.empty?
                    no_name_found = true
                end    
            end
            UI.messagebox OB[:no_name_found], MB_OK if no_name_found 
            id_arr = []
            @scene_data = []
            for i in 0..name_arr.size - 1
                unless id_arr.include?name_arr[i][:id]
                    id_arr << name_arr[i][:id] 
                    comp_array = Drawing_Export.get_comp_in_group_name(@part_list, name_arr[i][:id]) 
                    layer = Sketchup.active_model.layers.add(name_arr[i][:id])                      
                    comp_array.each {
                        |grp|                            
                        grp.layer = layer
                    }
                    @scene_data << {
                        :grp_name_id => name_arr[i][:id], 
                        :grp_name_title => name_arr[i][:grp_name], 
                        :pages => Drawing_Export.get_pages_with_grp_name_id(name_arr[i][:id]),
                        :comp_array => comp_array,
                        :layer => layer
                    }
                end
            end
            @scene_data << {
                :grp_name_id => 'amtflogic_custom_scene', 
                :grp_name_title => "Custom Scene", 
                :pages => Drawing_Export.get_custom_pages(),
                :comp_array => [],
                :layer => nil
            }
            @dialog = AMTF_Dialog.dialog
            @dialog.set_size(1120, 700) 
            @dialog.center
            @dialog.on('page_builder_request_data'){
              |deferred|               
              begin                 
                deferred.resolve(Drawing_Export.get_needed_scene(@scene_data))
              rescue => error
                  Utilities.print_exception(error, false)
                  Sketchup.set_status_text(error)
                  deferred.resolve(false)                         
              end  
            }
            @dialog.execute_script('activate_pagebuilder();')
            @template_file = File.join(PLUGIN_DIR, "/styles/template.layout")
            @dialog.on("select_drawing_template"){
              |deferred|               
              begin  
                choose_file = UI.openpanel("Open Layout File", "c:/", "Layout Files|*.layout;||")
                @template_file = choose_file if choose_file
                deferred.resolve(choose_file)
              rescue => error
                  Utilities.print_exception(error, false)
                  Sketchup.set_status_text(error)
                  deferred.resolve(false)                         
              end  
            }
            @dialog.on("generateDrawing"){
                |deferred, hashData| 
                if Sketchup.active_model.modified?
                  js_showError = "pagebuilder_showError('warning','#{OB[:File_not_saved]}');"
                  @dialog.execute_script(js_showError)
                else
                  generate_drawings(hashData)
                end
            }
            @dialog.on("minize_pagebuilder"){
              |deferred| 
              AMTF_Dialog.return_home             
          }
        end
        def self.generate_drawings(hashData)
          model = Sketchup.active_model
          model_path = Sketchup.active_model.path
          @save_file ||= ""
          if model_path.empty? || Sketchup.active_model.modified?
            UI.messagebox(OB[:File_not_saved], MB_OK)
            return nil
          else
            name = Sketchup.active_model.title
            path = File.dirname(model_path)
            @save_file =slashify( File.join(path, "#{name}.layout"))
          end
          doc = Layout::Document.open(@template_file)    
          hashSize = hashData.size
          pb = ExtraFuntion::ProgressBar.new(hashSize, "Generate Drawing") 
          i = 0
          timer = UI.start_timer(0.005, true){  
            if i == hashSize
              UI.stop_timer(timer)                       
              Sketchup.set_status_text(OB[:completed])
              js_showError = "pagebuilder_showError('warning','#{OB[:completed]}');"
              @dialog.execute_script(js_showError)
            end
            if i < hashSize
              begin
                key = hashData[i]["page_key"]
                scene_data = hashData[i]["scene_data"]
                template_type = hashData[i]["template_type"]
                add_model_viewport_page(model_path, key, doc, template_type, scene_data)
                pb.update(i)
                i +=1                           
              rescue => error                    
                  Utilities.print_exception(error, false) 
                  i +=1
              end
            end  
          }
          # hashData.each{
          #   |key, value|     
          #   count +=1            
          #   add_model_viewport_page(model_path, key, doc, value["template_type"], value["scene_data"], pb, count, hashSize)
          # }
          model.commit_operation
        end
        def self.add_model_viewport_page(model_path, title, doc, template_type, scene_data)
          tempPage = doc.pages[template_type.to_f]
          tempModels = tempPage.entities.grep(Layout::SketchUpModel)
          tempTitle = tempPage.entities.grep(Layout::FormattedText)
          titleTemplates = Hash.new
          tempTitle.each{
            |tit|
            titleTemplates[0] = tit if tit.plain_text == "FUJI-TITLE0"
            titleTemplates[1] = tit if tit.plain_text == "FUJI-TITLE1"
            titleTemplates[2] = tit if tit.plain_text == "FUJI-TITLE2"
            titleTemplates[3] = tit if tit.plain_text == "FUJI-TITLE3"
            titleTemplates[4] = tit if tit.plain_text == "FUJI-TITLE4"
            titleTemplates[5] = tit if tit.plain_text == "FUJI-TITLE5"
          }
          this_page = doc.pages.add(title)
          # Create an unshared layer for this new page only:
          layer = doc.layers.add(title)
          layer.set_nonshared(this_page, Layout::Layer::UNSHARELAYERACTION_CLEAR)
          this_page.set_layer_visibility(layer, true)
          su_pages = Sketchup.active_model.pages
          iHash = scene_index()          
          for i in 0.. scene_data.size - 1
            begin
              modelIndex = scene_data[i]["modelIndex"]
              scene_name = scene_data[i]["scene_name"]
              old_model = tempModels[modelIndex.to_i]   
              old_title = titleTemplates[modelIndex.to_i]
              title_style = old_title.style
              title_bound = old_title.bounds
              style = old_model.style
              view_bounds = old_model.bounds  
              scale = old_model.scale  
              # Create the viewport object:
              viewport = Layout::SketchUpModel.new(model_path, view_bounds)
              viewport.current_scene = iHash[scene_name] + 1
              # Add the viewport object to the document page on the unshared layer:
              doc.add_entity( viewport, layer, this_page )
              # Render the viewport:
              viewport.render_mode= Layout::SketchUpModel::HYBRID_RENDER
              viewport.render if viewport.render_needed?
              viewport.style = style
              view = viewport.view            
              viewport.scale = scale if [0,1,2,3,4,5,6,7].include?(view)
              text = Layout::FormattedText.new(scene_name, title_bound)
              text.style = title_style
              doc.add_entity( text, layer, this_page )
              dim_layer = "#{scene_name}_dim" 
              dims = dimByLayer(dim_layer)
              if dims.size > 0
                # Có lỗi ở dòng 232 này
                su_pages[scene_name].set_visibility(dim_layer, false)
                createDim(doc, viewport, dims, layer, this_page)
              end
              status = doc.save(@save_file)
            rescue => error                    
                Utilities.print_exception(error, false) 
                js_showError = "pagebuilder_showError('warning','#{error}');"
                  @dialog.execute_script(js_showError)
            end
          end 
        end ### add_model_viewport_page()
        def self.scene_index
          pages = Sketchup.active_model.pages
          iHash = Hash.new
          for i in 0..pages.size - 1
            page = pages[i]
            iHash[page.name] = i
          end
          return iHash
        end
        def self.slashify(path)
          path.gsub(/\\\\|\\/,'/')
        end
        def self.createDim(doc, viewport, dims, layer, page)
          dims.each {
            |entity|
            distance = entity.get_attribute("amtf_dict", "distance", 0).to_f.to_l
            start_3d = entity.start[1]
            end_3d = entity.end[1]
            offset_3d = entity.offset_vector
            tr = Geom::Transformation.new(Utilities.vector_multiply(distance, offset_3d.normalize))
            start_3d.transform!(tr)
            end_3d.transform!(tr)
            start_offset_3d = Geom::Point3d.new  offset_3d.x + start_3d.x, offset_3d.y + start_3d.y, offset_3d.z + start_3d.z
            start_2d = viewport.model_to_paper_point(start_3d)
            end_2d = viewport.model_to_paper_point(end_3d)
            start_offset_2d =  viewport.model_to_paper_point(start_offset_3d)
            start_offset_length =  Math.sqrt((start_2d.x - start_offset_2d.x) *
                                    (start_2d.x - start_offset_2d.x) +
                                    (start_2d.y - start_offset_2d.y) *
                                    (start_2d.y - start_offset_2d.y))
              # // Determine the direction the dimension lines will extend.
              start_end = Geom::Point2d.new end_2d.x - start_2d.x, end_2d.y - start_2d.y
              start_end_perp = Geom::Point2d.new start_end.y, -start_end.x
              start_offset = Geom::Point2d.new start_offset_2d.x - start_2d.x, start_offset_2d.y - start_2d.y
              # // Dot product between the line perpendicular to the start and end points,
              # // and the line from the start point to the start offset. A negative value
              # // indicates that we want the dimension length to be negative.
              dot_product = (start_end_perp.x * start_offset.x) + (start_end_perp.y * start_offset.y)
              start_offset_length *= -1 if dot_product < 0              
              start_point = Layout::ConnectionPoint.new(viewport, start_3d)
              end_point = Layout::ConnectionPoint.new(viewport, end_3d)
              dim = Layout::LinearDimension.new(start_2d, end_2d, start_offset_length)
              doc.add_entity(dim, layer, page)
              dim.connect(start_point, end_point)
              dim.auto_scale = true
              # Lấy style cho dim
              pages= doc.pages
              dimS = pages[1].entities.grep(Layout::LinearDimension)
              dim_style = nil
              dim_style = dimS[0].style if dimS.size > 0
              if dim_style
                dim_text_style = dim_style.get_sub_style(Layout::Style::DIMENSION_TEXT)
                dim_line_style = dim_style.get_sub_style(Layout::Style::DIMENSION_LINE)
                dim_start_ext_line = dim_style.get_sub_style(Layout::Style::DIMENSION_START_EXTENSION_LINE)
                dim_end_ext_line = dim_style.get_sub_style(Layout::Style::DIMENSION_END_EXTENSION_LINE)
                dim_leader_line = dim_style.get_sub_style(Layout::Style::DIMENSION_LEADER_LINE)
                style = dim.style
                # Apply the dimension text style changes to the dimension style
                 style.set_sub_style(Layout::Style::DIMENSION_TEXT, dim_text_style)
                 # Apply dimension line style changes to the dimension style
                 style.set_sub_style(Layout::Style::DIMENSION_LINE, dim_line_style)
                 style.set_sub_style(Layout::Style::DIMENSION_LEADER_LINE, dim_leader_line)
                 style.set_sub_style(Layout::Style::DIMENSION_START_EXTENSION_LINE, dim_start_ext_line)
                 style.set_sub_style(Layout::Style::DIMENSION_END_EXTENSION_LINE, dim_end_ext_line)
                 # Apply the dimension style changes to the dimension
                 style.set_dimension_units(Layout::Style::DECIMAL_MILLIMETERS, 1)
                 dim.style = style
              else
                style = dim.style
                # Set the dimension units on the root style
                style.set_dimension_units(Layout::Style::DECIMAL_MILLIMETERS, 1)
                # Get the dimension text style to make text changes
                dim_text_style = style.get_sub_style(Layout::Style::DIMENSION_TEXT)
                dim_text_style.text_color = Sketchup::Color.new(0, 0, 0, 0)
                # Get the dimension line style to make arrow head changes
                dim_line_style = style.get_sub_style(Layout::Style::DIMENSION_LINE)
                dim_line_style.start_arrow_type = Layout::Style::ARROW_FILLED_TRIANGLE
                dim_line_style.start_arrow_size = 1
                dim_line_style.end_arrow_type = Layout::Style::ARROW_FILLED_TRIANGLE
                dim_line_style.end_arrow_size= 1
                dim_line_style.stroke_width = 0.5
                # Apply the dimension text style changes to the dimension style
                style.set_sub_style(Layout::Style::DIMENSION_TEXT, dim_text_style)
                # Apply dimension line style changes to the dimension style
                style.set_sub_style(Layout::Style::DIMENSION_LINE, dim_line_style)
                # Apply the dimension style changes to the dimension
                dim.style = style
              end
              dimS.each {
                |ds|
                # doc.remove_entity(ds)
              }
          }
        end
        def self.pageHash
          pHash = Hash.new
          model = Sketchup.active_model
          pages = model.pages  
          for i in 0..pages.size - 1 
            pHash[pages[i].name] = i
          end
          return pHash
        end
        def self.dimByLayer(layer_name)
          model = Sketchup::active_model
          dims = model.entities.grep(Sketchup::DimensionLinear)
          dims_on_layer = dims.find_all {|item|
            item.layer.name == layer_name
          }
          return dims_on_layer
        end
      end
    end
  end