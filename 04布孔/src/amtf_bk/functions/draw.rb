# encoding: UTF-8
module AMTF
  module BUKONG
      module Draw_functions        
        def self.draw_base(data, pick_points)
          model = Sketchup.active_model       
          model.start_operation(OB[:create_frame], true) 
          ents = Sketchup.active_model.entities
          point_2 = pick_points.pop;
          point_1 = pick_points.pop;
          move_trans = Geom::Transformation.translation point_1
          vector1 = Geom::Vector3d.new(1,0,0)
          vector2 = point_1.vector_to(point_2)
          angle = vector1.angle_between vector2
          rotation_trans = Geom::Transformation.rotation point_1, [0, 0, 1],  angle
          groupArray = []            
            data.each do |item|
              points = []      
              group = model.active_entities.add_group()
              item["mesh"].each do |point|               
                points.push( 
                  Geom::Point3d.new(Utilities.unitConvert( point['x']), Utilities.unitConvert( point['y']), Utilities.unitConvert( point['z'])))
              end
             face = group.entities.add_face( Utilities.uniq_points(points))
              if( face.normal.z < 0 || face.normal.y < 0 || face.normal.x < 0)                
                face.reverse!
              end          
              face.pushpull( Utilities.unitConvert(item["thick"]))
              if(item["arc"] == true)
                makesoftsmooth(group)
              end              
              if item.has_key?("type")                
                  group.set_attribute 'amtf_dict', 'comp_type', 'back'  if item["type"] == 'back'
                  group.set_attribute 'amtf_dict', 'comp_type', 'no_hole'  if item["type"] == 'no_hole'
              end
              group = item["name"] if item.has_key?("name")
              groupArray.push(group)
            end
            frameGroup = model.active_entities.add_group()
            groupArray.each { |group| make_group(frameGroup, group) }
            ents.transform_entities move_trans, frameGroup
            ents.transform_entities rotation_trans, frameGroup
            model.commit_operation
        end
        def self.make_group(group, origin)
          comp_type = Utilities.get_comp_attribute(origin, 'comp_type', 'amtf_dict')
          group.entities.add_instance(origin.entities.parent, origin.transformation*group.transformation)
          origin.entities.parent.instances[1].material=origin.material
          origin.entities.parent.instances[1].layer=origin.layer
          origin.entities.parent.instances[1].set_attribute 'amtf_dict', 'comp_type', comp_type if !comp_type.nil?       
          origin.entities.parent.instances[1].name = origin.name
          origin.erase!
          return group
        end
        def self.makesoftsmooth(group)
           group.entities.to_a.each{|e|
            if e.class==Sketchup::Edge
              pt1 = e.vertices[0].position
              pt2 = e.vertices[1].position
              vector = pt1.vector_to(pt2)
              vector2 = Geom::Vector3d.new(0,0,1)
              if e.faces.length==2  and vector.parallel?(vector2)
                e.soft=true
                e.smooth=true
              end#if
            end
           }
         end#def
      end #Draw_functions
    end #BuKong
  end #Amtflogic