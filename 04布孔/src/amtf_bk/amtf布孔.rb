# encoding: UTF-8
require 'sketchup.rb'
require 'extensions.rb'
module AMTF
	module BUKONG
		class A布孔
			include AMTF_bk_mixin
			def 另存amtf布孔设置文件
				chosen_folder = UI.select_directory(title: "请选择  拟保存到的 文件夹")
				puts chosen_folder
				if !chosen_folder.nil?
					另存文件=File.join(chosen_folder, 'amtf布孔设置文件.json')
					puts FileUtils.cp Amtf布孔设置文件, 另存文件
					puts 另存文件
				end
			end
			def 导入amtf布孔设置文件
				chosen_folder = UI.select_directory(title: "请选择  amtf布孔设置文件.json  所在的文件夹")
				puts chosen_folder
				if !chosen_folder.nil?
					拟导入文件=File.join(chosen_folder, 'amtf布孔设置文件.json')
					原设置=读取设置
					puts 原设置.class
					json = File.read(拟导入文件)
					拟导入设置 = JSON.parse(json)
					原设置.merge!(拟导入设置)
    				File.write(Amtf布孔设置文件, JSON.dump(原设置))
				end
			end
		end
	end
end # module AMTF