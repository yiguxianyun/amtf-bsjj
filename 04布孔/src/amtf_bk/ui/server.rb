# encoding: UTF-8
require 'json'
module AMTF
	module BUKONG
		module ServerOptions
			def self.create_dialog
				options = {
					dialog_title: OB[:server],
					preferences_key: 'bukongcabinet.net',
					scrollable: false,
					resizable: false,
					width: 400,
					height: 160,
					left: 0,
					top: 100,
					min_width: 50,
					min_height: 50,
					max_width: 1000,
					max_height: 1000,
					style: UI::HtmlDialog::STYLE_DIALOG
				}
				dialog = UI::HtmlDialog.new(options)
				dialog.set_size(options[:width], options[:height]) # Ensure size is set.
				dialog.set_file(PLUGIN_DIR + '/ui/server.html')
				dialog.center
				dialog
			end
			def self.options
				@dialog.close if !@dialog.nil?
				@dialog = self.create_dialog
				@dialog.add_action_callback('get_data') do |action_context|
					server =
						Sketchup.read_default(PLUGIN_ID, 'server', 'server1')
					gui_data = { server: server }
					json_string = gui_data.to_json
					js_command = "createDom('" + json_string + "');"
					@dialog.execute_script(js_command)
					nil
				end
				@dialog.add_action_callback(
					'save_setting'
				) do |action_context, params|
					Sketchup.write_default(
						PLUGIN_ID,
						'server',
						params['server']
					)
					AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
					nil
				end
				@dialog.show
				return @dialog
			end
			def self.get_server_url
				server = Sketchup.read_default(PLUGIN_ID, 'server', 'server1')
				return FUJILOGIC_SERVER_0 if server == 'server0'
				return FUJILOGIC_SERVER_1 if server == 'server1'
				return FUJILOGIC_SERVER_2 if server == 'server2'
				return FUJILOGIC_SERVER_1
			end
			def self.get_update_server_url
				server = Sketchup.read_default(PLUGIN_ID, 'server', 'server1')
				return FUJILOGIC_UPDATE_SERVER_0 if server == 'server0'
				return FUJILOGIC_UPDATE_SERVER_1 if server == 'server1'
				return FUJILOGIC_UPDATE_SERVER_2 if server == 'server2'
				return FUJILOGIC_UPDATE_SERVER_1
			end
		end #ConnectorOptions
	end #BuKong
end #Amtflogic