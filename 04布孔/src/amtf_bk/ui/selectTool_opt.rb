# encoding: UTF-8
require 'json'
module AMTF
	module BUKONG
		module SelectOptions
			def self.create_dialog
				options = {
					dialog_title: OB[:select_tool],
					preferences_key: 'bukongcabinet.net',
					scrollable: false,
					resizable: false,
					width: 400,
					height: 100,
					left: 0,
					top: 100,
					min_width: 50,
					min_height: 50,
					max_width: 1000,
					max_height: 1000,
					style: UI::HtmlDialog::STYLE_DIALOG
				}
				data = { language: OB.saved_lang }
				dialog = UI::HtmlDialog.new(options)
				dialog.set_size(options[:width], options[:height]) # Ensure size is set.
				dialog.set_file(PLUGIN_DIR + '/ui/select_tool.html')
				dialog.center
				dialog.add_action_callback('get_data') do |action_context|
					js_command = "update_dom('#{OB.saved_lang}')"
					dialog.execute_script(js_command)
					nil
				end
				dialog
			end
		end #EdgeOptions
	end #BuKong
end #Amtflogic