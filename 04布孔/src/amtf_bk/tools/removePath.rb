# encoding: UTF-8
module AMTF
  module BUKONG
  module RemovePathModule
    class RemovePathTool < Tools::Base
      def activate  
        @@active_tool_class = self.class        
        @input  = Sketchup::InputPoint.new
        @offset_data = nil   
        @colored_edge = []
        @first_pick = false
        Sketchup.status_text = OB[:select_target_object]
      end
      def deactivate(view)        
        @colored_edge.each {
          |ed| 
          if !ed.nil?
            ed.hidden = false
          end
        }
        @colored_edge = []
        @offset_data = nil
        @first_pick = false
        @@active_tool_class =nil
        view.lock_inference
        view.invalidate
      end 
      def reset_tool   
        @colored_edge.each {
          |ed| 
          if !ed.nil?
            ed.hidden = false if !ed.nil? && ed.valid?
          end
        }       
        @colored_edge = []
        @offset_data = nil
        @first_pick = false
        Sketchup.status_text = ''
      end
      def resume(view)
        view.invalidate
      end    
      def onCancel(reason, view)
        reset_tool
        view.invalidate
      end  
      def draw(view)        
        return if @offset_data.nil?   
        view.drawing_color = Sketchup::Color.new(255, 0, 0, 64)
        view.line_stipple = "_"
        view.line_width = 1
        view.draw_polyline(@offset_data[:points])
        view.invalidate
      end
      def onMouseMove(flags, x, y, view)     
          view.invalidate
      end
      def onLButtonDown flags, x, y, view
        #  Khi click chuột vào component
        # Nhấp chuột lần th�?nhất: chọn loop của pick_face, nhấp chuột th�?2 thì chuyển sang offset
        # Lấy tất c�?các loop của face, và tất c�?các group con là tool_path hoặc curve_groove
        #  so sánh khoảng cách xem ch�?nhấp chuột gần cái nào nhất thì chọn nó đ�?chuyển đi offset
        #  @offset_data dùng cho mục đích hiển th�?bên view (các đường màu đ�?
        # Sau khi đã tính được @offset_data, thì tiếp tục truy vấn tất c�?các entities, lấy các phần t�?có cùng ID
        #  đó cũng chính là các bản copy, cho mục đích offset c�?các bản copy
        #  Trong trình t�?thực hiện: khi mới nhấp chuột lần 1: kiểm tra xem component đã có ID hay chưa
        #  nếu đã có ID thì lấy ID, nếu chưa có ID thì tạo ID cho component và face và edge
        #  ID của face lớn cũng được dùng làm ID cho edge. Nh�?ID của face lớn mà biết được loop nào của face (trong bản copy)
        #  được s�?dụng.
        Sketchup.active_model.selection.clear
          @input.pick view, x, y
          @ph.do_pick(x, y)
          get_pick = pick_me(@ph)
          return if get_pick.nil?
          @first_pick = !@first_pick
          pick_component = get_pick[:pick_component]
          pick_face = get_pick[:pick_face]
          pick_edge = get_pick[:pick_edge]
          pick_componentTrans = pick_component.transformation
          if !@first_pick              
              if @result.nil? || @result.size == 0
                reset_tool
                return
              end
              remove_huynh(@result)   
              reset_tool
          else
              input_click_post = @input.position  
              Sketchup.status_text = OB[:click_to_offset]                  
              # Khi click thì tạo luôn ID cho comp này, tạo ID cho các mặt và edge
              # Mục đích sau này khi có phần t�?copy thì cũng s�?biết được nó copy của phần t�?nào
              comp_id = Utilities.get_comp_attribute(pick_component, 'comp_id', 'amtf_dict')
              return if comp_id.nil?
              distance_array = []
              # Lấy tiếp các group nằm bên trong
              dist_sub_arr = []
              pick_component.entities.grep(Sketchup::Group).each do |grp|
                sub_grp_type = Utilities.get_comp_attribute(grp,'type', 'amtf_dict')              
                closed = Utilities.get_comp_attribute(grp,'closed', 'amtf_dict')
                if sub_grp_type == 'tool_path' || (sub_grp_type == 'curve_groove' && closed) 
                  # Các đường đã được tạo sẵn thì tính khoảng cách gần cạnh nhất             
                  minifixID = Utilities.get_comp_attribute(grp,'minifixID', 'amtf_dict')         
                  trans = pick_componentTrans*grp.transformation
                  dist = []
                  edges = grp.entities.grep(Sketchup::Edge) 
                  post = Utilities.point_context_convert(input_click_post, IDENTITY, trans) 
                  next unless Utilities.within_polygon?(post, edges)
                  points = TTLibrary.getPathPoints( edges )
                  next if points.nil?
                  points = points.map{
                    |pt| 
                    pt = Utilities.point_context_convert(pt, trans, IDENTITY)
                  }
                  edges.each do |ed|
                    line = ed.line                    
                    distance = post.distance_to_line(line)
                    dist << {
                      :instance => grp,
                      :distance => distance, 
                      :target => 'path',
                      :edges => edges,
                      :points => points,
                      :container => pick_component,
                      :id => minifixID
                    }
                  end #each
                  dist = dist.sort_by{ |item| item[:distance] } # Mảng khoảng cách đến các edge trong loop
                  dist_sub_arr << dist[0] # Lấy cái ngắn nhất đưa vào mảng của nhiều loop                  
                end #if
              end #each
              return unless dist_sub_arr.size > 0
              dist_sub_arr = dist_sub_arr.sort_by{ |item| item[:distance] }     
              distance_array << dist_sub_arr[0] if dist_sub_arr.size > 0
              distance_array = distance_array.sort_by{ |item| item[:distance] }
#               unless distance_array[0][:distance].to_l < 5 .mm
#                 reset_tool
#                 return 
#               end
              @offset_data = {
                  :instance => distance_array[0][:instance],
                  :target => distance_array[0][:target],
                  :edges => distance_array[0][:edges],
                  :points => distance_array[0][:points],
                  :container => distance_array[0][:container],
                  :id => distance_array[0][:id],
                  :face => pick_face,
                  :comp => pick_component,                    
              }
              mod = Sketchup.active_model
              mod.start_operation("Delete", true)            
              @offset_data[:edges].each{
                |ed|
                if !ed.nil?
                  @colored_edge << ed
                  ed.hidden = true 
                end                
              }            
              mod.commit_operation
              @result = [{
                :instance => distance_array[0][:instance],
                :container => distance_array[0][:container],
                :points => distance_array[0][:points],
                :id => distance_array[0][:id],
              } ]
              list = []             
              #Lấy hết tất c�?các thằng có cùng ID đ�?đưa sang offset
              entities = Sketchup.active_model.entities                
              entities.each do |entity|               
                  next unless entity.is_a?(Sketchup::Group)    
                  next if entity == pick_component            
                  explode = Utilities.get_comp_attribute(entity, 'amtflogic_exploded', 'amtf_dict')
                  if explode != 'true'  || Utilities.hasParent?(entity)
                    UI.messagebox OB[:explode_required], MB_OK
                    return
                  else             
                      next unless Utilities.solid?(entity)  
                      id = Utilities.get_comp_attribute(entity, 'comp_id', 'amtf_dict')
                      #  Ch�?lấy các bản copy (có cùng id)
                      next unless comp_id == id
                      list << entity if @offset_data[:target] == 'path'                       
                  end
              end        
              unless @offset_data[:target] == 'sheet' 
                  paths = []
                  # Duyệt qua list các phần t�?copy, lấy các 
                  for i in 0..list.size - 1
                      entity = list[i]
                      tool_path = entity.entities.grep(Sketchup::Group)
                      tool_path.each do |grp|
                          sub_grp_type = Utilities.get_comp_attribute(grp,'type', 'amtf_dict')
                          closed = Utilities.get_comp_attribute(grp,'closed', 'amtf_dict')                          
                          if sub_grp_type == 'tool_path' || (sub_grp_type == 'curve_groove' && closed)  
                            minifixID = Utilities.get_comp_attribute(grp,'minifixID', 'amtf_dict')   
                            if @offset_data[:id] == minifixID  
                              sub_edges = grp.entities.grep(Sketchup::Edge)
                              cpoints = TTLibrary.getPathPoints( sub_edges )
                              tr = entity.transformation * grp.transformation
                              cpoints = cpoints.map{
                                |pt| 
                                pt = Utilities.point_context_convert(pt, tr, IDENTITY)
                              }   
                              @result << {
                                :instance => grp,
                                :container => entity,
                                :points => cpoints,
                                :id => minifixID
                              }
                            end  
                          end  
                      end
                  end   
              end
          end
          view.lock_inference 
          view.invalidate    
      end 
      def remove_huynh(attrs)   
            mod = Sketchup.active_model
            mod.start_operation("Delete", true)            
            attrs.each do 
                |item|
                item[:instance].erase! if !item[:instance].nil? && item[:instance].valid?
            end            
            mod.commit_operation
        end
    end
  end#  OffsetToolModule
  end # BuKong
end #