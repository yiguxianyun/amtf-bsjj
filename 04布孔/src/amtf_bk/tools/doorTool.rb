# encoding: UTF-8
module AMTF
    module BUKONG      
      module DoorTool 
        class DoorAssignTool < Tools::Base             
            STS_CLASS_NAME   = OB[:groove_element].freeze
            def onLButtonDown(flags, x, y, view)
                mod = Sketchup.active_model
                mod.start_operation(OB[:groove_element], true)                 
                @input.pick view, x, y
                @ph.do_pick(x, y)
                get_pick = pick_me(@ph, big_face_required: true)
                return if get_pick.nil?
                pick_component = get_pick[:pick_component]
                pick_face = get_pick[:pick_face]
                return unless pick_component && pick_face
                return unless Utilities.instance?(pick_component) && pick_face.valid? && pick_face.is_a?(Sketchup::Face)
                comp_type = Utilities.get_comp_attribute(pick_component, 'comp_type', 'amtf_dict')
                point = @input.position 
                if comp_type === 'door'
                    pick_component.delete_attribute 'amtf_dict', 'comp_type'
                    self.delete_cline(pick_component)                 
                else                 
                    pick_component.set_attribute 'amtf_dict', 'comp_type', 'door' 
                    self.tao_cline(point, pick_component, pick_face)
                end     
                mod.commit_operation
            end
            def delete_cline(pick_component)
                subGroup = pick_component.entities.grep(Sketchup::Group)    
                if subGroup.size > 0    
                    for i in 0..subGroup.size - 1
                        group_type =  Utilities.get_comp_attribute(subGroup[i], 'type', 'amtf_dict')
                        if group_type == 'bukong_door_cline'                         
                            subGroup[i].erase!
                            break
                        end
                    end                            
                end
            end
            def tao_cline(point, pick_component, pick_face)
                # Xác định cạnh gần điểm click chuột
                # Xác định cạnh đối diện
                # Xác định trung điểm cạnh gần
                # V�?hai cái đường Cline tới hai đầu mút cạnh đối diện
                bigFaceArr = Utilities.get_big_faces(pick_component)    
                return unless bigFaceArr.include?(pick_face) 
                point = Utilities.point_context_convert(point, IDENTITY, pick_component.transformation)    
                boundingCorner = Utilities.amtfBoundingBox(pick_component,  pick_component.transformation)
                corners = []
                for i in 0..7                
                    cn = boundingCorner[i]
                    corners << cn if cn.on_plane?(pick_face.plane)
                end
                corners = Utilities.get_sorted_points_ccw(corners)
                return if corners.size == 0
                nearest_line = [ corners[0], corners[0].vector_to(corners[1]) ]
                kc_edge = point.distance_to_line(nearest_line)
                nearest_vertices = [ corners[0], corners[1]]
                for i in 1.. corners.size - 2
                    line = [ corners[i], corners[i].vector_to(corners[i+1]) ]
                    khoang_cach = point.distance_to_line(line)
                    if khoang_cach < kc_edge
                        kc_edge = khoang_cach
                        nearest_line = line
                        nearest_vertices = [corners[i], corners[i+1]]
                    end
                end
                corners.pop
                center = Utilities.find_center_of_points(corners)
                dist_to_center = point.distance(center)
                cline_grp = pick_component.entities.add_group
                if kc_edge < dist_to_center
                    dau_mut = []
                    for k in 0..corners.size - 1
                        co = corners[k]
                        dau_mut << co if !Utilities.point_compare?(co, nearest_vertices[0]) && !Utilities.point_compare?(co, nearest_vertices[1])
                    end
                    dau_mut = Utilities.uniq_points(dau_mut)    
                    self.delete_cline(pick_component)
                    trung_diem = Utilities.get_midpoint(nearest_vertices[0],nearest_vertices[1])
                    return if dau_mut.size < 2
                    cline_grp.entities.add_cline( trung_diem , dau_mut[0])
                    cline_grp.entities.add_cline( trung_diem , dau_mut[1])
                else
                    cline_grp.entities.add_cline(corners[0] , corners[2])
                    cline_grp.entities.add_cline(corners[1] , corners[3])
                end
                cline_grp.set_attribute('amtf_dict', 'type', 'bukong_door_cline')
            end
        end
    end
    end
end