# encoding: UTF-8
module AMTF
	module BUKONG
		module MovingTool
			class MoveHoles < Tools::Base
				STS_CLASS_NAME = OB[:move_tool].freeze
				def activate
					@@active_tool_class = self.class
					@input = Sketchup::InputPoint.new
					@pick_step = 0
					@my_vcb = ''
					Sketchup.status_text = OB[:select_target_object]
					@center_points = []
					@selectedCenter = nil
					@helper_point = nil
					@pick_data = {}
					AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
				end
				def deactivate(view)
					@@active_tool_class = nil
					@center_points = []
					@selectedCenter = nil
					@helper_point = nil
					@pick_data = {}
					@my_vcb = ''
					view.lock_inference
					view.invalidate
				end
				def enableVCB?
					return true
				end
				def clear_my_vcb
					@my_vcb = ''
				end
				def num_decode(key)
					case key
					when 48, 96
						return '0'
					when 49, 97
						return '1'
					when 50, 98
						return '2'
					when 51, 99
						return '3'
					when 52, 100
						return '4'
					when 53, 101
						return '5'
					when 54, 102
						return '6'
					when 55, 103
						return '7'
					when 56, 104
						return '8'
					when 57, 105
						return '9'
					when 190, 110
						return '.'
					else
						return ''
					end
				end
				def onKeyUp(key, repeat, flags, view)
					if key == 13
						distance = @my_vcb.to_s.to_l
						if @pick_data
							Sketchup.active_model.start_operation('Move', true)
							if @frame
								dir =
									Utilities.vector_context_convert(
										@pick_data[:direction],
										IDENTITY,
										@frame.transformation
									)
								connector_type =
									@pick_data[:pick_minifix].get_attribute(
										'amtf_dict',
										'type'
									)
								if connector_type == 'dogbone_female' ||
										connector_type == 'dogbone_male'
									connector_type = 'dogbone'
								end
								if @pick_data[:pick_minifix].get_attribute(
										'amtf_dict',
										'cabineo_cap'
								   )
									connector_type = 'cabineo_cap'
								end
								pairs =
									@pick_data[:pick_minifix].get_attribute(
										'amtf_dict',
										'pairs'
									)
								minifixID =
									@pick_data[:pick_minifix].get_attribute(
										'amtf_dict',
										'minifixID'
									)
								@frame.move_connector(
									connector_type,
									pairs,
									minifixID,
									dir,
									distance.to_f
								)
							end
							@pick_data[:same_sheet].each do |mini|
								vect =
									Utilities.vector_multiply(
										distance,
										@pick_data[:direction]
									)
								tr =
									Geom::Transformation.new(
										Utilities.vector_context_convert(
											vect,
											IDENTITY,
											mini[:trans]
										)
									)
								mini[:instance].transform!(tr)
							end
							@pick_data[:partner_sheet].each do |mini|
								p1 =
									Utilities.point_context_convert(
										@selectedCenter,
										IDENTITY,
										mini[:trans]
									)
								p2 =
									Utilities.point_context_convert(
										@helper_point,
										IDENTITY,
										mini[:trans]
									)
								vect = p1.vector_to(p2).normalize!
								tr =
									Geom::Transformation.new(
										Utilities.vector_multiply(
											distance,
											vect
										)
									)
								mini[:instance].transform!(tr)
							end
							@center_points = []
							@selectedCenter = nil
							@helper_point = nil
							@pick_data = {}
							Sketchup.active_model.commit_operation
						end
						reset_tool
					end
					@my_vcb += num_decode(key)
					view.lock_inference
					view.invalidate
					return true
				end
				def reset_tool
					@my_vcb = ''
					@move_data = nil
					Sketchup.status_text = OB[:select_target_object]
				end
				def resume(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def draw(view)
					view.drawing_color = 'yellow'
					view.line_width = 3
					if @selectedCenter
						view.draw_points(
							@selectedCenter,
							size = 10,
							style = 2,
							color = 'yellow'
						)
					end
					if @helper_point && @selectedCenter
						view.draw_lines @helper_point, @selectedCenter
					end
					if @center_points.size > 0
						@center_points.each do |p|
							view.draw_points(
								p,
								size = 10,
								style = 2,
								color = 'yellow'
							)
						end
					end
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					@center_points = []
					@selectedCenter = nil
					@helper_point = nil
					@pick_data = {}
					model = Sketchup.active_model
					model.start_operation(OB[:move_tool], true)
					@input.pick view, x, y
					@ph.do_pick(x, y)
					pick_post = @input.position
					pick = pick_sheet(@ph, x, y)
					pickFace = pick[:pick_face]
					return unless pick && !pick.empty?
					return unless pickFace
					# Lấy pick_poct là điểm trên mặt plane của pickFace
					pick_plane =
						AmtfMath.plane_context_convert(
							pickFace.plane,
							pick[:trans],
							IDENTITY
						)
					camera = Sketchup.active_model.active_view.camera
					camera_direction = camera.direction
					ray_line = [pick_post, camera_direction]
					pick_post = Geom.intersect_line_plane(ray_line, pick_plane)
					@frame = pick[:frame]
					return if pick.empty?
					pick_component = pick[:pick_component]
					return unless pick_component
					amtf_sheet =
						AmtfSheet.new(
							pick_component,
							pick[:trans],
							pick[:parent_matrix]
						)
					minifix = amtf_sheet.grep_connector
					select_list =
						minifix.concat(amtf_sheet.grep_lockdowel_as_hole)
					dogbone = amtf_sheet.grep_dogbone
					cabineo_cap = amtf_sheet.grep_cabineo_cap
					select_list = select_list.concat(cabineo_cap)
					select_list = select_list.concat(dogbone[:female])
					dist_sort =
						select_list.sort do |a, b|
							p1 =
								Utilities.point_context_convert(
									a[:hole],
									pick[:trans],
									IDENTITY
								)
							p2 =
								Utilities.point_context_convert(
									b[:hole],
									pick[:trans],
									IDENTITY
								)
							pick_post.distance(p1) <=> pick_post.distance(p2)
						end
					return if dist_sort.size == 0
					pick_minifix = dist_sort[0][:instance]
					@selectedCenter = dist_sort[0][:hole]
					return unless @selectedCenter
					@selectedCenter =
						Utilities.point_context_convert(
							@selectedCenter,
							pick[:trans],
							IDENTITY
						)
					type = pick_minifix.get_attribute('amtf_dict', 'type')
					if %w[
							chot_dot
							khoan_moi
							drawer_fitting_1
							drawer_fitting_2
					   ].include?(type)
						@helper_point = nil
						@selectedCenter = nil
						return
					end
					if type != 'lockdowel'
						related = find_related(pick, pick_minifix)
						return if !related
						return if !related[:partner_sheet].size == 0
						related[:same_sheet].each do |mini|
							@center_points <<
								Utilities.point_context_convert(
									mini[:hole],
									mini[:trans],
									IDENTITY
								)
						end
						related[:partner_sheet].each do |mini|
							@center_points <<
								Utilities.point_context_convert(
									mini[:hole],
									mini[:trans],
									IDENTITY
								)
						end
						log_edge = related[:lines]
						vect = @selectedCenter.vector_to(pick_post)
						direction = log_edge[0].vector_to(log_edge[1]).normalize
						direction = direction.reverse if vect.dot(direction) < 0
						@helper_point =
							@selectedCenter.transform(
								Geom::Transformation.new(
									Utilities.vector_multiply(300.mm, direction)
								)
							)
						@pick_data = {
							direction: direction,
							miniTrans: dist_sort[0][:miniTrans],
							pick_minifix: pick_minifix,
							same_sheet: related[:same_sheet],
							partner_sheet: related[:partner_sheet]
						}
					elsif type == 'lockdowel'
						center_faces =
							Utilities
								.definition(pick_minifix)
								.entities
								.grep(Sketchup::Face)
						ctr1 = nil
						ctr2 = nil
						ctr3 = nil
						if center_faces.size > 0
							tr = pick[:trans] * pick_minifix.transformation
							center_faces.each do |cp|
								type = cp.get_attribute('amtf_dict', 'type')
								case type
								when 'point_1'
									ctr1 =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											IDENTITY
										)
									@center_points << ctr1
								when 'point_2'
									ctr2 =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											IDENTITY
										)
									@center_points << ctr2
								when 'point_3'
									ctr3 =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											IDENTITY
										)
									@center_points << ctr3
								end
							end
						end
						return unless !ctr1.nil? && !ctr2.nil? && !ctr3.nil?
						vect = @selectedCenter.vector_to(pick_post)
						direction = ctr1.vector_to(ctr2).normalize
						direction = direction.reverse if vect.dot(direction) < 0
						@helper_point =
							@selectedCenter.transform(
								Geom::Transformation.new(
									Utilities.vector_multiply(300.mm, direction)
								)
							)
						@center_points = [ctr1, ctr2, ctr3]
						@pick_data = {
							direction: direction,
							miniTrans: dist_sort[0][:miniTrans],
							pick_minifix: pick_minifix,
							same_sheet: [
								{ instance: pick_minifix, trans: pick[:trans] }
							],
							partner_sheet: []
						}
					end
					Sketchup.status_text = OB[:enter_distance]
					view.lock_inference
					view.refresh
					view.invalidate
				end
				private
				def find_related(pick, pick_minifix)
					minifixID =
						pick_minifix.get_attribute('amtf_dict', 'minifixID')
					same_sheet = []
					partner_sheet = []
					sheets = {}
					query = AmtfQuery.new
					minifixList =
						query.minifixList(Sketchup.active_model.entities)
					minifixList.each do |mini|
						if minifixID == mini[:minifixID]
							if mini[:parent] == pick[:pick_component]
								same_sheet << mini
							else
								sheets[mini[:parent]] = {
									pick_component: mini[:parent],
									parent_matrix: mini[:grand_parent_matrix],
									trans: mini[:trans]
								}
							end
						end
					end
					check_result = []
					sheets.each do |key, partner|
						make_hole_data = { pick_data: pick, last_pick: partner }
						checker = AmtfMakeHole.new(make_hole_data)
						check_data = checker.check_intersect
						if check_data && check_data.size > 0
							check_result << {
								partner: partner[:pick_component],
								lines: check_data[0]
							}
						end
					end
					result = check_result[0]
					minifixList.each do |mini|
						next unless mini && !mini.nil?
						if minifixID == mini[:minifixID]
							if mini[:parent] == result[:partner]
								partner_sheet << mini
							end
						end
					end
					return(
						{
							partner_sheet: partner_sheet,
							same_sheet: same_sheet,
							lines: result[:lines]
						}
					)
				end
			end #MoveHoles Class
		end #  MovingTool
	end # BuKong
end #