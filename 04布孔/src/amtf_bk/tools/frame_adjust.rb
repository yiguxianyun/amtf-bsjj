# encoding: UTF-8
module AMTF
	module BUKONG
		module AdjustFRMModule
			LOCK_X = 39
			LOCK_Y = 37
			LOCK_Z = 38
			LEFT_OFFSET = 'LEFT_OFFSET'
			RIGHT_OFFSET = 'RIGHT_OFFSET'
			FRONT_OFFSET = 'FRONT_OFFSET'
			BACK_OFFSET = 'BACK_OFFSET'
			TOP_OFFSET = 'TOP_OFFSET'
			BOTTOM_OFFSET = 'BOTTOM_OFFSET'
			class FrameAdjust < Tools::Base
				STS_CLASS_NAME = OB[:resize_frame].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					Sketchup.active_model.selection.clear
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					@boxHelper = nil
					@resize_data = nil
					@offset_mark = nil
					@node_id = nil
					@node_data = {}
					@frame_data = {}
					@frame = nil
					@is_moving = false
					@my_vcb = ''
					@resize_direction = LOCK_X
					if AMTF_STORE.init_open_json == false
						saver = AmtfSaver.new
						saver.load_comp
						AMTF_STORE.init_open_json = true
					end
				end
				def deactivate(view)
					@@active_tool_class = nil
					@boxHelper = nil
					@resize_data = nil
					@node_data = {}
					@node_id = nil
					@frame_data = {}
					@frame = nil
					@is_moving = false
					@my_vcb = ''
					view.lock_inference
					view.invalidate
				end
				def num_decode(key)
					case key
					when 48, 96
						return '0'
					when 49, 97
						return '1'
					when 50, 98
						return '2'
					when 51, 99
						return '3'
					when 52, 100
						return '4'
					when 53, 101
						return '5'
					when 54, 102
						return '6'
					when 55, 103
						return '7'
					when 56, 104
						return '8'
					when 57, 105
						return '9'
					when 190, 110
						return '.'
					else
						return ''
					end
				end
				def onKeyUp(key, repeat, flags, view)
					if key == 13
						distance = @my_vcb.to_s.to_l
						if !@resize_data.nil?
							if distance != 0
								return unless @frame
								unless @boxHelper && @boxHelper.selected_point
									return
								end
								get_resize_data(view, distance)
								resize_frame
								@my_vcb = ''
								@is_moving = false
							end
						end
					elsif key == 39 || key == 37 || key == 38
						if key == 39
							@resize_direction = LOCK_X
							@resize_data = nil
							@colorLock = 'red'
						elsif key == 37
							@resize_direction = LOCK_Y
							@colorLock = 'green'
						elsif key == 38
							@resize_direction = LOCK_Z
							@colorLock = 'blue'
						end
						@is_moving = false
						@boxHelper = nil
						@resize_data = nil
						if !@frame_data.empty?
							selection = Sketchup.active_model.selection
							rootFrame = @frame_data[:frame]
							@frame = rootFrame
							if @frame_data[:found_sub_group] == true
								frame = @frame_data[:sub_grp]
								trans_path = @frame_data[:path]
								@node_id = @frame_data[:node_id]
								sub_index = @frame_data[:sub_index]
								return unless frame && @node_id
								@node_data = {}
								grand_parent = {}
								tree_path = rootFrame.findPath(@node_id)
								if !tree_path.empty?
									@node_data = tree_path[tree_path.size - 1]
									grand_parent = tree_path[tree_path.size - 2]
								end
								if @node_data.empty? || grand_parent.empty?
									return
								end
								matrix = IDENTITY
								vertices = @node_data['boundingBox']['vertices']
								for i in (sub_index - 1).downto(0)
									if trans_path[i].is_a?(Sketchup::Group)
										matrix =
											matrix *
												trans_path[i].transformation
									end
								end
								@boxHelper = AmtfBox.new(vertices, matrix)
								selection.clear
								selection.add(frame) if frame
							elsif @frame_data[:found_frame] == true
								matrix = rootFrame.transformation
								vertices =
									rootFrame.root['boundingBox']['vertices']
								@boxHelper = AmtfBox.new(vertices, matrix)
								@node_id = nil
								selection.clear
								selection.add(@frame.frame)
							end
							@boxHelper.draw(view)
						end
						view.lock_inference
						view.invalidate
					end
					@my_vcb += num_decode(key)
					view.invalidate
					return true
				end
				def enableVCB?
					return true
				end
				def clear_my_vcb
					@my_vcb = ''
				end
				def onCancel(reason, view)
					cancel_tool
					@resize_data = nil
					@is_moving = false
					@boxHelper = nil
					@node_data = {}
					view.lock_inference
					view.invalidate
				end
				def get_resize_data(view, distance = nil)
					if @boxHelper.selected_point[:snap] == 'rightPoint'
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								true,
								distance
							)
						@offset_mark = RIGHT_OFFSET
					elsif @boxHelper.selected_point[:snap] == 'leftPoint'
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								false,
								distance
							)
						@offset_mark = LEFT_OFFSET
					elsif @boxHelper.selected_point[:snap] == 'bottomPoint'
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								false,
								distance
							)
						@offset_mark = BOTTOM_OFFSET
					elsif @boxHelper.selected_point[:snap] == 'topPoint'
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								true,
								distance
							)
						@offset_mark = TOP_OFFSET
					elsif @boxHelper.selected_point[:snap] == 'frontPoint'
						@resize_data =
							@boxHelper.resizeY(
								view,
								@moving_point,
								true,
								distance
							)
						@offset_mark = FRONT_OFFSET
					elsif @boxHelper.selected_point[:snap] == 'backPoint'
						@resize_data =
							@boxHelper.resizeY(
								view,
								@moving_point,
								false,
								distance
							)
						@offset_mark = BACK_OFFSET
					elsif @boxHelper.selected_point[:snap] == 'Conner0'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = LEFT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BACK_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BOTTOM_OFFSET
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner1'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = RIGHT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BACK_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BOTTOM_OFFSET
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner2'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = RIGHT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = FRONT_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BOTTOM_OFFSET
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner3'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = LEFT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = FRONT_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BOTTOM_OFFSET
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner4'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = LEFT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BACK_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = TOP_OFFSET
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner5'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = RIGHT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = BACK_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = TOP_OFFSET
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner6'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = RIGHT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = FRONT_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = TOP_OFFSET
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner7'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@offset_mark = LEFT_OFFSET
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = FRONT_OFFSET
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@offset_mark = TOP_OFFSET
						end
					end
				end
				def onMouseMove(flags, x, y, view)
					return unless @is_moving
					@mouse_ip.pick(view, x, y)
					@moving_point = @mouse_ip.position
					return unless @frame
					return unless @boxHelper && @boxHelper.selected_point
					get_resize_data(view)
					view.tooltip = @mouse_ip.tooltip if @mouse_ip.valid?
					view.invalidate
				end
				def cancel_tool
					@boxHelper = nil
					@frame = nil
					@resize_data = nil
					@node_id = nil
					@node_data = {}
					@is_moving = false
				end
				def resize_sub_frame
					return unless !@offset_mark.nil?
					frame_width = @resize_data[:frame_width]
					frame_height = @resize_data[:frame_height]
					frame_depth = @resize_data[:frame_depth]
					left_offset = 0
					right_offset = 0
					top_offset = 0
					bottom_offset = 0
					front_offset = 0
					back_offset = 0
					distance_factor = @resize_data[:distance_factor]
					distance = @resize_data[:distance]
					case @offset_mark
					when LEFT_OFFSET
						left_offset = distance.to_l
						distance_factor = distance_factor
					when RIGHT_OFFSET
						right_offset = distance.to_l
						distance_factor = -distance_factor
					when FRONT_OFFSET
						front_offset = distance.to_l
						distance_factor = distance_factor
					when BACK_OFFSET
						back_offset = distance.to_l
						distance_factor = -distance_factor
					when TOP_OFFSET
						top_offset = distance.to_l
						distance_factor = -distance_factor
					when BOTTOM_OFFSET
						bottom_offset = distance.to_l
						distance_factor = distance_factor
					end
					@subframe_adjust_attrs = {
						root: @frame.root,
						unit: AMTF_Dialog.get_model_units,
						node_id: @node_id,
						frame_width: frame_width.to_f,
						frame_height: frame_height.to_f,
						frame_depth: frame_depth.to_f,
						frame_ID: @frame.id,
						frame: @frame,
						left_offset: left_offset.to_f,
						right_offset: right_offset.to_f,
						top_offset: top_offset.to_f,
						bottom_offset: bottom_offset.to_f,
						front_offset: front_offset.to_f,
						back_offset: back_offset.to_f,
						distance_factor: distance_factor
					}
					AMTF_Dialog.dialog.execute_script(
						"subframe_resize_command('#{@subframe_adjust_attrs.to_json}');"
					)
					AMTF_Dialog
						.dialog
						.on(
							'subframe_adjust_update_frame'
						) do |deferred, params|
							begin
								Sketchup.active_model.start_operation(
									OB[:resize_frame],
									true
								)
								adjust_data[:frame].update(params)
								Sketchup.active_model.commit_operation
								deferred.resolve(true)
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
					cancel_tool
					@boxHelper = nil
					@resize_data = nil
					@is_moving = false
				end
				def resize_frame()
					return unless @resize_data
					if isExceeded
						cancel_tool
						return
					end
					if !@node_id.nil?
						resize_sub_frame
					else
						root = @frame.root
						frame_width = @resize_data[:frame_width]
						frame_height = @resize_data[:frame_height]
						frame_depth = @resize_data[:frame_depth]
						translate = @resize_data[:translate]
						@frame_adjust_attrs = {
							unit: AMTF_Dialog.get_model_units,
							root: root,
							frame_width: frame_width.to_f,
							frame_height: frame_height.to_f,
							frame_depth: frame_depth.to_f,
							translate: translate,
							frame_ID: @frame.id,
							frame: @frame
						}
						frame_adjust_command(@frame_adjust_attrs)
						@boxHelper = nil
						@resize_data = nil
						@is_moving = false
					end
				end
				def isExceeded
					frame_width = @frame.root['frame_width']
					frame_height = @frame.root['frame_height']
					frame_depth = @frame.root['frame_depth']
					if !@node_data.empty?
						frame_width =
							@node_data['frame_width'] -
								@node_data['left_offset'] -
								@node_data['right_offset']
						frame_height =
							@node_data['frame_height'] -
								@node_data['bottom_offset'] -
								@node_data['top_offset']
						frame_depth =
							@node_data['frame_depth'] -
								@node_data['front_offset'] -
								@node_data['back_offset']
					end
					distance = @resize_data[:distance]
					factor = @resize_data[:distance_factor]
					# Check di chuyển vượt ra ngoài phạm vi
					if factor < 0
						if @boxHelper.selected_point[:snap] == 'rightPoint'
							if distance >= Utilities.unitConvert(frame_width)
								return true
							end
						elsif @boxHelper.selected_point[:snap] == 'topPoint'
							if distance >= Utilities.unitConvert(frame_height)
								return true
							end
						elsif @boxHelper.selected_point[:snap] == 'backPoint'
							if distance >= Utilities.unitConvert(frame_depth)
								return true
							end
						end
					else
						if @boxHelper.selected_point[:snap] == 'leftPoint'
							if distance >= Utilities.unitConvert(frame_width)
								return true
							end
						elsif @boxHelper.selected_point[:snap] == 'bottomPoint'
							if distance >= Utilities.unitConvert(frame_height)
								return true
							end
						elsif @boxHelper.selected_point[:snap] == 'frontPoint'
							if distance >= Utilities.unitConvert(frame_depth)
								return true
							end
						end
					end
					if @resize_direction == LOCK_X
						if factor < 0
							if @boxHelper.selected_point[:snap] == 'Conner1' ||
									@boxHelper.selected_point[:snap] ==
										'Conner2' ||
									@boxHelper.selected_point[:snap] ==
										'Conner5' ||
									@boxHelper.selected_point[:snap] ==
										'Conner6'
								if distance >=
										Utilities.unitConvert(frame_width)
									return true
								end
							end
						else
							if @boxHelper.selected_point[:snap] == 'Conner0' ||
									@boxHelper.selected_point[:snap] ==
										'Conner3' ||
									@boxHelper.selected_point[:snap] ==
										'Conner4' ||
									@boxHelper.selected_point[:snap] ==
										'Conner7'
								if distance >=
										Utilities.unitConvert(frame_width)
									return true
								end
							end
						end
					elsif @resize_direction == LOCK_Y
						if factor > 0
							if @boxHelper.selected_point[:snap] == 'Conner2' ||
									@boxHelper.selected_point[:snap] ==
										'Conner3' ||
									@boxHelper.selected_point[:snap] ==
										'Conner6' ||
									@boxHelper.selected_point[:snap] ==
										'Conner7'
								if distance >=
										Utilities.unitConvert(frame_depth)
									return true
								end
							end
						else
							if @boxHelper.selected_point[:snap] == 'Conner0' ||
									@boxHelper.selected_point[:snap] ==
										'Conner1' ||
									@boxHelper.selected_point[:snap] ==
										'Conner4' ||
									@boxHelper.selected_point[:snap] ==
										'Conner5'
								if distance >=
										Utilities.unitConvert(frame_depth)
									return true
								end
							end
						end
					elsif @resize_direction == LOCK_Z
						if factor < 0
							if @boxHelper.selected_point[:snap] == 'Conner4' ||
									@boxHelper.selected_point[:snap] ==
										'Conner5' ||
									@boxHelper.selected_point[:snap] ==
										'Conner6' ||
									@boxHelper.selected_point[:snap] ==
										'Conner7'
								if distance >=
										Utilities.unitConvert(frame_height)
									return true
								end
							end
						else
							if @boxHelper.selected_point[:snap] == 'Conner0' ||
									@boxHelper.selected_point[:snap] ==
										'Conner1' ||
									@boxHelper.selected_point[:snap] ==
										'Conner2' ||
									@boxHelper.selected_point[:snap] ==
										'Conner3'
								if distance >=
										Utilities.unitConvert(frame_height)
									return true
								end
							end
						end
					end
					return false
				end
				def onLButtonDown(flags, x, y, view)
					selection = Sketchup.active_model.selection
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pick_point = @mouse_ip.position
					if !@frame
						@frame_data = pick_frame_data_3
						if !@frame_data.empty?
							rootFrame = @frame_data[:frame]
							@frame = rootFrame
							if @frame_data[:found_sub_group] == true
								frame = @frame_data[:sub_grp]
								trans_path = @frame_data[:path]
								@node_id = @frame_data[:node_id]
								sub_index = @frame_data[:sub_index]
								return unless frame && @node_id
								@node_data = {}
								grand_parent = {}
								tree_path = rootFrame.findPath(@node_id)
								if !tree_path.empty?
									@node_data = tree_path[tree_path.size - 1]
									grand_parent = tree_path[tree_path.size - 2]
								end
								if @node_data.empty? || grand_parent.empty?
									return
								end
								matrix = IDENTITY
								vertices = @node_data['boundingBox']['vertices']
								for i in (sub_index - 1).downto(0)
									if trans_path[i].is_a?(Sketchup::Group)
										matrix =
											matrix *
												trans_path[i].transformation
									end
								end
								@boxHelper = AmtfBox.new(vertices, matrix)
								selection.clear
								selection.add(frame)
							elsif @frame_data[:found_frame] == true
								matrix = rootFrame.transformation
								vertices =
									rootFrame.root['boundingBox']['vertices']
								@boxHelper = AmtfBox.new(vertices, matrix)
								@node_id = nil
								selection.clear
								selection.add(@frame.frame)
							end
							@boxHelper.draw(view)
						end
						@is_moving = false
					else
						if @boxHelper && !@resize_data
							@boxHelper.pick_scale_point(view, pick_point)
							@is_moving = true
						else
							resize_frame
							@is_moving = false
							view.lock_inference
						end
					end
					view.invalidate
				end
				def getExtents
					bb = Geom::BoundingBox.new
					bb.add(@mouse_ip.position)
					bb
				end
				def draw(view)
					@boxHelper.draw(view) if @boxHelper
					@mouse_ip.draw(view) if @mouse_ip.display?
				end
				def frame_adjust_command(adjust_data)
					AMTF_Dialog.dialog.execute_script(
						"frame_adjust_command('#{adjust_data.to_json}');"
					)
					AMTF_Dialog
						.dialog
						.on('frame_adjust_update_frame') do |deferred, params|
							begin
								Sketchup.active_model.start_operation(
									OB[:resize_frame],
									true
								)
								adjust_data[:frame].update(params)
								adjust_data[:frame].frame.transform!(
									adjust_data[:translate]
								)
								matrix = adjust_data[:frame].transformation
								vertices =
									adjust_data[:frame].root['boundingBox'][
										'vertices'
									]
								@boxHelper = AmtfBox.new(vertices, matrix)
								Sketchup.active_model.commit_operation
								deferred.resolve(true)
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #