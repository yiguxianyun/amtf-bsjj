# encoding: UTF-8
# Ch�?cần m�?comment là chạy đúng ngay phần chọn đối tượng
module AMTF
  module BUKONG
  module EditFrameModule
    class EditFrame < Tools::Base
      STS_CLASS_NAME   = OB[:properties].freeze  
      def activate   
        @@active_tool_class = self.class   
        @mouse_ip  = Sketchup::InputPoint.new
        Sketchup.active_model.selection.clear
        Sketchup.status_text = OB[:select_target_object]         
        @frame = nil
        @is_moving = true
        rootFrame = nil 
        section_id = nil
        if AMTF_STORE.init_open_json  == false
            saver = AmtfSaver.new
            saver.load_comp()
            AMTF_STORE.init_open_json = true
        end    
      end
      def reset_tool           
        Sketchup.active_model.selection.clear
      end
      def deactivate(_view)
        @@active_tool_class = nil        
        @boundConners = []
        @frame = nil
        rootFrame = nil 
        section_id = nil 
      end  
      def onLButtonUp( flags, x, y, view )
        @is_moving = false
        @boxHelper = nil
        selection = Sketchup.active_model.selection      
        selection.clear 
        section_id = nil       
        node_id = nil
        return if @is_moving
        @ph.do_pick(x, y)
        pick_data = pick_frame_data_3()  
        if pick_data.nil? || pick_data.empty?
          @is_moving = true
          return 
        end
        rootFrame = pick_data[:frame]       
        frame = pick_data[:sub_grp]       
        return if rootFrame.nil? 
        sub_root = {}  
        if frame
          selection.add(frame)
          sub_root = pick_data[:sub_root]
          section_id = pick_data[:section_id]  
          node_id = pick_data[:node_id] 
        else
          selection.add(rootFrame.frame)
        end  
        data = {
          :root => rootFrame.root,
          :frame_ID => rootFrame.id,
          :node_id =>node_id,
          :sub_root => sub_root
        }  
        AMTF_Dialog.dialog.execute_script("edit_frame_send_data(#{data.to_json});")
        view.refresh
        view.invalidate
        true
      end
      def onMouseMove( flags, x, y, view )
          # return if !@is_moving 
          # @boxHelper = nil
          # @ph.do_pick(x, y)
          # pick_data = pick_frame_data_3()  
          # return if pick_data.nil? || pick_data.empty?
          # rootFrame = pick_data[:frame]
          # if pick_data[:found_sub_group] == true
          #   frame = pick_data[:sub_grp]      
          #   trans_path = pick_data[:path] 
          #   section_id = pick_data[:section_id] 
          #   sub_index   = pick_data[:sub_index] 
          #   return unless frame && section_id
          #   section_data = {}
          #   grand_parent  = {}
          #   tree_path = rootFrame.findPath(section_id)
          #   if !tree_path.empty?
          #       section_data = tree_path[tree_path.size - 1]
          #       grand_parent = tree_path[tree_path.size - 2]
          #   end
          #   return if section_data.empty? || grand_parent.empty?              
          #   matrix = IDENTITY
          #   vertices = boxCorners( section_data )  
          #   for i in (sub_index - 1).downto(0)                                    
          #       if trans_path[i].is_a?(Sketchup::Group)
          #           matrix = matrix*trans_path[i].transformation
          #       end
          #   end
          #   @boxHelper = AmtfBox.new( vertices, matrix, true )
          # elsif pick_data[:found_frame] == true
          #   matrix = rootFrame.transformation
          #   vertices = rootFrame.root['boundingBox']['vertices']
          #   @boxHelper = AmtfBox.new( vertices, matrix )
          # end
          # view.invalidate
          # true
      end
      # def onCancel(reason, view)
      #   @is_moving = true
      # end
      #  def getExtents
      #       bb = Geom::BoundingBox.new
      #       bb.add(@mouse_ip.position)
      #       bb      
      #   end  
      #   def draw(view)
      #       @boxHelper.draw(view ) if @boxHelper
      #   end
    def boxCorners(section_root)     
          org = Geom::Point3d.new Utilities.unitConvert(section_root['origin']['x']), 
                                      Utilities.unitConvert(section_root['origin']['y']), 
                                      Utilities.unitConvert(section_root['origin']['z']);
          w = Utilities.unitConvert(section_root['width2d'])
          h = Utilities.unitConvert(section_root['height2d'])
          d = Utilities.unitConvert(section_root['depth'])
          box = []
          box[ 0 ] = Geom::Point3d.new org.x, org.y + d, org.z
          box[ 1 ]  = Geom::Point3d.new org.x + w, org.y + d, org.z
          box[ 2 ]  = Geom::Point3d.new org.x + w, org.y, org.z
          box[ 3 ]  = Geom::Point3d.new org.x , org.y, org.z
          box[ 4 ] = Geom::Point3d.new org.x, org.y + d, org.z + h
          box[ 5 ]  = Geom::Point3d.new org.x + w, org.y + d, org.z + h
          box[ 6 ]  = Geom::Point3d.new org.x + w, org.y, org.z + h
          box[ 7 ]  = Geom::Point3d.new org.x , org.y, org.z + h
          return box                                
      end
  end#Class
  end#  OffsetToolModule
  end # BuKong
end # 