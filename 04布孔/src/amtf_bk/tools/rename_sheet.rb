# encoding: UTF-8
module AMTF
	module BUKONG
		module RenameSheet
			class RenameSheetTool < Tools::Base
				STS_CLASS_NAME = OB[:rename].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					@hold_shift_key = false
					@sheet_list = []
					AMTF_Dialog
						.dialog
						.on('execute_rename_sheets') do |deferred, name|
							begin
								if @sheet_list.size > 0 && name
									hash = {}
									@sheet_list.each do |sheet|
										frame = sheet[:frame]
										sheet_id = sheet[:sheet_id]
										if hash.key?(frame)
											hash[frame] << sheet_id
										else
											hash[frame] = [sheet_id]
										end
									end
									hash.each do |frame, list|
										attrs = frame.attrs
										list.each do |sheet_id|
											node_queue = [attrs]
											until node_queue.empty?
												node = node_queue.shift
												if node['root'] &&
														node['root']['sheets']
													node['root']['sheets']
														.each do |k, sheet_arr|
														sheet_arr.each do |obj|
															if obj['root'][
																	'id'
															   ] == sheet_id
																obj['root'][
																	'sheet_name'
																] =
																	name
															end
														end
													end
												end
												if node['root'] &&
														node['root']['sections']
													node_queue =
														node_queue.concat(
															node['root'][
																'sections'
															]
														)
												end
												if node['root'] &&
														node['root']['sub_part']
													node_queue =
														node_queue.concat(
															node['root'][
																'sub_part'
															]
														)
												end
											end
										end
										saver = AmtfSaver.new
										saver.save_comp_json(frame.frame, attrs)
									end
									deferred.resolve(true)
								end
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
				end
				def deactivate(view)
					@@active_tool_class = nil
					@sheet_list = []
					view.lock_inference
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					selection = Sketchup.active_model.selection
					pick_data = pick_frame_data_2
					if pick_data && pick_data[:found_sheet] == true
						sheet_id = pick_data[:sheet_id]
						frame = pick_data[:frame]
						pick_component = pick_data[:sheet]
						return unless frame && sheet_id
						root_data = {
							frame: frame,
							sheet_id: pick_data[:sheet_id],
							pick_component: pick_component
						}
						if @hold_shift_key == true
							@sheet_list << root_data
							selection.add(pick_component)
						else
							@sheet_list = [root_data]
							selection.clear
							selection.add(pick_component)
						end
					end
				end
				def onKeyDown(key, repeat, flags, view)
					@hold_shift_key = true if key == VK_SHIFT
				end
				def onKeyUp(key, repeat, flags, view)
					@hold_shift_key = false if key == VK_SHIFT
				end
			end # NameTool
		end
	end
end