# encoding: UTF-8
module AMTF
	module BUKONG
		module ReverseToolModule
			class ReverseTool < Tools::Base
				STS_CLASS_NAME = OB[:reverse_tool].freeze
				def activate
					AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
					@@active_tool_class = self.class
					@input = Sketchup::InputPoint.new
					Sketchup.status_text = OB[:select_target_object]
					@center_points = []
					@selectedCenter = nil
					@helper_point = nil
					@first_pick = false
					@pick_data = {}
					@frame = nil
				end
				def deactivate(view)
					@center_points = []
					@selectedCenter = nil
					@helper_point = nil
					@pick_data = {}
					@first_pick = false
					@@active_tool_class = nil
					@frame = nil
					view.lock_inference
					view.invalidate
				end
				def reset_tool
					@move_data = nil
					Sketchup.status_text = OB[:select_target_object]
				end
				def resume(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def draw(view)
					view.drawing_color = 'yellow'
					view.line_width = 3
					if @selectedCenter
						view.draw_points(
							@selectedCenter,
							size = 10,
							style = 2,
							color = 'yellow'
						)
					end
					if @center_points.size > 0
						@center_points.each do |p|
							view.draw_points(
								p,
								size = 10,
								style = 2,
								color = 'yellow'
							)
						end
					end
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					@first_pick = !@first_pick
					model = Sketchup.active_model
					model.start_operation(OB[:side_reverse], true)
					@input.pick view, x, y
					pick_post = @input.position
					pick = pick_sheet(@ph, x, y)
					@frame = pick[:frame]
					return if pick.empty?
					pick_component = pick[:pick_component]
					return unless pick_component
					if @first_pick
						@center_points = []
						@selectedCenter = nil
						@helper_point = nil
						@pick_data = {}
						amtf_sheet =
							AmtfSheet.new(
								pick_component,
								pick[:trans],
								pick[:parent_matrix]
							)
						select_list = amtf_sheet.grep_connector
						cabineo_cap = amtf_sheet.grep_cabineo_cap
						select_list = select_list.concat(cabineo_cap)
						dist_sort =
							select_list.sort do |a, b|
								p1 =
									Utilities.point_context_convert(
										a[:hole],
										pick[:trans],
										IDENTITY
									)
								p2 =
									Utilities.point_context_convert(
										b[:hole],
										pick[:trans],
										IDENTITY
									)
								pick_post.distance(p1) <=>
									pick_post.distance(p2)
							end
						return if dist_sort.size == 0
						pick_minifix = dist_sort[0][:instance]
						@selectedCenter =
							Utilities.point_context_convert(
								dist_sort[0][:hole],
								pick[:trans],
								IDENTITY
							)
						type = pick_minifix.get_attribute('amtf_dict', 'type')
						if type && type.include?('hinge')
							@helper_point = nil
							@selectedCenter = nil
							return
						end
						if %w[
								chot_dot
								khoan_moi
								dogbone_female
								dogbone
								patV
								drawer_fitting_1
								drawer_fitting_2
						   ].include?(type)
							@helper_point = nil
							@selectedCenter = nil
							return
						end
						if type == 'chot_cam' &&
								pick_minifix.get_attribute(
									'amtf_dict',
									'dogbone'
								)
							@helper_point = nil
							@selectedCenter = nil
							return
						end
						if pick_minifix.get_attribute(
								'amtf_dict',
								'cabineo_cap'
						   )
							@helper_point = nil
							@selectedCenter = nil
							return
						end
						if %w[
								oc_cam
								rafix_d2
								rafix
								chot_dot
								chot_go3
								chot_go2
								chot_cam
								cabineo_d
								cabineo_d1
								cabineo_d2
								cabineo_d3
								cabineo_cap
						   ].include?(type)
							related = find_related(pick, pick_minifix)
							return if !related
							related[:same_sheet].each do |mini|
								@center_points <<
									Utilities.point_context_convert(
										mini[:hole],
										mini[:trans],
										IDENTITY
									)
							end
							related[:partner_sheet].each do |mini|
								@center_points <<
									Utilities.point_context_convert(
										mini[:hole],
										mini[:trans],
										IDENTITY
									)
							end
							@pick_data = {
								pick_component: pick_component,
								miniTrans: dist_sort[0][:miniTrans],
								pick_minifix: pick_minifix,
								same_sheet: related[:same_sheet],
								partner_sheet: related[:partner_sheet],
								pick_face: pick[:pick_face],
								pick_post: dist_sort[0][:hole],
								trans: pick[:trans],
								amtf_sheet: amtf_sheet
							}
						end
					else
						if !@pick_data.empty?
							amtf_sheet = @pick_data[:amtf_sheet]
							thickness = amtf_sheet.sheet_thick
							big_faces = amtf_sheet.big_faces
							oposit_face = nil
							if @pick_data[:pick_face] == big_faces[0]
								oposit_face = big_faces[1]
							elsif @pick_data[:pick_face] == big_faces[1]
								oposit_face = big_faces[0]
							end
							return if oposit_face.nil?
							Sketchup.active_model.start_operation(
								OB[:side_reverse],
								true
							)
							point1 = @pick_data[:pick_post]
							point2 = nil
							z_axis = nil
							if Utilities.within_face?(
									point1,
									@pick_data[:pick_face]
							   )
								point2 =
									point1.project_to_plane(oposit_face.plane)
								z_axis = oposit_face.normal
							elsif Utilities.within_face?(point1, oposit_face)
								point2 =
									point1.project_to_plane(
										@pick_data[:pick_face].plane
									)
								z_axis = @pick_data[:pick_face].normal
							else
								return
							end
							move_vector = point1.vector_to(point2)
							@pick_data[:same_sheet].each do |mini|
								org = mini[:hole]
								minifixID = mini[:minifixID]
								connector_type =
									mini[:instance].get_attribute(
										'amtf_dict',
										'type'
									)
								if %w[
										chot_cam
										chot_go2
										chot_go3
										rafix_d1
										cabineo_d
								   ].include?(connector_type)
									next
								end
								pairs =
									mini[:instance].get_attribute(
										'amtf_dict',
										'pairs'
									)
								mini[:instance].transform!(
									Geom::Transformation.translation move_vector
								)
								if @frame
									@frame.reverse_connector(
										connector_type,
										pairs,
										minifixID,
										Utilities.vector_context_convert(
											z_axis,
											@pick_data[:trans],
											@frame.transformation
										)
									)
								end
							end
							@pick_data[:partner_sheet].each do |mini|
								connector_id =
									mini[:instance].get_attribute(
										'amtf_dict',
										'connector_type_id'
									)
								connector_type =
									mini[:instance].get_attribute(
										'amtf_dict',
										'type'
									)
								config_data = JSON.parse(mini[:config_data])
								if %w[chot_cam chot_go2 chot_go3].include?(
										connector_type
								   )
									mov_distance =
										thickness -
											2 *
												config_data['distance_h'].to_s
													.to_l
									vect =
										Utilities.vector_multiply(
											mov_distance,
											move_vector
										)
									translation =
										Geom::Transformation.translation vect
									mini[:instance].transform!(translation)
								elsif %w[rafix_d1].include?(connector_type)
									mov_distance =
										thickness -
											2 *
												config_data['distance_l1'].to_s
													.to_l
									vect =
										Utilities.vector_multiply(
											mov_distance,
											move_vector
										)
									translation =
										Geom::Transformation.translation vect
									mini[:instance].transform!(translation)
								elsif %w[cabineo_d].include?(connector_type)
									mov_distance =
										thickness -
											2 *
												config_data['distance_l4'].to_s
													.to_l
									vect =
										Utilities.vector_multiply(
											mov_distance,
											move_vector
										)
									translation =
										Geom::Transformation.translation vect
									mini[:instance].transform!(translation)
								end
							end
							@center_points = []
							@selectedCenter = nil
							@helper_point = nil
							@pick_data = {}
							Sketchup.active_model.commit_operation
						end
					end
					view.lock_inference
					view.invalidate
				end
				private
				def find_related(pick, pick_minifix)
					minifixID =
						pick_minifix.get_attribute('amtf_dict', 'minifixID')
					same_sheet = []
					partner_sheet = []
					sheets = {}
					query = AmtfQuery.new
					minifixList =
						query.minifixList(Sketchup.active_model.entities)
					minifixList.each do |mini|
						if minifixID == mini[:minifixID]
							if mini[:parent] == pick[:pick_component]
								same_sheet << mini
							else
								sheets[mini[:parent]] = {
									pick_component: mini[:parent],
									parent_matrix: mini[:grand_parent_matrix],
									trans: mini[:trans]
								}
							end
						end
					end
					check_result = []
					sheets.each do |key, partner|
						make_hole_data = { pick_data: pick, last_pick: partner }
						checker = AmtfMakeHole.new(make_hole_data)
						check_data = checker.check_intersect
						if check_data
							check_result << {
								partner: partner[:pick_component]
							}
						end
					end
					result = check_result[0]
					minifixList.each do |mini|
						next unless mini && !mini.nil?
						if minifixID == mini[:minifixID]
							if mini[:parent] == result[:partner]
								partner_sheet << mini
							end
						end
					end
					return(
						{ partner_sheet: partner_sheet, same_sheet: same_sheet }
					)
				end
			end # Reverse
			class ReverseAllTool < Tools::Base
				STS_CLASS_NAME = OB[:reverse_all_tool].freeze
				def activate
					@@active_tool_class = self.class
					@input = Sketchup::InputPoint.new
					AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
				end
				def deactivate(view)
					@@active_tool_class = nil
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					pick_data = {}
					mod = Sketchup.active_model
					mod.start_operation(OB[:reverse_all_tool], true)
					@input = Sketchup::InputPoint.new
					@input.pick view, x, y
					pick = pick_sheet(@ph, x, y)
					@frame = pick[:frame]
					return if pick.empty?
					pick_component = pick[:pick_component]
					return unless pick_component
					amtf_sheet =
						AmtfSheet.new(
							pick_component,
							pick[:trans],
							pick[:parent_matrix]
						)
					select_list = amtf_sheet.grep_connector
					cabineo_cap = amtf_sheet.grep_cabineo_cap
					select_list = select_list.concat(cabineo_cap)
					pick_datas = []
					select_list.each do |mini|
						pick_minifix = mini[:instance]
						next unless pick_minifix && pick_minifix.valid?
						type =
							mini[:instance].get_attribute('amtf_dict', 'type')
						if %w[
								oc_cam
								rafix_d2
								rafix_d
								cabineo_d1
								cabineo_d2
								cabineo_d3
						   ].include?(type) &&
								Utilities.within_face?(
									mini[:hole],
									pick[:pick_face]
								)
							related = find_related(pick, pick_minifix)
							next if !related
							next if related[:same_sheet].size == 0
							pick_datas << {
								pick_component: pick_component,
								pick_minifix: pick_minifix,
								same_sheet: related[:same_sheet],
								partner_sheet: related[:partner_sheet],
								pick_face: pick[:pick_face],
								pick_post: related[:same_sheet][0][:hole],
								trans: pick[:trans],
								amtf_sheet: amtf_sheet
							}
						end
					end
					visited = []
					pick_datas.each do |pick_data|
						if !pick_data.empty?
							amtf_sheet = pick_data[:amtf_sheet]
							thickness = amtf_sheet.sheet_thick
							big_faces = amtf_sheet.big_faces
							oposit_face = nil
							if pick_data[:pick_face] == big_faces[0]
								oposit_face = big_faces[1]
							elsif pick_data[:pick_face] == big_faces[1]
								oposit_face = big_faces[0]
							end
							return if oposit_face.nil?
							point1 = pick_data[:pick_post]
							point2 = nil
							z_axis = nil
							if Utilities.within_face?(
									point1,
									pick_data[:pick_face]
							   )
								point2 =
									point1.project_to_plane(oposit_face.plane)
								z_axis = oposit_face.normal
							elsif Utilities.within_face?(point1, oposit_face)
								point2 =
									point1.project_to_plane(
										pick_data[:pick_face].plane
									)
								z_axis = pick_data[:pick_face].normal
							else
								next
							end
							move_vector = point1.vector_to(point2)
							pick_data[:same_sheet].each do |mini|
								next if visited.include?(mini[:instance])
								org = mini[:hole]
								minifixID = mini[:minifixID]
								connector_type =
									mini[:instance].get_attribute(
										'amtf_dict',
										'type'
									)
								if %w[
										chot_cam
										chot_go2
										chot_go3
										rafix_d1
										cabineo_d
								   ].include?(connector_type)
									next
								end
								pairs =
									mini[:instance].get_attribute(
										'amtf_dict',
										'pairs'
									)
								mini[:instance].transform!(
									Geom::Transformation.new(move_vector)
								)
								visited << mini[:instance]
								if @frame
									@frame.reverse_connector(
										connector_type,
										pairs,
										minifixID,
										Utilities.vector_context_convert(
											z_axis,
											pick_data[:trans],
											@frame.transformation
										)
									)
								end
							end
							pick_data[:partner_sheet].each do |mini|
								next if visited.include?(mini[:instance])
								connector_id =
									mini[:instance].get_attribute(
										'amtf_dict',
										'connector_type_id'
									)
								connector_type =
									mini[:instance].get_attribute(
										'amtf_dict',
										'type'
									)
								config_data = JSON.parse(mini[:config_data])
								if %w[chot_cam chot_go2 chot_go3].include?(
										connector_type
								   )
									mov_distance =
										thickness -
											2 *
												config_data['distance_h'].to_s
													.to_l
									vect =
										Utilities.vector_multiply(
											mov_distance,
											move_vector
										)
									translation =
										Geom::Transformation.translation vect
									mini[:instance].transform!(translation)
									visited << mini[:instance]
								elsif %w[rafix_d1].include?(connector_type)
									mov_distance =
										thickness -
											2 *
												config_data['distance_l1'].to_s
													.to_l
									vect =
										Utilities.vector_multiply(
											mov_distance,
											move_vector
										)
									translation =
										Geom::Transformation.translation vect
									mini[:instance].transform!(translation)
									visited << mini[:instance]
								elsif %w[cabineo_d].include?(connector_type)
									mov_distance =
										thickness -
											2 *
												config_data['distance_l4'].to_s
													.to_l
									vect =
										Utilities.vector_multiply(
											mov_distance,
											move_vector
										)
									translation =
										Geom::Transformation.translation vect
									mini[:instance].transform!(translation)
									visited << mini[:instance]
								end
							end
						end
					end
					mod.commit_operation
					view.invalidate
				end
				def find_related(pick, pick_minifix)
					minifixID =
						pick_minifix.get_attribute('amtf_dict', 'minifixID')
					same_sheet = []
					partner_sheet = []
					sheets = {}
					query = AmtfQuery.new
					minifixList =
						query.minifixList(Sketchup.active_model.entities)
					minifixList.each do |mini|
						if minifixID == mini[:minifixID]
							if mini[:parent] == pick[:pick_component] &&
									!same_sheet.include?(mini)
								same_sheet << mini
							else
								sheets[mini[:parent]] = {
									pick_component: mini[:parent],
									parent_matrix: mini[:grand_parent_matrix],
									trans: mini[:trans]
								}
							end
						end
					end
					check_result = []
					sheets.each do |key, partner|
						make_hole_data = { pick_data: pick, last_pick: partner }
						checker = AmtfMakeHole.new(make_hole_data)
						check_data = checker.check_intersect
						if check_data
							check_result << {
								partner: partner[:pick_component]
							}
						end
					end
					result = check_result[0]
					minifixList.each do |mini|
						next unless mini && !mini.nil?
						if minifixID == mini[:minifixID]
							if mini[:parent] == result[:partner]
								partner_sheet << mini
							end
						end
					end
					return(
						{ partner_sheet: partner_sheet, same_sheet: same_sheet }
					)
				end
			end #ReverseAllTool
		end #  MovingTool
	end # BuKong
end #