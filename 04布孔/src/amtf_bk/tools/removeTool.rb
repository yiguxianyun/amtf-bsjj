# encoding: UTF-8
module AMTF
	module BUKONG
		module RemoveToolsModule
			class RemoveTool < Tools::Base
				STS_CLASS_NAME = OB[:remove_tool].freeze
				def activate
					@@active_tool_class = self.class
					@input = Sketchup::InputPoint.new
					Sketchup.status_text = OB[:select_target_object]
					@center_points = []
					@selectedCenter = nil
					@helper_point = nil
					@first_pick = false
					@pick_data = {}
					AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
				end
				def deactivate(view)
					@@active_tool_class = nil
					@center_points = []
					@selectedCenter = nil
					@helper_point = nil
					@pick_data = {}
					@first_pick = false
					view.lock_inference
					view.invalidate
				end
				def reset_tool
					@move_data = nil
					Sketchup.status_text = OB[:select_target_object]
				end
				def resume(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def draw(view)
					view.drawing_color = 'yellow'
					view.line_width = 3
					if @selectedCenter
						view.draw_points(
							@selectedCenter,
							size = 20,
							style = 4,
							color = 'yellow'
						)
					end
					if @center_points.size > 0
						@center_points.each do |p|
							view.draw_points(
								p,
								size = 20,
								style = 4,
								color = 'yellow'
							)
						end
					end
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					# puts "onbuttondown 👉 #{onbuttondown}" 
					@first_pick = !@first_pick
					model = Sketchup.active_model
					model.start_operation(OB[:move_tool], true)
					@input.pick view, x, y
					pick_post = @input.position
					pick = pick_sheet(@ph, x, y)
					@frame = pick[:frame]
					puts "pick.empty? 👉 #{pick.empty?}" 
					return if pick.empty?
					pick_component = pick[:pick_component]
					puts "pick_component 👉 #{pick_component.name}" 
					return unless pick_component
					if @first_pick
						@center_points = []
						@selectedCenter = nil
						@helper_point = nil
						@pick_data = {}
						amtf_sheet =AmtfSheet.new(pick_component,pick[:trans],pick[:parent_matrix])
						select_list = amtf_sheet.grep_connector
						puts "select_list 👉 #{select_list}" 
						dogbone = amtf_sheet.grep_dogbone
						cabineo_cap = amtf_sheet.grep_cabineo_cap
						select_list = select_list.concat(dogbone[:female])
						select_list = select_list.concat(cabineo_cap)
						select_list =select_list.concat(amtf_sheet.grep_lockdowel_as_hole)
						dist_sort = select_list.sort { |a, b|
							p1 = Utilities.point_context_convert(a[:hole], pick[:trans], IDENTITY)
							p2 = Utilities.point_context_convert(b[:hole], pick[:trans], IDENTITY)
							pick_post.distance(p1) <=> pick_post.distance(p2)
						  }
						puts "dist_sort.size == 0 👉 #{dist_sort.size == 0}" 
						return if dist_sort.size == 0
						# 挑出离选择点最近的连接件实例
						pick_minifix = dist_sort[0][:instance]
						@selectedCenter =
							Utilities.point_context_convert(
								dist_sort[0][:hole],
								pick[:trans],
								IDENTITY
							)
						type = pick_minifix.get_attribute('amtf_dict', 'type')
						# Do dogbone cung co chot_cam, return de tranh nham
						puts "type 👉 #{type}" 
						if type == 'chot_cam'
							if pick_minifix.get_attribute('amtf_dict','dogbone')
								type = 'dogbone'
							end
						end
						if pick_minifix.get_attribute('amtf_dict','cabineo_cap')
							type = 'cabineo_cap'
						end
						if %w[
								oc_cam
								rafix_d2
								rafix
								dogbone_female
								chot_dot
								chot_go3
								chot_go2
								chot_cam
								cabineo_d
								cabineo_d1
								cabineo_d2
								cabineo_d3
								cabineo_cap
								dogbone
								khoan_moi
								patV
								drawer_fitting_1
								drawer_fitting_2
						   ].include?(type) || (type && type.include?('hinge'))
							#查找板件中的连接件👇  
							related = find_related(pick, pick_minifix, type)
							return if !related
							related[:same_sheet].each do |mini|
								@center_points <<
									Utilities.point_context_convert(
										mini[:hole],
										mini[:trans],
										IDENTITY
									)
							end
							related[:partner_sheet].each do |mini|
								@center_points <<
									Utilities.point_context_convert(
										mini[:hole],
										mini[:trans],
										IDENTITY
									)
							end
							@pick_data = {
								pick_component: pick_component,
								miniTrans: dist_sort[0][:miniTrans],
								pick_minifix: pick_minifix,
								same_sheet: related[:same_sheet],
								partner_sheet: related[:partner_sheet],
								pick_face: pick[:pick_face],
								pick_post: pick_post,
								trans: pick[:trans],
								amtf_sheet: amtf_sheet
							}
						elsif type == 'lockdowel'
							center_faces =
								Utilities
									.definition(pick_minifix)
									.entities
									.grep(Sketchup::Face)
							ctr1 = nil
							ctr2 = nil
							ctr3 = nil
							if center_faces.size > 0
								tr = pick[:trans] * pick_minifix.transformation
								center_faces.each do |cp|
									type = cp.get_attribute('amtf_dict', 'type')
									case type
									when 'point_1'
										ctr1 =
											Utilities.point_context_convert(
												Utilities.find_center_face(cp),
												tr,
												IDENTITY
											)
										@center_points << ctr1
									when 'point_2'
										ctr2 =
											Utilities.point_context_convert(
												Utilities.find_center_face(cp),
												tr,
												IDENTITY
											)
										@center_points << ctr2
									when 'point_3'
										ctr3 =
											Utilities.point_context_convert(
												Utilities.find_center_face(cp),
												tr,
												IDENTITY
											)
										@center_points << ctr3
									end
								end
							end
							return unless !ctr1.nil? && !ctr2.nil? && !ctr3.nil?
							@pick_data = {
								pick_component: pick_component,
								miniTrans: dist_sort[0][:miniTrans],
								pick_minifix: pick_minifix,
								same_sheet: [
									{
										instance: pick_minifix,
										trans: pick[:trans]
									}
								],
								partner_sheet: [],
								pick_face: pick[:pick_face],
								pick_post: pick_post,
								trans: pick[:trans],
								amtf_sheet: amtf_sheet
							}
						end
					else #第二次点按钮，开始删除
						if !@pick_data.empty?
							amtf_sheet = @pick_data[:amtf_sheet]
							Sketchup.active_model.start_operation(
								'Remove',
								true
							)
							if @frame
								connector_type =
									@pick_data[:pick_minifix].get_attribute(
										'amtf_dict',
										'type'
									)
								pairs =
									@pick_data[:pick_minifix].get_attribute(
										'amtf_dict',
										'pairs'
									)
								minifixID =
									@pick_data[:pick_minifix].get_attribute(
										'amtf_dict',
										'minifixID'
									)
								@frame.remove_connector(
									connector_type,
									pairs,
									minifixID
								)
							end
							@pick_data[:same_sheet].each do |mini|
								mini[:instance].erase!
							end
							@pick_data[:partner_sheet].each do |mini|
								mini[:instance].erase!
							end
							@center_points = []
							@selectedCenter = nil
							@helper_point = nil
							@pick_data = {}
							Sketchup.active_model.commit_operation
						end
					end
					view.lock_inference
					view.invalidate
				end
				private

				def find_related(pick, pick_minifix, type)
					minifixID =pick_minifix.get_attribute('amtf_dict', 'minifixID')
					same_sheet = []
					partner_sheet = []
					sheets = {}
					query = AmtfQuery.new
					minifixList =query.minifixList(Sketchup.active_model.entities)
					minifixList.each do |mini|
						if minifixID == mini[:minifixID]
							if mini[:parent] == pick[:pick_component]
								same_sheet << mini
							else
								sheets[mini[:parent]] = {
									pick_component: mini[:parent],
									parent_matrix: mini[:grand_parent_matrix],
									trans: mini[:trans]
								}
							end
						end
					end
					check_result = []
					if %w[
							chot_dot
							khoan_moi
							lockdowel
							drawer_fitting_1
							drawer_fitting_2
					   ].include?(type)
						return { partner_sheet: [], same_sheet: same_sheet }
					end
					# partner板件可能有多个
					sheets.each do |key, partner|
						make_hole_data = { pick_data: pick, last_pick: partner }
						checker = AmtfMakeHole.new(make_hole_data)
						check_data = checker.check_intersect
						if check_data
							check_result << {
								partner: partner[:pick_component]
							}
						end
					end
					# 只取其中一个板件？
					result = check_result[0]
					minifixList.each do |mini|
						next unless mini && !mini.nil?
						if minifixID == mini[:minifixID]
							if mini[:parent] == result[:partner]
								partner_sheet << mini
							end
						end
					end
					return(
						{ partner_sheet: partner_sheet, same_sheet: same_sheet }
					)
				end
			end #MoveHoles Class
		end #  MovingTool
	end # BuKong
end #