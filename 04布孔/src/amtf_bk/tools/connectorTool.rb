# encoding: UTF-8
module AMTF
	module BUKONG
		module ConnectorTool
			class A连接件 < Tools::Base
				include AMTF_bk_mixin
				STS_CLASS_NAME = "连接件".freeze
				def activate
					@@active_tool_class = self.class
					@mouse_ip = Sketchup::InputPoint.new
					@ph = Sketchup.active_model.active_view.pick_helper
					@first_picked = false
					@last_pick = {}
					@总设置=读取设置
					@config_data=@总设置["连接件"]
					# puts @config_data
					@frame = nil
					if !@dialog.nil? && @dialog.visible?
						@dialog.bring_to_front
					else
						@dialog = create_dialog
					end
					# deferred=推迟; 延缓; 展期;
					@dialog.on('获取保存参数') do |deferred, data|
						begin
							puts "获取保存参数"
							@config_data = data
							@connector_id = data['connector_id']
							@connector_type = data['connector_type']
							@all_edge_banding = data['all_edge_banding']
							# if data['width'] && data['height']
							# 	@dialog.set_size(
							# 		data['width'].to_i,
							# 		data['height'].to_i
							# 	)
							# end
							# deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
				end

				def 布置连接件(flags, x, y, view)
					mod = Sketchup.active_model
					selection = Sketchup.active_model.selection
					entitites = mod.active_entities
					@mouse_ip.pick view, x, y
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					@first_picked = !@first_picked
					selection.clear
					selection.add(pick_data[:pick_component])
					pick_data[:position] = @mouse_ip.position
					if @first_picked
						@last_pick = pick_data
						@make_hole_data = {}
					elsif !@last_pick.empty?
						if @last_pick[:pick_component] ==pick_data[:pick_component]
							return nil
						end
						@make_hole_data = {
							pick_data: pick_data,
							last_pick: @last_pick,
							frame: pick_data[:frame]
						}
						@maker =AmtfMakeHole.new(@make_hole_data, view, @dialog)
						# @config_data["类型"]="连接件"
						# puts @config_data
						选中规格=查找选中规格(@config_data)
						选中规格["类型"]=@config_data["选中类型"]
						选中规格["候选阵列"]=@config_data["候选阵列"]
						@maker.set_config(选中规格)
						mod = Sketchup.active_model
							@maker.make_hole
						# mod.start_operation("布置连接件", true)
						# 	@maker.make_hole
						# mod.commit_operation
						@last_pick = {}
						@last_face = nil
					end
					view.refresh
					view.invalidate
				end

				def 查找选中规格(d)
					选中类型=nil
					选中规格=nil
					选中类型名称=d["选中类型"]
					候选类型s=d["候选类型"]
					候选类型s.each{|类型|
					  	# puts 类型["类型"]==选中类型名称
						if 类型["类型"]==选中类型名称
							选中类型=类型
							break
						end
					}
					# puts "选中类型  #{选中类型}"
					选中规格名称=选中类型["选中规格"]
					候选规格s=选中类型["候选规格"]
					候选规格s.each{|规格|
					  if 规格["规格"]==选中规格名称
						选中规格=规格
						break
					  end
					}
					# puts "选中规格  #{选中规格}"
					return 选中规格
				end

				def onLButtonDown(flags, x, y, view)
					布置连接件(flags, x, y, view)
				end
				
				def deactivate(_view)
					@@active_tool_class = nil
					@first_picked = false
					@last_pick = {}
					@config_data = {}
					@dialog.close if !@dialog.nil?
				end
				def draw(view)
					@maker.draw if @maker
					@hinge_maker.draw if @hinge_maker
					@drawer_maker.draw if @drawer_maker
				end
				def get_model_units()
					opts = Sketchup.active_model.options['UnitsOptions']
					unit_format = opts['LengthFormat']
					if unit_format != Length::Decimal
						if unit_format == Length::Fractional ||
								unit_format == Length::Architectural
							'INCHES' # Architectural or Fractional
						elsif unit_format == Length::Engineering
							'FEET' # Engineering
						else
							'DEFAULT'
						end
					else
						# Decimal ( unit_format == 0 )
						case opts['LengthUnit']
						when Length::Inches
							'INCHES'
						when Length::Feet
							'FEET'
						when Length::Centimeter
							'CENTIMETERS'
						when Length::Millimeter
							'MILLIMETERS'
						when Length::Meter
							'METERS'
						else
							'DEFAULT'
						end
					end
				end

				def create_dialog(url = nil)
					options = {
						dialog_title: "连接件😄",
						preferences_key: "连接件😄",
						# scrollable: false,
						# resizable: false,
						width: 400,
						height: 160,
						left: 0,
						top: 100,
						# min_width: 50,
						# min_height: 150,
						max_width: 1000,
						max_height: 1000,
						style: UI::HtmlDialog::STYLE_DIALOG
					}
					# AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
					# url = "#{ServerOptions.get_server_url}/hole_manual"
					@dialog = UI::HtmlDialog.new(options)

					path = File.join(PLUGIN_DIR, '/html/连接件.html')
					@dialog.set_file path
					# @dialog.center
					# @dialog.set_url(url)
					Bridge.decorate(@dialog)
					@dialog.add_action_callback("load_config") {|dialog, arg|
						puts "load_config"
						@dialog.execute_script("显示信息('#{@config_data.to_json}')")
					  }
					@dialog.add_action_callback("保存设置") {|dialog, arg|
						# puts arg
						# puts arg.class
						puts "保存设置"
						@总设置["连接件"]=arg
						保存设置_合并hash(@总设置)
					  }
					@dialog.add_action_callback("修改设置") {|dialog, arg|
						puts arg
						@config_data=arg
					  }
					@dialog.show
					@dialog
				end
			end # A连接件

			class MinifixTool < Tools::Base
				STS_CLASS_NAME = OB[:minifix_tool].freeze
				def activate
					@@active_tool_class = self.class
					@mouse_ip = Sketchup::InputPoint.new
					@ph = Sketchup.active_model.active_view.pick_helper
					@first_picked = false
					@last_pick = {}
					@config_data = {}
					@frame = nil
					if !@dialog.nil? && @dialog.visible?
						@dialog.bring_to_front
					else
						@dialog = create_dialog
						# @dialog=DLG_连接件.new()
						# @dialog.show
					end
					# deferred=推迟; 延缓; 展期;
					@dialog.on('set_hole_tool_data') do |deferred, data|
						begin
							@config_data = data
							@connector_id = data['connector_id']
							@connector_type = data['connector_type']
							@all_edge_banding = data['all_edge_banding']
							if data['width'] && data['height']
								@dialog.set_size(
									data['width'].to_i,
									data['height'].to_i
								)
							end
							deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
				end
				def do_drawer_fitting(flags, x, y, view)
					mod = Sketchup.active_model
					selection = Sketchup.active_model.selection
					entitites = mod.active_entities
					@mouse_ip.pick view, x, y
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					@first_picked = !@first_picked
					selection.clear
					selection.add(pick_data[:pick_component])
					pick_data[:position] = @mouse_ip.position
					if @first_picked
						@last_pick = pick_data
						@make_hole_data = {}
					elsif !@last_pick.empty?
						if @last_pick[:pick_component] ==
								pick_data[:pick_component]
							return nil
						end
						@make_hole_data = {
							pick_data: pick_data,
							last_pick: @last_pick
						}
						@drawer_maker = DrawerDrill.new(@make_hole_data, view)
						@drawer_maker.set_config(@config_data)
						mod = Sketchup.active_model
						mod.start_operation(OB[:minifix_tool], true)
						@drawer_maker.make_fitting
						mod.commit_operation
						@last_pick = {}
						@last_face = nil
					end
					view.refresh
					view.invalidate
				end
				def do_hinge(flags, x, y, view)
					mod = Sketchup.active_model
					selection = Sketchup.active_model.selection
					entitites = mod.active_entities
					@mouse_ip.pick view, x, y
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					@first_picked = !@first_picked
					selection.clear
					selection.add(pick_data[:pick_component])
					pick_data[:position] = @mouse_ip.position
					if @first_picked
						@last_pick = pick_data
						@amtflogic_hinge_tool_hiden_face_array = []
						@make_hole_data = {}
						@first_frame = pick_data[:frame]
						faces =
							Utilities
								.definition(pick_data[:pick_component])
								.entities
								.grep(Sketchup::Face)
						@drawHiddenGeometry =
							Sketchup.active_model.rendering_options[
								'DrawHiddenGeometry'
							]
						@drawHiddenObjects =
							Sketchup.active_model.rendering_options[
								'DrawHiddenObjects'
							]
						Sketchup.active_model.rendering_options[
							'DrawHiddenGeometry'
						] =
							false
						Sketchup.active_model.rendering_options[
							'DrawHiddenObjects'
						] =
							true
						Sketchup.active_model.start_operation('Hide', true)
						faces.each do |f|
							f.hidden = true
							@amtflogic_hinge_tool_hiden_face_array << f
						end
						Sketchup.active_model.commit_operation
						# Thực hiện ẩn cánh, ch�?còn lại cái khung
					elsif !@last_pick.empty?
						if @last_pick[:pick_component] ==
								pick_data[:pick_component]
							if @amtflogic_hinge_tool_hiden_face_array &&
									@amtflogic_hinge_tool_hiden_face_array
										.size > 0
								@amtflogic_hinge_tool_hiden_face_array
									.each { |f| f.hidden = false }
							end
							return
						end
						@second_frame = pick_data[:frame]
						@make_hinge_data = {
							pick_data: pick_data,
							first_frame: @first_frame,
							last_pick: @last_pick,
							second_frame: @second_frame
						}
						@hinge_maker = AmtfHinge.new(@make_hinge_data, view)
						@hinge_maker.set_config(@config_data)
						mod = Sketchup.active_model
						mod.start_operation(OB[:minifix_tool], true)
						@hinge_maker.make_hinge
						mod.commit_operation
						@last_pick = {}
						@last_face = nil
						if @amtflogic_hinge_tool_hiden_face_array &&
								@amtflogic_hinge_tool_hiden_face_array.size > 0
							@amtflogic_hinge_tool_hiden_face_array.each do |f|
								f.hidden = false
							end
						end
						@amtflogic_hinge_tool_hiden_face_array = []
						@first_frame = nil
						@second_frame = nil
						Sketchup.active_model.rendering_options[
							'DrawHiddenGeometry'
						] =
							@drawHiddenGeometry
						Sketchup.active_model.rendering_options[
							'DrawHiddenObjects'
						] =
							@drawHiddenObjects
					end
					view.refresh
					view.invalidate
				end
				def edge_banding(flags, x, y, view)
					mod = Sketchup.active_model
					mod.start_operation(OB[:edge_banding_edit], true)
					get_pick = pick_sheet(@ph, x, y)
					pickFace = get_pick[:pick_face]
					return if get_pick.empty?
					return if pickFace.nil?
					pick_component = get_pick[:pick_component]
					edge_banding_data =
						Utilities.get_setting(
							@connector_type,
							@config_data['config'],
							@connector_id
						)
					the_sheet =
						AmtfSheet.new(
							pick_component,
							get_pick[:trans],
							get_pick[:parent_matrix]
						)
					sheet_id = the_sheet.sheet_id
					faces = the_sheet.all_faces
					edge_face = the_sheet.edge_face
					big_faces = the_sheet.big_faces
					if @all_edge_banding == true
						is_edged = false
						edge_face.each do |f|
							if f.get_attribute('amtf_dict', 'edge_banding') ==
									'true'
								is_edged = true
								break
							end
						end
						if is_edged
							face_index_arr = []
							edge_face.each do |f|
								f.set_attribute 'amtf_dict',
								                'edge_banding',
								                'false'
								f.delete_attribute 'amtf_dict', 'edge_banding'
								f.delete_attribute 'amtf_dict', 'edge_type_id'
								f.delete_attribute 'amtf_dict', 'edge_type_name'
								f.delete_attribute 'amtf_dict',
								                   'edge_type_thick'
								f.material = pick_component.material
								face_index =
									f.get_attribute 'amtf_dict', 'face_index'
								face_index_arr << face_index if face_index
							end
							if get_pick[:frame] && !get_pick[:frame].nil? &&
									sheet_id && face_index_arr.size > 0
								get_pick[:frame].remove_multi_edging(
									sheet_id,
									face_index_arr
								)
							end
						else
							face_index_arr = []
							edge_face.each do |f|
								f.set_attribute 'amtf_dict',
								                'edge_banding',
								                'true'
								f.set_attribute 'amtf_dict',
								                'edge_type_id',
								                @connector_id
								f.set_attribute 'amtf_dict',
								                'edge_type_name',
								                edge_banding_data['name']
								f.set_attribute 'amtf_dict',
								                'edge_type_thick',
								                edge_banding_data[
										'edge_banding_thick'
								                ]
								f.material = edge_banding_data['color']
								face_index =
									f.get_attribute 'amtf_dict', 'face_index'
								face_index_arr << face_index if face_index
							end
							if get_pick[:frame] && !get_pick[:frame].nil? &&
									sheet_id && face_index_arr.size > 0
								edging_data = {
									'edge_banding' => true,
									'edge_type_id' => @connector_id,
									'edge_type_name' =>
										edge_banding_data['name'],
									'edge_type_thick' =>
										edge_banding_data['edge_banding_thick'],
									'color' => edge_banding_data['color']
								}
								get_pick[:frame].add_multi_edging(
									sheet_id,
									face_index_arr,
									edging_data
								)
							end
						end
					else
						return if big_faces.include?(pickFace)
						if pick_component.get_attribute(
								'amtf_dict',
								'no_auto_edge_banding'
						   ) == true
							pick_component.delete_attribute(
								'amtf_dict',
								'no_auto_edge_banding'
							)
							edge_face.each do |f|
								f.delete_attribute 'amtf_dict', 'edge_banding'
								f.delete_attribute 'amtf_dict', 'edge_type_id'
								f.delete_attribute 'amtf_dict', 'edge_type_name'
								f.delete_attribute 'amtf_dict',
								                   'edge_type_thick'
								f.material = pick_component.material
							end
						end
						edge_banding =
							pickFace.get_attribute('amtf_dict', 'edge_banding')
						if !edge_banding || edge_banding == 'false'
							pickFace.set_attribute 'amtf_dict',
							                       'edge_banding',
							                       'true'
							pickFace.set_attribute 'amtf_dict',
							                       'edge_type_id',
							                       @connector_id
							pickFace.set_attribute 'amtf_dict',
							                       'edge_type_name',
							                       edge_banding_data['name']
							pickFace.set_attribute 'amtf_dict',
							                       'edge_type_thick',
							                       edge_banding_data[
									'edge_banding_thick'
							                       ]
							pickFace.material = edge_banding_data['color']
							if get_pick[:frame] && !get_pick[:frame].nil?
								face_index =
									pickFace.get_attribute 'amtf_dict',
									                       'face_index'
								edging_data = {
									'edge_banding' => true,
									'edge_type_id' => @connector_id,
									'edge_type_name' =>
										edge_banding_data['name'],
									'edge_type_thick' =>
										edge_banding_data['edge_banding_thick'],
									'color' => edge_banding_data['color']
								}
								get_pick[:frame].add_edging(
									sheet_id,
									face_index,
									edging_data
								)
							end
						elsif edge_banding == 'true'
							pickFace.set_attribute 'amtf_dict',
							                       'edge_banding',
							                       'false'
							pickFace.delete_attribute 'amtf_dict',
							                          'edge_banding'
							pickFace.delete_attribute 'amtf_dict',
							                          'edge_type_id'
							pickFace.delete_attribute 'amtf_dict',
							                          'edge_type_name'
							pickFace.delete_attribute 'amtf_dict',
							                          'edge_type_thick'
							pickFace.material = pick_component.material
							if get_pick[:frame] && !get_pick[:frame].nil?
								face_index =
									pickFace.get_attribute 'amtf_dict',
									                       'face_index'
								get_pick[:frame].remove_edging(
									sheet_id,
									face_index
								)
							end
						end
					end
					if get_pick[:frame] && !get_pick[:frame].nil?
						sheet_attrs = {}
						dict = pick_component.attribute_dictionary 'amtf_dict'
						if dict
							dict.each { |key, value| sheet_attrs[key] = value }
						end
						get_pick[:frame].update_attributes(
							sheet_id,
							sheet_attrs
						)
					end
					mod.commit_operation
				end
				def do_groove(flags, x, y, view)
					mod = Sketchup.active_model
					selection = Sketchup.active_model.selection
					entitites = mod.active_entities
					@mouse_ip.pick view, x, y
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					@first_picked = !@first_picked
					selection.clear
					selection.add(pick_data[:pick_component])
					pick_data[:position] = @mouse_ip.position
					if @first_picked
						@last_pick = pick_data
						@make_hole_data = {}
					elsif !@last_pick.empty?
						if @last_pick[:pick_component] ==
								pick_data[:pick_component]
							return nil
						end
						@make_hole_data = {
							pick_data: pick_data,
							last_pick: @last_pick,
							frame: pick_data[:frame]
						}
						@maker =
							AmtfMakeHole.new(@make_hole_data, view, @dialog)
						@maker.set_config(@config_data)
						mod = Sketchup.active_model
						mod.start_operation(OB[:minifix_tool], true)
						case @connector_type
						when 'connector', 'chot_dot', 'khoan_moi', 'patV',
						     'rafix', 'lockdowel', 'cabineo', 'dogbone'
							@maker.make_hole
						when 'groove'
							@maker.back_and_bottom
						end
						mod.commit_operation
						@last_pick = {}
						@last_face = nil
					end
					view.refresh
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					case @connector_type
					when 'hinge'
						do_hinge(flags, x, y, view)
					when 'connector', 'groove', 'chot_dot', 'khoan_moi', 'patV',
					     'rafix', 'lockdowel', 'cabineo', 'dogbone'
						do_groove(flags, x, y, view)
					when 'edge_banding'
						edge_banding(flags, x, y, view)
					when 'drawer_fitting'
						do_drawer_fitting(flags, x, y, view)
					else
						js_command =
							"error_display('#{OB[:need_select_connector_type]}', 'alert-warning')"
						@dialog.execute_script(js_command)
					end
				end
				def deactivate(_view)
					@@active_tool_class = nil
					@first_picked = false
					@last_pick = {}
					@config_data = {}
					@dialog.close if !@dialog.nil?
				end
				def draw(view)
					@maker.draw if @maker
					@hinge_maker.draw if @hinge_maker
					@drawer_maker.draw if @drawer_maker
				end
				def get_model_units()
					opts = Sketchup.active_model.options['UnitsOptions']
					unit_format = opts['LengthFormat']
					if unit_format != Length::Decimal
						if unit_format == Length::Fractional ||
								unit_format == Length::Architectural
							'INCHES' # Architectural or Fractional
						elsif unit_format == Length::Engineering
							'FEET' # Engineering
						else
							'DEFAULT'
						end
					else
						# Decimal ( unit_format == 0 )
						case opts['LengthUnit']
						when Length::Inches
							'INCHES'
						when Length::Feet
							'FEET'
						when Length::Centimeter
							'CENTIMETERS'
						when Length::Millimeter
							'MILLIMETERS'
						when Length::Meter
							'METERS'
						else
							'DEFAULT'
						end
					end
				end

				def create_dialog(url = nil)
					options = {
						dialog_title: "连接件😄",
						preferences_key: "连接件😄",
						# scrollable: false,
						# resizable: false,
						width: 400,
						height: 160,
						left: 0,
						top: 100,
						# min_width: 50,
						# min_height: 150,
						max_width: 1000,
						max_height: 1000,
						style: UI::HtmlDialog::STYLE_DIALOG
					}
					# AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
					# url = "#{ServerOptions.get_server_url}/hole_manual"
					@dialog = UI::HtmlDialog.new(options)

					path = File.join(PLUGIN_DIR, '/html/连接件.html')
					@dialog.set_file path
					# @dialog.center
					# @dialog.set_url(url)
					Bridge.decorate(@dialog)
					@dialog.on('set_save_credential') do |deferred, credential|
						begin
							Sketchup.write_default(
								'Amtflogic',
								'configuration',
								credential
							)
							deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.on('set_window_size') do |deferred, width, height|
						begin
							@dialog.set_size(width.to_i, height.to_i)
							deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.on('load_config') do |deferred|
						begin
							saver = AmtfSaver.new
							config = saver.open_config
							credential =
								Sketchup.read_default(
									'Amtflogic',
									'configuration',
									''
								)
							deferred.resolve(
								{
									language: OB.saved_lang,
									config: config,
									version: EXTENSION.version,
									unit: self.get_model_units,
									su_filename: Sketchup.active_model.title,
									cre: credential,
									su_authorized: true
								}
							)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.show
					@dialog
				end
			end # MinifixTool
		end
	end
end