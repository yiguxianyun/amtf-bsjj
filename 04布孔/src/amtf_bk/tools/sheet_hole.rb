# encoding: UTF-8
module AMTF
	module BUKONG
		module SheetHoleModule
			class SheetHole < Tools::Base
				STS_CLASS_NAME = OB[:strange_sheet].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					reset_tool
					@parameters = {}
					AMTF_Dialog
						.dialog
						.on(
							'strange_sheet_set_params'
						) do |deferred, parameters|
							begin
								parameters = parameters['params']
								pull_depth =
									AmtfMath.thickness(
										@pick_data[:sheet],
										@matrix
									)
								root_data = {
									unit: AMTF_Dialog.get_model_units,
									pull_depth: pull_depth.to_f,
									root: @frame.root,
									frame_ID: @frame.id,
									sheet_id: @pick_data[:sheet_id],
									shapeType: parameters['shapeType'],
									jointType: parameters['jointType'],
									side_face: @hole_data[:side_face],
									conner: @hole_data[:conner],
									width: parameters['width'],
									height: parameters['height'],
									radius: parameters['radius'],
									x: parameters['x'],
									y: parameters['y'],
									name: parameters['name']
								}
								js_command =
									"strange_sheet('#{root_data.to_json}');"
								AMTF_Dialog.dialog.execute_script(js_command)
								reset_tool
								deferred.resolve(true)
							rescue => error
								AMTF_Dialog.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
				end
				def deactivate(view)
					@@active_tool_class = nil
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def resume(view)
					view.invalidate
				end
				def suspend(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.invalidate
				end
				def onMouseMove(flags, x, y, view); end
				def getExtents
					bb = Geom::BoundingBox.new
					bb.add(@mouse_ip.position)
					bb
				end
				def draw(view)
					@boxHelper.draw(view) if @boxHelper
					@mouse_ip.draw(view) if @mouse_ip.display?
					view.draw_points(@test_points, 50, 1, 'red') if @test_points
				end
				def doSheetTask
					return if @pick_data.nil? || @pick_data.empty?
					return unless @pick_data[:path].last.is_a?(Sketchup::Face)
					sheet_id = @pick_data[:sheet_id]
					@frame = @pick_data[:frame]
					sheet_type = @pick_data[:sheet_type]
					trans_path = @pick_data[:path]
					sub_index = @pick_data[:sub_index]
					return unless @frame && sheet_id
					@sheet_data = {}
					sheet_trans = IDENTITY
					@frame.traverseCallback do |node|
						if node['root'] && node['root']['sheets']
							node['root']['sheets'].each do |k, sheet_arr|
								sheet_arr.each do |obj|
									if obj['root']['id'] == sheet_id
										sheet_trans =
											Geom::Transformation.new(
												obj['root']['transform']
											)
										@sheet_data = {
											sheet: obj,
											parent: node['root']
										}
									end
								end
							end
						end
					end
					return if @sheet_data.empty?
					matrix = IDENTITY
					for i in (sub_index - 1).downto(0)
						if trans_path[i].is_a?(Sketchup::Group)
							matrix = matrix * trans_path[i].transformation
						end
					end
					sheet = @sheet_data[:sheet]
					matrix = matrix * sheet_trans
					vertices = sheet['root']['boundingBox']['vertices']
					@boxHelper = AmtfBox.new(vertices, matrix)
					@matrix = matrix
				end
				def get_the_data
					return nil if !@boxHelper.selected_point[:snap]
					if @boxHelper.selected_point[:snap] == 'rightPoint'
						return nil
					end
					if @boxHelper.selected_point[:snap] == 'leftPoint'
						return nil
					end
					if @boxHelper.selected_point[:snap] == 'bottomPoint'
						return nil
					end
					return nil if @boxHelper.selected_point[:snap] == 'topPoint'
					if @boxHelper.selected_point[:snap] == 'frontPoint'
						return nil
					end
					if @boxHelper.selected_point[:snap] == 'backPoint'
						return nil
					end
					x_axiz = Geom::Vector3d.new 1, 0, 0
					y_axiz = Geom::Vector3d.new 0, 1, 0
					z_axiz = Geom::Vector3d.new 0, 0, 1
					pick_face = @pick_data[:path].last
					unless pick_face.normal.parallel?(z_axiz) ||
							pick_face.normal.parallel?(x_axiz) ||
							pick_face.normal.parallel?(y_axiz)
						return nil
					end
					normal =
						Utilities.vector_context_convert(
							pick_face.normal,
							@matrix,
							IDENTITY
						)
					point =
						Utilities.point_context_convert(
							pick_face.vertices[0].position,
							@matrix,
							IDENTITY
						)
					plane = [point, normal.normalize]
					status = @boxHelper.selected_point[:point].on_plane?(plane)
					return nil unless status
					result = { conner: @boxHelper.selected_point[:snap] }
					if pick_face.normal.parallel?(z_axiz)
						if pick_face.normal.samedirection?(z_axiz)
							result[:side_face] = 'top'
						else
							result[:side_face] = 'bottom'
						end
					elsif pick_face.normal.parallel?(x_axiz)
						if pick_face.normal.samedirection?(x_axiz)
							result[:side_face] = 'right'
						else
							result[:side_face] = 'left'
						end
					elsif pick_face.normal.parallel?(y_axiz)
						if pick_face.normal.samedirection?(y_axiz)
							result[:side_face] = 'back'
						else
							result[:side_face] = 'front'
						end
					end
					return result
				end
				def onLButtonDown(flags, x, y, view)
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pick_point = @mouse_ip.position
					if !@frame
						@pick_data = pick_frame_data_2
						if @pick_data && @pick_data[:found_sheet] == true
							doSheetTask
						end
					else
						if @boxHelper
							if @hole_data.nil?
								@boxHelper.pick_scale_point(view, pick_point)
								@hole_data = get_the_data
							else
								AMTF_Dialog.dialog.execute_script(
									'get_stranger_params();'
								)
							end
						end
					end
					view.invalidate
				end
				def cancel_tool; end
				def reset_tool
					@frame = nil
					@pick_data = nil
					@boxHelper = nil
					@hole_data = nil
					@parameters = {}
					@root_data = {}
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #