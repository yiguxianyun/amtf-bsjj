# encoding: UTF-8
module AMTF
	module BUKONG
		module SheetEditModule
			class SheetEdit < Tools::Base
				STS_CLASS_NAME = OB[:edit_sheet].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					reset_tool
				end
				def deactivate(view)
					@@active_tool_class = nil
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def resume(view)
					view.invalidate
				end
				def suspend(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.invalidate
				end
				def getExtents
					bb = Geom::BoundingBox.new
					bb.add(@mouse_ip.position)
					bb
				end
				def doSheetTask
					return if @pick_data.nil? || @pick_data.empty?
					return unless @pick_data[:path].last.is_a?(Sketchup::Face)
				end
				def onLButtonDown(flags, x, y, view)
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pick_data = pick_frame_data_2
					if pick_data && pick_data[:found_sheet] == true
						sheet_id = pick_data[:sheet_id]
						frame = pick_data[:frame]
						sheet_type = pick_data[:sheet_type]
						trans_path = pick_data[:path]
						sub_index = pick_data[:sub_index]
						return unless frame && sheet_id
						sheet_data = {}
						frame.traverseCallback do |node|
							if node['root'] && node['root']['sheets']
								node['root']['sheets'].each do |k, sheet_arr|
									sheet_arr.each do |obj|
										if obj['root']['id'] == sheet_id
											sheet_data = {
												sheet: obj,
												parent: node['root']
											}
										end
									end
								end
							end
						end
						return if sheet_data.empty?
						root_data = {
							unit: AMTF_Dialog.get_model_units,
							root: frame.root,
							frame_ID: frame.id,
							sheet_id: pick_data[:sheet_id],
							sheet_data: sheet_data
						}
						js_command =
							"edit_sheet_send_data('#{root_data.to_json}');"
						AMTF_Dialog.dialog.execute_script(js_command)
					end
					view.invalidate
				end
				def cancel_tool; end
				def reset_tool; end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #