# encoding: UTF-8
module AMTF
    module BUKONG      
      module ProhibitionToolModule
        class ProhitbitionHole < Tools::Base 
            STS_CLASS_NAME   = OB[:hole_prohibited].freeze
            def onLButtonDown(flags, x, y, view)    
              mod = Sketchup.active_model
              mod.start_operation(OB[:hole_prohibited], true) 
              @ip = Sketchup::InputPoint.new
              @ip.pick view, x, y
              instance_path = @ip.instance_path
              pick_component = instance_path.root
              pick_face = @ip.face   
              return unless Utilities.instance?(pick_component) && pick_face.valid? && pick_face.is_a?(Sketchup::Face)              
              explode = Utilities.get_comp_attribute(pick_component, 'amtflogic_exploded', 'amtf_dict')              
              if explode != 'true' || Utilities.hasParent?(pick_component)
                  UI.messagebox OB[:explode_required], MB_OK
                  return
              end
              comp_type = Utilities.get_comp_attribute(pick_component, 'comp_type', 'amtf_dict')             
              if comp_type === 'no_hole'
                pick_component.delete_attribute 'amtf_dict', 'comp_type'                 
              else                 
                  pick_component.set_attribute 'amtf_dict', 'comp_type', 'no_hole' 
              end 
              part = SheetPart.new(pick_component, pick_component.transformation, IDENTITY)    
              subGroup = part.comp.entities.grep(Sketchup::Group)    
              if subGroup.size > 0    
                  for i in 0..subGroup.size - 1
                      group_type =  Utilities.get_comp_attribute(subGroup[i], 'type', 'amtf_dict')
                      if group_type == 'bukong_label'
                          subGroup[i].erase!
                          break
                      end
                  end                            
              end              
              part.drawArrow(pick_face)    
              mod.commit_operation
            end          
          end# Probibition Hole
      end
    end
end