# encoding: UTF-8
module AMTF
  module BUKONG
  module Division_Module
    class Division_change < Tools::Base
      STS_CLASS_NAME   = OB[:division].freeze  
      def activate   
        @@active_tool_class = self.class               
        @input  = Sketchup::InputPoint.new
        @face_vertices = []
        Sketchup.active_model.selection.clear
        Sketchup.status_text = OB[:select_target_object] 
        AMTF_STORE.show_all_sections()     
      end
      def reset_tool           
        Sketchup.active_model.selection.clear
      end
      def deactivate(_view)
        @@active_tool_class = nil
        AMTF_STORE.hide_all_sections() 
      end  
      def refresh
      end
      def onMouseMove( flags, x, y, view )
        @ph.do_pick(x, y)
        @input.pick(view, x, y)
        @face_vertices = []
        pickpaths = {}
        @section_face = nil
        @frame = nil
        # Iterate all pick-routes:
        for index in 0..@ph.count-1
            pickpath = @ph.path_at(index)
            pickpaths[index]= pickpath
        end
        if !pickpaths.empty?
            pickpaths.each{
                |index, value|
                if value.last.is_a?(Sketchup::Face) && value.first.is_a?(Sketchup::Group)
                    pick_face = value.last  
                    section_id = pick_face.get_attribute('amtf_dict','section_id')
                    next unless section_id
                    next unless value.first.is_a?(Sketchup::Group)   
                    next unless value.first.get_attribute('amtf_dict','comp_type') == "AmtfFrame"                                    
                    comp_id = value.first.persistent_id
                    trans_path = pickpaths[index]                                  
                    matrix = IDENTITY
                    for i in (trans_path.size - 1).downto(0)                                    
                        if trans_path[i].is_a?(Sketchup::Group)
                            matrix = matrix*trans_path[i].transformation
                        end
                    end
                    @face_vertices = pick_face.outer_loop.vertices
                    @face_vertices.map!{
                        |f| 
                        f = f.position.transform!(matrix)
                    }
                    @section_face = pick_face
                    @frame = AMTF_STORE.frame_premitives[ comp_id ] 
                    @trans_path = trans_path  
                    @section_face = pick_face
                end                          
            } 
        end
        view.tooltip = @input.tooltip if @input.valid?
         view.refresh
        view.invalidate
    end
      def draw(view)
          if @face_vertices.size > 0
              view.line_width = 4  
              view.drawing_color = "yellow"             
              view.draw(GL_LINE_LOOP, @face_vertices)
          end         
      end
      def onLButtonUp( flags, x, y, view )
        selection = Sketchup.active_model.selection       
        if @frame.nil? || @section_face.nil?
           reset
           return      
        end        
        if !@section_face.nil? and !@frame.nil?
         section_id = @section_face.get_attribute('amtf_dict','section_id')
         path = @frame.findPath(section_id)
         return if !path
         return if path.size < 2
         parent = path[path.size - 2] 
         section = path[path.size - 1]   
         current_distance = ''
        if parent['split'] == 'horizon_divider'
          current_distance = section['height2d']
        elsif parent['split'] == 'vertical_divider'
          current_distance = section['width2d']
        else 
          return
        end 
          distance = ''  
          while  distance == ''           
            prompts = [OB[:distance]  ]
            defaults = [current_distance.round(2).to_s]
            inputs = UI.inputbox(prompts, defaults, OB[:distance])
            return if !inputs            
            distance = inputs[0]  
          end
          root_data = {
                :frame_ID => @frame.id,
                :root => @frame.root, 
                :distance => distance,  
                :index => section["index"],
                :parent_section => parent,
                :current_distance => current_distance
              } 
          js_command = "adjust_division('#{root_data.to_json}');"      
          AMTF_Dialog.dialog.execute_script(js_command)
        end      
         view.refresh     
        view.invalidate
        true
      end
  end#Class
  end#  OffsetToolModule
  end # BuKong
end # 