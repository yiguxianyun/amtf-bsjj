# encoding: UTF-8
module AMTF
	module BUKONG
		module CurveSheet
			class CurveSheetTool < Tools::Base
				STS_CLASS_NAME = OB[:curve_sheet_tool].freeze
				def self.tool_activate
					Sketchup.active_model.selection.clear
					Sketchup.active_model.select_tool(new)
				end
				def onLButtonDown(flags, x, y, view)
					mod = Sketchup.active_model
					mod.start_operation(OB[:curve_sheet_tool], true)
					@ip = Sketchup::InputPoint.new
					@ip.pick view, x, y
					instance_path = @ip.instance_path
					pick_component = instance_path.root
					return unless pick_component.is_a?(Sketchup::Group)
					Sketchup.active_model.selection.clear
					Sketchup.active_model.selection.add pick_component
					boundingbox = pick_component.bounds
					volume =
						boundingbox.width * boundingbox.height *
							boundingbox.depth
					return unless volume === 0
					show_name_dialog(pick_component)
					mod.commit_operation
				end
				def create_dialog()
					options = {
						dialog_title: 'ONECLICK CABINET',
						preferences_key: 'bukongcabinet.net',
						style: UI::HtmlDialog::STYLE_DIALOG,
						# Set a fixed size now that we know the content size.
						resizable: false,
						scrollable: false,
						width: 300,
						height: 380
					}
					url = "#{ServerOptions.get_server_url}/curve_sheet"
					dialog = UI::HtmlDialog.new(options)
					dialog.set_size(options[:width], options[:height]) # Ensure size is set.
					dialog.set_url(url)
					dialog.center
					dialog
				end
				def show_name_dialog(pick_component)
					return unless pick_component
					@dialog.close if !@dialog.nil?
					@dialog = self.create_dialog
					Bridge.decorate(@dialog)
					@dialog.on('execute') do |deferred, params|
						begin
							path_data = make_data(pick_component)
							sheet_name = params['name']
							extra_grooves =
								Utilities.unitConvert(params['extra_grooves'])
							groove_spacing =
								Utilities.unitConvert(params['groove_spacing'])
							groove_width =
								Utilities.unitConvert(params['groove_width'])
							thick = Utilities.unitConvert(params['Thick'])
							height = Utilities.unitConvert(params['height'])
							sheet_data =
								generateCurveSheet(
									path_data,
									height,
									thick,
									groove_width,
									groove_spacing,
									extra_grooves
								)
							if sheet_data
								group =
									Sketchup.active_model.active_entities
										.add_group
								group.set_attribute 'amtf_dict',
								                    'amtflogic_exploded',
								                    'true'
								group.name = sheet_name if sheet_name
								sheet_data[:faces].each do |key, points|
									face =
										group.entities.add_face(
											Utilities.uniq_points(points)
										)
								end
								sheet_data[:grooves].each do |points|
									chanel_grp = group.entities.add_group
									group.set_attribute(
										'amtf_dict',
										'has_groove',
										'yes'
									)
									chanel_face =
										chanel_grp.entities.add_face(points)
									chanel_grp.set_attribute 'amtf_dict',
									                         'type',
									                         'custom_groove'
									chanel_grp.set_attribute 'amtf_dict',
									                         'minifixID',
									                         Utilities
											.CreatUniqueid
									chanel_grp.set_attribute 'amtf_dict',
									                         'groove_name',
									                         OB[
											:curve_sheet_groove
									                         ]
								end
							end
							if path_data && path_data.size > 0
								deferred.resolve(path_data.to_json)
							else
								deferred.resolve(false)
							end
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.show
				end
				def splitEdges(sorted_edges)
					temp = []
					line = nil
					curve = nil
					for i in 0..sorted_edges.size - 1
						edge = sorted_edges[i]
						if edge.curve
							if curve != edge.curve
								temp << {
									edge: [edge],
									curve: edge.curve,
									type: 'curve'
								}
							else
								temp[temp.size - 1][:edge] << edge
							end
							curve = edge.curve
						else
							curve = nil
							temp << { edge: edge, type: 'line' }
						end
					end
					return temp
				end
				def make_data(pick_component)
					edges = pick_component.entities.grep(Sketchup::Edge)
					paths = TTLibrary.findPaths(edges)
					return [] unless paths.size > 0
					path = paths[0][:path]
					return [] unless path.size > 1
					splitEdge = splitEdges(path)
					result = []
					for i in 0..splitEdge.size - 1
						seg = splitEdge[i]
						if seg[:type] === 'line'
							startPoint = seg[:edge].start
							endPoint = seg[:edge].end
							if i < splitEdge.size - 1
								nextEdge = splitEdge[i + 1]
								if nextEdge[:type] === 'line'
									nextPositions = [
										splitEdge[i + 1][:edge].start,
										splitEdge[i + 1][:edge].end
									]
									if nextPositions.include? startPoint
										startPoint = seg[:edge].end
										endPoint = seg[:edge].start
									end
								else
									nextPositions = [
										splitEdge[i + 1][:edge][0].start,
										splitEdge[i + 1][:edge][0].end
									]
									if nextPositions.include? startPoint
										startPoint = seg[:edge].end
										endPoint = seg[:edge].start
									end
								end
							else
								prevEdge = splitEdge[i - 1]
								if prevEdge[:type] === 'line'
									prevPositions = [
										splitEdge[i - 1][:edge].start,
										splitEdge[i - 1][:edge].end
									]
									if prevPositions.include? endPoint
										startPoint = seg[:edge].end
										endPoint = seg[:edge].start
									end
								else
									prevPositions = [
										splitEdge[i - 1][:edge][0].start,
										splitEdge[i - 1][:edge][0].end
									]
									if prevPositions.include? endPoint
										startPoint = seg[:edge].end
										endPoint = seg[:edge].start
									end
								end
							end
							result << {
								startPoint: startPoint.position,
								endPoint: endPoint.position
							}
						else
							# start_angle và end_angle �?đây ch�?đ�?phuc v�?mục đíhc tính len
							curve_eges = seg[:edge]
							curve = seg[:curve]
							theta = (curve.start_angle - curve.end_angle).abs
							unit_theta = theta / curve_eges.size
							start_angle = curve.start_angle
							for j in 0..curve_eges.size - 1
								startPoint = curve_eges[j].start
								endPoint = curve_eges[j].end
								if j < curve_eges.size - 1
									nextEdge = curve_eges[j + 1]
									nextPositions = [
										nextEdge.start,
										nextEdge.end
									]
									if nextPositions.include? startPoint
										startPoint = curve_eges[j].end
										endPoint = curve_eges[j].start
									end
								else
									prevEdge = curve_eges[j - 1]
									prevPositions = [
										prevEdge.start,
										prevEdge.end
									]
									if prevPositions.include? endPoint
										startPoint = curve_eges[j].end
										endPoint = curve_eges[j].start
									end
								end
								end_angle = start_angle + unit_theta
								result << {
									startPoint: startPoint.position,
									endPoint: endPoint.position,
									curve: {
										start_angle: start_angle,
										end_angle: end_angle,
										radius: curve.radius
									}
								}
								start_angle = end_angle
							end
						end
					end
					return result
				end
			end # Tool
			def self.generateCurveSheet(
				outerloop,
				h,
				d,
				groove_width,
				groove_spacing,
				extra_grooves
			)
				len = 0
				curve_len = 0
				groove_half = 0.5 * groove_width
				grooves = []
				prevIsLine = false
				prevIsCurve = false
				org = outerloop[0][:startPoint]
				for i in 0..outerloop.size - 1
					startPoint = outerloop[i][:startPoint]
					endPoint = outerloop[i][:endPoint]
					curve = outerloop[i][:curve]
					if curve
						theta = curve[:end_angle] - curve[:start_angle]
						len += (curve[:radius] * theta).abs
						curve_len = 0 if prevIsLine
						curve_len += (curve[:radius] * theta).abs
						prevIsCurve = true
						prevIsLine = false
					else
						if prevIsCurve
							# T�?curvr sang line: tính
							# khoảng cách đoạn cong = curve_len
							# lấy len tr�?đi curve_len - thêm 1 lần extra thì ra điểm bắt đầu
							# lấy len cộng thêm 2 lần extra thì ra tổng đ�?dài
							tong_khoang_cach =
								curve_len.to_l + 2 * extra_grooves.to_l
							numOfGrooves =
								(tong_khoang_cach.to_l + groove_spacing.to_l) /
									(groove_width.to_l + groove_spacing.to_l)
							numOfGrooves = numOfGrooves.floor
							x_diem_dau =
								org.x.to_l + len.to_l - tong_khoang_cach.to_l
							for k in 0..numOfGrooves - 1
								grooves.push(
									[
										Geom::Point3d.new(
											x_diem_dau.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l
										),
										Geom::Point3d.new(
											x_diem_dau.to_l +
												groove_width.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l
										),
										Geom::Point3d.new(
											x_diem_dau.to_l +
												groove_width.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l + h.to_l
										),
										Geom::Point3d.new(
											x_diem_dau.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l + h.to_l
										)
									]
								)
							end
						end
						prevIsCurve = false
						prevIsLine = true
						curve_len = 0
						len += startPoint.distance(endPoint)
					end
					if (i === outerloop.size - 1) && curve_len > 0
						tong_khoang_cach =
							curve_len.to_l + 2 * extra_grooves.to_l
						numOfGrooves =
							(tong_khoang_cach.to_l + groove_spacing.to_l) /
								(groove_width.to_l + groove_spacing.to_l)
						numOfGrooves = numOfGrooves.floor
						x_diem_dau =
							org.x.to_l + len.to_l - tong_khoang_cach.to_l
						for k in 0..numOfGrooves - 1
							grooves.push(
								[
									Geom::Point3d.new(
										x_diem_dau.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l
									),
									Geom::Point3d.new(
										x_diem_dau.to_l + groove_width.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l
									),
									Geom::Point3d.new(
										x_diem_dau.to_l + groove_width.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l + h.to_l
									),
									Geom::Point3d.new(
										x_diem_dau.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l + h.to_l
									)
								]
							)
						end
					end
				end
				faces = {
					face_1: [
						Geom::Point3d.new(org.x, org.y, org.z),
						Geom::Point3d.new(org.x + len, org.y, org.z),
						Geom::Point3d.new(org.x + len, org.y, org.z + h),
						Geom::Point3d.new(org.x, org.y, org.z + h)
					],
					face_2: [
						Geom::Point3d.new(org.x, org.y + d, org.z),
						Geom::Point3d.new(org.x, org.y + d, org.z + h),
						Geom::Point3d.new(org.x + len, org.y + d, org.z + h),
						Geom::Point3d.new(org.x + len, org.y + d, org.z)
					],
					face_3: [
						Geom::Point3d.new(org.x, org.y, org.z),
						Geom::Point3d.new(org.x, org.y, org.z + h),
						Geom::Point3d.new(org.x, org.y + d, org.z + h),
						Geom::Point3d.new(org.x, org.y + d, org.z)
					],
					face_4: [
						Geom::Point3d.new(org.x + len, org.y, org.z),
						Geom::Point3d.new(org.x + len, org.y + d, org.z),
						Geom::Point3d.new(org.x + len, org.y + d, org.z + h),
						Geom::Point3d.new(org.x + len, org.y, org.z + h)
					],
					face_5: [
						Geom::Point3d.new(org.x, org.y, org.z),
						Geom::Point3d.new(org.x, org.y + d, org.z),
						Geom::Point3d.new(org.x + len, org.y + d, org.z),
						Geom::Point3d.new(org.x + len, org.y, org.z)
					],
					face_6: [
						Geom::Point3d.new(org.x, org.y, org.z + h),
						Geom::Point3d.new(org.x + len, org.y, org.z + h),
						Geom::Point3d.new(org.x + len, org.y + d, org.z + h),
						Geom::Point3d.new(org.x, org.y + d, org.z + h)
					]
				}
				return { grooves: grooves, faces: faces }
			end
		end
	end
end