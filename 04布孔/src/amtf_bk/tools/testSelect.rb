# encoding: UTF-8
module AMTF
    module BUKONG      
      module SelectModule1 
         class SelectTool1 < Tools::Base   
             STS_CLASS_NAME   = OB[:select_tool].freeze    
            def enableVCB?
               if @state == "MOVE2"
                  return true
               end
            end
            def onUserText(text, view)
               if @state == "MOVE2"
                  begin
                     value = text.to_l
                  rescue
                     # Error parsing the text
                     UI.messagebox("Cannot convert #{text} to a Length")
                     value = nil
                     Sketchup::set_status_text "", SB_VCB_VALUE
                  end
                  return if ! value
                  vec = @ip2.position - @mpt1
                  if( vec.length == 0.0 )
                     UI.messagebox("Zero length invalid !")
                     return
                  end
                  vec.length = value
                  dx = vec.x
                  dy = vec.y
                  dz = vec.z
                  do_stretch(dx,dy,dz)
                  reset(@rectmode)
               end
              end
            def initialize()
               @option_comp = "MAKE UNIQUE"#"NOT STRETCH"#"MODIFY ALL"
               @option_curve = true
               @option_arc = true
            end
            def reset(rectmode=true)
               @dt = Time.now.to_f#Phân biệt giữa kéo và ch�?định điểm bằng thời gian nhấp chuột phải
               @ldn = false#Đối với các biện pháp đối phó khi nhấp chuột phải được nói bên ngoài phạm vi màn hình
               @pts = []
               @spts = []
               @movelist = Hash.new
               @drawn = false
               @rectmode= rectmode
               @state="DRAW AREA"
               #@state="MOVE"
               @mpt1 = []
               @mpt2 = []
                  @ip.clear
                  @ip1.clear
                  @ip2.clear
               @undobuff = 0
               @modify_dflist = []
               Sketchup.active_model.active_view.lock_inference
               Sketchup.active_model.selection.clear
            end
            def activate
               @@active_tool_class = self.class
               @ip  = Sketchup::InputPoint.new
               @ip1 = Sketchup::InputPoint.new
               @ip2 = Sketchup::InputPoint.new
               @dialog = tool_options()
               @select_type = "regtangle"
               Sketchup.active_model.active_view.invalidate
               reset
            end
            def deactivate(view)
               @@active_tool_class = nil
               @dialog.close
               if @undobuff > 0
                  Sketchup.undo
               end
               view.invalidate if @drawn == false
            end
            def onCancel(reason, view)
               #p "cancel"
               if @undobuff > 0
                  Sketchup.undo
               end
               reset(@rectmode)
            end
            def tool_options
               data = {}
               dialog = SelectOptions.create_dialog               
               dialog.show
               dialog.add_action_callback("set_type") { 
                  |action_context , params|  
                  @select_type = params
                  nil
               }  
               return dialog
            end
            def onLButtonDoubleClick(flags, x, y, view)
               puts "onLButtonDoubleClick: flags = #{flags}"
               puts "                          x = #{x}"
               puts "                          y = #{y}"
               puts "                       view = #{view}"
             end
            # def getMenu(menu)
            #    if @state=="DRAW AREA"
            #    #elsif @state=="MOVE"
            #    #end
            #       if @rectmode and @rectmode == true
            #          menu.add_item("Change Mode(Polyline By Clicking)") {
            #             reset(false)
            #          }
            #       else
            #          menu.add_item("Change Mode(Rectangle By Dragging)") {
            #             reset(true)
            #          }
            #          if @pts.size > 0
            #             menu.add_item("Undo Point") {
            #                @pts.pop
            #             }
            #             menu.add_item("Clear Points") {
            #                @pts = []
            #             }
            #          end
            #          menu.add_item("Close") {
            #             view = Sketchup.active_model.active_view
            #             @state="MOVE1"
            #             do_select_through()
            #             #do_select_through(view)
            #             #do_stretch
            #             @pts = []
            #          }
            #       end
            #    else
            #       menu.add_item("Input By Numerics") {
            #          prompts = ["X","Y","Z"]
            #          defaults = ["0","0","0"]
            #          res = UI.inputbox( prompts , defaults , "Stretch Values" )
            #          return if not res
            #          dx = res[0].to_l
            #          dy = res[1].to_l
            #          dz = res[2].to_l
            #          do_stretch(dx,dy,dz)
            #          reset(@rectmode)
            #       }
            #    end
            # end
            def onReturn(view)
               if @state=="DRAW AREA"
               elsif @state=="MOVE2"
                  dvec=@mpt1.vector_to(@mpt2)
                  dx = dvec.x
                  dy = dvec.y
                  dz = dvec.z
                  do_stretch(dx,dy,dz)
               end
            end
            def onLButtonDown(flags, x, y, view)
               if @state=="DRAW AREA"
                  if @rectmode == true
                     if @ldn == false
                        @dt = Time.now.to_f
                        @pts[0] = [x,y,0]
                     end
                     @ldn = true
                     #view.invalidate
                  end
               elsif @state=="MOVE2"
               end
            end
            def onLButtonUp(flags, x, y, view)
               puts "@pts #{@pts}"
               puts "@movelist #{@movelist}"
               if @state=="DRAW AREA"
                  if @rectmode == true
                     @pts[1] = [x,y,0]
                     @ldn = false
                     @state="MOVE1"
                     do_select_through()
                  else
                     @pts.push [x,y,0]
                  end
               elsif @state=="MOVE1"
                      if( @ip1.valid? )
                          @mpt1=@ip1.position
                     @state="MOVE2"
                      end
               elsif @state=="MOVE2"
                      if( @ip2.valid? )
                     @mpt2=@ip2.position
                     dvec=@mpt1.vector_to(@mpt2)
                     dx = dvec.x
                     dy = dvec.y
                     dz = dvec.z
                     do_stretch(dx,dy,dz)
                      end
               end
            end
            def onMouseEnter(view)
            end
            def onMouseLeave(view)
            end
            def onMouseMove(flags, x, y, view)
               if @state=="MOVE1"
                  # Sketchup::set_status_text "1st point for Stretching From:"
                  @ip.pick view, x, y
                  view.invalidate if( @ip.display? )
                  @ip1.copy! @ip
                  view.tooltip = @ip1.tooltip
                  view.invalidate
               elsif @state=="MOVE2"
                      # Getting the next end
                     #  Sketchup::set_status_text "2nd point for Stretching To:"
                      Sketchup::set_status_text OB[:distance], SB_VCB_LABEL
                      @ip2.pick view, x, y, @ip1
                      if( @ip2.valid? )
                          length = @ip1.position.distance(@ip2.position)
                          Sketchup::set_status_text length.to_s, SB_VCB_VALUE
                      end
                      view.tooltip = @ip2.tooltip if( @ip2.valid? )
                      view.invalidate
               else
                  if @rectmode==true
                     # Sketchup::set_status_text "Rectang Area Mode By Dragging"
                     if @ldn == true
                        @pts[1] = [x,y,0]
                        view.invalidate
                     end
                  else
                     # Sketchup::set_status_text "Polyline Area Mode By Clicking"
                     @tpt = [x,y,0]
                     view.invalidate
                  end
               end
            end
            def resume(view)
            end
            def suspend(view)
            end
            def draw(view)
               if @pts != []
                  vpts = []
                  if @rectmode == true and @pts.size > 1
                     vpts[0] = [@pts[0][0], @pts[0][1],0]
                     vpts[1] = [@pts[0][0], @pts[1][1],0]
                     vpts[2] = [@pts[1][0], @pts[1][1],0]
                     vpts[3] = [@pts[1][0], @pts[0][1],0]
                  else @pts.size > 0
                     @pts.each{|pt|
                        vpts.push pt
                     }
                     vpts.push @tpt if @tpt
                  end
                  if vpts.size > 1
                     paintcolor = 'red'#Sketchup::Color.new(255, 0, 0)
                     # paintcolor.alpha = 128
                     view.line_width= 1
                     view.drawing_color = paintcolor
                     view.draw2d(GL_LINE_LOOP,vpts)
                  end
               end
               #view.invalidate
               if @spts != []
                  view.draw_points( @spts, 5, 2, "red" )
               end
               if not @state == "DRAW AREA"
                  if @mpt1 != [] and @ip2.valid? 
                     view.set_color_from_line(@mpt1, @ip2.position)
                     if view.inference_locked?
                        view.line_width=2
                     end
                     view.draw_line( @mpt1 , @ip2.position )
                  end
                  if @mpt1 != []
                     view.draw_points @mpt1, 8, 2, "blue"
                  elsif@ip1.valid? 
                     view.draw_points @ip1.position, 8, 2, "blue"
                  end
                  if @ip2.valid? 
                     view.draw_points @ip2.position, 8, 2, "blue"
                  end
               end
               @drawn = true
            end
              def onKeyDown(key, rpt, flags, view)
                  # if( key == CONSTRAIN_MODIFIER_KEY )
                  #     if( view.inference_locked? )
                  #    #Release khóa
                  #         view.lock_inference
                  #     elsif( @state == "MOVE2" )
                  #         view.lock_inference @ip2 , @ip1
                  #     end
                  #     view.invalidate
                  # end
                  return if @ip1.nil?    
                  point1 = @ip.position
                  if key == VK_LEFT #y-axis           
                     lock_line = [ point1, [0, 1, 0]]            
                  elsif key == VK_RIGHT  
                     lock_line = [ point1, [1, 0, 0]]
                  elsif key == VK_UP  
                     lock_line = [ point1, [0, 0, 1]]
                  else
                     return            
                  end
                  lock_point = Utilities.find_point_online_with_distance( point1, lock_line[1], 1)
                  view.lock_inference(
                     Sketchup::InputPoint.new( point1),
                     Sketchup::InputPoint.new(lock_point)
                     )
                  view.invalidate
              end
              def onKeyUp(key, rpt, flags, view)
                  # if( key == CONSTRAIN_MODIFIER_KEY && view.inference_locked? )
                  # #Giải phóng khóa
                  #     view.lock_inference
                  # end
                  # view.invalidate
                  view.lock_inference
                  view.invalidate
              end
            def do_stretch(dx,dy,dz)
               opt = [0,0,0]
               dpt = [dx,dy,dz]
               #mtr = Geom::Transformation.translation(movevec)
               # Sketchup::set_status_text "Stretching Entities:Please wait..."
               Sketchup.active_model.start_operation OB[:stretch_tool], true
                  pcount = 0
                  pmax = @movelist.keys.size
                  @movelist.keys.each{|parent|
                     getarr = @movelist[parent]
                     if getarr
                        submovelist = getarr[0]
                        gtr = getarr[1]
                        opt2 = opt.transform( gtr.inverse )
                        dpt2 = dpt.transform( gtr.inverse )
                        movevec = opt2.vector_to(dpt2)
                        mtr = Geom::Transformation.translation(movevec)
                        vtlist , submovelist = submovelist.partition{|e| e.kind_of? Sketchup::Vertex }
                        crvlist , submovelist = submovelist.partition{|e| e.kind_of? Array }
                        parent.entities.transform_entities( mtr , submovelist )
                        parent.entities.transform_entities( mtr , vtlist )
                        if crvlist != []
                           crvlist.each{|crv,vts|
                              p crv
                              p vts
                              mpts = []
                              crv.vertices.each{|vt|
                                 if vts.index( vt )
                                    mpts.push vt.position.transform( mtr )
                                 else
                                    mpts.push vt.position
                                 end
                              }
                              crv.move_vertices( mpts )
                           }
                        end
                     end
                     pcount += 1
                     # Sketchup::set_status_text "Stretching Entities:Please wait...#{pcount}/#{pmax}" if ( pcount % 50 )== 0
                  }
               Sketchup.active_model.commit_operation
               reset(@rectmode)
            end
            def get_screen_point(x,y,view)
               camera = view.camera
               cdir = camera.eye.vector_to(camera.target)
               bbs = Sketchup.active_model.bounds
               dist = bbs.center.distance(camera.eye) + bbs.diagonal
               cdir.length = dist
               pos = camera.eye + cdir
               plane = [ pos, camera.direction ]
               line = view.pickray( x, y )
               ipt = Geom.intersect_line_plane(line, plane)
               return ipt
            end
            def do_select_through()
               # Sketchup::set_status_text "Getting Entities for Stretching:Please wait..."
               view = Sketchup.active_model.active_view
               pts = []
               if @rectmode==true
                  pts[0] = [ @pts[0][0], @pts[0][1] , 0 ]
                  pts[1] = [ @pts[0][0], @pts[1][1] , 0 ]
                  pts[2] = [ @pts[1][0], @pts[1][1] , 0 ]
                  pts[3] = [ @pts[1][0], @pts[0][1] , 0 ]
               else
                  pts = @pts
               end
               if pts.size < 4
                  reset(@rectmode)
                  return
               end
               areaface = pts
               sels = Sketchup.active_model.selection
               @count = 0
               @undobuff = 0
               Sketchup.active_model.start_operation OB[:stretch_tool], true
               select_through_in_area( sels , areaface )
               Sketchup.active_model.commit_operation# if @undobuff > 0
               #@movelist.values.each{|sumlist,gtr|
               #	p sumlist
               #}
               #@spts = []
               hashpts = Hash.new
               vtscount = 0
               grpcount = 0
               crvcount = 0
               othercount = 0
               @movelist.keys.each{|parent|
                  getarr = @movelist[parent]
                  if getarr
                     submovelist = getarr[0]
                     gtr = getarr[1]
                     submovelist.each{|e|
                        if e.kind_of? Sketchup::Vertex
                           vtscount += 1
                           pt3 = e.position.transform( gtr )
                           hashpts[pt3] = 0
                           # Sketchup.set_status_text "Selected::#{vtscount}Vertices and #{grpcount}Components/Groups and #{crvcount}Curves and #{othercount}Others." if( vtscount % 50 ) == 0
                        elsif e.kind_of? Sketchup::Group or e.kind_of? Sketchup::ComponentInstance
                           grpcount += 1
                           # Sketchup.set_status_text "Selected::#{vtscount}Vertices and #{grpcount}Components/Groups and #{crvcount}Curves and #{othercount}Others." if( grpcount % 10 ) == 0
                        elsif e.kind_of? Array
                           e.each{|crv,vts|
                              if vts
                                 crvcount += 1
                                 if vts.kind_of? Array
                                    vts.each{|vt|
                                       pt3 = vt.position.transform(gtr)
                                       hashpts[pt3] = 0
                                    }
                                 elsif vts.kind_of? Sketchup::Vertex
                                    pt3 = vts.position.transform(gtr)
                                    hashpts[pt3] = 0
                                 end
                              end
                           }
                           # Sketchup.set_status_text "Selected::#{vtscount}Vertices and #{grpcount}Components/Groups and #{crvcount}Curves and #{othercount}Others." if( grpcount % 5 ) == 0
                        else
                           othercount += 1
                           # Sketchup.set_status_text "Selected::#{vtscount}Vertices and #{grpcount}Components/Groups and #{crvcount}Curves and #{othercount}Others." if( othercount % 5 ) == 0
                        end
                     }
                  end
               }
               #ArrayよりもHashの方が高速なので一度ハッシュで受けてからKeysのArrayを取り出�?
               @spts = hashpts.keys
               # Sketchup.set_status_text "Selected::#{vtscount}Vertices and #{grpcount}Components/Groups and #{crvcount}Curves and #{othercount}Others."
               @pts = []
               if vtscount == 0 and grpcount == 0 and crvcount == 0 and othercount == 0
                  reset(@rectmode) 
               end
               view.invalidate
               #view.refresh
               #何も選択が無いときは最初に戻る
            end
            def select_through_in_area( sels , areaface , ents = Sketchup.active_model.active_entities , gtr = Geom::Transformation.new )
               crvlist = []
               vts_incurve = Hash.new#カーブ内の頂点で重複を避けるためのストッカー
               parent = ents.parent
               getarr = @movelist[ parent ]
               if getarr and getarr != []
                  submovelist , tr2 = getarr
               else
                  submovelist = []
               end
               ents.each{|e|
                  if e.visible? and e.layer.visible?
                     if submovelist.index(e)
                        #既に選択されている場�?
                     else
                        #選択されていない場合
                        if e.kind_of? Sketchup::Group and e.locked? == false
                           chk = grp_is_in_area( areaface , e , gtr ) 
                           if chk == 8
                              sels.add e
                              submovelist.push e
                              #このオブジェクト内の選択は除外する処�?
                           elsif chk == -9
                           else
                              if area2d_from_bounds( areaface , e , gtr )==true
                                 if e.entities.find{|e2| e2 != nil }.parent.count_instances > 1
                                    @undobuff += 1
                                    #この処理を煮詰める必要がある このグループ内の頂点などが選択されない場合は飛ばすように�?
                                    #Sketchup.active_model.start_operation "Stretch Move",true,true
                                       e.make_unique
                                    #Sketchup.active_model.commit_operation
                                 end
                                 select_through_in_area( sels , areaface , e.entities , gtr * e.transformation )
                              end
                           end
                        elsif e.kind_of? Sketchup::ComponentInstance and e.locked? == false
                           chk = grp_is_in_area( areaface , e , gtr ) 
                           if chk == 8
                              sels.add e
                              submovelist.push e
                              #このオブジェクト内の選択は除外する処�?
                           elsif chk == -9
                           else
                              if @option_comp != "NOT STRETCH"
                                 if area2d_from_bounds( areaface , e , gtr )==true
                                    if !@modify_dflist.index( e.definition )
                                       if e.definition.count_instances > 1
                                          @undobuff += 1
                                          #Sketchup.active_model.start_operation "Stretch Move",true,true
                                          #この処理を煮詰める必要がある このグループ内の頂点などが選択されない場合は飛ばすように�?
                                             e.make_unique
                                          #Sketchup.active_model.commit_operation
                                       end
                                       select_through_in_area( sels , areaface , e.definition.entities , gtr * e.transformation )
                                       if @option_comp == "MODIFY ALL"
                                          @modify_dflist.push e.definition
                                       end
                                    end
                                 end
                              end
                           end
                        elsif e.kind_of? Sketchup::Edge
                        #エッジの場合はさらに複雑な処理をする
                           crv = e.curve
                           if crv
                              if !crvlist.index(crv)
                                 crvlist.push crv
                                 if curve_is_in_area( areaface, crv , gtr , vts_incurve )
                                    sels.add crv.edges
                                    submovelist.push crv
                                 end
                              end
                           else
                              if edge_is_in_area( areaface, e , gtr )
                                 #sels.add e
                                 #submovelist.push e
                                 #このエッジ内の選択点は除外する処�?
                              end
                           end
                        elsif e.kind_of? Sketchup::ConstructionLine
                           if cline_is_in_area( areaface, e , gtr )
                              sels.add e
                              submovelist.push e
                           end
                        elsif e.kind_of? Sketchup::ConstructionPoint
                           if cpoint_is_in_area( areaface, e , gtr )
                              sels.add e
                              submovelist.push e
                           end
                        end
                     end
                  end
                  @count += 1
                  Sketchup.set_status_text "Please wait...#{@count}" if ( @count % 500 ) == 0
               }
               #moves = [ submovelist , gtr ]
               getarr = @movelist[ parent ]
               if getarr and getarr != []
                  submovelist2 , tr2 = getarr
               else
                  submovelist2 = []
               end
               submovelist2.concat(submovelist)
               @movelist[ parent ]= [ submovelist2 , gtr ]
               #####ここでプレビュー用の点座標群を更新す�?
               ##
               #submovelist2.each{|e|
               #	if e.kind_of? Sketchup::Vertex
               #		pt3 = e.position.transform( gtr )
               #		if not @spts.index( pt3 )
               #			@spts.push pt3
               #		end
               #	end
               #}
               @drawn = false
            end
            def grp_is_in_area( areaface , ent , gtr )
               bbs = ent.bounds
               view = Sketchup.active_model.active_view
               #eye = view.camera.eye
               chk = 0
               bbareaface = []
               (0..7).each{|i|
                  pt = bbs.corner(i).transform(gtr)
                  bbareaface.push view.screen_coords( pt )
                  if check_pt_in_area( areaface , bbs.corner(i) , gtr ) == true
                     chk += 1
                  else
                     chk += -1
                  end
               }
               if chk == -8
                  bbchk = 0
                  areaface.each{|vpt|
                     if check_pt_in_area( bbareaface ,nil , gtr ,vpt) == true
                        bbchk += 1
                     else
                        bbchk += -1
                     end
                  }
                  if bbchk == -8
                     chk = -9
                  end
               end
               return chk
            end
            def cpoint_is_in_area( areaface , cpoint , gtr )
               pts = []
               pts[0] = cpoint.position
               chk = check_pts_in_area( areaface , pts , gtr )
               return chk
            end
            def cline_is_in_area( areaface , cline , gtr )
               pts = []
               pts[0] = cline.start
               pts[1] = cline.end
               return false if not pts[0] or not pts[1]
               chk = check_pts_in_area( areaface , pts , gtr )
               return chk
            end
            def curve_is_in_area( areaface , crv , gtr , vts_incurve )
               movelist = []
               parent = crv.parent
               getarr = @movelist[ parent ]
               if getarr and getarr != []
                  submovelist , tr2 = getarr
               else
                  submovelist = []
               end
               submovelist = [] if submovelist == nil
               chk = 0
               cnt = 0
               #vts = []
               #vts[0] = crv.vertices[0]
               #vts[1] = crv.vertices[-1]
               vts = crv.vertices
               vts.each{|vt|
                  if check_pt_in_area( areaface , vt.position ,gtr )
                     movelist.push vt
                     chk += 1
                  else
                     chk += -1
                  end
                  cnt += 1
               }
               #if chk == cnt
                  #movelist.each{|mvt|
                  #	submovelist.reject!{|k,v| k == mvt }
                  #}
                  #@movelist[parent] = [ submovelist , gtr ]
               #	return true
               if chk == cnt
                  return true
               elsif chk != -cnt# and !crv.kind_of? Sketchup::ArcCurve
                  if crv.kind_of? Sketchup::ArcCurve
                     #円弧カーブで途中の点が選択される場合は一度通常のカーブにす�?
                     #この際idは変化しないことを確認し�?
                     pts = crv.vertices.map{|vt| vt.position }
                     crv.edges[0].explode_curve
                     eds = parent.entities.add_curve( pts )
                     crv = eds[0].curve
                  end
                  if @option_curve == true
                     crv_vts = [nil]
                     movelist.each{|mvt|
                        #if not submovelist.index(mvt)
                        if !vts_incurve.key?(mvt)
                           vts_incurve[mvt] = 0
                           crv_vts.push mvt
                        end
                        #end
                     }
                     crarr = [ crv , crv_vts ]
                     submovelist.push crarr
                     @movelist[parent] = [ submovelist , gtr ]
                     return nil
                  end
               end
               return nil
            end
            def edge_is_in_area( areaface , ed , gtr )
               parent = ed.parent
               getarr = @movelist[ parent ]
               if getarr and getarr != []
                  submovelist , tr2 = getarr
               else
                  submovelist = []
               end
               submovelist = [] if submovelist == nil
               movelist = []
               chk = 0
               cnt = 0
               ed.vertices.each{|vt|
                  #頂点がカーブ内にある場合は除外す�?
                  #curve_interiorでは上手く判定できない場合がある
                  #if vt.curve_interior? == nil
                  if vt.edges.find{|ed| ed.curve } == nil
                     #p vt.curve_interior?
                     if check_pt_in_area( areaface , vt.position ,gtr )
                        movelist.push vt
                        chk += 1
                     else
                        chk += -1
                     end
                     cnt += 1
                  else
                  #elsif !vts_incurve.key?(vt)
                  #	vts_incurve[vt] = 0
                  #	vts = [];vts.push vt
                  #	submovelist.push [ vt.curve , vts ]
                  #	@movelist[parent] = [ submovelist , gtr ]
                  end
               }
               movelist.each{|mvt|
                  if not submovelist.index(mvt)
                     submovelist.push mvt
                  end
               }
               @movelist[parent] = [ submovelist , gtr ]
               #if chk == cnt
                  #movelist.each{|mvt|
                  #	submovelist.reject!{|k,v| k == mvt }
                  #}
                  #@movelist[parent] = [ submovelist , gtr ]
               #	return true
               #else
               return nil
               #end
               #pts = ed.vertices.map{|vt| vt.position }
               #chk = check_pts_in_area( areaface , pts , gtr )
               #return chk
            end
            def check_pts_in_area( areaface , pts , gtr )
               chk = 0
               cnt = 0
               pts.each{|pt|
                  if check_pt_in_area( areaface , pt ,gtr )
                     chk += 1
                  else
                     chk += -1
                  end
                  cnt += 1
               }
               if chk == cnt
                  return true
               else
                  return nil
               end
            end
            def check_pt_in_area( areaface , pt , gtr , xy = nil)
               #areafaceを画面上の座標として考え�?
               #geomにpoint_in_polygon_2Dがあったのでそれに乗り換�?
               view = Sketchup.active_model.active_view
               #eye = Sketchup.active_model.active_view.camera.eye
               if !xy
                  cpt = pt.transform(gtr)
                  xy = view.screen_coords( cpt )
               end
               xy.z = 0.0
               if Geom.point_in_polygon_2D(xy, areaface,true) == true
                  return true
               end
               return false
=begin
   #偏角の合計から内外を判定する
         sumang = 0.0
         indice = []
         (areaface.size).times{|i|
         indice.push( i -1 )
         }
         indice.each{|i|
            apt = areaface[i]
            bpt = areaface[i+1]
            vec1 = xy.vector_to(apt)
            vec2 = xy.vector_to(bpt)
            ang1 = Math.atan2( vec1.y , vec1.x ).radians
            ang2 = Math.atan2( vec2.y , vec2.x ).radians
            ang1 = 360 + ang1 if ang1 < 0.0
            ang2 = 360 + ang2 if ang2 < 0.0
            ang3 = ang2- ang1
            if ang3 > 180
               ang3 = ang3 - 360
            elsif ang3 < -180
               ang3 = ang3 + 360
            end
            sumang += ang3
         }
         if sumang.abs < 0.01
            #偏角の合計が0に近いので外
            return false
         else
            #�?
            return true
         end
=end
            end
            def area2d_from_bounds( areaface , e , gtr )
               #バウンディングボックスの頂点で囲まれた範囲内にareaface点群が含まれるかをチェックす�?
               gtr2 = gtr * e.transformation
               view = Sketchup.active_model.active_view
               cdir = view.camera.direction
               cdir2 = cdir.transform( gtr2.inverse )
               if e.kind_of? Sketchup::Group
                  bbsc = e.local_bounds
               else
                  bbsc = e.definition.bounds
               end
               bareas = []
               if cdir.x < 0
                  bareas.push [1,3,7,5].map{|i| view.screen_coords( bbsc.corner(i).transform(gtr2) ) }
               elsif cdir.x > 0
                  bareas.push [0,2,6,4].map{|i| view.screen_coords( bbsc.corner(i).transform(gtr2) ) }
               end
               if cdir.y < 0
                  bareas.push [2,3,7,6].map{|i| view.screen_coords( bbsc.corner(i).transform(gtr2) ) }
               elsif cdir.y > 0
                  bareas.push [0,1,5,4].map{|i| view.screen_coords( bbsc.corner(i).transform(gtr2) ) }
               end
               if cdir.z < 0
                  bareas.push [4,5,7,6].map{|i| view.screen_coords( bbsc.corner(i).transform(gtr2) ) }
               elsif cdir.z > 0
                  bareas.push [0,1,3,2].map{|i| view.screen_coords( bbsc.corner(i).transform(gtr2) ) }
               end
               bareas.each{|barea|
                  #Sketchup.active_model.active_entities.add_face( barea )
                  areaface.each{|xy|
                     if check_pt_in_area( barea , nil , gtr , xy) == true
                        #p "check_pt_in_area true"
                        return true
                     end
                  }
               }
               #全ての頂点はbareas外にある、線分の交差判定を行�?
               bareas.each{|barea|
                  if check_int_bounds_area( areaface , barea ) == true
                     #p "check_int_bounds_area true"
                     return true
                  end
               }
               return false
            end
            def check_int_bounds_area( areaface , barea )
               p "check_int_bounds_area"
               [[0,1],[1,2],[2,3],[3,0]].each{|i1,i2|
                  xy = [ barea[i1] , barea[i2] ]
                  if check_int_line_area( areaface , xy )
                     return true
                  else
                  end
               }
               return false
            end
            def check_int_line_area( areaface , xy1 )
               view = Sketchup.active_model.active_view
               xy1[0].z = 0.0
               xy1[1].z = 0.0
               indice = []
               (areaface.size).times{|i|
               indice.push( i -1 )
               }
               xy2 = []
               indice.each{|i|
                  xy2[0] = areaface[i]
                  xy2[1] = areaface[i+1]
                  xy2[0].z = 0.0
                  xy2[1].z = 0.0
                  if check_intersect_edges(xy1,xy2) == true
                     return true
                  end
               }
               return false
            end
            def check_intersect_edges(pts1,pts2)
               pts1[0].z = 0
               pts1[1].z = 0
               pts2[0].z = 0
               pts2[1].z = 0
               line1 = [pts1[0],pts1[0].vector_to(pts1[1])]
               dist1 = pts1[0].distance(pts1[1]) + 0.0.mm
               line2 = [pts2[0],pts2[0].vector_to(pts2[1])]
               dist2 = pts2[0].distance(pts2[1]) + 0.0.mm
               if dist1 <= 0.0 or dist2 <= 0.0
               else
                  ipt = Geom.intersect_line_line(line1, line2)
                  if ipt
                     ilen = ipt.distance(pts1[0]) + ipt.distance(pts1[1])
                     ilen2 = ipt.distance(pts2[0]) + ipt.distance(pts2[1])
                     if ilen <= ( dist1 *1.001 ) and ilen2 <= ( dist2 * 1.001 )
                        return true
                     end
                  end
               end
               return false
            end
         end
      end
    end
end