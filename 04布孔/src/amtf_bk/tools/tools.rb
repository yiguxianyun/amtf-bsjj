# encoding: UTF-8
module AMTF
	module BUKONG
		module Tools
			class Base
				@@active_tool_class = nil
				def initialize
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					if AMTF_STORE.init_open_json == false
						saver = AmtfSaver.new
						saver.load_comp
						AMTF_STORE.init_open_json = true
					end
				end
				def self.active?
					@@active_tool_class == self
				end
				def self.tool_activate
					Sketchup.active_model.select_tool(new)
				end
				def activate
					@@active_tool_class = self.class
					@input = Sketchup::InputPoint.new
					display_status
				end
				def deactivate(_view)
					@@active_tool_class = nil
				end
				# Display status text
				def display_status
					Sketchup.status_text = self.class::STS_CLASS_NAME
				end
				def self.identifier
					# Based on Tool's class name.
					name.split('::').last.downcase.to_sym
				end
				def getInstructorContentDirectory
					# File.join(PLUGIN_DIR, "instructor", "#{self.class.identifier}.html")
				end
				def onCancel(_reason, _view)
					reset
				end
				# @see http://ruby.sketchup.com/Sketchup/Tool.html
				def resume(_view)
					display_status
				end
				# Reset tool to its original state.
				def reset
					Sketchup.active_model.selection.clear
					display_status
				end
				def pick_frame_data_1
					frame = nil
					pickpaths = {}
					for index in 0..@ph.count - 1
						pickpath = @ph.path_at(index)
						pickpaths[index] = pickpath
					end
					found = {}
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							if path.first.is_a?(Sketchup::Group)
								frame =
									AMTF_STORE.frame_premitives[
										path.first.persistent_id
									]
								if frame
									for i in (path.size - 1).downto(0)
										if path[i].is_a?(Sketchup::Group)
											sub_grp = path[i]
											sub_comp_type =
												sub_grp.get_attribute 'amtf_dict',
												                      'comp_type'
											section_id =
												sub_grp.get_attribute 'amtf_dict',
												                      'section_id'
											sheet_id =
												sub_grp.get_attribute(
													'amtf_dict',
													'sheet_id'
												)
											sheet_type =
												sub_grp.get_attribute 'amtf_dict',
												                      'sheet_type'
											if sheet_type == 'HOZ_DIV' ||
													sheet_type == 'VER_DIV'
												found = {
													index: index,
													path: path,
													sub_index: i,
													frame: frame,
													sheet: sub_grp,
													found_sub_group: false,
													found_sheet: true,
													sheet_id: sheet_id,
													sheet_type: sheet_type
												}
												break
											elsif sub_comp_type ==
													'AmtfSubFrame'
												found = {
													index: index,
													path: path,
													sub_index: i,
													frame: frame,
													sub_grp: sub_grp,
													found_sub_group:
														sub_grp != path.first,
													found_sheet: false,
													section_id: section_id
												}
												break
											end
										end
									end
								end
							end
							break if !found.empty?
						end
					end
					if found.empty? && frame
						found = {
							frame: frame,
							found_sheet: false,
							found_sub_group: false
						}
					end
					return found
				end
				def pick_frame_data_3
					frame = nil
					pickpaths = {}
					for index in 0..@ph.count - 1
						pickpath = @ph.path_at(index)
						pickpaths[index] = pickpath
					end
					found = {}
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							if path.first.is_a?(Sketchup::Group)
								frame =
									AMTF_STORE.frame_premitives[
										path.first.persistent_id
									]
								if frame
									for i in (path.size - 1).downto(0)
										if path[i].is_a?(Sketchup::Group)
											sub_grp = path[i]
											sub_comp_type =
												sub_grp.get_attribute 'amtf_dict',
												                      'comp_type'
											section_id =
												sub_grp.get_attribute 'amtf_dict',
												                      'section_id'
											node_id =
												sub_grp.get_attribute 'amtf_dict',
												                      'node_id'
											if sub_comp_type == 'AmtfSubFrame'
												sub_root = {}
												frame
													.traverseCallback do |node|
													if node['root']['id'] ==
															node_id
														sub_root = node['root']
													end
												end
												found = {
													index: index,
													path: path,
													sub_index: i,
													frame: frame,
													sub_grp: sub_grp,
													found_sub_group:
														sub_grp != path.first,
													section_id: section_id,
													sub_root: sub_root,
													node_id: node_id
												}
												break
											end
										end
									end
								end
							end
							break if !found.empty?
						end
					end
					if found.empty? && frame
						found = {
							frame: frame,
							found_frame: true,
							found_sub_group: false
						}
					end
					return found
				end
				def pick_frame_data_2
					frame = nil
					pickpaths = {}
					for index in 0..@ph.count - 1
						pickpath = @ph.path_at(index)
						pickpaths[index] = pickpath
					end
					found = {}
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							if path.first.is_a?(Sketchup::Group)
								frame =
									AMTF_STORE.frame_premitives[
										path.first.persistent_id
									]
								if frame
									for i in (path.size - 1).downto(0)
										if path[i].is_a?(Sketchup::Group)
											sub_grp = path[i]
											sub_comp_type =
												sub_grp.get_attribute 'amtf_dict',
												                      'comp_type'
											section_id =
												sub_grp.get_attribute 'amtf_dict',
												                      'section_id'
											sheet_id =
												sub_grp.get_attribute(
													'amtf_dict',
													'sheet_id'
												)
											sheet_type =
												sub_grp.get_attribute 'amtf_dict',
												                      'sheet_type'
											sheet_index =
												sub_grp.get_attribute 'amtf_dict',
												                      'sheet_index'
											back_id =
												sub_grp.get_attribute 'amtf_dict',
												                      'back_id'
											door_id =
												sub_grp.get_attribute 'amtf_dict',
												                      'door_id'
											if !sheet_id.nil? &&
													!sheet_type.nil?
												found = {
													index: index,
													path: path,
													sub_index: i,
													frame: frame,
													sheet: sub_grp,
													found_sub_group: false,
													found_sheet: true,
													sheet_id: sheet_id,
													sheet_type: sheet_type,
													sheet_index: sheet_index,
													back_id: back_id,
													door_id: door_id
												}
												break
											elsif sub_comp_type ==
													'AmtfSubFrame'
												found = {
													index: index,
													path: path,
													sub_index: i,
													frame: frame,
													sub_grp: sub_grp,
													found_sub_group:
														sub_grp != path.first,
													found_sheet: false,
													section_id: section_id
												}
												break
											end
										end
									end
								end
							end
							break if !found.empty?
						end
					end
					if found.empty? && frame
						found = {
							frame: frame,
							found_sheet: false,
							found_sub_group: false
						}
					end
					return found
				end
				def pick_frame_data
					frame = nil
					pickpaths = {}
					for index in 0..@ph.count - 1
						pickpath = @ph.path_at(index)
						pickpaths[index] = pickpath
					end
					if !pickpaths.empty?
						pickpaths.each do |index, value|
							if value.first.is_a?(Sketchup::Group)
								frame =
									AMTF_STORE.frame_premitives[
										value.first.persistent_id
									]
							end
						end
					end
					return { frame: frame }
				end
				# đang có pick_sheet, pick_face, pick_face_or_edge  giống nhau, do các function làm trước đó nằm rải rác,
				# lúc nào có thời gian s�?làm gọn lại
				# 目前pick_sheet、pick_face、pick_face_or_edge功能相似，因为之前的功能分散，有时间会整理一下。
				def make_unique_when_picked(ph, x, y)
					ph.do_pick(x, y)
					pickpaths = {}
					for index in 0..ph.count - 1
						pickpath = ph.path_at(index)
						pickpaths[index] = pickpath
					end
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							for i in (path.size - 1).downto(0)
								if Utilities.instance?(path[i])
									path[i].make_unique
								end
							end
						end
					end
					nil
				end

				# 这个函数接受三个参数：`ph`，`x`和`y`。
				# `ph`参数是一个具有`do_pick`和`path_at`方法的类的对象。`do_pick`方法使用`x`和`y`参数进行调用。
				# `path_at`方法在循环中被调用，以获取`ph`中每个索引处的路径。这些路径被存储在一个名为`pickpaths`的哈希表中，索引作为键。
				# 然后，函数检查`pickpaths`是否为空。如果不为空，它会遍历`pickpaths`中的每个路径，并检查路径的第一个元素是否是`Utilities`类的实例。如果是，则检查第一个元素是否是`Sketchup::Group`，并根据组的持久ID从全局变量`AMTF_STORE.frame_premitives`中获取框架。
				# 函数然后按逆序遍历路径，并检查每个元素是否是实体。如果是，则将相关信息（如框架、索引、路径、子索引、板件等）设置为`found`哈希表。
				# 如果`found`不为空，则通过将路径中每个元素的变换矩阵相乘来计算变换矩阵。它还通过将路径中除最后一个元素外的每个元素的变换矩阵相乘来计算父矩阵。
				# 如果`pick_entity`是`Sketchup::Edge`的实例，则找到该边所属的大面，并根据边是否在第一个或第二个大面的边缘中确定选取面。
				# 最后，函数返回`found`哈希表。
				def pick_sheet(ph, x, y)
					make_unique_when_picked(ph, x, y)
					ph.do_pick(x, y)
					pickpaths = {}
					for index in 0..ph.count - 1
						pickpath = ph.path_at(index)
						pickpaths[index] = pickpath
					end
					found = {}
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							if Utilities.instance?(path.first)
								frame = nil
								if path.first.is_a?(Sketchup::Group)
									frame =AMTF_STORE.frame_premitives[path.first.persistent_id]
								end
								for i in (path.size - 1).downto(0)
									if Utilities.solid?(path[i])
										found = {
											frame: frame,
											index: index,
											path: path,
											sub_index: i,
											pick_component: path[i],
											pick_entity: path.last
										}
										break
									end
								end
							end
							break if !found.empty?
						end
					end
					if !found.empty?
						matrix = IDENTITY
						trans_path = found[:path]
						sub_index = found[:sub_index]
						for i in 0..sub_index
							matrix = matrix * trans_path[i].transformation
						end
						parent_matrix = IDENTITY
						for i in 0..sub_index - 1
							parent_matrix =parent_matrix * trans_path[i].transformation
						end
						found[:parent_matrix] = parent_matrix
						found[:trans] = matrix
						pick_face = nil
						if found[:pick_entity].is_a?(Sketchup::Edge)
							big_faces =Utilities.get_big_faces(
									found[:pick_component],
									found[:trans]
								)
							return {} unless big_faces
							edges0 = big_faces[0].edges
							edges1 = big_faces[1].edges
							if edges0.include?(found[:pick_entity])
								pick_face = big_faces[0]
							elsif edges1.include?(found[:pick_entity])
								pick_face = big_faces[1]
							end
						else
							pick_face = found[:pick_entity]
						end
						found[:pick_face] = pick_face
					end
					return found
				end

				def pick_face(ph, x, y)
					make_unique_when_picked(ph, x, y)
					ph.do_pick(x, y)
					pickpaths = {}
					for index in 0..ph.count - 1
						pickpath = ph.path_at(index)
						pickpaths[index] = pickpath
					end
					found = {}
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							if Utilities.instance?(path.first)
								for i in (path.size - 1).downto(0)
									if Utilities.solid?(path[i])
										pick_face = nil
										if path.last.is_a?(Sketchup::Edge)
											big_faces =
												Utilities.get_big_faces(
													path.first,
													path.first.transformation
												)
											next unless big_faces
											p1 = path.last.start.position
											if Utilities.within_face?(
													p1,
													big_faces[0]
											   )
												pick_face = big_faces[0]
											elsif Utilities.within_face?(
													p1,
													big_faces[1]
											    )
												pick_face = big_faces[1]
											end
										else
											pick_face = path.last
										end
										found = {
											index: index,
											path: path,
											sub_index: i,
											pick_component: path[i],
											pick_entity: path.last,
											pick_face: pick_face
										}
										break
									end
								end
							end
							break if !found.empty?
						end
					end
					if !found.empty?
						matrix = IDENTITY
						trans_path = found[:path]
						sub_index = found[:sub_index]
						for i in 0..sub_index
							matrix = matrix * trans_path[i].transformation
						end
						parent_matrix = IDENTITY
						for i in 0..sub_index - 1
							parent_matrix =
								parent_matrix * trans_path[i].transformation
						end
						found[:parent_matrix] = parent_matrix
						found[:trans] = matrix
					end
					return found
				end
				def pick_face_or_edge(ph, x, y)
					make_unique_when_picked(ph, x, y)
					ph.do_pick(x, y)
					pickpaths = {}
					for index in 0..ph.count - 1
						pickpath = ph.path_at(index)
						pickpaths[index] = pickpath
					end
					found = {}
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							if Utilities.instance?(path.first)
								for i in (path.size - 1).downto(0)
									if Utilities.solid?(path[i])
										pick_face = nil
										if path.last.is_a?(Sketchup::Edge)
											big_faces =
												Utilities.get_big_faces(
													path.first,
													path.first.transformation
												)
											next unless big_faces
											p1 = path.last.start.position
											if Utilities.within_face?(
													p1,
													big_faces[0]
											   )
												pick_face = big_faces[0]
											elsif Utilities.within_face?(
													p1,
													big_faces[1]
											    )
												pick_face = big_faces[1]
											end
										else
											pick_face = path.last
										end
										found = {
											index: index,
											path: path,
											sub_index: i,
											pick_component: path[i],
											pick_entity: path.last,
											pick_face: pick_face
										}
										break
									end
								end
							end
							break if !found.empty?
						end
					end
					if !found.empty?
						matrix = IDENTITY
						trans_path = found[:path]
						sub_index = found[:sub_index]
						for i in 0..sub_index
							matrix = matrix * trans_path[i].transformation
						end
						parent_matrix = IDENTITY
						for i in 0..sub_index - 1
							parent_matrix =
								parent_matrix * trans_path[i].transformation
						end
						found[:parent_matrix] = parent_matrix
						found[:trans] = matrix
					end
					return found
				end
			end #Base
			class FirstPriorityTool < Base
				STS_CLASS_NAME = OB[:priority].freeze
				def onLButtonDown(flags, x, y, view)
					@input.pick view, x, y
					get_pick = pick_face_or_edge(@ph, x, y)
					return if get_pick.empty?
					板件 = get_pick[:pick_component]
					pick_face = get_pick[:pick_face]
					priority_type =
						pick_face.get_attribute('amtf_dict', 'priority_type')
					big_faces =
						Utilities.get_big_faces(
							板件,
							get_pick[:trans]
						)
					oposit_face = big_faces[1]
					oposit_face = big_faces[0] if pick_face == big_faces[1]
					mod = Sketchup.active_model
					mod.start_operation(OB[:priority], true)
					if priority_type.nil? || priority_type === 'second_priority'
						pick_face.set_attribute 'amtf_dict',
						                        'priority_type',
						                        'first_priority'
						oposit_face.set_attribute 'amtf_dict',
						                          'priority_type',
						                          'second_priority'
					elsif priority_type === 'first_priority'
						pick_face.set_attribute 'amtf_dict',
						                        'priority_type',
						                        'second_priority'
						oposit_face.set_attribute 'amtf_dict',
						                          'priority_type',
						                          'first_priority'
					end
					part =
						AmtfSheet.new(
							板件,
							get_pick[:trans],
							get_pick[:parent_matrix]
						)
					subGroup =
						Utilities
							.definition(part.comp)
							.entities
							.grep(Sketchup::Group)
					if subGroup.size > 0
						for i in 0..subGroup.size - 1
							group_type =
								subGroup[i].get_attribute('amtf_dict', 'type')
							if group_type == 'bukong_label'
								subGroup[i].erase!
								break
							end
						end
					end
					part.drawArrow(pick_face)
					mod.commit_operation
				end
			end 

			class A旋转标签 < Base
				STS_CLASS_NAME = OB[:旋转标签].freeze
				def onLButtonDown(flags, x, y, view)
					mod = Sketchup.active_model
					mod.start_operation(OB[:旋转标签], true)
					@input.pick view, x, y
					get_pick = pick_sheet(@ph, x, y)
					return if get_pick.empty?
					板件 = get_pick[:pick_component]
					板件obj =	AmtfSheet.new(板件,板件.transformation,IDENTITY)
					板h=板件obj.获取板信息
					puts 板h[:长边长]
					puts 板h[:短边长]
					长短比=板h[:长边长]/板h[:短边长]

					标签=板件obj.获取标签
					if 标签.nil?
						result = UI.messagebox("没找到标签对象！")
						return
					end
					tr = 标签.transformation
					标签平行长边吗=板件obj.标签平行长边吗
					rot =Geom::Transformation.rotation tr.origin,tr.zaxis,90.degrees
					标签.transform!(rot)
					if 标签平行长边吗
						缩放比例=1/长短比
					else
						缩放比例=长短比
					end
					sr = Geom::Transformation.scaling(tr.origin,缩放比例)
					#就得先旋转，再缩放↓
					标签.transform! sr

					# pick_face = get_pick[:pick_face]
					# subGroup =
					# 	Utilities
					# 		.definition(板件)
					# 		.entities
					# 		.grep(Sketchup::Group)
					# return unless subGroup.size > 0
					# subGroup.each do |ent|
					# 	label = ent.get_attribute('amtf_dict', 'type')
					# 	if (label == 'bukong_label')
					# 		tr = ent.transformation
					# 		# tr = 板件.transformation*ent.transformation
					# 		# puts "tr.origin1:"
					# 		# puts tr.origin
					# 		# puts "tr.zaxis:"
					# 		# puts tr.zaxis
					# 		# 大面s = 板件.big_faces[0]

					# 		puts "ent.bounds:"
					# 		puts ent.bounds.width
					# 		puts ent.bounds.height
					# 		puts ent.bounds.depth

					# 		puts "板件.bounds:"
					# 		puts 板件.bounds.width
					# 		puts 板件.bounds.height
					# 		puts 板件.bounds.depth
					# 		amtf.nil


					# 		# zAxes = pick_face.normal
					# 		# center = ent.bounds.center
					# 		# amtf.nil
					# 		# unless Utilities.within_face?(center, pick_face)
					# 		# 	puts "pick_face"
					# 		# 	puts pick_face
					# 		# 	return
					# 		# end
					# 		rot =Geom::Transformation.rotation tr.origin,tr.zaxis,90.degrees
					# 		ent.transform!(rot)
					# 	end
					# end
					mod.commit_operation
				end
			end # LabelRotate

			class A翻转标签 < Base
				STS_CLASS_NAME = OB[:翻转标签].freeze
				def onLButtonDown(flags, x, y, view)
					mod = Sketchup.active_model
					mod.start_operation(OB[:翻转标签], true)
					@input.pick view, x, y
					get_pick = pick_sheet(@ph, x, y)
					return if get_pick.empty?
					板件 = get_pick[:pick_component]
					板件obj =	AmtfSheet.new(板件,板件.transformation,IDENTITY)
					板厚=板件obj.sheet_thick
					puts "板厚"
					puts 板厚

					标签=板件obj.获取标签
					if 标签.nil?
						result = UI.messagebox("没找到标签对象！")
						return
					end
					tr = 标签.transformation
					tr到中心 =Geom::Transformation.new(Utilities.vector_multiply(-0.5 * 板厚,tr.zaxis))
					板中心点 = tr.origin.transform(tr到中心)

					rot =Geom::Transformation.rotation 板中心点,tr.xaxis,180.degrees
					标签.transform!(rot)
					mod.commit_operation
				end
			end # A翻转标签

			class HideSheet < Base
				STS_CLASS_NAME = OB[:hide_sheet].freeze
				def onLButtonDown(flags, x, y, view)
					mod = Sketchup.active_model
					mod.start_operation(OB[:hide_sheet], true)
					@input.pick view, x, y
					get_pick = pick_sheet(@ph, x, y)
					return if get_pick.empty?
					板件 = get_pick[:pick_component]
					板件.visible = false if 板件
					mod.commit_operation
				end
			end # HideSheet
		end #Tools
	end #BuKong
end #Amtflogic