# encoding: UTF-8
module AMTF
    module BUKONG
    module CurveGrooveTool
      class CurveGroove < Tools::Base  
        STS_CLASS_NAME   = OB[:curve_groove_tool].freeze
         def onLButtonDown(flags, x, y, view)
           mod = Sketchup.active_model
           mod.start_operation(OB[:curve_groove_tool], true)
           @ph.do_pick(x, y)         
            pick_arr = @ph.path_at(0)         
            return unless pick_arr 
            pick_component = pick_arr[pick_arr.size - 2]  
            pick_face  = pick_arr[pick_arr.size - 1] 
            return unless Utilities.instance?(pick_component) && pick_face.valid? && pick_face.is_a?(Sketchup::Face)           
            explode = Utilities.get_comp_attribute(pick_component, 'amtflogic_exploded', 'amtf_dict')     
            if explode != 'true'
               UI.messagebox OB[:explode_required], MB_OK
               return
            end
            selection = Sketchup.active_model.selection
            return unless selection.size > 0
            curve_groove = selection[0]
            return unless curve_groove.is_a?(Sketchup::Group)
            found_grp = false
            curve_groove.entities.each do
              |ent|
              if ent.is_a?(Sketchup::Group) 
                found_grp = true  
                break
              end
            end
            if found_grp
              UI.messagebox OB[:found_sub_group], MB_OK
               return
            end            
            bounds = curve_groove.bounds
            # Tách các đường kín liên tục thành các dối tượng và tạo thành group
            # Các dối tượng có cùng layer thì sau này s�?lấy cùng kiểu đường chạy dao
            bound_edges = curve_groove.entities.grep(Sketchup::Edge)
            Sketchup.active_model.selection.clear
            multiPath = TTLibrary.getMultiPathPointsHash(bound_edges)
            for i in 0..multiPath.size - 1
              ptr_arr = multiPath[i][:path]
              temp_arr = []
              for j in 0..ptr_arr.size - 1               
                ptr = Utilities.point_context_convert( ptr_arr[j][:point], curve_groove.transformation, pick_component.transformation)
                ptr = ptr.project_to_plane(pick_face.plane)
                #TODO: Check transformation. This is nly correct if exploded
                temp_arr << ptr
              end
              new_group = pick_component.entities.add_group 
              new_group.layer = ptr_arr[0][:layer]
              temp_arr = temp_arr.map{|pt| pt = pt.transform(new_group.transformation.inverse)}
              for j in 0..temp_arr.size - 2
                new_group.entities.add_line temp_arr[j], temp_arr[j+1]
              end
              new_group.set_attribute 'amtf_dict', 'type', 'curve_groove' 
              new_group.set_attribute 'amtf_dict', 'closed', multiPath[i][:closed]
              new_group.set_attribute 'amtf_dict', 'minifixID', Utilities.CreatUniqueid()
            end
            # Tạo id cho comp và id cho loop
            comp_id = pick_component.get_attribute('amtf_dict', 'pathcomp_id')
            if comp_id.nil?
              comp_id = Utilities.CreatUniqueid()
              pick_component.set_attribute 'amtf_dict', 'pathcomp_id', comp_id
              big_faces = Utilities.get_big_faces(pick_component)
              big_faces.each do |f|
                f.loops.each do |lo|
                  lo_id = Utilities.CreatUniqueid()
                  lo.set_attribute 'amtf_dict', 'loop_id', lo_id
                end
              end
            end
           curve_groove.erase!
           mod.commit_operation
         end 
      end#Class
      end #Module
    end#BuKong
end# Amtf