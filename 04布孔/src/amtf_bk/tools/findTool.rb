# encoding: UTF-8
module AMTF
	module BUKONG
		module FindTools
			class Find < Tools::Base
				STS_CLASS_NAME = OB[:find_by_id].freeze
				def activate
					@@active_tool_class = self.class
					if !@dialog.nil? && @dialog.visible?
						@dialog.bring_to_front
					else
						@dialog = create_dialog
						@dialog.show
					end
					@dialog.on('find_by_id') do |deferred, find_data|
						begin
							@find = AmtfFindID.new if @find.nil?
							@find.set_data(find_data)
							@find.execute
							deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
				end
				def deactivate(_view)
					@@active_tool_class = nil
					@find.show_all if @find && !@find.keep_hiden
					@dialog.close if @dialog
				end
				def create_dialog(url = nil)
					options = {
						dialog_title: 'BuKong',
						preferences_key: 'bukongcabinet.net',
						style: UI::HtmlDialog::STYLE_DIALOG,
						resizable: true,
						scrollable: true,
						width: 320,
						height: 700,
						right: 0,
						top: 0
					}
					AMTF_Dialog.dialog.close if !AMTF_Dialog.dialog.nil?
					url = "#{ServerOptions.get_server_url}/find_by_id"
					@dialog = UI::HtmlDialog.new(options)
					@dialog.center
					@dialog.set_url(url)
					Bridge.decorate(@dialog)
					@dialog.on('set_save_credential') do |deferred, credential|
						begin
							Sketchup.write_default(
								'Amtflogic',
								'configuration',
								credential
							)
							deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.on('set_window_size') do |deferred, width, height|
						begin
							@dialog.set_size(width.to_i, height.to_i)
							deferred.resolve(true)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.on('load_config') do |deferred|
						begin
							saver = AmtfSaver.new
							config = saver.open_config
							credential =
								Sketchup.read_default(
									'Amtflogic',
									'configuration',
									''
								)
							deferred.resolve(
								{
									language: OB.saved_lang,
									config: config,
									version: EXTENSION.version,
									unit: self.get_model_units,
									su_filename: Sketchup.active_model.title,
									cre: credential,
									su_authorized: true
								}
							)
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.show
					@dialog
				end
				def get_model_units()
					opts = Sketchup.active_model.options['UnitsOptions']
					unit_format = opts['LengthFormat']
					if unit_format != Length::Decimal
						if unit_format == Length::Fractional ||
								unit_format == Length::Architectural
							'INCHES' # Architectural or Fractional
						elsif unit_format == Length::Engineering
							'FEET' # Engineering
						else
							'DEFAULT'
						end
					else
						# Decimal ( unit_format == 0 )
						case opts['LengthUnit']
						when Length::Inches
							'INCHES'
						when Length::Feet
							'FEET'
						when Length::Centimeter
							'CENTIMETERS'
						when Length::Millimeter
							'MILLIMETERS'
						when Length::Meter
							'METERS'
						else
							'DEFAULT'
						end
					end
				end
			end # MinifixTool
		end
	end
end