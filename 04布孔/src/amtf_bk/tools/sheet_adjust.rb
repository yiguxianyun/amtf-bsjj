# encoding: UTF-8
module AMTF
	module BUKONG
		module AdjustModule
			class SheetAdjust < Tools::Base
				STS_CLASS_NAME = OB[:sheet_adjust].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					reset_tool
					if AMTF_STORE.init_open_json == false
						saver = AmtfSaver.new
						saver.load_comp
						AMTF_STORE.init_open_json = true
					end
				end
				def deactivate(view)
					@@active_tool_class = nil
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def num_decode(key)
					case key
					when 48, 96
						return '0'
					when 49, 97
						return '1'
					when 50, 98
						return '2'
					when 51, 99
						return '3'
					when 52, 100
						return '4'
					when 53, 101
						return '5'
					when 54, 102
						return '6'
					when 55, 103
						return '7'
					when 56, 104
						return '8'
					when 57, 105
						return '9'
					when 190, 110
						return '.'
					else
						return ''
					end
				end
				def onKeyUp(key, repeat, flags, view)
					if key == 13
						distance = @my_vcb.to_l
						if distance != 0
							if @doing_task == 'SHEET'
								if @boxHelper && !@move_data.empty? &&
										!@sheet_data.empty?
									return unless @frame
									return unless @boxHelper.selected_point
									return unless @pick_data
									axis = 'Z_AXIS'
									axis = 'X_AXIS' if @pick_data[
										:sheet_type
									] == 'VER_DIV'
									@move_data =
										@boxHelper.move(
											view,
											@moving_point,
											axis,
											distance
										)
									sheet_shift
									@is_moving = false
									view.lock_inference
								end
							elsif @doing_task == 'SUB_FRAME'
								if @boxHelper && @is_moving
									return unless @frame
									return unless @boxHelper.selected_point
									get_resize_data(view, distance)
									sub_frame_adjust
									@is_moving = false
									view.lock_inference
								end
							end
						end
						@my_vcb = ''
					end
					@my_vcb += num_decode(key)
					view.invalidate
					return true
				end
				def enableVCB?
					return true
				end
				def clear_my_vcb
					@my_vcb = ''
				end
				def resume(view)
					view.invalidate
				end
				def suspend(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.invalidate
				end
				def get_resize_data(view, distance = nil)
					vertical = @grand_parent['split'] == 'vertical_divider'
					if @boxHelper.selected_point[:snap] == 'rightPoint' &&
							vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'leftPoint' &&
							vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'bottomPoint' &&
							!vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'topPoint' &&
							!vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner0' &&
							vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner0' &&
							!vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner1' &&
							vertical
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								true,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner1' &&
							!vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner2' &&
							vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner2' &&
							!vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner3' &&
							vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner3' &&
							!vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner4' &&
							vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner4' &&
							!vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner5' &&
							vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner5' &&
							!vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner6' &&
							vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner6' &&
							!vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner7' &&
							vertical
						@correct_index = false
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					elsif @boxHelper.selected_point[:snap] == 'Conner7' &&
							!vertical
						@correct_index = true
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								@correct_index,
								distance
							)
					end
				end
				def onMouseMove(flags, x, y, view)
					@mouse_ip.pick(view, x, y)
					@moving_point = @mouse_ip.position
					@move_data = {}
					return unless @boxHelper && @boxHelper.selected_point
					if @doing_task == 'SHEET'
						axis = 'Z_AXIS'
						axis = 'X_AXIS' if @pick_data[:sheet_type] == 'VER_DIV'
						@move_data = @boxHelper.move(view, @moving_point, axis)
					elsif @doing_task == 'SUB_FRAME'
						return unless @boxHelper && @boxHelper.selected_point
						get_resize_data(view)
					end
					view.tooltip = @mouse_ip.tooltip if @mouse_ip.valid?
					view.invalidate
				end
				def doSheetTask
					return if @pick_data.nil? || @pick_data.empty?
					sheet_id = @pick_data[:sheet_id]
					@frame = @pick_data[:frame]
					sheet_type = @pick_data[:sheet_type]
					trans_path = @pick_data[:path]
					sub_index = @pick_data[:sub_index]
					return unless @frame && sheet_id
					@sheet_data = {}
					sheet_trans = IDENTITY
					@frame.traverseCallback do |node|
						if node['root'] && node['root']['sheets']
							node['root']['sheets'].each do |k, sheet_arr|
								sheet_arr.each do |obj|
									if obj['root']['id'] == sheet_id
										sheet_trans =
											Geom::Transformation.new(
												obj['root']['transform']
											)
										@sheet_data = {
											sheet: obj,
											parent: node['root']
										}
									end
								end
							end
						end
					end
					return if @sheet_data.empty?
					matrix = IDENTITY
					for i in (sub_index - 1).downto(0)
						if trans_path[i].is_a?(Sketchup::Group)
							matrix = matrix * trans_path[i].transformation
						end
					end
					sheet = @sheet_data[:sheet]
					vertices = sheet['root']['boundingBox']['vertices']
					@boxHelper = AmtfBox.new(vertices, matrix)
				end
				def doSubFrameTask
					return if @pick_data.nil? || @pick_data.empty?
					@frame = @pick_data[:frame]
					trans_path = @pick_data[:path]
					section_id = @pick_data[:section_id]
					sub_index = @pick_data[:sub_index]
					sub_grp = @pick_data[:sub_grp]
					return unless @frame && section_id
					@section_data = {}
					@grand_parent = {}
					tree_path = @frame.findPath(section_id)
					if !tree_path.empty?
						@section_data = tree_path[tree_path.size - 1]
						@grand_parent = tree_path[tree_path.size - 2]
					end
					return if @section_data.empty? || @grand_parent.empty?
					if !@grand_parent['split']
						reset_tool
						return
					end
					matrix = IDENTITY
					vertices = boxCorners(@section_data)
					for i in (sub_index - 1).downto(0)
						if trans_path[i].is_a?(Sketchup::Group)
							matrix = matrix * trans_path[i].transformation
						end
					end
					@boxHelper = AmtfBox.new(vertices, matrix, true)
				end
				def getExtents
					bb = Geom::BoundingBox.new
					bb.add(@mouse_ip.position)
					bb
				end
				def draw(view)
					@boxHelper.draw(view) if @boxHelper
					@mouse_ip.draw(view) if @mouse_ip.display?
					view.draw_points(@test_point, 40, 1, 'green') if @test_point
				end
				def onLButtonDown(flags, x, y, view)
					selection = Sketchup.active_model.selection
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pick_point = @mouse_ip.position
					if !@frame
						@pick_data = pick_frame_data_1
						if @pick_data && @pick_data[:found_sheet] == true
							@doing_task = 'SHEET'
							doSheetTask
						elsif @pick_data && @pick_data[:found_sub_group] == true
							@doing_task = 'SUB_FRAME'
							doSubFrameTask
						end
						@is_moving = false
					else
						if @doing_task == 'SHEET'
							if @boxHelper && @move_data.empty?
								@boxHelper.pick_scale_point(view, pick_point)
								@is_moving = true
							elsif @boxHelper && !@move_data.empty? &&
									!@sheet_data.empty?
								sheet_shift
								@is_moving = false
								view.lock_inference
							end
						elsif @doing_task == 'SUB_FRAME'
							if @boxHelper && !@is_moving
								@boxHelper.pick_scale_point(view, pick_point)
								@is_moving = true
							elsif @boxHelper && @is_moving
								sub_frame_adjust
								@is_moving = false
								@is_moving = false
								view.lock_inference
							end
						end
					end
					view.invalidate
				end
				def reset_tool
					@my_vcb = ''
					@pick_data = nil
					@boxHelper = nil
					@sheet_data = {}
					@move_data = {}
					@frame = nil
					@section_data = {}
					@grand_parent = {}
					@doing_task = nil
					@resize_data = {}
				end
				def sub_frame_adjust()
					return unless @resize_data[:distance]
					root_data = {
						unit: AMTF_Dialog.get_model_units,
						distance: @resize_data[:distance].to_f,
						frame_width: @resize_data[:frame_width].to_l.to_f,
						frame_height: @resize_data[:frame_height].to_l.to_f,
						frame_depth: @resize_data[:frame_depth].to_l.to_f,
						factor: @resize_data[:distance_factor],
						section_data: @section_data,
						grand_parent: @grand_parent,
						correct_index: @correct_index,
						root: @frame.root,
						frame_ID: @frame.id
					}
					js_command = "sub_frame_adjust('#{root_data.to_json}');"
					AMTF_Dialog.dialog.execute_script(js_command)
					reset_tool
				end
				def sheet_shift()
					root_data = {
						unit: AMTF_Dialog.get_model_units,
						distance: @move_data[:distance].to_f,
						sheet: @sheet_data[:sheet],
						parent_section: @sheet_data[:parent],
						root: @frame.root,
						factor: @move_data[:factor],
						frame_ID: @frame.id
					}
					js_command = "sheet_shift('#{root_data.to_json}');"
					AMTF_Dialog.dialog.execute_script(js_command)
					reset_tool
				end
				def boxCorners(section_root)
					org =
						Geom::Point3d.new Utilities.unitConvert(
								section_root['origin']['x']
						                  ),
						                  Utilities.unitConvert(
								section_root['origin']['y']
						                  ),
						                  Utilities.unitConvert(
								section_root['origin']['z']
						                  )
					w = Utilities.unitConvert(section_root['width2d'])
					h = Utilities.unitConvert(section_root['height2d'])
					d = Utilities.unitConvert(section_root['depth'])
					box = []
					box[0] = Geom::Point3d.new org.x, org.y + d, org.z
					box[1] = Geom::Point3d.new org.x + w, org.y + d, org.z
					box[2] = Geom::Point3d.new org.x + w, org.y, org.z
					box[3] = Geom::Point3d.new org.x, org.y, org.z
					box[4] = Geom::Point3d.new org.x, org.y + d, org.z + h
					box[5] = Geom::Point3d.new org.x + w, org.y + d, org.z + h
					box[6] = Geom::Point3d.new org.x + w, org.y, org.z + h
					box[7] = Geom::Point3d.new org.x, org.y, org.z + h
					return box
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #