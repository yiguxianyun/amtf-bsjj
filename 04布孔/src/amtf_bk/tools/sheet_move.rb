# encoding: UTF-8
module AMTF
	module BUKONG
		module SheetMoveModule
			LOCK_X = 39
			LOCK_Y = 37
			LOCK_Z = 38
			class SheetMove < Tools::Base
				STS_CLASS_NAME = OB[:move_sheet].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					reset_tool
					@resize_direction = nil
					if AMTF_STORE.init_open_json == false
						saver = AmtfSaver.new
						saver.load_comp
						AMTF_STORE.init_open_json = true
					end
				end
				def deactivate(view)
					@@active_tool_class = nil
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def num_decode(key)
					case key
					when 48, 96
						return '0'
					when 49, 97
						return '1'
					when 50, 98
						return '2'
					when 51, 99
						return '3'
					when 52, 100
						return '4'
					when 53, 101
						return '5'
					when 54, 102
						return '6'
					when 55, 103
						return '7'
					when 56, 104
						return '8'
					when 57, 105
						return '9'
					when 190, 110
						return '.'
					else
						return ''
					end
				end
				def onKeyUp(key, repeat, flags, view)
					if key == 13
						distance = @my_vcb.to_s.to_l
						if distance != 0 && @is_moving == true && @boxHelper
							@move_data =
								@boxHelper.free_move(
									view,
									@moving_point,
									@resize_direction,
									distance
								)
							move_sheet
							@is_moving = false
							view.lock_inference
						end
						@my_vcb = ''
					elsif key == 39 || key == 37 || key == 38
						if key == 39
							@resize_direction =
								@resize_direction == LOCK_X ? nil : LOCK_X
							@colorLock = 'red'
						elsif key == 37
							@resize_direction =
								@resize_direction == LOCK_Y ? nil : LOCK_Y
							@colorLock = 'green'
						elsif key == 38
							@resize_direction =
								@resize_direction == LOCK_Z ? nil : LOCK_Z
							@colorLock = 'blue'
						end
						# cancel_tool()
						view.lock_inference
					end
					@my_vcb += num_decode(key)
					view.invalidate
					return true
				end
				def enableVCB?
					return true
				end
				def clear_my_vcb
					@my_vcb = ''
				end
				def resume(view)
					view.invalidate
				end
				def suspend(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.invalidate
				end
				def onMouseMove(flags, x, y, view)
					return unless @is_moving
					@mouse_ip.pick(view, x, y)
					@moving_point = @mouse_ip.position
					@move_data = {}
					return unless @boxHelper && @boxHelper.selected_point
					@move_data =
						@boxHelper.free_move(
							view,
							@moving_point,
							@resize_direction
						)
					view.tooltip = @mouse_ip.tooltip if @mouse_ip.valid?
					view.invalidate
				end
				def doSheetTask
					return if @pick_data.nil? || @pick_data.empty?
					sheet_id = @pick_data[:sheet_id]
					@frame = @pick_data[:frame]
					sheet_type = @pick_data[:sheet_type]
					trans_path = @pick_data[:path]
					sub_index = @pick_data[:sub_index]
					return unless @frame && sheet_id
					@sheet_data = {}
					sheet_trans = IDENTITY
					@frame.traverseCallback do |node|
						if node['root'] && node['root']['sheets']
							node['root']['sheets'].each do |k, sheet_arr|
								sheet_arr.each do |obj|
									if obj['root']['id'] == sheet_id
										sheet_trans =
											Geom::Transformation.new(
												obj['root']['transform']
											)
										@sheet_data = {
											sheet: obj,
											parent: node['root']
										}
									end
								end
							end
						end
					end
					return if @sheet_data.empty?
					matrix = IDENTITY
					for i in (sub_index - 1).downto(0)
						if trans_path[i].is_a?(Sketchup::Group)
							matrix = matrix * trans_path[i].transformation
						end
					end
					matrix = matrix * sheet_trans
					sheet = @sheet_data[:sheet]
					vertices = sheet['root']['boundingBox']['vertices']
					@boxHelper = AmtfBox.new(vertices, matrix)
				end
				def getExtents
					bb = Geom::BoundingBox.new
					bb.add(@mouse_ip.position)
					bb
				end
				def draw(view)
					@boxHelper.draw(view) if @boxHelper
					@mouse_ip.draw(view) if @mouse_ip.display?
				end
				def onLButtonDown(flags, x, y, view)
					selection = Sketchup.active_model.selection
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pick_point = @mouse_ip.position
					if !@frame
						@pick_data = pick_frame_data_2
						if @pick_data && @pick_data[:found_sheet] == true
							doSheetTask
						end
						@is_moving = false
					else
						if @boxHelper && @move_data.empty?
							@boxHelper.pick_scale_point(view, pick_point)
							@is_moving = true
						elsif @is_moving == true && @boxHelper &&
								!@move_data.empty?
							move_sheet
							@is_moving = false
							view.lock_inference
						end
					end
					view.invalidate
				end
				def cancel_tool
					@move_data = nil
					@is_moving = false
					@boxHelper = nil
					doSheetTask
				end
				def reset_tool
					@my_vcb = ''
					@pick_data = nil
					@boxHelper = nil
					@sheet_data = {}
					@frame = nil
					@section_data = {}
					@grand_parent = {}
					@doing_task = nil
					@move_data = {}
				end
				def move_sheet()
					return unless @move_data
					return unless @frame
					trans = @move_data[:trans]
					sheet_id = @pick_data[:sheet_id]
					matrix = IDENTITY
					mod_root =
						@frame.traverseCallback do |node|
							if node['root'] && node['root']['sheets']
								node['root']['sheets'].each do |k, sheet_arr|
									sheet_arr.each do |obj|
										if obj['root']['id'] == sheet_id
											tr =
												Geom::Transformation.new(
													obj['root']['transform']
												)
											matrix = tr * trans
											obj['root']['transform'] =
												matrix.to_a
										end
									end
								end
							end
						end
					root_data = { root: mod_root['root'], frame_ID: @frame.id }
					js_command = "update_frame_root('#{root_data.to_json}');"
					AMTF_Dialog.dialog.execute_script(js_command)
					reset_tool
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #