# encoding: UTF-8
module AMTF
  module BUKONG
  module V_Module
    class V_split < Tools::Base
      STS_CLASS_NAME   = OB[:v_split].freeze  
      def activate   
        @@active_tool_class = self.class               
        @input  = Sketchup::InputPoint.new
        @face_vertices = []
        Sketchup.active_model.selection.clear
        Sketchup.status_text = OB[:select_target_object] 
        AMTF_STORE.show_all_sections()    
        AMTF_Dialog.dialog.on("sheet_split_update_frame"){
          | deferred, attrs, frame_ID |   
          begin       
            amtfFrame = AMTF_STORE.frame_premitives[ frame_ID ]
            Sketchup.active_model.start_operation(OB[:v_split], true)
            amtfFrame.update( attrs ) if amtfFrame
            AMTF_STORE.show_all_sections()
            Sketchup.active_model.commit_operation
            deferred.resolve(true)  
          rescue => error
            Utilities.print_exception(error, false)
            Sketchup.set_status_text(error)
            deferred.resolve(false)                         
          end 
        }
      end
      def reset_tool           
        Sketchup.active_model.selection.clear
      end
      def deactivate(_view)
        @@active_tool_class = nil
        AMTF_STORE.hide_all_sections() 
      end  
      def refresh
      end
      def onMouseMove( flags, x, y, view )
        @ph.do_pick(x, y)
        @input.pick(view, x, y)
        @face_vertices = []
        pickpaths = {}
        @section_face = nil
        @frame = nil
        # Iterate all pick-routes:
        for index in 0..@ph.count-1
            pickpath = @ph.path_at(index)
            pickpaths[index]= pickpath
        end
        if !pickpaths.empty?
            pickpaths.each{
                |index, value|
                if value.last.is_a?(Sketchup::Face) && value.first.is_a?(Sketchup::Group)
                    pick_face = value.last  
                    section_id = pick_face.get_attribute('amtf_dict','section_id')
                    next unless section_id
                    next unless value.first.is_a?(Sketchup::Group)   
                    next unless value.first.get_attribute('amtf_dict','comp_type') == "AmtfFrame"                                    
                    comp_id = value.first.persistent_id
                    trans_path = pickpaths[index]                                  
                    matrix = IDENTITY
                    for i in (trans_path.size - 1).downto(0)                                    
                        if trans_path[i].is_a?(Sketchup::Group)
                            matrix = matrix*trans_path[i].transformation
                        end
                    end
                    @face_vertices = pick_face.outer_loop.vertices
                    @face_vertices.map!{
                        |f| 
                        f = f.position.transform!(matrix)
                    }
                    @section_face = pick_face
                    @frame = AMTF_STORE.frame_premitives[ comp_id ] 
                    @trans_path = trans_path  
                    @section_face = pick_face
                end                          
            } 
        end
        view.tooltip = @input.tooltip if @input.valid?
         view.refresh
        view.invalidate
    end
      def draw(view)
          if @face_vertices.size > 0
              view.line_width = 4  
              view.drawing_color = "yellow"             
              view.draw(GL_LINE_LOOP, @face_vertices)
          end         
      end
      def onLButtonUp( flags, x, y, view )
        selection = Sketchup.active_model.selection       
        if @frame.nil? || @section_face.nil?
           reset
           return      
        end        
        if !@section_face.nil? and !@frame.nil?
          @divide_equation = ''
          @div_thick = '' 
          while  @divide_equation == '' || @div_thick == ''   
            prompts = [OB[:divide_equation], OB[:div_thick], OB[:right_to_left]]
            defaults = ["/2", FUJI_GLOBAL_VARIABLE[:div_thick].to_f, "No"]
            list = ["", "", "Yes|No"]
            inputs = UI.inputbox(prompts, defaults, list, OB[:divide_equation])
            return if !inputs            
            @divide_equation = inputs[0]
            @div_thick = inputs[1]
            @right_to_left = inputs[2]
            FUJI_GLOBAL_VARIABLE[:div_thick] = @div_thick
          end
          root_data = {
                :frame_ID => @frame.id,
                :root => @frame.root,
                :section_id => @section_face.get_attribute('amtf_dict','section_id'),
                :split => 'vertical_divider',
                :div => @divide_equation,
                :right_to_left => @right_to_left,
                :div_thick => @div_thick,
              }
          AMTF_Dialog.dialog.execute_script("sheet_split_command(#{root_data.to_json});")
        end      
         view.refresh     
        view.invalidate
        true
      end
  end#Class
  end#  OffsetToolModule
  end # BuKong
end # 