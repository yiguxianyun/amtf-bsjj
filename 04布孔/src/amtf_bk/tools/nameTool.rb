# encoding: UTF-8
module AMTF
	module BUKONG
		module NameTool
			class GiveNameTool < Tools::Base
				STS_CLASS_NAME = OB[:name_tool].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					@hold_shift_key = false
					@sheet_list = []
					@dialog.close if !@dialog.nil?
					@dialog = create_dialog
					@dialog.add_action_callback(
						'save_name'
					) do |action_context, name|
						begin
							if @sheet_list.size > 0 && name
								Sketchup.active_model.start_operation(
									OB[:name_tool],
									true
								)
								hash = {}
								@sheet_list.each do |sheet|
									frame = sheet[:frame]
									next if frame.nil?
									sheet_id = sheet[:sheet_id]
									if hash.key?(frame)
										hash[frame] << sheet_id
									else
										hash[frame] = [sheet_id]
									end
								end
								hash.each do |frame, list|
									attrs = frame.attrs
									list.each do |sheet_id|
										node_queue = [attrs]
										until node_queue.empty?
											node = node_queue.shift
											if node['root'] &&
													node['root']['sheets']
												node['root']['sheets']
													.each do |k, sheet_arr|
													sheet_arr.each do |obj|
														if obj['root']['id'] ==
																sheet_id
															obj['root'][
																'sheet_name'
															] =
																name
														end
													end
												end
											end
											if node['root'] &&
													node['root']['sections']
												node_queue =
													node_queue.concat(
														node['root']['sections']
													)
											end
											if node['root'] &&
													node['root']['sub_part']
												node_queue =
													node_queue.concat(
														node['root']['sub_part']
													)
											end
										end
									end
									saver = AmtfSaver.new
									saver.save_comp_json(frame.frame, attrs)
								end
								@sheet_list.each do |sheet|
									pick_component = sheet[:pick_component]
									trans = sheet[:trans]
									parent_tr = sheet[:parent_tr]
									pickFace = sheet[:pick_face]
									pick_component.name = name
									part =
										AmtfSheet.new pick_component,
										              trans,
										              parent_tr
									subGroup =
										Utilities
											.definition(part.comp)
											.entities
											.grep(Sketchup::Group)
									if subGroup.size > 0
										for i in 0..subGroup.size - 1
											next unless subGroup[i]
											next if subGroup[i].nil?
											next unless subGroup[i].valid?
											group_type =
												subGroup[i].get_attribute(
													'amtf_dict',
													'type'
												)
											if group_type == 'bukong_label'
												subGroup[i].erase!
												break
											end
										end
									end
									part.drawArrow(pickFace)
								end
								@sheet_list = []
								Sketchup.active_model.commit_operation
							end
						rescue => error
							Utilities.print_exception(error, false)
							Sketchup.set_status_text(error)
							deferred.resolve(false)
						end
					end
					@dialog.show
				end
				def deactivate(view)
					@@active_tool_class = nil
					@sheet_list = []
					@dialog.close
					view.lock_inference
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					return if @dialog.nil?
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					pick_component = pick_data[:pick_component]
					pickFace = pick_data[:pick_face]
					trans = pick_data[:trans]
					parent_tr = pick_data[:parent_matrix]
					sheet_id =
						pick_component.get_attribute('amtf_dict', 'sheet_id')
					frame_data = pick_frame_data
					return if pick_component.nil?
					@dialog.execute_script("add_name('#{pick_component.name}')")
					selection = Sketchup.active_model.selection
					root_data = {
						frame: frame_data[:frame],
						sheet_id: sheet_id,
						pick_component: pick_component,
						pick_face: pickFace,
						trans: trans,
						parent_tr: parent_tr
					}
					if @hold_shift_key == true
						@sheet_list << root_data
						selection.add(pick_component)
					else
						@sheet_list = [root_data]
						selection.clear
						selection.add(pick_component)
					end
				end
				def onKeyDown(key, repeat, flags, view)
					@hold_shift_key = true if key == VK_SHIFT
				end
				def onKeyUp(key, repeat, flags, view)
					@hold_shift_key = false if key == VK_SHIFT
				end
				def create_dialog()
					options = {
						dialog_title: OB[:name_tool],
						preferences_key: 'bukongcabinet.net',
						style: UI::HtmlDialog::STYLE_DIALOG,
						# Set a fixed size now that we know the content size.
						resizable: false,
						scrollable: false,
						width: 500,
						height: 150
					}
					dialog = UI::HtmlDialog.new(options)
					dialog.set_size(options[:width], options[:height]) # Ensure size is set.
					dialog.set_file(PLUGIN_DIR + '/ui/name.html')
					dialog.center
					dialog
				end
			end # NameTool
		end
	end
end