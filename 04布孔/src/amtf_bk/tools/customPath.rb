# encoding: UTF-8
module AMTF
	module BUKONG
		module CustomPathTool
			class CustomPath < Tools::Base
				STS_CLASS_NAME = OB[:custom_path].freeze
				def do_groove(pick_component, groove, pick_data)
					begin
						saver = AmtfSaver.new
						config = saver.open_config
						unless config
							Sketchup.status_text "There isn't config data!"
							return
						end
						config = JSON.parse(config)
						groove_config = config['groove']
						unless groove_config
							Sketchup.status_text "There isn't config data!"
							return
						end
						mapping = {}
						grv_list = ''
						default = ''
						groove_config.each_with_index do |item, idx|
							title = ''
							map_idx = "#{idx + 1}-idx"
							mapping[map_idx] = {
								groove_type_id: item['id'],
								config: item
							}
							item['params1'].each do |para|
								if para['key'] == 'name'
									grv_list += "#{idx + 1}-#{para['value']}"
									grv_list += '|' if idx <
										groove_config.size - 1
									mapping[map_idx][:groove_name] =
										para['value']
								end
								if para['key'] == 'groove_depth'
									mapping[map_idx][:groove_depth] =
										para['value']
								end
								if para['key'] == 'extra_grooves'
									mapping[map_idx][:extra_grooves] =
										para['value']
								end
								if para['key'] == 'color'
									mapping[map_idx][:color] = para['value']
								end
							end
						end
						groove_name = ''
						correct = false
						while !correct
							prompts = [OB[:select_name]]
							defaults = ["1-#{mapping['1-idx'][:groove_name]}"]
							list = [grv_list]
							inputs =
								UI.inputbox(
									prompts,
									defaults,
									list,
									OB[:custom_groove]
								)
							if !inputs
								Sketchup.active_model.tools.pop_tool
								return
							end
							groove_name = inputs[0].to_s
							groove_name = inputs[1].to_s if groove_name.empty?
							correct = true if !groove_name.empty?
						end
						index = groove_name.index('-')
						index_map = groove_name.slice(0..index - 1)
						config_val = mapping["#{index_map}-idx"]
						return unless config_val && config_val[:config]
						hash =
							Utilities.get_setting(
								'groove',
								config,
								config_val[:groove_type_id]
							)
						return unless hash
						hash['id'] = config_val[:groove_type_id]
						Sketchup.active_model.start_operation 'custom_groove',
						                                      true
						pick_component.make_unique
						chanel_grp =
							Utilities
								.definition(pick_component)
								.entities
								.add_group
						tr =
							Utilities.transform_context_convert(
								groove.transformation,
								pick_data[:trans]
							)
						inst =
							Utilities
								.definition(chanel_grp)
								.entities
								.add_instance(Utilities.definition(groove), tr)
						inst.explode
						groove.erase!
						pick_component.set_attribute(
							'amtf_dict',
							'has_groove',
							'yes'
						)
						minifixID = Utilities.CreatUniqueid
						chanel_grp.set_attribute 'amtf_dict', 'type', 'chanel'
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_type_id',
						                         config_val[:groove_type_id]
						chanel_grp.set_attribute 'amtf_dict',
						                         'minifixID',
						                         Utilities.CreatUniqueid
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_name',
						                         config_val[:groove_name]
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_depth',
						                         config_val[:groove_depth]
						chanel_grp.set_attribute 'amtf_dict',
						                         'extra_grooves',
						                         config_val[:extra_grooves]
						chanel_grp.set_attribute 'amtf_dict',
						                         'config_data',
						                         hash.to_json
						chanel_grp.material = config_val[:color] if config_val[
							:color
						]
						Sketchup.active_model.commit_operation
					rescue => error
						Utilities.print_exception(error, false)
					end
				end
				def do_point_point(pick_component, groove, pick_data)
					config_data = {}
					while config_data.empty?
						prompts = [
							OB[:name],
							OB[:toolNumber],
							OB[:cut_depth],
							OB[:cut_pass]
						]
						defaults = [
							FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:name],
							FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:toolNumber],
							FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:cut_depth],
							FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:cut_pass]
						]
						inputs =
							UI.inputbox(prompts, defaults, OB[:custom_groove])
						if !inputs
							Sketchup.active_model.tools.pop_tool
							return
						end
						groove_name = inputs[0].to_s
						toolNumber = inputs[1].to_s
						cut_depth = inputs[2].to_s
						cut_pass = inputs[3].to_s
						FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:name] = groove_name
						FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:toolNumber] =
							toolNumber
						FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:cut_depth] = cut_depth
						FUJI_GLOBAL_CUSTOMPATH_VARIABLE[:cut_pass] = cut_pass
						check = true
						check = false if !Utilities.isNumber?(toolNumber)
						check = false if !Utilities.isNumber?(cut_depth)
						check = false if !Utilities.isNumber?(cut_pass)
						check = false if groove_name.empty?
						if check
							config_data = {
								name: groove_name,
								toolNumber: toolNumber,
								cut_depth: cut_depth,
								cut_pass: cut_pass,
								cut_type: '5'
							}
						end
					end
					return if config_data.empty?
					Sketchup.active_model.start_operation(
						OB[:custom_path],
						true
					)
					bound_edges = []
					if groove.is_a?(Sketchup::Group)
						bound_edges = groove.entities.grep(Sketchup::Edge)
					elsif groove.is_a?(Sketchup::Edge)
						bound_edges = [groove]
					end
					multiPath = TTLibrary.getMultiPathPointsHash(bound_edges)
					for i in 0..multiPath.size - 1
						ptr_arr = multiPath[i][:path]
						temp_arr = []
						for j in 0..ptr_arr.size - 1
							transform = IDENTITY
							transform = groove.transformation if groove.is_a?(
								Sketchup::Group
							)
							ptr =
								Utilities.point_context_convert(
									ptr_arr[j][:point],
									transform,
									pick_data[:trans]
								)
							ptr =
								ptr.project_to_plane(
									pick_data[:pick_face].plane
								)
							temp_arr << ptr
						end
						new_group = pick_component.entities.add_group
						new_group.layer = ptr_arr[0][:layer]
						temp_arr =
							temp_arr.map do |pt|
								pt =
									pt.transform(
										new_group.transformation.inverse
									)
							end
						for j in 0..temp_arr.size - 2
							new_group.entities.add_line temp_arr[j],
							                            temp_arr[j + 1]
						end
						new_group.set_attribute(
							'amtf_dict',
							'config_data',
							config_data.to_json
						)
						new_group.set_attribute 'amtf_dict', 'type', 'tool_path'
						new_group.set_attribute 'amtf_dict',
						                        'closed',
						                        multiPath[i][:closed]
						new_group.set_attribute 'amtf_dict',
						                        'minifixID',
						                        Utilities.CreatUniqueid
						new_layer =
							Sketchup.active_model.layers.add(
								'Amtf_miscellaneous'
							)
						new_group.layer = new_layer
					end
					groove.erase!
					Sketchup.active_model.commit_operation
				end
				def onLButtonDown(flags, x, y, view)
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					pick_component = pick_data[:pick_component]
					return unless pick_component
					selection = Sketchup.active_model.selection
					return unless selection.size > 0
					groove = selection[0]
					unless groove.is_a?(Sketchup::Group) ||
							groove.is_a?(Sketchup::Edge)
						return
					end
					faces = []
					if groove.is_a?(Sketchup::Group)
						faces = groove.entities.grep(Sketchup::Face)
						found_grp = false
						groove.entities.each do |ent|
							if ent.is_a?(Sketchup::Group)
								found_grp = true
								break
							end
						end
						if found_grp
							UI.messagebox OB[:found_sub_group], MB_OK
							return
						end
					end
					if faces.size == 0
						do_point_point(pick_component, groove, pick_data)
					elsif faces.size == 1
						do_groove(pick_component, groove, pick_data)
					else
						UI.messagebox OB[:invalid_object], MB_OK
						return
					end
				end
			end #Class
		end #  CustomPathTool
	end # BuKong
end #