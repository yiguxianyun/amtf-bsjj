# encoding: UTF-8
module AMTF
	module BUKONG
		module OffsetToolModule
			class OffsetTool < Tools::Base
				STS_CLASS_NAME = OB[:offset_tool].freeze
				def activate
					@@active_tool_class = self.class
					@mouse_ip = Sketchup::InputPoint.new
					@ph = Sketchup.active_model.active_view.pick_helper
					url = "#{ServerOptions.get_server_url}/nesting"
					if AMTF_Dialog.dialog.visible?
						AMTF_Dialog.dialog.bring_to_front
						AMTF_Dialog.dialog.set_url(url)
					else
						AMTF_Dialog.create_dialog
						AMTF_Dialog.dialog.set_url(url)
						AMTF_Dialog.dialog.show
					end
				end
				def reset_tool
					@selected_id = []
					Sketchup.active_model.selection.clear
				end
				def deactivate(_view)
					@@active_tool_class = nil
				end
				def onKeyDown(key, repeat, flags, view); end
				def onLButtonDown(flags, x, y, view)
					@start = Geom::Point3d.new(x, y, 0)
				end
				def onLButtonUp(flags, x, y, view)
					reset
					view.invalidate
					true
				end
				def onMouseMove(flags, x, y, view)
					@end = Geom::Point3d.new(x, y, 0)
					view.refresh
					view.invalidate
				end
				def reset
					@start = nil
					@end = nil
				end
				def valid?
					@start && @end
				end
				def draw(view); end
				def onSetCursor
					UI.set_cursor(TTLibrary.get_cursor[:select])
				end
				def entities_hash
					# tr�?v�?3 hash
					# 1. comp_id là key, value: mảng các sheet có cùng ID: s�?dụng cho việc chọn đ�?offset, xóa, sửa
					#  {:comp_list => [(sheet)group], :path_list => [minifixID]} bao gồm c�?các loop có ID
					# 2. minifixID (hoặc loop ID) là key, value là:  {:path_json => , :instance => }
					# 3. model_path_hash: hash (thông tin các json) key là path_id, value: tên các path
					# tạo nên các entities hash này khi khởi tạo
					# Kịch bản: khi click vào tấm: tạo ra d�?liệu của nó đ�?sinh giao diện:
					# tra hash 1=> ra được nó có các đường dao nào
					#  t�?đường dao tra vào hash 2, tra được đường dao đó tên gì. ID gì
					# Luồng đi của hành động:
					# Click vào tấm => tìm đường nào là đường lựa chọn, tạo d�?liệu như trên,
					# kèm theo danh sách ID của đường được chọn (MinifixID),
					# gửi sang đ�?sinh giao diện
					# Khi click bên giao diện: xác định được minifixID, gửi v�?đ�?thêm vào selection
					# click chuột ch�?đ�?chọn và b�?chọn
					comp_id_hash = Hash.new
					path_hash = Hash.new
					model_path_hash = Hash.new
					parent_list = Hash.new
					entities = Sketchup.active_model.entities
					entities.each do |entity|
						next unless entity.is_a?(Sketchup::Group)
						explode =
							Utilities.get_comp_attribute(
								entity,
								'amtflogic_exploded',
								'amtf_dict'
							)
						if explode != 'true' || Utilities.hasParent?(entity)
							UI.messagebox OB[:explode_required], MB_OK
							return
						else
							next unless entity.valid?
							# next unless Utilities.solid?(entity)
							entity.make_unique
							comp_id =
								entity.get_attribute('amtf_dict', 'pathcomp_id')
							next if comp_id.nil?
							miniID_List = []
							big_faces = Utilities.get_big_faces(entity)
							big_faces.each do |f|
								loops = f.loops
								loops.each do |lo|
									if !parent_list.has_key?(lo)
										parent_list[lo] = entity
									end
									loopID =
										lo.get_attribute('amtf_dict', 'loop_id')
									next if loopID.nil?
									miniID_List << loopID
									if !path_hash.has_key?(loopID)
										path_hash[loopID] = [lo]
									else
										path_hash[loopID] << lo
									end
									path_json =
										lo.get_attribute(
											'amtf_dict',
											'path_json'
										)
									unless path_json.nil?
										path_json = JSON.parse(path_json)
										path_json.each do |pth|
											if !model_path_hash.has_key?(
													pth['path_id']
											   )
												model_path_hash[
													pth['path_id']
												] =
													pth['path_name']
											end
										end
									end
								end
							end
							ents = entity.entities
							ents.each do |obj|
								next unless obj.is_a?(Sketchup::Group)
								if !parent_list.has_key?(obj)
									parent_list[obj] = entity
								end
								sub_grp_type =
									obj.get_attribute('amtf_dict', 'type')
								if sub_grp_type == 'tool_path' ||
										(sub_grp_type == 'curve_groove') ||
										sub_grp_type == 'pocket_path'
									minifixID =
										obj.get_attribute(
											'amtf_dict',
											'minifixID'
										)
									next if minifixID.nil?
									miniID_List << minifixID
									if !path_hash.has_key?(minifixID)
										path_hash[minifixID] = [obj]
									else
										path_hash[minifixID] << obj
									end
									path_json =
										obj.get_attribute(
											'amtf_dict',
											'path_json'
										)
									unless path_json.nil?
										path_json = JSON.parse(path_json)
										path_json.each do |pth|
											if !model_path_hash.has_key?(
													pth['path_id']
											   )
												model_path_hash[
													pth['path_id']
												] =
													pth['path_name']
											end
										end
									end
								end
							end
							if !comp_id_hash.has_key?(comp_id)
								comp_id_hash[comp_id] = {
									comp_list: [entity],
									miniID_List: miniID_List
								}
							else
								comp_id_hash[comp_id][:comp_list] << entity
								comp_id_hash[comp_id][:miniID_List].push(
									*miniID_List
								)
								comp_id_hash[comp_id][:miniID_List].uniq!
							end
						end
					end
					return(
						{
							comp_id_hash: comp_id_hash,
							path_hash: path_hash,
							model_path_hash: model_path_hash,
							parent_list: parent_list
						}
					)
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #
# Lưu đ�?khi làm offset cho 2 đường
# dialog.on("pocket"){
#   |deferred, params|
#   check_list = params["check_list"]
#   begin
#     list_0 = @path_hash[check_list[0]]
#     list_1 = @path_hash[check_list[1]]
#     Sketchup.active_model.start_operation("add pocket", true)
#     visited = []
#     for i in 0..list_0.size - 1
#       for j in 0..list_1.size - 1
#         if @parent_list[list_0[i]] == @parent_list[list_1[j]]  &&
#           !visited.include?(list_0[i]) && !visited.include?(list_1[j])
#           visited << list_0[i]
#           visited << list_1[j]
#           parent_0 = @parent_list[list_0[i]]
#           pocket = parent_0.entities.add_group
#           points = []
#           list = [list_0[i], list_1[j]]
#           list.each{
#             |grp|
#             if grp.is_a?(Sketchup::Loop)
#               bound_edges = grp.edges
#               points_0 = TTLibrary.getPathPoints(bound_edges)
#               points_0 = points_0.map{ |point| Utilities.point_context_convert(point, parent_0.transformation, tr2)}
#               points << points_0
#             elsif grp.is_a?(Sketchup::Group)
#               next unless grp.get_attribute "amtf_dict", "type" == 'tool_path'
#               bound_edges = grp.entities.grep(Sketchup::Edge)
#               next unless bound_edges && bound_edges.size > 0
#               puts bound_edges.size
#               points_0 = TTLibrary.getPathPoints(bound_edges)
#               tr1 = parent_0.transformation*grp.transformation
#               tr2 = parent_0.transformation*pocket.transformation
#               points_0 = points_0.map{ |point| Utilities.point_context_convert(point, tr1, tr2)}
#               points << points_0
#             end
#           }
#           return unless points.size  > 0
#           face0 = pocket.entities.add_face points[0]
#           area0 = face0.area
#           face0.erase!
#           face1 = pocket.entities.add_face  points[1]
#           area1 = face1.area
#           face1.erase!
#           if area0 > area1
#             pocket.entities.add_face(points[0])
#             pocket.entities.add_face(points[1]).erase!
#           else
#             pocket.entities.add_face(points[1])
#             pocket.entities.add_face(points[0]).erase!
#           end
#           pocket.set_attribute 'amtf_dict', 'type', 'pocket_path'
#           pocket.set_attribute 'amtf_dict', 'path_json', params['paths'].to_json
#           pocket.set_attribute 'amtf_dict', 'minifixID',  Utilities.CreatUniqueid()
#         end
#       end
#     end
#     Sketchup.active_model.commit_operation
#     Sketchup.set_status_text(OB[:completed])
#     model_info = entities_hash()
#     @comp_id_hash = model_info[:comp_id_hash]
#     @path_hash = model_info[:path_hash]
#     @model_path_hash = model_info[:model_path_hash]
#     @parent_list = model_info[:parent_list]
#     paths = {
#       :selected => @selected_id,
#       :data => create_path_data(@comp_id),
#       :hidden_list => hiddenList(@comp_id)
#     }
#     js_command = "offset_updateDOM('#{paths.to_json}');"
#     @dialog.execute_script(js_command)
#     deferred.resolve(true)
#   rescue => error
#     Utilities.print_exception(error, false)
#     Sketchup.set_status_text(error)
#     deferred.resolve(false)
#   end