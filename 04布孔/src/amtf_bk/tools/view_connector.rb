# encoding: UTF-8
module AMTF
	module BUKONG
		module ViewConnectorModule
			class ViewConnector < Tools::Base
				STS_CLASS_NAME = OB[:view_minifix].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					@connectors = []
				end
				def deactivate(view)
					@@active_tool_class = nil
					if @connectors.size > 0
						@connectors.each do |con|
							if con && con.modal && con.modal.valid?
								con.modal.erase!
							end
						end
					end
					Sketchup.active_model.rendering_options[
						'ModelTransparency'
					] =
						@old_mode if !@old_mode.nil?
					view.lock_inference
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					mod = Sketchup.active_model
					selection = Sketchup.active_model.selection
					mod.start_operation(OB[:view_minifix], true)
					@old_mode =Sketchup.active_model.rendering_options['ModelTransparency']
					Sketchup.active_model.rendering_options['ModelTransparency'] =true
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pickpaths = {}
					for index in 0..@ph.count - 1
						pickpath = @ph.path_at(index)
						pickpaths[index] = pickpath
					end
					pick_comp = nil
					if !pickpaths.empty?
						pickpaths.each do |index, path|
							if Utilities.instance?(path.first)
								pick_comp = path.first
							end
						end
					end
					return if pick_comp.nil?
					selection.clear
					selection.add(pick_comp)
					sheets = []
					if Utilities.solid?(pick_comp)
						sheets.push(AmtfSheet.new(pick_comp,pick_comp.transformation,IDENTITY))
					else
						query = AmtfQuery.new
						sheets =query.query_sheets(Utilities.definition(pick_comp).entities)
					end
					sheets.each do |sheet|
						conns = sheet.grep_connector
						conns.each do |hash_obj|
							group = hash_obj[:instance]
							center = hash_obj[:hole]
							config_data = hash_obj[:config_data]
							radius = group.get_attribute 'amtf_dict', 'radius'
							radius = radius.to_f.to_l
							type = hash_obj[:type]
							big_faces = sheet.get_big_faces
							face = nil
							if Utilities.within_face?(center, big_faces[0])
								face = big_faces[0]
							elsif Utilities.within_face?(center, big_faces[1])
								face = big_faces[1]
							end
							next if face.nil?
							next if !config_data
							config_data = JSON.parse(config_data)
							height_1 = 0
							move_1 = 0
							case type
							when 'chot_cam'
								height_1 =
									(config_data['distance_l'].to_f + 10).to_s
										.to_l
								move_1 = 10.mm
							when 'oc_cam', 'khoan_moi', 'chot_dot', 'patV',
							     'rafix_d', 'hinge35'
								height_1 =
									(config_data['d_depth'].to_f + 2).to_s.to_l
								move_1 =
									(config_data['d_depth'].to_f + 1).to_s.to_l
							when 'rafix_d1', 'hinge31', 'hinge32', 'hinge41',
							     'hinge42', 'hinge51', 'hinge52', 'hinge61',
							     'hinge62', 'drawer_fitting_1'
								height_1 =
									(config_data['d1_depth'].to_f + 2).to_s.to_l
								move_1 =
									(config_data['d1_depth'].to_f + 1).to_s.to_l
							when 'chot_go2', 'rafix_d2', 'drawer_fitting_2'
								height_1 =
									(config_data['d2_depth'].to_f + 2).to_s.to_l
								move_1 =
									(config_data['d2_depth'].to_f + 1).to_s.to_l
							when 'chot_go3'
								height_1 =
									(config_data['d3_depth'].to_f + 2).to_s.to_l
								move_1 =
									(config_data['d3_depth'].to_f + 1).to_s.to_l
							end
							chot_cam =CircleModel.new(
									sheet.comp,
									Utilities.point_context_convert(
										center,
										sheet.trans,
										IDENTITY
									),
									radius,
									face.normal,
									height_1,
									move_1,
									sheet.trans
								)
							chot_cam.drawModelView
							@connectors << chot_cam
						end
					end
					mod.commit_operation
				end
			end #  Tool
		end
	end
end