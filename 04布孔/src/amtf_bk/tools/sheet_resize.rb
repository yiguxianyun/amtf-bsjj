# encoding: UTF-8
module AMTF
	module BUKONG
		module SheetResizeModule
			LOCK_X = 39
			LOCK_Y = 37
			LOCK_Z = 38
			class SheetResize < Tools::Base
				STS_CLASS_NAME = OB[:resize_sheet].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					reset_tool
					@resize_direction = LOCK_X
					@resize_data = nil
					@resize_smart = {}
					if AMTF_STORE.init_open_json == false
						saver = AmtfSaver.new
						saver.load_comp
						AMTF_STORE.init_open_json = true
					end
					AMTF_Dialog
						.dialog
						.on(
							'sheet_resize_update_frame'
						) do |deferred, params, frame_ID|
							begin
								if params && frame_ID
									frame =
										AMTF_STORE.frame_premitives[
											frame_ID.to_i
										]
									if frame
										Sketchup.active_model.start_operation(
											OB[:resize_sheet],
											true
										)
										frame.update(params)
										Sketchup.active_model.commit_operation
										deferred.resolve(true)
									else
										Sketchup.set_status_text('error')
										deferred.resolve(false)
									end
								end
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
				end
				def deactivate(view)
					@@active_tool_class = nil
					reset_tool
					view.lock_inference
					view.invalidate
				end
				def num_decode(key)
					case key
					when 48, 96
						return '0'
					when 49, 97
						return '1'
					when 50, 98
						return '2'
					when 51, 99
						return '3'
					when 52, 100
						return '4'
					when 53, 101
						return '5'
					when 54, 102
						return '6'
					when 55, 103
						return '7'
					when 56, 104
						return '8'
					when 57, 105
						return '9'
					when 190, 110
						return '.'
					else
						return ''
					end
				end
				def onKeyUp(key, repeat, flags, view)
					if key == 13
						distance = @my_vcb.to_s.to_l
						if distance.to_f != 0
							if @is_moving == true && @boxHelper &&
									!@resize_data.nil?
								if !@resize_smart.empty?
									resize_smart(distance)
								else
									resize_sheet(distance)
								end
								@is_moving = false
								view.lock_inference
							end
						end
						@my_vcb = ''
					elsif key == 39 || key == 37 || key == 38
						if key == 39
							@resize_direction = LOCK_X
							@colorLock = 'red'
						elsif key == 37
							@resize_direction = LOCK_Y
							@colorLock = 'green'
						elsif key == 38
							@resize_direction = LOCK_Z
							@colorLock = 'blue'
						end
						cancel_tool
						view.lock_inference
					end
					@my_vcb += num_decode(key)
					view.invalidate
					return true
				end
				def enableVCB?
					return true
				end
				def clear_my_vcb
					@my_vcb = ''
				end
				def resume(view)
					view.invalidate
				end
				def suspend(view)
					view.invalidate
				end
				def onCancel(reason, view)
					reset_tool
					view.invalidate
				end
				def get_resize_data(view, distance = nil)
					if @boxHelper.selected_point[:snap] == 'rightPoint'
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								true,
								distance
							)
						@correct_index = true
						@is_left_offset = false
						@is_right_offset = true
						@is_top_offset = false
						@is_bottom_offset = false
						@is_front_offset = false
						@is_back_offset = false
						@offset_factor = -@resize_data[:distance_factor]
					elsif @boxHelper.selected_point[:snap] == 'leftPoint'
						@resize_data =
							@boxHelper.resizeX(
								view,
								@moving_point,
								false,
								distance
							)
						@correct_index = false
						@is_left_offset = true
						@is_right_offset = false
						@is_top_offset = false
						@is_bottom_offset = false
						@is_front_offset = false
						@is_back_offset = false
						@offset_factor = @resize_data[:distance_factor]
					elsif @boxHelper.selected_point[:snap] == 'bottomPoint'
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								false,
								distance
							)
						@correct_index = false
						@is_left_offset = false
						@is_right_offset = false
						@is_top_offset = false
						@is_bottom_offset = true
						@is_front_offset = false
						@is_back_offset = false
						@offset_factor = @resize_data[:distance_factor]
					elsif @boxHelper.selected_point[:snap] == 'topPoint'
						@resize_data =
							@boxHelper.resizeZ(
								view,
								@moving_point,
								true,
								distance
							)
						@correct_index = true
						@is_left_offset = false
						@is_right_offset = false
						@is_top_offset = true
						@is_bottom_offset = false
						@is_front_offset = false
						@is_back_offset = false
						@offset_factor = -@resize_data[:distance_factor]
					elsif @boxHelper.selected_point[:snap] == 'frontPoint'
						@resize_data =
							@boxHelper.resizeY(
								view,
								@moving_point,
								true,
								distance
							)
						@correct_index = true
						@is_left_offset = false
						@is_right_offset = false
						@is_top_offset = false
						@is_bottom_offset = false
						@is_front_offset = true
						@is_back_offset = false
						@offset_factor = @resize_data[:distance_factor]
					elsif @boxHelper.selected_point[:snap] == 'backPoint'
						@resize_data =
							@boxHelper.resizeY(
								view,
								@moving_point,
								false,
								distance
							)
						@correct_index = false
						@is_left_offset = false
						@is_right_offset = false
						@is_top_offset = false
						@is_bottom_offset = false
						@is_front_offset = false
						@is_back_offset = true
						@offset_factor = -@resize_data[:distance_factor]
					elsif @boxHelper.selected_point[:snap] == 'Conner0'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = true
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = true
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = true
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner1'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = true
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = true
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = true
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner2'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = true
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = true
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = true
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner3'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = true
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = true
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = true
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner4'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = true
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = true
							@offset_factor = -@resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = true
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner5'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = true
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = true
							@offset_factor = -@resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = true
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner6'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = true
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = true
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = true
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						end
					elsif @boxHelper.selected_point[:snap] == 'Conner7'
						if @resize_direction == LOCK_X
							@resize_data =
								@boxHelper.resizeX(
									view,
									@moving_point,
									false,
									distance
								)
							@correct_index = false
							@is_left_offset = true
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Y
							@resize_data =
								@boxHelper.resizeY(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = false
							@is_bottom_offset = false
							@is_front_offset = true
							@is_back_offset = false
							@offset_factor = @resize_data[:distance_factor]
						elsif @resize_direction == LOCK_Z
							@resize_data =
								@boxHelper.resizeZ(
									view,
									@moving_point,
									true,
									distance
								)
							@correct_index = true
							@is_left_offset = false
							@is_right_offset = false
							@is_top_offset = true
							@is_bottom_offset = false
							@is_front_offset = false
							@is_back_offset = false
							@offset_factor = -@resize_data[:distance_factor]
						end
					end
				end
				def onMouseMove(flags, x, y, view)
					return unless @is_moving
					@mouse_ip.pick(view, x, y)
					@moving_point = @mouse_ip.position
					@move_data = {}
					return unless @boxHelper && @boxHelper.selected_point
					get_resize_data(view)
					view.tooltip = @mouse_ip.tooltip if @mouse_ip.valid?
					view.invalidate
				end
				def doSmartBack
					return if @pick_data.nil? || @pick_data.empty?
					sheet_id = @pick_data[:sheet_id]
					sheet_index = @pick_data[:sheet_index]
					return unless sheet_index && sheet_id
					@frame = @pick_data[:frame]
					sheet_type = @pick_data[:sheet_type]
					trans_path = @pick_data[:path]
					sub_index = @pick_data[:sub_index]
					back_id = @pick_data[:back_id]
					return unless @frame
					@sheet_data = {}
					sheet_trans = IDENTITY
					div_equation = ''
					back_size = 0
					split_type = -1
					@frame.traverseCallback do |node|
						if node['root'] && node['root']['back'] &&
								node['root']['back']['id'] &&
								node['root']['back']['id'] == back_id
							div_equation = node['root']['back']['div_equation']
							split_type = node['root']['back']['split_type']
							index = 0
							if node['root']['back']['sections'] &&
									node['root']['back']['sections'].size > 0
								back_size =
									node['root']['back']['sections'][0]['root'][
										'sections'
									].size
								node['root']['back']['sections'][0]['root'][
									'sections'
								].each do |obj|
									if index == sheet_index
										@sheet_data = {
											sheet: obj,
											parent: node['root']
										}
									end
									index = index + 1
								end
							end
						end
					end
					return if @sheet_data.empty?
					@resize_smart = {
						smart_type: 'smart_back',
						div_equation: div_equation,
						index: sheet_index,
						back_size: back_size,
						split_type: split_type
					}
					matrix = IDENTITY
					for i in (sub_index - 1).downto(0)
						if trans_path[i].is_a?(Sketchup::Group)
							matrix = matrix * trans_path[i].transformation
						end
					end
					sheet = @sheet_data[:sheet]
					matrix = matrix * sheet_trans
					vertices = sheet['root']['boundingBox']['vertices']
					@boxHelper = AmtfBox.new(vertices, matrix)
				end
				def doSmartDoor
					return if @pick_data.nil? || @pick_data.empty?
					sheet_id = @pick_data[:sheet_id]
					sheet_index = @pick_data[:sheet_index]
					return unless sheet_index && sheet_id
					@frame = @pick_data[:frame]
					sheet_type = @pick_data[:sheet_type]
					trans_path = @pick_data[:path]
					sub_index = @pick_data[:sub_index]
					door_id = @pick_data[:door_id]
					return unless @frame
					@sheet_data = {}
					sheet_trans = IDENTITY
					tree_path = @frame.findPath(door_id)
					node_data = {}
					grand_parent = {}
					if !tree_path.empty?
						node_data = tree_path[tree_path.size - 1]
						grand_parent = tree_path[tree_path.size - 2]
					end
					return if node_data.empty? || grand_parent.empty?
					div_equation = grand_parent['div_equation']
					split_type = grand_parent['split_type']
					index = 0
					door_size = 0
					if node_data['sections'] && node_data['sections'].size > 0
						door_size = node_data['sections'].size
						node_data['sections'].each do |obj|
							@sheet_data = { sheet: obj } if index == sheet_index
							index = index + 1
						end
					end
					return if @sheet_data.empty?
					@resize_smart = {
						smart_type: 'smart_door',
						div_equation: div_equation,
						index: sheet_index,
						door_size: door_size,
						split_type: split_type,
						door_parent_id: grand_parent['id']
					}
					matrix = IDENTITY
					for i in (sub_index - 1).downto(0)
						if trans_path[i].is_a?(Sketchup::Group)
							matrix = matrix * trans_path[i].transformation
						end
					end
					sheet = @sheet_data[:sheet]
					matrix = matrix * sheet_trans
					vertices = sheet['root']['boundingBox']['vertices']
					@boxHelper = AmtfBox.new(vertices, matrix)
				end
				def doSheetTask
					return if @pick_data.nil? || @pick_data.empty?
					sheet_id = @pick_data[:sheet_id]
					@frame = @pick_data[:frame]
					sheet_type = @pick_data[:sheet_type]
					trans_path = @pick_data[:path]
					sub_index = @pick_data[:sub_index]
					return unless @frame && sheet_id
					@sheet_data = {}
					sheet_trans = IDENTITY
					@frame.traverseCallback do |node|
						next unless node['root'] && node['root']['sheets']
						node['root']['sheets'].each do |k, sheet_arr|
							sheet_arr.each do |obj|
								if obj['root']['id'] == sheet_id
									sheet_trans =
										Geom::Transformation.new(
											obj['root']['transform']
										)
									@sheet_data = {
										sheet: obj,
										parent: node['root']
									}
								end
							end
						end
					end
					return if @sheet_data.empty?
					matrix = IDENTITY
					for i in (sub_index - 1).downto(0)
						if trans_path[i].is_a?(Sketchup::Group)
							matrix = matrix * trans_path[i].transformation
						end
					end
					sheet = @sheet_data[:sheet]
					matrix = matrix * sheet_trans
					vertices = sheet['root']['boundingBox']['vertices']
					@boxHelper = AmtfBox.new(vertices, matrix)
				end
				def getExtents
					bb = Geom::BoundingBox.new
					bb.add(@mouse_ip.position)
					bb
				end
				def draw(view)
					@boxHelper.draw(view) if @boxHelper
					@mouse_ip.draw(view) if @mouse_ip.display?
				end
				def onLButtonDown(flags, x, y, view)
					selection = Sketchup.active_model.selection
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					pick_point = @mouse_ip.position
					if !@frame
						@pick_data = pick_frame_data_2
						if @pick_data && @pick_data[:found_sheet] == true
							if @pick_data[:sheet_type] == 'VER_HALVED_JOINT'
								return
							end
							if @pick_data[:sheet_type] == 'HOZ_HALVED_JOINT'
								return
							end
							if @pick_data[:sheet_type] == 'smart_back'
								doSmartBack
							elsif @pick_data[:sheet_type] == 'smart_door'
								doSmartDoor
							else
								doSheetTask
							end
						end
						@is_moving = false
					else
						if @boxHelper && @resize_data.nil?
							@boxHelper.pick_scale_point(view, pick_point)
							@is_moving = true
						elsif @is_moving == true && @boxHelper &&
								!@resize_data.nil?
							!@resize_smart.empty? ? resize_smart : resize_sheet
							@is_moving = false
							view.lock_inference
						end
					end
					view.invalidate
				end
				def cancel_tool
					@resize_data = nil
					@is_moving = false
					@boxHelper = nil
					doSheetTask
				end
				def reset_tool
					@my_vcb = ''
					@pick_data = nil
					@boxHelper = nil
					@sheet_data = {}
					@frame = nil
					@section_data = {}
					@grand_parent = {}
					@doing_task = nil
					@resize_data = nil
					@resize_smart = {}
				end
				def resize_smart(distance = nil)
					return unless @resize_data
					if @resize_smart[:div_equation] == '/1'
						reset_tool
						return
					end
					snap = @boxHelper.selected_point[:snap]
					if @resize_smart[:index] == 0
						if %w[
								Conner0
								Conner3
								Conner4
								Conner7
								leftPoint
						   ].include?(snap)
							reset_tool
							Sketchup.status_text = OB[:not_suitable_edge]
							return
						end
					end
					if @resize_smart[:index] ==
							@resize_smart[:back_size].to_i - 1
						if %w[
								Conner1
								Conner2
								Conner5
								Conner6
								rightPoint
						   ].include?(snap)
							reset_tool
							Sketchup.status_text = OB[:not_suitable_edge]
							return
						end
					end
					if @is_front_offset || @is_back_offset
						reset_tool
						Sketchup.status_text = OB[:not_suitable_edge]
						return
					end
					if @resize_smart[:split_type] == 0
						if @is_left_offset || @is_right_offset
							reset_tool
							Sketchup.status_text = OB[:not_suitable_edge]
							return
						end
					end
					if @resize_smart[:split_type] == 1
						if @is_top_offset || @is_bottom_offset
							reset_tool
							Sketchup.status_text = OB[:not_suitable_edge]
							return
						end
					end
					return if @resize_data[:distance].nil?
					root = @frame.root
					distance = @resize_data[:distance].to_l if distance.nil?
					root_data = {
						unit: AMTF_Dialog.get_model_units,
						smart_type: @resize_smart[:smart_type],
						distance: distance.to_f,
						index: @resize_smart[:index],
						root: @frame.root,
						is_left_offset: @is_left_offset,
						is_right_offset: @is_right_offset,
						is_top_offset: @is_top_offset,
						is_bottom_offset: @is_bottom_offset,
						offset_factor: @offset_factor,
						frame_ID: @frame.id,
						back_id: @pick_data[:back_id],
						door_id: @pick_data[:door_id],
						door_parent_id: @resize_smart[:door_parent_id]
					}
					js_command = "back_resize('#{root_data.to_json}');"
					AMTF_Dialog.dialog.execute_script(js_command)
					reset_tool
				end
				def resize_sheet(distance = nil)
					return unless @resize_data
					root = @frame.root
					distance = @resize_data[:distance].to_l if distance.nil?
					root_data = {
						unit: AMTF_Dialog.get_model_units,
						distance: distance.to_f,
						sheet: @sheet_data[:sheet],
						root: @frame.root,
						is_left_offset: @is_left_offset,
						is_right_offset: @is_right_offset,
						is_top_offset: @is_top_offset,
						is_bottom_offset: @is_bottom_offset,
						is_front_offset: @is_front_offset,
						is_back_offset: @is_back_offset,
						offset_factor: @offset_factor,
						frame_ID: @frame.id
					}
					js_command = "sheet_resize('#{root_data.to_json}');"
					AMTF_Dialog.dialog.execute_script(js_command)
					reset_tool
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #