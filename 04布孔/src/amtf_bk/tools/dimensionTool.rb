# encoding: UTF-8
module AMTF
  module BUKONG
  module DimToolModule
    class DimTool < Tools::Base      
        STS_CLASS_NAME   = OB[:dimension_tool].freeze
        def initialize (params)  
          @dialog = params[:dialog];
          @scene_data = params[:scene_data]
          @filename_project_view = params[:filename_project_view]
          @filename_axo_view =  params[:filename_axo_view]
          @page_name = params[:page_name]
          @grp_name_id = params[:grp_name_id]     
          @mouse_up = false
          @start = nil  # Geom::Point3d
          @end = nil    # Geom::Point3d      
          @mouse_ip = Sketchup::InputPoint.new      
          @pick_stage = 0
          @selected_points = []
          # view = Sketchup.active_model.active_view
          # entities = Sketchup.active_model.entities
          # @sheet_data = []
          # for i in 0..entities.size - 1
          #   entity = entities[i]
          #   next unless entity.is_a?(Sketchup::Group)                
          #   explode = Utilities.get_comp_attribute(entity, 'amtflogic_exploded', 'amtf_dict')
          #   if explode != 'true'  || Utilities.hasParent?(entity)
          #     UI.messagebox OB[:explode_required], MB_OK
          #     return
          #   else             
          #     next unless Utilities.solid?(entity)
          #     @sheet_data << Utilities.get_sheet_vertext(entity) 
          #   end
          # end    
          @part_list = params[:part_list]
          @vHash = vertexHash[:vHash]
          @allFaces = vertexHash[:faces]
      end
      def vertexHash
        vHash = Hash.new   
        faces = []    
        @part_list.each{
          |part|
          fcs = part.comp.entities.grep(Sketchup::Face)
          fcs.each{
            |f|
            faces << {:face => f, :comp => part.comp}
          }
          edges = part.comp.entities.grep(Sketchup::Edge)
          edges.each{
            |edg|
            vHash[edg.start] = part.comp if !vHash.has_key?(edg.start)
            vHash[edg.end] = part.comp if !vHash.has_key?(edg.end)
          }          
        }
        return {:vHash => vHash, :faces => faces}
      end
      def bbPlane(pick_post)
        # Get bounding box plane
        groups = @scene_data.select{
          |item|
          item[:grp_name_id] = @grp_name_id
        }
        return if groups.size == 0        
        tempBB = Geom::BoundingBox.new
        groups[0][:comp_array].each{
          |grp|  
          next if !grp.valid?
          tempBB.add grp.bounds          
        }
        bbox = Utilities.bound_points(tempBB)         
        center = tempBB.center
        return if bbox.size < 8
        left_plane = [bbox[0], bbox[0].vector_to(bbox[1]).normalize]
        right_plane = [bbox[1], bbox[1].vector_to(bbox[0]).normalize]
        top_plane = [bbox[4], bbox[4].vector_to(bbox[0]).normalize]
        bottom_plane = [bbox[0], bbox[0].vector_to(bbox[4]).normalize]
        front_plane = [bbox[6], bbox[6].vector_to(bbox[4]).normalize]
        back_plane = [bbox[4], bbox[4].vector_to(bbox[6]).normalize]
        direction = direction_by_page_name(center, pick_post)
        plane2 = nil
        if direction == X_AXIS
          plane2 = right_plane
        elsif direction == X_AXIS.reverse
          plane2 = left_plane 
        elsif direction == Y_AXIS
          plane2 = front_plane
        elsif direction == Y_AXIS.reverse
          plane2 = back_plane 
        elsif direction == Z_AXIS
          plane2 = top_plane
        elsif direction == Z_AXIS.reverse
          plane2 = bottom_plane 
        end
        return {
          :plane2 => plane2,
          :center => center
        }
      end
      def onLButtonDown flags, x, y, view 
        unless (!@mouse_up && @pick_stage == 0) || (@mouse_up && @pick_stage == 1)
          reset
          return 
        end
        @pick_stage +=1       
        @start = Geom::Point3d.new( x, y, 0 ) if @pick_stage ==1    
        pick_post = @mouse_ip.position  
        bBox = bbPlane(pick_post)
        @center = bBox[:center]
        if @pick_stage == 2 
          @pick_stage = 0         
          @input.pick(view, x, y)
          # pick_post = @input.position  
          # center = Utilities.find_center_of_points(@selected_points)           
          layer_label = "#{@page_name}_dim"
          scene_layer = Sketchup.active_model.layers.add(layer_label)
          Sketchup.active_model.pages.each{
            |page|
            if page.name == @page_name
              page.set_visibility scene_layer, true
            else
              page.set_visibility scene_layer, false
            end
          }
          direction = direction_by_page_name(@center, pick_post)
          plane2 = bBox[:plane2]
          normal1 = nil
          if @page_name.match(/\w*\-amtf\-topView/)
            normal1 = Z_AXIS.reverse        
          elsif  @page_name.match(/\w*\-amtf\-bottomView/)
            normal1 = Z_AXIS        
          elsif  @page_name.match(/\w*\-amtf\-frontView/)
            normal1 = Y_AXIS.reverse  
          elsif  @page_name.match(/\w*\-amtf\-backView/)
            normal1 = Y_AXIS           
          elsif  @page_name.match(/\w*\-amtf\-rightView/)
            normal1 = X_AXIS.reverse   
          elsif  @page_name.match(/\w*\-amtf\-leftView/)
            normal1 = X_AXIS   
          elsif  @page_name.match(/\w*\-amtf\-[^\n]*SliceView/)
            page = Sketchup.active_model.pages[@page_name]
            vector_s = page.get_attribute 'amtf_dict', 'direction'
            normal1 = Utilities.string_vector_decode(vector_s)
          end
          plane1 = [[ORIGIN, normal1]]
          dim_data = xu_ly_diem(normal1, direction, plane1, plane2, @selected_items)     
          dim_data = short_first(dim_data)      
          Sketchup.active_model.start_operation "Dim"          
          offset = Utilities.vector_multiply(10*DIM_SPACING, direction.normalize)
          spacing = 0
          dim_data.each{
            |key, data|           
            data.each do 
              |item|   
              edge = item[:edge]
              comp = item[:comp]
              path =  Sketchup::InstancePath.new([comp, edge])           
              ptr0 = Utilities.point_context_convert(edge.start.position, comp.transformation, IDENTITY)
              ptr1 = Utilities.point_context_convert(edge.end.position, comp.transformation, IDENTITY)
              off_0 = ptr0.project_to_plane(plane2)
              dist_0 = ptr0.distance(off_0).to_f
              offset = Utilities.vector_multiply(dist_0.to_l + DIM_OFFSET + spacing, direction.normalize)              
              dim = Sketchup.active_model.entities.add_dimension_linear([path, ptr0], [path, ptr1], offset)  
              dim.layer = scene_layer       
              dim.set_attribute("amtf_dict", "distance", dist_0 )
            end
            spacing += DIM_SPACING          
          }
          # end
          # for i in 0..dim_data.size - 1
          #   next if final[i][:comp].hidden?
          #   path = Sketchup::InstancePath.new([final[i][:comp], final[i][:edge]])
          #   ptr0 = Utilities.point_context_convert(final[i][:edge].start.position, final[i][:comp].transformation, IDENTITY)
          #   ptr1 = Utilities.point_context_convert(final[i][:edge].end.position, final[i][:comp].transformation, IDENTITY)
          #   off_0 = ptr0.project_to_plane(plane2)
          #   dist_0 = ptr0.distance(off_0).to_f
          #   offset = Utilities.vector_multiply(dist_0.to_l + DIM_OFFSET + DIM_SPACING, direction.normalize)
          #   dim = Sketchup.active_model.entities.add_dimension_linear([path, ptr0], [path, ptr1], offset)           
          #   dim.layer = scene_layer
          #   # Lưu khoảng cách distance đ�?sang bên layout lấy các điểm v�?cùng mốc
          #   dim.set_attribute("amtf_dict", "distance", dist_0 )
          # end
          reset
        end
        Sketchup.active_model.commit_operation
        view.lock_inference 
        view.invalidate    
      end 
      def onLButtonUp( flags, x, y, view )
        if  @pick_stage  == 1 
          @mouse_up = true
        else
          @mouse_up = false          
        end
        return if @pick_stage != 1 || selection_polygon.nil?
        @selected_items = []
        @vHash.each{
          |v, comp|
          point = Utilities.point_context_convert(v.position, comp.transformation, IDENTITY)
          screen_point = view.screen_coords(point)
          if Geom.point_in_polygon_2D( screen_point, selection_polygon, true )
            @selected_items << {
              :selected_point => point,
              :comp => comp,
              :vertex => v
            }
            @selected_points << point
          end
        }
        @start = nil
        @end = nil
        view.invalidate
      end
      def onMouseMove( flags, x, y, view )
          @end = Geom::Point3d.new( x, y, 0 )
          @mouse_ip.pick(view, x, y)
          view.refresh
          view.invalidate
      end
      def deactivate(_view)
        @@active_tool_class = nil
        @start = nil
        @end = nil
        @mouse_up = false
        @selected_points = []
        @selected_items = []
        @pick_stage = 0
        @mouse_ip = nil
        @center = nil 
        @mouse_ip = nil
      end 
      def onCancel(reason, view)
        @start = nil
        @end = nil
        @mouse_up = false
        @selected_points = []
        @selected_items = []
        view.invalidate
      end 
      def reset          
        @@active_tool_class = nil
        @start = nil
        @end = nil
        @mouse_up = false
        @selected_points = []
        @selected_items = []
      end
      def valid?
          @start && @end
      end
      def left_to_right?
          return false unless valid?
          @start.x < @end.x
      end
      def draw( view )
          if @selected_points
            for i in 0..@selected_points.size - 1
                view.draw_points(@selected_points[i], 10, 2, "red")
            end
          end
          if @center &&  @mouse_ip && @mouse_up && @pick_stage == 1 && @selected_points.size > 0
            view_direction = direction_by_page_name(@center, @mouse_ip.position)
            target = Utilities.find_point_online_with_distance(@center, view_direction, DIRECTION_DRAW_DISTANCE)
            view.line_stipple = '_'
            view.line_width = 1
            view.drawing_color = 'red'#CLR_SELECTION
            view.draw_lines @center, target
          end
          return false unless valid?
          view.line_stipple = (left_to_right?) ? '' : '_'
          view.line_width = 0.5
          view.drawing_color = 'red'#CLR_SELECTION
          view.draw2d( GL_LINE_LOOP, selection_polygon() )     
          true
      end    
      def onSetCursor              
        UI.set_cursor(TTLibrary.get_cursor[:select])
      end     
      def selection_polygon
          return if @start.nil? || @end.nil?
          pt1 = @start
          pt3 = @end
          pt2 = @start.clone
          pt2.x = pt3.x
          pt4 = @start.clone
          pt4.y = pt3.y
          [ pt1, pt2, pt3, pt4 ]
      end
      def uniq_array(arr)
        return if !arr 
        pos = []
        for i in  0..arr.size - 2
            for j in i + 1 .. arr.size - 1
              pos << j if arr[i][:comp] == arr[j][:comp] && arr[i][:vertex] == arr[j][:vertex]
            end
        end               
        temp = []
        for i in 0..arr.size - 1                
            temp << arr[i] if !pos.include?(i)
        end
        return temp
      end
      def direction_by_page_name(center, pick_post)
        return unless center && pick_post
        direction = nil
          if @page_name.match(/\w*\-amtf\-topView/) || @page_name.match(/\w*\-amtf\-bottomView/)
            pick_post = pick_post.project_to_plane([ORIGIN, Z_AXIS])
            center = center.project_to_plane([ORIGIN, Z_AXIS])
            vector = center.vector_to(pick_post)    
            if vector.angle_between(Y_AXIS) <= 45.degrees              
              direction = Y_AXIS
            elsif vector.angle_between(Y_AXIS.reverse) <= 45.degrees         
              direction = Y_AXIS.reverse
            elsif vector.angle_between(X_AXIS) <= 45.degrees   
              direction = X_AXIS
            elsif vector.angle_between(X_AXIS.reverse) <= 45.degrees   
              direction = X_AXIS.reverse
            end  
        elsif @page_name.match(/\w*\-amtf\-frontView/) || @page_name.match(/\w*\-amtf\-backView/)
            pick_post = pick_post.project_to_plane([ORIGIN, Y_AXIS.reverse])
            center = center.project_to_plane([ORIGIN, Y_AXIS.reverse])
            vector = center.vector_to(pick_post)           
            if vector.angle_between(X_AXIS) <= 45.degrees              
              direction = X_AXIS
            elsif vector.angle_between(X_AXIS.reverse) <= 45.degrees         
              direction = X_AXIS.reverse
            elsif vector.angle_between(Z_AXIS.reverse) <= 45.degrees   
              direction = Z_AXIS.reverse
            elsif vector.angle_between(Z_AXIS) <= 45.degrees   
              direction = Z_AXIS
            end            
        elsif @page_name.match(/\w*\-amtf\-rightView/) || @page_name.match(/\w*\-amtf\-leftView/)
            pick_post = pick_post.project_to_plane([ORIGIN, X_AXIS.reverse])
            center = center.project_to_plane([ORIGIN, X_AXIS.reverse])
            vector = center.vector_to(pick_post)
            if vector.angle_between(Y_AXIS) <= 45.degrees              
              direction = Y_AXIS
            elsif vector.angle_between(Y_AXIS.reverse) <= 45.degrees         
              direction = Y_AXIS.reverse
            elsif vector.angle_between(Z_AXIS.reverse) <= 45.degrees   
              direction = Z_AXIS.reverse
            elsif vector.angle_between(Z_AXIS) <= 45.degrees   
              direction = Z_AXIS
            end  
        elsif @page_name.match(/\w*\-amtf\-[^\n]*SliceView/) 
          page = Sketchup.active_model.pages[@page_name]
          vector_s = page.get_attribute 'amtf_dict', 'direction'
          axis = Utilities.string_vector_decode(vector_s)
          if axis == X_AXIS || axis == X_AXIS.reverse
            pick_post = pick_post.project_to_plane([ORIGIN, X_AXIS.reverse])
            center = center.project_to_plane([ORIGIN, X_AXIS.reverse])
            vector = center.vector_to(pick_post)
            if vector.angle_between(Y_AXIS) <= 45.degrees              
              direction = Y_AXIS
            elsif vector.angle_between(Y_AXIS.reverse) <= 45.degrees         
              direction = Y_AXIS.reverse
            elsif vector.angle_between(Z_AXIS.reverse) <= 45.degrees   
              direction = Z_AXIS.reverse
            elsif vector.angle_between(Z_AXIS) <= 45.degrees   
              direction = Z_AXIS
            end 
          elsif axis == Y_AXIS || axis == Y_AXIS.reverse
              pick_post = pick_post.project_to_plane([ORIGIN, Y_AXIS.reverse])
              center = center.project_to_plane([ORIGIN, Y_AXIS.reverse])
              vector = center.vector_to(pick_post)           
              if vector.angle_between(X_AXIS) <= 45.degrees              
                direction = X_AXIS
              elsif vector.angle_between(X_AXIS.reverse) <= 45.degrees         
                direction = X_AXIS.reverse
              elsif vector.angle_between(Z_AXIS.reverse) <= 45.degrees   
                direction = Z_AXIS.reverse
              elsif vector.angle_between(Z_AXIS) <= 45.degrees   
                direction = Z_AXIS
              end  
          elsif axis == Z_AXIS || axis == Z_AXIS.reverse
              pick_post = pick_post.project_to_plane([ORIGIN, Z_AXIS])
              center = center.project_to_plane([ORIGIN, Z_AXIS])
              vector = center.vector_to(pick_post)    
              if vector.angle_between(Y_AXIS) <= 45.degrees              
                direction = Y_AXIS
              elsif vector.angle_between(Y_AXIS.reverse) <= 45.degrees         
                direction = Y_AXIS.reverse
              elsif vector.angle_between(X_AXIS) <= 45.degrees   
                direction = X_AXIS
              elsif vector.angle_between(X_AXIS.reverse) <= 45.degrees   
                direction = X_AXIS.reverse
              end 
          end            
        end
        return direction
      end
      def xu_ly_diem(normal1, normal2, plane1, plane2, vertices)
        collected = []  
        pArr = [] 
        vertices.each{
          |v|          
          next if che_khuat(v, normal1)
          pt1 = v[:selected_point].project_to_plane(plane2)
          next if diem_trung(pArr, pt1)
          pArr << pt1
          collected << v 
        }
        final = Hash.new
        collected.each{
          |item|
          edges = item[:vertex].edges
          edges.each{
            |edg|
            vector = edg.line[1]
            if !vector.parallel?(normal2) && !Geom.intersect_line_plane(edg.line, plane1)              
              final[edg] = item[:comp] if !final.has_key?(edg)
            end
          }
        }
        return final
      end
      def diem_trung(arr, point)
        arr.each{
          |pt|
          return true if Utilities.point_compare?(pt, point)
        }
        return false
      end
      def che_khuat(v, normal)
        # point �?root context
        point = v[:selected_point]
        khuat = false
        @allFaces.each{
          |ent|
          face = ent[:face]
          comp = ent[:comp]
          next if Utilities.within_face?(Utilities.point_context_convert(point, IDENTITY, comp.transformation), face)
          ray = [point, normal]
          intr = Geom.intersect_line_plane(ray, face.plane)    
          next if !intr 
          intr = Utilities.point_context_convert(intr, IDENTITY, comp.transformation)
          next if !Utilities.within_face?(intr, face)
          vector = point.vector_to(intr)
          return true if vector.dot(normal) < 0
        }
        return khuat
      end
      def short_first(dim_data)       
        result = Hash.new
        dim_data.each{
          |edg, grp|          
          dist = edg.start.position.distance(edg.end.position)
          if result.has_key?(dist)
            result[dist] << {:edge => edg, :comp => grp}
          else
            result[dist] = [{:edge => edg, :comp => grp}]
          end          
        }
        result.sort_by{|k,v| k}
      end
      # def che_khuat( above_faces, view, point, polygon, tr )
      #   return false if above_faces.size == 0
      #   above_faces.any?{ 
      #     |polygon|   
      #     in_polygon?( view, point, polygon, tr)
      #   }
      # end
      def in_polygon?( view, point, polygon, tr)
        # point = view.screen_coords(point)
        # polygon.map!{ |ptr|  view.screen_coords(ptr) }
        result = Geom.point_in_polygon_2D( point, polygon, true )
        result
      end
      def xu_ly_diem2(normal1, normal2, plane1, plane2, vertices)
        # normal1 - pháp tuyến mặt chiếu
        # normal2 - pháp tuyến mặt vuông góc với hướng s�?lấy DIM
        # Chiếu hết theo lên mặt plane1, sau đó chiếu hết lên plane2
        # Lọc lấy các điểm duy nhất
        # xắp xếp theo th�?t�?của trục
        vertices.map!{
          |item|
          pt = Utilities.point_context_convert(pt, item[:comp].transformation, IDENTITY)
          pt = item[:selected_point].project_to_plane(plane1)
          pt = pt.project_to_plane(plane2)
          # path =  Sketchup::InstancePath.new([item[:comp], item[:vertex]])
          item = {
            :point=> pt, 
            # :path => path,
            :comp => item[:comp],
            :selected_point => item[:selected_point]
          }
        }                             
        pos = []
        for i in  0..vertices.size - 2
            for j in i + 1 .. vertices.size - 1
                if vertices[j][:point].x == vertices[i][:point].x && vertices[j][:point].y == vertices[i][:point].y && vertices[j][:point].z == vertices[i][:point].z
                    pos << j
                end
            end
        end               
        temp = []
        for i in 0..vertices.size - 1            
            temp << vertices[i] if !pos.include?(i)
        end
        if X_AXIS.parallel?(normal1)
          if Y_AXIS.parallel?(normal2)
            temp.sort_by!{
              |item|
              item[:point].z
            }
          elsif Z_AXIS.parallel?(normal2)
            temp.sort_by!{
              |item|
              item[:point].y
            }
          end
        elsif Y_AXIS.parallel?(normal1)
          if Z_AXIS.parallel?(normal2)
            temp.sort_by!{
              |item|
              item[:point].x
            }
          elsif X_AXIS.parallel?(normal2)
            temp.sort_by!{
              |item|
              item[:point].z
            }
          end
        elsif Z_AXIS.parallel?(normal1)
          if Y_AXIS.parallel?(normal2)
            temp.sort_by!{
              |item|
              item[:point].x
            }
          elsif X_AXIS.parallel?(normal2)
            temp.sort_by!{
              |item|
              item[:point].y
            }
          end
        end
        return temp
      end
      def xu_ly_diem1(normal1, normal2, plane1, plane2, vertices)
        # normal1 - pháp tuyến mặt chiếu
        # normal2 - pháp tuyến mặt vuông góc với hướng s�?lấy DIM
        # B�?hết các cạnh có cùng vector ch�?phương với phương chiếu
        #  Tìm xem có cạnh nào là hình chiếu của cạnh kia không?
        #  Tìm các cặp cạnh tương ứng song song, chiếu xuống mặt, nếu hình chiếu trùng nhau thì
        #  tìm khoảng cách t�?điểm chiếu tới cạnh, cạnh nào có khoảng cách xa hơn thì lấy
        #  Lọc ra mảng: sheet - edges
        return unless normal1 && normal2 && plane1 && plane2 && vertices
        edge_comp = []
        for i in 0..vertices.size - 1
          edges = vertices[i][:vertex].edges
          edges.each {
            |edg|
             # X�?lý luôn các cạnh song với pháp tuyến của mặt plane1 và plane 2             
            vector = edg.start.position.vector_to(edg.end.position)
            next if vector.parallel?(normal1)
            next if vector.parallel?(normal2)
            edge_comp << {:edge => edg, :comp => vertices[i][:comp]}
          }
        end
        edge_comp.uniq!  
        mang_1 = []
        trung = []
        da_lay = []
        # Lấy các edge không có hình chiếu trùng vào mảng mang_1[]
        # lấy các edge có hình chiếu trùng vào mảng trung[]
        for i in 0..edge_comp.size - 1
          e1 = edge_comp[i][:edge]
          vt1 = e1.start.position.vector_to(e1.end.position)
          found = false
          for j in 0..edge_comp.size - 1                         
            e2 = edge_comp[j][:edge]
            next if e1 == e2
            vt2 = e2.start.position.vector_to(e2.end.position)
            p10 = e1.start.position.project_to_plane(plane1)
            p11 = e1.end.position.project_to_plane(plane1)
            p20 = e2.start.position.project_to_plane(plane1)
            p21 = e2.end.position.project_to_plane(plane1)
            if (Utilities.point_compare?(p10, p20) && Utilities.point_compare?(p11, p21)) ||
              (Utilities.point_compare?(p10, p21) && Utilities.point_compare?(p11, p20))
              if p10.distance(e1.start.position) > p20.distance(e2.start.position)                  
                trung << {:edge => e1, :comp => edge_comp[i][:comp] } if !da_lay.include?(e1)
                da_lay << e1
                da_lay << e2
              elsif p10.distance(e1.start.position) < p20.distance(e2.start.position) 
                trung << {:edge => e2, :comp => edge_comp[j][:comp] } if !da_lay.include?(e2)
                da_lay << e1
                da_lay << e2
              # else
              #   trung << {:edge => e2, :comp => edge_comp[j][:comp] }
              end 
              found = true
            end
          end
          if !found
            mang_1 << {:edge => e1, :comp => edge_comp[i][:comp]} if !da_lay.include?(e1)
            da_lay << e1
          end
        end
        trung.uniq!        
        mang_1.push(*trung)
        mang_2 = []
        trung2 = []
        da_lay = []
        for i in 0..mang_1.size - 1
          e1 = mang_1[i][:edge]
          vt1 = e1.start.position.vector_to(e1.end.position)
          found = false
          for j in  0..mang_1.size - 1     
            e2 = mang_1[j][:edge]
            next if e1 == e2
            vt2 = e2.start.position.vector_to(e2.end.position)     
            p10 = e1.start.position.project_to_plane(plane2)            
            p11 = e1.end.position.project_to_plane(plane2)
            p20 = e2.start.position.project_to_plane(plane2)
            p21 = e2.end.position.project_to_plane(plane2)      
            if (Utilities.point_compare?(p10, p20) && Utilities.point_compare?(p11, p21)) ||
              (Utilities.point_compare?(p10, p21) && Utilities.point_compare?(p11, p20))
              if p10.distance(e1.start.position) < p20.distance(e2.start.position)                
                trung2 << {:edge => e1, :comp => mang_1[i][:comp] } if !da_lay.include?(e1)
                da_lay << e1
                da_lay << e2
              elsif p10.distance(e1.start.position) > p20.distance(e2.start.position)
                trung2 << {:edge => e2, :comp => mang_1[j][:comp] }if !da_lay.include?(e2)
                da_lay << e1
                da_lay << e2
              end 
              found = true
            end  
          end
          if !found
            mang_2 << {:edge => e1, :comp => mang_1[i][:comp] }  if !da_lay.include?(e1)           
            da_lay << e1
          end           
        end   
        trung2.uniq!
        mang_2.push(*trung2) 
        mang_2.uniq!
        mang_3 = []
        trung3 = []
        da_lay = []
        for i in 0..mang_2.size - 1
          e1 = mang_2[i][:edge]
          vt1 = e1.start.position.vector_to(e1.end.position)
          found = false
          for j in  0..mang_2.size - 1     
            e2 = mang_2[j][:edge]
            next if e1 == e2
            vt2 = e2.start.position.vector_to(e2.end.position)  
            p10 = e1.start.position.project_to_plane(plane1)            
            p11 = e1.end.position.project_to_plane(plane1)
            p20 = e2.start.position.project_to_plane(plane1)
            p21 = e2.end.position.project_to_plane(plane1)    
            pc10 = p10.project_to_plane(plane2)            
            pc11 = p11.project_to_plane(plane2)
            pc20 = p20.project_to_plane(plane2)
            pc21 = p21.project_to_plane(plane2)    
            if (Utilities.point_compare?(pc10, pc20) && Utilities.point_compare?(pc11, pc21)) ||
              (Utilities.point_compare?(pc10, pc21) && Utilities.point_compare?(pc11, pc20))
              trung3 << {:edge => e1, :comp => mang_2[i][:comp] } if !da_lay.include?(e1)
              da_lay << e1
              da_lay << e2
              # if p10.distance(e1.start.position) < p20.distance(e2.start.position)                
              #   trung3 << {:edge => e1, :comp => mang_2[i][:comp] } if !da_lay.include?(e1)
              #   da_lay << e1
              # elsif p10.distance(e1.start.position) > p20.distance(e2.start.position)
              #   trung3 << {:edge => e2, :comp => mang_2[j][:comp] }if !da_lay.include?(e2)
              #   da_lay << e2
              # end 
              found = true
            end  
          end
          if !found
            mang_3 << {:edge => e1, :comp => mang_2[i][:comp] }  if !da_lay.include?(e1)           
            da_lay << e1
          end           
        end   
        trung3.uniq!
        mang_3.push(*trung3) 
        mang_3.uniq!
        # Sắp xếp theo trật t�? đ�?dài của edge
        mang_3 = mang_3.sort_by{
          |item|
          item[:edge].start.position.distance(item[:edge].end.position)
        }
        mang_3
      end
      def top_face(grp, plane, normal)
        faces  = grp.entities.grep(Sketchup::Face)  
        arr = []      
        faces.each{
          |f|
          center = Utilities.find_center_face(f)
          projectPoint = center.project_to_plane(plane)
          vector = center.vector_to(projectPoint)          
          height = center.distance_to_plane(plane)
          height = - height if vector.dot(normal) < 0
          arr << {
            :face => f,
            :height => height
          }
        }        
        arr.sort_by!{ |f|  f[:height] }
        arr.last
      end
    end#Class
  end#  DimToolModule
  end # BuKong
end # 
