# encoding: UTF-8
module AMTF
	module BUKONG
		module RemoveSheetModule
			class RemoveSheet < Tools::Base
				STS_CLASS_NAME = OB[:remove_sheet].freeze
				def activate
					@ph = Sketchup.active_model.active_view.pick_helper
					@mouse_ip = Sketchup::InputPoint.new
					@@active_tool_class = self.class
					Sketchup.status_text = OB[:select_target_object]
					if AMTF_STORE.init_open_json == false
						saver = AmtfSaver.new
						saver.load_comp
						AMTF_STORE.init_open_json = true
					end
					# url = "#{ServerOptions.get_server_url}/tool_bag"
					# if !AMTF_Dialog.dialog.nil? && AMTF_Dialog.dialog.visible?
					#     AMTF_Dialog.dialog.bring_to_front
					#     AMTF_Dialog.dialog.set_url(url)
					# else
					#     AMTF_Dialog.create_dialog()
					#     AMTF_Dialog.dialog.show()
					# end
					AMTF_Dialog
						.dialog
						.on(
							'sheet_remove_update_frame'
						) do |deferred, params, frame_ID|
							begin
								amtfFrame =
									AMTF_STORE.frame_premitives[frame_ID.to_i]
								if amtfFrame
									Sketchup.active_model.start_operation(
										OB[:remove_sheet],
										true
									)
									amtfFrame.update(params)
									Sketchup.active_model.commit_operation
									deferred.resolve(true)
								end
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
				end
				def deactivate(view)
					@@active_tool_class = nil
					view.invalidate
				end
				def resume(view)
					view.invalidate
				end
				def suspend(view)
					view.invalidate
				end
				def onCancel(reason, view)
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					selection = Sketchup.active_model.selection
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					pick_path = pick_data[:path]
					frame = pick_path.first
					unless frame.get_attribute('amtf_dict', 'comp_type') ==
							'AmtfFrame'
						return
					end
					pick_component = pick_data[:pick_component]
					pick_face = pick_data[:pick_face]
					amtfFrame = AMTF_STORE.frame_premitives[frame.persistent_id]
					return unless amtfFrame
					sheet_id =
						pick_component.get_attribute('amtf_dict', 'sheet_id')
					root = amtfFrame.root
					selection.clear
					selection.add(frame)
					sheet_type =
						pick_component.get_attribute('amtf_dict', 'sheet_type')
					AMTF_Dialog.dialog.execute_script(
						"execute_delete_sheet(
                        {   sheet_type:'#{sheet_type}',
                            sheet_id : '#{sheet_id}',
                            frame_ID : '#{frame.persistent_id}',
                            root : '#{root.to_json}'
                        }
                    );"
					)
					view.invalidate
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #