# encoding: UTF-8
module AMTF
    module BUKONG
    module DrawingTool
      class DrawingExport < Tools::Base   
        def initialize(params) 
            @dialog = params[:dialog]
            @scene_data = params[:scene_data]
            @filename_project_view = params[:filename_project_view]
            @filename_axo_view = params[:filename_axo_view]
            @part_list = params[:part_list]
            @ph = Sketchup.active_model.active_view.pick_helper
            @tool_name = params[:tool_name]
            @comp_arr = []
            @points = []
        end
        def activate
          @@active_tool_class = self.class
          @mouse_ip = Sketchup::InputPoint.new
          @picked_first_ip = Sketchup::InputPoint.new
          update_ui
        end    
        def deactivate(view)
          @@active_tool_class =nil
          view.lock_inference
          view.invalidate
        end    
        def resume(view)
          update_ui
          view.invalidate
        end    
        def onCancel(reason, view)
          reset_tool
          view.lock_inference
          view.invalidate
        end    
        def onMouseMove(flags, x, y, view)
          if picked_first_point?
            @mouse_ip.pick(view, x, y, @picked_first_ip)
          else
            @mouse_ip.pick(view, x, y)
          end
          view.tooltip = @mouse_ip.tooltip if @mouse_ip.valid?
          view.invalidate
        end    
        def onKeyUp(key, repeat, flags, view)
          view.lock_inference
          view.invalidate
        end
        def onKeyDown(key, repeat, flags, view)
          return unless @points. size === 1            
          if key == VK_LEFT #y-axis           
            lock_vector = Geom::Vector3d.new  [0, 1, 0]           
          elsif key == VK_RIGHT  
            lock_vector = Geom::Vector3d.new  [1, 0, 0]  
          elsif key == VK_UP  
            lock_vector = Geom::Vector3d.new  [0, 0, 1]  
          else
            return            
          end
          second_point = Utilities.find_point_online_with_distance(@points[0], lock_vector, 1)
          view.lock_inference(
              Sketchup::InputPoint.new(@points[0]),
              Sketchup::InputPoint.new(second_point)
            )
          view.invalidate          
        end
        def onLButtonDown(flags, x, y, view)
            selection = Sketchup.active_model.selection   
            @ph.do_pick(x, y)
            get_pick = pick_me(@ph)
            return if get_pick.nil?
            pick_component = get_pick[:pick_component]
            selection.clear
            selection.add( pick_component) 
            @points = picked_points 
            if (@points. size === 2)   
              post0 =  @points[0]
              post1 =  @points[1]
              return if Utilities.point_compare?(post0, post1)
              direction = post0.vector_to(post1).normalize
              projection_tool(direction, post1, pick_component)
              view.lock_inference
              view.invalidate
              reset_tool
            else
                @picked_first_ip.copy!(@mouse_ip)
            end  
            update_ui
            view.invalidate
        end    
        CURSOR_PENCIL = 632
        def onSetCursor         
          UI.set_cursor(CURSOR_PENCIL) if @tool_name != 'custom_scene'
        end    
        def draw(view)
          draw_preview(view)
          @mouse_ip.draw(view) if @mouse_ip.display?
        end
        private    
        def update_ui
          # if picked_first_point?
          #   Sketchup.status_text = OB[:select_end_point]
          # else
          #   Sketchup.status_text = OB[:select_start_point]
          # end
        end    
        def reset_tool
          @picked_first_ip.clear
          update_ui
        end
        def picked_first_point?
          @picked_first_ip.valid?
        end
        def picked_points
          points = []
          points << @picked_first_ip.position if picked_first_point?
          points << @mouse_ip.position if @mouse_ip.valid?
          points
        end
        def draw_preview(view)
          points = picked_points          
          view.draw_points points[0], 5, 2, "red" if points[0]
          status = view.draw_points points[1], 5, 2, "red" if points[1]
          return unless points.size == 2
          view.set_color_from_line(*points)
          view.line_width = 1
          view.line_stipple = ''
          view.draw(GL_LINES, points)
        end
        def projection_tool(direction, post1, pick_component)              
              grp_name = Utilities.get_comp_attribute(pick_component, 'prefix', 'amtf_dict')
              if grp_name.nil?  
                UI.messagebox OB[:no_name], MB_OK  
                return
              end
              if grp_name.empty?  
                UI.messagebox OB[:no_name], MB_OK  
                return
              end
              grp_name_id = Utilities.fj_uniq_name(grp_name)
              pages = Sketchup.active_model.pages
              existing_pages = []
              @scene_data.each do
                |scene|
                existing_pages.push(*scene[:pages]) if scene[:pages].size > 0
              end
              page_name = ''
              distance = 0
              while page_name == '' || distance.nil?
                if @tool_name == 'slice_tool'
                  prompts = [OB[:name], OB[:distance_or_blank]]
                  defaults = ["", ""]                
                elsif @tool_name == 'projection_tool'
                  prompts = [OB[:name]]
                  defaults = [""]
                end
                inputs = UI.inputbox( prompts, defaults ,OB[:name])
                return if !inputs
                distance = Utilities.normalize(inputs[1])
                name = Utilities.fj_uniq_name(inputs[0])                
                page_name = name if !existing_pages.include?(name)
              end
              if distance != 0
                line = [@points[0], @points[0].vector_to(@points[1])]
                post1 = Utilities.find_point_online_with_distance(@points[0], line[1], distance)
              end
              if @tool_name == 'slice_tool'
                entities = Sketchup.active_model.entities
                sp = entities.add_section_plane(post1, direction)
                sp.activate
              end
              page_name =  grp_name_id + '-amtf-'+Utilities.fj_uniq_name(page_name)+'SliceView'
              Drawing_Export.create_project_scene(@part_list, page_name, @filename_project_view, direction, grp_name_id, @scene_data, slice_tool: true)
              data_to_js = {
                  :grp_name_id => grp_name_id,
                  :scene_data => Drawing_Export.get_needed_scene(@scene_data) 
              }
              json_str = data_to_js.to_json
              js_command = "update_drawing_list('" + json_str +"');"
              @dialog.execute_script(js_command)
        end
      end #Class
    end#  DrawingTool
    end # BuKong
  end # 