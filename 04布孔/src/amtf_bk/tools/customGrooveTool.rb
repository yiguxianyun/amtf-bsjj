# encoding: UTF-8
module AMTF
	module BUKONG
		module CustomGrooveTool
			class CustomGroove < Tools::Base
				STS_CLASS_NAME = OB[:custom_groove].freeze
				def onLButtonDown(flags, x, y, view)
					pick_data = pick_sheet(@ph, x, y)
					return if pick_data.empty?
					pick_component = pick_data[:pick_component]
					return unless pick_component
					selection = Sketchup.active_model.selection
					return unless selection.size > 0
					groove = selection[0]
					return unless groove.is_a?(Sketchup::Group)
					faces = groove.entities.grep(Sketchup::Face)
					return if faces.size != 1
					begin
						saver = AmtfSaver.new
						config = saver.open_config
						unless config
							Sketchup.status_text "There isn't config data!"
							return
						end
						config = JSON.parse(config)
						groove_config = config['groove']
						unless groove_config
							Sketchup.status_text "There isn't config data!"
							return
						end
						mapping = {}
						grv_list = ''
						default = ''
						groove_config.each_with_index do |item, idx|
							title = ''
							map_idx = "#{idx + 1}-idx"
							mapping[map_idx] = {
								groove_type_id: item['id'],
								config: item
							}
							item['params1'].each do |para|
								if para['key'] == 'name'
									grv_list += "#{idx + 1}-#{para['value']}"
									grv_list += '|' if idx <
										groove_config.size - 1
									mapping[map_idx][:groove_name] =
										para['value']
								end
								if para['key'] == 'groove_depth'
									mapping[map_idx][:groove_depth] =
										para['value']
								end
								if para['key'] == 'extra_grooves'
									mapping[map_idx][:extra_grooves] =
										para['value']
								end
								if para['key'] == 'color'
									mapping[map_idx][:color] = para['value']
								end
							end
						end
						groove_name = ''
						correct = false
						while !correct
							prompts = [OB[:select_name]]
							defaults = ["1-#{mapping['1-idx'][:groove_name]}"]
							list = [grv_list]
							inputs =
								UI.inputbox(
									prompts,
									defaults,
									list,
									OB[:custom_groove]
								)
							if !inputs
								Sketchup.active_model.tools.pop_tool
								return
							end
							groove_name = inputs[0].to_s
							groove_name = inputs[1].to_s if groove_name.empty?
							correct = true if !groove_name.empty?
						end
						index = groove_name.index('-')
						index_map = groove_name.slice(0..index - 1)
						config_val = mapping["#{index_map}-idx"]
						return unless config_val && config_val[:config]
						hash =
							Utilities.get_setting(
								'groove',
								config,
								config_val[:groove_type_id]
							)
						return unless hash
						hash['id'] = config_val[:groove_type_id]
						Sketchup.active_model.start_operation 'custom_groove',
						                                      true
						chanel_grp =
							Utilities
								.definition(pick_component)
								.entities
								.add_group
						tr =
							Utilities.transform_context_convert(
								groove.transformation,
								pick_data[:trans]
							)
						inst =
							Utilities
								.definition(chanel_grp)
								.entities
								.add_instance(Utilities.definition(groove), tr)
						inst.explode
						groove.erase!
						pick_component.set_attribute(
							'amtf_dict',
							'has_groove',
							'yes'
						)
						minifixID = Utilities.CreatUniqueid
						chanel_grp.set_attribute 'amtf_dict', 'type', 'chanel'
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_type_id',
						                         config_val[:groove_type_id]
						chanel_grp.set_attribute 'amtf_dict',
						                         'minifixID',
						                         Utilities.CreatUniqueid
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_name',
						                         config_val[:groove_name]
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_depth',
						                         config_val[:groove_depth]
						chanel_grp.set_attribute 'amtf_dict',
						                         'extra_grooves',
						                         config_val[:extra_grooves]
						chanel_grp.set_attribute 'amtf_dict',
						                         'config_data',
						                         hash.to_json
						chanel_grp.material = config_val[:color] if config_val[
							:color
						]
						Sketchup.active_model.commit_operation
					rescue => error
						Utilities.print_exception(error, false)
					end
				end
			end #Class
		end #  CustomgrooveTool
	end # BuKong
end #