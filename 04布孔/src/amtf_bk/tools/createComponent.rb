# encoding: UTF-8
module AMTF
	module BUKONG
		module Component
			class CreateComponent < Tools::Base
				STS_CLASS_NAME = OB[:create_component].freeze
				def activate
					@@active_tool_class = self.class
					@mouse_ip = Sketchup::InputPoint.new
					@ph = Sketchup.active_model.active_view.pick_helper
					Sketchup.active_model.selection.clear
					@pick_snap = {}
					@frame = nil
					@face_vertices = []
					@shift_key_hold = false
					if AMTF_STORE.init_open_json == false
						saver = AmtfSaver.new
						saver.load_comp
						AMTF_STORE.init_open_json = true
					end
					AMTF_Dialog
						.dialog
						.on('createComponent') do |deferred, data|
							begin
								model_data = JSON.parse(data.to_json)
								Sketchup.active_model.start_operation(
									OB[:create_component],
									true
								)
								@frame = AmtfFrame.new
								@frame.init(model_data)
								@inst = @frame.frame
								# @inst.visible = false
								frame_depth = model_data['root']['frame_depth']
								vect =
									Geom::Vector3d.new(
										0,
										Utilities.unitConvert(frame_depth),
										0
									)
								@shift_origin = Geom::Transformation.new vect
								Sketchup.active_model.commit_operation
								AMTF_STORE.show_all_sections
								deferred.resolve(true)
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
					AMTF_Dialog
						.dialog
						.on(
							'place_component_adjust_update_frame'
						) do |deferred, params, frame_ID|
							begin
								amtfFrame =
									AMTF_STORE.frame_premitives[frame_ID]
								Sketchup.active_model.start_operation(
									OB[:create_component],
									true
								)
								amtfFrame.update(JSON.parse(params.to_json))
								@frame.erase!
								Sketchup.active_model.commit_operation
								AMTF_STORE.show_all_sections
								@pick_snap = {}
								deferred.resolve(true)
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
				end
				def deactivate(view)
					@@active_tool_class = nil
					@pick_snap = {}
					@frame = nil
					AMTF_STORE.hide_all_sections
				end
				def onCancel(reason, view)
					@cancelled = true
					@pick_snap = {}
					@frame = nil
					# Sketchup.active_model.select_tool(nil)
				end
				def onKeyUp(key, repeat, flags, view)
					@shift_key_hold = false if key == 16
				end
				def onKeyDown(key, repeat, flags, view)
					if key == 16
						@shift_key_hold = true
						if @section_face && @targetFrame && !@trans_path.empty?
							place_in_frame
						end
					end
				end
				def onLButtonDown(flags, x, y, view)
					return if !@inst
					@mouse_ip = view.inputpoint(x, y)
					point = @mouse_ip.position
					if @pick_snap.empty?
						@inst.move!(
							@shift_origin *
								Geom::Transformation.translation(point)
						)
					end
					if !@pick_snap.empty?
						AMTF_Dialog.dialog.execute_script(
							"place_component_adjust_request_data(#{@pick_snap.to_json});"
						)
					end
					Sketchup.active_model.selection.add(@inst)
					view.invalidate
					@placed = true
					@inst = nil
					@face_vertices = []
				end
				def onMouseMove(flags, x, y, view)
					return if !@inst
					@ph.do_pick(x, y)
					@mouse_ip.pick(view, x, y)
					point = @mouse_ip.position
					@pick_snap = {} if !@shift_key_hold
					@trans_path = []
					pickpaths = {}
					@face_vertices = []
					for index in 0..@ph.count - 1
						pickpath = @ph.path_at(index)
						pickpaths[index] = pickpath if !pickpath.include?(@inst)
					end
					if !pickpaths.empty?
						pickpaths.each do |index, value|
							if value.last.is_a?(Sketchup::Face) &&
									value.first.is_a?(Sketchup::Group)
								pick_face = value.last
								section_id =
									pick_face.get_attribute(
										'amtf_dict',
										'section_id'
									)
								next unless section_id
								next unless value.first.is_a?(Sketchup::Group)
								unless value.first.get_attribute(
										'amtf_dict',
										'comp_type'
								       ) == 'AmtfFrame'
									next
								end
								comp_id = value.first.persistent_id
								trans_path = pickpaths[index]
								matrix = IDENTITY
								for i in (trans_path.size - 1).downto(0)
									if trans_path[i].is_a?(Sketchup::Group)
										matrix =
											matrix *
												trans_path[i].transformation
									end
								end
								@face_vertices = pick_face.outer_loop.vertices
								@face_vertices.map! do |f|
									f = f.position.transform!(matrix)
								end
								@section_face = pick_face
								@targetFrame =
									AMTF_STORE.frame_premitives[comp_id]
								@trans_path = trans_path
								place_in_frame if @shift_key_hold
							end
						end
					elsif @pick_snap.empty?
						camera = Sketchup.active_model.active_view.camera
						direction = camera.direction
						line = [point, direction]
						plane = [
							Geom::Point3d.new(0, 0, 0),
							Geom::Vector3d.new(0, 0, 1)
						]
						point = Geom.intersect_line_plane(line, plane)
					end
					# @inst.visible = true
					if @pick_snap.empty?
						@inst.move!(
							@shift_origin *
								Geom::Transformation.translation(point)
						)
					end
					view.tooltip = @mouse_ip.tooltip if @mouse_ip.valid?
					view.invalidate
				end
				def draw(view)
					if @face_vertices.size > 0
						view.line_width = 4
						view.drawing_color = 'yellow'
						view.draw(GL_LINE_LOOP, @face_vertices)
					end
					if @trans_path && @trans_path.size > 0 && @pick_snap &&
							@frame && !@pick_snap.empty? && @targetFrame
						view.line_width = 3
						view.drawing_color = 'yellow'
						view.draw_lines @boundConners[0], @boundConners[1]
						view.draw_lines @boundConners[1], @boundConners[2]
						view.draw_lines @boundConners[2], @boundConners[3]
						view.draw_lines @boundConners[0], @boundConners[3]
						view.draw_lines @boundConners[4], @boundConners[5]
						view.draw_lines @boundConners[5], @boundConners[6]
						view.draw_lines @boundConners[6], @boundConners[7]
						view.draw_lines @boundConners[4], @boundConners[7]
						view.draw_lines @boundConners[0], @boundConners[4]
						view.draw_lines @boundConners[1], @boundConners[5]
						view.draw_lines @boundConners[2], @boundConners[6]
						view.draw_lines @boundConners[3], @boundConners[7]
						view.draw_points(@boundConners[0], 10, 1, 'yellow')
						view.draw_points(@boundConners[1], 10, 1, 'yellow')
						view.draw_points(@boundConners[2], 10, 1, 'yellow')
						view.draw_points(@boundConners[3], 10, 1, 'yellow')
						view.draw_points(@boundConners[4], 10, 1, 'yellow')
						view.draw_points(@boundConners[5], 10, 1, 'yellow')
						view.draw_points(@boundConners[6], 10, 1, 'yellow')
						view.draw_points(@boundConners[7], 10, 1, 'yellow')
					end
					@mouse_ip.draw(view) if @mouse_ip.display?
				end
				def get_section_by_id(selected)
					selected
				end
				def place_in_frame
					section_id =
						@section_face.get_attribute('amtf_dict', 'section_id')
					return if !section_id
					return unless @frame
					return unless @targetFrame
					selected_sections = {}
					@targetFrame.traverseCallback do |node|
						selected_sections = node['root'] if node['root'] &&
							node['root']['id'] &&
							node['root']['id'] == section_id
					end
					return if selected_sections.empty?
					sec_origin = selected_sections['origin']
					width = selected_sections['width2d']
					height = selected_sections['height2d']
					depth = selected_sections['depth']
					return unless sec_origin
					origin =
						Geom::Point3d.new(
							Utilities.unitConvert(sec_origin['x']),
							Utilities.unitConvert(sec_origin['y'] + depth),
							Utilities.unitConvert(sec_origin['z'])
						)
					@matrix = IDENTITY
					for i in (@trans_path.size - 1).downto(0)
						if @trans_path[i].is_a?(Sketchup::Group)
							comp_type =
								@trans_path[i].get_attribute 'amtf_dict',
								                             'comp_type'
							unless comp_type == 'AmtfFrame' ||
									comp_type == 'AmtfSubFrame'
								next
							end
							@matrix = @matrix * @trans_path[i].transformation
						end
					end
					origin.transform!(@matrix)
					@inst.move!(Geom::Transformation.translation(origin))
					@boundConners = boxCorners(sec_origin, width, height, depth)
					@boundConners.map! { |point| point.transform!(@matrix) }
					@pick_snap = {
						width: width,
						height: height,
						depth: depth,
						origin: origin,
						frame_ID: @targetFrame.id,
						root: @targetFrame.root,
						sub_root: @frame.root,
						section_id: section_id
					}
				end
				def boxCorners(origin, w, h, d)
					box = []
					box[0] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']),
						                  Utilities.unitConvert(origin['y']) +
								Utilities.unitConvert(d),
						                  Utilities.unitConvert(origin['z'])
					box[1] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']) +
								Utilities.unitConvert(w),
						                  Utilities.unitConvert(origin['y']) +
								Utilities.unitConvert(d),
						                  Utilities.unitConvert(origin['z'])
					box[2] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']) +
								Utilities.unitConvert(w),
						                  Utilities.unitConvert(origin['y']),
						                  Utilities.unitConvert(origin['z'])
					box[3] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']),
						                  Utilities.unitConvert(origin['y']),
						                  Utilities.unitConvert(origin['z'])
					box[4] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']),
						                  Utilities.unitConvert(origin['y']) +
								Utilities.unitConvert(d),
						                  Utilities.unitConvert(origin['z']) +
								Utilities.unitConvert(h)
					box[5] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']) +
								Utilities.unitConvert(w),
						                  Utilities.unitConvert(origin['y']) +
								Utilities.unitConvert(d),
						                  Utilities.unitConvert(origin['z']) +
								Utilities.unitConvert(h)
					box[6] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']) +
								Utilities.unitConvert(w),
						                  Utilities.unitConvert(origin['y']),
						                  Utilities.unitConvert(origin['z']) +
								Utilities.unitConvert(h)
					box[7] =
						Geom::Point3d.new Utilities.unitConvert(origin['x']),
						                  Utilities.unitConvert(origin['y']),
						                  Utilities.unitConvert(origin['z']) +
								Utilities.unitConvert(h)
					return box
				end
				def boxOrigin(frame_root)
					return(
						Geom::Point3d.new Utilities.unitConvert(bound[0]['x']),
						                  Utilities.unitConvert(bound[0]['y']),
						                  Utilities.unitConvert(bound[0]['z'])
					)
				end
			end
		end
	end
end