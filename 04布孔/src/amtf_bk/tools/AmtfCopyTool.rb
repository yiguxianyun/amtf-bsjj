# encoding: UTF-8
module AMTF
    module BUKONG
    module AmtfCopyTool
      class AmtfCopy < Tools::Base   
        def activate  
          @@active_tool_class = self.class        
          @input  = Sketchup::InputPoint.new             
          Sketchup.status_text = OB[:select_target_object]
          @isDragging = false;
          @sheets = [];
          @firsPick = nil
        end
        def deactivate(view)
          @@active_tool_class =nil
          view.lock_inference
          view.invalidate
          @isDragging = false;
          @sheets = [];
        end 
        def onKeyUp(key, repeat, flags, view)         
          view.lock_inference
          view.invalidate
          return true
        end
        def reset_tool  
          Sketchup.status_text = OB[:select_target_object]
          @isDragging = false;
          @sheets = [];
        end
        def resume(view)
          view.invalidate
        end    
        def onCancel(reason, view)
          reset_tool
          view.lock_inference
          view.invalidate
        end  
        def onLButtonDown flags, x, y, view
          model = Sketchup.active_model
          @input.pick view, x, y
          pick_post =  @input.position
          selection = model.selection
          return unless selection.size > 0
          if !@isDragging            
            @isDragging = true
            @firsPick = pick_post
            list  = []
            Utilities.traverseArray( selection, @sheets )
            if @sheets.size === 0
              reset_tool
              return
            end   
          else
            @isDragging = false
            move_vector = @firsPick.vector_to( pick_post )
            translate =  Geom::Transformation.translation(move_vector)
            Sketchup.active_model.start_operation "copy", true
            sheet_id_map = {};
            new_group_list = []
            @sheets.each{
              | sheet |
              element = sheet.comp
              new_group = Sketchup.active_model.entities.add_group
              name = element.name              
              tr = element.transformation;
              inst = Utilities.definition(new_group).entities.add_instance(Utilities.definition(element), tr ) 
              inst.explode
              new_group.name = name
              dict = element.attribute_dictionary 'amtf_dict'    
              if dict           
                  dict.each do |key,value| 
                      new_group.set_attribute 'amtf_dict', key, value
                  end
              end
              new_group.set_attribute 'amtf_dict', 'amtflogic_exploded', 'true'
              new_group.material = element.material  
              new_group.move!( translate )
              new_group.make_unique()
              new_group_list << new_group
              sheet_id_map[ element.persistent_id ] = new_group.persistent_id;
            }
            map_minifixID = {};
            @sheets.each{
              |grp|
              connectors = grp.entities.grep(Sketchup::Group)
              connectors.each{
                |mini|                
                minifixID = mini.get_attribute('amtf_dict', 'minifixID')
                map_minifixID[ minifixID ] = minifixID
              }
            }
            new_MinifixID = {}
            map_minifixID.each {
              |key, value|
              new_MinifixID[ key ] = Utilities.CreatUniqueid()
            }
            new_group_list.each {
              |grp|
              connectors = grp.entities.grep(Sketchup::Group)
              connectors.each{
                |mini|
                partner_sheet_id = mini.get_attribute('amtf_dict', 'partner_sheet_id')
                minifixID = mini.get_attribute('amtf_dict', 'minifixID')
                if( partner_sheet_id && sheet_id_map.key?( partner_sheet_id ) )
                  mini.set_attribute 'amtf_dict', 'partner_sheet_id', sheet_id_map[ partner_sheet_id ]
                end
                if( minifixID && new_MinifixID.key?( minifixID ) )
                  mini.set_attribute 'amtf_dict', 'minifixID', new_MinifixID[ minifixID ]
                end
              }
            }
            model.start_operation(OB[:move_tool], true)
            selection.clear()
            reset_tool
            Sketchup.active_model.tools.pop_tool
          end
          view.lock_inference 
          view.invalidate      
          model.commit_operation
        end  
      end #MoveHoles Class    
    end#  MovingTool
    end # BuKong
  end # 
