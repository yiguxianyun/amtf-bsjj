# encoding: UTF-8
module AMTF
	module BUKONG
		module SheetOptionsModule
			class SheetOptions < Tools::Base
				STS_CLASS_NAME = OB[:sheet_options].freeze
				def activate
					@@active_tool_class = self.class
					@mouse_ip = Sketchup::InputPoint.new
					@ph = Sketchup.active_model.active_view.pick_helper
					url = "#{ServerOptions.get_server_url}/sheet_options"
					if !AMTF_Dialog.dialog.nil? && AMTF_Dialog.dialog.visible?
						AMTF_Dialog.dialog.bring_to_front
						AMTF_Dialog.dialog.set_url(url)
					else
						AMTF_Dialog.create_dialog
						AMTF_Dialog.dialog.set_url(url)
						AMTF_Dialog.dialog.show
					end
					AMTF_Dialog
						.dialog
						.on('set_sheet_options') do |deferred, data|
							begin
								@config_data = data['config']
								@is_no_hole_sheet = data['is_no_hole_sheet']
								@groove_type_id = data['groove_type_id']
								@is_door_sheet = data['is_door_sheet']
								@sheet_grain_angle = data['sheet_grain_angle']
								@is_no_edge_banding = data['is_no_edge_banding']
								deferred.resolve(true)
							rescue => error
								Utilities.print_exception(error, false)
								Sketchup.set_status_text(error)
								deferred.resolve(false)
							end
						end
				end
				def deactivate(_view)
					@@active_tool_class = nil
					if !AMTF_Dialog.dialog.nil?
						url = "#{ServerOptions.get_server_url}/tool_bag"
						AMTF_Dialog.dialog.set_url(url)
					end
				end
				def onLButtonDown(flags, x, y, view)
					@mouse_ip.pick view, x, y
					get_pick = pick_sheet(@ph, x, y)
					return if get_pick.empty?
					pick_component = get_pick[:pick_component]
					pick_face = get_pick[:pick_face]
					mod = Sketchup.active_model
					mod.start_operation(OB[:sheet_options], true)
					part =
						AmtfSheet.new(
							pick_component,
							get_pick[:trans],
							get_pick[:parent_matrix]
						)
					edge_faces = part.edge_face
					if @is_no_hole_sheet == true
						pick_component.set_attribute(
							'amtf_dict',
							'comp_type',
							'no_hole'
						)
					else
						pick_component.delete_attribute(
							'amtf_dict',
							'comp_type',
							'no_hole'
						)
					end
					if @is_no_edge_banding == true
						pick_component.set_attribute(
							'amtf_dict',
							'no_auto_edge_banding',
							@is_no_edge_banding
						)
						edge_faces.each do |f|
							f.set_attribute 'amtf_dict', 'edge_banding', 'false'
							f.material = '#333333'
							f.delete_attribute 'amtf_dict', 'edge_banding_type'
						end
					else
						if pick_component.get_attribute(
								'amtf_dict',
								'no_auto_edge_banding'
						   ) == true
							pick_component.delete_attribute(
								'amtf_dict',
								'no_auto_edge_banding'
							)
							edge_faces.each do |f|
								f.delete_attribute 'amtf_dict', 'edge_banding'
								f.delete_attribute 'amtf_dict', 'edge_type_id'
								f.delete_attribute 'amtf_dict', 'edge_type_name'
								f.delete_attribute 'amtf_dict',
								                   'edge_type_thick'
								f.material = pick_component.material
							end
						end
					end
					if @is_door_sheet == true
						pick_component.set_attribute 'amtf_dict',
						                             'comp_type_door',
						                             'door'
						tao_cline(
							@mouse_ip.position,
							pick_component,
							pick_face,
							get_pick[:trans]
						)
					else
						pick_component.delete_attribute 'amtf_dict',
						                                'comp_type_door'
						delete_cline(pick_component)
					end
					if @groove_type_id
						pick_component.set_attribute 'amtf_dict',
						                             'comp_type_back',
						                             'back'
						groove_setting =
							Utilities.get_setting(
								'groove',
								@config_data,
								@groove_type_id
							)
						pick_component.set_attribute 'amtf_dict',
						                             'groove_type_id',
						                             @groove_type_id
						pick_component.set_attribute 'amtf_dict',
						                             'groove_name',
						                             groove_setting['name']
						pick_component.set_attribute 'amtf_dict',
						                             'extra_grooves',
						                             groove_setting[
								'extra_grooves'
						                             ]
						pick_component.set_attribute 'amtf_dict',
						                             'extra_grooves_width',
						                             groove_setting[
								'extra_grooves_width'
						                             ]
						pick_component.set_attribute 'amtf_dict',
						                             'groove_depth',
						                             groove_setting[
								'groove_depth'
						                             ]
						pick_component.set_attribute 'amtf_dict',
						                             'groove_color',
						                             groove_setting['color']
					else
						pick_component.delete_attribute 'amtf_dict',
						                                'comp_type_back'
						pick_component.delete_attribute 'amtf_dict',
						                                'groove_type_id'
						pick_component.delete_attribute 'amtf_dict',
						                                'groove_name'
						pick_component.delete_attribute 'amtf_dict',
						                                'extra_grooves_width'
						pick_component.delete_attribute 'amtf_dict',
						                                'extra_grooves'
						pick_component.delete_attribute 'amtf_dict',
						                                'groove_depth'
						pick_component.delete_attribute 'amtf_dict',
						                                'groove_color'
					end
					if @sheet_grain_angle
						pick_component.set_attribute 'amtf_dict',
						                             'custom_grain',
						                             @sheet_grain_angle
					else
						pick_component.delete_attribute 'amtf_dict',
						                                'custom_grain'
					end
					subGroup =
						Utilities
							.definition(part.comp)
							.entities
							.grep(Sketchup::Group)
					if subGroup.size > 0
						for i in 0..subGroup.size - 1
							group_type =
								subGroup[i].get_attribute('amtf_dict', 'type')
							if group_type == 'bukong_label'
								subGroup[i].erase!
								break
							end
						end
					end
					part.drawArrow(pick_face)
					frame_data = pick_frame_data
					if !frame_data[:frame].nil?
						sheet_id =
							pick_component.get_attribute(
								'amtf_dict',
								'sheet_id'
							)
						frame = frame_data[:frame]
						if sheet_id
							sheet_attrs = {}
							dict =
								pick_component.attribute_dictionary 'amtf_dict'
							if dict
								dict.each do |key, value|
									sheet_attrs[key] = value
								end
							end
							frame.update_attributes(sheet_id, sheet_attrs)
						end
					end
					mod.commit_operation
				end
				def delete_cline(pick_component)
					subGroup =
						Utilities
							.definition(pick_component)
							.entities
							.grep(Sketchup::Group)
					if subGroup.size > 0
						for i in 0..subGroup.size - 1
							group_type =
								subGroup[i].get_attribute('amtf_dict', 'type')
							if group_type == 'bukong_door_cline'
								if subGroup[i] && subGroup[i].valid?
									subGroup[i].erase!
								end
							end
						end
					end
				end
				def tao_cline(point, pick_component, pick_face, trans)
					delete_cline(pick_component)
					# Xác định cạnh gần điểm click chuột
					# Xác định cạnh đối diện
					# Xác định trung điểm cạnh gần
					# V�?hai cái đường Cline tới hai đầu mút cạnh đối diện
					bigFaceArr = Utilities.get_big_faces(pick_component, trans)
					return unless bigFaceArr.include?(pick_face)
					point =
						Utilities.point_context_convert(point, IDENTITY, trans)
					boundingCorner =
						Utilities.amtfBoundingBox(pick_component, trans)
					corners = []
					for i in 0..7
						cn = boundingCorner[i]
						corners << cn if cn.on_plane?(pick_face.plane)
					end
					corners = Utilities.get_sorted_points_ccw(corners)
					return if corners.size == 0
					corners.push(corners[0])
					nearest_line = [
						corners[0],
						corners[0].vector_to(corners[1])
					]
					kc_edge = point.distance_to_line(nearest_line)
					nearest_vertices = [corners[0], corners[1]]
					for i in 1..corners.size - 2
						line = [
							corners[i],
							corners[i].vector_to(corners[i + 1])
						]
						khoang_cach = point.distance_to_line(line)
						if khoang_cach < kc_edge
							kc_edge = khoang_cach
							nearest_line = line
							nearest_vertices = [corners[i], corners[i + 1]]
						end
					end
					center = Utilities.find_center_of_points(corners)
					dist_to_center = point.distance(center)
					cline_grp =
						Utilities.definition(pick_component).entities.add_group
					cline_grp.set_attribute(
						'amtf_dict',
						'type',
						'bukong_door_cline'
					)
					if kc_edge < dist_to_center
						dau_mut = []
						for k in 0..corners.size - 1
							co = corners[k]
							if !Utilities.point_compare?(
									co,
									nearest_vertices[0]
							   ) &&
									!Utilities.point_compare?(
										co,
										nearest_vertices[1]
									)
								dau_mut << co
							end
						end
						dau_mut = Utilities.uniq_points(dau_mut)
						trung_diem =
							Utilities.get_midpoint(
								nearest_vertices[0],
								nearest_vertices[1]
							)
						return if dau_mut.size < 2
						cline_grp.entities.add_cline(trung_diem, dau_mut[0])
						cline_grp.entities.add_cline(trung_diem, dau_mut[1])
					else
						cline_grp.entities.add_cline(corners[0], corners[2])
						cline_grp.entities.add_cline(corners[1], corners[3])
					end
				end
			end #Class
		end #  OffsetToolModule
	end # BuKong
end #