# encoding: UTF-8
module AMTF
	module BUKONG
		module CustomHoleTool
			class CustomHole < Tools::Base
				def activate
					@@active_tool_class = self.class
					@input = Sketchup::InputPoint.new
					@pointmove = nil
					@last_comp = nil
					@lock_color = 'black'
					@step = 0
					@points_draw = []
					Sketchup.status_text = OB[:select_start_point]
				end
				def deactivate(view)
					@pointmove = nil
					@last_comp = nil
					@step = 0
					@@active_tool_class = nil
					@points_draw = []
					view.lock_inference
					view.invalidate
				end
				def resume(view)
					view.invalidate
				end
				def onCancel(reason, view)
					view.lock_inference
					view.invalidate
				end
				def draw(view)
					view.drawing_color = @lock_color
					if !@pointmove.nil?
						# V�?điểm tại chuột
						status = view.draw_points @pointmove, 10, 2, @lock_color
					end
					if @points_draw.size > 1
						view.line_width = 1
						for i in 0..@points_draw.size - 2
							view.draw_lines @points_draw[i], @points_draw[i + 1]
						end
					end
					if !@pointmove.nil? && @points_draw.size > 0
						view.line_width = 1
						view.draw_lines @points_draw.last, @pointmove
					end
				end
				def onMouseMove(flags, x, y, view)
					@input.pick view, x, y
					@pointmove = @input.position
					view.tooltip = @input.tooltip
					view.invalidate
				end
				def onLButtonDown(flags, x, y, view)
					@input.pick view, x, y
					get_pick = pick_face_or_edge(@ph, x, y)
					return if get_pick.empty?
					pick_component = get_pick[:pick_component]
					return unless pick_component
					unless pick_component.is_a?(Sketchup::Group) ||
							pick_component.is_a?(Sketchup::ComponentInstance)
						return
					end
					return unless Utilities.solid?(pick_component)
					pick_face = get_pick[:pick_face]
					return unless pick_face
					if @step == 0
						@last_comp = pick_component
						@trans = get_pick[:trans]
						@pick_face = pick_face
						@step += 1
						selection = Sketchup.active_model.selection
						selection.clear
						selection.add(pick_component)
						@points_draw << @input.position
					elsif @step == 1
						return unless @last_comp == pick_component
						@points_draw << @input.position
						draw_a_new_custom_path
						@pointmove = nil
						@last_comp = nil
						@points_draw = []
						@step = 0
					end
					view.lock_inference
					view.invalidate
				end
				def draw_a_new_custom_path
					config_data = {}
					while config_data.empty?
						prompts = [
							OB[:name],
							OB[:diameter],
							OB[:quantity],
							OB[:d_tool],
							OB[:d_depth],
							OB[:d_passes]
						]
						defaults = [
							FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:name],
							FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:diameter],
							FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:quantity],
							FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:d_tool],
							FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:d_depth],
							FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:d_passes]
						]
						inputs = UI.inputbox(prompts, defaults, OB[:make_hole])
						if !inputs
							Sketchup.active_model.tools.pop_tool
							@points_draw = []
							@step = 0
							return
						end
						hole_name = inputs[0].to_s
						diameter = inputs[1].to_s
						hole_qty = inputs[2].to_s
						d_tool = inputs[3].to_s
						d_depth = inputs[4].to_s
						d_passes = inputs[5].to_s
						FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:name] = hole_name
						FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:diameter] = diameter
						FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:quantity] = hole_qty
						FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:d_tool] = d_tool
						FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:d_depth] = d_depth
						FUJI_GLOBAL_CUSTOMHOLE_VARIABLE[:d_passes] = d_passes
						check = true
						check = false if !Utilities.isNumber?(diameter)
						check = false if !Utilities.isNumber?(hole_qty)
						check = false if !Utilities.isNumber?(d_tool)
						check = false if !Utilities.isNumber?(d_depth)
						check = false if !Utilities.isNumber?(d_passes)
						check = false if hole_name.empty?
						if check
							config_data = {
								name: hole_name,
								d_tool: d_tool,
								d_depth: d_depth,
								d_passes: d_passes,
								d_type: '3',
								diameter: diameter,
								hole_qty: hole_qty
							}
						end
					end
					return if config_data.empty?
					return if @points_draw.size != 2
					holes =
						hole_locations(
							@points_draw[0],
							@points_draw[1],
							config_data[:hole_qty]
						)
					radius = 0.5 * config_data[:diameter].to_l
					minifixID = Utilities.CreatUniqueid
					Sketchup.active_model.start_operation(OB[:make_hole], true)
					holes.each do |p|
						height_1 = (config_data[:d_depth].to_f + 2).to_s.to_l
						move_1 = (config_data[:d_depth].to_f + 1).to_s.to_l
						chot_cam =
							CircleModel.new(
								@last_comp,
								p[:chot_cam],
								radius,
								@pick_face.normal,
								height_1,
								move_1,
								@trans
							)
						chot_cam.drawModel
						chot_cam.set_attribute('amtf_dict', 'type', 'khoan_moi')
						chot_cam.set_attribute(
							'amtf_dict',
							'minifixID',
							minifixID
						)
						chot_cam.set_attribute('amtf_dict', 'radius', radius)
						chot_cam.set_attribute(
							'amtf_dict',
							'config_data',
							config_data.to_json
						)
					end
					Sketchup.active_model.commit_operation
				end
				CURSOR_PENCIL = 632
				def onSetCursor
					UI.set_cursor(CURSOR_PENCIL)
				end
				def onKeyUp(key, repeat, flags, view)
					@lock_color = 'black'
					view.lock_inference
					view.invalidate
				end
				def onKeyDown(key, repeat, flags, view)
					return if @points_draw.empty?
					if key == VK_LEFT
						#y-axis
						lock_line = [@points_draw.last, [0, 1, 0]]
						@lock_color = 'green'
					elsif key == VK_RIGHT
						lock_line = [@points_draw.last, [1, 0, 0]]
						@lock_color = 'red'
					elsif key == VK_UP
						lock_line = [@points_draw.last, [0, 0, 1]]
						@lock_color = 'blue'
					else
						return
					end
					lock_point =
						Utilities.find_point_online_with_distance(
							@points_draw.last,
							lock_line[1],
							1
						)
					view.lock_inference(
						Sketchup::InputPoint.new(@points_draw.last),
						Sketchup::InputPoint.new(lock_point)
					)
					view.invalidate
				end
				def hole_locations(point_1, point_2, qty)
					n = qty.to_i
					distance = point_1.distance(point_2)
					tpoint = []
					tpoint[0] = { chot_cam: point_1 }
					tpoint[n - 1] = { chot_cam: point_2 }
					if n > 2
						len = distance / (n - 1)
						for i in 1..n - 2
							tpi =
								Utilities.find_point_between_two_points(
									tpoint[i - 1][:chot_cam],
									point_2,
									len
								)
							tpoint[i] = { chot_cam: tpi }
						end
					end
					return tpoint
				end
			end #Class
		end #  CustomPathTool
	end # BuKong
end #