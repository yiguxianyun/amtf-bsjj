# encoding: UTF-8
module AMTF
module BUKONG      
module RemoveCurveModule 
    class RemoveCurveTool < Tools::Base   
        def activate  
            @@active_tool_class = self.class                           
            Sketchup.status_text = OB[:select_target_object]
          end
          def deactivate(view)         
            @@active_tool_class =nil
            view.lock_inference
            view.invalidate
          end 
          def reset_tool       
            Sketchup.status_text = OB[:select_target_object]
          end
          def resume(view)
            view.invalidate
          end    
          def onCancel(reason, view)
            reset_tool
            view.lock_inference
            view.invalidate
          end  
          def onLButtonDown flags, x, y, view
              @ph.do_pick(x, y)
              get_pick = pick_me(@ph)
              return if get_pick.nil?
              pick_component = get_pick[:pick_component]
              pick_face = get_pick[:pick_face]
              pick_edge = get_pick[:pick_edge]
              selection = Sketchup.active_model.selection                                    
              pick_componentTrans = pick_component.transformation
              pick_component.parent.entities.each do
                |item|
                item.make_unique if item.is_a?(Sketchup::Group)
              end
              selection.clear
              selection.add pick_component  
              model = Sketchup.active_model
              model.start_operation("Delete", true)  
              pick_component.entities.each do 
                  |group|
                  next unless group.is_a?(Sketchup::Group)
                  group_type = Utilities.get_comp_attribute(group, 'type', 'amtf_dict')
                  group.erase! if group_type == 'curve_groove'
              end
              model.commit_operation
              reset_tool
              view.lock_inference 
              view.invalidate    
          end  
     end# RemoveTool
end
end
end