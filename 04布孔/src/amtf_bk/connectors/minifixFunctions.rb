# encoding: UTF-8
module AMTF
	module BUKONG
		class MinifixFunctions < ConnectorFunctions
			include AMTF_bk_mixin
			def create_connector(data)
				# 接触大面法向: di chuyển ốc cam vào trong tấm
				# 凸轮孔放置面法向: di chuyển chốt cam vào giữa tấm
				# move_vect3: di chuyển theo chiều đầu đến cuối
				ptr = data[:ptr]
				面板=data[:faceSheet]
				# puts "data👉 #{data}"
				# puts "面板.comp.name => #{面板.comp.name}"
				边板=data[:edgeSheet]
				# puts "边板.comp.name => #{边板.comp.name}"
				# puts "变换前接触大面法向 => #{data[:bigFace].normal}"
				# 从局部坐标系变换到世界坐标系
				接触大面法向 =Utilities.vector_context_convert(
					data[:bigFace].normal,
					面板.trans,
					IDENTITY
				).normalize
				# puts "变换后接触大面法向 => #{接触大面法向}"
				# sel = Sketchup.active_model.selection
				# sel.add data[:bigFace]
				# amtf.nil

				凸轮孔放置面法向 =Utilities.vector_context_convert(
						data[:edgeFace].normal,
						边板.trans,
						IDENTITY
					).normalize
				dVector =Utilities.vector_multiply(
						@config_data['distance_h'].to_s.to_l,
						凸轮孔放置面法向.reverse
					)
				# puts "凸轮孔放置面法向 => #{凸轮孔放置面法向}"
				# puts "dVector => #{dVector}"
				chot_cam_translate = Geom::Transformation.translation dVector
				center_chotcam = ptr[:chot_cam].transform(chot_cam_translate)
				center_chotgo1 = ptr[:chot_go1].transform(chot_cam_translate)
				center_chotgo2 = ptr[:chot_go2].transform(chot_cam_translate)
				凸轮中心距边缘=@config_data['distance_l'].to_s.to_l
				凸轮孔中心 =ptr[:chot_cam].transform(Utilities.vector_multiply(
							凸轮中心距边缘,
							接触大面法向
						)
					)
				# Kiểm tra Overlap va ra ngoai
				overlap_threshold = 0.5 * @config_data['diameter'].to_s.to_l
				unless FittingUtils.not_overlap?(凸轮孔中心,@existingHoleEdgeComp,
						overlap_threshold,
						data[:edgeFace],
						边板.trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.not_overlap?(center_chotcam,@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						面板.trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				move_vect3 = data[:line][0].vector_to(data[:line][1]).normalize
				if FittingUtils.out_of_face?(凸轮孔中心,data[:edgeFace],move_vect3,
						0.5 * @config_data['diameter'].to_s.to_l,
						边板.trans
				   )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(center_chotcam,data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['diameter'].to_s.to_l,
						面板.trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				# Ốc cam
				bankinh_r = 0.5 * @config_data['diameter'].to_s.to_l
				height_4 = @config_data['凸轮孔深度'].to_s.to_l
				# puts "height_4"
				# puts height_4
				# move_4 = (@config_data['distance_h'].to_f - 1).to_s.to_l
				move_4 = 25.mm 
				puts "move_4"
				puts move_4
				oc_cam =CircleModel.new(边板.comp,凸轮孔中心,bankinh_r,data[:edgeFace].normal,
						height_4,
						move_4,
						边板.trans
					)
				连接件孔名称="#{@config_data["类型"]}_#{@config_data["规格"]}"

				连接件孔=oc_cam.drawModelView(连接件孔名称)
				oc_cam.set_attribute('amtf_dict', 'type', 'oc_cam')
				oc_cam.set_attribute('amtf_dict', 'minifixID', data[:minifixID])
				# oc_cam.set_attribute(
				# 	'amtf_dict',
				# 	'connector_type_id',
				# 	@connector_id
				# )
				oc_cam.set_attribute('amtf_dict', 'radius', bankinh_r)
				oc_cam.set_attribute('amtf_dict','config_data',@config_data.to_json)
				# puts "@config_data.to_json"
				# puts @config_data.to_json
				oc_cam.set_attribute('amtf_dict','pairs',"#{@e1_id}~#{@e2_id}")
				oc_cam.set_attribute('amtf_dict','persit_pairs',
					"#{面板.comp.persistent_id}~#{边板.comp.persistent_id}"
				)
				# amtf.nil
				bankinh_r1 = 0.5 * @config_data['diameter_1'].to_s.to_l
				bankinh_r2 = 0.5 * @config_data['diameter_2'].to_s.to_l
				螺杆盲孔深=@config_data['螺杆盲孔深'].mm
				height_1 = (@config_data['distance_l'].to_f + 10).to_s.to_l
				move_1 = 10.mm

				# 螺杆侧孔 =CircleModel.new(边板.comp,center_chotcam,
				# 		bankinh_r1,
				# 		data[:bigFace].normal.reverse,
				# 		凸轮中心距边缘,
				# 		move_1,
				# 		边板.trans
				# 	)
				# 螺杆侧孔.drawModelView("螺杆侧孔")
				# amtf.nil
				# # 传入的参数会被子函数修改！
				# 螺杆侧孔 =CircleModel.new(连接件孔,center_chotcam,
				# 	bankinh_r1,
				# 	data[:bigFace].normal.reverse,
				# 	凸轮中心距边缘,
				# 	move_1,
				# 	边板.comp.transformation
				# )
				# 螺杆侧孔.添加圆柱()
				添加圆柱(边板.comp,连接件孔,center_chotcam,bankinh_r1,data[:bigFace].normal.reverse,凸轮中心距边缘)

				chot_cam =CircleModel.new(面板.comp,center_chotcam,
						bankinh_r1,
						data[:bigFace].normal,
						螺杆盲孔深,
						move_1,
						面板.trans
					)
				chot_cam.drawModelView("螺杆盲孔")
				# chot_cam.drawModel
				chot_cam.set_attribute('amtf_dict', 'type', 'chot_cam')
				chot_cam.set_attribute('amtf_dict','minifixID',data[:minifixID])
				chot_cam.set_attribute('amtf_dict', 'radius', bankinh_r1)
				chot_cam.set_attribute('amtf_dict','config_data',@config_data.to_json)
				chot_cam.set_attribute('amtf_dict','pairs',"#{@e1_id}~#{@e2_id}")
				chot_cam.set_attribute('amtf_dict',	'persit_pairs',
					"#{面板.comp.persistent_id}~#{边板.comp.persistent_id}"
				)

				# Side drill
				# edge_and_face = data[:edge_and_face]
				# chot_cam_side =
				# 	CircleModel.new(
				# 		边板.comp,
				# 		center_chotcam,
				# 		bankinh_r1,
				# 		edge_and_face.bendingFace.keys[0].normal,
				# 		height_1,
				# 		move_1,
				# 		边板.trans
				# 	)
				# chot_cam_side.drawSideModel
				# chot_cam_side.set_attribute(
				# 	'amtf_dict',
				# 	'type',
				# 	'chot_cam_side'
				# )
				# chot_cam_side.set_attribute(
				# 	'amtf_dict',
				# 	'minifixID',
				# 	data[:minifixID]
				# )
				# chot_cam_side.set_attribute('amtf_dict', 'radius', bankinh_r1)
				# chot_cam_side.set_attribute(
				# 	'amtf_dict',
				# 	'config_data',
				# 	@config_data.to_json
				# )
				# chot_cam_side.set_attribute(
				# 	'amtf_dict',
				# 	'pairs',
				# 	"#{@e1_id}~#{@e2_id}"
				# )
				if FittingUtils.not_overlap?(center_chotgo1,@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						面板.trans
				   )
					if FittingUtils.chot_cam_not_out_of_face?(center_chotgo1,data[:bigFace],
							data[:line][0],
							data[:line][1],
							0.5 * @config_data['diameter'].to_s.to_l,
							面板.trans
					   )
						# Chốt g�?1
						bankinh_r3 = 0.5 * @config_data['diameter_3'].to_s.to_l
						if bankinh_r3 != 0
							height_2 = 20.mm
							move_2 = 10.mm
							chot_go1 =
								CircleModel.new(
									面板.comp,
									center_chotgo1,
									bankinh_r3,
									data[:bigFace].normal,
									height_2,
									move_2,
									面板.trans
								)
							chot_go1.drawModel
							chot_go1.set_attribute(
								'amtf_dict',
								'type',
								'chot_go3'
							)
							chot_go1.set_attribute(
								'amtf_dict',
								'minifixID',
								data[:minifixID]
							)
							# chot_go1.set_attribute(
							# 	'amtf_dict',
							# 	'connector_type_id',
							# 	@connector_id
							# )
							chot_go1.set_attribute(
								'amtf_dict',
								'radius',
								bankinh_r3
							)
							chot_go1.set_attribute(
								'amtf_dict',
								'config_data',
								@config_data.to_json
							)
							chot_go1.set_attribute(
								'amtf_dict',
								'pairs',
								"#{@e1_id}~#{@e2_id}"
							)
							chot_go1.set_attribute(
								'amtf_dict',
								'persit_pairs',
								"#{面板.comp.persistent_id}~#{边板.comp.persistent_id}"
							)
						end
					end
				end
				if FittingUtils.not_overlap?(center_chotgo2,@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						面板.trans
				   )
					if FittingUtils.chot_cam_not_out_of_face?(center_chotgo2,data[:bigFace],
							data[:line][0],
							data[:line][1],
							0.5 * @config_data['diameter'].to_s.to_l,
							面板.trans
					   )
						# Chốt g�?2
						bankinh_r2 = 0.5 * @config_data['diameter_2'].to_s.to_l
						if bankinh_r2 != 0
							height_3 = 20.mm
							move_3 = 10.mm
							chot_go2 =
								CircleModel.new(
									面板.comp,
									center_chotgo2,
									bankinh_r2,
									data[:bigFace].normal,
									height_3,
									move_3,
									面板.trans
								)
							chot_go2.drawModel
							chot_go2.set_attribute('amtf_dict','type','chot_go2')
							chot_go2.set_attribute('amtf_dict','minifixID',data[:minifixID])
							# chot_go2.set_attribute(
							# 	'amtf_dict',
							# 	'connector_type_id',
							# 	@connector_id
							# )
							chot_go2.set_attribute('amtf_dict','radius',bankinh_r2)
							chot_go2.set_attribute(
								'amtf_dict',
								'config_data',
								@config_data.to_json
							)
							chot_go2.set_attribute(
								'amtf_dict',
								'pairs',
								"#{@e1_id}~#{@e2_id}"
							)
							chot_go2.set_attribute(
								'amtf_dict',
								'persit_pairs',
								"#{面板.comp.persistent_id}~#{边板.comp.persistent_id}"
							)
						end
					end
				end
			end
		end #Class
	end
end