# encoding: UTF-8
module AMTF
	module BUKONG
		module FittingUtils
			def self.not_overlap?(p, points, threshold, face, tr)
				return true if points.size == 0
				p1 = Utilities.point_context_convert(p, IDENTITY, tr)
				ok = true
				points.each do |pt|
					ok = false if (
						p1.distance(pt[:hole]).to_s.to_l < threshold
					) && Utilities.within_face?(pt[:hole], face)
				end
				return ok
			end
			def self.out_of_face?(p, face, d, threshold, tr)
				p1 = Utilities.point_context_convert(p, IDENTITY, tr)
				direction = Utilities.vector_context_convert(d, IDENTITY, tr)
				reverse = direction.reverse.normalize
				reverse = Utilities.vector_multiply(threshold.to_l, reverse)
				trans = Geom::Transformation.translation(reverse)
				p1.transform!(reverse)
				return !Utilities.within_face?(p1, face)
			end
			def self.chot_cam_not_out_of_face?(
				ptr,
				face,
				ptr0,
				ptr1,
				threshold,
				tr,
				edgeComp = nil
			)
				p = Utilities.point_context_convert(ptr, IDENTITY, tr)
				p0 = Utilities.point_context_convert(ptr0, IDENTITY, tr)
				p1 = Utilities.point_context_convert(ptr1, IDENTITY, tr)
				direction = p0.vector_to(p1)
				direction = p1.vector_to(p0) if p.distance(p1) > p.distance(p0)
				direction = direction.normalize
				direction = Utilities.vector_multiply(threshold.to_l, direction)
				trans = Geom::Transformation.translation(direction)
				p2 = p.transform(direction)
				return Utilities.within_face?(p2, face)
			end
			def self.get_vector_for_hole(p, p1, p2)
				if p.distance(p1) < p.distance(p2)
					return p1.vector_to(p2).normalize
				else
					return p2.vector_to(p1).normalize
				end
			end
			def self.project_oposit(point, sheet, opo_face)
				pt =
					Utilities.point_context_convert(
						point,
						IDENTITY,
						sheet.trans
					)
				pt = pt.project_to_plane(opo_face.plane)
				pt = Utilities.point_context_convert(pt, sheet.trans, IDENTITY)
				return pt
			end
			def self.check_pair(sheet1, sheet2, hash, part_frame, part1_frame)
				return false unless part_frame
				return false unless part1_frame
				return false unless part1_frame == part_frame
				return false unless hash && !hash.empty?
				return false unless hash.key?(part_frame)
				id1 = sheet1.sheet_id
				return false unless id1
				id2 = sheet2.sheet_id
				return false unless id2
				key1 = "#{id1}~#{id2}"
				key2 = "#{id2}~#{id1}"
				return(
					hash[part_frame].key?(key1) || hash[part_frame].key?(key2)
				)
			end
			def self.get_default_grooove(config)
				return {} unless config['groove']
				id = nil
				first_id = nil
				i = 0
				found = false
				config['groove'].each do |obj|
					id = obj['id']
					first_id = obj['id'] if i == 0
					i += 1
					obj['params1'].each do |item|
						if item['key'] == 'default' &&
								(
									item['value'] == 'true' ||
										item['value'] == true
								)
							found = true
							break
						end
					end
					obj['params2'].each do |item|
						if item['key'] == 'default' &&
								(
									item['value'] == 'true' ||
										item['value'] == true
								)
							found = true
							break
						end
					end
					break if found
				end
				return {} if id.nil? && first_id.nil?
				if !id.nil?
					hash = Utilities.get_setting('groove', config, id)
					hash['id'] = id
					return hash
				elsif !first_id.nil?
					hash = Utilities.get_setting('groove', config, first_id)
					hash['id'] = first_id
					return hash
				end
			end
		end
	end
end