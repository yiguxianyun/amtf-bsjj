# encoding: UTF-8
module AMTF
	module BUKONG
		class ChotdotFunctions < ConnectorFunctions
			def create_connector(data)
				# move_vect1: di chuyển ốc cam vào trong tấm
				# move_vect2: di chuyển chốt cam vào giữa tấm
				# move_vect3: di chuyển theo chiều đầu đến cuối
				ptr = data[:ptr]
				move_vect1 =
					Utilities.vector_context_convert(
						data[:bigFace].normal,
						data[:faceSheet].trans,
						IDENTITY
					).normalize
				move_vect2 =
					Utilities.vector_context_convert(
						data[:edgeFace].normal,
						data[:edgeSheet].trans,
						IDENTITY
					).normalize
				move_vect3 = data[:line][0].vector_to(data[:line][1]).normalize
				dVector =
					Utilities.vector_multiply(
						@config_data['distance_l2'].to_s.to_l,
						move_vect2
					)
				chot_cam_translate = Geom::Transformation.translation dVector
				center_chotcam = ptr[:chot_cam].transform(chot_cam_translate)
				center_chotcam_upper = center_chotcam.clone
				# Kiểm tra Overlap va ra ngoai
				overlap_threshold = 0.5 * @config_data['diameter'].to_s.to_l
				unless FittingUtils.not_overlap?(
						center_chotcam,
						@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						data[:faceSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(
						center_chotcam,
						data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['diameter'].to_s.to_l,
						data[:faceSheet].trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				bankinh_r = 0.5 * @config_data['diameter'].to_s.to_l
				height_1 = (@config_data['d_depth']).to_s.to_l
				move_1 = (@config_data['d_depth'].to_f - 1).to_s.to_l
				if @config_data['bellow_side_number'].to_i > 0
					for i in 0..@config_data['bellow_side_number'].to_i - 1
						if i > 0
							chot_cam_translate =
								Geom::Transformation.translation Utilities
										.vector_multiply(
										@config_data['distance_l1'].to_s.to_l,
										dVector.normalize
								                                 )
							center_chotcam.transform!(chot_cam_translate)
						end
						unless FittingUtils.not_overlap?(
								center_chotcam,
								@existingHoleFaceComp,
								overlap_threshold,
								data[:bigFace],
								data[:faceSheet].trans
						       )
							display_error(
								OB[:over_lap_connector],
								'alert-warning'
							)
							return
						end
						unless FittingUtils.chot_cam_not_out_of_face?(
								center_chotcam,
								data[:bigFace],
								data[:line][0],
								data[:line][1],
								0.5 * @config_data['diameter'].to_s.to_l,
								data[:faceSheet].trans
						       )
							display_error(OB[:out_of_face], 'alert-warning')
							return
						end
						chot_cam =
							CircleModel.new(
								data[:faceSheet].comp,
								center_chotcam,
								bankinh_r,
								data[:bigFace].normal,
								height_1,
								move_1,
								data[:faceSheet].trans
							)
						chot_cam.drawModel
						chot_cam.set_attribute('amtf_dict', 'type', 'chot_dot')
						chot_cam.set_attribute(
							'amtf_dict',
							'connector_type_id',
							@connector_id
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'minifixID',
							data[:minifixID]
						)
						chot_cam.set_attribute('amtf_dict', 'radius', bankinh_r)
						chot_cam.set_attribute(
							'amtf_dict',
							'config_data',
							@config_data.to_json
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'pairs',
							"#{@e1_id}~#{@e2_id}"
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'persit_pairs',
							"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
						)
					end
				end
				if @config_data['upper_side_number'].to_i > 0
					for i in 0..@config_data['upper_side_number'].to_i - 1
						# # Kiểm tra Overlap va ra ngoai
						chot_cam_translate =
							Geom::Transformation.translation Utilities
									.vector_multiply(
									@config_data['distance_l1'].to_s.to_l,
									move_vect2.reverse
							                                 )
						center_chotcam_upper.transform!(chot_cam_translate)
						overlap_threshold =
							0.5 * @config_data['diameter'].to_s.to_l
						unless FittingUtils.not_overlap?(
								center_chotcam_upper,
								@existingHoleFaceComp,
								overlap_threshold,
								data[:bigFace],
								data[:faceSheet].trans
						       )
							display_error(
								OB[:over_lap_connector],
								'alert-warning'
							)
							return
						end
						unless FittingUtils.chot_cam_not_out_of_face?(
								center_chotcam_upper,
								data[:bigFace],
								data[:line][0],
								data[:line][1],
								0.5 * @config_data['diameter'].to_s.to_l,
								data[:faceSheet].trans
						       )
							display_error(OB[:out_of_face], 'alert-warning')
							return
						end
						chot_cam =
							CircleModel.new(
								data[:faceSheet].comp,
								center_chotcam_upper,
								bankinh_r,
								data[:bigFace].normal,
								height_1,
								move_1,
								data[:faceSheet].trans
							)
						chot_cam.drawModel
						chot_cam.set_attribute('amtf_dict', 'type', 'chot_dot')
						chot_cam.set_attribute(
							'amtf_dict',
							'connector_type_id',
							@connector_id
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'minifixID',
							data[:minifixID]
						)
						chot_cam.set_attribute('amtf_dict', 'radius', bankinh_r)
						chot_cam.set_attribute(
							'amtf_dict',
							'config_data',
							@config_data.to_json
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'pairs',
							"#{@e1_id}~#{@e2_id}"
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'persit_pairs',
							"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
						)
					end
				end
			end
		end
	end
end