# encoding: UTF-8

module AMTF
	module BUKONG
		class CabineoFunctions < ConnectorFunctions
			def create_connector(data)
				# move_vect1: di chuyển ốc cam vào trong tấm
				# move_vect2: di chuyển chốt cam vào giữa tấm
				# move_vect3: di chuyển theo chiều đầu đến cuối
				# move_vect1：表示将螺钉卡入板中的移动向量。该向量确定了螺钉插入的方向和距离。
				# move_vect2：表示将凸缘销卡入板中心的移动向量。该向量确定了凸缘销插入的方向和距离。
				# move_vect3：表示沿着起点到终点方向的移动向量。该向量确定了沿着特定路径或线路的移动方向和距离。
				ptr = data[:ptr]
				move_vect1 =Utilities.vector_context_convert(
						data[:bigFace].normal,
						data[:faceSheet].trans,
						IDENTITY
					).normalize
				move_vect2 =Utilities.vector_context_convert(
						data[:edgeFace].normal,
						data[:edgeSheet].trans,
						IDENTITY
					).normalize
				move_vect3 = data[:line][0].vector_to(data[:line][1]).normalize
				dVector =Utilities.vector_multiply(
						@config_data['distance_l4'].to_s.to_l,
						move_vect2.reverse
					)
				chot_cam_translate = Geom::Transformation.translation dVector
				center_oc_cam1 =ptr[:chot_cam].transform(
						Utilities.vector_multiply(
							@config_data['distance_l1'].to_s.to_l,
							move_vect1
						)
					)
				center_oc_cam2 =center_oc_cam1.transform(
						Utilities.vector_multiply(
							@config_data['distance_l2'].to_s.to_l,
							move_vect1
						)
					)
				center_oc_cam3 =center_oc_cam2.transform(
						Utilities.vector_multiply(
							@config_data['distance_l3'].to_s.to_l,
							move_vect1
						)
					)
				center_chotcam = ptr[:chot_cam].transform(chot_cam_translate)
				# Kiểm tra Overlap va ra ngoai
				overlap_threshold = 0.5 * @config_data['diameter'].to_s.to_l
				unless FittingUtils.not_overlap?(
						center_oc_cam1,
						@existingHoleEdgeComp,
						overlap_threshold,
						data[:edgeFace],
						data[:edgeSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.not_overlap?(
						center_oc_cam2,
						@existingHoleEdgeComp,
						overlap_threshold,
						data[:edgeFace],
						data[:edgeSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.not_overlap?(
						center_oc_cam3,
						@existingHoleEdgeComp,
						overlap_threshold,
						data[:edgeFace],
						data[:edgeSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.not_overlap?(
						center_chotcam,
						@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						data[:faceSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				if FittingUtils.out_of_face?(
						center_oc_cam1,
						data[:edgeFace],
						move_vect3,
						0.5 * @config_data['diameter'].to_s.to_l,
						data[:edgeSheet].trans
				   )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(
						center_chotcam,
						data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['diameter'].to_s.to_l,
						data[:faceSheet].trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				# Ốc cam
				bankinh_r1 = 0.5 * @config_data['diameter_1'].to_s.to_l
				bankinh_r = 0.5 * @config_data['diameter'].to_s.to_l
				bankinh_r2 = 0.5 * @config_data['diameter_2'].to_s.to_l
				bankinh_r3 = 0.5 * @config_data['diameter_3'].to_s.to_l
				height_4 = @config_data['distance_l4'].to_s.to_l
				move_4 = (@config_data['distance_l4'].to_f - 1).to_s.to_l
				oc_cam1 =CircleModel.new(
						data[:edgeSheet].comp,
						center_oc_cam1,
						bankinh_r1,
						data[:edgeFace].normal,
						height_4,
						move_4,
						data[:edgeSheet].trans
					)
				oc_cam1.drawModel
				oc_cam1.set_attribute('amtf_dict', 'type', 'cabineo_d1')
				oc_cam1.set_attribute(
					'amtf_dict',
					'minifixID',
					data[:minifixID]
				)
				oc_cam1.set_attribute('amtf_dict', 'radius', bankinh_r1)
				oc_cam1.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				oc_cam1.set_attribute(
					'amtf_dict',
					'pairs',
					"#{@e1_id}~#{@e2_id}"
				)
				oc_cam1.set_attribute(
					'amtf_dict',
					'persit_pairs',
					"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
				)
				oc_cam2 =CircleModel.new(
						data[:edgeSheet].comp,
						center_oc_cam2,
						bankinh_r2,
						data[:edgeFace].normal,
						height_4,
						move_4,
						data[:edgeSheet].trans
					)
				oc_cam2.drawModel
				oc_cam2.set_attribute('amtf_dict', 'type', 'cabineo_d2')
				oc_cam2.set_attribute(
					'amtf_dict',
					'minifixID',
					data[:minifixID]
				)
				oc_cam2.set_attribute('amtf_dict', 'radius', bankinh_r2)
				oc_cam2.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				oc_cam2.set_attribute(
					'amtf_dict',
					'pairs',
					"#{@e1_id}~#{@e2_id}"
				)
				oc_cam2.set_attribute(
					'amtf_dict',
					'persit_pairs',
					"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
				)
				oc_cam3 =CircleModel.new(
						data[:edgeSheet].comp,
						center_oc_cam3,
						bankinh_r3,
						data[:edgeFace].normal,
						height_4,
						move_4,
						data[:edgeSheet].trans
					)
				oc_cam3.drawModel
				oc_cam3.set_attribute('amtf_dict', 'type', 'cabineo_d3')
				oc_cam3.set_attribute(
					'amtf_dict',
					'minifixID',
					data[:minifixID]
				)
				oc_cam3.set_attribute('amtf_dict', 'radius', bankinh_r3)
				oc_cam3.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				oc_cam3.set_attribute(
					'amtf_dict',
					'pairs',
					"#{@e1_id}~#{@e2_id}"
				)
				oc_cam3.set_attribute(
					'amtf_dict',
					'persit_pairs',
					"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
				)
				height_1 = (@config_data['distance_l4'].to_f + 10).to_s.to_l
				move_1 = 10.mm
				chot_cam =CircleModel.new(
						data[:faceSheet].comp,
						center_chotcam,
						bankinh_r,
						data[:bigFace].normal,
						height_1,
						move_1,
						data[:faceSheet].trans
					)
				chot_cam.drawModel
				chot_cam.set_attribute('amtf_dict', 'type', 'cabineo_d')
				chot_cam.set_attribute(
					'amtf_dict',
					'minifixID',
					data[:minifixID]
				)
				chot_cam.set_attribute(
					'amtf_dict',
					'pairs',
					"#{@e1_id}~#{@e2_id}"
				)
				chot_cam.set_attribute(
					'amtf_dict',
					'persit_pairs',
					"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
				)
				# chot_cam.set_attribute(
				# 	'amtf_dict',
				# 	'connector_type_id',
				# 	@connector_id
				# )
				chot_cam.set_attribute('amtf_dict', 'radius', bankinh_r)
				chot_cam.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				unless @config_data['cabineo_cap'] &&
						@config_data['cabineo_cap'].to_f > 0
					return
				end
				cap_config = {
					name: "#{@config_data['name']}-Cap",
					groove_depth: @config_data['cap_depth'],
					extra_grooves: 0,
					extra_grooves_width: 0,
					toolNumber: @config_data['cap_tool'],
					groove_type: 2,
					groove_passes: 1,
					diameter_1: @config_data['diameter_1'],
					diameter_2: @config_data['diameter_2'],
					diameter_3: @config_data['diameter_3'],
					distance_l1: @config_data['distance_l1'],
					distance_l2: @config_data['distance_l2'],
					distance_l3: @config_data['distance_l3'],
					cabineo_cap: @config_data['cabineo_cap']
				}
				cap_shiftY =
					0.5 * @config_data['diameter_1'].to_s.to_l -
						@config_data['distance_l1'].to_s.to_l
				cap_org =
					ptr[:chot_cam].transform(
						Utilities.vector_multiply(
							cap_shiftY,
							move_vect1.reverse
						)
					)
				cap_width =
					@config_data['diameter_1'].to_s.to_l +
						2 * @config_data['cabineo_cap'].to_s.to_l
				cap_height =
					0.5 * @config_data['diameter_1'].to_s.to_l +
						@config_data['distance_l2'].to_s.to_l +
						@config_data['distance_l3'].to_s.to_l +
						0.5 * @config_data['diameter_3'].to_s.to_l +
						@config_data['cabineo_cap'].to_s.to_l
				cap_radius =
					0.5 * @config_data['diameter_1'].to_s.to_l +
						@config_data['cabineo_cap'].to_s.to_l
				cover_cap =
					CabineoModel.new(
						{
							parent: data[:edgeSheet].comp,
							origin: cap_org,
							z_axis: data[:edgeFace].normal,
							y_axis:
								Utilities.vector_context_convert(
									data[:bigFace].normal,
									data[:faceSheet].trans,
									IDENTITY
								).normalize,
							trans: data[:edgeSheet].trans,
							width: cap_width,
							radius: cap_radius,
							height: cap_height,
							config_data: cap_config,
							minifixID: data[:minifixID],
							pairs: "#{@e1_id}~#{@e2_id}",
							persit_pairs:
								"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
						}
					)
				cover_cap.drawModel
			end
		end
	end

end