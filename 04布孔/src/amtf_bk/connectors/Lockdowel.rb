# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfLockdowel
			attr_accessor :center_1, :center_2, :center_3
			def initialize(params)
				@center_1 = params[:center_1]
				@center_2 = params[:center_2]
				@center_3 = params[:center_3]
				@radius = params[:radius]
				@minifixID = params[:minifixID]
				@type = params[:type]
				@connector_type = params[:connector_type]
				@type_id = params[:type_id]
				@partner_sheet_id = params[:partner_sheet_id]
				@trans = params[:trans]
				@height = 10.mm
				@move = 5.mm
				@parent = params[:parent]
				@config_data = params[:config_data]
				@z_axis =
					Utilities.vector_context_convert(
						params[:normal],
						@trans,
						IDENTITY
					)
				@pairs = params[:pairs]
				@persit_pairs = params[:persit_pairs]
			end
			def instance
				return @modal
			end
			def drawCircle
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.transform!(Geom::Transformation.new(@center_1, @z_axis))
				@center_1.transform!(@modal.transformation.inverse)
				@center_2.transform!(@modal.transformation.inverse)
				@center_3.transform!(@modal.transformation.inverse)
				@line_1 = @modal.entities.add_line @center_1, @center_2
				@line_1.set_attribute 'amtf_dict', 'type', 'lockdowel_line1'
				@line_2 = @modal.entities.add_line @center_1, @center_3
				@line_2.set_attribute 'amtf_dict', 'type', 'lockdowel_line2'
				@circle1 =
					@modal.entities.add_circle @center_1, [0, 0, 1], @radius
				face1 = @modal.entities.add_face(@circle1)
				@circle2 =
					@modal.entities.add_circle @center_2,
					                           [0, 0, 1],
					                           0.5 * @radius
				face2 = @modal.entities.add_face(@circle2)
				@circle3 =
					@modal.entities.add_circle @center_3, [0, 0, 1], @radius
				face3 = @modal.entities.add_face(@circle3)
				face1.set_attribute 'amtf_dict', 'type', 'point_1'
				face2.set_attribute 'amtf_dict', 'type', 'point_2'
				face3.set_attribute 'amtf_dict', 'type', 'point_3'
				@modal.transform!(@trans.inverse)
				new_layer =
					Sketchup.active_model.layers.add('Amtf_Connector_View')
				@modal.layer = new_layer
				@modal.set_attribute('amtf_dict', 'lockdowel_modal', true)
				@modal.set_attribute 'amtf_dict',
				                     'connector_type',
				                     @connector_type
				@modal.set_attribute 'amtf_dict', 'type', @type
				@modal.set_attribute 'amtf_dict', 'minifixID', @minifixID
				@modal.set_attribute 'amtf_dict', 'connector_type_id', @type_id
				@modal.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				@modal.set_attribute('amtf_dict', 'pairs', @pairs)
				@modal.set_attribute('amtf_dict', 'persit_pairs', @persit_pairs)
			end
			def drawCircle1
				@definition = Sketchup.active_model.definitions.add('Connector')
				@modal = @definition.entities.add_group
				@modal.transform!(Geom::Transformation.new(@center_1, @z_axis))
				@center_1.transform!(@modal.transformation.inverse)
				@center_2.transform!(@modal.transformation.inverse)
				@center_3.transform!(@modal.transformation.inverse)
				@line_1 = @modal.entities.add_line @center_1, @center_2
				@line_1.set_attribute 'amtf_dict', 'type', 'lockdowel_line1'
				@line_2 = @modal.entities.add_line @center_1, @center_3
				@line_2.set_attribute 'amtf_dict', 'type', 'lockdowel_line2'
				@cpoint1 = @modal.entities.add_cpoint @center_1
				@cpoint1.set_attribute 'amtf_dict', 'type', 'cpoint_1'
				@cpoint2 = @modal.entities.add_cpoint @center_2
				@cpoint2.set_attribute 'amtf_dict', 'type', 'cpoint_2'
				@cpoint3 = @modal.entities.add_cpoint @center_3
				@cpoint3.set_attribute 'amtf_dict', 'type', 'cpoint_3'
				@circle1 =
					@modal.entities.add_circle @center_1, [0, 0, 1], @radius
				@circle2 =
					@modal.entities.add_circle @center_2, [0, 0, 1], @radius
				face2 = @modal.entities.add_face(@circle2)
				face2.pushpull(@height)
				@circle3 =
					@modal.entities.add_circle @center_3, [0, 0, 1], @radius
				face3 = @modal.entities.add_face(@circle3)
				face3.pushpull(@height)
				@inst =
					Utilities
						.definition(@parent)
						.entities
						.add_instance(@definition, @trans.inverse)
				new_layer =
					Sketchup.active_model.layers.add('Amtf_miscellaneous')
				@inst.layer = new_layer
				@inst.set_attribute('amtf_dict', 'lockdowel_modal', true)
				@inst.set_attribute 'amtf_dict',
				                    'connector_type',
				                    @connector_type
				@inst.set_attribute 'amtf_dict', 'type', @type
				@inst.set_attribute 'amtf_dict', 'minifixID', @minifixID
				@inst.set_attribute 'amtf_dict', 'connector_type_id', @type_id
				@inst.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
			end
		end
	end
end