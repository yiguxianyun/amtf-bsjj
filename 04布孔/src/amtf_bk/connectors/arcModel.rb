# encoding: UTF-8
module AMTF
	module BUKONG
		class ArcModel
			attr_accessor :modal
			def initialize(data)
				@parent = data[:parent]
				@radius = data[:radius]
				@trans = data[:trans]
				@z_axis =
					Utilities.vector_context_convert(
						data[:z_axis],
						@trans,
						IDENTITY
					)
				@x_axis = data[:x_axis]
				@y_axis = @x_axis.cross(@z_axis)
				@origin = data[:origin]
				@start_angle = data[:start_angle]
				@end_angle = data[:end_angle]
			end
			def set_attribute(dict, key, val)
				return unless @modal
				@modal.set_attribute(dict, key, val)
			end
			def transform!(tr)
				@modal.transform!(tr)
			end
			def drawModel
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.transform!(
					Geom::Transformation.new(@origin, @x_axis, @y_axis)
				)
				arc =
					@modal.entities.add_arc(
						[0, 0, 0],
						[1, 0, 0],
						[0, 0, 1],
						@radius,
						@start_angle,
						@end_angle,
						50
					)
				@modal.transform!(@trans.inverse)
				# new_layer =
				# 	Sketchup.active_model.layers.add('Amtf_miscellaneous')
				# @modal.layer = new_layer
				# @modal.set_attribute('amtf_dict', 'minifix_modal', true)
			end
		end #Class
	end
end