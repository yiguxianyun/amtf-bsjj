# encoding: UTF-8
module AMTF
	module BUKONG
		class CabineoModel
			attr_accessor :origin, :elements
			def initialize(data)
				@parent = data[:parent]
				@origin = data[:origin]
				@pairs = data[:pairs]
				@persit_pairs = data[:persit_pairs]
				@z_axis =
					Utilities.vector_context_convert(
						data[:z_axis],
						data[:trans],
						IDENTITY
					)
				@y_axis = data[:y_axis]
				@x_axis = @y_axis.cross(@z_axis)
				@color = data[:color]
				@color = 'green' if !@color
				@trans = data[:trans]
				@config_data = data[:config_data]
				@minifixID = data[:minifixID]
				@polyline = [
					[
						{
							points: [
								Geom::Point2d.new(
									-0.5 * data[:width],
									data[:height] - data[:radius]
								),
								Geom::Point2d.new(-0.5 * data[:width], 0),
								Geom::Point2d.new(0.5 * data[:width], 0),
								Geom::Point2d.new(
									0.5 * data[:width],
									data[:height] - data[:radius]
								)
							],
							type: 'line',
							closed: false
						},
						{
							points: [
								Geom::Point2d.new(
									0.5 * data[:width],
									data[:height] - data[:radius]
								),
								Geom::Point2d.new(0, data[:height]),
								Geom::Point2d.new(
									-0.5 * data[:width],
									data[:height] - data[:radius]
								)
							],
							type: 'Arc3point',
							closed: false
						}
					]
				]
			end
			def drawModel
				@elements =
					AmtfPaths.new (
							{
								polyline: @polyline,
								origin: @origin,
								parent: @parent,
								z_axis: @z_axis,
								x_axis: @x_axis,
								color: @color,
								trans: @trans
							}
					              )
				@elements.drawModelEdge
				@elements.set_attribute('amtf_dict', 'cabineo_cap', true)
				@elements.set_attribute('amtf_dict', 'type', 'chanel')
				@elements.set_attribute('amtf_dict', 'minifixID', @minifixID)
				@elements.set_attribute(
					'amtf_dict',
					'groove_name',
					@config_data[:name]
				)
				@elements.set_attribute(
					'amtf_dict',
					'groove_depth',
					@config_data[:groove_depth]
				)
				@elements.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				@elements.set_attribute('amtf_dict', 'pairs', @pairs)
				@elements.set_attribute(
					'amtf_dict',
					'persit_pairs',
					@persit_pairs
				)
			end
		end
	end
end