# encoding: UTF-8
module AMTF
	module BUKONG
		class LockdowelFunctions < ConnectorFunctions
			def create_connector(data)
				# move_vect1: di chuyển ốc cam vào trong tấm
				# move_vect2: di chuyển chốt cam vào giữa tấm
				# move_vect3: di chuyển theo chiều đầu đến cuối
				ptr = data[:ptr]
				move_vect1 =
					Utilities.vector_context_convert(
						data[:bigFace].normal,
						data[:faceSheet].trans,
						IDENTITY
					).normalize
				move_vect2 =
					Utilities.vector_context_convert(
						data[:edgeFace].normal,
						data[:edgeSheet].trans,
						IDENTITY
					).normalize
				move_vect3 = data[:line][0].vector_to(data[:line][1]).normalize
				half_thick =
					0.5 *
						AmtfMath.thickness(
							data[:edgeSheet].comp,
							data[:edgeSheet].trans
						)
				dVector =
					Utilities.vector_multiply(half_thick, move_vect2.reverse)
				chot_cam_translate = Geom::Transformation.translation dVector
				# Kiểm tra Overlap va ra ngoai
				overlap_threshold = 0.5 * @config_data['diameter'].to_s.to_l
				center_chotcam_1 = ptr[:center1].transform(chot_cam_translate)
				center_chotcam_2 = ptr[:center2].transform(chot_cam_translate)
				center_chotcam_3 = ptr[:center3].transform(chot_cam_translate)
				ban_kinh_cam = 4.mm
				overlap_threshold = ban_kinh_cam
				unless FittingUtils.not_overlap?(
						center_chotcam_1,
						@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						data[:faceSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(
						center_chotcam_1,
						data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['diameter'].to_s.to_l,
						data[:faceSheet].trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				unless FittingUtils.not_overlap?(
						center_chotcam_2,
						@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						data[:faceSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(
						center_chotcam_2,
						data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['diameter'].to_s.to_l,
						data[:faceSheet].trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				unless FittingUtils.not_overlap?(
						center_chotcam_3,
						@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						data[:faceSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(
						center_chotcam_3,
						data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['diameter'].to_s.to_l,
						data[:faceSheet].trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				lockdowel =
					AmtfLockdowel.new(
						{
							center_1: center_chotcam_1,
							center_2: center_chotcam_2,
							center_3: center_chotcam_3,
							normal: data[:bigFace].normal,
							radius: 4.mm,
							minifixID: data[:minifixID],
							type: 'lockdowel',
							connector_type: @config_data['name'],
							connector_type_id: @connector_id,
							trans: data[:faceSheet].trans,
							parent: data[:faceSheet].comp,
							config_data: @config_data,
							pairs: "#{@e1_id}~#{@e2_id}",
							persit_pairs:
								"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
						}
					)
				lockdowel.drawCircle
			end
		end
	end
end