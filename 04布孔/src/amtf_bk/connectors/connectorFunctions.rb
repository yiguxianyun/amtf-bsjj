# encoding: UTF-8
module AMTF
	module BUKONG
		class ConnectorFunctions
			def initialize(data)
				@connector_type = data[:connector_type]
				@edge_and_face = data[:edge_and_face]
				@for_frame = data[:for_frame]
				@dialog = data[:dialog]
				@e1_id = data[:e1_id]
				@e2_id = data[:e2_id]
				@frame = data[:frame]
				@point_e2 = data[:point_e2]
				@view = data[:view]
				@connector_number = data[:connector_number]
				@connector_array = data[:connector_array]
				@intersect_lines = data[:intersect_lines]
				@config_data = data[:config_data]
				@existingHoleFaceComp = data[:existingHoleFaceComp]
				@existingHoleEdgeComp = data[:existingHoleEdgeComp]
				@line_direction = data[:line_direction]
				@mapping = data[:mapping]
				@minifix_list = data[:minifix_list]
				@sheet1 = data[:sheet1]
				@sheet2 = data[:sheet2]
			end

			def execute
				if @edge_and_face && @edge_and_face.intersect_lines
					if @for_frame == true && @frame
						puts "@for_frame == true && @frame"
						if @connector_array == true
							dir_vect = Geom::Vector3d.new(@line_direction)
							@edge_and_face.intersect_lines
								.each_with_index do |line, line_idx|
								line_vect = line[0].vector_to(line[1])
								line1 = line
								line1 = line1.reverse if dir_vect.dot(
									line_vect
								) < 0
								plastic_bighole_points =
									self.get_hole_locations(
										line1[0],
										line1[1],
										@config_data,
										@connector_number.to_i
									)
								self.create_multi_post_frame(
									plastic_bighole_points,
									line1,
									line_idx
								)
							end
						elsif @connector_array == false
							if @minifix_list
								@minifix_list.each do |minifixID, mini|
									line0 = @edge_and_face.intersect_lines[0]
									vect = line0[0].vector_to(line0[1])
									line_direction =
										Utilities.vector_context_convert(
											Geom::Vector3d.new(
												mini['line_direction']
											),
											@frame.transformation,
											IDENTITY
										)
									line0 =
										@edge_and_face.intersect_lines
											.last if vect.dot(line_direction) <
										0
									self.create_single_post_frame(
										line0,
										minifixID,
										mini,
										line_direction
									)
								end
							end
						end
					elsif @connector_array == false && !@auto
						puts "@connector_array == false && !@auto"
						self.create_single_post(
							@edge_and_face.intersect_lines[0]
						)
					else
						puts "execute  else"
						# model = Sketchup.active_model
						# entities = model.active_entities
						@edge_and_face.intersect_lines.each_with_index{ |line, line_idx|
							plastic_bighole_points =
								self.get_hole_locations(
									line[0],
									line[1],
									@config_data,
									@connector_number.to_i
								)
							self.create_multi_post(
								plastic_bighole_points,
								line,
								line_idx
							)
							# line = entities.add_line line[0],line[1] #画出接触边
						}
					end
				end
			end

			def get_hole_locations(p1, p2, config_data, enter_qty = 0)
				puts "@connector_type ↓"
				puts @connector_type
				case @connector_type
				# when 'connector'
				when '三合一'
					return(
						Utilities.get_cam_hole_locations(p1,p2,config_data,enter_qty)
					)
				when 'dogbone'
					return(
						Utilities.get_dogbone_hole_locations(p1,p2,config_data,enter_qty)
					)
				when 'lockdowel'
					enter_qty = 0
					enter_qty = @connector_number.to_i if @connector_number
					locParams = {
						distance_l1: @config_data['distance_l1'],
						distance_l2: @config_data['distance_l2'],
						distance_l: @config_data['distance_l'],
						p1: p1,
						p2: p2,
						enter_qty: enter_qty,
						distance_range: @config_data['distance_range']
					}
					return Utilities.get_lockdowel_locations(locParams)
				else
					return(
						Utilities.get_hole_locations(
							p1,
							p2,
							config_data,
							enter_qty
						)
					)
				end
			end

			def get_single_hole_locations(
					diem_dau,
					diem_cuoi,
					khoang_cach,
					config_data
				)
				ptr = nil
				# Xác định điểm đặt l�?
				plastic_ptr =
					Utilities.find_point_between_two_points(
						diem_dau,
						diem_cuoi,
						khoang_cach
					)
				move_vector3 = diem_dau.vector_to(diem_cuoi).normalize
				move_vector2 = diem_cuoi.vector_to(diem_dau).normalize
				puts "@connector_type👉#{@connector_type}"
				case @connector_type
				# when 'connector'
				when '三合一'
					distance_l2 = config_data['distance_l2']
					distance_l3 = config_data['distance_l3']
					move_vector2 =
						Utilities.vector_multiply(
							distance_l2.to_s.to_l,
							move_vector2
						)
					move_vector3 =
						Utilities.vector_multiply(
							distance_l3.to_s.to_l,
							move_vector3
						)
					tr2 = Geom::Transformation.translation move_vector2
					tr3 = Geom::Transformation.translation move_vector3
					ptr = {
						chot_cam: plastic_ptr,
						chot_go1: plastic_ptr.transform(tr2),
						chot_go2: plastic_ptr.transform(tr3)
					}
				when 'dogbone'
					distance_l =
						khoang_cach + 0.5 * config_data['distance_l1'].to_s.to_l
					distance_l3 =
						0.5 * config_data['distance_l1'].to_s.to_l +
							config_data['distance_l3'].to_s.to_l
					point_1 =
						Utilities.find_point_between_two_points(
							diem_dau,
							diem_cuoi,
							distance_l
						)
					ptr = {
						chot_cam: point_1,
						chot_go2:
							Utilities.find_point_between_two_points(
								point_1,
								diem_cuoi,
								distance_l3
							)
					}
				when 'lockdowel'
					center1 =
						Utilities.find_point_between_two_points(
							diem_dau,
							diem_cuoi,
							khoang_cach
						)
					distance_l1 = config_data['distance_l1']
					distance_l2 = config_data['distance_l2']
					tr1 =
						Geom::Transformation.new (
								Utilities.vector_multiply(
									distance_l1.to_s.to_l,
									move_vector3
								)
						                         )
					tr2 =
						Geom::Transformation.new (
								Utilities.vector_multiply(
									distance_l2.to_s.to_l,
									move_vector3
								)
						                         )
					center2 = center1.transform(tr2)
					center3 = center1.transform(tr1)
					ptr = {
						center1: center1,
						center2: center2,
						center3: center3
					}
				else
					ptr = { chot_cam: plastic_ptr }
				end
				return ptr
			end

			def create_multi_post(plastic_bighole_points, line, line_idx)
				ids = {}
				index_map = {}
				normal = Geom::Vector3d.new(0, 0, 0)
				puts "@e1_id  =→ #{@e1_id} " 

				if @e1_id && @e2_id
					if @frame && !@frame.nil?
						frame_trans = @frame.transformation
						normal =Utilities.vector_context_convert(
								@edge_and_face.edgeFace.normal,
								@edge_and_face.edgeSheet.trans,
								frame_trans
							)
					end
					remove_hole_list = []
					existing_holes =@existingHoleFaceComp.concat(@existingHoleEdgeComp)
					persistent_id_1 = @sheet1.comp.persistent_id.to_s
					persistent_id_2 = @sheet2.comp.persistent_id.to_s
					existing_holes.each do |mini|
						if mini && mini[:minifixID] && mini[:persit_pairs] &&
								mini[:persit_pairs].include?(persistent_id_1) &&
								mini[:persit_pairs].include?(persistent_id_2)
							puts "删除旧孔ing #{mini[:instance].name}"
							mini[:instance].erase! if mini[:instance].valid?
							remove_hole_list.push(mini[:minifixID])
						end
					end
					@existingHoleFaceComp =@existingHoleFaceComp.select { |x|
							!remove_hole_list.include? (x[:minifixID])
							# 如果x[:minifixID]不在remove_hole_list中，即不满足条件，则该元素会被保留下来。
						}
					@existingHoleEdgeComp =@existingHoleEdgeComp.select { |x|
							!remove_hole_list.include? (x[:minifixID])
						}
				end

				plastic_bighole_points.each_with_index do |ptr, p_index|
					minifixID = Utilities.CreatUniqueid
					puts "minifixID 👉 #{minifixID}" 
					# puts "ptr 👉 #{ptr}" 
					self.create_connector(
						{
							ptr: ptr,
							minifixID: minifixID,
							bigFace: @edge_and_face.bigFace,
							faceSheet: @edge_and_face.faceSheet,
							edgeFace: @edge_and_face.edgeFace,
							edgeSheet: @edge_and_face.edgeSheet,
							line: line,
							edge_and_face: @edge_and_face
						}
					)
					ids[minifixID] = { index: p_index, normal: normal.to_a }
					index_map[p_index.to_s] = minifixID
				end
				self.update_frame_multi_post(ids, index_map, line, line_idx)
			end

			def create_single_post(line)
				@helper = {}
				diem_dau = line[0]
				diem_cuoi = line[1]
				project_point =
					@point_e2.project_to_line(AmtfMath.get_line(line))
				if project_point.distance(line[0]) >
						project_point.distance(line[1])
					diem_dau = line[1]
					diem_cuoi = line[0]
				end
				dist = project_point.distance(diem_dau)
				vect =
					Utilities.vector_multiply(
						dist,
						project_point.vector_to(diem_dau).normalize
					)
				@helper[:point] = [@point_e2, @point_e2.offset(vect)]
				@view.refresh
				khoang_cach = 0
				while khoang_cach == 0
					promt_str =
						OB[:distance] + ' (' + Utilities.get_model_units + ')'
					prompts = [promt_str]
					defaults = ['0.0']
					inputs = UI.inputbox(prompts, defaults, OB[:distance])
					if !inputs
						@helper = nil
						return
					end
					enterNum = Utilities.parse_input(inputs)
					if enterNum
						khoang_cach = enterNum
						correct = true
					end
				end
				@helper = nil
				ptr =
					self.get_single_hole_locations(
						diem_dau,
						diem_cuoi,
						khoang_cach,
						@config_data
					)
				minifixID = Utilities.CreatUniqueid
				self.create_connector(
					{
						ptr: ptr,
						minifixID: minifixID,
						bigFace: @edge_and_face.bigFace,
						faceSheet: @edge_and_face.faceSheet,
						edgeFace: @edge_and_face.edgeFace,
						edgeSheet: @edge_and_face.edgeSheet,
						edge_and_face: @edge_and_face,
						line: line
					}
				)
				self.update_frame_single_post(
					minifixID,
					diem_dau,
					diem_cuoi,
					khoang_cach
				)
			end
			def update_frame_single_post(
					minifixID,
					diem_dau,
					diem_cuoi,
					khoang_cach
				)
				if @frame && !@frame.nil?
					if @e1_id && @e2_id
						frame_trans = @frame.transformation
						dir_vect =
							Utilities.vector_context_convert(
								diem_dau.vector_to(diem_cuoi),
								IDENTITY,
								frame_trans
							)
						normal =
							Utilities.vector_context_convert(
								@edge_and_face.edgeFace.normal,
								@edge_and_face.edgeSheet.trans,
								frame_trans
							)
						@frame.update_single_post_minifix(
							{
								e1_id: @e1_id,
								e2_id: @e2_id,
								config_data: @config_data,
								line_direction: dir_vect.to_a,
								minifixID: minifixID,
								khoang_cach: khoang_cach.to_f,
								normal: normal.to_a,
								connector_type: @connector_type
							}
						)
					end
				end
			end

			def create_single_post_frame(
					input_line,
					minifixID,
					mini,
					line_direction
				)
				return unless mini && mini['normal']
				return unless !mini['deleted']
				normal =
					Utilities.vector_context_convert(
						Geom::Vector3d.new(mini['normal']),
						@frame.transformation,
						@edge_and_face.edgeSheet.trans
					)
				test_normal = @edge_and_face.edgeFace.normal.dot(normal)
				edgeFace = @edge_and_face.edgeFace
				line = input_line
				if test_normal < 0
					edgeFace = @edge_and_face.edgeSheet.get_oposit(edgeFace)
					if edgeFace
						line =
							line.map do |pt|
								FittingUtils.project_oposit(
									pt,
									@edge_and_face.edgeSheet,
									edgeFace
								)
							end
					end
				end
				diem_dau = line[0]
				diem_cuoi = line[1]
				vect = diem_dau.vector_to(diem_cuoi)
				if vect.dot(line_direction) < 0
					diem_dau = line[1]
					diem_cuoi = line[0]
					line = [diem_dau, diem_cuoi]
				end
				khoang_cach = 0
				if mini.key?('moved')
					mv_vect =
						Utilities.vector_context_convert(
							Geom::Vector3d.new(mini['moved']['direction']),
							@frame.transformation,
							IDENTITY
						)
					if mv_vect.dot(diem_dau.vector_to(diem_cuoi)) < 0
						d = mini['khoang_cach'] - mini['moved']['distance']
						khoang_cach = d.to_l
					else
						d = mini['khoang_cach'] + mini['moved']['distance']
						khoang_cach = d.to_l
					end
				else
					khoang_cach = mini['khoang_cach'].to_l
				end
				ptr =
					self.get_single_hole_locations(
						diem_dau,
						diem_cuoi,
						khoang_cach,
						@config_data
					)
				self.create_connector(
					{
						ptr: ptr,
						minifixID: minifixID,
						bigFace: @edge_and_face.bigFace,
						faceSheet: @edge_and_face.faceSheet,
						edgeFace: edgeFace,
						edgeSheet: @edge_and_face.edgeSheet,
						edge_and_face: @edge_and_face,
						line: line
					}
				)
				return
			end

			def update_frame_multi_post(ids, index_map, line, line_idx)
				if @frame && !@frame.nil?
					if @e1_id && @e2_id
						frame_trans = @frame.transformation
						dir_vect =Utilities.vector_context_convert(
								@edge_and_face.line_direction,
								IDENTITY,
								frame_trans
							)
						@frame.update_multi_post_minifix(
							{
								e1_id: @e1_id,
								e2_id: @e2_id,
								config_data: @config_data,
								line_direction: dir_vect.to_a,
								ids: ids,
								line_idx: line_idx,
								index_map: index_map,
								connector_number: @connector_number,
								connector_array: @connector_array,
								connector_type: @connector_type
							}
						)
					end
				end
			end

			def create_multi_post_frame(plastic_bighole_points, line, line_idx)
				line_data = @mapping[line_idx.to_s]
				return if !line_data
				return if !line_data.key?('ids')
				return if !line_data.key?('index_map')
				index_map = line_data['index_map']
				ids = line_data['ids']
				# Chạy qua dãy l�?
				plastic_bighole_points.each_with_index do |ptr, p_index|
					minifixID = index_map[p_index.to_s]
					next unless minifixID
					normal_data = ids[minifixID]
					next unless !normal_data['deleted']
					next unless normal_data && normal_data['normal']
					if normal_data.key?('moved')
						mv_vect =
							Utilities.vector_context_convert(
								Geom::Vector3d.new(
									normal_data['moved']['direction']
								),
								@frame.transformation,
								IDENTITY
							)
						if @edge_and_face.line_direction.dot(mv_vect) < 0
							mv_vect =
								@edge_and_face.line_direction.reverse.normalize
						else
							mv_vect = @edge_and_face.line_direction.normalize
						end
						mv_vect =
							Utilities.vector_multiply(
								normal_data['moved']['distance'].to_l,
								mv_vect
							)
						tr = Geom::Transformation.translation mv_vect
						ptr1 = {}
						ptr.each { |pk, pv| ptr1[pk] = pv.transform(tr) }
						ptr = ptr1
					end
					normal =
						Utilities.vector_context_convert(
							Geom::Vector3d.new(normal_data['normal']),
							@frame.transformation,
							@edge_and_face.edgeSheet.trans
						)
					test_normal = @edge_and_face.edgeFace.normal.dot(normal)
					edgeFace = @edge_and_face.edgeFace
					line1 = line
					points = ptr
					if test_normal < 0
						edgeFace = @edge_and_face.edgeSheet.get_oposit(edgeFace)
						if edgeFace
							line1 =
								line1.map do |pt|
									FittingUtils.project_oposit(
										pt,
										@edge_and_face.edgeSheet,
										edgeFace
									)
								end
							points = {}
							ptr.each do |k, pt|
								points[k] =
									FittingUtils.project_oposit(
										pt,
										@edge_and_face.edgeSheet,
										edgeFace
									)
							end
						end
					end
					next unless edgeFace
					self.create_connector(
						{
							ptr: points,
							minifixID: minifixID,
							bigFace: @edge_and_face.bigFace,
							faceSheet: @edge_and_face.faceSheet,
							edgeFace: edgeFace,
							edgeSheet: @edge_and_face.edgeSheet,
							edge_and_face: @edge_and_face,
							line: line1
						}
					)
				end
			end
			def draw
				return unless @view
				if !@helper.nil?
					if @helper[:point]
						@view.draw_points(@helper[:point], 10, 1, 'red')
					end
					@view.drawing_color = 'yellow'
					@view.line_width = 5
					@view.draw_lines @helper[:point] if @helper[:point]
				end
			end
			def display_error(error_message, error_class)
				return if @dialog.nil?
				js_command =
					"error_display('#{error_message}', '#{error_class}')"
				@dialog.execute_script(js_command)
			end
		end #Class
	end
end