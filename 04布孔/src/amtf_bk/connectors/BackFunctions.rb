# encoding: UTF-8
module AMTF
	module BUKONG
		class BackFunctions
			def initialize(data)
				@sheet1 = data[:sheet1]
				@sheet2 = data[:sheet2]
				@frame = data[:frame]
				@auto = data[:auto]
				@for_frame = data[:for_frame]
				@config_data = data[:config_data]
				@config = data[:config]
				@connector_id = data[:connector_id]
				@groove_save_config = data[:groove_save_config]
				@min_groove_depht = data[:min_groove_depht]
			end
			def execute
				unless Utilities.instance?(@sheet1.comp) &&
						Utilities.instance?(@sheet2.comp)
					return nil
				end
				begin
					es1 = Utilities.definition(@sheet1.comp).entities
					es2 = Utilities.definition(@sheet2.comp).entities
					grp = Sketchup.active_model.entities.add_group
					ges = grp.entities
					invt2 = @sheet2.parent_tr.inverse
					gt = invt2 * grp.transformation
					et = invt2 * @sheet1.trans
					es1.intersect_with(true, et, ges, gt, true, @sheet2.comp)
					unless ges[0]
						grp.erase! if grp && grp.valid?
						return nil
					end
					edges = ges.grep(Sketchup::Edge)
					unless edges
						grp.erase! if grp && grp.valid?
						return nil
					end
					the_sheet = nil
					the_trans = nil
					the_back = nil
					back_trans = nil
					if !Utilities.is_back_comp?(
							IDENTITY,
							@sheet1.trans,
							edges,
							@sheet1.comp
					   )
						the_sheet = @sheet1.comp
						the_back = @sheet2.comp
						the_trans = @sheet1.trans
						back_trans = @sheet2.trans
					elsif !Utilities.is_back_comp?(
							IDENTITY,
							@sheet2.trans,
							edges,
							@sheet2.comp
					    )
						the_sheet = @sheet2.comp
						the_back = @sheet1.comp
						the_trans = @sheet2.trans
						back_trans = @sheet1.trans
					end
					if the_back.nil? || the_sheet.nil?
						grp.erase! if grp && grp.valid?
						return
					end
					if @auto
						edge_faces = Utilities.get_edge_faces(the_back)
						edge_faces.each do |f|
							f.delete_attribute 'amtf_dict', 'edge_banding'
							f.delete_attribute 'amtf_dict', 'edge_type_id'
							f.delete_attribute 'amtf_dict', 'edge_type_name'
							f.delete_attribute 'amtf_dict', 'edge_type_thick'
							f.set_attribute 'amtf_dict', 'edge_banding', 'false'
							f.material = the_back.material
						end
					end
					groove_type_id =
						the_back.get_attribute 'amtf_dict', 'groove_type_id'
					groove_type_name =
						the_back.get_attribute 'amtf_dict', 'groove_name'
					groove_type_extend =
						the_back.get_attribute 'amtf_dict', 'extra_grooves'
					groove_type_extend_width =
						the_back.get_attribute 'amtf_dict',
						                       'extra_grooves_width'
					groove_depth =
						the_back.get_attribute 'amtf_dict', 'groove_depth'
					groove_color =
						the_back.get_attribute 'amtf_dict', 'groove_color'
					save_config = {
						'name' => groove_type_name,
						'groove_depth' => groove_depth,
						'extra_grooves' => groove_type_extend,
						'extra_grooves_width' => groove_type_extend_width,
						'color' => groove_color,
						'default' => false,
						'toolNumber' => '1',
						'groove_type' => 4,
						'groove_passes' => 1
					}
					if groove_type_name.nil? || groove_type_extend.nil?
						if @for_frame
							groove_type_id = @groove_save_config['id']
							groove_type_name = @groove_save_config['name']
							groove_type_extend =
								@groove_save_config['extra_grooves']
							groove_type_extend_width =
								@groove_save_config['extra_grooves_width']
							groove_depth = @groove_save_config['groove_depth']
							groove_color = @groove_save_config['color']
							save_config = @groove_save_config
						else
							groove_type_id = @connector_id
							groove_type_name = @config_data['name']
							groove_type_extend = @config_data['extra_grooves']
							groove_type_extend_width =
								@config_data['extra_grooves_width']
							groove_depth = @config_data['groove_depth']
							groove_color = @config_data['color']
							save_config = @config_data
						end
					end
					groove_type_extend = 0 if groove_type_extend.nil?
					groove_type_extend_width = 0 if groove_type_extend_width
						.nil?
					# Tìm cạnh dài các cạnh dài nhất trong các cạnh, 2 cạnh dài nhất s�?dùng đ�?làm rãnh hậu
					# Nới rãnh hậu dài đ�?cắt
					points_data =
						Utilities.points_of_longe_edges(
							edges,
							the_sheet,
							the_trans,
							groove_type_extend,
							groove_type_extend_width
						)
					if points_data.nil?
						grp.erase! if grp && grp.valid?
						return
					end
					points = points_data[:points]
					selected_face = points_data[:selected_face]
					if @min_groove_depht && @min_groove_depht > 0
						the_depth =
							Utilities.get_groove_depth(
								selected_face,
								the_trans,
								the_back,
								back_trans
							)
						if the_depth < @min_groove_depht
							grp.erase! if grp && grp.valid?
							return nil
						end
					end
					grp.erase!
					points =
						points.map do |point|
							p1 =
								Utilities.point_context_convert(
									point,
									IDENTITY,
									the_trans
								)
							projected_point =
								p1.project_to_plane(selected_face.plane)
						end
					chanel_grp = the_sheet.entities.add_group
					the_sheet.set_attribute('amtf_dict', 'has_groove', 'yes')
					existingChanel = []
					the_sheet.entities.each do |rabbet|
						next unless rabbet.is_a?(Sketchup::Group)
						group_type = rabbet.get_attribute('amtf_dict', 'type')
						group_trans = the_trans * rabbet.transformation
						if (group_type == 'chanel')
							existingChanel <<
								Utilities.point_context_convert(
									Utilities.find_centroid(rabbet),
									group_trans,
									the_trans
								)
						end
					end
					center_point = Utilities.find_center_of_points(points)
					overlap = false
					existingChanel.each do |rabbet|
						overlap = true if rabbet.distance(center_point) <
							AmtfMath.thickness(the_sheet, the_trans)
					end
					if !overlap
						chanel_face = chanel_grp.entities.add_face(points)
						chanel_grp.set_attribute 'amtf_dict', 'type', 'chanel'
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_type_id',
						                         groove_type_id
						chanel_grp.set_attribute 'amtf_dict',
						                         'minifixID',
						                         Utilities.CreatUniqueid
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_name',
						                         groove_type_name
						chanel_grp.set_attribute 'amtf_dict',
						                         'groove_depth',
						                         groove_depth
						chanel_grp.set_attribute 'amtf_dict',
						                         'extra_grooves',
						                         groove_type_extend
						chanel_grp.set_attribute(
							'amtf_dict',
							'config_data',
							save_config.to_json
						)
						chanel_grp.material = groove_color if groove_color
						new_layer =
							Sketchup.active_model.layers.add 'Amtf Groove'
						chanel_grp.layer = new_layer
					end
					if @frame && !@frame.nil? && !@for_frame
						e1_id = @sheet1.sheet_id
						e2_id = @sheet2.sheet_id
						if e1_id && e2_id
							@frame.update_groove(
								{
									e1_id: e1_id,
									e2_id: e2_id,
									save_config: save_config
								}
							)
						end
					end
				rescue => error
					puts error
				end
			end
			def display_error(error_message, error_class)
				return if @dialog.nil?
				js_command =
					"error_display('#{error_message}', '#{error_class}')"
				@dialog.execute_script(js_command)
			end
		end #Class
	end
end