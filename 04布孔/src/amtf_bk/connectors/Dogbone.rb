# encoding: UTF-8
module AMTF
	module BUKONG
		class DogboneMale
			attr_accessor :origin, :elements
			def initialize(data)
				@parent = data[:parent]
				@origin = data[:origin]
				@z_axis =
					Utilities.vector_context_convert(
						data[:z_axis],
						data[:trans],
						IDENTITY
					)
				@y_axis = data[:y_axis]
				@x_axis = @z_axis.cross(@y_axis)
				@color = data[:color]
				@color = 'green' if !@color
				@trans = data[:trans]
				@depth = data[:depth]
				@points = [
					Geom::Point3d.new(-0.5 * data[:width], 0, 0),
					Geom::Point3d.new(-0.5 * data[:width], -data[:height], 0),
					Geom::Point3d.new(0.5 * data[:width], -data[:height], 0),
					Geom::Point3d.new(0.5 * data[:width], 0, 0),
					Geom::Point3d.new(-0.5 * data[:width], 0, 0)
				]
			end
			def set_attribute(dict, key, val)
				return unless @modal
				@modal.set_attribute(dict, key, val)
			end
			def drawModel
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.transform!(
					Geom::Transformation.new(@origin, @x_axis, @y_axis)
				)
				edges = []
				for i in 0..3
					e = @modal.entities.add_line(@points[i], @points[i + 1])
					edges << e
					if i < 3
						e.set_attribute('amtf_dict', 'dogbone_male_edge', true)
					end
				end
				face = @modal.entities.add_face(edges)
				face.pushpull(-@depth, true)
				face.reverse!
				@modal.transform!(@trans.inverse)
				new_layer =
					Sketchup.active_model.layers.add('Amtf_dogbone_male')
				@modal.layer = new_layer
				@modal.set_attribute('amtf_dict', 'dogbone_modal', 'male')
			end
		end
		class DogboneFemale
			attr_accessor :origin, :elements
			def initialize(data)
				@parent = data[:parent]
				@origin = data[:origin]
				@z_axis =
					Utilities.vector_context_convert(
						data[:z_axis],
						data[:trans],
						IDENTITY
					)
				@x_axis = data[:x_axis]
				@y_axis = @z_axis.cross(@x_axis)
				@color = data[:color]
				@color = 'green' if !@color
				@trans = data[:trans]
				width = data[:sheet_thick] + 2 * data[:tolerance]
				height =
					data[:distance_l1] - data[:tool_diameter] +
						2 * data[:extra_grooves]
				@pocket = [
					Geom::Point3d.new(-0.5 * width, -0.5 * height, 0),
					Geom::Point3d.new(0.5 * width, -0.5 * height, 0),
					Geom::Point3d.new(0.5 * width, 0.5 * height, 0),
					Geom::Point3d.new(-0.5 * width, 0.5 * height, 0),
					Geom::Point3d.new(-0.5 * width, -0.5 * height, 0)
				]
				@line1 = [
					Geom::Point3d.new(
						-0.5 * width + 0.5 * data[:tool_diameter],
						-0.5 * height,
						0
					),
					Geom::Point3d.new(
						-0.5 * width + 0.5 * data[:tool_diameter],
						0.5 * height,
						0
					)
				]
				@line2 = [
					Geom::Point3d.new(
						0.5 * width - 0.5 * data[:tool_diameter],
						-0.5 * height,
						0
					),
					Geom::Point3d.new(
						0.5 * width - 0.5 * data[:tool_diameter],
						0.5 * height,
						0
					)
				]
			end
			def set_attribute(dict, key, val)
				return unless @modal
				@modal.set_attribute(dict, key, val)
			end
			def drawModel
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.transform!(
					Geom::Transformation.new(@origin, @x_axis, @y_axis)
				)
				for i in 0..@pocket.size - 2
					e = @modal.entities.add_line(@pocket[i], @pocket[i + 1])
					e.set_attribute('amtf_dict', 'dogbone_edge', true)
				end
				line1 = @modal.entities.add_line(@line1)
				line1.set_attribute('amtf_dict', 'dogbone_mark', true)
				line2 = @modal.entities.add_line(@line2)
				line2.set_attribute('amtf_dict', 'dogbone_mark', true)
				@modal.transform!(@trans.inverse)
				new_layer =
					Sketchup.active_model.layers.add('Amtf_dogbone_female')
				@modal.layer = new_layer
				@modal.set_attribute('amtf_dict', 'dogbone_modal', 'female')
			end
		end
	end
end