# encoding: UTF-8
module AMTF
	module BUKONG
		class DogboneFunctions < ConnectorFunctions
			def create_connector(data)
				# move_vect1: di chuyển ốc cam vào trong tấm
				# move_vect2: di chuyển chốt cam vào giữa tấm
				# move_vect3: di chuyển theo chiều đầu đến cuối
				ptr = data[:ptr]
				move_vect1 =
					Utilities.vector_context_convert(
						data[:bigFace].normal,
						data[:faceSheet].trans,
						IDENTITY
					).normalize
				move_vect2 =
					Utilities.vector_context_convert(
						data[:edgeFace].normal,
						data[:edgeSheet].trans,
						IDENTITY
					).normalize
				move_vect3 = data[:line][0].vector_to(data[:line][1]).normalize
				sheet_thick = data[:edgeSheet].sheet_thick
				dVector =
					Utilities.vector_multiply(
						0.5 * sheet_thick,
						move_vect2.reverse
					)
				chot_cam_translate = Geom::Transformation.translation dVector
				center_oc_cam = ptr[:chot_cam]
				center_chotcam = ptr[:chot_cam].transform(chot_cam_translate)
				center_chot_go = ptr[:chot_go2].transform(chot_cam_translate)
				# Kiểm tra Overlap va ra ngoai
				overlap_threshold = 0.5 * @config_data['distance_l1'].to_s.to_l
				unless FittingUtils.not_overlap?(
						center_oc_cam,
						@existingHoleEdgeComp,
						overlap_threshold,
						data[:edgeFace],
						data[:edgeSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.not_overlap?(
						center_chotcam,
						@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						data[:faceSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				if FittingUtils.out_of_face?(
						center_oc_cam,
						data[:edgeFace],
						move_vect3,
						0.5 * @config_data['distance_l1'].to_s.to_l,
						data[:edgeSheet].trans
				   )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(
						center_chotcam,
						data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['distance_l1'].to_s.to_l,
						data[:faceSheet].trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				width =
					@config_data['distance_l1'].to_s.to_l -
						@config_data['diameter'].to_s.to_l
				height = @config_data['distance_l2'].to_s.to_l
				male =
					DogboneMale.new(
						{
							parent: data[:edgeSheet].comp,
							origin: center_oc_cam,
							z_axis: data[:edgeFace].normal,
							y_axis:
								Utilities.vector_context_convert(
									data[:bigFace].normal,
									data[:faceSheet].trans,
									IDENTITY
								).normalize,
							trans: data[:edgeSheet].trans,
							width: width,
							height: height,
							depth: sheet_thick
						}
					)
				male.drawModel
				male.set_attribute('amtf_dict', 'type', 'dogbone_male')
				male.set_attribute('amtf_dict', 'minifixID', data[:minifixID])
				male.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				male.set_attribute('amtf_dict', 'pairs', "#{@e1_id}~#{@e2_id}")
				male.set_attribute(
					'amtf_dict',
					'persit_pairs',
					"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
				)
				extra_grooves = 0.mm
				if @config_data['extra_grooves']
					extra_grooves = @config_data['extra_grooves'].to_s.to_l
				end
				female =
					DogboneFemale.new(
						{
							parent: data[:faceSheet].comp,
							origin: center_chotcam,
							z_axis: data[:bigFace].normal,
							x_axis:
								Utilities.vector_context_convert(
									data[:edgeFace].normal,
									data[:edgeSheet].trans,
									IDENTITY
								).normalize,
							trans: data[:faceSheet].trans,
							distance_l1: @config_data['distance_l1'].to_s.to_l,
							sheet_thick: sheet_thick,
							tool_diameter: @config_data['diameter'].to_s.to_l,
							tolerance: @config_data['tolerance'].to_s.to_l,
							extra_grooves: extra_grooves
						}
					)
				female.drawModel
				female.set_attribute('amtf_dict', 'type', 'dogbone_female')
				female.set_attribute('amtf_dict', 'minifixID', data[:minifixID])
				female.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				female.set_attribute(
					'amtf_dict',
					'pairs',
					"#{@e1_id}~#{@e2_id}"
				)
				female.set_attribute(
					'amtf_dict',
					'persit_pairs',
					"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
				)
				bankinh_r1 = 0.5 * @config_data['diameter_1'].to_s.to_l
				return unless bankinh_r1 > 0
				unless FittingUtils.not_overlap?(
						center_chot_go,
						@existingHoleFaceComp,
						overlap_threshold,
						data[:bigFace],
						data[:faceSheet].trans
				       )
					display_error(OB[:over_lap_connector], 'alert-warning')
					return
				end
				unless FittingUtils.chot_cam_not_out_of_face?(
						center_chot_go,
						data[:bigFace],
						data[:line][0],
						data[:line][1],
						0.5 * @config_data['distance_l1'].to_s.to_l,
						data[:faceSheet].trans
				       )
					display_error(OB[:out_of_face], 'alert-warning')
					return
				end
				height_1 = 20.mm
				move_1 = 10.mm
				chot_go =
					CircleModel.new(
						data[:faceSheet].comp,
						center_chot_go,
						bankinh_r1,
						data[:bigFace].normal,
						height_1,
						move_1,
						data[:faceSheet].trans
					)
				chot_go.drawModel
				chot_go.set_attribute('amtf_dict', 'type', 'chot_cam')
				chot_go.set_attribute('amtf_dict', 'dogbone', true)
				chot_go.set_attribute(
					'amtf_dict',
					'minifixID',
					data[:minifixID]
				)
				# chot_cam.set_attribute(
				# 	'amtf_dict',
				# 	'connector_type_id',
				# 	@connector_id
				# )
				chot_go.set_attribute('amtf_dict', 'radius', bankinh_r1)
				chot_go.set_attribute(
					'amtf_dict',
					'config_data',
					@config_data.to_json
				)
				chot_go.set_attribute(
					'amtf_dict',
					'pairs',
					"#{@e1_id}~#{@e2_id}"
				)
				chot_go.set_attribute(
					'amtf_dict',
					'persit_pairs',
					"#{data[:faceSheet].comp.persistent_id}~#{data[:edgeSheet].comp.persistent_id}"
				)
			end
		end
	end
end