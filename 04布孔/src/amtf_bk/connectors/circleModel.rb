# encoding: UTF-8

module AMTF
	module BUKONG
		class CircleModel
			include AMTF_bk_mixin
			attr_accessor :modal
			def initialize(
					parent,
					center,
					diameter,
					z_axis,
					height,
					move,
					trans,
					color = 'red'
				)
				@parent = parent
				@diameter = diameter

				# 从局部坐标系变换到世界坐标系
				@z_axis =Utilities.vector_context_convert(z_axis, trans, IDENTITY)
				@height = height
				@move = move
				@color = color
				@trans = trans
				@center = center
			end
			def set_attribute(dict, key, val)
				return unless @modal
				@modal.set_attribute(dict, key, val)
			end
			# def transform!(tr)
			# 	@modal.transform!(tr)
			# end
			def drawModelView(名称="孔")
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.name=名称
				@modal.material = 'red'
				# 对齐世界坐标系的Z轴画圆柱
				@modal.transform!(Geom::Transformation.new(@center, @z_axis))
				circle =@modal.entities.add_circle [0, 0, -@height],[0, 0, 1],@diameter
				face = @modal.entities.add_face(circle)
				face.pushpull(@height)
				# 变换到局部坐标系
				@modal.transform!(@trans.inverse)
				new_layer =Sketchup.active_model.layers.add('0_99 lianjiejian')
				@modal.layer = new_layer
				# @modal.set_attribute('amtf_dict', 'minifix_modal_view', true)
				@modal.set_attribute('amtf_dict', 'minifix_modal', true)
				return @modal
			end
			
			def drawModel
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.name="三合一"
				@modal.transform!(Geom::Transformation.new(@center, @z_axis))
				circle =@modal.entities.add_circle [0, 0, 0], [0, 0, 1], @diameter
				@modal.transform!(@trans.inverse)
				new_layer =Sketchup.active_model.layers.add('0_99 lianjiejian')
				@modal.layer = new_layer
				@modal.set_attribute('amtf_dict', 'minifix_modal', true)
			end
			def drawSideModel
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.transform!(Geom::Transformation.new(@center, @z_axis))
				circle =
					@modal.entities.add_circle [0, 0, 0], [0, 0, 1], @diameter
				@modal.transform!(@trans.inverse)
				new_layer =
					Sketchup.active_model.layers.add('Amtf_miscellaneous')
				@modal.layer = new_layer
				@modal.set_attribute('amtf_dict', 'side_modal', true)
			end
		end #Class
	end

end