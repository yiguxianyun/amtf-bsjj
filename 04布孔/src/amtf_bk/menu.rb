# encoding: UTF-8
module AMTF
	module BUKONG
		unless file_loaded?(__FILE__)
			@@path_to_resources = File.join(File.dirname(__FILE__), 'images')
			file_loaded(__FILE__)
			PLUGIN_ID = 'amtf布孔'

			AMTF_STORE = AMTFStore.new
			FUJI_SU_AUTH =
				'LmvnpUgThN4LGHyTty3z8Mq6M96nBqNYjJ8JWg2ED4Ycw2Sm4tJ5EQcJZJqN9cqh'
			OB = Ordbok.new
			AMTF_Dialog = AMTFDialog.new

			Sketchup.add_observer(BuKongAppObserver.new)
			Sketchup.active_model.add_observer(BuKongModelObserver.new)
			Sketchup.active_model.entities.add_observer(
				BuKongEntitiesObserver.new
			)
			menu = UI.menu('Plugins').add_submenu('amtf布孔')
			command = UI::Command.new("另存amtf布孔设置文件"){ A布孔.new().另存amtf布孔设置文件}
			menu.add_item(command)
			command = UI::Command.new("导入amtf布孔设置文件"){ A布孔.new().导入amtf布孔设置文件}
			menu.add_item(command)

			tb1 = UI::Toolbar.new('amtf布孔')
			cmd =UI::Command.new("连接件") do
				ConnectorTool::A连接件.tool_activate
				end
			cmd.large_icon =cmd.small_icon = File.join(PLUGIN_DIR, 'images', '添加连接件.png')
			cmd.tooltip = "添加连接件"
			cmd.status_bar_text = "添加连接件"
			cmd.set_validation_proc do
				ConnectorTool::A连接件.active? ? MF_CHECKED : MF_UNCHECKED
			end
			tb1.add_item cmd

			cmd =UI::Command.new("删除连接件") do
					if !RemoveToolsModule::RemoveTool.active?
						RemoveToolsModule::RemoveTool.tool_activate
					end
				end
			cmd.large_icon =cmd.small_icon = File.join(PLUGIN_DIR, 'images', '删除连接件.png')
			cmd.tooltip = "删除连接件"
			cmd.status_bar_text = "删除连接件"
			cmd.set_validation_proc do
				if RemoveToolsModule::RemoveTool.active?
					MF_CHECKED
				else
					MF_UNCHECKED
				end
			end
			tb1.add_item cmd

			cmd =
				UI::Command.new(OB[:打标签]) do
					Hole_functions.打标签
					Sketchup.active_model.tools.pop_tool
				end
			cmd.large_icon =
				cmd.small_icon =
					File.join(PLUGIN_DIR, 'images', '打标签.png')
			cmd.tooltip = OB[:打标签]
			cmd.status_bar_text = OB[:打标签]
			tb1.add_item cmd

			cmd =UI::Command.new(OB[:旋转标签]) {Tools::A旋转标签.tool_activate}
			cmd.large_icon =cmd.small_icon =File.join(PLUGIN_DIR, 'images', '旋转标签.png')
			cmd.tooltip = OB[:旋转标签]
			cmd.status_bar_text = OB[:旋转标签]
			tb1.add_item cmd

			cmd =UI::Command.new(OB[:翻转标签]) {Tools::A翻转标签.tool_activate}
			cmd.large_icon =cmd.small_icon =File.join(PLUGIN_DIR, 'images', '翻转标签.png')
			cmd.tooltip = OB[:翻转标签]
			cmd.status_bar_text = OB[:翻转标签]
			tb1.add_item cmd

			command = UI::Command.new("刷新amtf"){ reload_bk}
			command.small_icon = File.join(@@path_to_resources,"amtf-无我偈2.png")
			command.large_icon = File.join(@@path_to_resources,"amtf-无我偈2.png")
			command.tooltip = ("刷新amtf")
			command.status_bar_text = ("刷新amtf")
			tb1.add_item(command)

			tb1.restore
			Sketchup.active_model.tools.add_observer(AmtfToolsObserver.new)
			# Run this block ONCE upon first load:
			unless defined?(@loaded)
				@request = nil
				@response = nil
				# Attach this submodule as an AppObserver object:
				Sketchup.add_observer(self)
				@loaded = true
			end
		end
	end
end