require 'sketchup.rb'
require 'json'
module AMTF_bk_mixin
  Amtf布孔设置文件=File.join(__dir__, 'amtf布孔设置.json')#大写的为模块常量

  def 读取设置()
    json = File.read(Amtf布孔设置文件)
    # puts json.class #==String
    return 设置 = JSON.parse(json) 
  end

  # def 读取设置k值(k)
  #   原设置=读取设置
  #   k = 原设置[k]
  #   return k
  # end

  def 读取amtf_su设置
    amtf_su设置文件路径=读取设置k值("amtf_su设置文件路径")
    amtf_su设置文件=File.join(amtf_su设置文件路径, 'amtf_su设置文件.json')
    if File.exist? amtf_su设置文件
      
    else
      amtf_su设置文件=File.join(__dir__, 'amtf_su设置文件_默认.json')
    end
    puts amtf_su设置文件
  end

  def 保存设置(k,v)
    原设置=读取设置
    puts 原设置[k]
    原设置[k]=v
    File.write(Amtf布孔设置文件, JSON.dump(原设置))
  end

  def 保存设置_合并hash(传入h)
    原设置=读取设置
    puts 原设置.class
    原设置.merge!(传入h)
    File.write(Amtf布孔设置文件, JSON.dump(原设置))
  end

  def 添加圆柱(parent,c,center,半径,z_axis,height)
    center=center.transform(parent.transformation.inverse)
    center=center.transform(c.transformation.inverse)

    z_axis=z_axis.transform(parent.transformation.inverse)
    z_axis=z_axis.transform(c.transformation.inverse)

    # center=center.transform(trans.inverse)
    # puts "center 👉 #{center}" 
    circle =c.entities.add_circle center,z_axis,半径
    face = c.entities.add_face(circle)
    face.pushpull(-height)
    # amtf.nil
    # @modal.transform!(trans.inverse)
    # new_layer =Sketchup.active_model.layers.add('0_99 lianjiejian')
    # @modal.layer = new_layer
    # @modal.set_attribute('amtf_dict', 'minifix_modal_view', true)
    # @modal.set_attribute('amtf_dict', 'minifix_modal', true)
  end
end
