# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfSheet
			attr_accessor :bounds,:id,:comp,:trans,:parent_tr,:relatedPart,:node
			def initialize(comp, trans, parent_tr, node = nil)
				@id = comp.persistent_id
				@comp = comp
				comp.make_unique
				@trans = trans
				@parent_tr = parent_tr
				@node = node
				bb = comp.bounds
				@bounds = Geom::BoundingBox.new
				for i in 0..7
					@bounds.add(
						Utilities.point_context_convert(
							bb.corner(i),
							@parent_tr,
							IDENTITY
						)
					)
				end
				@relatedPart = {}
			end
			
			def addRelated(partsArray)
				return unless partsArray && partsArray.size > 0
				for i in 0..partsArray.size - 1
					part = partsArray[i]
					next if part == self
					next if !Utilities.isOverlap?(part.bounds, @bounds)
					# my_big = self.big_faces
					# vector1 = my_big[0].normal
					# your_big = part.big_faces
					# vector2 = your_big[0].normal
					# next unless vector1.垂直icular?(vector2)
					@relatedPart[part] = part
				end
			end
			def transformation
				return @trans
			end
			def all_faces
				Utilities.definition(@comp).entities.grep(Sketchup::Face)
			end
			def big_faces
				faces =Utilities.definition(@comp).entities.grep(Sketchup::Face)
				return unless faces && faces.size > 0
				faces.compact!
				out = faces.sort_by { |f| AmtfMath.face_area(f, @trans) }
				result = [out[out.size - 1], out[out.size - 2]]
				return result
			end
			def edge_face
				faces =Utilities.definition(@comp).entities.grep(Sketchup::Face)
				return unless faces && faces.size > 0
				faces.compact!
				out = faces.sort_by { |f| AmtfMath.face_area(f, @trans) }
				result = out.slice(0, out.size - 2)
				return result
			end
			def edge_banding(config, edge_auto_id, no_edge_thick)
				return unless self.sheet_thick > no_edge_thick.to_s.to_l
				unless config && config['edge_banding_thick'] &&
						config['name'] && config['color']
					return
				end
				attrs = {}
				edging = {}
				part_frame = Utilities.get_frame_of_part(self)
				id = self.sheet_id
				if part_frame && id
					attrs = part_frame.attrs
					if attrs && attrs.key?('root')
						if !attrs['root'].key?('edging')
							attrs['root']['edging'] = {}
						end
						if !attrs['root']['edging'].key?(id)
							attrs['root']['edging'][id] = {}
						end
						edging = attrs['root']['edging'][id]
					end
				end
				faces = edge_face
				faces.each do |f|
					f.set_attribute 'amtf_dict', 'edge_banding', 'true'
					f.set_attribute 'amtf_dict', 'edge_type_id', edge_auto_id
					f.set_attribute 'amtf_dict','edge_type_name',config['name']
					f.set_attribute 'amtf_dict','edge_type_thick',config['edge_banding_thick']
					f.material = config['color']
					face_index = f.get_attribute('amtf_dict', 'face_index')
					if face_index
						edging[face_index] = {
							'edge_banding' => true,
							'edge_type_id' => edge_auto_id,
							'edge_type_name' => config['name'],
							'edge_type_thick' => config['edge_banding_thick'],
							'color' => config['color']
						}
					end
				end
				if part_frame
					part_frame.saver.save_comp_json(part_frame.frame, attrs)
				end
			end
			def get_grain_vector
				faces = get_big_faces
				获取标签_vector或者长边_vector(faces[0])
			end

			def 获取标签
				subGroup =Utilities.definition(self.comp).entities.grep(Sketchup::Group)
				标签 = nil
				subGroup.each { |ent|
					if (ent.name == "标签")
						# tr = ent.transformation
						标签 = ent
						break
					end
				}
				return 标签
			end

			def 获取长边_vector(face)
				# faces = get_big_faces
				# face=faces[0]
				edge_array =
				face.edges.sort_by do |e|
					e
						.start
						.position
						.transform(@trans)
						.vector_to(e.end.position.transform(@trans))
						.length
				end
				long = edge_array.pop
				pt1 = long.vertices[0].position
				pt2 = long.vertices[1].position
				长边_vector = pt1.vector_to(pt2)
				custom_grain =
					self.comp.get_attribute('amtf_dict', 'custom_grain')
				if !custom_grain.nil? &&
						Utilities.isNumber?(custom_grain.to_s) &&
						custom_grain.to_f > 0
					tr =
						Geom::Transformation.new pt1,
												face.normal,
												custom_grain.to_f.degrees
					长边_vector = 长边_vector.transform(tr)
				end
				return 长边_vector

			end

			def 标签平行长边吗()
				标签=获取标签
				标签_vector=标签.transformation.xaxis
				长边_vector=获取长边_vector(get_big_faces[0])
				标签平行长边=长边_vector.parallel?(标签_vector)
				puts "标签平行长边吗"
				puts 标签平行长边
				return 标签平行长边
			end

			def 获取标签_vector或者长边_vector(face)
				标签=获取标签
				return 标签.transformation.xaxis unless 标签.nil?
				# 如果没有打过标签👇
				获取长边_vector(get_big_faces[0])
			end

			def get_big_faces
				faces =
					Utilities.definition(@comp).entities.grep(Sketchup::Face)
				return unless faces && faces.size > 0
				faces.compact!
				out = faces.sort_by { |f| AmtfMath.face_area(f, @trans) }
				result = [out[out.size - 1], out[out.size - 2]]
				pick_face = out[out.size - 2]
				priority_type =
					pick_face.get_attribute('amtf_dict', 'priority_type')
				result = [
					out[out.size - 2],
					out[out.size - 1]
				] if priority_type != 'first_priority'
				return result
			end

			def grep_connector
				existingHole = []
				cams2 =Utilities.definition(self.comp).entities.grep(Sketchup::Group)
				cams2.each do |inst|
					is_modal = inst.get_attribute('amtf_dict', 'minifix_modal')
					if is_modal == true
						# 转换到全局坐标系 @trans=板件全局变换
						miniTrans = @trans * inst.transformation
						孔重心点 =Utilities.point_context_convert(
								Utilities.find_centroid(inst),
								miniTrans,
								@trans
							)
						existingHole << {
							hole: 孔重心点,
							instance: inst,
							type: inst.get_attribute('amtf_dict', 'type'),
							config_data:inst.get_attribute('amtf_dict', 'config_data'),
							connector_type_id:inst.get_attribute('amtf_dict','connector_type_id'),
							pairs: inst.get_attribute('amtf_dict', 'pairs'),
							persit_pairs:inst.get_attribute('amtf_dict', 'persit_pairs'),
							minifixID:inst.get_attribute('amtf_dict', 'minifixID'),
							trans: @trans,
							parent: @comp
						}
					end
				end
				return existingHole
			end

			def grep_side_holes
				existingHole = []
				cams2 =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::Group)
				cams2.each do |inst|
					is_modal = inst.get_attribute('amtf_dict', 'side_modal')
					if is_modal == true
						miniTrans = @trans * inst.transformation
						孔重心点 =
							Utilities.point_context_convert(
								Utilities.find_centroid(inst),
								miniTrans,
								@trans
							)
						existingHole << {
							hole: 孔重心点,
							instance: inst,
							type: inst.get_attribute('amtf_dict', 'type'),
							config_data:
								inst.get_attribute('amtf_dict', 'config_data'),
							connector_type_id:
								inst.get_attribute(
									'amtf_dict',
									'connector_type_id'
								),
							minifixID:
								inst.get_attribute('amtf_dict', 'minifixID'),
							trans: @trans
						}
					end
				end
				return existingHole
			end
			def grep_cabineo_cap
				cabineo_cap = []
				cams2 =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::Group)
				cams2.each do |inst|
					is_modal = inst.get_attribute('amtf_dict', 'cabineo_cap')
					if is_modal == true
						miniTrans = @trans * inst.transformation
						孔重心点 =
							Utilities.point_context_convert(
								Utilities.find_centroid(inst),
								miniTrans,
								@trans
							)
						cabineo_cap << {
							hole: 孔重心点,
							instance: inst,
							type: 'cabineo_cap',
							config_data:
								inst.get_attribute('amtf_dict', 'config_data'),
							connector_type_id:
								inst.get_attribute(
									'amtf_dict',
									'connector_type_id'
								),
							pairs: inst.get_attribute('amtf_dict', 'pairs'),
							persit_pairs:
								inst.get_attribute('amtf_dict', 'persit_pairs'),
							minifixID:
								inst.get_attribute('amtf_dict', 'minifixID'),
							trans: @trans,
							parent: @comp
						}
					end
				end
				return cabineo_cap
			end
			def grep_dogbone
				female = []
				male = []
				dogbone =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::Group)
				dogbone.each do |inst|
					dogbone_modal =
						inst.get_attribute('amtf_dict', 'dogbone_modal')
					if dogbone_modal == 'female'
						miniTrans = @trans * inst.transformation
						edges = inst.entities.grep(Sketchup::Edge)
						dogbone_edges =
							edges.select do |e|
								e.get_attribute('amtf_dict', 'dogbone_edge')
							end
						multiPath =
							TTLibrary.getMultiPathPointsHash(dogbone_edges)
						next unless multiPath && multiPath.size == 1
						dogbone_female_points = []
						ptr_arr = multiPath[0][:path]
						points = []
						for j in 0..ptr_arr.size - 1
							points <<
								Utilities.point_context_convert(
									ptr_arr[j][:point],
									miniTrans,
									@trans
								)
						end
						conn = {
							config_data:
								inst.get_attribute('amtf_dict', 'config_data'),
							type: 'dogbone_female_points',
							points: points
						}
						dogbone_female_points << conn
						dogbone_female_D = []
						dogbone_mark =
							edges.select do |e|
								e.get_attribute('amtf_dict', 'dogbone_mark')
							end
						dogbone_mark.each do |edg|
							dogbone_female_D << {
								center1:
									Utilities.point_context_convert(
										edg.start.position,
										miniTrans,
										@trans
									),
								center2:
									Utilities.point_context_convert(
										edg.end.position,
										miniTrans,
										@trans
									)
							}
						end
						孔重心点 =
							Utilities.point_context_convert(
								Utilities.find_centroid(inst),
								miniTrans,
								@trans
							)
						minifixID = inst.get_attribute('amtf_dict', 'minifixID')
						female << {
							hole: 孔重心点,
							instance: inst,
							type: inst.get_attribute('amtf_dict', 'type'),
							config_data:
								inst.get_attribute('amtf_dict', 'config_data'),
							connector_type_id:
								inst.get_attribute(
									'amtf_dict',
									'connector_type_id'
								),
							minifixID: minifixID,
							trans: @trans,
							pairs: inst.get_attribute('amtf_dict', 'pairs'),
							persit_pairs:
								inst.get_attribute('amtf_dict', 'persit_pairs'),
							dogbone_female_points: {
								pocket: dogbone_female_points,
								dogbone_female_D: dogbone_female_D,
								minifixID: minifixID
							},
							parent: @comp
						}
					end
					if dogbone_modal == 'male'
						miniTrans = @trans * inst.transformation
						edges = inst.entities.grep(Sketchup::Edge)
						dogbone_edges =
							edges.select do |e|
								e.get_attribute(
									'amtf_dict',
									'dogbone_male_edge'
								)
							end
						multiPath =
							TTLibrary.getMultiPathPointsHash(dogbone_edges)
						dogbone_male_points = []
						for i in 0..multiPath.size - 1
							ptr_arr = multiPath[i][:path]
							points = []
							for j in 0..ptr_arr.size - 1
								points <<
									Utilities.point_context_convert(
										ptr_arr[j][:point],
										miniTrans,
										@trans
									)
							end
							conn = {
								config_data:
									inst.get_attribute(
										'amtf_dict',
										'config_data'
									),
								type: 'dogbone_male_points',
								points: points
							}
							dogbone_male_points << conn
						end
						孔重心点 =
							Utilities.point_context_convert(
								Utilities.find_centroid(inst),
								miniTrans,
								@trans
							)
						male << {
							hole: 孔重心点,
							instance: inst,
							type: inst.get_attribute('amtf_dict', 'type'),
							config_data:
								inst.get_attribute('amtf_dict', 'config_data'),
							connector_type_id:
								inst.get_attribute(
									'amtf_dict',
									'connector_type_id'
								),
							pairs: inst.get_attribute('amtf_dict', 'pairs'),
							persit_pairs:
								inst.get_attribute('amtf_dict', 'persit_pairs'),
							minifixID:
								inst.get_attribute('amtf_dict', 'minifixID'),
							trans: @trans,
							dogbone_male_points: dogbone_male_points,
							parent: @comp
						}
					end
				end
				return { male: male, female: female }
			end
			def grep_lockdowel_as_hole
				#  Đ�?lấy các connector lockdowel cho mục đich tool di chuyển
				existingHole = []
				cams1 =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::Group)
				cams1.each do |inst|
					is_modal =
						inst.get_attribute('amtf_dict', 'lockdowel_modal')
					if is_modal == true
						center_faces =
							Utilities
								.definition(inst)
								.entities
								.grep(Sketchup::Face)
						if center_faces.size > 0
							tr = @trans * inst.transformation
							conn = {
								instance: inst,
								config_data:
									inst.get_attribute(
										'amtf_dict',
										'config_data'
									),
								minifixID:
									inst.get_attribute(
										'amtf_dict',
										'minifixID'
									),
								connector_type_id:
									inst.get_attribute(
										'amtf_dict',
										'connector_type_id'
									),
								pairs: inst.get_attribute('amtf_dict', 'pairs'),
								persit_pairs:
									inst.get_attribute(
										'amtf_dict',
										'persit_pairs'
									),
								type: 'lockdowel',
								trans: @trans,
								parent: @comp
							}
							center_faces.each do |cp|
								type = cp.get_attribute('amtf_dict', 'type')
								case type
								when 'point_1'
									conn[:p1] =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											@trans
										)
									conn[:hole] =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											@trans
										)
								when 'point_2'
									conn[:p2] =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											@trans
										)
								when 'point_3'
									conn[:p3] =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											@trans
										)
								end
								existingHole << conn
							end
						end
					end
				end
				return existingHole
			end
			def grep_lockdowel
				existingHole = []
				cams1 =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::Group)
				cams1.each do |inst|
					is_modal =
						inst.get_attribute('amtf_dict', 'lockdowel_modal')
					if is_modal == true
						center_faces =
							Utilities
								.definition(inst)
								.entities
								.grep(Sketchup::Face)
						if center_faces.size > 0
							tr = @trans * inst.transformation
							conn = {
								config_data:
									inst.get_attribute(
										'amtf_dict',
										'config_data'
									),
								connector_type_id:
									inst.get_attribute(
										'amtf_dict',
										'connector_type_id'
									),
								pairs: inst.get_attribute('amtf_dict', 'pairs'),
								persit_pairs:
									inst.get_attribute(
										'amtf_dict',
										'persit_pairs'
									),
								type: 'lockdowel',
								parent: @comp
							}
							center_faces.each do |cp|
								type = cp.get_attribute('amtf_dict', 'type')
								case type
								when 'point_1'
									conn[:p1] =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											@trans
										)
								when 'point_2'
									conn[:p2] =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											@trans
										)
								when 'point_3'
									conn[:p3] =
										Utilities.point_context_convert(
											Utilities.find_center_face(cp),
											tr,
											@trans
										)
								end
							end
							existingHole << conn
						end
					end
				end
				return existingHole
			end
			def grep_custom_path
				existingHole = []
				cams =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::ComponentInstance)
				cams.each do |inst|
					is_modal =
						inst.get_attribute('amtf_dict', 'customPath_modal')
					if is_modal == true
						modal =
							Utilities
								.definition(inst)
								.entities
								.grep(Sketchup::Group)
						if modal.size > 0
							tr =
								@trans * inst.transformation *
									modal[0].transformation
							cpoints =
								modal[0].entities.grep(
									Sketchup::ConstructionPoint
								)
							if cpoints.size > 0
								cpoints.sort_by! do |c|
									c.get_attribute('amtf_dict', 'index').to_i
								end
								conn = {
									config_data:
										inst.get_attribute(
											'amtf_dict',
											'config_data'
										),
									type: 'customPath',
									points: []
								}
								cpoints.each do |cp|
									conn[:points] <<
										Utilities.point_context_convert(
											cp.position,
											tr,
											@trans
										)
								end
								existingHole << conn
							end
						end
					end
				end
				return existingHole
			end
			def grep_tool_path
				grp =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::Group)
				tool_paths =
					grp.select do |e|
						e.get_attribute('amtf_dict', 'type') == 'tool_path'
					end
				return [] unless tool_paths.size > 0
				existingHole = []
				tool_paths.each do |e|
					edges = e.entities.grep(Sketchup::Edge)
					next unless edges.size > 0
					multiPath = TTLibrary.getMultiPathPointsHash(edges)
					tr = @trans * e.transformation
					for i in 0..multiPath.size - 1
						ptr_arr = multiPath[i][:path]
						points = []
						for j in 0..ptr_arr.size - 1
							ptr =
								Utilities.point_context_convert(
									ptr_arr[j][:point],
									tr,
									@trans
								)
							points << ptr
						end
						conn = {
							config_data:
								e.get_attribute('amtf_dict', 'config_data'),
							type: 'customPath',
							points: points,
							closed: multiPath[i][:closed]
						}
						existingHole << conn
					end
				end
				return existingHole
			end
			def grep_channel
				grp =
					Utilities
						.definition(self.comp)
						.entities
						.grep(Sketchup::Group)
				channels =
					grp.select do |rabbet|
						rabbet.get_attribute('amtf_dict', 'type') == 'chanel'
					end
				return [] unless channels.size > 0
				arr = []
				channels.each do |group|
					if group.get_attribute('amtf_dict', 'cabineo_cap')
						cap_edges = group.entities.grep(Sketchup::Edge)
						next unless cap_edges.size > 0
						multiPath = TTLibrary.getMultiPathPointsHash(cap_edges)
						next unless multiPath.size == 1
						miniTrans = @trans * group.transformation
						ptr_arr = multiPath[0][:path]
						layer_name =
							group.get_attribute 'amtf_dict',
							                    'groove_name',
							                    'pocket'
						points = []
						for j in 0..ptr_arr.size - 1
							ptr =
								Utilities.point_context_convert(
									ptr_arr[j][:point],
									miniTrans,
									@trans
								)
							points << ptr
						end
						arr << {
							offset: 0,
							points: points,
							offset_points: [],
							layer_name: layer_name,
							config_data:
								group.get_attribute('amtf_dict', 'config_data')
						}
					else
						face = group.entities.grep(Sketchup::Face)
						next unless face.size == 1
						loops = face[0].loops
						next if loops.size == 0 || loops.size > 2
						oloop = face[0].outer_loop
						miniTrans = @trans * group.transformation
						layer_name =
							group.get_attribute 'amtf_dict',
							                    'groove_name',
							                    'pocket'
						points = []
						oloop.vertices.each do |v|
							points <<
								Utilities.point_context_convert(
									v.position,
									miniTrans,
									@trans
								)
						end
						if loops.size == 1
							arr << {
								points: points,
								layer_name: layer_name,
								config_data:
									group.get_attribute(
										'amtf_dict',
										'config_data'
									)
							}
						elsif loops.size == 2
							inner = loops.select { |l| l != oloop }
							edges1 = oloop.edges
							edges2 = inner[0].edges
							edges =
								edges2.select do |e|
									e.line[1].parallel?(edges1[0].line[1])
								end
							next if edges.size == 0
							edges =
								edges.sort_by do |e|
									e.start.position.distance_to_line(
										edges1[0].line
									)
								end
							p1 =
								Utilities.point_context_convert(
									edges[0].start.position,
									miniTrans,
									IDENTITY
								)
							line1 =
								AmtfMath.line_context_convert(
									edges1[0].line,
									miniTrans,
									IDENTITY
								)
							distance = p1.distance_to_line(line1)
							offset_points = []
							inner[0].vertices.each do |v|
								offset_points <<
									Utilities.point_context_convert(
										v.position,
										miniTrans,
										@trans
									)
							end
							arr << {
								offset: distance,
								points: points,
								offset_points: offset_points,
								layer_name: layer_name,
								config_data:
									group.get_attribute(
										'amtf_dict',
										'config_data'
									)
							}
						end
					end
				end
				return arr
			end
			def get_point_to_point(executing_error, config)
				insert_drilling = InserDrilling.new(self, executing_error)
				insert_drilling.execute(config)
			end
			def process_big_faces(executing_error)
				big_faces = self.get_big_faces
				comp = self.comp
				side_face_array = self.edge_face
				# Có 2 trường hợp, là both-side hoặc là single side.
				# Lấy các đỉnh, các tâm l�? các đỉnh của chanel
				# Chuyển v�?tọa đ�? lấy pháp tuyến của mặt làm trục Z, và quay một góc theo chiều vân g�?so với trục Y, quanh trục Z
				children = self.grep_connector
				face0 =
					AmtfFace.new(
						big_faces[0],
						big_faces[1],
						side_face_array,
						@trans,
						executing_error
					)
				face1 =
					AmtfFace.new(
						big_faces[1],
						big_faces[0],
						side_face_array,
						@trans,
						executing_error
					)
				if children && children.size > 0
					children.each do |hash_obj|
						type = hash_obj[:type]
						group = hash_obj[:instance]
						孔重心点 = hash_obj[:hole]
						config_data = hash_obj[:config_data]
						connector_type_id = hash_obj[:connector_type_id]
						minifixID = hash_obj[:minifixID]
						radius = group.get_attribute 'amtf_dict', 'radius'
						radius = radius.to_f.to_l
						face0.insert_holes(
							孔重心点,
							type,
							radius,
							config_data,
							connector_type_id,
							minifixID
						)
						face1.insert_holes(
							孔重心点,
							type,
							radius,
							config_data,
							connector_type_id,
							minifixID
						)
					end
				end
				dogbones = self.grep_dogbone
				if dogbones
					dogbones[:male].each do |c|
						face0.insert_male_dogbone(c)
						face1.insert_male_dogbone(c)
					end
					dogbones[:female].each do |c|
						face0.insert_female_dogbone(c)
						face1.insert_female_dogbone(c)
					end
				end
				chanels = self.grep_channel
				if chanels && chanels.size > 0
					chanels.each do |c|
						face0.insert_chanel(c)
						face1.insert_chanel(c)
					end
				end
				locdowel = self.grep_lockdowel
				if locdowel && locdowel.size > 0
					locdowel.each do |c|
						face0.insert_lockdowel(c)
						face1.insert_lockdowel(c)
					end
				end
				# grep_custom_path và customPath_old dùng đ�?lấy d�?liệu khi người dùng s�?dụng 3.1.4, s�?b�?đi �?các phiên bản sau
				customPath_old = self.grep_custom_path
				if customPath_old && customPath_old.size > 0
					customPath_old.each do |c|
						face0.insert_customPath(c)
						face1.insert_customPath(c)
					end
				end
				customPath = self.grep_tool_path
				if customPath && customPath.size > 0
					customPath.each do |c|
						face0.insert_customPath(c)
						face1.insert_customPath(c)
					end
				end
				grain0 = 获取长边_vector(big_faces[0])
				grain1 = 获取长边_vector(big_faces[1])
				face0.convert_to_root_axes(grain0)
				face1.convert_to_root_axes(grain1)
				return { face0: face0, face1: face1 }
			end
			def get_nesting_data(executing_error)
				process_faces = self.process_big_faces(executing_error)
				face0 = process_faces[:face0]
				face1 = process_faces[:face1]
				big_0 = face0.big_face
				big_1 = face1.big_face
				priority_0 =
					big_0.get_attribute('amtf_dict', 'priority_type') ==
						'first_priority'
				priority_1 =
					big_1.get_attribute('amtf_dict', 'priority_type') ==
						'first_priority'
				material_and_color = self.mat_and_color
				mat_name = nil
				mat_color = nil
				if material_and_color
					mat_name = material_and_color[:name]
					mat_color = material_and_color[:color]
				end
				if priority_1
					return(
						{
							both_side: face1.has_hole?,
							id: Utilities.CreatUniqueid,
							label: self.label_without_id,
							group_label: self.group_label,
							part_label: self.get_name_without_id,
							numbering: self.get_numbering_id,
							connector_sum: {},
							top: {
								points: face0.points,
								innerLoop: face0.innerLoop,
								curveEdges: face0.curveEdges,
								curveEdges_inner: face0.curveEdges_inner,
								holePoints: face0.holePoints,
								dogboneMaleHoles: face0.dogboneMaleHoles,
								dogboneFemaleHoles: face0.dogboneFemaleHoles,
								lockdowelPoints: face0.lockdowelPoints,
								chanelPoints: face0.chanelPoints,
								edgeBandingMark: face0.edgeBandingMark,
								edgeBandingMark_inner:
									face0.edgeBandingMark_inner,
								dimension: self.dimension(big_faces[0]),
								tool_path: face0.tool_path,
								pocket_path: face0.pocket_path,
								custom_path: face0.custom_path
							},
							bottom: {
								holePoints: face0.holePoints_bottom,
								dogboneFemaleHoles:
									face0.dogboneFemaleHoles_bottom,
								chanelPoints: face0.chanelPoints_bottom,
								lockdowelPoints: face0.lockdowelPoints_bottom,
								tool_path: face0.tool_path_bottom,
								pocket_path: face0.pocket_path_bottom,
								custom_path: face0.custom_path_bottom
							},
							thick: self.sheet_thick.to_f,
							material: mat_name,
							color: mat_color
						}
					)
				elsif priority_0
					return(
						{
							both_side: face0.has_hole?,
							id: Utilities.CreatUniqueid,
							label: self.label_without_id,
							group_label: self.group_label,
							part_label: self.get_name_without_id,
							numbering: self.get_numbering_id,
							connector_sum: {},
							top: {
								points: face1.points,
								innerLoop: face1.innerLoop,
								curveEdges: face1.curveEdges,
								curveEdges_inner: face1.curveEdges_inner,
								dogboneMaleHoles: face1.dogboneMaleHoles,
								dogboneFemaleHoles: face1.dogboneFemaleHoles,
								holePoints: face1.holePoints,
								lockdowelPoints: face1.lockdowelPoints,
								chanelPoints: face1.chanelPoints,
								edgeBandingMark: face1.edgeBandingMark,
								edgeBandingMark_inner:
									face1.edgeBandingMark_inner,
								dimension: self.dimension(big_faces[1]),
								tool_path: face1.tool_path,
								pocket_path: face1.pocket_path,
								custom_path: face1.custom_path
							},
							bottom: {
								holePoints: face1.holePoints_bottom,
								dogboneFemaleHoles:
									face1.dogboneFemaleHoles_bottom,
								chanelPoints: face1.chanelPoints_bottom,
								lockdowelPoints: face1.lockdowelPoints_bottom,
								tool_path: face1.tool_path_bottom,
								pocket_path: face1.pocket_path_bottom,
								custom_path: face1.custom_path_bottom
							},
							thick: self.sheet_thick.to_f,
							material: mat_name,
							color: mat_color
						}
					)
				else
					if face0.has_hole?
						return(
							{
								both_side: face0.has_hole? && face1.has_hole?,
								id: Utilities.CreatUniqueid,
								label: self.label_without_id,
								group_label: self.group_label,
								part_label: self.get_name_without_id,
								numbering: self.get_numbering_id,
								connector_sum: {},
								top: {
									points: face0.points,
									innerLoop: face0.innerLoop,
									curveEdges: face0.curveEdges,
									curveEdges_inner: face0.curveEdges_inner,
									dogboneMaleHoles: face0.dogboneMaleHoles,
									dogboneFemaleHoles:
										face0.dogboneFemaleHoles,
									holePoints: face0.holePoints,
									lockdowelPoints: face0.lockdowelPoints,
									chanelPoints: face0.chanelPoints,
									edgeBandingMark: face0.edgeBandingMark,
									edgeBandingMark_inner:
										face0.edgeBandingMark_inner,
									dimension: self.dimension(big_faces[0]),
									tool_path: face0.tool_path,
									pocket_path: face0.pocket_path,
									custom_path: face0.custom_path
								},
								bottom: {
									holePoints: face0.holePoints_bottom,
									dogboneFemaleHoles:
										face0.dogboneFemaleHoles_bottom,
									chanelPoints: face0.chanelPoints_bottom,
									lockdowelPoints:
										face0.lockdowelPoints_bottom,
									tool_path: face0.tool_path_bottom,
									pocket_path: face0.pocket_path_bottom,
									custom_path: face0.custom_path_bottom
								},
								thick: self.sheet_thick.to_f,
								material: mat_name,
								color: mat_color
							}
						)
					else
						return(
							{
								both_side: face0.has_hole? && face1.has_hole?,
								id: Utilities.CreatUniqueid,
								label: self.label_without_id,
								group_label: self.group_label,
								part_label: self.get_name_without_id,
								numbering: self.get_numbering_id,
								connector_sum: {},
								top: {
									points: face1.points,
									innerLoop: face1.innerLoop,
									curveEdges: face1.curveEdges,
									curveEdges_inner: face1.curveEdges_inner,
									dogboneMaleHoles: face1.dogboneMaleHoles,
									dogboneFemaleHoles:
										face1.dogboneFemaleHoles,
									holePoints: face1.holePoints,
									lockdowelPoints: face1.lockdowelPoints,
									chanelPoints: face1.chanelPoints,
									edgeBandingMark: face1.edgeBandingMark,
									edgeBandingMark_inner:
										face1.edgeBandingMark_inner,
									dimension: self.dimension(big_faces[1]),
									tool_path: face1.tool_path,
									pocket_path: face1.pocket_path,
									custom_path: face1.custom_path
								},
								bottom: {
									holePoints: face1.holePoints_bottom,
									dogboneFemaleHoles:
										face1.dogboneFemaleHoles_bottom,
									chanelPoints: face1.chanelPoints_bottom,
									lockdowelPoints:
										face1.lockdowelPoints_bottom,
									tool_path: face1.tool_path_bottom,
									pocket_path: face1.pocket_path_bottom,
									custom_path: face1.custom_path_bottom
								},
								thick: self.sheet_thick.to_f,
								material: mat_name,
								color: mat_color
							}
						)
					end
				end
			end
			def get_numbering_id
				self.comp.get_attribute('amtf_dict', 'amtf_bukong_id')
			end
			def sheet_id
				self.comp.get_attribute('amtf_dict', 'sheet_id')
			end
			def label
				prefix = group_label
				label = prefix.to_s + ' - ' + @comp.name.to_s
			end
			def group_label
				prefix = self.comp.get_attribute('amtf_dict', 'prefix')
				group_name = prefix.to_s if !prefix.nil?
			end
			def part_label
				@comp.name.to_s
			end
			def label_without_id
				prefix = group_label
				label = prefix.to_s + ' - ' + self.get_name_without_id
			end
			def dimension(face)
				vertices = face.vertices
				bb = Geom::BoundingBox.new
				points = []
				vertices.each do |v|
					points.push(
						Utilities.point_context_convert(
							v.position,
							@trans,
							IDENTITY
						)
					)
				end
				origin =
					Utilities.point_context_convert(
						face.vertices.first.position,
						@trans,
						IDENTITY
					)
				grain = 获取长边_vector(face)
				yaxis =
					Utilities.vector_context_convert(grain, @trans, IDENTITY)
						.normalize
				zaxis =
					Utilities.vector_context_convert(
						face.normal,
						@trans,
						IDENTITY
					).normalize
				xaxis = yaxis.cross(zaxis).normalize
				tr = Geom::Transformation.new(xaxis, yaxis, zaxis, origin)
				points.map! do |p|
					Utilities.point_context_convert(p, IDENTITY, tr)
				end
				bb.add(points)
				return { width: bb.width, height: bb.height, depth: bb.depth }
			end
			def color
				mat = self.material
				return mat.color.to_a if mat
			end
			def mat_and_color
				mat = self.material
				return { color: mat.color.to_a, name: mat.name } if mat
			end
			def material
				return @comp.material if @comp.material
				mat = AmtfMath.get_material(@comp)
			end
			def mat_gan_nhan
				big_faces = self.get_big_faces
				comp = self.comp
				side_face_array = self.edge_face
				children = grep_connector
				face0 =
					AmtfFace.new(
						big_faces[0],
						big_faces[1],
						side_face_array,
						@trans
					)
				face1 =
					AmtfFace.new(
						big_faces[1],
						big_faces[0],
						side_face_array,
						@trans
					)
				if children && children.size > 0
					children.each do |hash_obj|
						type = hash_obj[:type]
						group = hash_obj[:instance]
						孔重心点 = hash_obj[:hole]
						config_data = hash_obj[:config_data]
						connector_type_id = hash_obj[:connector_type_id]
						minifixID = hash_obj[:minifixID]
						radius = group.get_attribute 'amtf_dict', 'radius'
						radius = radius.to_f.to_l
						face0.insert_holes(
							孔重心点,
							type,
							radius,
							config_data,
							connector_type_id,
							minifixID
						)
						face1.insert_holes(
							孔重心点,
							type,
							radius,
							config_data,
							connector_type_id,
							minifixID
						)
					end
				end
				chanels = grep_channel
				if chanels && chanels.size > 0
					chanels.each do |c|
						face0.insert_chanel(c)
						face1.insert_chanel(c)
					end
				end
				locdowel = grep_lockdowel
				if locdowel && locdowel.size > 0
					locdowel.each do |c|
						face0.insert_lockdowel(c)
						face1.insert_lockdowel(c)
					end
				end
				# Tiêu chí 1: Mặt không chứa liên kết
				if !face0.has_hole?
					return big_faces[0]
				elsif !face1.has_hole?
					return big_faces[1]
				end
				# Tiêu chí 2: Mặt hướng lên trên
				z_asis = Geom::Vector3d.new [0, 0, 1]
				normal0 =
					big_faces[0].normal.transform(
						self.comp.transformation.inverse
					)
				normal1 =
					big_faces[1].normal.transform(
						self.comp.transformation.inverse
					)
				return big_faces[1] if normal1.dot(z_asis) > 0
				return big_faces[0] if normal0.dot(z_asis) > 0
				# Tiêu chí 3: Mặt hướng ngược lại trục Y
				y_asis = Geom::Vector3d.new [0, 1, 0]
				return big_faces[0] if normal0.dot(y_asis) < 0
				return big_faces[1] if normal1.dot(y_asis) < 0
				return big_faces[0]
			end
			def sheet_thick
				AmtfMath.thickness(self.comp, @trans)
			end

			def 获取板信息()
				大面=big_faces[0]
				孔重心点 = Utilities.find_center_face(大面)
				长边_vector = self.获取长边_vector(大面).normalize
				长边半长线 = [孔重心点, 长边_vector]
				edges = 大面.edges
				长边方向交点s = []
				for i in 0..edges.size - 1
					intrP = Utilities.intersect_line_edge(长边半长线, edges[i])
					长边方向交点s << intrP if !intrP.nil?
				end
				长边方向交点s = Utilities.uniq_points(长边方向交点s)
				return unless 长边方向交点s.size > 0
				长边长 = 长边方向交点s[0].distance(长边方向交点s[1])

				边界中心点 = Utilities.get_midpoint(长边方向交点s[0], 长边方向交点s[1])
				# Đường vuông góc, sau đó cho cắt các cạnh đ�?tính khoảng cách
				# 将正方形对角线垂直切割成两段，然后测量它们之间的距离👇。
				面上任意点 = Utilities.point_at_face(大面)
				prj_point = 面上任意点.project_to_line(长边半长线)
				短边_vector = prj_point.vector_to(面上任意点).normalize
				短边_line = [孔重心点, 短边_vector]
				# Cho 垂直 cắt edges đ�?tìm ra khoảng ngăn
				短边边方向交点s = []
				for i in 0..edges.size - 1
					intrP =
						Utilities.intersect_line_edge(短边_line, edges[i])
					短边边方向交点s << intrP if !intrP.nil?
				end
				短边边方向交点s = Utilities.uniq_points(短边边方向交点s)
				短边长 = 短边边方向交点s[0].distance(短边边方向交点s[1])
				板h=
				{
					边界中心点: 边界中心点,
					长边长: 长边长,
					短边长: 短边长,
					长边_vector: 长边_vector,
					孔重心点: 孔重心点
				}
				return 板h
			end
			
			def drawArrow(pick_face)
				板h=获取板信息()
				长边长=板h[:长边长]
				短边长=板h[:短边长]
				边界中心点=板h[:边界中心点]
				长边_vector=板h[:长边_vector]
				孔重心点=板h[:孔重心点]

				# 标签长 = 0.9 * 长边长
				标签长 =长边长-100.mm
				if 标签长<80.mm
					标签长 = 0.9 * 长边长
				end
				
				箭头宽 = 0.3*短边长
				if 箭头宽>40.mm
					箭头宽=40.mm
				end
				priority_type =
					pick_face.get_attribute('amtf_dict', 'priority_type')
				arrow =
					ArrowModel.new(
						{
							origin: 边界中心点,
							标签长: 标签长,
							箭头宽: 箭头宽,
							长边长: 长边长,
							短边长: 短边长,
							parent: self.comp,
							trans: self.trans,
							z_axis: pick_face.normal,
							x_axis: 长边_vector,
							priority_type: priority_type,
							孔重心点: 孔重心点
						}
					)
				arrow.drawModel
			end

			def get_numbering_id
				return self.comp.get_attribute('amtf_dict', 'amtf_bukong_id')
			end
			def add_id_to_name(id)
				name = @comp.name
				# name = name.to_s.gsub(/\[\d*\]/, '')
				name = name.to_s.gsub(/__(\d+). /, '')
				name = "__#{id.to_s}. #{name}"
				@comp.name = name
			end
			def get_name_without_id
				name = @comp.name
				name = name.to_s.gsub(/\[\d*\]/, '')
				return name
			end
			def edge_center_within_other?(e2)
				e1_edges_face = self.edge_face
				cen_1_in_e2 = false
				e1_edges_face.each do |eg_face|
					cen =
						Utilities.point_context_convert(
							Utilities.find_center_face(eg_face),
							@trans,
							e2.parent_tr
						)
					if Utilities.within?(cen, e2.comp, false, true)
						cen_1_in_e2 = true
						break
					end
				end
				return cen_1_in_e2
			end
			def intersection_with(e2)
				entities1 = Utilities.definition(@comp).entities
				entities2 = Utilities.definition(e2.comp).entities
				temp_group = Sketchup.active_model.entities.add_group
				temp_group.transform!(e2.trans)
				entities1.intersect_with(
					false,
					Utilities.transform_context_convert(@trans, e2.trans),
					temp_group.entities,
					IDENTITY,
					true,
					Utilities.find_mesh_geometry(entities2)
				)
				return temp_group
			end
			def comp_type
				@comp.get_attribute('amtf_dict', 'comp_type')
			end
			def sheet_type
				@comp.get_attribute('amtf_dict', 'sheet_type')
			end
			def sheet_id
				@comp.get_attribute('amtf_dict', 'sheet_id')
			end
			def get_oposit(face)
				bigs = self.get_big_faces
				if face == bigs[0]
					return bigs[1]
				elsif face == bigs[1]
					return bigs[0]
				end
				nil
			end
		end
	end
end