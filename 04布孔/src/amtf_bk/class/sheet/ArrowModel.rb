# encoding: UTF-8
module AMTF
	module BUKONG
		class ArrowModel
			attr_accessor :modal
			def initialize(data)
				@parent = data[:parent]
				@trans = data[:trans]
				# puts "IDENTITY 👇"
				# puts IDENTITY
				# str4x4 = IDENTITY.to_a.each_slice(4).inject { |str, row| "#{str}\r\n#{row}" }
				# puts str4x4
				# amtf.nil
				@z_axis =Utilities.vector_context_convert(data[:z_axis],@trans,IDENTITY)
				@z_axis.reverse!
					# IDENTITY代表一个不进行任何变换的4x4矩阵。IDENTITY.inverse表示这个矩阵的逆矩阵，也就是一个仍然不进行任何变换的4x4矩阵。
				@x_axis =Utilities.vector_context_convert(data[:x_axis],@trans,IDENTITY)
				@y_axis = @x_axis.cross(@z_axis)
				@origin =Utilities.point_context_convert(data[:origin],@trans,IDENTITY)
				@标签长 = data[:标签长]
				@箭头宽 = data[:箭头宽]
				@长边长 = data[:长边长]
				@短边长 = data[:短边长]
				@priority_type = data[:priority_type]
				@center = data[:center]
			end
			def set_attribute(dict, key, val)
				return unless @modal
				@modal.set_attribute(dict, key, val)
			end
			def transform!(tr)
				@modal.transform!(tr)
			end
			def drawModel
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.set_attribute 'amtf_dict', 'type', 'bukong_label'
				@modal.transform!(Geom::Transformation.new(@origin, @x_axis, @y_axis))
				@modal.transform!(@trans.inverse)
				new_layer = Sketchup.active_model.layers.add('Amtf_label')
				@modal.layer = new_layer
				@modal.name = "标签"
				tr_箭头终点 =Geom::Transformation.new(Utilities.vector_multiply(0.5 * @标签长,Geom::Vector3d.new(1, 0, 0)))
				# tr_箭头三角末端 =Geom::Transformation.new(Utilities.vector_multiply(-0.6* @箭头宽,Geom::Vector3d.new(1, 0, 0)))
				# tr_箭头三角末端 =Geom::Transformation.new(
				# 		Utilities.vector_multiply(
				# 			0.5 * @标签长+0.5 * @箭头宽,
				# 			Geom::Vector3d.new(1, 0, 0)
				# 		)
				# 	)
				tr_a =Geom::Transformation.new(Utilities.vector_multiply(0.5 * @标签长,Geom::Vector3d.new(-1, 0, 0)))
				point_a = ORIGIN.transform(tr_a)
				箭头终点 = ORIGIN.transform(tr_箭头终点)
				中点=Utilities.get_midpoint(point_a,箭头终点)
				puts "中点"
				puts 中点

				tr_箭头三角末端 =Geom::Transformation.new(Utilities.vector_multiply(-2* @箭头宽,Geom::Vector3d.new(1, 0, 0)))
				point_箭头三角末端 = 箭头终点.transform(tr_箭头三角末端)

				line_ab = @modal.entities.add_line(point_a, 箭头终点)
				# amtf.nil
				line_ab.set_attribute('amtf_dict', 'grain_direction', true)
				tr_d =Geom::Transformation.new(Utilities.vector_multiply(@箭头宽,Geom::Vector3d.new(0, -1, 0)))
				tr_e =Geom::Transformation.new(Utilities.vector_multiply(@箭头宽,Geom::Vector3d.new(0, 1, 0)))
				point_d = point_箭头三角末端.transform(tr_d)
				point_e = point_箭头三角末端.transform(tr_e)
				# point_c = 箭头终点.transform(tr_c)
				# @modal.entities.add_line(point_d, point_e)
				# amtf.nil
				@modal.entities.add_line(point_d, 箭头终点)
				@modal.entities.add_line(point_e, 箭头终点)
				# amtf.nil
				
				comp_name = @parent.name
				编号=Utilities.解读编号(comp_name)
				priority_text = "<#{OB[:priority]}: 0>"
				if @priority_type === 'second_priority'
					priority_text = "<#{OB[:priority]}: 2>"
				elsif @priority_type === 'first_priority'
					priority_text = "<#{OB[:priority]}: 1>"
				end
				prefix = @parent.get_attribute('amtf_dict', 'prefix')
				group_name = prefix.to_s if !prefix.nil?
				grain_angle_text =
					@parent.get_attribute('amtf_dict', 'custom_grain')
				grain_angle_text = '0' if grain_angle_text.nil?
				groove_text = @parent.get_attribute('amtf_dict', 'groove_name')
				groove_text = "(#{groove_text})" if groove_text
				# label_text =
				# 	"<#{OB[:angle]}: #{grain_angle_text}> #{priority_text}\n(#{编号}) - #{group_name}\n#{groove_text}"
				label_text ="(#{编号})"
				# 中点=Utilities.get_midpoint(
				# 			point_a,
				# 			箭头终点
				# 		)
				text_org =中点.transform(Geom::Transformation.new(
							Utilities.vector_multiply(
								0.1 * @箭头宽,
								# Y_AXIS.reverse
								Y_AXIS
							)
						)
					)
				@text_modal = @modal.entities.add_group
				puts "text_org"
				puts text_org
				@text_modal.transform!(Geom::Transformation.new(text_org, X_AXIS, Y_AXIS))
				return if !label_text
				# amtf.nil
				font = AmtfMath.get_font
				# amtf.nil
				font = '楷体' if font.nil?
				@text_modal.entities.add_3d_text(
					label_text,
					TextAlignCenter,
					font,
					false,
					false,
					0.3 * @箭头宽,
					0,
					0,
					false,
					0
				)
				# amtf.nil

				textBounds = @text_modal.bounds
				dimension = [
					textBounds.height,
					textBounds.width,
					textBounds.depth
				]
				# puts textBounds.corner(0)
				文字center = textBounds.center
				puts "文字center"
				puts 文字center
				puts 文字center[0]
				dimension = dimension.sort
				text_width = dimension[2]
				text_height = dimension[1]
				project1 = text_org.project_to_line([ORIGIN, Y_AXIS])
				puts "text_width"
				puts text_width
				puts text_height

				# trk=Utilities.vector_multiply(-0.5 * text_width,X_AXIS)
				trk=Utilities.vector_multiply(-文字center[0],X_AXIS)
				puts "trk"
				puts trk
				tr到中心 =Geom::Transformation.new(trk)
				@text_modal.transform!(tr到中心)
				ratio = 1
				# dist_de = point_d.distance(point_e)
				# if text_width > @标签长
				# 	ratio = [@标签长 / text_width, dist_de / text_height].min
				# else
				# 	ratio = [@标签长 / text_width, dist_de / text_height].min
				# end
				# amtf.nil

				ratio=(@短边长*0.4-0.1 * @箭头宽)/text_height
				scale = Geom::Transformation.scaling(text_org, ratio)
				@text_modal.transform!(scale)
				# amtf.nil

				# mov_dist = (@标签长 - text_width) / 2
				# mov_dist = mov_dist.abs
				# # amtf.nil
				# mov_vect =
				# targetPoint =
				# 	Utilities.get_midpoint(
				# 		point_a,
				# 		箭头终点
				# 	)
				# @text_modal.transform!(
				# 	Geom::Transformation.new point_c.vector_to(targetPoint)
				# )
			end
		end
	end
end