# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfOctree
			MAX_OBJECTS = 10
			MAX_LEVELS = 6
			attr_accessor :nodes, :bounds, :objects
			def initialize(pLevel, pBounds)
				@level = pLevel
				@objects = []
				@bounds = pBounds
				@nodes = []
				@devided = false
			end
			def query_related_part(part)
				result = []
				return result unless Utilities.isOverlap?(@bounds, part.bounds)
				result += @objects
				# Not subdivided
				return result unless @devided
				result += @nodes[0].query_related_part(part)
				result += @nodes[1].query_related_part(part)
				result += @nodes[2].query_related_part(part)
				result += @nodes[3].query_related_part(part)
				result += @nodes[4].query_related_part(part)
				result += @nodes[5].query_related_part(part)
				result += @nodes[6].query_related_part(part)
				result += @nodes[7].query_related_part(part)
				return result
			end
			def insert(part)
				return false unless Utilities.isOverlap?(@bounds, part.bounds)
				if (@level > MAX_LEVELS)
					@objects.push(part)
					part.addRelated(@objects)
					return true
				end
				if @objects.size < MAX_OBJECTS && !@devided
					@objects.push(part)
					part.addRelated(@objects)
					return true
				end
				subdivide unless @devided
				@nodes[0].insert(part)
				@nodes[1].insert(part)
				@nodes[2].insert(part)
				@nodes[3].insert(part)
				@nodes[4].insert(part)
				@nodes[5].insert(part)
				@nodes[6].insert(part)
				@nodes[7].insert(part)
				# Khi đã chia thì copy của cha vào con, như vậy thì ch�?có lá là chứa part
				for i in 0..@objects.size - 1
					@nodes[0].insert(@objects[i])
					@nodes[1].insert(@objects[i])
					@nodes[2].insert(@objects[i])
					@nodes[3].insert(@objects[i])
					@nodes[4].insert(@objects[i])
					@nodes[5].insert(@objects[i])
					@nodes[6].insert(@objects[i])
					@nodes[7].insert(@objects[i])
				end
				@objects.clear
			end
			def subdivide
				bbox = self.buildBBox
				@nodes[0] = AmtfOctree.new(@level + 1, bbox[:bottom_back_left])
				@nodes[1] = AmtfOctree.new(@level + 1, bbox[:bottom_front_left])
				@nodes[2] = AmtfOctree.new(@level + 1, bbox[:bottom_back_right])
				@nodes[3] =
					AmtfOctree.new(@level + 1, bbox[:bottom_front_right])
				@nodes[4] = AmtfOctree.new(@level + 1, bbox[:top_back_left])
				@nodes[5] = AmtfOctree.new(@level + 1, bbox[:top_front_left])
				@nodes[6] = AmtfOctree.new(@level + 1, bbox[:top_back_right])
				@nodes[7] = AmtfOctree.new(@level + 1, bbox[:top_front_right])
				@devided = true
				# self.draw_bb1(bbox[:bottom_back_left])
				# self.draw_bb1(bbox[:bottom_front_left])
				# self.draw_bb1(bbox[:bottom_back_right])
				# self.draw_bb1(bbox[:top_back_left])
				# self.draw_bb1(bbox[:top_front_left])
				# self.draw_bb1(bbox[:top_back_right])
				# self.draw_bb1(bbox[:top_front_right])
			end
			def buildBBox
				pts = Utilities.bound_points(@bounds)
				pBtmCenter =
					Utilities.get_midpoint(
						Utilities.get_midpoint(pts[0], pts[1]),
						Utilities.get_midpoint(pts[2], pts[3])
					)
				pTopCenter =
					Utilities.get_midpoint(
						Utilities.get_midpoint(pts[4], pts[5]),
						Utilities.get_midpoint(pts[6], pts[7])
					)
				ptCenter = Utilities.get_midpoint(pBtmCenter, pTopCenter)
				ptCenterLeft =
					Utilities.get_midpoint(
						Utilities.get_midpoint(pts[4], pts[5]),
						Utilities.get_midpoint(pts[0], pts[1])
					)
				ptCenterRight =
					Utilities.get_midpoint(
						Utilities.get_midpoint(pts[6], pts[7]),
						Utilities.get_midpoint(pts[2], pts[3])
					)
				ptBackCenter =
					Utilities.get_midpoint(
						Utilities.get_midpoint(pts[0], pts[4]),
						Utilities.get_midpoint(pts[2], pts[6])
					)
				ptFrontCenter =
					Utilities.get_midpoint(
						Utilities.get_midpoint(pts[3], pts[7]),
						Utilities.get_midpoint(pts[1], pts[5])
					)
				###
				bottom_back_left = Geom::BoundingBox.new
				pt = []
				pt[0] = pts[0]
				pt[1] = Utilities.get_midpoint(pts[0], pts[1])
				pt[2] = Utilities.get_midpoint(pts[0], pts[2])
				pt[3] = pBtmCenter
				pt[4] = Utilities.get_midpoint(pts[0], pts[4])
				pt[5] = ptCenterLeft
				pt[6] = ptBackCenter
				pt[7] = ptCenter
				bottom_back_left.add(pt)
				###
				bottom_front_left = Geom::BoundingBox.new
				pt = []
				pt[0] = Utilities.get_midpoint(pts[0], pts[1])
				pt[1] = pts[1]
				pt[2] = pBtmCenter
				pt[3] = Utilities.get_midpoint(pts[1], pts[3])
				pt[4] = ptCenterLeft
				pt[5] = Utilities.get_midpoint(pts[1], pts[5])
				pt[6] = ptCenter
				pt[7] = ptFrontCenter
				bottom_front_left.add(pt)
				###
				bottom_back_right = Geom::BoundingBox.new
				pt = []
				pt[0] = Utilities.get_midpoint(pts[0], pts[2])
				pt[1] = Utilities.get_midpoint(pts[1], pts[3])
				pt[2] = Utilities.get_midpoint(pts[2], pts[3])
				pt[3] = pts[3]
				pt[4] = ptCenter
				pt[5] = ptFrontCenter
				pt[6] = ptCenterRight
				pt[7] = Utilities.get_midpoint(pts[7], pts[3])
				bottom_back_right.add(pt)
				###
				bottom_front_right = Geom::BoundingBox.new
				pt = []
				pt[0] = pBtmCenter
				pt[1] = Utilities.get_midpoint(pts[1], pts[3])
				pt[2] = Utilities.get_midpoint(pts[2], pts[3])
				pt[3] = pts[3]
				pt[4] = ptCenter
				pt[5] = ptFrontCenter
				pt[6] = ptCenterRight
				pt[7] = Utilities.get_midpoint(pts[7], pts[3])
				bottom_front_right.add(pt)
				###
				top_back_left = Geom::BoundingBox.new
				pt = []
				pt[0] = Utilities.get_midpoint(pts[0], pts[4])
				pt[1] = ptCenterLeft
				pt[2] = ptBackCenter
				pt[3] = ptCenter
				pt[4] = pts[4]
				pt[5] = Utilities.get_midpoint(pts[5], pts[4])
				pt[6] = Utilities.get_midpoint(pts[6], pts[4])
				pt[7] = pTopCenter
				top_back_left.add(pt)
				###
				top_front_left = Geom::BoundingBox.new
				pt = []
				pt[0] = ptCenterLeft
				pt[1] = Utilities.get_midpoint(pts[5], pts[1])
				pt[2] = ptCenter
				pt[3] = ptFrontCenter
				pt[4] = Utilities.get_midpoint(pts[5], pts[4])
				pt[5] = pts[5]
				pt[6] = pTopCenter
				pt[7] = Utilities.get_midpoint(pts[5], pts[7])
				top_front_left.add(pt)
				###
				top_back_right = Geom::BoundingBox.new
				pt = []
				pt[0] = ptBackCenter
				pt[1] = ptCenter
				pt[2] = Utilities.get_midpoint(pts[2], pts[6])
				pt[3] = ptCenterRight
				pt[4] = Utilities.get_midpoint(pts[4], pts[6])
				pt[5] = pTopCenter
				pt[6] = pts[6]
				pt[7] = Utilities.get_midpoint(pts[7], pts[6])
				top_back_right.add(pt)
				###
				top_front_right = Geom::BoundingBox.new
				pt = []
				pt[0] = ptCenter
				pt[1] = ptFrontCenter
				pt[2] = ptCenterRight
				pt[3] = Utilities.get_midpoint(pts[7], pts[3])
				pt[4] = pTopCenter
				pt[5] = Utilities.get_midpoint(pts[7], pts[5])
				pt[6] = Utilities.get_midpoint(pts[7], pts[7])
				pt[7] = pts[7]
				top_front_right.add(pt)
				return(
					{
						bottom_back_left: bottom_back_left,
						bottom_front_left: bottom_front_left,
						bottom_front_right: bottom_front_right,
						bottom_back_right: bottom_back_right,
						top_back_left: top_back_left,
						top_front_left: top_front_left,
						top_back_right: top_back_right,
						top_front_right: top_front_right
					}
				)
			end
			def draw_bb1(bbox)
				puts 'Draw heheheheh'
				draw_faces = false
				model = Sketchup.active_model
				ents = model.active_entities
				pts = self.bound_points(bbox)
				# .map { |pt| pt.transform( IDENTITY) }
				if pts.length == 8
					if draw_faces
						faces = [
							[pts[0], pts[2], pts[3], pts[1]],
							[pts[0], pts[4], pts[5], pts[1]],
							[pts[0], pts[4], pts[6], pts[2]],
							[pts[2], pts[6], pts[7], pts[3]],
							[pts[3], pts[7], pts[5], pts[1]],
							[pts[4], pts[6], pts[7], pts[5]]
						]
					else
						faces = [
							[pts[0], pts[1]],
							[pts[1], pts[3]],
							[pts[3], pts[2]],
							[pts[2], pts[0]],
							[pts[4], pts[5]],
							[pts[5], pts[7]],
							[pts[7], pts[6]],
							[pts[6], pts[4]],
							[pts[0], pts[4]],
							[pts[1], pts[5]],
							[pts[2], pts[6]],
							[pts[3], pts[7]]
						]
					end
				else
					if draw_faces
						faces = [pts]
					else
						faces = [
							[pts[0], pts[1]],
							[pts[1], pts[2]],
							[pts[2], pts[3]],
							[pts[3], pts[0]]
						]
					end
				end
				faces.each do |face|
					if draw_faces
						ents.add_face(face)
					else
						face.each { |pt| ents.add_cpoint(pt) }
						ents.add_cline(face[0], face[1])
					end
				end
			end
			def bound_points(bound)
				if bound.width == 0
					[0, 2, 6, 4].map { |i| bound.corner(i) }
				elsif bound.height == 0
					[0, 1, 5, 4].map { |i| bound.corner(i) }
				elsif bound.depth == 0
					[0, 1, 3, 2].map { |i| bound.corner(i) }
				else
					(0..7).map { |i| bound.corner(i) }
				end
			end
		end
	end
end