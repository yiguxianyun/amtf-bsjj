# encoding: UTF-8
module AMTF
    module BUKONG 
        class AmtfSheetHelper
            attr_accessor   :corners, 
                            :origin,
                            :selected_point,
                            :adjust_distance
            def initialize( vertices, matrix, converted = false )
                if !converted
                    @init_conners = vertices.map{
                        |p|
                        p = Geom::Point3d.new Utilities.unitConvert(p['x']), 
                                            Utilities.unitConvert(p['y']), 
                                            Utilities.unitConvert(p['z']);
                    }
                else
                    @init_conners = vertices.clone()
                end
                @corners = @init_conners.clone()
                @origin = @init_conners[0]
                @matrix = matrix
                @rightPoint = Utilities.find_center_of_points([@init_conners[1], @init_conners[2], @init_conners[5], @init_conners[6]])
                @leftPoint = Utilities.find_center_of_points([@init_conners[0], @init_conners[3], @init_conners[4], @init_conners[7]])
                @bottomPoint = Utilities.find_center_of_points([@init_conners[0], @init_conners[1], @init_conners[2], @init_conners[3]])
                @topPoint = Utilities.find_center_of_points([@init_conners[4], @init_conners[5], @init_conners[6], @init_conners[7]])
                @frontPoint = Utilities.find_center_of_points([@init_conners[2], @init_conners[3], @init_conners[6], @init_conners[7]])
                @backPoint = Utilities.find_center_of_points([@init_conners[0], @init_conners[1], @init_conners[5], @init_conners[4]])                
                @scale_point =[
                    @rightPoint, 
                    @leftPoint, 
                    @bottomPoint, 
                    @topPoint, 
                    @frontPoint, 
                    @backPoint]
                @selected_point = nil
                @scale_point = @scale_point.concat(@corners)
            end
        end
    end
end
