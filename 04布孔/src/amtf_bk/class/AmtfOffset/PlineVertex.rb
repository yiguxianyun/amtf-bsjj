# encoding: UTF-8
module AMTF
	module BUKONG
		class PlineVertex
			attr_accessor :x, :y, :bulge
			def initialize(point, bulge)
				@x = point.x
				@y = point.y
				@bulge = bulge
			end
		end
		class PlineOffsetSegment
			attr_accessor :v1, :v2, :origV2Pos, :collapsedArc
			def initialize(attrs)
				@v1 = attrs[:v1]
				@v2 = attrs[:v2]
				@origV2Pos = attrs[:origV2Pos]
				@collapsedArc = attrs[:collapsedArc]
			end
		end
		class AABB
			attr_accessor :xMin, :yMin, :xMax, :yMax
			def initialize(xMin, yMin, xMax, yMax)
				@xMin = xMin
				@yMin = yMin
				@xMax = xMax
				@yMax = yMax
			end
			def expand(val)
				@xMin -= val
				@yMin -= val
				@xMax += val
				@yMax += val
			end
		end
	end
end