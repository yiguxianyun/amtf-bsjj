# encoding: UTF-8
module AMTF
	module BUKONG
		#     function lineVisitor(v1, v2) {
		# 	result.push(new PlineOffsetSegment({}))
		# 	let seg = result[result.length - 1]
		# 	seg.collapsedArc = false
		# 	seg.origV2Pos = v2
		# 	let edge = vertexPostMinus(v2, v1)
		# 	let offsetV = vertexMultiply(unitPerp(edge), offset)
		# 	seg.v1 = vertexPlus(v1, offsetV)
		# 	seg.v1.bulge = v1.bulge
		# 	seg.v2 = vertexPlus(v2, offsetV)
		# 	seg.v2.bulge = v2.bulge
		# }
		#
		class CreateUntrimmedOffsetSegments
			def initialize(pline, offset)
				@result = []
				@pline = pline
				@offset = offset
			end
			def lineVisitor(v1, v2)
				result.push(PlineOffsetSegment.new({}))
				seg = result[result.length - 1]
				seg.collapsedArc = false
				seg.origV2Pos = v2
				edge = v2 - v1
			end
		end
	end
end