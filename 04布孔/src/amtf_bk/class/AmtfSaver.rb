# encoding: UTF-8
require 'openssl'
require 'uri'
require 'base64'
require 'json'
module AMTF
	module BUKONG
		ALPHABET ='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
		ENCODING ='MOhqm0PnycUZeLdK8YvDCgNfb7FJtiHT52BrxoAkas9RWlXpEujSGI64VzQ31w'
		class AmtfSaver
			def initialize(); end
			def encode(text)
				text.tr(ALPHABET, ENCODING)
			end
			def decode(text)
				text.tr(ENCODING, ALPHABET)
			end
			def save_comp_json(comp, data)
				return unless comp && comp.valid?
				file_data = Base64.encode64(JSON.generate(data))
				encrypted = encode(file_data)
				comp.set_attribute 'amtf_dict', 'model', encrypted
			end
			def open_comp_json(comp)
				return unless comp && comp.valid?
				model_data = comp.get_attribute 'amtf_dict', 'model'
				return if model_data.nil?
				str = decode(model_data)
				data_hash = JSON.parse(Base64.decode64(str))
				return data_hash
			end
			def save_config(data)
				file_data = Base64.encode64(JSON.generate(data))
				encrypted = encode(file_data)
				Sketchup.write_default('Amtflogic', 'config4', encrypted)
			end
			def open_config()
				data = Sketchup.read_default('Amtflogic', 'config4', nil)
				# puts "data.nil?"
				# puts data.nil?
				# return {} if data.nil? || data.empty?
				return nil if data.nil? || data.empty?
				str = decode(data)
				data_hash = JSON.parse(Base64.decode64(str))
				return data_hash
			end
			# def save_json()
			# model = Sketchup.active_model
			# data = {}
			# model.entities.each{
			#     |ent|
			#     comp_type = ent.get_attribute 'amtf_dict', 'comp_type'
			#     comp_id = ent.get_attribute 'amtf_dict', 'comp_id'
			#     if comp_type == 'AmtfFrame'
			#         if AMTF_STORE.frame_premitives[comp_id]
			#             data[ comp_id ] = AMTF_STORE.frame_premitives[comp_id].root
			#         end
			#     end
			# }
			# file_data = Base64.encode64(JSON.generate( data ))
			# encrypted =  encode(file_data)
			# model.set_attribute 'amtf_dict', 'model', encrypted
			# end
			# def open_json()
			# model = Sketchup.active_model
			# model_data = model.get_attribute 'amtf_dict', 'model'
			# return if model_data.nil?
			# str = decode(model_data)
			# data_hash = JSON.parse(Base64.decode64(str))
			# if data_hash
			#     model.entities.each{
			#         |ent|
			#         comp_type = ent.get_attribute 'amtf_dict', 'comp_type'
			#         comp_id = ent.get_attribute 'amtf_dict', 'comp_id'
			#         if comp_type == 'AmtfFrame' && comp_id
			#             if  data_hash.key?( comp_id )
			#                 model_data = data_hash[comp_id]
			#                 frm = AmtfFrame.new
			#                 frm.init( model_data, ent )
			#             end
			#         end
			#     }
			# end
			# end
			def load_comp
				query = AmtfQuery.new
				frames = query.query_frame(Sketchup.active_model.entities)
				frames.each do |ent|
					attrs = open_comp_json(ent)
					next unless attrs
					frm = AmtfFrame.new
					frm.init(attrs, ent)
				end
			end
		end
	end
end