# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfSide
			attr_accessor :points, :holePoints
			def initialize(attrs)
				@face = attrs[:face]
				@comp_trans = attrs[:trans]
				@executing_error = attrs[:executing_error] || []
				@points = []
				@holePoints = []
			end
			def outer_loop
				@face.outer_loop
			end
			def get_points(the_loop)
				vertices = the_loop.vertices
				hash =
					vertices.map do |v|
						Utilities.point_context_convert(
							v.position,
							@comp_trans,
							IDENTITY
						)
					end
				return hash
			end
			def insert_holes(
				center,
				type,
				radius,
				config_data,
				connector_type_id,
				minifixID
			)
				return if radius <= 0
				if Utilities.within_face?(center, @face)
					@holePoints.push(
						{
							center:
								Utilities.point_context_convert(
									center,
									@comp_trans,
									IDENTITY
								),
							type: type,
							radius: radius,
							config_data: config_data,
							connector_type_id: connector_type_id,
							minifixID: minifixID
						}
					)
				end
			end #insert hole
			def get_origin
				org = @face.vertices.first.position
				return(
					Utilities.point_context_convert(org, @comp_trans, IDENTITY)
				)
			end
			def convert_to_root_axes(grain_vector, origin = nil)
				yaxis =
					Utilities.vector_context_convert(
						grain_vector,
						@comp_trans,
						IDENTITY
					).normalize
				zaxis =
					Utilities.vector_context_convert(
						@big_face.normal,
						@comp_trans,
						IDENTITY
					).normalize
				xaxis = yaxis.cross(zaxis).normalize
				origin = get_origin if origin.nil?
				tr = Geom::Transformation.new(xaxis, yaxis, zaxis, origin)
				@holePoints =
					@holePoints.map do |obj|
						obj = {
							center:
								Utilities.point_context_convert(
									obj[:center],
									IDENTITY,
									tr
								),
							type: obj[:type],
							radius: obj[:radius],
							config_data: obj[:config_data],
							connector_type_id: obj[:connector_type_id],
							minifixID: obj[:minifixID]
						}
					end
				points = get_points(outer_loop)
				@points =
					points.map do |p|
						Utilities.point_context_convert(p, IDENTITY, tr)
					end
			end
		end
	end
end