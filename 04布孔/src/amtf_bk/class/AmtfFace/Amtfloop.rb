# encoding: UTF-8
module AMTF
	module BUKONG
		# X�?lý dán cạnh
		# Chạy qua loop_0. Kiểm tra xem mặt cạnh nào chứa edge này và nó có dán cạnh không
		# Nếu có:
		# Lấy offset_vector
		# Offset 2 đầu mút của , tìm ra line song song, cách cạnh vào phía trong bằng đ�?dầy dán cạnh
		# Tìm giao điểm của line này với edge next và edge previous
		# loops là mảng hash chứa {:edge => {:start => point3D , :end => point3D}, :offsetVector => ..., :edgeBanding => true, :done => false }
		class AmtfFace
			attr_accessor :big_face,
			              :points,
			              :innerLoop,
			              :curveEdges,
			              :curveEdges_inner,
			              :holePoints,
			              :holePoints_bottom,
			              :lockdowelPoints,
			              :lockdowelPoints_bottom,
			              :chanelPoints,
			              :chanelPoints_bottom,
			              :tool_path,
			              :tool_path_bottom,
			              :pocket_path,
			              :pocket_path_bottom,
			              :edgeBandingMark,
			              :edgeBandingMark_inner,
			              :custom_path,
			              :custom_path_bottom,
			              :dogbone_male,
			              :dogbone_male_bottom,
			              :dogboneMaleHoles,
			              :dogboneFemaleHoles,
			              :dogboneFemaleHoles_bottom
			def initialize(
				big_face,
				oposit_face,
				side_face_array,
				comp_trans,
				executing_error = []
			)
				@big_face = big_face
				@oposit_face = oposit_face
				@side_face_array = side_face_array
				@comp_trans = comp_trans
				@points = []
				@innerLoop = []
				@curveEdges = []
				@curveEdges_inner = []
				@holePoints = []
				@holePoints_bottom = []
				@lockdowelPoints = []
				@lockdowelPoints_bottom = []
				@chanelPoints = []
				@chanelPoints_bottom = []
				@tool_path = []
				@tool_path_bottom = []
				@pocket_path = []
				@pocket_path_bottom = []
				@edgeBandingMark = []
				@edgeBandingMark_inner = []
				@custom_path = []
				@custom_path_bottom = []
				@dogbone_male = []
				@dogboneMaleHoles = []
				@dogboneFemaleHoles = []
				@dogboneFemaleHoles_bottom = []
				@executing_error = executing_error
			end
			def outer_loop
				@big_face.outer_loop
			end
			def inner_loop
				oloop_0 = @big_face.outer_loop
				loop_0 = @big_face.loops
				inloop_0 = loop_0.select { |lp| !lp.outer? }
			end
			def get_points(the_loop)
				banding = get_banding_hash
				edges = the_loop.edges
				hash = []
				for i in 0..edges.size - 1
					prevEdge = edges[i - 1]
					vertex_0 = edges[i].vertices[0]
					vertex_1 = edges[i].vertices[1]
					startPoint = nil
					endPoint = nil
					if prevEdge.vertices.include?(vertex_0)
						startPoint = vertex_0
						endPoint = vertex_1
					else
						startPoint = vertex_1
						endPoint = vertex_0
					end
					hash.push(
						{
							startPoint:
								Utilities.point_context_convert(
									startPoint.position,
									@comp_trans,
									IDENTITY
								),
							endPoint:
								Utilities.point_context_convert(
									endPoint.position,
									@comp_trans,
									IDENTITY
								),
							banding: banding[edges[i]]
						}
					)
				end
				if @dogbone_male && @dogbone_male.size > 0
					# Chạy qua hash, so từng cạnh với điểm đầu và cuối của dogbone (lấy ra t�?mảng)
					# Nếu nó online thì đưa vào cạnh
					visited = []
					hash.each do |v|
						line = [v[:startPoint], v[:endPoint]]
						for i in 0..@dogbone_male.size - 1
						# Quay chiều ngược lại nếu không cùng chiều
							next if visited.include?(i)
							p0 = @dogbone_male[i][0]
							p3 = @dogbone_male[i][3]
							if p0.on_line?(line) && p3.on_line?(line)
								vect1 = p0.vector_to(p3)
								vect2 = v[:startPoint].vector_to(v[:endPoint])
								if vect1.dot(vect2) < 0
									@dogbone_male[i].reverse!
								end
								visited << i
								v[:dogbone] = [] if !v[:dogbone]
								v[:dogbone] << @dogbone_male[i]
							end
						end
					end
					# Chạy qua hash và x�?lý đưa vào cạnh
					new_hash = []
					hash.each do |v|
						if v[:dogbone] && v[:dogbone].size > 0
							dir = v[:startPoint].vector_to(v[:endPoint])
							temp = [v[:startPoint]]
							status = sort_dogbone(v[:dogbone], dir)
							if status == false
								@executing_error.push(
									"Error: #{OB[:dogbone_overlap]}"
								)
								return []
							end
							new_hash << {
								startPoint: v[:startPoint].clone,
								endPoint: v[:dogbone].first[0],
								banding: nil
							}
							last_v = nil
							for idx in 0..v[:dogbone].size - 1
								dogbone = v[:dogbone][idx]
								next unless dogbone.size > 0
								if !last_v.nil?
									new_hash << {
										startPoint: last_v.clone,
										endPoint: dogbone.first.clone,
										banding: nil
									}
								end
								for k in 0..dogbone.size - 2
									new_hash << {
										startPoint: dogbone[k].clone,
										endPoint: dogbone[k + 1].clone,
										banding: nil
									}
								end
								last_v = dogbone.last
							end
							new_hash << {
								startPoint: v[:dogbone].last[3].clone,
								endPoint: v[:endPoint].clone,
								banding: nil
							}
						else
							new_hash << v
						end
					end
					return new_hash
				end
				return hash
			end
			def sort_dogbone(dogbone, dir)
				size = dogbone.size
				status = true
				loop do
					swapped = false
					(size - 1).times do |i|
						v00 = dogbone[i][0].vector_to(dogbone[i + 1][0])
						v01 = dogbone[i][0].vector_to(dogbone[i + 1][3])
						v10 = dogbone[i][3].vector_to(dogbone[i + 1][0])
						v11 = dogbone[i][3].vector_to(dogbone[i + 1][3])
						u00 = dogbone[i + 1][0].vector_to(dogbone[i][0])
						u01 = dogbone[i + 1][0].vector_to(dogbone[i][3])
						u10 = dogbone[i + 1][3].vector_to(dogbone[i][0])
						u11 = dogbone[i + 1][3].vector_to(dogbone[i][3])
						if u10.dot(dir) > 0
							temp = dogbone[i].clone
							dogbone[i] = dogbone[i + 1]
							dogbone[i + 1] = temp
							swapped = true
						elsif v01.dot(dir) > 0 && v10.dot(dir) < 0
							status = false
							break
						end
					end
					break if not swapped
				end
				return status
			end
			def offset_loop(arr, inner = false)
				# Sau khi đã chuyển hết v�?mặt phẳng XOY thì thực hiện offset này
				hash = []
				orientation = self.test_ccw(arr)
				if inner
					if orientation
						temp = []
						while arr.size > 0
							ptr = arr.pop
							temp << {
								startPoint: ptr[:endPoint],
								endPoint: ptr[:startPoint],
								banding: ptr[:banding]
							}
						end
						arr = temp
					end
				else
					if !orientation
						temp = []
						while arr.size > 0
							ptr = arr.pop
							temp << {
								startPoint: ptr[:endPoint],
								endPoint: ptr[:startPoint],
								banding: ptr[:banding]
							}
						end
						arr = temp
					end
				end
				for i in 0..arr.size - 1
					banding = arr[i][:banding]
					if !banding.nil? && banding[:day_nep_dan_canh] &&
							banding[:day_nep_dan_canh] != 0
						d = banding[:day_nep_dan_canh].to_s.to_l
						v = arr[i][:startPoint].vector_to(arr[i][:endPoint])
						offset_vector =
							Geom::Vector3d.new(-v.y, v.x, 0).normalize!
						offset = Utilities.vector_multiply(d, offset_vector)
						tr = Geom::Transformation.new(offset)
						hash.push(
							{
								startPoint: arr[i][:startPoint].transform(tr),
								endPoint: arr[i][:endPoint].transform(tr),
								banding: arr[i][:banding]
							}
						)
					else
						hash.push(arr[i])
					end
				end
				hash2 = []
				for i in 0..hash.size - 1
					next_edge = hash[i + 1] if i < hash.size - 1
					next_edge = hash[0] if i == hash.size - 1
					if next_edge[:banding].nil? && hash[i][:banding].nil? &&
							hash[i - 1][:banding].nil?
						hash2.push(hash[i])
					else
						nextLine = [
							next_edge[:startPoint],
							next_edge[:startPoint].vector_to(
								next_edge[:endPoint]
							)
						]
						prevLine = [
							hash[i - 1][:startPoint],
							hash[i - 1][:startPoint].vector_to(
								hash[i - 1][:endPoint]
							)
						]
						line = [
							hash[i][:startPoint],
							hash[i][:startPoint].vector_to(hash[i][:endPoint])
						]
						startPoint = Geom.intersect_line_line(line, prevLine)
						endPoint = Geom.intersect_line_line(line, nextLine)
						if !startPoint.nil? && !endPoint.nil?
							hash2.push(
								{
									startPoint: startPoint,
									endPoint: endPoint,
									banding: hash[i][:banding]
								}
							)
						elsif startPoint.nil? && !endPoint.nil?
							hash2.push(
								{
									startPoint: hash[i][:startPoint],
									endPoint: endPoint,
									banding: hash[i][:banding]
								}
							)
						elsif !startPoint.nil? && endPoint.nil?
							hash2.push(
								{
									startPoint: startPoint,
									endPoint: hash[i][:endPoint],
									banding: hash[i][:banding]
								}
							)
						elsif startPoint.nil? && endPoint.nil?
							hash2.push(
								{
									startPoint: hash[i][:startPoint],
									endPoint: hash[i][:endPoint],
									banding: hash[i][:banding]
								}
							)
						end
					end
				end
				return hash2
			end
			def get_banding_hash
				hash = {}
				@side_face_array.each do |f|
					edge_banding = f.get_attribute('amtf_dict', 'edge_banding')
					edge_banding_type =
						f.get_attribute('amtf_dict', 'edge_type_name')
					f.edges.each do |e|
						if edge_banding == 'true'
							day_nep_dan_canh =
								f.get_attribute('amtf_dict', 'edge_type_thick')
							day_nep_dan_canh = 0 if day_nep_dan_canh.nil?
							hash[e] = {
								edge_banding_type: edge_banding_type,
								day_nep_dan_canh: day_nep_dan_canh
							}
						end
					end
				end
				return hash
			end
			def insert_holes(
				center,
				type,
				radius,
				config_data,
				connector_type_id,
				minifixID
			)
				return if radius <= 0
				case type
				when 'oc_cam', 'chot_cam', 'chot_go3', 'chot_go2', 'hinge',
				     'khoan_moi', 'chot_dot', 'patV', 'rafix_d', 'rafix_d1',
				     'rafix_d2', 'hinge35', 'hinge31', 'hinge32', 'hinge41',
				     'hinge42', 'hinge51', 'hinge52', 'hinge61', 'hinge62',
				     'drawer_fitting_1', 'drawer_fitting_2', 'cabineo_d',
				     'cabineo_d1', 'cabineo_d2', 'cabineo_d3'
					if Utilities.within_face?(center, @big_face)
						@holePoints.push(
							{
								center:
									Utilities.point_context_convert(
										center,
										@comp_trans,
										IDENTITY
									),
								type: type,
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
					elsif Utilities.within_face?(center, @oposit_face)
						prj = center.project_to_plane(@big_face.plane)
						@holePoints_bottom.push(
							{
								center:
									Utilities.point_context_convert(
										prj,
										@comp_trans,
										IDENTITY
									),
								type: type,
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
					end
				end
			end #insert hole
			def insert_female_dogbone(path)
				return unless path[:dogbone_female_points]
				unless path[:dogbone_female_points][:pocket] &&
						path[:dogbone_female_points][:pocket].size > 0
					return
				end
				return unless path[:dogbone_female_points][:dogbone_female_D]
				config_data =
					path[:dogbone_female_points][:pocket][0][:config_data]
				minifixID = path[:dogbone_female_points][:minifixID]
				config_data_parse = JSON.parse(config_data)
				radius = 0.5 * config_data_parse['diameter'].to_s.to_l
				connector_type_id = JSON.parse(config_data)['connector_type_id']
				pockets = path[:dogbone_female_points][:pocket]
				dogbone_female_Ds =
					path[:dogbone_female_points][:dogbone_female_D]
				pockets.each do |pocket|
					cfg = {
						name: config_data_parse['name'],
						toolNumber: config_data_parse['d_tool'],
						groove_depth: config_data_parse['d_depth'],
						groove_passes: config_data_parse['d_passes'],
						groove_type: 4
					}
					insert_chanel(
						{
							points: pocket[:points],
							layer_name: 'female_dogbone',
							config_data: cfg.to_json
						}
					)
				end
				return if !radius || radius == 0
				dogbone_female_Ds.each do |female_hole|
					center1 = female_hole[:center1]
					center2 = female_hole[:center2]
					next unless center1 && center2
					if Utilities.within_face?(center1, @big_face)
						@dogboneFemaleHoles.push(
							{
								center1:
									Utilities.point_context_convert(
										center1,
										@comp_trans,
										IDENTITY
									),
								center2:
									Utilities.point_context_convert(
										center2,
										@comp_trans,
										IDENTITY
									),
								type: 'dogbone_female_D',
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
					elsif Utilities.within_face?(center1, @oposit_face)
						prj1 = center1.project_to_plane(@big_face.plane)
						prj2 = center2.project_to_plane(@big_face.plane)
						@dogboneFemaleHoles_bottom.push(
							{
								center1:
									Utilities.point_context_convert(
										prj1,
										@comp_trans,
										IDENTITY
									),
								center2:
									Utilities.point_context_convert(
										prj2,
										@comp_trans,
										IDENTITY
									),
								type: 'dogbone_female_D',
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
					end
				end
			end
			def insert_male_dogbone(path)
				return unless path[:dogbone_male_points]
				return unless path[:dogbone_male_points].size == 2
				male_points_0 = path[:dogbone_male_points][0][:points]
				male_points_1 = path[:dogbone_male_points][1][:points]
				config_data = path[:dogbone_male_points][0][:config_data]
				minifixID = path[:minifixID]
				config_data_parse = JSON.parse(config_data)
				radius = 0.5 * config_data_parse['diameter'].to_s.to_l
				connector_type_id = JSON.parse(config_data)['connector_type_id']
				on_face =
					male_points_0.all? { |p| p.on_plane?(@big_face.plane) }
				on_oposit =
					male_points_0.all? { |p| p.on_plane?(@oposit_face.plane) }
				male_points_1 =
					male_points_1.map do |p|
						Utilities.point_context_convert(
							p,
							@comp_trans,
							IDENTITY
						)
					end
				male_points_0 =
					male_points_0.map do |p|
						Utilities.point_context_convert(
							p,
							@comp_trans,
							IDENTITY
						)
					end
				move1 =
					Utilities.vector_multiply(
						radius,
						male_points_0[0].vector_to(male_points_0[1]).normalize
					)
				tr1 = Geom::Transformation.new move1
				move03 =
					Utilities.vector_multiply(
						radius,
						male_points_0[0].vector_to(male_points_0[3]).normalize
					)
				tr03 = Geom::Transformation.new move03
				move30 =
					Utilities.vector_multiply(
						radius,
						male_points_0[3].vector_to(male_points_0[0]).normalize
					)
				tr30 = Geom::Transformation.new move30
				# Ch�?đưa vào mặt top
				if on_face
					@dogbone_male << male_points_0
					if radius && radius != 0
						hole0 = male_points_0[0].transform(tr1)
						hole01 = hole0.transform(tr30)
						hole3 = male_points_0[3].transform(tr1)
						hole31 = hole3.transform(tr03)
						@dogboneMaleHoles.push(
							{
								center1: hole0,
								center2: hole01,
								type: 'dogbone_male_D',
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
						@dogboneMaleHoles.push(
							{
								center1: hole3,
								center2: hole31,
								type: 'dogbone_male_D',
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
					end
				end
				if on_oposit
					@dogbone_male << male_points_1
					if radius && radius != 0
						hole0 = male_points_1[0].transform(tr1)
						hole01 = hole0.transform(tr30)
						hole3 = male_points_1[3].transform(tr1)
						hole31 = hole3.transform(tr03)
						@dogboneMaleHoles.push(
							{
								center1: hole0,
								center2: hole01,
								type: 'dogbone_male_D',
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
						@dogboneMaleHoles.push(
							{
								center1: hole3,
								center2: hole31,
								type: 'dogbone_male_D',
								radius: radius,
								config_data: config_data,
								connector_type_id: connector_type_id,
								minifixID: minifixID
							}
						)
					end
				end
			end
			def insert_chanel(chanel)
				on_face =
					chanel[:points].all? { |p| p.on_plane?(@big_face.plane) }
				on_oposit =
					chanel[:points].all? { |p| p.on_plane?(@oposit_face.plane) }
				if on_face
					offset_points = []
					if chanel[:offset_points]
						offset_points =
							chanel[:offset_points].map do |p|
								Utilities.point_context_convert(
									p,
									@comp_trans,
									IDENTITY
								)
							end
					end
					@chanelPoints.push(
						{
							offset_points: offset_points,
							points:
								chanel[:points].map do |p|
									Utilities.point_context_convert(
										p,
										@comp_trans,
										IDENTITY
									)
								end,
							layer_name: chanel[:layer_name],
							config_data: chanel[:config_data],
							offset: chanel[:offset]
						}
					)
				end
				if on_oposit
					offset_points = []
					if chanel[:offset_points]
						offset_points =
							chanel[:offset_points].map do |p|
								prj = p.project_to_plane(@big_face.plane)
								Utilities.point_context_convert(
									prj,
									@comp_trans,
									IDENTITY
								)
							end
					end
					@chanelPoints_bottom.push(
						{
							offset_points: offset_points,
							points:
								chanel[:points].map do |p|
									prj = p.project_to_plane(@big_face.plane)
									Utilities.point_context_convert(
										prj,
										@comp_trans,
										IDENTITY
									)
								end,
							layer_name: chanel[:layer_name],
							config_data: chanel[:config_data],
							offset: chanel[:offset]
						}
					)
				end
			end
			def insert_customPath(path)
				points = path[:points]
				on_face = points.all? { |p| p.on_plane?(@big_face.plane) }
				on_oposit = points.all? { |p| p.on_plane?(@oposit_face.plane) }
				if on_face
					@custom_path.push(
						{
							points:
								points.map do |p|
									Utilities.point_context_convert(
										p,
										@comp_trans,
										IDENTITY
									)
								end,
							config_data: path[:config_data],
							closed: path[:closed]
						}
					)
				end
				if on_oposit
					prj = points.map { |p| p.project_to_plane(@big_face.plane) }
					@custom_path_bottom.push(
						{
							points:
								prj.map do |p|
									Utilities.point_context_convert(
										p,
										@comp_trans,
										IDENTITY
									)
								end,
							config_data: path[:config_data],
							closed: path[:closed]
						}
					)
				end
			end
			def insert_lockdowel(lockdowel)
				points = [lockdowel[:p1], lockdowel[:p2], lockdowel[:p3]]
				on_face = points.all? { |p| p.on_plane?(@big_face.plane) }
				on_oposit = points.all? { |p| p.on_plane?(@oposit_face.plane) }
				if on_face
					@lockdowelPoints.push(
						{
							p1:
								Utilities.point_context_convert(
									lockdowel[:p1],
									@comp_trans,
									IDENTITY
								),
							p2:
								Utilities.point_context_convert(
									lockdowel[:p2],
									@comp_trans,
									IDENTITY
								),
							p3:
								Utilities.point_context_convert(
									lockdowel[:p3],
									@comp_trans,
									IDENTITY
								),
							config_data: lockdowel[:config_data]
						}
					)
				end
				if on_oposit
					prj1 = lockdowel[:p1].project_to_plane(@big_face.plane)
					prj2 = lockdowel[:p2].project_to_plane(@big_face.plane)
					prj3 = lockdowel[:p3].project_to_plane(@big_face.plane)
					@lockdowelPoints_bottom.push(
						{
							p1:
								Utilities.point_context_convert(
									prj1,
									@comp_trans,
									IDENTITY
								),
							p2:
								Utilities.point_context_convert(
									prj2,
									@comp_trans,
									IDENTITY
								),
							p3:
								Utilities.point_context_convert(
									prj3,
									@comp_trans,
									IDENTITY
								),
							config_data: lockdowel[:config_data]
						}
					)
				end
			end
			def get_origin
				org = @big_face.vertices.first.position
				return(
					Utilities.point_context_convert(org, @comp_trans, IDENTITY)
				)
			end
			def convert_to_root_axes(grain_vector, origin = nil)
				yaxis =
					Utilities.vector_context_convert(
						grain_vector,
						@comp_trans,
						IDENTITY
					).normalize
				zaxis =
					Utilities.vector_context_convert(
						@big_face.normal,
						@comp_trans,
						IDENTITY
					).normalize
				xaxis = yaxis.cross(zaxis).normalize
				origin = get_origin if origin.nil?
				tr = Geom::Transformation.new(xaxis, yaxis, zaxis, origin)
				@dogboneMaleHoles =
					@dogboneMaleHoles.map do |obj|
						obj = {
							center1:
								Utilities.point_context_convert(
									obj[:center1],
									IDENTITY,
									tr
								),
							center2:
								Utilities.point_context_convert(
									obj[:center2],
									IDENTITY,
									tr
								),
							type: obj[:type],
							radius: obj[:radius],
							config_data: obj[:config_data],
							connector_type_id: obj[:connector_type_id],
							minifixID: obj[:minifixID]
						}
					end
				@dogboneFemaleHoles =
					@dogboneFemaleHoles.map do |obj|
						obj = {
							center1:
								Utilities.point_context_convert(
									obj[:center1],
									IDENTITY,
									tr
								),
							center2:
								Utilities.point_context_convert(
									obj[:center2],
									IDENTITY,
									tr
								),
							type: obj[:type],
							radius: obj[:radius],
							config_data: obj[:config_data],
							connector_type_id: obj[:connector_type_id],
							minifixID: obj[:minifixID]
						}
					end
				@dogboneFemaleHoles_bottom =
					@dogboneFemaleHoles_bottom.map do |obj|
						obj = {
							center1:
								Utilities.point_context_convert(
									obj[:center1],
									IDENTITY,
									tr
								),
							center2:
								Utilities.point_context_convert(
									obj[:center2],
									IDENTITY,
									tr
								),
							type: obj[:type],
							radius: obj[:radius],
							config_data: obj[:config_data],
							connector_type_id: obj[:connector_type_id],
							minifixID: obj[:minifixID]
						}
					end
				#X�?lý point_0
				@holePoints =
					@holePoints.map do |obj|
						obj = {
							center:
								Utilities.point_context_convert(
									obj[:center],
									IDENTITY,
									tr
								),
							type: obj[:type],
							radius: obj[:radius],
							config_data: obj[:config_data],
							connector_type_id: obj[:connector_type_id],
							minifixID: obj[:minifixID]
						}
					end
				@holePoints_bottom =
					@holePoints_bottom.map do |obj|
						obj = {
							center:
								Utilities.point_context_convert(
									obj[:center],
									IDENTITY,
									tr
								),
							type: obj[:type],
							radius: obj[:radius],
							config_data: obj[:config_data],
							connector_type_id: obj[:connector_type_id],
							minifixID: obj[:minifixID]
						}
					end
				@chanelPoints =
					@chanelPoints.map do |obj|
						obj = {
							offset_points:
								obj[:offset_points].map do |p|
									Utilities.point_context_convert(
										p,
										IDENTITY,
										tr
									)
								end,
							points:
								obj[:points].map do |p|
									Utilities.point_context_convert(
										p,
										IDENTITY,
										tr
									)
								end,
							config_data: obj[:config_data],
							layer_name: obj[:layer_name],
							offset: obj[:offset]
						}
					end
				@chanelPoints_bottom =
					@chanelPoints_bottom.map do |obj|
						obj = {
							offset_points:
								obj[:offset_points].map do |p|
									Utilities.point_context_convert(
										p,
										IDENTITY,
										tr
									)
								end,
							points:
								obj[:points].map do |p|
									Utilities.point_context_convert(
										p,
										IDENTITY,
										tr
									)
								end,
							config_data: obj[:config_data],
							layer_name: obj[:layer_name],
							offset: obj[:offset]
						}
					end
				@lockdowelPoints =
					@lockdowelPoints.map do |obj|
						obj = {
							p1:
								Utilities.point_context_convert(
									obj[:p1],
									IDENTITY,
									tr
								),
							p2:
								Utilities.point_context_convert(
									obj[:p2],
									IDENTITY,
									tr
								),
							p3:
								Utilities.point_context_convert(
									obj[:p3],
									IDENTITY,
									tr
								),
							config_data: obj[:config_data]
						}
					end
				@lockdowelPoints_bottom =
					@lockdowelPoints_bottom.map do |obj|
						obj = {
							p1:
								Utilities.point_context_convert(
									obj[:p1],
									IDENTITY,
									tr
								),
							p2:
								Utilities.point_context_convert(
									obj[:p2],
									IDENTITY,
									tr
								),
							p3:
								Utilities.point_context_convert(
									obj[:p3],
									IDENTITY,
									tr
								),
							config_data: obj[:config_data]
						}
					end
				@custom_path =
					@custom_path.map do |obj|
						obj = {
							points:
								obj[:points].map do |p|
									Utilities.point_context_convert(
										p,
										IDENTITY,
										tr
									)
								end,
							config_data: obj[:config_data],
							closed: obj[:closed]
						}
					end
				@custom_path_bottom =
					@custom_path_bottom.map do |obj|
						obj = {
							points:
								obj[:points].map do |p|
									Utilities.point_context_convert(
										p,
										IDENTITY,
										tr
									)
								end,
							config_data: obj[:config_data],
							closed: obj[:closed]
						}
					end
				points = get_points(outer_loop)
				points =
					points.map do |obj|
						obj = {
							startPoint:
								Utilities.point_context_convert(
									obj[:startPoint],
									IDENTITY,
									tr
								),
							endPoint:
								Utilities.point_context_convert(
									obj[:endPoint],
									IDENTITY,
									tr
								),
							banding: obj[:banding]
						}
					end
				@edgeBandingMark = offset_loop(points)
				@edgeBandingMark.each { |obj| @points.push(obj[:startPoint]) }
				inners = inner_loop
				if (inners.size > 0)
					inners.each do |lo|
						inner_points = get_points(lo)
						inner_points =
							inner_points.map do |obj|
								obj = {
									startPoint:
										Utilities.point_context_convert(
											obj[:startPoint],
											IDENTITY,
											tr
										),
									endPoint:
										Utilities.point_context_convert(
											obj[:endPoint],
											IDENTITY,
											tr
										),
									banding: obj[:banding]
								}
							end
						temp = []
						edgeBandingMark_inner = offset_loop(inner_points, true)
						edgeBandingMark_inner.each do |obj|
							temp.push(obj[:startPoint])
						end
						@innerLoop.push(temp)
						@edgeBandingMark_inner << edgeBandingMark_inner
					end
				end
			end
			def has_hole?
				return true if @holePoints.size > 0
				return true if @lockdowelPoints.size > 0
				return true if @chanelPoints.size > 0
				return true if @tool_path.size > 0
				return true if @pocket_path.size > 0
				return true if @custom_path.size > 0
				return true if @dogboneFemaleHoles.size > 0
				return false
			end
			def test_ccw(arr)
				points = arr.map { |p| p[:startPoint] }
				Utilities.isClockwise?(points)
			end
		end
	end #BuKong
end #Amtflogic