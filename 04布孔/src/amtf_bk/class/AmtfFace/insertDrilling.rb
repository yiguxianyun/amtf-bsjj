# encoding: UTF-8
module AMTF
	module BUKONG
		class InserDrilling
			# Chạy qua mảng các mặt
			# Kiểm tra xem l�?nào thuộc nó
			#Lấy loop (lưu ý dogbone và dán nẹp)
			# Chuyển hết v�?mặt xoy, lấy theo gốc là góc và X, Y
			# Tr�?v�? side_0 = [width, heigth, x, y], side_1 ....
			attr_accessor :side_0, :side_1, :side_2, :side_3, :side_4, :side_5
			def initialize(sheet, executing_error = [])
				@sheet = sheet
				@side_0 = {}
				@side_1 = {}
				@side_2 = {}
				@side_3 = {}
				@side_4 = {}
				@side_5 = {}
				@executing_error = executing_error
			end
			def get_config_data(config)
				if !config
					@executing_error << 'Empty config'
					return
				end
				if config.empty?
					@executing_error << 'Empty config'
					return
				end
				if !config['params1']
					@executing_error << 'Empty config'
					return
				end
				x_horizon_params =
					config['params1'].select do |item|
						item['key'] == 'x_horizon'
					end
				if x_horizon_params.size == 0
					@executing_error << 'Empty config'
					return
				end
				origin_params =
					config['params1'].select do |item|
						item['key'] == 'set_origin'
					end
				if origin_params.size == 0
					@executing_error << 'Empty config'
					return
				end
				x_horizon = x_horizon_params[0]['value']
				conner = origin_params[0]['value']
				return { conner: conner, x_horizon: x_horizon }
			end
			def convert_by_conner(point, bound, conner, x_horizon)
				if conner == 1
					origin = Geom::Point3d.new(bound[:minX], bound[:maxY], 0)
					x_point = Geom::Point3d.new(bound[:maxX], bound[:maxY], 0)
					y_point = Geom::Point3d.new(bound[:minX], bound[:minY], 0)
					if !x_horizon
						y_point =
							Geom::Point3d.new(bound[:maxX], bound[:maxY], 0)
						x_point =
							Geom::Point3d.new(bound[:minX], bound[:minY], 0)
					end
					x_axis = origin.vector_to(x_point).normalize
					y_axis = origin.vector_to(y_point).normalize
					tr = Geom::Transformation.new(origin, x_axis, y_axis)
					return point.transform(tr)
				elsif conner == 2
					if x_horizon
					else
					end
				elsif conner == 3
					if x_horizon
					else
					end
				elsif conner == 4
					if x_horizon
					else
					end
				end
			end
			def execute(config)
				config_data = self.get_config_data(config)
				conner = config_data[:conner]
				x_horizon = config_data[:x_horizon]
				process_faces = @sheet.process_big_faces(@executing_error)
				face0 = process_faces[:face0]
				face1 = process_faces[:face1]
				# Chọn side 0
				# Nếu face_0 có l�?==> chọn face0, ngược lại lấy face1
				# chọn side 1
				# Lấy edge trùng, chuyển tọa đ�?v�?cùng với tọa đ�?của point_0
				# Side 0
				bound_0 = FaceHelper.create_boundingbox(face0.points)
				points_0 =
					face0.points.map do |p|
						convert_by_conner(p, bound_0, conner, x_horizon)
					end
				holePoints0 =
					face0.holePoints.map do |obj|
						obj = {
							center:
								convert_by_conner(
									obj[:center],
									bound_0,
									conner,
									x_horizon
								),
							type: obj[:type],
							radius: obj[:radius],
							config_data: obj[:config_data],
							connector_type_id: obj[:connector_type_id],
							minifixID: obj[:minifixID]
						}
					end
				@side_0 = {
					conner: conner,
					x_horizon: x_horizon,
					holePoints: holePoints0
				}
			end
		end
	end
end