# encoding: UTF-8
module AMTF
	module BUKONG
		module FaceHelper
			def self.create_boundingbox(path)
				box = Hash.new
				box[:minX] = Float::INFINITY
				box[:minY] = Float::INFINITY
				box[:maxX] = -Float::INFINITY
				box[:maxY] = -Float::INFINITY
				for i in 0..path.size - 1
					x = path[i].x
					y = path[i].y
					box[:minX] = [box[:minX], x].min
					box[:minY] = [box[:minY], y].min
					box[:maxX] = [box[:maxX], x].max
					box[:maxY] = [box[:maxY], y].max
				end
				if path.size == 0
					box[:minX] = 0
					box[:minY] = 0
					box[:maxX] = 0
					box[:maxY] = 0
				end
				return box
			end
			def self.isRectangular?(points)
				return false if points.size != 4
				test = true
				for i in 0..points.size - 3
					v1 = points[i].vector_to(points[i + 1])
					v2 = points[i + 1].vector_to(points[i + 2])
					test = false if v1.dot(v2) != 0
				end
				return test
			end
		end
	end
end