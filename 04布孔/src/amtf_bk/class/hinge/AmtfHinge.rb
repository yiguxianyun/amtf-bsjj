# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfHinge
			def initialize(attrs, view = nil)
				@view = view
				@attrs = attrs
				@e1 = attrs[:last_pick][:pick_component]
				@e2 = attrs[:pick_data][:pick_component]
				@pick_face_1 = attrs[:last_pick][:pick_face]
				@pick_face_2 = attrs[:pick_data][:pick_face]
				@tr1 = attrs[:last_pick][:trans]
				@tr2 = attrs[:pick_data][:trans]
				@parent_tr1 = attrs[:last_pick][:parent_matrix]
				@parent_tr2 = attrs[:pick_data][:parent_matrix]
				@point_e1 = attrs[:last_pick][:position]
				@point_e2 = attrs[:pick_data][:position]
				@auto = view.nil?
				@helper = nil
				@door_frame = attrs[:first_frame]
				@side_frame = attrs[:second_frame]
			end
			def get_setting(type, config, conID)
				data = {}
				return data unless config[type]
				config[type].each do |obj|
					if obj['id'] == conID
						data = obj
						break
					end
				end
				return {} unless !data.empty?
				hash = {}
				data['params1'].each do |item|
					hash[item['key']] = item['value']
				end
				data['params2'].each do |item|
					hash[item['key']] = item['value']
				end
				return hash
			end
			def set_config(data)
				@connector_type = data['connector_type']
				@config = data['config']
				@connector_id = data['connector_id']
				@connector_number = data['connector_number']
				@connector_array = data['connector_array']
				@config_data =
					get_setting(@connector_type, @config, @connector_id)
				if data['distance_from_edge'] &&
						data['distance_from_edge'].to_s.to_l > 0
					@config_data['distance_l'] = data['distance_from_edge']
				end
			end
			def draw
				if !@helper.nil? && @helper.size > 0
					@view.draw_points(@helper, 10, 1, 'red')
					@view.drawing_color = 'red'
					@view.line_width = 2
					@view.draw_lines @helper
				end
				if !@helper1.nil? && @helper1.size > 0
					@view.draw_points(@helper1, 10, 1, 'green')
				end
				if !@helper2.nil? && @helper2.size > 0
					@view.draw_points(@helper2, 10, 1, 'yellow')
					@view.drawing_color = 'red'
					@view.line_width = 2
					@view.draw_lines @helper2
				end
				if !@checkPoints.nil? && @checkPoints.size > 0
					@view.draw_points(@checkPoints, 10, 1, 'blue')
					@view.drawing_color = 'blue'
					@view.line_width = 2
					@view.draw_lines @checkPoints
				end
			end
			def make_hinge
				faces = Utilities.definition(@e1).entities.grep(Sketchup::Face)
				out = faces.sort_by { |f| AmtfMath.face_area(f, @tr1) }
				arr2 = [out[out.size - 1], out[out.size - 2]]
				if arr2[0] == @pick_face_1
					plane1 = arr2[1].plane
					@doorFace = arr2[1]
				elsif arr2[1] == @pick_face_1
					plane1 = arr2[0].plane
					@doorFace = arr2[0]
				else
					return
				end
				normal2 =
					Utilities.vector_context_convert(
						@pick_face_2.normal,
						@tr2,
						@tr1
					)
				point2 =
					Utilities.point_context_convert(
						@pick_face_2.vertices[0].position,
						@tr2,
						@tr1
					)
				plane2 = [point2, normal2.normalize]
				intersect_line = Geom.intersect_plane_plane(plane1, plane2)
				return unless intersect_line
				edges = @doorFace.edges
				distArr = []
				edges.each do |edg|
					vect = edg.start.position.vector_to(edg.end.position)
					if vect.parallel?(intersect_line[1])
						distArr << {
							edge: edg,
							distance:
								edg.start.position.distance_to_plane(plane2)
						}
					end
				end
				distArr.sort! { |a, b| a[:distance] <=> b[:distance] }
				@edge_1 = distArr[0][:edge]
				@helper = []
				@edge_1.vertices.each do |vertex|
					p = vertex.position
					@helper <<
						Utilities.point_context_convert(p, @tr1, IDENTITY)
				end
				return unless @helper && @helper.size == 2
				plane1_in_2 = [
					Utilities.point_context_convert(
						@pick_face_1.vertices[0].position,
						@tr1,
						@tr2
					),
					Utilities.vector_context_convert(
						@pick_face_1.normal,
						@tr1,
						@tr2
					).normalize
				]
				distArr2 = []
				@pick_face_2.edges.each do |edg|
					vect = edg.start.position.vector_to(edg.end.position)
					vect1 =
						Utilities.vector_context_convert(
							intersect_line[1],
							@tr1,
							@tr2
						)
					if vect.parallel?(vect1)
						distArr2 << {
							edge: edg,
							distance:
								edg.start.position.distance_to_plane(
									plane1_in_2
								)
						}
					end
				end
				distArr2.sort! { |a, b| a[:distance] <=> b[:distance] }
				@edge_2 = distArr2[0][:edge]
				@helper1 = []
				@edge_2.vertices.each do |vertex|
					p = vertex.position
					@helper1 <<
						Utilities.point_context_convert(p, @tr2, IDENTITY)
				end
				points = []
				if @connector_array == false
					line = [
						@helper[0],
						@helper[0].vector_to(@helper[1]).normalize
					]
					project = @point_e1.project_to_line(line)
					diem_dau = @helper[0]
					diem_cuoi = @helper[1]
					# Đoạn x�?lý hiện th�?đ�?người dùng biết đang lấy khoảng cách so với cạnh nào
					dist0 = project.distance(@helper[0])
					dist1 = project.distance(@helper[1])
					if dist0 > dist1
						diem_dau = @helper[1]
						diem_cuoi = @helper[0]
					end
					@helper2 = [diem_dau, project]
					@view.refresh
					khoang_cach = 0
					while khoang_cach == 0
						promt_str =
							OB[:distance] + ' (' + Utilities.get_model_units +
								')'
						prompts = [promt_str]
						defaults = ['0.0']
						inputs = UI.inputbox(prompts, defaults, OB[:distance])
						if !inputs
							@helper = []
							@helper1 = []
							@helper2 = []
							return
						end
						enterNum = Utilities.parse_input(inputs)
						if enterNum
							khoang_cach = enterNum
							correct = true
						end
					end
					# # Xác định điểm đặt l�?
					plastic_ptr =
						Utilities.find_point_between_two_points(
							diem_dau,
							diem_cuoi,
							khoang_cach
						)
					points = [{ chot_cam: plastic_ptr }]
				else
					points =
						Utilities.get_hole_locations(
							@helper[0],
							@helper[1],
							@config_data,
							@connector_number.to_i
						)
					@helper2 = [points[0][:chot_cam], points[1][:chot_cam]]
					self.update_frame_multi_post
				end
				@helper = []
				@helper1 = []
				@helper2 = []
				drawHingePost(points)
			end
			def update_frame_multi_post
				# @door_frame.traverseCallback do |node|
				# 	next unless node['root'] && node['root']['sheets']
				# 	node['root']['sheets'].each do |k, sheet_arr|
				# 		sheet_arr.each do |obj|
				# 			if obj['root']['id'] == sheet_id
				# 				sheet_trans =
				# 					Geom::Transformation.new(
				# 						obj['root']['transform']
				# 					)
				# 				@sheet_data = {
				# 					sheet: obj,
				# 					parent: node['root']
				# 				}
				# 			end
				# 		end
				# 	end
				# end
				# puts @sheet_data
				# return if @sheet_data.empty?
				# matrix = IDENTITY
				# for i in (sub_index - 1).downto(0)
				# 	if trans_path[i].is_a?(Sketchup::Group)
				# 		matrix = matrix * trans_path[i].transformation
				# 	end
				# end
				# sheet = @sheet_data[:sheet]
				# matrix = matrix * sheet_trans
				# vertices = sheet['root']['boundingBox']['vertices']
				# @boxHelper = AmtfBox.new(vertices, matrix)
				# puts @boxHelper
			end
			def drawHingePost(points)
				entitites = Sketchup.active_model.active_entities
				selection = Sketchup.active_model.selection
				return unless points && points.size > 0
				# L�?bản l�?
				pt1 = Utilities.point_at_face(@doorFace)
				pt2 = pt1.project_to_line(@edge_1.line)
				direction1 = pt2.vector_to(pt1).normalize
				mv_vector1 = @edge_1.line[1].normalize
				mv_vector1 =
					Utilities.vector_context_convert(mv_vector1, @tr1, IDENTITY)
				mv_vector1.normalize!
				direction1 =
					Utilities.vector_context_convert(direction1, @tr1, IDENTITY)
				direction1.normalize!
				pt3 = Utilities.point_at_face(@pick_face_2)
				pt4 = pt3.project_to_line(@edge_2.line)
				direction2 = pt4.vector_to(pt3).normalize
				mv_vector2 = @edge_2.line[1].normalize
				mv_vector2 =
					Utilities.vector_context_convert(mv_vector2, @tr1, IDENTITY)
						.normalize
				direction2 =
					Utilities.vector_context_convert(direction2, @tr1, IDENTITY)
						.normalize
				points.each do |p|
					minifixID = Utilities.CreatUniqueid
					d35 =
						p[:chot_cam].transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									@config_data['distance_m'].to_s.to_l,
									direction1
								)
							)
						)
					tam_31 =
						p[:chot_cam].transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									@config_data['distance_l3'].to_s.to_l,
									direction1
								)
							)
						)
					doorPlane = [
						Utilities.point_context_convert(
							@doorFace.vertices[0].position,
							@tr1,
							IDENTITY
						),
						Utilities.vector_context_convert(
							@doorFace.normal,
							@tr1,
							IDENTITY
						).normalize
					]
					# d35 = d35.project_to_plane(doorPlane)
					tam_31 = tam_31.project_to_plane(doorPlane)
					bankinh_r = 0.5 * @config_data['diameter'].to_s.to_l
					height_1 = (@config_data['d_depth']).to_s.to_l
					move_1 = (@config_data['d_depth'].to_f - 1).to_s.to_l
					oc_35 =
						CircleModel.new(
							@e1,
							d35,
							bankinh_r,
							@doorFace.normal,
							height_1,
							move_1,
							@tr1
						)
					oc_35.drawModel
					oc_35.set_attribute('amtf_dict', 'type', 'hinge35')
					oc_35.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_35.set_attribute('amtf_dict', 'radius', bankinh_r.to_s)
					oc_35.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d_depth'].to_s
					)
					oc_35.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_35.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					next unless @config_data['diameter_1'].to_f > 0
					vit31 =
						tam_31.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l2'].to_s.to_l,
									mv_vector1
								)
							)
						)
					vit32 =
						tam_31.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l2'].to_s.to_l,
									mv_vector1.reverse
								)
							)
						)
					bankinh_r1 = 0.5 * @config_data['diameter_1'].to_s.to_l
					oc_31 =
						CircleModel.new(
							@e1,
							vit31,
							bankinh_r1,
							@doorFace.normal,
							height_1,
							move_1,
							@tr1
						)
					oc_31.drawModel
					oc_31.set_attribute('amtf_dict', 'type', 'hinge31')
					oc_31.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_31.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_31.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_31.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_31.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					oc_32 =
						CircleModel.new(
							@e1,
							vit32,
							bankinh_r1,
							@doorFace.normal,
							height_1,
							move_1,
							@tr1
						)
					oc_32.drawModel
					oc_32.set_attribute('amtf_dict', 'type', 'hinge32')
					oc_32.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_32.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_32.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_32.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_32.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					next unless @config_data['distance_l4'].to_f > 0
					vit4 =
						p[:chot_cam].transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									@config_data['distance_l4'].to_s.to_l,
									direction2
								)
							)
						)
					face2_plane = [
						Utilities.point_context_convert(
							@pick_face_2.vertices[0].position,
							@tr2,
							IDENTITY
						),
						Utilities.vector_context_convert(
							@pick_face_2.normal,
							@tr2,
							IDENTITY
						).normalize
					]
					vit4 = vit4.project_to_plane(face2_plane)
					vit41 =
						vit4.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l1'].to_s.to_l,
									mv_vector2
								)
							)
						)
					vit42 =
						vit4.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l1'].to_s.to_l,
									mv_vector2.reverse
								)
							)
						)
					oc_41 =
						CircleModel.new(
							@e2,
							vit41,
							bankinh_r1,
							@pick_face_2.normal,
							height_1,
							move_1,
							@tr2
						)
					oc_41.drawModel
					oc_41.set_attribute('amtf_dict', 'type', 'hinge41')
					oc_41.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_41.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_41.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_41.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_41.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					oc_42 =
						CircleModel.new(
							@e2,
							vit42,
							bankinh_r1,
							@pick_face_2.normal,
							height_1,
							move_1,
							@tr2
						)
					oc_42.drawModel
					oc_42.set_attribute('amtf_dict', 'type', 'hinge42')
					oc_42.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_42.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_42.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_42.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_42.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					next unless @config_data['distance_l5'].to_f > 0
					vit5 =
						vit4.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									@config_data['distance_l5'].to_s.to_l,
									direction2
								)
							)
						)
					vit51 =
						vit5.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l1'].to_s.to_l,
									mv_vector2
								)
							)
						)
					vit52 =
						vit5.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l1'].to_s.to_l,
									mv_vector2.reverse
								)
							)
						)
					oc_51 =
						CircleModel.new(
							@e2,
							vit51,
							bankinh_r1,
							@pick_face_2.normal,
							height_1,
							move_1,
							@tr2
						)
					oc_51.drawModel
					oc_51.set_attribute('amtf_dict', 'type', 'hinge51')
					oc_51.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_51.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_51.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_51.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_51.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					oc_52 =
						CircleModel.new(
							@e2,
							vit52,
							bankinh_r1,
							@pick_face_2.normal,
							height_1,
							move_1,
							@tr2
						)
					oc_52.drawModel
					oc_52.set_attribute('amtf_dict', 'type', 'hinge52')
					oc_52.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_52.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_52.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_52.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_52.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					next unless @config_data['distance_l6'].to_f > 0
					vit6 =
						vit5.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									@config_data['distance_l6'].to_s.to_l,
									direction2
								)
							)
						)
					vit61 =
						vit6.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l1'].to_s.to_l,
									mv_vector2
								)
							)
						)
					vit62 =
						vit6.transform(
							Geom::Transformation.new(
								Utilities.vector_multiply(
									0.5 * @config_data['distance_l1'].to_s.to_l,
									mv_vector2.reverse
								)
							)
						)
					oc_61 =
						CircleModel.new(
							@e2,
							vit61,
							bankinh_r1,
							@pick_face_2.normal,
							height_1,
							move_1,
							@tr2
						)
					oc_61.drawModel
					oc_61.set_attribute('amtf_dict', 'type', 'hinge61')
					oc_61.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_61.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_61.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_61.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_61.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
					oc_62 =
						CircleModel.new(
							@e2,
							vit62,
							bankinh_r1,
							@pick_face_2.normal,
							height_1,
							move_1,
							@tr2
						)
					oc_62.drawModel
					oc_62.set_attribute('amtf_dict', 'type', 'hinge62')
					oc_62.set_attribute('amtf_dict', 'minifixID', minifixID)
					oc_62.set_attribute('amtf_dict', 'radius', bankinh_r1)
					oc_62.set_attribute(
						'amtf_dict',
						'cut_depth',
						@config_data['d1_depth'].to_s
					)
					oc_62.set_attribute(
						'amtf_dict',
						'connector_id',
						@connector_id
					)
					oc_62.set_attribute(
						'amtf_dict',
						'config_data',
						@config_data.to_json
					)
				end
			end
		end
	end
end