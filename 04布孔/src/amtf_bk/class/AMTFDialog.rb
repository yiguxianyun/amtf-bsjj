# encoding: UTF-8
module AMTF
	module BUKONG
		class AMTFDialog
			attr_accessor :dialog, :attrs, :no_closed
			def initialize()
				puts "AMTFDialog  initialize "
				@dialog.close if !@dialog.nil?
				@no_closed = false
			end
			def return_home
				url = "#{ServerOptions.get_server_url}/tool_bag"
				if @dialog.visible?
					@dialog.bring_to_front
					@dialog.set_url(url)
				else
					create_dialog
					@dialog.set_url(url)
					@dialog.show
				end
			end
			def on_deactivate_tool
				url = "#{ServerOptions.get_server_url}/tool_bag"
				@dialog.set_url(url) if !@dialog.nil? && @dialog.visible?
			end
			def create_dialog(url = nil)
				options = {
					dialog_title: 'amtf_布孔',
					preferences_key: 'amtf.net',
					style: UI::HtmlDialog::STYLE_DIALOG,
					resizable: true,
					scrollable: true,
					width: FUJI_WINDOW_SCREEN[:lib][:width],
					height: FUJI_WINDOW_SCREEN[:lib][:height],
					right: 0,
					top: 0
				}
				# url = "#{ServerOptions.get_server_url}/tool_bag" if url.nil?
				# @dialog = DLG_布孔.new()
				@dialog.set_size(options[:width], options[:height]) # Ensure size is set.
				# @dialog.center
				# @dialog.set_url(url)
				if url.nil?
					url="布孔"
				end
				path = File.join(__dir__, "html/#{url}.html")
				dialog.set_file path
				Bridge.decorate(@dialog)
				@dialog.on('execute_cleaning') do |deferred, options|
					begin
						CleaningModule.cleaning(options)
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('set_save_credential') do |deferred, credential|
					begin
						Sketchup.write_default(
							'Amtflogic',
							'configuration',
							credential
						)
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('set_window_size') do |deferred, width, height|
					begin
						@dialog.set_size(width.to_i, height.to_i)
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('activate_component_tool') do |deferred|
					begin
						Component::CreateComponent.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_rename_sheet') do |deferred|
					begin
						RenameSheet::RenameSheetTool.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_remove_sheet') do |deferred|
					begin
						RemoveSheetModule::RemoveSheet.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_edit_frame') do |deferred|
					begin
						EditFrameModule::EditFrame.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_remove_sub') do |deferred|
					begin
						RemoveSubModule::RemoveSub.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_adjust_div') do |deferred|
					begin
						AdjustModule::SheetAdjust.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_horizon_split') do |deferred|
					begin
						H_Module::H_split.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_vertical_split') do |deferred|
					begin
						V_Module::V_split.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_move_sheet') do |deferred|
					begin
						SheetMoveModule::SheetMove.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_strange_sheet') do |deferred|
					begin
						if !SheetHoleModule::SheetHole.active?
							SheetHoleModule::SheetHole.tool_activate
						end
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_edit_strange_sheet') do |deferred|
					begin
						if !SheetEditModule::SheetEdit.active?
							SheetEditModule::SheetEdit.tool_activate
						end
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_rotate_sheet') do |deferred|
					begin
						SheetRotateModule::SheetRotate.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_resize_frame') do |deferred|
					begin
						AdjustFRMModule::FrameAdjust.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_resize_sheet') do |deferred|
					begin
						SheetResizeModule::SheetResize.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_division') do |deferred|
					begin
						Division_Module::Division_change.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('update_frame_root') do |deferred, params, frame_ID|
					begin
						if params && frame_ID
							frame = AMTF_STORE.frame_premitives[frame_ID.to_i]
							if frame
								Sketchup.active_model.start_operation(
									'Update Frame',
									true
								)
								frame.update(params)
								Sketchup.active_model.commit_operation
								if Division_Module::Division_change.active? ||
										H_Module::H_split.active? ||
										V_Module::V_split.active?
									AMTF_STORE.show_all_sections
								end
								deferred.resolve(true)
							else
								Sketchup.set_status_text('error')
								deferred.resolve(false)
							end
						end
					rescue => error
						Utilities.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('redering') do |deferred, attrs|
					begin
					rescue => error
						Utilities.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('save_config') do |deferred, config|
					begin
						if config
							saver = AmtfSaver.new
							saver.save_config(config)
							deferred.resolve(true)
						end
					rescue => error
						Utilities.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_component_tool_of_not') do |deferred, w, h|
					begin
						if !Component::CreateComponent.active?
							Component::CreateComponent.tool_activate
						end
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('active_create_component') do |deferred, w, h|
					begin
						@dialog.set_size(w.to_i, h.to_i) if !w.nil? && !h.nil?
						Component::CreateComponent.tool_activate
						deferred.resolve(true)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('load_old_config') do |deferred|
					begin
						saver = AmtfSaver.new
						config = saver.open_config
						deferred.resolve(config)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.on('load_config') do |deferred|
					begin
						credential =
							Sketchup.read_default(
								'Amtflogic',
								'configuration',
								''
							)
						deferred.resolve(
							{
								language: OB.saved_lang,
								version: EXTENSION.version,
								unit: self.get_model_units,
								su_filename: Sketchup.active_model.title,
								cre: credential,
								su_authorized: true
							}
						)
					rescue => error
						self.print_exception(error, false)
						Sketchup.set_status_text(error)
						deferred.resolve(false)
					end
				end
				@dialog.show
				@dialog
			end
			def get_model_units()
				opts = Sketchup.active_model.options['UnitsOptions']
				unit_format = opts['LengthFormat']
				if unit_format != Length::Decimal
					if unit_format == Length::Fractional ||
							unit_format == Length::Architectural
						'INCHES' # Architectural or Fractional
					elsif unit_format == Length::Engineering
						'FEET' # Engineering
					else
						'DEFAULT'
					end
				else
					# Decimal ( unit_format == 0 )
					case opts['LengthUnit']
					when Length::Inches
						'INCHES'
					when Length::Feet
						'FEET'
					when Length::Centimeter
						'CENTIMETERS'
					when Length::Millimeter
						'MILLIMETERS'
					when Length::Meter
						'METERS'
					else
						'DEFAULT'
					end
				end
			end
			def print_exception(exception, explicit)
				puts "[#{explicit ? 'EXPLICIT' : 'INEXPLICIT'}] #{exception.class}: #{exception.message}"
				puts exception.backtrace.join("\n")
			end
		end

		# class DLG_布孔 < UI::HtmlDialog
		# 	def initialize
		# 	  options = {
		# 		dialog_title: "布孔😄",
		# 		preferences_key: "布孔😄",
		# 		# scrollable: false,
		# 		# resizable: false,
		# 		width: 400,
		# 		height: 160,
		# 		left: 0,
		# 		top: 100,
		# 		# min_width: 50,
		# 		# min_height: 150,
		# 		max_width: 1000,
		# 		max_height: 1000,
		# 		style: UI::HtmlDialog::STYLE_DIALOG
		# 	  }
		# 	  super(options)
		# 	#   path = File.join(__dir__, '/html/布孔.html')
		# 	#   set_file path
		# 	  # Display the dialog box
		# 	  if RUBY_PLATFORM.index "darwin"
		# 		show_modal
		# 	  else
		# 		show
		# 	  end
		# 	end #initialize
		# 	def close
		# 		super(close)
		# 	end
		# end 
	end
end