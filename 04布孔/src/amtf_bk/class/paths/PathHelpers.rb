# encoding: UTF-8
module AMTF
	module BUKONG
		module PathHelpers
			def self.midpoint2d(p0, p1)
				return Geom::Point2d.new((p0.x + p1.x) / 2, (p0.y + p1.y) / 2)
			end
			def self.unitPerp(v)
				result = Geom::Vector2d.new(-v.y, v.x)
				return result.normalize
			end
			def self.line2d_intersect(line1, line2)
				p1 = line1[0]
				v1 = line1[1]
				p2 = line2[0]
				v2 = line2[1]
				a1 = v1.y
				b1 = -v1.x
				c1 = v1.x * p1.y - v1.y * p1.x
				a2 = v2.y
				b2 = -v2.x
				c2 = v2.x * p2.y - v2.y * p2.x
				d = a1 * b2 - a2 * b1
				dx = c2 * b1 - c1 * b2
				dy = a2 * c1 - a1 * c2
				if (d == 0)
					return false
				else
					return Geom::Point2d.new(dx / d, dy / d)
				end
			end
			def self.arc3Points(p1, p2, p3)
				mid1 = midpoint2d(p1, p2)
				mid2 = midpoint2d(p2, p3)
				perpend1 = unitPerp(p1.vector_to(p2))
				perpend2 = unitPerp(p2.vector_to(p3))
				line1 = [mid1, perpend1]
				line2 = [mid2, perpend2]
				center = line2d_intersect(line1, line2)
				x_axis = Geom::Vector2d.new(1, 0)
				startAngle = center.vector_to(p1).angle_between(x_axis)
				endAngle = center.vector_to(p3).angle_between(x_axis)
				radius = center.distance(p1)
				return(
					{
						p1: p1,
						p2: p2,
						p3: p3,
						center: center,
						startAngle: startAngle,
						endAngle: endAngle,
						radius: radius
					}
				)
			end
			def self.arc2Points(p1, p2, center)
				x_axis = Geom::Vector2d.new(1, 0)
				startAngle = center.vector_to(p1).angle_between(x_axis)
				endAngle = center.vector_to(p2).angle_between(x_axis)
				radius = center.distance(p1)
				return(
					{
						p1: p1,
						p2: p2,
						center: center,
						startAngle: startAngle,
						endAngle: endAngle,
						radius: radius
					}
				)
			end
		end
	end
end