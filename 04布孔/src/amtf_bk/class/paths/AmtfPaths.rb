# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfPaths
			attr_accessor :modal, :edges, :all_edges
			def initialize(attrs)
				@origin = attrs[:origin]
				@polyline = attrs[:polyline]
				@parent = attrs[:parent]
				@z_axis = attrs[:z_axis]
				@x_axis = attrs[:x_axis]
				@y_axis = @z_axis.cross(@x_axis)
				@color = attrs[:color]
				@color = 'green' if !@color
				@trans = attrs[:trans]
				@edges = {}
				@all_edges = []
			end
			def set_attribute(dict, key, val)
				return unless @modal
				@modal.set_attribute(dict, key, val)
			end
			def drawModelEdge
				@modal = Utilities.definition(@parent).entities.add_group
				@modal.transform!(
					Geom::Transformation.new(@origin, @x_axis, @y_axis)
				)
				@polyline.each do |pline|
					@edges[pline] = []
					pline.each do |item|
						if item[:type] == 'line'
							points =
								item[:points].map do |p|
									Geom::Point3d.new(p.x, p.y, 0)
								end
							for i in 0..points.size - 2
								e =
									@modal.entities.add_line(
										points[i],
										points[i + 1]
									)
								@all_edges << e
								@edges[pline] << e
							end
						elsif item[:type] == 'Arc3point'
							arc =
								PathHelpers.arc3Points(
									item[:points][0],
									item[:points][1],
									item[:points][2]
								)
							center =
								Geom::Point3d.new(
									arc[:center].x,
									arc[:center].y,
									0
								)
							e =
								@modal.entities.add_arc(
									center,
									X_AXIS,
									Z_AXIS,
									arc[:radius],
									arc[:startAngle],
									arc[:endAngle],
									50
								)
							@all_edges.concat(e)
							@edges[pline].concat(e)
						elsif item[:type] == 'Arc2point'
						end
					end
					# @modal.entities.add_face(@edges[pline])
				end
				@modal.transform!(@trans.inverse)
			end
		end
	end
end