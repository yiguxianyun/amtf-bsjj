# encoding: UTF-8
module AMTF
	module BUKONG
		class DrawerDrill
			def initialize(attrs, view = nil)
				@view = view
				@attrs = attrs
				@e1 = attrs[:last_pick][:pick_component]
				@e2 = attrs[:pick_data][:pick_component]
				@pick_face_1 = attrs[:last_pick][:pick_face]
				@pick_face_2 = attrs[:pick_data][:pick_face]
				@tr1 = attrs[:last_pick][:trans]
				@tr2 = attrs[:pick_data][:trans]
				@parent_tr1 = attrs[:last_pick][:parent_matrix]
				@parent_tr2 = attrs[:pick_data][:parent_matrix]
				@point_e1 = attrs[:last_pick][:position]
				@point_e2 = attrs[:pick_data][:position]
				@auto = view.nil?
				@helper = nil
				@frame_1 = attrs[:last_pick][:frame]
				@frame_2 = attrs[:pick_data][:frame]
				@frame = attrs[:frame] #Trường hợp frame
				@e1_id = @e1.get_attribute('amtf_dict', 'sheet_id')
				@e2_id = @e2.get_attribute('amtf_dict', 'sheet_id')
			end
			def get_setting(type, config, conID)
				data = {}
				return data unless config[type]
				config[type].each do |obj|
					if obj['id'] == conID
						data = obj
						break
					end
				end
				return {} unless !data.empty?
				hash = {}
				data['params1'].each do |item|
					hash[item['key']] = item['value']
				end
				data['params2'].each do |item|
					hash[item['key']] = item['value']
				end
				return hash
			end
			def set_config(data)
				@connector_type = data['connector_type']
				@config = data['config']
				@connector_id = data['connector_id']
				@config_data =
					get_setting(@connector_type, @config, @connector_id)
			end
			def set_config_frame(data)
				@minifix_list = data['minifix_list']
			end
			def draw
				if !@helper.nil? && @helper.size > 0
					@view.draw_points(@helper, 10, 1, 'red')
					@view.drawing_color = 'red'
					@view.line_width = 2
					@view.draw_lines @helper
				end
				if !@helper1.nil? && @helper1.size > 0
					@view.draw_points(@helper1, 10, 1, 'green')
				end
				if !@helper2.nil? && @helper2.size > 0
					@view.draw_points(@helper2, 10, 1, 'yellow')
					@view.drawing_color = 'red'
					@view.line_width = 2
					@view.draw_lines @helper2
				end
			end
			def find_face_pair
				#  Tìm cặp mặt lớn gần nhau
				big_face_e1 = Utilities.get_big_faces(@e1, @tr1)
				big_face_e2 = Utilities.get_big_faces(@e2, @tr2)
				normal0 =
					Utilities.vector_context_convert(
						big_face_e2[0].normal,
						@tr2,
						@tr1
					)
				point0 =
					Utilities.point_context_convert(
						big_face_e2[0].vertices[0].position,
						@tr2,
						@tr1
					)
				plane0 = [point0, normal0.normalize]
				normal1 =
					Utilities.vector_context_convert(
						big_face_e2[1].normal,
						@tr2,
						@tr1
					)
				point1 =
					Utilities.point_context_convert(
						big_face_e2[1].vertices[0].position,
						@tr2,
						@tr1
					)
				plane1 = [point1, normal1.normalize]
				p0 = Utilities.point_at_face(big_face_e1[0])
				p1 = Utilities.point_at_face(big_face_e1[1])
				x00 = {
					point: p0,
					face1: big_face_e1[0],
					face2: big_face_e2[0],
					distance: p0.distance_to_plane(plane0)
				}
				x01 = {
					point: p0,
					face1: big_face_e1[0],
					face2: big_face_e2[1],
					distance: p0.distance_to_plane(plane1)
				}
				x10 = {
					point: p1,
					face1: big_face_e1[1],
					face2: big_face_e2[0],
					distance: p1.distance_to_plane(plane0)
				}
				x11 = {
					point: p1,
					face1: big_face_e1[1],
					face2: big_face_e2[1],
					distance: p1.distance_to_plane(plane1)
				}
				arr = [x00, x01, x10, x11].sort_by! { |x| x[:distance] }
				return arr[0]
			end
			def canh_day(face, tr)
				return unless face
				edges = face.edges
				edges =
					edges.sort_by! do |e|
						mid =
							Utilities.get_midpoint(
								Utilities.point_context_convert(
									e.start.position,
									tr,
									IDENTITY
								),
								Utilities.point_context_convert(
									e.end.position,
									tr,
									IDENTITY
								)
							)
						mid.z
					end
				return [
					Utilities.point_context_convert(
						edges[0].start.position,
						tr,
						IDENTITY
					),
					Utilities.point_context_convert(
						edges[0].end.position,
						tr,
						IDENTITY
					)
				]
			end
			def make_frame_fitting
				frame_tr = @frame.transformation
				if @minifix_list
					@minifix_list.each do |minifixID, fitting_data|
						next if fitting_data['deleted']
						@config_data = fitting_data['config_data']
						pair = self.find_face_pair
						day = self.canh_day(pair[:face1], @tr1)
						line_direction =
							Utilities.vector_context_convert(
								Geom::Vector3d.new(
									fitting_data['line_direction']
								),
								frame_tr,
								IDENTITY
							)
						dir = day[0].vector_to(day[1])
						day.reverse! if dir.dot(line_direction) < 0
						point_z =
							Utilities.point_context_convert(
								Utilities.point_at_face(pair[:face1]),
								@tr1,
								IDENTITY
							)
						line = [day[0], day[0].vector_to(day[1]).normalize]
						project = point_z.project_to_line(line)
						vector_z = project.vector_to(point_z).normalize
						khoang_cach = fitting_data['khoang_cach'].to_f.to_l
						build_connector(
							pair,
							day,
							khoang_cach,
							vector_z,
							minifixID
						)
					end
				end
			end
			def build_connector(pair, day, khoang_cach, vector_z, minifixID)
				canh = [day[0], day[1]]
				move1 =
					Geom::Transformation.new(
						Utilities.vector_multiply(khoang_cach, vector_z)
					)
				canh = canh.map! { |x| x.transform(move1) }
				normal1 =
					Utilities.vector_context_convert(
						pair[:face1].normal,
						@tr1,
						IDENTITY
					)
				point1 =
					Utilities.point_context_convert(
						pair[:face1].vertices[0].position,
						@tr1,
						IDENTITY
					)
				plane1 = [point1, normal1.normalize]
				normal2 =
					Utilities.vector_context_convert(
						pair[:face2].normal,
						@tr2,
						IDENTITY
					)
				point2 =
					Utilities.point_context_convert(
						pair[:face2].vertices[0].position,
						@tr2,
						IDENTITY
					)
				plane2 = [point2, normal2.normalize]
				location = get_hole_locations(canh[0], canh[1], @config_data)
				bankinh_r1 = 0.5 * @config_data['diameter_1'].to_s.to_l
				bankinh_r2 = 0.5 * @config_data['diameter_2'].to_s.to_l
				height_1 = (@config_data['d_depth'].to_f + 2).to_s.to_l
				move_1 = (@config_data['d_depth'].to_f + 1).to_s.to_l
				if @config_data['diameter_1'].to_f > 0
					location[:loc1].each do |p|
						p1 = p.project_to_plane(plane1)
						chot_cam =
							CircleModel.new(
								@e1,
								p1,
								bankinh_r1,
								pair[:face1].normal,
								height_1,
								move_1,
								@tr1
							)
						chot_cam.drawModel
						chot_cam.set_attribute(
							'amtf_dict',
							'type',
							'drawer_fitting_1'
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'connector_type_id',
							@connector_id
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'minifixID',
							minifixID
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'radius',
							bankinh_r1
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'config_data',
							@config_data.to_json
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'pairs',
							"#{@e1_id}~#{@e2_id}"
						)
					end
				end
				if @config_data['diameter_2'].to_f > 0
					location[:loc2].each do |p|
						p2 = p.project_to_plane(plane2)
						chot_cam =
							CircleModel.new(
								@e2,
								p2,
								bankinh_r2,
								pair[:face2].normal,
								height_1,
								move_1,
								@tr2
							)
						chot_cam.drawModel
						chot_cam.set_attribute(
							'amtf_dict',
							'type',
							'drawer_fitting_2'
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'connector_type_id',
							@connector_id
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'minifixID',
							minifixID
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'radius',
							bankinh_r2
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'config_data',
							@config_data.to_json
						)
						chot_cam.set_attribute(
							'amtf_dict',
							'pairs',
							"#{@e1_id}~#{@e2_id}"
						)
					end
				end
			end
			def make_fitting
				pair = find_face_pair
				day = canh_day(pair[:face1], @tr1)
				day = day.sort_by! { |p| p.distance(@point_e1) }
				@helper = [day[0], day[1]]
				point_z =
					Utilities.point_context_convert(
						Utilities.point_at_face(pair[:face1]),
						@tr1,
						IDENTITY
					)
				line = [day[0], day[0].vector_to(day[1]).normalize]
				project = point_z.project_to_line(line)
				vector_z = project.vector_to(point_z).normalize
				move =
					Geom::Transformation.new(
						Utilities.vector_multiply(50.mm, vector_z)
					)
				@helper2 = [day[0].transform(move), day[1].transform(move)]
				@view.refresh
				khoang_cach = 0
				while khoang_cach == 0
					promt_str =
						OB[:distance] + ' (' + Utilities.get_model_units + ')'
					prompts = [promt_str]
					defaults = [@config_data['distance_h'].to_s]
					inputs = UI.inputbox(prompts, defaults, OB[:distance])
					if !inputs
						@helper = []
						@helper1 = []
						@helper2 = []
						return
					end
					enterNum = Utilities.parse_input(inputs)
					if enterNum
						khoang_cach = enterNum
						correct = true
					end
				end
				minifixID = Utilities.CreatUniqueid
				self.build_connector(
					pair,
					day,
					khoang_cach,
					vector_z,
					minifixID
				)
				if @frame_1 && @frame_2 && @frame_1 == @frame_2
					direction =
						Utilities.vector_context_convert(
							day[0].vector_to(day[1]),
							IDENTITY,
							@frame_1.transformation
						)
					@frame_1.update_single_post_minifix(
						{
							e1_id: @e1_id,
							e2_id: @e2_id,
							config_data: @config_data,
							line_direction: direction.to_a,
							khoang_cach: khoang_cach.to_f,
							minifixID: minifixID,
							connector_type: 'drawer_fitting'
						}
					)
				end
				@helper = []
				@helper1 = []
				@helper2 = []
				@view.refresh
			end
			def get_hole_locations(p1, p2, config_data)
				distance_l1 = config_data['distance_l1'].to_s.to_l
				distance_l2 = config_data['distance_l2'].to_s.to_l
				distance_l3 = config_data['distance_l3'].to_s.to_l
				distance_l4 = config_data['distance_l4'].to_s.to_l
				distance_l5 = config_data['distance_l5'].to_s.to_l
				distance_l6 = config_data['distance_l6'].to_s.to_l
				point_1 =
					Utilities.find_point_between_two_points(p1, p2, distance_l1)
				point_2 =
					Utilities.find_point_between_two_points(
						point_1,
						p2,
						distance_l2
					)
				point_3 =
					Utilities.find_point_between_two_points(
						point_2,
						p2,
						distance_l3
					)
				point_4 =
					Utilities.find_point_between_two_points(p1, p2, distance_l4)
				point_5 =
					Utilities.find_point_between_two_points(
						point_4,
						p2,
						distance_l5
					)
				point_6 =
					Utilities.find_point_between_two_points(
						point_5,
						p2,
						distance_l6
					)
				return(
					{
						loc1: [point_1, point_2, point_3],
						loc2: [point_4, point_5, point_6]
					}
				)
			end
		end
	end
end