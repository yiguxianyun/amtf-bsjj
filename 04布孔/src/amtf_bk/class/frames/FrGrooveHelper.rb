# encoding: UTF-8
module AMTF
	module BUKONG
		module FrGrooveHelper
			def self.remove_groove(e_id, saver, frame, attrs)
				grooving = attrs['root']['grooving']
				remove_list = []
				if grooving
					grooving.each do |k, v|
						remove_list.push(k) if k.include?(e_id)
					end
				end
				if remove_list.size > 0
					remove_list.each { |k| grooving.delete(k) }
				end
				attrs['root']['grooving'] = grooving
				saver.save_comp_json(frame, attrs)
			end
			def self.update_groove(data, saver, frame, attrs)
				e1_id = data[:e1_id]
				e2_id = data[:e2_id]
				save_config = data[:save_config]
				grooving = attrs['root']['grooving']
				grooving = {} if !grooving
				key1 = "#{e1_id}~#{e2_id}"
				key2 = "#{e2_id}~#{e1_id}"
				key = nil
				if grooving.key?(key2)
					key = key2
				else
					grooving.key?(key1)
					key = key1
				end
				grooving[key] = {
					'pair' => [e1_id, e2_id],
					'save_config' => save_config
				}
				attrs['root']['grooving'] = grooving
				saver.save_comp_json(frame, attrs)
			end
			def self.create_groove(frame, sheet_hash, attrs)
				# Hiện tại các tấm hậu b�?thay đổi ID cho nên phải làm như th�?này
				back_list = []
				sheet_hash.each do |k, b|
					comp_type = b.comp.get_attribute 'amtf_dict', 'comp_type'
					sheet_id = b.comp.get_attribute 'amtf_dict', 'sheet_id'
					back_list << b if comp_type == 'back' && sheet_id
				end
				if attrs['root']['grooving'] &&
						!attrs['root']['grooving'].empty?
					attrs['root']['grooving'].each do |k, v|
						if v['pair'] && v['save_config']
							e1_id = v['pair'][0]
							e2_id = v['pair'][1]
							e1 = sheet_hash[e1_id]
							e2 = sheet_hash[e2_id]
							if e1 && e2
								make_hole_data = {
									pick_data: {
										pick_component: e2.comp,
										parent_matrix: e2.parent_tr,
										trans: e2.trans
									},
									last_pick: {
										pick_component: e1.comp,
										parent_matrix: e1.parent_tr,
										trans: e1.trans
									},
									frame: frame
								}
								maker = AmtfMakeHole.new(make_hole_data)
								maker.set_config_frame_groove(v['save_config'])
								maker.back_and_bottom
							elsif e1.nil? && !e2.nil?
								back_list.each do |b|
									make_hole_data = {
										pick_data: {
											pick_component: b.comp,
											parent_matrix: b.parent_tr,
											trans: b.trans
										},
										last_pick: {
											pick_component: e2.comp,
											parent_matrix: e2.parent_tr,
											trans: e2.trans
										},
										frame: frame
									}
									maker = AmtfMakeHole.new(make_hole_data)
									maker.set_config_frame_groove(
										v['save_config']
									)
									maker.back_and_bottom
								end
							elsif e2.nil? && !e1.nil?
								back_list.each do |b|
									make_hole_data = {
										pick_data: {
											pick_component: b.comp,
											parent_matrix: b.parent_tr,
											trans: b.trans
										},
										last_pick: {
											pick_component: e1.comp,
											parent_matrix: e1.parent_tr,
											trans: e1.trans
										},
										frame: frame
									}
									maker = AmtfMakeHole.new(make_hole_data)
									maker.set_config_frame_groove(
										v['save_config']
									)
									maker.back_and_bottom
								end
							end
						end
					end
				end
			end
		end
	end
end