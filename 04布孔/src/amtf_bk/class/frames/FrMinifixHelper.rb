# encoding: UTF-8
module AMTF
	module BUKONG
		module FrMinifixHelper
			def self.create_connector(frame, sheet_hash, attrs)
				type_list = %w[
					connector
					cabineo
					dogbone
					chot_dot
					khoan_moi
					patV
					rafix
					lockdowel
					drawer_fitting
				]
				type_list.each do |connector_type|
					if attrs['root'][connector_type] &&
							!attrs['root'][connector_type].empty?
						attrs['root'][connector_type].each do |k, v|
							if v['pair']
								e1_id = v['pair'][0]
								e2_id = v['pair'][1]
								e1 = sheet_hash[e1_id]
								e2 = sheet_hash[e2_id]
								make_hole_data = {
									pick_data: {
										pick_component: e2.comp,
										parent_matrix: e2.parent_tr,
										trans: e2.trans
									},
									last_pick: {
										pick_component: e1.comp,
										parent_matrix: e1.parent_tr,
										trans: e1.trans
									},
									frame: frame
								}
								if v['multi_post'] && !v['multi_post'].empty?
									maker = AmtfMakeHole.new(make_hole_data)
									maker.set_config_frame_multi_post(
										v['multi_post'],
										connector_type
									)
									maker.make_hole
								end
								if v['single_post'] && !v['single_post'].empty?
									if connector_type != 'drawer_fitting'
										maker = AmtfMakeHole.new(make_hole_data)
										maker.set_config_frame_single_post(
											v['single_post'],
											connector_type
										)
										maker.make_hole
									elsif connector_type == 'drawer_fitting'
										maker = DrawerDrill.new(make_hole_data)
										maker.set_config_frame(v['single_post'])
										maker.make_frame_fitting
									end
								end
							end
						end
					end
				end
			end
			def self.update_single_post_minifix(data, saver, frame, attrs)
				e1_id = data[:e1_id]
				e2_id = data[:e2_id]
				config_data = data[:config_data]
				line_direction = data[:line_direction]
				normal = data[:normal]
				khoang_cach = data[:khoang_cach]
				minifixID = data[:minifixID]
				connector_type = data[:connector_type]
				return unless connector_type
				minifix = attrs['root'][connector_type]
				minifix = {} if !minifix
				key1 = "#{e1_id}~#{e2_id}"
				key2 = "#{e2_id}~#{e1_id}"
				key = nil
				if minifix.key?(key2)
					key = key2
				elsif minifix.key?(key1)
					key = key1
				end
				if key.nil?
					key = key1
					if connector_type == 'drawer_fitting'
						single_post = { 'minifix_list' => {} }
						single_post['minifix_list'][minifixID] = {
							'khoang_cach' => khoang_cach,
							'line_direction' => line_direction,
							'config_data' => config_data
						}
						temp = {
							'pair' => [e1_id, e2_id],
							'single_post' => single_post
						}
						minifix[key] = temp
					else
						single_post = {
							'line_direction' => line_direction,
							'config_data' => config_data,
							'minifix_list' => {}
						}
						single_post['minifix_list'][minifixID] = {
							'khoang_cach' => khoang_cach,
							'line_direction' => line_direction,
							'normal' => normal
						}
						temp = {
							'pair' => [e1_id, e2_id],
							'single_post' => single_post
						}
						minifix[key] = temp
					end
				else
					if minifix[key].key?('single_post')
						if minifix[key]['single_post'].key?('minifix_list')
							if connector_type == 'drawer_fitting'
								minifix[key]['single_post']['minifix_list'][
									minifixID
								] = {
									'khoang_cach' => khoang_cach,
									'line_direction' => line_direction,
									'config_data' => config_data
								}
							else
								minifix[key]['single_post']['minifix_list'][
									minifixID
								] = {
									'khoang_cach' => khoang_cach,
									'line_direction' => line_direction,
									'normal' => normal
								}
							end
						end
					else
						minifix_list = {}
						if connector_type == 'drawer_fitting'
							minifix_list[minifixID] = {
								'khoang_cach' => khoang_cach,
								'line_direction' => line_direction,
								'config_data' => config_data
							}
							minifix[key]['single_post'] = {
								'minifix_list' => minifix_list
							}
						else
							minifix_list[minifixID] = {
								'khoang_cach' => khoang_cach,
								'line_direction' => line_direction,
								'normal' => normal
							}
							minifix[key]['single_post'] = {
								'line_direction' => line_direction,
								'config_data' => config_data,
								'minifix_list' => minifix_list
							}
						end
					end
				end
				attrs['root'][connector_type] = minifix
				saver.save_comp_json(frame, attrs)
			end
			def self.update_multi_post_minifix(data, saver, frame, attrs)
				e1_id = data[:e1_id]
				e2_id = data[:e2_id]
				config_data = data[:config_data]
				line_direction = data[:line_direction]
				ids = data[:ids]
				index_map = data[:index_map]
				connector_number = data[:connector_number]
				connector_array = true
				line_idx = data[:line_idx]
				connector_type = data[:connector_type]
				key1 = "#{e1_id}~#{e2_id}"
				key2 = "#{e2_id}~#{e1_id}"
				if attrs['root']['connector'] &&
						attrs['root']['connector'][key1]
					attrs['root']['connector'][key1]['multi_post'] = {}
				end
				if attrs['root']['connector'] &&
						attrs['root']['connector'][key2]
					attrs['root']['connector'][key2]['multi_post'] = {}
				end
				if attrs['root']['dogbone'] && attrs['root']['dogbone'][key1]
					attrs['root']['dogbone'][key1]['multi_post'] = {}
				end
				if attrs['root']['dogbone'] && attrs['root']['dogbone'][key2]
					attrs['root']['dogbone'][key2]['multi_post'] = {}
				end
				if attrs['root']['khoan_moi'] &&
						attrs['root']['khoan_moi'][key1]
					attrs['root']['khoan_moi'][key1]['multi_post'] = {}
				end
				if attrs['root']['khoan_moi'] &&
						attrs['root']['khoan_moi'][key2]
					attrs['root']['khoan_moi'][key2]['multi_post'] = {}
				end
				if attrs['root']['chot_dot'] && attrs['root']['chot_dot'][key1]
					attrs['root']['chot_dot'][key1]['multi_post'] = {}
				end
				if attrs['root']['chot_dot'] && attrs['root']['chot_dot'][key2]
					attrs['root']['chot_dot'][key2]['multi_post'] = {}
				end
				if attrs['root']['patV'] && attrs['root']['patV'][key1]
					attrs['root']['patV'][key1]['multi_post'] = {}
				end
				if attrs['root']['patV'] && attrs['root']['patV'][key2]
					attrs['root']['patV'][key2]['multi_post'] = {}
				end
				if attrs['root']['rafix'] && attrs['root']['rafix'][key1]
					attrs['root']['rafix'][key1]['multi_post'] = {}
				end
				if attrs['root']['rafix'] && attrs['root']['rafix'][key2]
					attrs['root']['rafix'][key2]['multi_post'] = {}
				end
				if attrs['root']['lockdowel'] &&
						attrs['root']['lockdowel'][key1]
					attrs['root']['lockdowel'][key1]['multi_post'] = {}
				end
				if attrs['root']['lockdowel'] &&
						attrs['root']['lockdowel'][key2]
					attrs['root']['lockdowel'][key2]['multi_post'] = {}
				end
				if attrs['root']['cabineo'] && attrs['root']['cabineo'][key1]
					attrs['root']['cabineo'][key1]['multi_post'] = {}
				end
				if attrs['root']['cabineo'] && attrs['root']['cabineo'][key2]
					attrs['root']['cabineo'][key2]['multi_post'] = {}
				end
				return unless connector_type
				minifix = attrs['root'][connector_type]
				minifix = {} if !minifix
				key = nil
				if minifix.key?(key2)
					key = key2
				elsif minifix.key?(key1)
					key = key1
				end
				if key.nil?
					key = key1
					multi_post = {
						'line_direction' => line_direction,
						'connector_number' => connector_number,
						'connector_array' => connector_array,
						'config_data' => config_data,
						'mapping' => {}
					}
					multi_post['mapping'][line_idx.to_s] = {
						'ids' => ids,
						'index_map' => index_map
					}
					temp = {
						'pair' => [e1_id, e2_id],
						'multi_post' => multi_post
					}
					minifix[key] = temp
				else
					multi_post = {
						'line_direction' => line_direction,
						'connector_number' => connector_number,
						'connector_array' => connector_array,
						'config_data' => config_data,
						'mapping' => {}
					}
					multi_post['mapping'][line_idx.to_s] = {
						'ids' => ids,
						'index_map' => index_map
					}
					minifix[key]['multi_post'] = multi_post
				end
				attrs['root'][connector_type] = minifix
				saver.save_comp_json(frame, attrs)
			end
			def self.reverse_minifix(
				normal,
				pairs,
				minifixID,
				type,
				attrs,
				saver,
				frame
			)
				minifix = attrs['root'][type]
				return if minifix.nil? || minifix.empty?
				key_arr = pairs.split(/\~/, -1)
				return unless key_arr && key_arr.size == 2
				e1_id = key_arr[0]
				e2_id = key_arr[1]
				key1 = "#{e1_id}~#{e2_id}"
				key2 = "#{e2_id}~#{e1_id}"
				key = nil
				if minifix.key?(key1)
					key = key1
				elsif minifix.key?(key2)
					key = key2
				end
				return if key.nil?
				if minifix[key].key?('single_post') &&
						minifix[key]['single_post'].key?('minifix_list') &&
						minifix[key]['single_post']['minifix_list'].key?(
							minifixID
						)
					minifix[key]['single_post']['minifix_list'][minifixID][
						'normal'
					] =
						normal.to_a
				elsif minifix[key].key?('multi_post') &&
						minifix[key]['multi_post'].key?('mapping')
					minifix[key]['multi_post']['mapping'].each do |k, v|
						if v && v.key?('ids') && v['ids'].key?(minifixID)
							v['ids'][minifixID]['normal'] = normal.to_a
						end
					end
				end
				attrs['root'][type] = minifix
				saver.save_comp_json(frame, attrs)
				nil
			end
			def self.remove_minifix(
				pairs,
				minifixID,
				attrs,
				saver,
				frame,
				connector_type
			)
				minifix = attrs['root'][connector_type]
				return if minifix.nil? || minifix.empty?
				key_arr = pairs.split(/\~/, -1)
				return unless key_arr && key_arr.size == 2
				e1_id = key_arr[0]
				e2_id = key_arr[1]
				key1 = "#{e1_id}~#{e2_id}"
				key2 = "#{e2_id}~#{e1_id}"
				key = nil
				if minifix.key?(key1)
					key = key1
				elsif minifix.key?(key2)
					key = key2
				end
				return if key.nil?
				if minifix[key].key?('single_post') &&
						minifix[key]['single_post'].key?('minifix_list') &&
						minifix[key]['single_post']['minifix_list'].key?(
							minifixID
						)
					minifix[key]['single_post']['minifix_list'][minifixID][
						'deleted'
					] =
						true
				elsif minifix[key].key?('multi_post') &&
						minifix[key]['multi_post'].key?('mapping')
					minifix[key]['multi_post']['mapping'].each do |k, v|
						if v && v.key?('ids') && v['ids'].key?(minifixID)
							v['ids'][minifixID]['deleted'] = true
						end
					end
				end
				attrs['root'][connector_type] = minifix
				saver.save_comp_json(frame, attrs)
				nil
			end
			def self.move_minifix(
				pairs,
				minifixID,
				attrs,
				saver,
				frame,
				direction,
				distance,
				connector_type
			)
				minifix = attrs['root'][connector_type]
				return if minifix.nil? || minifix.empty?
				key_arr = pairs.split(/\~/, -1)
				return unless key_arr && key_arr.size == 2
				e1_id = key_arr[0]
				e2_id = key_arr[1]
				key1 = "#{e1_id}~#{e2_id}"
				key2 = "#{e2_id}~#{e1_id}"
				key = nil
				if minifix.key?(key1)
					key = key1
				elsif minifix.key?(key2)
					key = key2
				end
				return if key.nil?
				if minifix[key].key?('single_post') &&
						minifix[key]['single_post'].key?('minifix_list') &&
						minifix[key]['single_post']['minifix_list'].key?(
							minifixID
						)
					moved =
						minifix[key]['single_post']['minifix_list'][minifixID][
							'moved'
						]
					if !moved
						minifix[key]['single_post']['minifix_list'][minifixID][
							'moved'
						] = {
							'direction' => direction.to_a,
							'distance' => distance
						}
					else
						old_dir = Geom::Vector3d.new(moved['direction'])
						new_distance = 0
						new_direction = nil
						if old_dir.dot(direction) < 0
							if distance > moved['distance']
								new_distance = distance - moved['distance']
								new_direction = direction
							else
								new_distance = moved['distance'] - distance
								new_direction = direction.reverse
							end
						else
							new_distance = distance + moved['distance']
							new_direction = direction
						end
						minifix[key]['single_post']['minifix_list'][minifixID][
							'moved'
						] = {
							'direction' => new_direction.to_a,
							'distance' => new_distance
						}
					end
				elsif minifix[key].key?('multi_post') &&
						minifix[key]['multi_post'].key?('mapping')
					minifix[key]['multi_post']['mapping'].each do |k, v|
						if v && v.key?('ids') && v['ids'].key?(minifixID)
							moved = v['ids'][minifixID]['moved']
							if !moved
								v['ids'][minifixID]['moved'] = {
									'direction' => direction.to_a,
									'distance' => distance
								}
							else
								old_dir = Geom::Vector3d.new(moved['direction'])
								new_distance = 0
								new_direction = nil
								if old_dir.dot(direction) < 0
									if distance > moved['distance']
										new_distance =
											distance - moved['distance']
										new_direction = direction
									else
										new_distance =
											moved['distance'] - distance
										new_direction = direction.reverse
									end
								else
									new_distance = distance + moved['distance']
									new_direction = direction
								end
								v['ids'][minifixID]['moved'] = {
									'direction' => new_direction.to_a,
									'distance' => new_distance
								}
							end
						end
					end
				end
				attrs['root'][connector_type] = minifix
				saver.save_comp_json(frame, attrs)
				nil
			end
		end
	end
end