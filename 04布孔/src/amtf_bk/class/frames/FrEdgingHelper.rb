# encoding: UTF-8
module AMTF
	module BUKONG
		module FrEdgingHelper
			def self.add_edging(sheet_id, face_index, edging_data, frame)
				if sheet_id && face_index
					frame_attrs = frame.attrs
					if frame_attrs && frame_attrs.key?('root')
						if !frame_attrs['root'].key?('edging')
							frame_attrs['root']['edging'] = {}
						end
						if !frame_attrs['root']['edging'].key?(sheet_id)
							frame_attrs['root']['edging'][sheet_id] = {}
						end
						frame_attrs['root']['edging'][sheet_id][face_index] =
							edging_data
						frame.saver.save_comp_json(frame.frame, frame_attrs)
					end
				end
			end
			def self.remove_edging(sheet_id, face_index, frame)
				if sheet_id && face_index
					frame_attrs = frame.attrs
					if frame_attrs && frame_attrs.key?('root')
						if frame_attrs['root'].key?('edging') &&
								frame_attrs['root']['edging'].key?(sheet_id) &&
								frame_attrs['root']['edging'][sheet_id].key?(
									face_index
								)
							frame_attrs['root']['edging'][sheet_id].delete(
								face_index
							)
							frame.saver.save_comp_json(frame.frame, frame_attrs)
						end
					end
				end
			end
			def self.remove_multi_edging(sheet_id, face_index_arr, frame)
				if sheet_id && face_index_arr && face_index_arr.size > 0
					frame_attrs = frame.attrs
					if frame_attrs && frame_attrs.key?('root')
						if frame_attrs['root'].key?('edging') &&
								frame_attrs['root']['edging'].key?(sheet_id)
							face_index_arr.each do |face_index|
								if frame_attrs['root']['edging'][sheet_id].key?(
										face_index
								   )
									frame_attrs['root']['edging'][sheet_id]
										.delete(face_index)
								end
							end
							frame.saver.save_comp_json(frame.frame, frame_attrs)
						end
					end
				end
			end
			def self.add_multi_edging(
				sheet_id,
				face_index_arr,
				edging_data,
				frame
			)
				if sheet_id
					frame_attrs = frame.attrs
					if frame_attrs && frame_attrs.key?('root')
						if !frame_attrs['root'].key?('edging')
							frame_attrs['root']['edging'] = {}
						end
						if !frame_attrs['root']['edging'].key?(sheet_id)
							frame_attrs['root']['edging'][sheet_id] = {}
						end
						face_index_arr.each do |face_index|
							frame_attrs['root']['edging'][sheet_id][
								face_index
							] =
								edging_data if face_index
						end
						frame.saver.save_comp_json(frame.frame, frame_attrs)
					end
				end
			end
			def self.get_edging_hash(sheet_id, frame)
				frame_attrs = frame.attrs
				if frame_attrs && frame_attrs.key?('root')
					if frame_attrs['root'].key?('edging') &&
							frame_attrs['root']['edging'].key?(sheet_id)
						return frame_attrs['root']['edging'][sheet_id]
					end
				end
				nil
			end
		end
	end
end