# encoding: UTF-8
module AMTF
	module BUKONG
		module FrAttributeHelper
			def self.update_attributes(sheet_id, params, saver, frame)
				if sheet_id
					attrs = frame.attrs
					node_queue = [attrs]
					until node_queue.empty?
						node = node_queue.shift
						if node['root'] && node['root']['sheets']
							node['root']['sheets'].each do |k, sheet_arr|
								sheet_arr.each do |obj|
									if obj['root']['id'] == sheet_id
										obj['root']['sheet_attributes'] = params
									end
								end
							end
						end
						if node['root'] && node['root']['sections']
							node_queue =
								node_queue.concat(node['root']['sections'])
						end
						if node['root'] && node['root']['sub_part']
							node_queue =
								node_queue.concat(node['root']['sub_part'])
						end
					end
					saver = AmtfSaver.new
					saver.save_comp_json(frame.frame, attrs)
				end
			end
			def self.clean_attributes(sheet_id, saver, frame)
				if sheet_id
					attrs = frame.attrs
					node_queue = [attrs]
					until node_queue.empty?
						node = node_queue.shift
						if node['root'] && node['root']['sheets']
							node['root']['sheets'].each do |k, sheet_arr|
								sheet_arr.each do |obj|
									if obj['root']['id'] == sheet_id
										obj['root']['sheet_attributes'] = {}
									end
								end
							end
						end
						if node['root'] && node['root']['sections']
							node_queue =
								node_queue.concat(node['root']['sections'])
						end
						if node['root'] && node['root']['sub_part']
							node_queue =
								node_queue.concat(node['root']['sub_part'])
						end
					end
					saver = AmtfSaver.new
					saver.save_comp_json(frame.frame, attrs)
				end
			end
		end
	end
end