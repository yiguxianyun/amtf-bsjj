# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfFrame
			attr_accessor :frame, :id, :boundingBox, :saver
			def initialize; end
			def init(attrs, frame = nil)
				@saver = AmtfSaver.new
				if !frame
					@frame = Sketchup.active_model.active_entities.add_group
					@id = @frame.persistent_id
					@frame.set_attribute 'amtf_dict', 'comp_type', 'AmtfFrame'
					@saver.save_comp_json(@frame, attrs)
					AMTF_STORE.add_frame(self)
					self.draw_bounds
					self.draw_geometries(attrs, @frame)
					self.trimStretcher
					self.create_connector
				else
					# Load json
					frame.make_unique
					@id = frame.persistent_id
					@frame = frame
					AMTF_STORE.add_frame(self)
					query = AmtfQuery.new
					section_faces = query.query_section(@frame)
					section_faces.each do |f|
						AMTF_STORE.add_sections(f)
						f.set_attribute('amtf_dict', 'frame_id_of_section', @id)
					end
				end
			end
			def origin_cpoint; end
			def create_connector
				query = AmtfQuery.new
				sheet_hash = query.query_sheet_hash(@frame.entities)
				FrMinifixHelper.create_connector(@frame, sheet_hash, self.attrs)
				FrGrooveHelper.create_groove(@frame, sheet_hash, self.attrs)
			end
			def update_single_post_minifix(data)
				FrMinifixHelper.update_single_post_minifix(
					data,
					@saver,
					@frame,
					self.attrs
				)
			end
			def update_multi_post_minifix(data)
				FrMinifixHelper.update_multi_post_minifix(
					data,
					@saver,
					@frame,
					self.attrs
				)
			end
			def remove_groove(e_id)
				FrGrooveHelper.remove_groove(e_id, @saver, @frame, self.attrs)
			end
			def update_groove(data)
				FrGrooveHelper.update_groove(data, @saver, @frame, self.attrs)
			end
			def get_minifix_data(e1_id, e2_id, connector_type)
				attrs = self.attrs
				minifix = attrs['root'][connector_type]
				return {} if !minifix
				key1 = "#{e1_id}~#{e2_id}"
				key2 = "#{e2_id}~#{e1_id}"
				if minifix.key?(key2)
					return minifix[key2]
				else
					return minifix[key1]
				end
			end
			def get_minifix_array_data(e1_id, e2_id, connector_type)
				minifix_data =
					self.get_minifix_data(e1_id, e2_id, connector_type)
				minifix_list = {}
				if minifix_data && minifix_data.key?('multi_post') &&
						minifix_data['multi_post'].key?('mapping')
					minifix_data['multi_post']['mapping'].each do |k, v|
						minifix_list.merge!(v['ids']) if v && v.key?('ids')
					end
				end
				return minifix_list
			end
			def clean_attributes(sheet_id)
				FrAttributeHelper.clean_attributes(sheet_id, @saver, self)
			end
			def update_attributes(sheet_id, params)
				FrAttributeHelper.update_attributes(
					sheet_id,
					params,
					@saver,
					self
				)
			end
			def clean_connector(sheet_id)
				attrs = self.attrs
				type_list = %w[
					connector
					cabineo
					auto_edging
					grooving
					dogbone
					chot_dot
					khoan_moi
					patV
					rafix
					lockdowel
					drawer_fitting
				]
				type_list.each do |con_typ|
					if attrs['root'][con_typ]
						remove_list = []
						attrs['root'][con_typ].each do |k, v|
							remove_list.push(k) if k.include?(sheet_id)
						end
						if remove_list.size > 0
							remove_list.each do |k|
								attrs['root'][con_typ].delete(k)
							end
						end
					end
				end
				@saver.save_comp_json(@frame, attrs)
			end
			def reverse_connector(connector_type, pairs, minifixID, normal)
				if %w[oc_cam chot_cam chot_go3 chot_go2].include?(
						connector_type
				   )
					type = 'connector'
				elsif %w[
						cabineo_d
						cabineo_d1
						cabineo_d2
						cabineo_d3
						cabineo_cap
				    ].include?(connector_type)
					type = 'cabineo'
				elsif %w[rafix_d rafix_d2].include?(connector_type)
					type = 'rafix'
				end
				FrMinifixHelper.reverse_minifix(
					normal,
					pairs,
					minifixID,
					type,
					self.attrs,
					@saver,
					@frame
				)
			end
			def remove_connector(connector_type, pairs, minifixID)
				type = connector_type
				if %w[oc_cam chot_cam chot_go3 chot_go2].include?(
						connector_type
				   )
					type = 'connector'
				elsif %w[dogbone_male dogbone_female].include?(connector_type)
					type = 'dogbone'
				elsif %w[
						cabineo_d
						cabineo_d1
						cabineo_d2
						cabineo_d3
						cabineo_cap
				    ].include?(connector_type)
					type = 'cabineo'
				elsif %w[rafix_d rafix_d2].include?(connector_type)
					type = 'rafix'
				elsif %w[drawer_fitting_1 drawer_fitting_2].include?(
						connector_type
				    )
					type = 'drawer_fitting'
				end
				FrMinifixHelper.remove_minifix(
					pairs,
					minifixID,
					self.attrs,
					@saver,
					@frame,
					type
				)
			end
			def move_connector(
				connector_type,
				pairs,
				minifixID,
				direction,
				distance
			)
				type = connector_type
				if %w[oc_cam chot_cam chot_go3 chot_go2].include?(
						connector_type
				   )
					type = 'connector'
				elsif %w[dogbone_male dogbone_female].include?(connector_type)
					type = 'dogbone'
				elsif %w[
						cabineo_d
						cabineo_d1
						cabineo_d2
						cabineo_d3
						cabineo_cap
				    ].include?(connector_type)
					type = 'cabineo'
				elsif %w[rafix_d rafix_d2].include?(connector_type)
					type = 'rafix'
				end
				FrMinifixHelper.move_minifix(
					pairs,
					minifixID,
					self.attrs,
					@saver,
					@frame,
					direction,
					distance,
					type
				)
			end
			def add_edging(sheet_id, face_index, edging_data)
				FrEdgingHelper.add_edging(
					sheet_id,
					face_index,
					edging_data,
					self
				)
			end
			def remove_edging(sheet_id, face_index)
				FrEdgingHelper.remove_edging(sheet_id, face_index, self)
			end
			def remove_multi_edging(sheet_id, face_index_arr)
				FrEdgingHelper.remove_multi_edging(
					sheet_id,
					face_index_arr,
					self
				)
			end
			def add_multi_edging(sheet_id, face_index_arr, edging_data)
				FrEdgingHelper.add_multi_edging(
					sheet_id,
					face_index_arr,
					edging_data,
					self
				)
			end
			def get_edging_hash(sheet_id)
				return FrEdgingHelper.get_edging_hash(sheet_id, self)
			end
			def root
				attrs = @saver.open_comp_json(@frame)
				return attrs['root']
			end
			def attrs
				attrs = @saver.open_comp_json(@frame)
				return attrs
			end
			def erase!
				@frame.erase!
				AMTF_STORE.remove_frame(self)
			end
			def transformation
				@frame.transformation if @frame && @frame.valid?
			end
			def entities
				@frame.entities
			end
			def boundingBox
				@boundingBox
			end
			def show_face_bound
				if @boundingBox
					@boundingBox.entities.each do |ent|
						if ent.is_a?(Sketchup::Group)
							if ent.get_attribute(
									'amtf_dict',
									'is_frame_bound'
							   ) == 'is_frame_bound'
								ent.visible = true
							end
						end
					end
				end
			end
			def hide_face_bound
				if @boundingBox
					@boundingBox.entities.each do |ent|
						if ent.is_a?(Sketchup::Group)
							if ent.get_attribute(
									'amtf_dict',
									'is_frame_bound'
							   ) == 'is_frame_bound'
								ent.visible = false
							end
						end
					end
				end
			end
			def draw_bounds
				@axes = @frame.entities.add_group
				@axes.name = 'axes_group'
				@axes.set_attribute 'amtf_dict', 'comp_type', 'amtf_axes_group'
				@boundingBox = @frame.entities.add_group
				@boundingBox.name = 'bound_group'
				@boundingBox.set_attribute 'amtf_dict',
				                           'comp_type',
				                           'amtf_bound_group'
				root = self.root
				vertices =
					root['boundingBox']['vertices'].map do |p|
						p1 =
							Geom::Point3d.new(
								Utilities.unitConvert(p['x']),
								Utilities.unitConvert(p['y']),
								Utilities.unitConvert(p['z'])
							)
					end
				x_axis = @axes.entities.add_line vertices[0], vertices[1]
				x_axis.set_attribute('amtf_dict', 'axes', 'x_axis')
				y_axis = @axes.entities.add_line vertices[0], vertices[3]
				y_axis.set_attribute('amtf_dict', 'axes', 'y_axis')
				z_axis = @axes.entities.add_line vertices[0], vertices[4]
				z_axis.set_attribute('amtf_dict', 'axes', 'z_axis')
				x_axis.visible = false
				y_axis.visible = false
				z_axis.visible = false
				@axes.locked = true
				if root['frame_type'] == 'blank_frame'
					@boundingBox.entities.add_cline vertices[0], vertices[1]
					@boundingBox.entities.add_cline vertices[1], vertices[2]
					@boundingBox.entities.add_cline vertices[2], vertices[3]
					@boundingBox.entities.add_cline vertices[0], vertices[3]
					@boundingBox.entities.add_cline vertices[4], vertices[5]
					@boundingBox.entities.add_cline vertices[5], vertices[6]
					@boundingBox.entities.add_cline vertices[6], vertices[7]
					@boundingBox.entities.add_cline vertices[4], vertices[7]
					@boundingBox.entities.add_cline vertices[0], vertices[4]
					@boundingBox.entities.add_cline vertices[1], vertices[5]
					@boundingBox.entities.add_cline vertices[2], vertices[6]
					@boundingBox.entities.add_cline vertices[3], vertices[7]
					model = Sketchup.active_model
					layers = model.layers
					new_layer = layers.add 'Amtf Bounds'
					@boundingBox.layer = new_layer
				end
			end
			def draw_geometries(node, parent)
				root = node['root']
				if root.key?('sheets')
					sheets = root['sheets']
					drawSheet(sheets, parent)
				end
				if root.key?('sub_part')
					for i in 0..root['sub_part'].size - 1
						sect = root['sub_part'][i]
						if sect['root']['node_type'] == 'frame'
							sub_part_frame = parent.entities.add_group
							sub_part_frame.set_attribute(
								'amtf_dict',
								'node_id',
								sect['root']['id']
							)
							sub_part_frame.set_attribute(
								'amtf_dict',
								'comp_type',
								'AmtfSubFrame'
							)
							draw_geometries(sect, sub_part_frame)
						else
							if sect['root']['node_type'] != 'with_frame' &&
									sect['root']['sections'].size == 0
								drawGlass(sect['root'], parent)
							end
							draw_geometries(sect, parent)
						end
					end
				end
				if root.key?('sections')
					for i in 0..root['sections'].size - 1
						sect = root['sections'][i]
						if sect['root']['node_type'] == 'frame'
							frame = parent.entities.add_group
							frame.set_attribute(
								'amtf_dict',
								'section_id',
								sect['root']['parentID']
							)
							frame.set_attribute(
								'amtf_dict',
								'node_id',
								sect['root']['id']
							)
							frame.set_attribute(
								'amtf_dict',
								'comp_type',
								'AmtfSubFrame'
							)
							draw_geometries(sect, frame)
						elsif sect['root']['node_type'] == 'door'
							sheet_group = parent.entities.add_group
							back_faces = sect['root']['faces']
							back_faces.each do |k, ptr_arr|
								temp_arr = []
								for j in 0..ptr_arr.size - 1
									temp_arr <<
										Geom::Point3d.new(
											Utilities.unitConvert(
												ptr_arr[j]['x']
											),
											Utilities.unitConvert(
												ptr_arr[j]['y']
											),
											Utilities.unitConvert(
												ptr_arr[j]['z']
											)
										)
								end
								face = sheet_group.entities.add_face(temp_arr)
							end
							sheet_group.set_attribute 'amtf_dict',
							                          'comp_type',
							                          'door'
							sheet_group.set_attribute 'amtf_dict',
							                          'sheet_type',
							                          'smart_door'
							sheet_group.set_attribute 'amtf_dict',
							                          'sheet_id',
							                          sect['root']['id']
							sheet_group.set_attribute 'amtf_dict',
							                          'door_id',
							                          root['id']
							sheet_group.set_attribute 'amtf_dict',
							                          'sheet_index',
							                          i
							sheet_group.name = OB[:DOOR]
						else
							if sect['root']['node_type'] != 'with_frame' &&
									sect['root']['sections'].size == 0
								drawGlass(sect['root'], parent)
							end
							draw_geometries(sect, parent)
						end
					end
				end
				if root.key?('back')
					if root['back']['sections'] &&
							root['back']['sections'].size > 0
						index = 0
						root['back']['sections'][0]['root']['sections']
							.each do |sec|
							sheet_group = parent.entities.add_group
							back_faces = sec['root']['faces']
							back_faces.each do |k, ptr_arr|
								temp_arr = []
								for j in 0..ptr_arr.size - 1
									temp_arr <<
										Geom::Point3d.new(
											Utilities.unitConvert(
												ptr_arr[j]['x']
											),
											Utilities.unitConvert(
												ptr_arr[j]['y']
											),
											Utilities.unitConvert(
												ptr_arr[j]['z']
											)
										)
								end
								face = sheet_group.entities.add_face(temp_arr)
							end
							sheet_group.set_attribute 'amtf_dict',
							                          'comp_type',
							                          'back'
							sheet_group.set_attribute 'amtf_dict',
							                          'sheet_type',
							                          'smart_back'
							sheet_group.set_attribute 'amtf_dict',
							                          'sheet_id',
							                          sec['root']['id']
							sheet_group.set_attribute 'amtf_dict',
							                          'back_id',
							                          root['back']['id']
							sheet_group.set_attribute 'amtf_dict',
							                          'sheet_index',
							                          index
							sheet_group.name = OB[:BACK]
							index = index + 1
						end
					end
				end
			end
			def drawGlass(root, parent)
				return unless root['faces'] || root['door']
				return unless root['sections'].size == 0
				glass_group = parent.entities.add_group
				glass_group.set_attribute(
					'amtf_dict',
					'is_section_face',
					'is_section_face'
				)
				glass_group.name = 'section_face'
				if root.key?('faces')
					root['faces'].each do |k, ptr_arr|
						temp_arr = []
						for j in 0..ptr_arr.size - 1
							p =
								Geom::Point3d.new(
									Utilities.unitConvert(ptr_arr[j]['x']),
									Utilities.unitConvert(ptr_arr[j]['y']),
									Utilities.unitConvert(ptr_arr[j]['z'])
								)
							temp_arr << p
						end
						temp_arr = Utilities.uniq_points(temp_arr)
						if temp_arr.size >= 3
							face = glass_group.entities.add_face(temp_arr)
							material = AMTF_STORE.create_material
							face.material = material
							face.back_material = material
							AMTF_STORE.add_sections(face)
							face.set_attribute(
								'amtf_dict',
								'section_id',
								root['id']
							)
							face.set_attribute(
								'amtf_dict',
								'frame_id_of_section',
								@id
							)
							face.visible = false
						end
					end
					glass_group
						.entities
						.grep(Sketchup::Edge)
						.each { |e| e.visible = false } if AMTF_STORE
						.is_showing == false
				end
			end
			def drawSheet(sheets, parent)
				sheets.each do |key, arr|
					arr.each do |sheet|
						sheet_id = sheet['root']['id']
						next if sheet['root']['deleted'] == true
						edging_hash = self.get_edging_hash(sheet_id)
						if sheet['root']['sheet_type'] === 'curve_toebase'
							curve_arc = sheet['root']['curve_arc']['edges']
							toebase =
								Utilities.unitConvert(
									sheet['root']['curve_arc']['height']
								)
							input_thick =
								Utilities.unitConvert(
									sheet['root']['curve_arc']['thick']
								)
							groove_spacing =
								Utilities.unitConvert(
									sheet['root']['curve_arc']['groove_spacing']
								)
							groove_width =
								Utilities.unitConvert(
									sheet['root']['curve_arc']['groove_width']
								)
							extra_grooves =
								Utilities.unitConvert(
									sheet['root']['curve_arc']['extra_grooves']
								)
							cure_data =
								generateCurveSheet(
									curve_arc,
									toebase,
									input_thick,
									groove_width,
									groove_spacing,
									extra_grooves
								)
							if cure_data
								flat_group = parent.entities.add_group
								flat_group.set_attribute 'amtf_dict',
								                         'comp_type',
								                         'flating_curve'
								if sheet['root']['sheet_name']
									if OB.key?(sheet['root']['sheet_name'])
										flat_group.name =
											OB[sheet['root']['sheet_name']]
									else
										flat_group.name =
											sheet['root']['sheet_name']
									end
								end
								cure_data[:faces].each do |key, points|
									points = Utilities.uniq_points(points)
									if points.size > 2
										flat_group.entities.add_face(points)
									end
								end
								cure_data[:grooves].each do |points|
									chanel_grp = flat_group.entities.add_group
									flat_group.set_attribute(
										'amtf_dict',
										'has_groove',
										'yes'
									)
									chanel_face =
										chanel_grp.entities.add_face(points)
									chanel_grp.set_attribute 'amtf_dict',
									                         'type',
									                         'chanel'
									chanel_grp.set_attribute 'amtf_dict',
									                         'minifixID',
									                         Utilities
											.CreatUniqueid
									chanel_grp.set_attribute 'amtf_dict',
									                         'groove_name',
									                         OB[
											:curve_sheet_groove
									                         ]
									save_config = {
										name: OB[:curve_sheet_groove],
										groove_depth: 5,
										extra_grooves: 2,
										color: '#70FFFF',
										default: false,
										toolNumber: 1,
										groove_type: 4,
										groove_passes: 1
									}
									chanel_grp.set_attribute 'amtf_dict',
									                         'groove_depth',
									                         5
									chanel_grp.set_attribute 'amtf_dict',
									                         'extra_grooves',
									                         2
									chanel_grp.set_attribute(
										'amtf_dict',
										'config_data',
										save_config.to_json
									)
									new_layer =
										Sketchup.active_model
											.layers.add 'Amtf Groove'
									chanel_grp.material = '#70FFFF'
								end
							end
							flat_group.visible = false
						end
						sheet_group = parent.entities.add_group
						if sheet['root']['sheet_name']
							if OB.key?(sheet['root']['sheet_name'])
								sheet_group.name =
									OB[sheet['root']['sheet_name']]
							else
								sheet_group.name = sheet['root']['sheet_name']
							end
						end
						sheet_group.set_attribute 'amtf_dict',
						                          'sheet_id',
						                          sheet_id
						sheet_group.set_attribute 'amtf_dict',
						                          'sheet_type',
						                          sheet['root']['sheet_type']
						error = false
						sheet['root']['faces'].each do |k, ptr_arr|
							temp_arr = []
							for j in 0..ptr_arr.size - 1
								temp_arr <<
									Geom::Point3d.new(
										Utilities.unitConvert(ptr_arr[j]['x']),
										Utilities.unitConvert(ptr_arr[j]['y']),
										Utilities.unitConvert(ptr_arr[j]['z'])
									)
							end
							pre_size = temp_arr.size
							temp_arr = Utilities.uniq_points(temp_arr)
							face = sheet_group.entities.add_face(temp_arr)
							face.set_attribute 'amtf_dict', 'face_index', k
							if edging_hash && edging_hash[k]
								face.set_attribute 'amtf_dict',
								                   'edge_banding',
								                   'true'
								face.set_attribute 'amtf_dict',
								                   'edge_type_id',
								                   edging_hash[k][
										'edge_type_id'
								                   ]
								face.set_attribute 'amtf_dict',
								                   'edge_type_name',
								                   edging_hash[k][
										'edge_type_name'
								                   ]
								face.set_attribute 'amtf_dict',
								                   'edge_type_thick',
								                   edging_hash[k][
										'edge_type_thick'
								                   ]
								face.material = edging_hash[k]['color']
							end
							post_size = temp_arr.size
							error = true if post_size - pre_size != 0
						end
						sheet_trans =
							Geom::Transformation.new(sheet['root']['transform'])
						sheet_group.transform!(sheet_trans)
						if sheet['root']['sheet_type'] === 'DRAWER_DOOR' ||
								sheet['root']['sheet_type'] === 'smart_door' ||
								sheet['root']['sheet_type'] === 'SMART_DOOR' ||
								sheet['root']['sheet_type'] === 'DOOR'
							sheet_group.set_attribute 'amtf_dict',
							                          'comp_type',
							                          'no_hole'
						end
						if sheet['root']['sheet_type'] === 'curve_toebase'
							sheet_group.set_attribute 'amtf_dict',
							                          'comp_type',
							                          'curve_toebase'
						end
						if sheet['root']['sheet_attributes'] &&
								!sheet['root']['sheet_attributes'].empty?
							sheet['root']['sheet_attributes']
								.each do |at_key, at_val|
								sheet_group.set_attribute 'amtf_dict',
								                          at_key,
								                          at_val
							end
							no_auto_edge_banding =
								sheet['root']['sheet_attributes'][
									'no_auto_edge_banding'
								]
							if no_auto_edge_banding &&
									!no_auto_edge_banding.nil?
								# �?đây ch�?đ�?lấy edge_face nên tạo cái sheet_part như này,
								#  có th�?chưa chính xác hoàn toàn nhưng có th�?
								# chấp nhận được
								sheet_part =
									AmtfSheet.new(
										sheet_group,
										sheet_trans,
										IDENTITY
									)
								edge_faces = sheet_part.edge_face
								edge_faces.each { |f| f.material = '#333333' }
							end
						end
						if sheet['root']['strange_hole']
							sheet['root']['strange_hole'].each do |stranger|
								if stranger['shapeType'] == 'rectangular'
									strimmer = stranger['strimmer']
									strimmer_group = parent.entities.add_group
									error1 = false
									strimmer.each do |k, ptr_arr|
										temp_arr = []
										for j in 0..ptr_arr.size - 1
											temp_arr <<
												Geom::Point3d.new(
													Utilities.unitConvert(
														ptr_arr[j]['x']
													),
													Utilities.unitConvert(
														ptr_arr[j]['y']
													),
													Utilities.unitConvert(
														ptr_arr[j]['z']
													)
												)
										end
										pre_size = temp_arr.size
										temp_arr =
											Utilities.uniq_points(temp_arr)
										face =
											strimmer_group.entities.add_face(
												temp_arr
											)
										post_size = temp_arr.size
										if post_size - pre_size != 0
											error1 = true
										end
									end
									strimmer_group.transform!(sheet_trans)
									if stranger['jointType'] == 'substract'
										target =
											SolidOperations.subtract(
												sheet_group,
												strimmer_group
											)
									elsif stranger['jointType'] == 'union'
										target =
											SolidOperations.union(
												sheet_group,
												strimmer_group
											)
									end
									if strimmer_group && strimmer_group.valid?
										strimmer_group.erase!
									end
								elsif stranger['shapeType'] == 'circle'
									strimmer = stranger['strimmer']
									strimmer_group = parent.entities.add_group
									center = strimmer['center']
									center =
										Geom::Point3d.new(
											center['x'].to_s.to_l,
											center['y'].to_s.to_l,
											center['z'].to_s.to_l
										)
									normal = strimmer['normal']
									normal =
										Geom::Vector3d.new(
											normal['x'].to_s.to_l,
											normal['y'].to_s.to_l,
											normal['z'].to_s.to_l
										).normalize
									pull_depth =
										strimmer['pull_depth'].to_s.to_l
									radius = strimmer['radius'].to_s.to_l
									circle_edges =
										strimmer_group
											.entities.add_circle center,
										                                   normal,
										                                   radius,
										                                   92
									cỉcles_face =
										strimmer_group.entities.add_face(
											circle_edges
										)
									cỉcles_face.pushpull(pull_depth)
									strimmer_group.transform!(sheet_trans)
									if stranger['jointType'] == 'substract'
										target =
											SolidOperations.subtract(
												sheet_group,
												strimmer_group
											)
									elsif stranger['jointType'] == 'union'
										target =
											SolidOperations.union(
												sheet_group,
												strimmer_group
											)
									end
									if strimmer_group && strimmer_group.valid?
										strimmer_group.erase!
									end
								end
							end
						end
					end
				end
			end
			def update(attrs)
				@saver.save_comp_json(@frame, attrs)
				hidden_sheet = {}
				query = AmtfQuery.new
				hide_sheet_list = query.query_sheets(@frame.entities, false)
				hide_sheet_list.each do |sh|
					ent = sh.comp
					next if ent.visible?
					node_id = ent.get_attribute 'amtf_dict', 'node_id'
					sheet_id = ent.get_attribute 'amtf_dict', 'sheet_id'
					if !sheet_id.nil?
						hidden_sheet[sheet_id] = {
							mat: ent.material,
							visible: ent.visible?
						}
					elsif !node_id.nil?
						hidden_sheet[node_id] = {
							mat: ent.material,
							visible: ent.visible?
						}
					end
				end
				# Ch�?này đang b�?chạy lại 2 lần?? tạm thời chấp nhận vậy
				@frame.entities.each do |ent|
					if ent.is_a?(Sketchup::Group)
						node_id = ent.get_attribute 'amtf_dict', 'node_id'
						sheet_id = ent.get_attribute 'amtf_dict', 'sheet_id'
						if !sheet_id.nil?
							hidden_sheet[sheet_id] = {
								mat: ent.material,
								visible: ent.visible?
							}
						elsif !node_id.nil?
							hidden_sheet[node_id] = {
								mat: ent.material,
								visible: ent.visible?
							}
						end
						comp_type = ent.get_attribute 'amtf_dict', 'comp_type'
						if comp_type == 'AmtfFrame' ||
								comp_type == 'AmtfSubFrame'
							AMTF_STORE.frame_premitives.delete(
								ent.persistent_id
							)
						end
					end
					ent.erase!
				end
				self.draw_bounds
				self.draw_geometries(attrs, @frame)
				self.trimStretcher
				self.create_connector
				@frame.entities.each do |ent|
					if ent.is_a?(Sketchup::Group)
						node_id = ent.get_attribute 'amtf_dict', 'node_id'
						sheet_id = ent.get_attribute 'amtf_dict', 'sheet_id'
						if !sheet_id.nil? && hidden_sheet.key?(sheet_id)
							ent.material = hidden_sheet[sheet_id][:mat]
							ent.visible = hidden_sheet[sheet_id][:visible]
						elsif !node_id.nil? && hidden_sheet.key?(node_id)
							ent.material = hidden_sheet[node_id][:mat]
							ent.visible = hidden_sheet[node_id][:visible]
						end
					end
				end
				hide_sheet_list2 = query.query_sheets(@frame.entities, false)
				hide_sheet_list2.each do |sh|
					ent = sh.comp
					node_id = ent.get_attribute 'amtf_dict', 'node_id'
					sheet_id = ent.get_attribute 'amtf_dict', 'sheet_id'
					if !sheet_id.nil? && hidden_sheet.key?(sheet_id)
						ent.material = hidden_sheet[sheet_id][:mat]
						ent.visible = hidden_sheet[sheet_id][:visible]
					elsif !node_id.nil? && hidden_sheet.key?(node_id)
						ent.material = hidden_sheet[node_id][:mat]
						ent.visible = hidden_sheet[node_id][:visible]
					end
				end
			end
			def trimStretcher
				query = AmtfQuery.new
				ver_divs =
					query.query_get_part(
						self.entities,
						%w[VER_DIV HOZ_DIV ENDL ENDR]
					)
				stretchers =
					query.query_get_part(
						self.entities,
						%w[
							TOEBASE
							TOP_STRETCHER
							STRETCHER
							DRAWER_SUPPORT
							BOTTOM_STRETCHER
						]
					)
				for i in 0..ver_divs.size - 1
					for j in 0..stretchers.size - 1
						stretch = stretchers[j].comp
						ver = ver_divs[i].comp
						target = SolidOperations.subtract(ver, stretch)
					end
				end
			end
			def findPath(id)
				arr = []
				root = self.root
				if hasPath(root, arr, id)
					return arr
				else
					return []
				end
			end
			def hasPath(root, arr, id)
				return false if root.nil? || root.empty?
				arr.push(root)
				return true if root['id'] == id
				if root.key?('sections')
					for i in 0..root['sections'].size - 1
						if hasPath(root['sections'][i]['root'], arr, id)
							return true
						end
					end
				end
				if root.key?('sub_part')
					for i in 0..root['sub_part'].size - 1
						if hasPath(root['sub_part'][i]['root'], arr, id)
							return true
						end
					end
				end
				arr.pop
				return false
			end
			def traverseCallback(&block)
				attrs = self.attrs
				node_queue = [attrs]
				until node_queue.empty?
					node = node_queue.shift
					yield node
					if node['root'] && node['root']['sections']
						node_queue = node_queue.concat(node['root']['sections'])
					end
					if node['root'] && node['root']['sub_part']
						node_queue = node_queue.concat(node['root']['sub_part'])
					end
				end
				return attrs
			end
			def generateCurveSheet(
				outerloop,
				h,
				d,
				groove_width,
				groove_spacing,
				extra_grooves
			)
				len = 0
				curve_len = 0
				groove_half = 0.5 * groove_width
				grooves = []
				prevIsLine = false
				prevIsCurve = false
				org = outerloop[0]['startPoint']
				org =
					Geom::Point3d.new Utilities.unitConvert(org['x']),
					                  Utilities.unitConvert(org['y']),
					                  Utilities.unitConvert(org['z'])
				for i in 0..outerloop.size - 1
					startPoint = outerloop[i]['startPoint']
					endPoint = outerloop[i]['endPoint']
					startPoint =
						Geom::Point3d.new Utilities.unitConvert(
								startPoint['x']
						                  ),
						                  Utilities.unitConvert(
								startPoint['y']
						                  ),
						                  Utilities.unitConvert(startPoint['z'])
					endPoint =
						Geom::Point3d.new Utilities.unitConvert(endPoint['x']),
						                  Utilities.unitConvert(endPoint['y']),
						                  Utilities.unitConvert(endPoint['z'])
					curve = outerloop[i]['curve']
					if curve
						theta = curve['end_angle'] - curve['start_angle']
						len +=
							(Utilities.unitConvert(curve['radius']) * theta).abs
						curve_len = 0 if prevIsLine
						curve_len +=
							(Utilities.unitConvert(curve['radius']) * theta).abs
						prevIsCurve = true
						prevIsLine = false
					else
						if prevIsCurve
							# T�?curvr sang line: tính
							# khoảng cách đoạn cong = curve_len
							# lấy len tr�?đi curve_len - thêm 1 lần extra thì ra điểm bắt đầu
							# lấy len cộng thêm 2 lần extra thì ra tổng đ�?dài
							tong_khoang_cach =
								curve_len.to_l + 2 * extra_grooves.to_l
							numOfGrooves =
								(tong_khoang_cach.to_l + groove_spacing.to_l) /
									(groove_width.to_l + groove_spacing.to_l)
							numOfGrooves = numOfGrooves.floor
							x_diem_dau =
								org.x.to_l + len.to_l - tong_khoang_cach.to_l
							for k in 0..numOfGrooves - 1
								grooves.push(
									[
										Geom::Point3d.new(
											x_diem_dau.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l
										),
										Geom::Point3d.new(
											x_diem_dau.to_l +
												groove_width.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l
										),
										Geom::Point3d.new(
											x_diem_dau.to_l +
												groove_width.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l + h.to_l
										),
										Geom::Point3d.new(
											x_diem_dau.to_l +
												k *
													(
														groove_width.to_l +
															groove_spacing.to_l
													),
											org.y.to_l + d.to_l,
											org.z.to_l + h.to_l
										)
									]
								)
							end
						end
						prevIsCurve = false
						prevIsLine = true
						curve_len = 0
						len += startPoint.distance(endPoint)
					end
					if (i === outerloop.size - 1) && curve_len > 0
						tong_khoang_cach =
							curve_len.to_l + 2 * extra_grooves.to_l
						numOfGrooves =
							(tong_khoang_cach.to_l + groove_spacing.to_l) /
								(groove_width.to_l + groove_spacing.to_l)
						numOfGrooves = numOfGrooves.floor
						x_diem_dau =
							org.x.to_l + len.to_l - tong_khoang_cach.to_l
						for k in 0..numOfGrooves - 1
							grooves.push(
								[
									Geom::Point3d.new(
										x_diem_dau.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l
									),
									Geom::Point3d.new(
										x_diem_dau.to_l + groove_width.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l
									),
									Geom::Point3d.new(
										x_diem_dau.to_l + groove_width.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l + h.to_l
									),
									Geom::Point3d.new(
										x_diem_dau.to_l +
											k *
												(
													groove_width.to_l +
														groove_spacing.to_l
												),
										org.y.to_l + d.to_l,
										org.z.to_l + h.to_l
									)
								]
							)
						end
					end
				end
				faces = {
					face_1: [
						Geom::Point3d.new(org.x, org.y, org.z),
						Geom::Point3d.new(org.x + len, org.y, org.z),
						Geom::Point3d.new(org.x + len, org.y, org.z + h),
						Geom::Point3d.new(org.x, org.y, org.z + h)
					],
					face_2: [
						Geom::Point3d.new(org.x, org.y + d, org.z),
						Geom::Point3d.new(org.x, org.y + d, org.z + h),
						Geom::Point3d.new(org.x + len, org.y + d, org.z + h),
						Geom::Point3d.new(org.x + len, org.y + d, org.z)
					],
					face_3: [
						Geom::Point3d.new(org.x, org.y, org.z),
						Geom::Point3d.new(org.x, org.y, org.z + h),
						Geom::Point3d.new(org.x, org.y + d, org.z + h),
						Geom::Point3d.new(org.x, org.y + d, org.z)
					],
					face_4: [
						Geom::Point3d.new(org.x + len, org.y, org.z),
						Geom::Point3d.new(org.x + len, org.y + d, org.z),
						Geom::Point3d.new(org.x + len, org.y + d, org.z + h),
						Geom::Point3d.new(org.x + len, org.y, org.z + h)
					],
					face_5: [
						Geom::Point3d.new(org.x, org.y, org.z),
						Geom::Point3d.new(org.x, org.y + d, org.z),
						Geom::Point3d.new(org.x + len, org.y + d, org.z),
						Geom::Point3d.new(org.x + len, org.y, org.z)
					],
					face_6: [
						Geom::Point3d.new(org.x, org.y, org.z + h),
						Geom::Point3d.new(org.x + len, org.y, org.z + h),
						Geom::Point3d.new(org.x + len, org.y + d, org.z + h),
						Geom::Point3d.new(org.x, org.y + d, org.z + h)
					]
				}
				return { grooves: grooves, faces: faces }
			end
		end
	end
end