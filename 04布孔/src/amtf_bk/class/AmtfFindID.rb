# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfFindID
			attr_accessor :hiden_list, :option, :list, :keep_hiden
			def initialize(); end
			def set_data(find_data)
				@option = find_data['option']
				@keep_hiden = find_data['option']['keep_hiden']
				@list = find_data['list']
				if @list
					@list.map! do |x|
						x.to_s
						x.downcase
					end
				end
				@hiden_list = []
			end
			def show_all
				while @hiden_list.size > 0
					comp = @hiden_list.pop
					comp.hidden = false
				end
			end
			def execute
				while @hiden_list.size > 0
					comp = @hiden_list.pop
					comp.hidden = false
				end
				return if @list.size == 0
				return if !@option
				Sketchup.active_model.start_operation(OB[:find_by_id], true)
				@amtf_query = AmtfQuery.new
				@parts =
					@amtf_query.query_sheets(
						Sketchup.active_model.entities,
						false
					)
				@parts.each do |p|
					in_array = false
					case @option['find_by']
					when 'find_by_id'
						part_id = p.get_numbering_id
						@list.each do |s|
							in_array = true if s.to_i == part_id.to_i
						end
					when 'find_by_name'
						part_label = p.part_label
						next if part_label.nil?
						part_label.downcase!
						@list.each do |s|
							in_array = true if part_label.include?(s)
						end
					end
					if in_array
						Sketchup.active_model.selection.add(p.comp)
						p.comp.hidden = false
					else
						@hiden_list.push(p.comp)
						p.comp.hidden = true
					end
				end
				Sketchup.active_model.commit_operation
			end
		end
	end
end