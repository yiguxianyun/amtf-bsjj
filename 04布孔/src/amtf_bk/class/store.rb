# encoding: UTF-8
module AMTF
	module BUKONG
		def self.print_store(id)
			puts AMTF_STORE.frame_premitives[id].root
		end
		def self.amtf_show_hide_grid
			AMTF_STORE.is_showing = !AMTF_STORE.is_showing
			if AMTF_STORE.is_showing == true
				AMTF_STORE.show_grid
				view = Sketchup.active_model.active_view
				view.refresh
			else
				AMTF_STORE.hide_grid
				view = Sketchup.active_model.active_view
				view.refresh
			end
		end
		class AMTFStore
			attr_accessor :active_tool,
			              :frame_premitives,
			              :init_open_json,
			              :counter,
			              :mapping,
			              :is_showing,
			              :material
			def initialize
				@sections = {}
				@active_drag_tool = false
				@frame_premitives = {}
				@active_tool = nil
				@init_open_json = false
				@counter = 0
				@mapping = {}
				@is_showing = false
			end
			def create_material
				mats = Sketchup.active_model.materials
				@material =
					Sketchup.active_model.materials.add(
						'amtf_glass'
					) unless @material = mats['amtf_glass']
				@material.color = 'green'
				@material.alpha = 0
				return @material
			end
			def reset
				@sections = {}
				@active_drag_tool = false
				@frame_premitives = {}
				@active_tool = nil
				@init_open_json = false
				@counter = 0
				@mapping = {}
			end
			def sections
				@sections
			end
			def add_frame(frame)
				@frame_premitives[frame.id] = frame
				@mapping[frame.root['id']] = frame if frame.root
			end
			def remove_frame(frame)
				@frame_premitives.delete(frame.id)
			end
			def frame_premitives
				@frame_premitives
			end
			def frame(id)
				@frame_premitives[id]
			end
			def set_active_drag_tool
				@active_drag_tool = true
			end
			def unset_active_drag_tool
				@active_drag_tool = false
			end
			def mapping
				@mapping
			end
			def active_drag_tool
				@active_drag_tool
			end
			def add_sections(face)
				@sections[face] = face
			end
			def show_all_sections
				@sections.each do |k, sec|
					sec.visible = true if sec && sec.valid?
				end
			end
			def show_grid
				@sections.each do |k, sec|
					if sec && sec.valid?
						sec.visible = true
						sec.edges.each { |e| e.visible = true }
					end
				end
			end
			def hide_all_sections
				@sections.each do |k, sec|
					sec.visible = false if sec && sec.valid?
				end
			end
			def hide_grid
				@sections.each do |k, sec|
					if sec && sec.valid?
						sec.visible = false
						sec.edges.each { |e| e.visible = false }
					end
				end
			end
		end
	end
end