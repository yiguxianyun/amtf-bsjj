# encoding: UTF-8
module AMTF
    module BUKONG 
        class AmtfBox
            LOCK_X = 39
            LOCK_Y = 37
            LOCK_Z = 38
            attr_accessor   :corners, 
                            :origin,
                            :selected_point,
                            :adjust_distance
            def initialize( vertices, matrix, converted = false )
                if !converted
                    @init_conners = vertices.map{
                        |p|
                        p = Geom::Point3d.new Utilities.unitConvert(p['x']), 
                                            Utilities.unitConvert(p['y']), 
                                            Utilities.unitConvert(p['z']);
                    }
                else
                    @init_conners = vertices.clone()
                end
                @corners = @init_conners.clone()
                @origin = @init_conners[0]
                @matrix = matrix
                @rightPoint = Utilities.find_center_of_points([@init_conners[1], @init_conners[2], @init_conners[5], @init_conners[6]])
                @leftPoint = Utilities.find_center_of_points([@init_conners[0], @init_conners[3], @init_conners[4], @init_conners[7]])
                @bottomPoint = Utilities.find_center_of_points([@init_conners[0], @init_conners[1], @init_conners[2], @init_conners[3]])
                @topPoint = Utilities.find_center_of_points([@init_conners[4], @init_conners[5], @init_conners[6], @init_conners[7]])
                @frontPoint = Utilities.find_center_of_points([@init_conners[2], @init_conners[3], @init_conners[6], @init_conners[7]])
                @backPoint = Utilities.find_center_of_points([@init_conners[0], @init_conners[1], @init_conners[5], @init_conners[4]])                
                @scale_point =[
                    @rightPoint, 
                    @leftPoint, 
                    @bottomPoint, 
                    @topPoint, 
                    @frontPoint, 
                    @backPoint]
                @selected_point = nil
                @scale_point = @scale_point.concat(@corners)
                @pick_points = []    
            end       
            def pick_scale_point(view, point)
                conners = @scale_point.map{ 
                    |p| 
                   p1 = p.transform(@matrix)
                   p = view.screen_coords(p1)
                }
                pick = view.screen_coords(point)
                minD = Float::INFINITY
                index = 0
                for i in 0..conners.size - 1
                    co = conners[ i ]
                    dist = co.distance( pick )
                    if dist < minD
                        minD = dist 
                        index = i
                    end
                end
                if index == 0
                    @selected_point = {
                        :point => @rightPoint.transform(@matrix),
                        :snap => "rightPoint"
                    }
                elsif index == 1
                    @selected_point = {
                        :point => @leftPoint.transform(@matrix),
                        :snap => "leftPoint"
                    }
                elsif index == 2
                    @selected_point = {
                        :point => @bottomPoint.transform(@matrix),
                        :snap => "bottomPoint"
                    }
                elsif index == 3
                     @selected_point = {
                        :point => @topPoint.transform(@matrix),
                        :snap => "topPoint"
                    }
                elsif index == 4
                    @selected_point = {
                        :point => @frontPoint.transform(@matrix),
                        :snap => "frontPoint"
                    }
                elsif index == 5 
                     @selected_point = {
                        :point => @backPoint.transform(@matrix),
                        :snap => "backPoint"
                    }
                elsif index == 6 
                     @selected_point = {
                        :point => @corners[0].transform(@matrix),
                        :snap => "Conner0"
                    }
                elsif index == 7
                     @selected_point = {
                        :point => @corners[1].transform(@matrix),
                        :snap => "Conner1"
                    }
                elsif index == 8
                     @selected_point = {
                        :point => @corners[2].transform(@matrix),
                        :snap => "Conner2"
                    }
                elsif index == 9
                     @selected_point = {
                        :point => @corners[3].transform(@matrix),
                        :snap => "Conner3"
                    }
                elsif index == 10
                     @selected_point = {
                        :point => @corners[4].transform(@matrix),
                        :snap => "Conner4"
                    }
                elsif index == 11
                     @selected_point = {
                        :point => @corners[5].transform(@matrix),
                        :snap => "Conner5"
                    }
                elsif index == 12
                     @selected_point = {
                        :point => @corners[6].transform(@matrix),
                        :snap => "Conner6"
                    }
                elsif index == 13
                     @selected_point = {
                        :point => @corners[7].transform(@matrix),
                        :snap => "Conner7"
                    }
                end
                return @selected_point
            end  
            def free_move(view, moving_point, resize_direction, adjust_distance = nil )  
                if(!resize_direction.nil?)
                    org = Geom::Point3d.new ;
                    org.transform!( @matrix )
                    axis_point = Geom::Point3d.new
                    if resize_direction == LOCK_X
                        axis_point = Geom::Point3d.new(1, 0, 0);
                        axis_point.transform!( @matrix );
                    elsif resize_direction == LOCK_Y
                        axis_point = Geom::Point3d.new(0, 1, 0);
                        axis_point.transform!( @matrix );
                    elsif resize_direction == LOCK_Z
                        axis_point = Geom::Point3d.new(0, 0, 1);
                        axis_point.transform!( @matrix );
                    end
                    direction = org.vector_to(axis_point);
                    pick_point = @selected_point[:point]
                    lock_point = pick_point.transform(Geom::Transformation.new direction)
                    direction_line = [pick_point, direction]
                    projected = moving_point.project_to_line(direction_line)
                    vect = pick_point.vector_to( projected )                    
                    distance_factor = 1
                    distance_factor = -1 if direction.dot(vect) < 0      
                    distance = pick_point.distance(projected)
                    if adjust_distance
                        distance = adjust_distance                    
                    end
                    trans = Geom::Transformation.new (Utilities.vectorMultiply(vect.normalize(), distance))
                    trans =  @matrix.inverse * trans * @matrix
                    @corners = @init_conners.map{ |p| p.transform( trans)}
                    return {
                        :distance => distance,
                        :factor => distance_factor,
                        :trans =>  trans
                    }
                else
                    vect = @selected_point[:point].vector_to(moving_point) 
                    trans = Geom::Transformation.new (vect)            
                    trans = Geom::Transformation.new (Utilities.vectorMultiply(vect.normalize(), adjust_distance)) if !adjust_distance.nil?
                    trans =  @matrix.inverse * trans * @matrix
                    @corners = @init_conners.map{ |p| p.transform( trans)}                  
                    return {:trans =>  trans}
                end
            end
            def move( view, moving_point, axis, adjust_distance = nil )  
                org = Geom::Point3d.new ;
                org.transform!( @matrix )
                axis_point = Geom::Point3d.new
                if axis == "X_AXIS"
                    axis_point = Geom::Point3d.new(1, 0, 0);
                    axis_point.transform!( @matrix );
                elsif axis == "Y_AXIS"
                     axis_point = Geom::Point3d.new(0, 1, 0);
                    axis_point.transform!( @matrix );
                elsif axis == "Z_AXIS"
                    axis_point = Geom::Point3d.new(0, 0, 1);
                    axis_point.transform!( @matrix );
                end
                direction = org.vector_to(axis_point);
                pick_point = @selected_point[:point]
                lock_point = pick_point.transform(Geom::Transformation.new direction)
                direction_line = [pick_point, direction]
                projected = moving_point.project_to_line(direction_line)
                vect = pick_point.vector_to( projected )
                distance_factor = 1
                distance_factor = -1 if direction.dot(vect) < 0     
                distance = pick_point.distance(projected)
                if adjust_distance
                    distance = adjust_distance                    
                end                 
                tr = Geom::Transformation.new (Utilities.vectorMultiply(vect.normalize(), distance))
                trans =  @matrix.inverse * tr * @matrix 
                 @corners = @init_conners.map{ |p| p.transform( trans)}
                return {
                    :distance => distance,
                    :factor => distance_factor
                }
            end
            def resizeX( view, moving_point, pickRight, adjust_distance = nil )       
                org = Geom::Point3d.new ;
                org.transform!( @matrix )
                x_point = Geom::Point3d.new(1, 0, 0);
                x_point.transform!( @matrix );
                direction = org.vector_to(x_point);
                pick_point = @selected_point[:point]
                direction_line = [pick_point, direction]
                projected = moving_point.project_to_line(direction_line)
                vect = pick_point.vector_to( projected )
                distance = pick_point.distance(projected)
                if adjust_distance
                    distance = adjust_distance                    
                end
                tr = Geom::Transformation.new (Utilities.vectorMultiply(vect.normalize(), distance))
                trans =  @matrix.inverse * tr * @matrix 
                distance_factor = 1
                distance_factor = -1 if direction.dot(vect) < 0
                if( pickRight == true )                            
                    @corners[ 1 ] = @init_conners[1].transform( trans)
                    @corners[ 2 ] = @init_conners[2].transform( trans)
                    @corners[ 5 ] = @init_conners[5].transform( trans)
                    @corners[ 6 ] = @init_conners[6].transform( trans)   
                else       
                    @corners[ 0 ] = @init_conners[0].transform( trans)
                    @corners[ 3 ] = @init_conners[3].transform( trans)
                    @corners[ 4 ] = @init_conners[4].transform( trans)
                    @corners[ 7 ] = @init_conners[7].transform( trans)  
                end
                frame_width = @corners[ 0 ].distance(@corners[ 1 ])
                frame_height = @corners[ 0 ].distance(@corners[ 4 ])
                frame_depth = @corners[ 0 ].distance(@corners[ 3 ])
                conr1 = Utilities.point_context_convert(@init_conners[0], @matrix , IDENTITY)
                conr2 = Utilities.point_context_convert(@corners[ 0 ], @matrix , IDENTITY) 
                translate = Geom::Transformation.new conr1.vector_to(conr2)
                return {
                    :distance => distance,
                    :frame_width => frame_width,
                    :frame_height => frame_height,
                    :frame_depth => frame_depth,
                    :translate => translate,
                    :distance_factor => distance_factor
                }
            end
            def resizeY( view, moving_point, pickFront, adjust_distance = nil)       
                org = Geom::Point3d.new ;
                org.transform!( @matrix )
                y_point = Geom::Point3d.new(0, 1, 0);
                y_point.transform!( @matrix );
                direction = org.vector_to(y_point);
                pick_point = @selected_point[:point]
                lock_point = pick_point.transform(Geom::Transformation.new direction)
                direction_line = [pick_point, direction]
                projected = moving_point.project_to_line(direction_line)
                vect = pick_point.vector_to( projected )
                distance = pick_point.distance(projected)
                # view.lock_inference(
                #   Sketchup::InputPoint.new( pick_point),
                #   Sketchup::InputPoint.new(lock_point)
                #   )
                if adjust_distance
                    distance = adjust_distance
                end
                tr = Geom::Transformation.new (Utilities.vectorMultiply(vect.normalize(), distance))
                trans =  @matrix.inverse * tr * @matrix 
                distance_factor = 1
                distance_factor = -1 if direction.dot(vect) < 0
                if( pickFront == true )  
                     @corners[ 2 ] = @init_conners[2].transform( trans)
                    @corners[ 3 ] = @init_conners[3].transform( trans)
                    @corners[ 6 ] = @init_conners[6].transform( trans)
                    @corners[ 7 ] = @init_conners[7].transform( trans)
                else   
                    @corners[ 0 ] = @init_conners[0].transform( trans)
                    @corners[ 1 ] = @init_conners[1].transform( trans)
                    @corners[ 4 ] = @init_conners[4].transform( trans)
                    @corners[ 5 ] = @init_conners[5].transform( trans)  
                end
                frame_width = @corners[ 0 ].distance(@corners[ 1 ])
                frame_height = @corners[ 0 ].distance(@corners[ 4 ])
                frame_depth = @corners[ 0 ].distance(@corners[ 3 ])
                # translate = Geom::Transformation.new @init_conners[0].vector_to(@corners[ 0 ])
                conr1 = Utilities.point_context_convert(@init_conners[0], @matrix , IDENTITY)
                conr2 = Utilities.point_context_convert(@corners[ 0 ], @matrix , IDENTITY) 
                translate = Geom::Transformation.new conr1.vector_to(conr2)
                return {
                    :distance => distance,
                    :frame_width => frame_width,
                    :frame_height => frame_height,
                    :frame_depth => frame_depth,
                    :translate => translate,
                    :distance_factor => distance_factor
                }
            end
            def resizeZ(view, moving_point, pickTop, adjust_distance = nil)      
                org = Geom::Point3d.new ;
                org.transform!( @matrix )
                z_point = Geom::Point3d.new(0, 0, 1);
                z_point.transform!( @matrix );
                direction = org.vector_to(z_point);
                pick_point = @selected_point[:point]
                lock_point = pick_point.transform(Geom::Transformation.new direction)
                direction_line = [pick_point, direction]
                projected = moving_point.project_to_line(direction_line)
                vect = pick_point.vector_to( projected )
                distance = pick_point.distance(projected)
                # view.lock_inference(
                #   Sketchup::InputPoint.new( pick_point),
                #   Sketchup::InputPoint.new(lock_point)
                #   )
                if adjust_distance
                    distance = adjust_distance
                end
                tr = Geom::Transformation.new (Utilities.vectorMultiply(vect.normalize(), distance))
                trans =  @matrix.inverse * tr * @matrix 
                distance_factor = 1
                distance_factor = -1 if direction.dot(vect) < 0
                if( pickTop == true )   
                    @corners[ 4 ] = @init_conners[4].transform( trans)
                    @corners[ 5 ] = @init_conners[5].transform( trans)
                    @corners[ 6 ] = @init_conners[6].transform( trans)
                    @corners[ 7 ] = @init_conners[7].transform( trans) 
                else 
                    @corners[ 0 ] = @init_conners[0].transform( trans)
                    @corners[ 1 ] = @init_conners[1].transform( trans)
                    @corners[ 2 ] = @init_conners[2].transform( trans)
                    @corners[ 3 ] = @init_conners[3].transform( trans)   
                end
                frame_width = @corners[ 0 ].distance(@corners[ 1 ])
                frame_height = @corners[ 0 ].distance(@corners[ 4 ])
                frame_depth = @corners[ 0 ].distance(@corners[ 3 ])
                 conr1 = Utilities.point_context_convert(@init_conners[0], @matrix , IDENTITY)
                conr2 = Utilities.point_context_convert(@corners[ 0 ], @matrix , IDENTITY) 
                translate = Geom::Transformation.new conr1.vector_to(conr2)
                return {
                    :distance => distance,
                    :frame_width => frame_width,
                    :frame_height => frame_height,
                    :frame_depth => frame_depth,
                    :translate => translate,
                    :distance_factor => distance_factor
                }
            end
            def pick(point)
                @pick_points << point.clone()
            end
            def clear_pick 
                @pick_points = []
            end
            def rotate(view, moving_point, lock_axis, enter_angle = nil)
                return if @pick_points.size == 0
                @lock_axis = lock_axis
                @isRotateTool = true
                org = Geom::Point3d.new ;
                org.transform!( @matrix )
                x_point = Geom::Point3d.new(1, 0, 0);
                x_point.transform!( @matrix );
                y_point = Geom::Point3d.new(0, 1, 0);
                y_point.transform!( @matrix );
                z_point = Geom::Point3d.new(0, 0, 1);
                z_point.transform!( @matrix );
                @lock_plane = [@pick_points.first, org.vector_to(x_point).normalize!]
                trucX = org.vector_to(x_point).normalize!
                trucY = org.vector_to(y_point).normalize!
                trucZ = org.vector_to(z_point).normalize!
                ext_len = 2000.mm
                normal_size = 1
                selected_size = 2
                x_size = normal_size
                y_size = normal_size
                z_size = normal_size
                @rot_axes =  trucX
                if @lock_axis == LOCK_X
                    x_size = selected_size 
                     @lock_color = 'red'
                     @lock_plane = [@pick_points.first, trucX]
                     @rot_axes =  trucX
                end
                if @lock_axis == LOCK_Y
                    y_size = selected_size 
                    @lock_color = 'green' 
                    @lock_plane = [@pick_points.first,trucY ]
                    @rot_axes =  trucY
                end
                if @lock_axis == LOCK_Z
                    z_size = selected_size 
                     @lock_color = 'blue' 
                     @lock_plane = [@pick_points.first, trucZ]
                    @rot_axes =  trucZ
                end
                pick_point = @pick_points.first
                @rotate_moving_point = moving_point.project_to_plane(@lock_plane)
                @x_axes = {
                    :p1 => @pick_points.first, 
                    :p2 => @pick_points.first.transform(Geom::Transformation.translation( Utilities.vectorMultiply(trucX, ext_len))),
                    :size => x_size
                }
                @y_axes = {
                    :p1 => @pick_points.first, 
                    :p2 => @pick_points.first.transform(Geom::Transformation.translation( Utilities.vectorMultiply(trucY, ext_len))),
                    :size => y_size
                }
                @z_axes = {
                    :p1 => @pick_points.first, 
                    :p2 => @pick_points.first.transform(Geom::Transformation.translation( Utilities.vectorMultiply(trucZ, ext_len))),
                    :size => z_size
                }
                if @pick_points.size == 2
                    p1 = @pick_points.first.project_to_plane(@lock_plane)
                    p2 = @pick_points.last.project_to_plane(@lock_plane)
                    vector1 = p1.vector_to(p2)
                    vector2 = p1.vector_to(@rotate_moving_point)
                    angle = vector1.angle_between vector2  
                    if !enter_angle.nil?
                        angle = enter_angle*Math::PI/ 180
                    end
                    factor = (Utilities.ccw_order?(p2, @rotate_moving_point, p1, @rot_axes)) ? 1 : -1 ;
                    trans = Geom::Transformation.rotation(@pick_points.first, @rot_axes, factor*angle)
                    trans =  @matrix.inverse * trans * @matrix
                    @corners = @init_conners.map{
                        |p| 
                        p = p.transform(trans)
                    }
                    return {:trans => trans}
                end
                nil
            end
            def draw(view)               
                view.line_width = 3
                normal_size = 5
                selected_size = 15
if @the_check_point
    view.draw_points( @the_check_point, 30, 1, 'red')
end
if @the_check_line
    view.drawing_color = 'red'  
    view.draw_lines @the_check_line[:p1], @the_check_line[:p2]
end
                if @isRotateTool && @pick_points.size > 0 && @rotate_moving_point
                    view.draw_points(@pick_points.last, 10, 1, @lock_color)
                    view.draw_points(@rotate_moving_point, 10, 1, @lock_color)
                    view.line_width = 1
                    view.line_stipple = "_"  
                    view.drawing_color = @lock_color  
                    view.draw_lines @rotate_moving_point, @pick_points.first
                    view.line_stipple = ""  
                    if @x_axes
                        view.line_width = @x_axes[:size]  
                        view.drawing_color = 'red'  
                        view.draw_lines @x_axes[:p1], @x_axes[:p2]
                    end
                    if @y_axes
                        view.line_width = @y_axes[:size]  
                        view.drawing_color = 'green'  
                        view.draw_lines @y_axes[:p1], @y_axes[:p2]
                    end
                    if @z_axes
                        view.line_width = @z_axes[:size]  
                        view.drawing_color = 'blue'  
                        view.draw_lines @z_axes[:p1], @z_axes[:p2]
                    end
                    if @pick_points.size == 2
                        view.draw_points( @pick_points.first, 50, 1, @lock_color)
                        view.line_width = 2
                        view.drawing_color = @lock_color  
                        view.line_stipple = "_" 
                        view.draw_lines @pick_points.first, @pick_points.last
                        view.line_stipple = "" 
                    end
                end
                corners = @corners.map{ |p| p.transform(@matrix)}
                if corners && corners.size > 0
                    for i in 0..corners.size - 1
                        pixel = normal_size
                        if i == 0 && @selected_point &&  @selected_point[:snap] == 'Conner0' ||
                            i == 1 && @selected_point &&  @selected_point[:snap] == 'Conner1' ||
                            i == 2 && @selected_point &&  @selected_point[:snap] == 'Conner2' ||
                            i == 3 && @selected_point &&  @selected_point[:snap] == 'Conner3' ||
                            i == 4 && @selected_point &&  @selected_point[:snap] == 'Conner4' ||
                            i == 5 && @selected_point &&  @selected_point[:snap] == 'Conner5' ||
                            i == 6 && @selected_point &&  @selected_point[:snap] == 'Conner6' ||
                            i == 7 && @selected_point &&  @selected_point[:snap] == 'Conner7'
                            pixel = selected_size
                        end
                        view.draw_points( corners[ i ], pixel, 1, 'yellow')
                    end
                    view.drawing_color = 'yellow'  
                    view.draw_lines corners[ 0 ], corners[ 1 ]
                    view.draw_lines corners[ 1 ], corners[ 2 ]
                    view.draw_lines corners[ 2 ], corners[ 3 ]
                    view.draw_lines corners[ 0 ], corners[ 3 ]
                    view.draw_lines corners[ 4 ], corners[ 5 ]
                    view.draw_lines corners[ 5 ], corners[ 6 ]
                    view.draw_lines corners[ 6 ], corners[ 7 ]
                    view.draw_lines corners[ 4 ], corners[ 7 ]
                    view.draw_lines corners[ 0 ], corners[ 4 ]                    
                    view.draw_lines corners[ 1 ], corners[ 5 ]
                    view.draw_lines corners[ 2 ], corners[ 6 ]                    
                    view.draw_lines corners[ 3 ], corners[ 7 ]
                end
                rightPoint = @rightPoint.transform(@matrix)
                leftPoint = @leftPoint.transform(@matrix)
                bottomPoint = @bottomPoint.transform(@matrix)
                topPoint = @topPoint.transform(@matrix)
                frontPoint = @frontPoint.transform(@matrix)
                backPoint = @backPoint.transform(@matrix)
                x_axis = leftPoint.vector_to(rightPoint).normalize!
                y_axis = frontPoint.vector_to(backPoint).normalize!
                z_axis = bottomPoint.vector_to(topPoint).normalize!
                ext_len = 0
                px1 = leftPoint.transform(Geom::Transformation.translation( Utilities.vectorMultiply(x_axis.reverse, ext_len)))
                px2 = rightPoint.transform(Geom::Transformation.translation( Utilities.vectorMultiply(x_axis, ext_len)))
                py1 = frontPoint.transform(Geom::Transformation.translation( Utilities.vectorMultiply(y_axis.reverse, ext_len)))
                py2 = backPoint.transform(Geom::Transformation.translation( Utilities.vectorMultiply(y_axis, ext_len)))
                pz1 = bottomPoint.transform(Geom::Transformation.translation( Utilities.vectorMultiply(z_axis.reverse, ext_len)))
                pz2 = topPoint.transform(Geom::Transformation.translation( Utilities.vectorMultiply(z_axis, ext_len)))
                if @selected_point && @selected_point[:snap] == 'rightPoint'
                    view.draw_points( rightPoint, selected_size, 1, Sketchup::Color.new(255,0,255))
                else
                    view.draw_points( rightPoint, normal_size, 1, 'red')
                end
                if @selected_point && @selected_point[:snap] == 'leftPoint'
                    view.draw_points( leftPoint, selected_size, 1, Sketchup::Color.new(255,0,255))
                else
                    view.draw_points( leftPoint, normal_size, 1, 'red')
                end
                if @selected_point && @selected_point[:snap] == 'bottomPoint'
                    view.draw_points( bottomPoint, selected_size, 1, Sketchup::Color.new(255,0,255))
                else
                    view.draw_points( bottomPoint, normal_size, 1, 'blue')
                end
                if @selected_point && @selected_point[:snap] == 'topPoint'
                    view.draw_points( topPoint, selected_size, 1, Sketchup::Color.new(255,0,255))
                else
                    view.draw_points( topPoint, normal_size, 1, 'blue')
                end
                if @selected_point && @selected_point[:snap] == 'frontPoint'
                    view.draw_points( frontPoint, selected_size, 1, Sketchup::Color.new(255,0,255))
                else
                    view.draw_points( frontPoint, normal_size, 1, 'green')
                end
                if @selected_point && @selected_point[:snap] == 'backPoint'
                    view.draw_points( backPoint, selected_size, 1, Sketchup::Color.new(255,0,255))
                else
                    view.draw_points( backPoint, normal_size, 1, 'green')
                end
                view.line_width = 1  
                view.drawing_color = 'red'  
                view.draw_lines px1, px2
                view.drawing_color = 'green'  
                view.draw_lines py1, py2
                view.drawing_color = 'blue'  
                view.draw_lines pz1, pz2
            end            
        end
        class PointHelper
            attr_accessor   :center, :boundingBox
            def initialize( center, name, id, dimension = 10.mm )
                @dimension = dimension
                @center = center
                @conners = []
                @conners[0] = Geom::Point3d.new center.x - dimension, center.y + dimension, center.z - dimension
                @conners[1] = Geom::Point3d.new center.x + dimension, center.y + dimension, center.z - dimension
                @conners[2] = Geom::Point3d.new center.x + dimension, center.y - dimension, center.z - dimension
                @conners[3] = Geom::Point3d.new center.x - dimension, center.y - dimension, center.z - dimension
                @conners[4] = Geom::Point3d.new center.x - dimension, center.y + dimension, center.z + dimension
                @conners[5] = Geom::Point3d.new center.x + dimension, center.y + dimension, center.z + dimension
                @conners[6] = Geom::Point3d.new center.x + dimension, center.y - dimension, center.z + dimension
                @conners[7] = Geom::Point3d.new center.x - dimension, center.y - dimension, center.z + dimension
                @name = name
                @id = id
                self.draw_bounds
            end
            def transform!(trans)
                @boundingBox.transform!(trans)
            end
            def erase
               @boundingBox.erase!  
            end
            def draw_bounds
                @boundingBox = Sketchup.active_model.entities.add_group()
                @boundingBox.name = 'PointHelper'
                @boundingBox.set_attribute 'amtf_dict', 'comp_type', 'amtf_PointHelper'               
                @boundingBox.set_attribute('amtf_dict','amtf_PointHelper_name', @name)
                @boundingBox.set_attribute('amtf_dict','amtf_PointHelper_id', @id)
                face_bottom = @boundingBox.entities.add_face( [@conners[0], @conners[1],@conners[2], @conners[3]] )
                face_bottom.material = 'yellow'; 
                face_top = @boundingBox.entities.add_face( [@conners[7], @conners[6],@conners[5], @conners[4]] )
                face_top.material = 'yellow'; 
                face_left = @boundingBox.entities.add_face( [@conners[7], @conners[4],@conners[0], @conners[3]] )
                face_left.material = 'yellow'; 
                face_right = @boundingBox.entities.add_face( [@conners[1], @conners[5],@conners[6], @conners[2]] )
                face_right.material = 'yellow'; 
                face_front = @boundingBox.entities.add_face( [@conners[3], @conners[2],@conners[6], @conners[7]] )
                face_front.material = 'yellow'; 
                face_back = @boundingBox.entities.add_face( [@conners[0], @conners[4],@conners[5], @conners[1]] )
                face_back.material = 'yellow'; 
                model = Sketchup.active_model
                layers = model.layers
                new_layer = layers.add "Amtf Temp"
                @boundingBox.layer = new_layer         
                @boundingBox.locked = true       
            end 
        end
    end
end