# encoding: UTF-8
require 'json'
module AMTF
	module BUKONG
		module AmtfDC
			def self.build_component(comp_data)
				definition =
					Sketchup.active_model.definitions.add(comp_data[:name])
				definition.entities.erase_entities(definition.entities.to_a) # erase any pre-existing entities
				tree = comp_data[:tree]
				component =
					Sketchup.active_model.entities.add_instance(
						definition,
						IDENTITY
					)
				dynamic_attributes = comp_data[:dynamic_attributes]
				if dynamic_attributes && dynamic_attributes.size > 0
					dynamic_attributes.each do |k, v|
						component.set_attribute('dynamic_attributes', k, v)
					end
				end
				traveseBuild(tree, component)
				return component
			end
			def self.traveseBuild(entities, component)
				faces = entities['faces']
				faces = entities[:faces] if faces.nil?
				faces.each do |face|
					outer_points = face['outer_points']
					outer_points = face[:outer_points] if outer_points.nil?
					if outer_points && outer_points.size > 0
						points = []
						outer_points.each do |p_str|
							point = p_str #.split(',').map { |s| s.to_i }
							points << point
						end
						if points.size > 0
							Utilities
								.definition(component)
								.entities
								.add_face(points)
						end
					end
				end
				groups = entities['groups']
				groups = entities[:groups] if groups.nil?
				if groups.size > 0
					groups.each do |grp|
						group =
							Utilities.definition(component).entities.add_group
						name = grp['name']
						name = grp[:name] if name.nil?
						group.name = name if !name.nil?
						ents = grp['entities']
						ents = grp[:entities] if ents.nil?
						tr = grp['transform']
						tr = grp[:transform] if tr.nil?
						if !tr.nil?
							transform = Geom::Transformation.new tr
							group.transform!(transform)
						end
						dynamic_attributes = grp['dynamic_attributes']
						dynamic_attributes =
							grp[:dynamic_attributes] if dynamic_attributes.nil?
						if dynamic_attributes && dynamic_attributes.size > 0
							dynamic_attributes.each do |k, v|
								group.set_attribute('dynamic_attributes', k, v)
							end
						end
						traveseBuild(ents, group) if ents && ents.size > 0
					end
				end
				component_instances = entities['component_instances']
				component_instances =
					entities[:component_instances] if component_instances.nil?
				if component_instances && component_instances.size > 0
					component_instances.each do |comp|
						tr = comp['transform']
						tr = comp[:transform] if tr.nil?
						transform = Geom::Transformation.new tr if !tr.nil?
						def_name = comp['def_name']
						def_name = comp[:def_name] if def_name.nil?
						if def_name
							definition =
								Sketchup.active_model.definitions.add(def_name)
							inst =
								Utilities
									.definition(component)
									.entities
									.add_instance(definition, transform)
							name = comp['name'] if name.nil?
							name = comp[:name] if name.nil?
							inst.name = name if !name.nil?
							dynamic_attributes = comp['dynamic_attributes']
							dynamic_attributes =
								comp[:dynamic_attributes] if dynamic_attributes
								.nil?
							if dynamic_attributes && dynamic_attributes.size > 0
								dynamic_attributes.each do |k, v|
									inst.set_attribute(
										'dynamic_attributes',
										k,
										v
									)
								end
							end
							ents = comp['entities']
							ents = comp[:entities] if ents.nil?
							traveseBuild(ents, inst) if ents && ents.size > 0
						end
					end
				end
			end
			def self.read_dc
				model = Sketchup.active_model
				# Get the selected components
				components = model.selection.grep(Sketchup::ComponentInstance)
				return if components.size == 0
				# Assumes that sang is the 1st entity in model.
				sang = components[0]
				tree = entities_recursive(Utilities.definition(sang).entities)
				def_name = Utilities.definition(sang).name
				dynamic_attributes =
					get_dynamic_attributes(
						Utilities.definition(sang),
						'dynamic_attributes'
					)
				puts "tree=#{tree.to_json}"
				comp =
					build_component(
						{
							name: def_name,
							tree: tree,
							dynamic_attributes: dynamic_attributes
						}
					)
				# sang_def = sang.definition
				# dict_name = 'dynamic_attributes'
				# get_json_from_dict(sang, dict_name)
				# dcs = $dc_observers.get_latest_class
				# dcs.redraw_with_undo(sang)
			end
			def self.get_points(lup)
				points = []
				lup.vertices.each { |v| points << v.position }
				return points
			end
			def self.inner_loop(face)
				oloop_0 = face.outer_loop
				loop_0 = face.loops
				inloop_0 = loop_0.select { |lp| !lp.outer? }
				points = []
				inloop_0.each { |lup| points << get_points(lup) }
				return points
			end
			def self.entities_recursive(entities)
				result = { faces: [], groups: [], component_instances: [] }
				entities.each do |entity|
					if entity.is_a? Sketchup::Face
						result[:faces].push(
							{
								outer_points: get_points(entity.outer_loop),
								inner_points: inner_loop(entity)
							}
						)
					elsif entity.is_a? Sketchup::Group
						result[:groups].push(
							{
								name: entity.name,
								entities: entities_recursive(entity.entities),
								dynamic_attributes:
									get_dynamic_attributes(
										Utilities.definition(entity),
										'dynamic_attributes'
									),
								transform: entity.transformation.to_a
							}
						)
					elsif entity.is_a? Sketchup::ComponentInstance
						result[:component_instances].push(
							{
								name: entity.name,
								def_name: Utilities.definition(entity).name,
								entities:
									entities_recursive(
										entity.definition.entities
									),
								dynamic_attributes:
									get_dynamic_attributes(
										Utilities.definition(entity),
										'dynamic_attributes'
									),
								transform: entity.transformation.to_a
							}
						)
					end
				end
				return result
			end
			def self.get_dynamic_attributes(obj, dict_name)
				d = obj.attribute_dictionary(dict_name, false)
				return false if d.nil?
				h = {} # create a hash
				d.each_pair { |k, v| h[k] = v }
				return h
			end
		end
	end
end