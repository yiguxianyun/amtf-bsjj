# encoding: UTF-8
module AMTF
	module BUKONG
		class PathTree
			attr_accessor :nodes, :parent, :object
			def initialize(object, parent = nil)
				@nodes = Hash.new
				@parent = parent
				@object = object
			end
			def add_node(object)
				node = PathTree.new(object, self)
				@nodes[node] = node
				return node
			end
		end
		class AmtfQuery
			attr_accessor :list, :path_node
			def initialize
				@list = []
			end
			def query_section(frame)
				list = []
				traverseSectionFace(frame, list)
				return list
			end
			def query_section_grp(frame)
				list = []
				traverseSectionFaceGrp(frame, list)
				return list
			end
			def query_frame(entities)
				list = []
				traverseFrame(entities, list)
				return list
			end
			def query_corner(entities)
				list = []
				traverseCorner(entities, list)
				return list
			end

			def query_sheets(entities, ignore_hidden = true)
				@list = []
				@path_node = PathTree.new(entities)
				traverse(entities, @list, @path_node, ignore_hidden)
				return @list
			end

			# def 查找单块板(entities)
			# 	@list = []
			# 	@path_node = PathTree.new(entities)
			# 	traverse(entities, @list, @path_node, ignore_hidden)
			# 	return @list
			# end

			def query_sheet_hash(entities, ignore_hidden = true)
				list = {}
				traverse_hash(entities, list, ignore_hidden)
				return list
			end
			def query_get_part(entities, part_type, no_hidden = false)
				list = []
				traverseGetPart(entities, list, part_type, no_hidden)
				return list
			end
			def minifixList(entities)
				query_sheets(entities)
				mini_list = []
				@list.each do |sheet|
					cams =
						Utilities
							.definition(sheet.comp)
							.entities
							.grep(Sketchup::Group)
					cams.each do |inst|
						inst.make_unique
						is_modal =
							inst.get_attribute('amtf_dict', 'minifix_modal')
						lockdowel_modal =
							inst.get_attribute('amtf_dict', 'lockdowel_modal')
						dogbone_modal =
							inst.get_attribute('amtf_dict', 'dogbone_modal')
						cabineo_cap =
							inst.get_attribute('amtf_dict', 'cabineo_cap')
						if is_modal == true || dogbone_modal || cabineo_cap
							tr = sheet.trans * inst.transformation
							mini_list << {
								modal_tr: tr,
								hole:
									Utilities.point_context_convert(
										Utilities.find_centroid(inst),
										tr,
										sheet.trans
									),
								instance: inst,
								type: inst.get_attribute('amtf_dict', 'type'),
								config_data:
									inst.get_attribute(
										'amtf_dict',
										'config_data'
									),
								connector_type_id:
									inst.get_attribute(
										'amtf_dict',
										'connector_type_id'
									),
								minifixID:
									inst.get_attribute(
										'amtf_dict',
										'minifixID'
									),
								parent: sheet.comp,
								trans: sheet.trans,
								grand_parent_matrix: sheet.parent_tr
							}
						end
						if lockdowel_modal == true
							center_faces =
								Utilities
									.definition(inst)
									.entities
									.grep(Sketchup::Face)
							if center_faces.size > 0
								tr = sheet.trans * inst.transformation
								conn = {
									modal_tr: tr,
									instance: inst,
									config_data:
										inst.get_attribute(
											'amtf_dict',
											'config_data'
										),
									minifixID:
										inst.get_attribute(
											'amtf_dict',
											'minifixID'
										),
									connector_type_id:
										inst.get_attribute(
											'amtf_dict',
											'connector_type_id'
										),
									type: 'lockdowel',
									trans: sheet.trans,
									grand_parent_matrix: sheet.parent_tr
								}
								center_faces.each do |cp|
									type = cp.get_attribute('amtf_dict', 'type')
									case type
									when 'point_1'
										conn[:p1] =
											Utilities.point_context_convert(
												Utilities.find_center_face(cp),
												tr,
												sheet.trans
											)
										conn[:hole] =
											Utilities.point_context_convert(
												Utilities.find_center_face(cp),
												tr,
												sheet.trans
											)
									when 'point_2'
										conn[:p2] =
											Utilities.point_context_convert(
												Utilities.find_center_face(cp),
												tr,
												sheet.trans
											)
									when 'point_3'
										conn[:p3] =
											Utilities.point_context_convert(
												Utilities.find_center_face(cp),
												tr,
												sheet.trans
											)
									end
									mini_list << conn
								end
							end
						end
					end
				end
				return mini_list
			end
			
			def traverse(
				entity,
				list,
				tree_node,
				ignore_hidden,
				transformation = IDENTITY
			)
				if (entity.is_a?(Sketchup::Selection) ||entity.is_a?(Sketchup::Entities))
					entity.each do |child_entity|
						node = tree_node.add_node(child_entity)
						traverse(
							child_entity,
							list,
							node,
							ignore_hidden,
							transformation
						)
					end
				elsif Utilities.instance?(entity)
					node = tree_node.add_node(entity)
					instance_tr = transformation * entity.transformation
					if Utilities.solid?(entity)
						if ignore_hidden
							if Utilities.solid?(entity) && !entity.hidden?
								sheetpart =
									AmtfSheet.new entity,
									              instance_tr,
									              transformation,
									              node
								list << sheetpart
							end
						else
							sheetpart =
								AmtfSheet.new entity,
								              instance_tr,
								              transformation,
								              node
							list << sheetpart
						end
					else
						Utilities
							.definition(entity)
							.entities
							.each do |child_entity|
								next unless Utilities.instance?(child_entity)
								child_node = tree_node.add_node(node)
								traverse(
									child_entity,
									list,
									child_node,
									ignore_hidden,
									instance_tr
								)
							end
					end
				end
			end

			def traverse_hash(
				entity,
				list,
				ignore_hidden,
				transformation = IDENTITY
			)
				if (
						entity.is_a?(Sketchup::Selection) ||
							entity.is_a?(Sketchup::Entities)
				   )
					entity.each do |child_entity|
						traverse_hash(
							child_entity,
							list,
							ignore_hidden,
							transformation
						)
					end
				elsif Utilities.instance?(entity)
					instance_tr = transformation * entity.transformation
					if Utilities.solid?(entity)
						if ignore_hidden
							if Utilities.solid?(entity) && !entity.hidden?
								sheetpart =
									AmtfSheet.new entity,
									              instance_tr,
									              transformation
								sheet_id = sheetpart.sheet_id
								list[sheet_id] = sheetpart if sheet_id
							end
						else
							sheetpart =
								AmtfSheet.new entity,
								              instance_tr,
								              transformation
							sheet_id = sheetpart.sheet_id
							list[sheet_id] = sheetpart if sheet_id
						end
					else
						Utilities
							.definition(entity)
							.entities
							.each do |child_entity|
								next unless Utilities.instance?(child_entity)
								traverse_hash(
									child_entity,
									list,
									ignore_hidden,
									instance_tr
								)
							end
					end
				end
			end
			def traverseGetPart(
				entity,
				list,
				part_type,
				no_hidden,
				transformation = IDENTITY
			)
				if (
						entity.is_a?(Sketchup::Selection) ||
							entity.is_a?(Sketchup::Entities)
				   )
					entity.each do |child_entity|
						traverseGetPart(
							child_entity,
							list,
							part_type,
							no_hidden,
							transformation
						)
					end
				elsif Utilities.instance?(entity)
					instance_tr = transformation * entity.transformation
					if Utilities.solid?(entity)
						if no_hidden == true && entity.hidden?
							# DO nothing
						else
							sheet_type =
								entity.get_attribute('amtf_dict', 'sheet_type')
							if part_type.include?(sheet_type)
								sheetpart =
									AmtfSheet.new entity,
									              instance_tr,
									              transformation
								list << sheetpart
							end
						end
					else
						Utilities
							.definition(entity)
							.entities
							.each do |child_entity|
								next unless Utilities.instance?(child_entity)
								traverseGetPart(
									child_entity,
									list,
									part_type,
									no_hidden,
									instance_tr
								)
							end
					end
				end
			end
			def traverseFrame(entity, list)
				if (
						entity.is_a?(Sketchup::Selection) ||
							entity.is_a?(Sketchup::Entities)
				   )
					entity.each do |child_entity|
						traverseFrame(child_entity, list)
					end
				elsif Utilities.instance?(entity)
					comp_type = entity.get_attribute 'amtf_dict', 'comp_type'
					list << entity if comp_type == 'AmtfFrame'
					Utilities
						.definition(entity)
						.entities
						.each do |child_entity|
							traverseFrame(child_entity, list)
						end
				end
			end
			def traverseSectionFace(entity, list)
				if Utilities.instance?(entity)
					Utilities
						.definition(entity)
						.entities
						.each do |child_entity|
							traverseSectionFace(child_entity, list)
						end
				elsif entity.is_a?(Sketchup::Face)
					section_id = entity.get_attribute('amtf_dict', 'section_id')
					list << entity if section_id
				end
			end
			def traverseSectionFaceGrp(entity, list)
				if Utilities.instance?(entity)
					Utilities
						.definition(entity)
						.entities
						.each do |child_entity|
							traverseSectionFaceGrp(child_entity, list)
						end
				elsif entity.is_a?(Sketchup::Face)
					section_id =
						entity.get_attribute(
							'is_section_face',
							'is_section_face'
						)
					list << entity if section_id
				end
			end
			def traverseCorner(entity, list)
				if Utilities.instance?(entity)
					Utilities
						.definition(entity)
						.entities
						.each do |child_entity|
							traverseCorner(child_entity, list)
						end
				elsif entity.is_a?(Sketchup::ConstructionPoint)
					corner = entity.get_attribute('amtf_dict', 'corner')
					list << entity if corner
				end
			end
		end
	end
end