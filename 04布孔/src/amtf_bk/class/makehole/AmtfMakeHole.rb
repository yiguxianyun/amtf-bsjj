# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfMakeHole
			attr_accessor :frame
			def initialize(attrs, view = nil, dialog = nil)
				@frame = attrs[:frame]
				@view = view
				@attrs = attrs
				@e1 = attrs[:last_pick][:pick_component]
				@e2 = attrs[:pick_data][:pick_component]
				@pick_face_1 = attrs[:last_pick][:pick_face]
				@pick_face_2 = attrs[:pick_data][:pick_face]
				@e1.make_unique
				@e2.make_unique
				@tr1 = attrs[:last_pick][:trans]
				@tr2 = attrs[:pick_data][:trans]
				@parent_tr1 = attrs[:last_pick][:parent_matrix]
				@parent_tr2 = attrs[:pick_data][:parent_matrix]
				@point_e1 = attrs[:last_pick][:position]
				@point_e2 = attrs[:pick_data][:position]
				@auto = view.nil?
				@helper = nil
				@dialog = dialog
				@for_frame = false
				@e1_id = @e1.get_attribute('amtf_dict', 'sheet_id')
				@e2_id = @e2.get_attribute('amtf_dict', 'sheet_id')
				@sheet1 = AmtfSheet.new(@e1, @tr1, @parent_tr1)
				@sheet2 = AmtfSheet.new(@e2, @tr2, @parent_tr2)
			end

			# def set_config(data,类型)
			def set_config(data)
				# @connector_type = data['类型']
				@connector_type = data['类型']
				# @config = data['config']
				# @connector_id = data['connector_id']
				# @connector_number = data['connector_number']
				@connector_array = data['阵列']
				@config_data=data
				if @auto
					@is_create_groove = data['create_groove']
					@groove_id = data['groove_id']
					@no_drawer_hole = data['no_drawer_hole']
					@all_edge_banding = data['all_edge_banding']
					@auto_connector = data['auto_connector']
					@auto_edge_banding = data['auto_edge_banding']
					@connector_type = data['conn_auto_type']
					@connector_id = data['conn_auto_id']
					@hole_at_corner = data['hole_at_corner']
					@no_connector_thick =data['nesting_config']['no_connector_thick'].to_s.to_l
					@no_edge_thick =data['nesting_config']['no_edge_thick'].to_s.to_l
					@no_connector_dim =data['nesting_config']['no_connector_dim'].to_s.to_l
				end
				@min_groove_depht = 0
				if @config && @config.key?('general_setting') &&
						@config['general_setting'][0].key?('params2') &&
						@config['general_setting'][0]['params2']
					min_depht =
						@config['general_setting'][0]['params2']
							.select { |obj| obj['key'] == 'groove_min_depth' }
					@min_groove_depht =
						min_depht[0]['value'].to_s.to_l if min_depht.size == 1
				end
				# @config_data =
				# 	Utilities.get_setting(
				# 		@connector_type,
				# 		@config,
				# 		@connector_id
				# 	)
				# if data['distance_from_edge'] &&
				# 		data['distance_from_edge'].to_s.to_l > 0
				# 	case @connector_type
				# 	when 'connector'
				# 		@config_data['distance_l1'] = data['distance_from_edge']
				# 	when 'chot_dot', 'khoan_moi', 'patV', 'rafix', 'lockdowel',
				# 	     'cabineo', 'dogbone'
				# 		@config_data['distance_l'] = data['distance_from_edge']
				# 	end
				# end
			end
			
			def set_config_frame_groove(save_config)
				@auto = false
				@for_frame = true
				@groove_save_config = save_config
			end
			def set_config_frame_multi_post(multi_post, connector_type)
				@auto = false
				@for_frame = true
				@connector_array = multi_post['connector_array']
				@connector_number = multi_post['connector_number']
				@line_direction = multi_post['line_direction']
				@config_data = multi_post['config_data']
				@mapping = multi_post['mapping']
				@connector_type = connector_type
			end
			def set_config_frame_single_post(single_post, connector_type)
				@auto = false
				@for_frame = true
				@connector_array = false
				@connector_type = connector_type
				@config_data = single_post['config_data']
				@minifix_list = single_post['minifix_list']
			end
			def draw
				if !@helper.nil?
					if @helper[:point]
						@view.draw_points(@helper[:point], 10, 1, 'red')
					end
					@view.drawing_color = 'yellow'
					@view.line_width = 5
					@view.draw_lines @helper[:point] if @helper[:point]
				end
				@minifix_function_obj.draw if @minifix_function_obj
				@cabineo_function_obj.draw if @cabineo_function_obj
				@dogbone_function_obj.draw if @dogbone_function_obj
				@chotdot_function_obj.draw if @chotdot_function_obj
				@khoan_moi_function_obj.draw if @khoan_moi_function_obj
				@patV_function_obj.draw if @patV_function_obj
				@rafix_function_obj.draw if @rafix_function_obj
				@camu_function_obj.draw if @camu_function_obj
			end
			def check_intersect
				# Method này để lấy ra các intersect, kiểm tra khi di chuyển connector
				# Kiểm tra tâm của mặt thuộc khối này có nằm trong khối kia không
				# nếu có, thì đó là trường hợp làm rãnh hậu
				if @sheet1.edge_center_within_other?(@sheet2) ||
						@sheet2.edge_center_within_other?(@sheet1)
					return false
				end
				@edge_and_face =
					AmtfEdgeFace.new(
						{
							sheet1: @sheet1,
							sheet2: @sheet2,
							pick_face_1: @pick_face_1,
							pick_face_2: @pick_face_2,
							frame: @frame,
							for_frame: @for_frame,
							auto: @auto,
							connector_type: @connector_type
						}
					)
				ges = @edge_and_face.do_intersect
				return false unless ges
				check_edge_face = @edge_and_face.check_edge_face
				return false unless check_edge_face
				return @edge_and_face.intersect_lines
			end
			def back_and_bottom
				groove_config = @config_data
				if @auto
					groove_config =
						Utilities.get_setting('groove', @config, @groove_id)
				end
				back =
					BackFunctions.new(
						{
							sheet1: @sheet1,
							sheet2: @sheet2,
							frame: @frame,
							auto: @auto,
							for_frame: @for_frame,
							groove_save_config: @groove_save_config,
							connector_id: @groove_id,
							config_data: groove_config,
							config: @config,
							min_groove_depht: @min_groove_depht
						}
					)
				back.execute
			end

			def make_hole
				if @sheet1.edge_center_within_other?(@sheet2) ||
						@sheet2.edge_center_within_other?(@sheet1)
					unless @auto || @for_frame
						xuly_error(
							nil,
							OB[:invalid_connection],
							'alert-warning'
						)
						return
					end
					if @auto == true && @is_create_groove == true
						self.back_and_bottom
					end
					return
				end
				@edge_and_face =AmtfEdgeFace.new(
						{
							sheet1: @sheet1,
							sheet2: @sheet2,
							pick_face_1: @pick_face_1,
							pick_face_2: @pick_face_2,
							frame: @frame,
							for_frame: @for_frame,
							auto: @auto,
							connector_type: @connector_type
						}
					)
				ges = @edge_and_face.do_intersect
				puts "ges👉#{ges}"
				unless ges
					if !@view.nil?
						xuly_error(
							nil,
							OB[:invalid_connection],
							'alert-warning'
						)
					end
					return
				end
				check_edge_face = @edge_and_face.check_edge_face
				unless check_edge_face
					if !@view.nil?
						xuly_error(
							nil,
							OB[:invalid_connection],
							'alert-warning'
						)
					end
					return nil
				end
				if @auto == true && @all_edge_banding == false
					if (@sheet1.sheet_type == 'HOZ_HALVED_JOINT' &&
								@sheet2.sheet_type == 'VER_HALVED_JOINT'
					   ) ||(
								@sheet1.sheet_type == 'VER_HALVED_JOINT' &&
									@sheet2.sheet_type == 'HOZ_HALVED_JOINT'
							)
						edgefaces_1 = @sheet1.edge_face
						edgefaces_2 = @sheet2.edge_face
						size1 = edgefaces_1.size - 1
						lats_index1 = "edge_face_#{size1.to_s}"
						size2 = edgefaces_2.size - 1
						lats_index2 = "edge_face_#{size2.to_s}"
						edgefaces_1.each do |f|
							face_index =
								f.get_attribute('amtf_dict', 'face_index')
							next if face_index == lats_index1
							if %w[
									edge_face_0
									edge_face_1
									edge_face_2
									edge_face_3
							   ].include?(face_index)
								next
							end
							f.set_attribute 'amtf_dict', 'edge_banding', 'false'
							f.delete_attribute 'amtf_dict', 'edge_banding'
							f.delete_attribute 'amtf_dict', 'edge_type_id'
							f.delete_attribute 'amtf_dict', 'edge_type_name'
							f.delete_attribute 'amtf_dict', 'edge_type_thick'
							f.material = @sheet1.comp.material
						end
						edgefaces_2.each do |f|
							face_index =
								f.get_attribute('amtf_dict', 'face_index')
							next if face_index == lats_index2
							if %w[
									edge_face_0
									edge_face_1
									edge_face_2
									edge_face_3
							   ].include?(face_index)
								next
							end
							f.set_attribute 'amtf_dict', 'edge_banding', 'false'
							f.delete_attribute 'amtf_dict', 'edge_banding'
							f.delete_attribute 'amtf_dict', 'edge_type_id'
							f.delete_attribute 'amtf_dict', 'edge_type_name'
							f.delete_attribute 'amtf_dict', 'edge_type_thick'
							f.material = @sheet2.comp.material
						end
					else
						@edge_and_face.bendingFace.each do |k, f|
							f[:face].set_attribute 'amtf_dict',
							                       'edge_banding',
							                       'false'
							f[:face].delete_attribute 'amtf_dict',
							                          'edge_banding'
							f[:face].delete_attribute 'amtf_dict',
							                          'edge_type_id'
							f[:face].delete_attribute 'amtf_dict',
							                          'edge_type_name'
							f[:face].delete_attribute 'amtf_dict',
							                          'edge_type_thick'
							f[:face].material = f[:sheet].comp.material
							if @frame
								sheet_id = f[:sheet].sheet_id
								face_index =
									f[:face].get_attribute(
										'amtf_dict',
										'face_index'
									)
								if sheet_id && face_index
									@frame.remove_edging(sheet_id, face_index)
								end
							end
						end
					end
				end
				if @auto && @connector_type == 'dogbone' &&
						@edge_and_face.on_boundary && @hole_at_corner == false
					return
				end
				return if @config_data.empty?
				return if !@auto_connector && @auto && !@for_frame
				e1_comp_type = @sheet1.comp_type
				e2_comp_type = @sheet2.comp_type
				if @auto
					if e1_comp_type == 'no_hole' || e1_comp_type == 'door' ||
							e2_comp_type == 'no_hole' || e2_comp_type == 'door'
						return
					end
					return unless @sheet1.sheet_thick > @no_connector_thick
					return unless @sheet2.sheet_thick > @no_connector_thick
					dimmension1 = @sheet1.dimension(@sheet1.big_faces[0])
					unless dimmension1[:width] > @no_connector_dim &&
							dimmension1[:height] > @no_connector_dim
						return
					end
					dimmension2 = @sheet2.dimension(@sheet2.big_faces[0])
					unless dimmension2[:width] > @no_connector_dim &&
							dimmension2[:height] > @no_connector_dim
						return
					end
					if @no_drawer_hole
						if %w[
								FRONT_DRAWER
								ENDL_DRAWER
								ENDR_DRAWER
								BACK_DRAWER
								BOTTOM_DRAWER
						   ].include?(@sheet1.sheet_type)
							return
						end
						if %w[
								FRONT_DRAWER
								ENDL_DRAWER
								ENDR_DRAWER
								BACK_DRAWER
								BOTTOM_DRAWER
						   ].include?(@sheet2.sheet_type)
							return
						end
					end
				end
				puts "@edge_and_face.intersect_lines#{@edge_and_face.intersect_lines}"
				# 找出已有的连接件
				if @edge_and_face.intersect_lines
					existingHoleFaceComp =@edge_and_face.faceSheet.grep_connector
					existingHoleFaceComp.concat(
						@edge_and_face.faceSheet.grep_dogbone[:female]
					)
					existingHoleFaceComp.concat(
						@edge_and_face.faceSheet.grep_dogbone[:male]
					)
					existingHoleFaceComp.concat(
						@edge_and_face.faceSheet.grep_cabineo_cap
					)
					existingHoleFaceComp.concat(
						@edge_and_face.faceSheet.grep_lockdowel_as_hole
					)
					existingHoleEdgeComp =
						@edge_and_face.edgeSheet.grep_connector
					existingHoleEdgeComp.concat(
						@edge_and_face.edgeSheet.grep_dogbone[:female]
					)
					existingHoleEdgeComp.concat(
						@edge_and_face.edgeSheet.grep_dogbone[:male]
					)
					existingHoleEdgeComp.concat(
						@edge_and_face.edgeSheet.grep_cabineo_cap
					)
					existingHoleEdgeComp.concat(
						@edge_and_face.edgeSheet.grep_lockdowel_as_hole
					)
					begin
						executing_data = {
							connector_type: @connector_type,
							edge_and_face: @edge_and_face,
							for_frame: @for_frame,
							dialog: @dialog,
							e1_id: @e1_id,
							e2_id: @e2_id,
							sheet1: @sheet1,
							sheet2: @sheet2,
							frame: @frame,
							point_e2: @point_e2,
							view: @view,
							connector_number: @connector_number,
							connector_array: @connector_array,
							config_data: @config_data,
							line_direction: @line_direction,
							mapping: @mapping,
							minifix_list: @minifix_list,
							existingHoleFaceComp: existingHoleFaceComp,
							existingHoleEdgeComp: existingHoleEdgeComp
						}
						puts "@connector_type👉#{@connector_type}"
						# if @connector_type == 'connector'
						if @connector_type == '三合一'
							@minifix_function_obj =MinifixFunctions.new(executing_data)
							@minifix_function_obj.execute
						elsif @connector_type == 'chot_dot'
							@chotdot_function_obj =
								ChotdotFunctions.new(executing_data)
							@chotdot_function_obj.execute
						elsif @connector_type == 'khoan_moi'
							@khoan_moi_function_obj =
								KhoanmoiFunctions.new(executing_data)
							@khoan_moi_function_obj.execute
						elsif @connector_type == 'patV'
							@patV_function_obj =
								PatVFunctions.new(executing_data)
							@patV_function_obj.execute
						elsif @connector_type == 'rafix'
							@rafix_function_obj =
								RafixFunctions.new(executing_data)
							@rafix_function_obj.execute
						elsif @connector_type == 'lockdowel'
							@camu_function_obj =
								LockdowelFunctions.new(executing_data)
							@camu_function_obj.execute
						elsif @connector_type == 'cabineo'
							@cabineo_function_obj =
								CabineoFunctions.new(executing_data)
							@cabineo_function_obj.execute
						elsif @connector_type == 'dogbone'
							@dogbone_function_obj =
								DogboneFunctions.new(executing_data)
							@dogbone_function_obj.execute
						else
							xuly_error(
								nil,
								OB[:invalid_connection],
								'alert-warning'
							)
						end
					rescue => error
						Utilities.print_exception(error, false)
						xuly_error(
							nil,
							OB[:invalid_connection],
							'alert-warning'
						)
					end
				end
				nil
			end

			def xuly_error(intersect_grp, error_message, error_class)
				return if @dialog.nil?
				if !intersect_grp.nil? && intersect_grp.valid?
					intersect_grp.erase!
				end
				js_command =
					"error_display('#{error_message}', '#{error_class}')"
				@dialog.execute_script(js_command)
			end
		end
	end
end