# encoding: UTF-8
module AMTF
	module BUKONG
		class AmtfEdgeFace
			attr_accessor :bendingFace,
			              :edgeSheet,
			              :edgeFace,
			              :bigFace,
			              :faceSheet,
			              :intersect_grp,
			              :intersect_lines,
			              :line_direction,
			              :on_boundary
			def initialize(attrs)
				@sheet1 = attrs[:sheet1]
				@sheet2 = attrs[:sheet2]
				@pick_face_1 = attrs[:pick_face_1]
				@pick_face_2 = attrs[:pick_face_2]
				@frame = attrs[:frame]
				@for_frame = attrs[:for_frame]
				@auto = attrs[:auto]
				@intersect_lines = []
				@intersect_paths = []
				@on_boundary = false
				@connector_type = attrs[:connector_type]
			end
			def erase_intersect
				@intersect_grp.erase! if @intersect_grp.valid?
			end
			def do_intersect
				@intersect_grp = @sheet1.intersection_with(@sheet2)
				ges = @intersect_grp.entities.grep(Sketchup::Edge)
				unless ges[0]
					@intersect_grp.erase! if @intersect_grp.valid?
					return false
				end
				multi_paths = TTLibrary.getMultiPathPointsHash(ges)
				multi_paths.map! do |pth|
					pth[:path].map! do |p|
						p[:point] =
							Utilities.point_context_convert(
								p[:point],
								@intersect_grp.transformation,
								IDENTITY
							)
					end
				end
				@points = multi_paths.flatten
				@paths = []
				multi_paths.each do |pth|
					get_path = MakeHoleHelper.get_long_paths(pth)
					@paths << get_path if get_path
				end
				self.erase_intersect
				return true
			end
			def check_edge_face
				edgeFace1 = @sheet1.edge_face
				bigFace1 = @sheet1.big_faces
				edgeFace2 = @sheet2.edge_face
				bigFace2 = @sheet2.big_faces
				@intersect_lines = []
				@intersect_paths = []
				select_edge_face = nil
				pick_edge_face = nil
				@on_boundary = false
				# check và loại b�?trường hợp mặt lớn tiếp xúc mặt lớn, mặt bé tiếp xúc mặt bé
				test_bigface1 =
					MakeHoleHelper.path_within_face_kind(
						bigFace1,
						@paths,
						@sheet1
					)
				test_bigface2 =
					MakeHoleHelper.path_within_face_kind(
						bigFace2,
						@paths,
						@sheet2
					)
				return if test_bigface1 && test_bigface2
				test_edgeFace1 =
					MakeHoleHelper.path_within_face_kind(
						edgeFace1,
						@paths,
						@sheet1
					)
				test_edgeFace2 =
					MakeHoleHelper.path_within_face_kind(
						edgeFace2,
						@paths,
						@sheet2
					)
				return if test_edgeFace1 && test_edgeFace2
				# Lấy các mặt lớn chứa c�?line1 và line2 là big_face
				test_big1 = MakeHoleHelper.select_big(bigFace1, @paths, @sheet1)
				if test_big1
					@edgeSheet = @sheet2
					@faceSheet = @sheet1
					@bigFace = test_big1[:face]
					@bendingFace =
						MakeHoleHelper.select_bending_faces(
							edgeFace2,
							@paths,
							@sheet2
						)
					select_edge_face = bigFace2
				else
					test_big2 =
						MakeHoleHelper.select_big(bigFace2, @paths, @sheet2)
					if test_big2
						@edgeSheet = @sheet1
						@faceSheet = @sheet2
						@bigFace = test_big2[:face]
						@bendingFace =
							MakeHoleHelper.select_bending_faces(
								edgeFace1,
								@paths,
								@sheet1
							)
						select_edge_face = bigFace1
					end
				end
				if !@bendingFace || !@edgeSheet || !@faceSheet || !@bigFace
					return
				end
				if @auto || @for_frame
					@paths.each do |path|
						test_line =
							MakeHoleHelper.select_line(
								path,
								@bigFace,
								@faceSheet,
								@connector_type
							)
						if test_line
							@intersect_lines << test_line[:path]
							@on_boundary = true if test_line[:on_boundary] ==
								true
						end
					end
					return if @intersect_lines.size == 0
					test_edge_face =
						select_edge_face.select do |f|
							@intersect_lines[0].all? do |p|
								Utilities.within_face?(
									Utilities.point_context_convert(
										p,
										IDENTITY,
										@edgeSheet.trans
									),
									f
								)
							end
						end
					return if test_edge_face.size == 0
					@edgeFace = test_edge_face[0]
					need_reverse = false
					@line_direction =
						@intersect_lines[0][0].vector_to(@intersect_lines[0][1])
							.normalize
					if @line_direction.dot(Y_AXIS) != 0
						need_reverse = true if @line_direction.dot(Y_AXIS) < 0
					elsif @line_direction.dot(X_AXIS) != 0
						need_reverse = true if @line_direction.dot(X_AXIS) < 0
					elsif @line_direction.dot(Z_AXIS) != 0
						need_reverse = true if @line_direction.dot(Z_AXIS) < 0
					end
					if need_reverse == true
						@intersect_lines.each { |l| l.reverse! }
						@line_direction.reverse!
					end
				else
					pick_face = nil
					edgeSheet = nil
					if select_edge_face.include?(@pick_face_1)
						pick_face = @pick_face_1
						edgeSheet = @sheet1
					else
						select_edge_face.include?(@pick_face_2)
						pick_face = @pick_face_2
						edgeSheet = @sheet2
					end
					return if pick_face.nil? || edgeSheet.nil?
					@edgeFace = pick_face
					@paths.each do |path|
						test_line =
							MakeHoleHelper.select_line_manual(
								path,
								pick_face,
								edgeSheet
							)
						@intersect_lines << test_line if test_line
					end
					@line_direction =
						@intersect_lines[0][0].vector_to(@intersect_lines[0][1])
							.normalize
				end
				return true
			end
		end
	end
end