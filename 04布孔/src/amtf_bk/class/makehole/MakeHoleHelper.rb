# encoding: UTF-8
module AMTF
	module BUKONG
		module MakeHoleHelper
			DO_CAO_CAM = 1400.mm
			def self.face_contain_points(iter_line, edgeCompTr, face)
				iter_line.all? do |p|
					Utilities.within_face?(
						Utilities.point_context_convert(
							p,
							IDENTITY,
							edgeCompTr
						),
						face
					)
				end
			end
			def self.get_long_paths(points)
				# Tìm ra 2 điểm đầu cạnh dài
				# Đầu tiên: xác định 2 cạnh đối dài nhất
				lines = []
				lines[0] = [points[0], points[1]]
				# �?đây các điểm đã được xắp xếp ccw
				for i in 2..points.size - 1
					line = lines.last
					if !points[i].on_line?(
							[line[0], line[0].vector_to(line[1]).normalize]
					   )
						lines.push([points[i - 1], points[i]])
					else
						if AmtfMath.between?(line[0], points[i], line[1])
							lines.last[1] = points[i]
						end
					end
				end
				lines = lines.sort_by { |line| line[0].distance(line[1]) }
				return(
					{
						line1: lines[lines.size - 1],
						line2: lines[lines.size - 2]
					}
				)
			end
			def self.path_within_face(path, face, tr)
				test1 =
					path[:line1].all? do |p|
						Utilities.within_face?(
							Utilities.point_context_convert(p, IDENTITY, tr),
							face
						)
					end
				test2 =
					path[:line2].all? do |p|
						Utilities.within_face?(
							Utilities.point_context_convert(p, IDENTITY, tr),
							face
						)
					end
				return test1 && test2
			end
			def self.path_within_face_kind(faces, paths, sheet)
				return false unless faces.size >= 2
				result = false
				faces.each do |face|
					test =
						paths.all? do |pth|
							test =
								MakeHoleHelper.path_within_face(
									pth,
									face,
									sheet.trans
								)
						end
					if test
						result = true
						break
					end
				end
				return result
			end
			def self.select_big(big_faces, paths, sheet)
				return false unless big_faces.size == 2
				result = false
				big_faces.each do |face|
					test = false
					paths.each do |pth|
						test =
							MakeHoleHelper.path_within_face(
								pth,
								face,
								sheet.trans
							)
					end
					if test
						result = { face: face, sheet: sheet }
						break
					end
				end
				return result
			end
			def self.is_bending_edge?(path, edge_face, sheet)
				test1 =
					path[:line1].all? do |p|
						Utilities.within_face?(
							Utilities.point_context_convert(
								p,
								IDENTITY,
								sheet.trans
							),
							edge_face
						)
					end
				return true if test1
				return false
			end
			def self.select_line_manual(path, pick_face, edgeSheet)
				mid1 = Utilities.get_midpoint(path[:line1][0], path[:line1][1])
				mid2 = Utilities.get_midpoint(path[:line2][0], path[:line2][1])
				test1 =
					Utilities.within_face?(
						Utilities.point_context_convert(
							mid1,
							IDENTITY,
							edgeSheet.trans
						),
						pick_face
					)
				return path[:line1] if test1
				test2 =
					Utilities.within_face?(
						Utilities.point_context_convert(
							mid2,
							IDENTITY,
							edgeSheet.trans
						),
						pick_face
					)
				return path[:line2] if test2
				return false
			end
			def self.select_line(path, big_face, sheet, connector_type)
				# t�?mặt lớn, nếu mid on_boudary thì lấy cạnh kia
				mid1 = Utilities.get_midpoint(path[:line1][0], path[:line1][1])
				mid2 = Utilities.get_midpoint(path[:line2][0], path[:line2][1])
				test1 =
					Utilities.on_boundary?(
						Utilities.point_context_convert(
							mid1,
							IDENTITY,
							sheet.trans
						),
						big_face
					)
				return { path: path[:line2], on_boundary: true } if test1
				test2 =
					Utilities.on_boundary?(
						Utilities.point_context_convert(
							mid2,
							IDENTITY,
							sheet.trans
						),
						big_face
					)
				return { path: path[:line1], on_boundary: true } if test2
				# Chiếu đầu mút của path[:line1][0] lên line2, lấy vector đ�?xét lấy cạnh nào
				line = [
					path[:line2][0],
					path[:line2][0].vector_to(path[:line2][1])
				]
				projected_point = path[:line1][0].project_to_line(line)
				test_direction = path[:line1][0].vector_to(projected_point)
				plane = [
					Geom::Point3d.new(0, 0, 0),
					Geom::Vector3d.new(0, 0, 1)
				]
				projected_point2 = path[:line1][0].project_to_plane(plane)
				panel_height = projected_point2.distance(path[:line1][0])
				is_line2 = false
				if test_direction.dot(Z_AXIS) != 0
					if connector_type == 'chot_dot'  #outside hole  层板
						if test_direction.dot(Z_AXIS) < 0
							is_line2 = true
						else
							is_line2 = false
						end
					elsif panel_height < DO_CAO_CAM
						is_line2 = true if test_direction.dot(Z_AXIS) < 0
					else
						is_line2 = true if test_direction.dot(Z_AXIS) > 0
					end
				elsif test_direction.dot(X_AXIS) != 0
					is_line2 = true if test_direction.dot(X_AXIS) > 0
				elsif test_direction.dot(Y_AXIS) != 0
					is_line2 = true if test_direction.dot(Y_AXIS) > 0
				end
				if is_line2 == true
					return { path: path[:line2], on_boundary: false }
				else
					return { path: path[:line1], on_boundary: false }
				end
				return false
			end
			def self.select_bending_faces(edge_faces, paths, sheet)
				bending_edge = {}
				edge_faces.each do |face|
					paths.each do |pth|
						if is_bending_edge?(pth, face, sheet)
							bending_edge[face] = { face: face, sheet: sheet }
							break
						end
					end
				end
				return bending_edge
			end
		end
	end
end