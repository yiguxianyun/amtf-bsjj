# encoding: UTF-8
require 'sketchup.rb'
require 'extensions.rb'
module AMTF
	module BUKONG
		# 先加载基层
			files=%w(
				AMTF_bk_mixin
				amtf布孔
			)
		  	files.each { |f|	
				Sketchup.require(File.join(PLUGIN_DIR, f)) 
			}

		# 再加载全部
			# pattern=File.join(PLUGIN_DIR, '**/*.{rb,rbe}')
			# puts "require  pattern:#{pattern}"
			# Dir.glob(pattern).each { |file|
			# # puts "require file: "+file
			# Sketchup.require file
			# }

		# #region  各种require
			Sketchup.require(File.join(PLUGIN_DIR, 'lib/bridge'))
			Sketchup.require(File.join(PLUGIN_DIR, 'ui/server'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AMTFDialog'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfSaver'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfFindID'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/store'))
			Sketchup.require(File.join(PLUGIN_DIR, 'languages/ordbok'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfSheetHelper'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/sheet/ArrowModel'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/sheet/AmtfSheet'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfSpatial'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfQuery'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/makehole/MakeHoleHelper'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/makehole/AmtfEdgeFace'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/makehole/AmtfMakeHole'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/hinge/AmtfHingeHelper'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/hinge/AmtfHinge'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/DrawerDrill'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfDC'))
			Sketchup.require(File.join(PLUGIN_DIR, 'languages/pluralization_rules'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/toolObserver'))
			Sketchup.require(File.join(PLUGIN_DIR, 'menu'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/fitingUtils'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/circleModel'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/arcModel'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/BackFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/connectorFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/minifixFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/cabineoFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/dogboneFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/chotdotFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/khoanmoiFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/patVFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/rafixFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/lockdowelFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/Lockdowel'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/Dogbone'))
			Sketchup.require(File.join(PLUGIN_DIR, 'connectors/Cabineo'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/tools'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/rename_sheet'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/sheet_hole'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/view_connector'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/sheet_edit'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/division_change'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/customHole'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/customGrooveTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/movingTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/removeTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/connectorTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/offsetTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/curveGroove'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/drawingTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/doorTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/removeCurve'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/prohitbitionHole'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/sheet_options'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/reverseTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/removePath'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/dimensionTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/selectTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/edit_frame'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/AmtfCopyTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/curveSheetTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/createComponent'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/h_split'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/v_split'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/sheet_adjust'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/frame_adjust'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/remove_sheet'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/customPath'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/sheet_resize'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/sheet_move'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/remove_sub'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/sheet_rotate'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/nameTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'tools/findTool'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfBox'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/frames/FrMinifixHelper'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/frames/FrGrooveHelper'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/frames/FrEdgingHelper'))
			Sketchup.require(
				File.join(PLUGIN_DIR, 'class/frames/FrAttributeHelper')
			)
			Sketchup.require(File.join(PLUGIN_DIR, 'class/frames/AmtfFrame'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/paths/PathHelpers'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/paths/AmtfPaths'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfFace/AmtfSide'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfFace/insertDrilling'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfFace/FaceHelper'))
			Sketchup.require(File.join(PLUGIN_DIR, 'class/AmtfFace/amtfloop'))
			Sketchup.require(File.join(PLUGIN_DIR, 'lib/progressbar'))
			Sketchup.require(File.join(PLUGIN_DIR, 'lib/TTLibrary'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/common'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/AmtfMath'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/UtilsFunctions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/nesting'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/cleaning'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/draw'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/config'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/worker_function'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/hole_functions'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/offset'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/drawing_export'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/explode'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/BuKongFunction'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/layout_drawing'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/ray'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/solid_operations'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/draw_sheet'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/giveName'))
			Sketchup.require(File.join(PLUGIN_DIR, 'ui/selectTool_opt'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/makeDC'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/update'))
			Sketchup.require(File.join(PLUGIN_DIR, 'functions/point_to_point'))
		# #end region
		# Reload extension.
		def self.reload_bk(clear_console = true, undo = false)
			# Hide warnings for already defined constants.
			verbose = $VERBOSE
			$VERBOSE = nil
			# Use a timer to make call to method itself register to console.
			# Otherwise the user cannot use up arrow to repeat command.
			# UI.start_timer(0) { SKETCHUP_CONSOLE.clear } if clear_console
			# Sketchup.undo if undo
			puts "load file: #{File.join(PLUGIN_DIR, '**/*.{rb,rbe}')}"
			Dir.glob(File.join(PLUGIN_DIR, '**/*.{rb,rbe}')).each { |file| 
				# puts "load file: "+file
				load(file) 
			}
			puts "~"*108
			ensure
			$VERBOSE = verbose
	
			nil
		end
	end # module AmtfCabinetDesign
end # module AMTF