module AMTF
	module BUKONG
		module TTLibrary
			# Sorts the given set of edges from start to end. If the edges form a loop
			# an arbitrary start is picked.
			#对给定的边集进行从开始到结束的排序。如果边构成一个循环，将选择任意一个作为起点。
			# @todo Comment source
			#
			# @param [Array<Sketchup::Edge>] edges
			#
			# @return [Array<Sketchup::Edge>] Sorted set of edges.
			# @since 2.5.0
			def self.sort(edges)
				if edges.is_a?(Hash)
					return self.sort_from_hash(edges)
				elsif edges.is_a?(Enumerable)
					lookup = {}
					for edge in edges
						lookup[edge] = edge
					end
					rslt = self.sort_from_hash(lookup)
					return rslt
				else
					raise ArgumentError,
					      '"edges" argument must be a collection of edges.'
				end
			end
			# Sorts the given set of edges from start to end. If the edges form a loop
			# an arbitrary start is picked.
			#
			# @param [Hash] edges Sketchup::Edge as keys
			#
			# @return [Array<Sketchup::Edge>] Sorted set of edges.
			# @since 2.5.0
			def self.sort_from_hash(edges)
				# Get starting edge - then trace the connected edges from either end.
				start_edge = edges.keys.first
				# Find the next left and right edge
				vertices = start_edge.vertices
				left = []
				for e in vertices.first.edges
					left << e if e != start_edge && edges[e]
				end
				right = []
				for e in vertices.last.edges
					right << e if e != start_edge && edges[e]
				end
				return nil if left.size > 1 || right.size > 1 # Check for forks
				left = left.first
				right = right.first
				# Sort edges from start to end
				sorted = [start_edge]
				# Right
				edge = right
				until edge.nil?
					sorted << edge
					connected = []
					for v in edge.vertices
						for e in v.edges
							connected << e if edges[e] && !sorted.include?(e)
						end
					end
					# if connected.size > 1
					# 	puts '==='
					# 	puts connected
					# 	Sketchup.active_model.rendering_options[
					# 		'EdgeColorMode'
					# 	] =
					# 		0
					# 	e.material = 'red'
					# 	puts e
					# end
					return nil if connected.size > 1 # Check for forks
					edge = connected.first
				end
				# Left
				unless sorted.include?(left)
					# Fix: 2.6.0
					edge = left
					until edge.nil?
						sorted.unshift(edge)
						connected = []
						for v in edge.vertices
							for e in v.edges
								if edges[e] && !sorted.include?(e)
									connected << e
								end
							end
						end
						return nil if connected.size > 1 # Check for forks
						edge = connected.first
					end
				end
				sorted
			end
			def self.findPaths(edges)
				# Get starting edge - then trace the connected edges from either end.
				if edges.is_a?(Hash)
					self.sort_from_hash(edges)
				elsif edges.is_a?(Enumerable)
					lookup = {}
					for edge in edges
						lookup[edge] = edge
					end
					self.path_from_hash(lookup)
				else
					raise ArgumentError,
					      '"edges" argument must be a collection of edges.'
				end
			end
			def self.path_from_hash(edges)
				# Get starting edge - then trace the connected edges from either end.
				visited = []
				not_visited = edges.keys
				paths = []
				while not_visited.size > 0
					start_edge = not_visited.pop
					next if visited.include? start_edge
					visited << start_edge if !visited.include? start_edge
					# Find the next left and right edge
					vertices = start_edge.vertices
					left = []
					for e in vertices.first.edges
						if e != start_edge && edges[e] && !visited.include?(e)
							left << e
						end
					end
					right = []
					for e in vertices.last.edges
						if e != start_edge && edges[e] && !visited.include?(e)
							right << e
						end
					end
					left = findNextEdge(left, start_edge)
					right = findNextEdge(right, start_edge)
					# Sort edges from start to end
					sorted = [start_edge]
					# Right
					edge = right
					until edge.nil?
						if !visited.include?(edge)
							sorted << edge
							visited << edge
						end
						connected = []
						for v in edge.vertices
							for e in v.edges
								if edges[e] && !sorted.include?(e) &&
										!visited.include?(e)
									connected << e
								end
							end
						end
						edge = findNextEdge(connected, edge)
					end
					# Left
					unless sorted.include?(left) && !visited.include?(left)
						edge = left
						until edge.nil?
							if !visited.include?(edge)
								sorted.unshift(edge)
								visited << edge
							end
							connected = []
							for v in edge.vertices
								for e in v.edges
									if edges[e] && !sorted.include?(e) &&
											!visited.include?(e)
										connected << e
									end
								end
							end
							edge = findNextEdge(connected, edge)
						end
					end
					paths << { path: sorted }
				end
				paths
			end
			def self.findNextEdge(edges, start_edge)
				return unless edges.size > 0
				return edges.first if edges.size == 1
				#Tìm đường nào thẳng  hơn thì đi
				angleArr = []
				for i in 0..edges.size - 1
					# Vertex chung
					vertex_chung = nil
					second_othervertex = nil
					for v in edges[i].vertices
						if start_edge.vertices.include? v
							vertex_chung = v
							second_othervertex = edges[i].other_vertex v
						end
					end
					first_othervertex = start_edge.other_vertex vertex_chung
					vector1 =
						vertex_chung.position.vector_to(
							first_othervertex.position
						)
					vector2 =
						vertex_chung.position.vector_to(
							second_othervertex.position
						)
					angleArr << {
						angle: vector1.angle_between(vector2),
						edge: edges[i]
					}
				end
				angleArr = angleArr.sort_by { |obj| obj[:angle] }
				return angleArr.last[:edge]
			end
			def self.getPathPoints(input_edges)
				return unless input_edges && input_edges.size > 0
				edges = sort(input_edges)
				return unless edges
				if edges.size == 1
					return edges[0].start.position, edges[0].end.position
				end
				first_edge = edges[0]
				firt_vertex = edges[0].vertices[0]
				firt_vertex = edges[0].vertices[1] if edges[1]
					.vertices.include? firt_vertex
				sorted = [firt_vertex.position]
				vertices = first_edge.vertices
				for i in 1..edges.size - 1
					if i == edges.size - 1
						if vertices.include? edges[i].vertices[0]
							sorted << edges[i].vertices[0].position
							sorted << edges[i].vertices[1].position
						elsif vertices.include? edges[i].vertices[1]
							sorted << edges[i].vertices[1].position
							sorted << edges[i].vertices[0].position
						end
					else
						for v in edges[i].vertices
							sorted << v.position if vertices.include? v
						end
						vertices = edges[i].vertices
					end
				end
				return sorted
			end
			# def self.getPathCurvePoints(edges)
			#     return unless edges && edges.size > 0
			#     edges = sort( edges )
			#     curve = false
			#     radius = nil;
			#     startAngle = nil
			#     endAngle = nil
			#     center = nil
			#     bulge = 0
			#     if edges.size == 1
			#         edg= edges[0]
			#         if !edg.curve.nil? && defined?(edg.curve.radius) == "method"
			#             curve = true
			#             radius = edg.curve.radius
			#             startAngle = edg.curve.start_angle
			#             endAngle = edg.curve.end_angle
			#             center = edg.curve.center
			#             bulge = Utilities.getBulge(edg.start.position, edg.end.position, center)
			#         end
			#         output = [
			#         {   :point => edg.start.position,
			#             :curve => curve,
			#             :radius => radius,
			#             :startAngle => startAngle,
			#             :endAngle => endAngle,
			#             :center => center,
			#             :bulge => bulge
			#         },
			#         {   :point => edg.end.position,
			#             :curve => curve,
			#             :radius => radius,
			#             :startAngle => startAngle,
			#             :endAngle => endAngle,
			#             :center => center,
			#             :bulge => bulge
			#         }]
			#         return output
			#     end
			#     return unless edges
			#     first_edge = edges[0]
			#     firt_vertex = edges[0].vertices[0]
			#     second_vertex = edges[0].vertices[1]
			#     if edges[1].vertices.include? firt_vertex
			#         firt_vertex = edges[0].vertices[1]
			#         second_vertex = edges[0].vertices[0]
			#     end
			#     if !edges[0].curve.nil? && defined?(edges[0].curve.radius) == "method"
			#         edg = edges[0]
			#         curve = true
			#         radius = edg.curve.radius
			#         startAngle = edg.curve.start_angle
			#         endAngle = edg.curve.end_angle
			#         center = edg.curve.center
			#         bulge = Utilities.getBulge(first_edge.position, second_vertex.position, center)
			#     end
			#     sorted = [
			#         {   :point => firt_vertex.position,
			#             :curve => curve,
			#             :radius => radius,
			#             :startAngle => startAngle,
			#             :endAngle => endAngle,
			#             :center => center,
			#             :bulge => bulge
			#         }]
			#     vertices = first_edge.vertices
			#     for i in 1..edges.size - 1
			#         curve = false
			#         radius = nil;
			#         startAngle = nil
			#         endAngle = nil
			#         center = nil
			#         bulge = 0
			#         if !edges[i].curve.nil? && defined?(edges[i].curve.radius) == "method"
			#             edg = edges[0]
			#             curve = true
			#             radius = edg.curve.radius
			#             startAngle = edg.curve.start_angle
			#             endAngle = edg.curve.end_angle
			#             center = edg.curve.center
			#             bulge = Utilities.getBulge(edges[i].vertices[0].position, edges[i].vertices[1].position, center)
			#         end
			#         if i== edges.size - 1
			#             if vertices.include? edges[i].vertices[0]
			#                 sorted << {
			#                     :point => edges[i].vertices[0].position,
			#                     :curve => curve,
			#                     :radius => radius,
			#                     :startAngle => startAngle,
			#                     :endAngle => endAngle,
			#                     :center => center,
			#                     :bulge => bulge
			#                 }
			#                 sorted << {
			#                     :point => edges[i].vertices[1].position,
			#                     :curve => curve,
			#                     :radius => radius,
			#                     :startAngle => startAngle,
			#                     :endAngle => endAngle,
			#                     :center => center,
			#                     :bulge => bulge
			#                 }
			#             elsif vertices.include? edges[i].vertices[1]
			#                 sorted << {
			#                     :point => edges[i].vertices[1].position,
			#                     :curve => curve,
			#                     :radius => radius,
			#                     :startAngle => startAngle,
			#                     :endAngle => endAngle,
			#                     :center => center,
			#                     :bulge => bulge
			#                 }
			#                 sorted << {
			#                     :point => edges[i].vertices[0].position,
			#                     :curve => curve,
			#                     :radius => radius,
			#                     :startAngle => startAngle,
			#                     :endAngle => endAngle,
			#                     :center => center,
			#                     :bulge => bulge
			#                 }
			#             end
			#         else
			#             for v in edges[i].vertices
			#                 if vertices.include? v
			#                     sorted << {
			#                         :point => v.position,
			#                         :curve => curve,
			#                         :radius => radius,
			#                         :startAngle => startAngle,
			#                         :endAngle => endAngle,
			#                         :center => center,
			#                         :bulge => bulge
			#                     }
			#                 end
			#             end
			#             vertices = edges[i].vertices
			#         end
			#     end
			#     return sorted
			# end
			def self.getMultiPathPoints(input_edges)
				return unless input_edges && input_edges.size > 0
				edges_hash = findPaths(input_edges)
				return unless edges_hash
				result = []
				for index in 0..edges_hash.size - 1
					edges = edges_hash[index][:path]
					first_edge = edges[0]
					firt_vertex = edges[0].vertices[0]
					firt_vertex = edges[0].vertices[1] if edges[1]
						.vertices.include? firt_vertex
					sorted = [firt_vertex.position]
					vertices = first_edge.vertices
					for i in 1..edges.size - 1
						if i == edges.size - 1
							if vertices.include? edges[i].vertices[0]
								sorted << edges[i].vertices[0].position
								sorted << edges[i].vertices[1].position
							elsif vertices.include? edges[i].vertices[1]
								sorted << edges[i].vertices[1].position
								sorted << edges[i].vertices[0].position
							end
						else
							for v in edges[i].vertices
								sorted << v.position if vertices.include? v
							end
							vertices = edges[i].vertices
						end
					end
					result << sorted
				end
				result
			end
			def self.getMultiPathPointsHash(input_edges)
				return unless input_edges && input_edges.size > 0
				if input_edges.size == 1
					sorted = []
					sorted << {
						point: input_edges[0].start.position,
						layer: input_edges[0].layer
					}
					sorted << {
						point: input_edges[0].end.position,
						layer: input_edges[0].layer
					}
					return [{ path: sorted, closed: false }]
				end
				edges_hash = findPaths(input_edges)
				edges_hash.compact!
				return unless edges_hash
				result = []
				for index in 0..edges_hash.size - 1
					edges = edges_hash[index][:path]
					next if edges.size == 0
					next if edges[0].nil?
					first_edge = edges[0]
					firt_vertex = edges[0].vertices[0]
					if edges[1].nil?
						result << {
							path: [
								{
									point: edges[0].start.position,
									layer: first_edge.layer
								},
								{
									point: edges[0].end.position,
									layer: first_edge.layer
								}
							],
							closed: false
						}
						next
					end
					firt_vertex = edges[0].vertices[1] if edges[1]
						.vertices.include? firt_vertex
					sorted = [
						{ point: firt_vertex.position, layer: first_edge.layer }
					]
					vertices = first_edge.vertices
					for i in 1..edges.size - 1
						if i == edges.size - 1
							if vertices.include? edges[i].vertices[0]
								sorted << {
									point: edges[i].vertices[0].position,
									layer: edges[i].layer
								}
								sorted << {
									point: edges[i].vertices[1].position,
									layer: edges[i].layer
								}
							elsif vertices.include? edges[i].vertices[1]
								sorted << {
									point: edges[i].vertices[1].position,
									layer: edges[i].layer
								}
								sorted << {
									point: edges[i].vertices[0].position,
									layer: edges[i].layer
								}
							end
						else
							for v in edges[i].vertices
								if vertices.include? v
									sorted << {
										point: v.position,
										layer: edges[i].layer
									}
								end
							end
							vertices = edges[i].vertices
						end
					end
					closed =
						Utilities.point_compare?(
							sorted.first[:point],
							sorted.last[:point]
						)
					result << { path: sorted, closed: closed }
				end
				result
			end
			def self.getMultiToolPath(input_edges)
				return unless input_edges && input_edges.size > 0
				if input_edges.size == 1
					sorted = []
					sorted << input_edges[0].start.position
					sorted << input_edges[0].end.position
					return [{ path: sorted, closed: false }]
				end
				edges_hash = findPaths(input_edges)
				edges_hash.compact!
				return unless edges_hash
				result = []
				for index in 0..edges_hash.size - 1
					edges = edges_hash[index][:path]
					next if edges.size == 0
					next if edges[0].nil? || edges[1].nil?
					first_edge = edges[0]
					firt_vertex = edges[0].vertices[0]
					firt_vertex = edges[0].vertices[1] if edges[1]
						.vertices.include? firt_vertex
					sorted = [firt_vertex.position]
					vertices = first_edge.vertices
					for i in 1..edges.size - 1
						if i == edges.size - 1
							if vertices.include? edges[i].vertices[0]
								sorted << edges[i].vertices[0].position
								sorted << edges[i].vertices[1].position
							elsif vertices.include? edges[i].vertices[1]
								sorted << edges[i].vertices[1].position
								sorted << edges[i].vertices[0].position
							end
						else
							for v in edges[i].vertices
								sorted << v.position if vertices.include? v
							end
							vertices = edges[i].vertices
						end
					end
					closed = Utilities.point_compare?(sorted.first, sorted.last)
					result << { path: sorted, closed: closed }
				end
				result
			end
			def self.get_cursor
				cursors = {
					default: 0,
					invalid: 663,
					hand: 671,
					hand_invalid: 918,
					link: 670,
					erase: 645,
					pencil: 632,
					freehand: 655,
					arc_1: 629,
					arc_2: 631,
					arc_3: 630,
					man: 612,
					position_camera: 653,
					position_camera_3d: 902,
					walk: 420,
					walk_3d: 904,
					look_around: 418,
					look_around_3d: 903,
					orbit: 419,
					orbit_3d: 900,
					pan: 1003,
					pan_2d: 901,
					zoom: 421,
					zoom_region: 422,
					zoom_3d: 905,
					zoom_2d: 907,
					zoom_2d_region: 906,
					offset: 646,
					offset_invalid: 679,
					dropper: 651,
					dropper_texture: 652,
					paint: 681, # 647
					paint_same: 650,
					paint_object: 649,
					paint_connected: 648,
					paint_invalid: 680,
					text: 678,
					follow_me: 640,
					follow_me_invalid: 678,
					pushpull: 639,
					pushpull_add: 755,
					pushpull_invalid: 707,
					tape: 638,
					tape_add: 731,
					select: 633,
					select_add: 634,
					select_remove: 636,
					select_toggle: 635,
					select_step_1: 924,
					select_step_2: 925,
					select_invalid: 926,
					rectangle: 637,
					move: 641,
					move_copy: 642,
					move_fold: 672,
					move_invalid: 673,
					position: 658,
					position_invalid: 673,
					scale: 736,
					scale_invalid: 730,
					scale_n_s: 659,
					scale_n_ne: 666,
					scale_ne: 661,
					scale_ne_e: 667,
					scale_w_e: 660,
					scale_n_nw: 665,
					scale_nw: 662,
					scale_nw_w: 664,
					rotate: 643,
					rotate_copy: 644,
					rotate_invalid: 713
				}
			end
		end
	end
end