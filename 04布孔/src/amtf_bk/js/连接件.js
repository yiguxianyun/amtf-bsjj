var 上一个按键 = "";
$(document).ready(function () {
    load_config();
    $("#测试").on("click", function () {
        保存设置();
    });
    $(".回车执行").change(function (e) {
        console.log(上一个按键);
        if (上一个按键 != "esc") {
            // this.blur();
            缩放移动对象(this);
        }
    });
});

window.onbeforeunload = function (ev) {
    // alert("准备退出了");
    保存设置();
    // alert("真退出了");
    // 连接件.选中类型 = $("input[name='类型单选组']:checked").val()
    // data.选中规格 = $("input[name='连接件规格单选组']:checked").val()
    // // sketchup.保存设置(data);
};
function 保存设置() {
    // sketchup.保存设置(当前连接件);

    if (!对象值是否相等(当前连接件, 连接件)) {
        console.log(当前连接件);
        sketchup.保存设置(当前连接件);
    }
    // let 设置有变 = false;
    // 当前选中类型 = $("input[name='类型单选组']:checked").val();
    // 选中类型 = 连接件.选中类型;
    // if (当前选中类型 != 选中类型) {
    //     设置有变 = true;
    //     连接件.选中类型 = 当前选中类型;
    // }

    // 候选类型 = 连接件.候选类型;
    // 候选类型.forEach(e => {
    //     类型 = e.类型;
    //     选中规格 = e.选中规格;
    //     当前选中规格 = $("input[name='" + 类型 + "--规格单选组']:checked").val();
    //     if (当前选中规格 != 选中规格) {
    //         设置有变 = true;
    //         e.选中规格 = 当前选中规格;
    //     }
    // });
    // if (设置有变) {
    //     console.log(连接件);
    //     // sketchup.保存设置(连接件);
    // }
}

// <!-- 调用ruby👇 -->
function load_config() {
    // alert("load_config")
    sketchup.load_config();
}

// <!-- 供ruby调用V2👇 -->
function 显示信息(params) {
    // alert(params);
    console.log(params);
    // const 连接件 = JSON.parse(params);
    window.连接件 = JSON.parse(params);
    window.当前连接件 = 深拷贝(连接件);
    // console.log("当前连接件==连接件?", 当前连接件 == 连接件);
    // console.log("当前连接件==连接件?", 对象值是否相等(当前连接件, 连接件));
    $("#类型").html("");
    候选类型 = 连接件.候选类型;
    // 候选规格 = 连接件.候选规格

    候选类型.forEach(e => {
        console.log(e);
        类型 = e.类型;
        let 新行html = $("#类型-模板行").html().format({ 类型 });
        let 新行 = $(新行html).appendTo("#类型");
        是否选中 = 类型 == 连接件.选中类型;
        if (是否选中) {
            let k = 新行.find("input");
            k.attr("checked", true);
        }

        候选规格 = e.候选规格;
        候选规格.forEach(e2 => {
            规格 = e2.规格;
            是否选中 = 规格 == e.选中规格;
            console.log(规格 == 连接件.选中规格);
            let 新行html = $("#规格-模板行").html().format({ 规格, 类型 });
            let 新行 = $(新行html).appendTo("#" + 类型 + "-规格");
            if (是否选中) {
                let k = 新行.find("input");
                k.attr("checked", true);
            }
        });
    });
    阵列 = 连接件.候选阵列;
    阵列.forEach(e => {
        总长 = e.总长;
        端距 = e.端距;
        数量 = e.数量;
        let 新行html = $("#阵列_模板行").html().format({ 总长, 端距, 数量 });
        let 新行 = $(新行html).appendTo("#阵列");
    });
    绑定事件();
}

function 绑定事件() {
    $("input[name='类型单选组']").change(function (e) {
        // console.log(e);
        console.log("当前连接件==连接件?", 对象值是否相等(当前连接件, 连接件));
        当前选中类型 = $("input[name='类型单选组']:checked").val();
        当前连接件.选中类型 = 当前选中类型;
        console.log("当前选中类型👉", 当前选中类型);
        console.log("当前连接件==连接件?", 对象值是否相等(当前连接件, 连接件));
        sketchup.修改设置(当前连接件);
        // alert('hi');
    });
    $("input[name*='--规格单选组']").change(function (e) {
        console.log(e.target.name.split("--")[0]);
        选中规格单选组全名 = e.target.name;
        选中规格的上级类型 = 选中规格单选组全名.split("--")[0];
        选中规格 = $("input[name=" + 选中规格的上级类型 + "--规格单选组]:checked").val();
        console.log(选中规格);
        当前连接件.选中规格的上级类型.选中规格 = 选中规格;
        // alert('hi');
        sketchup.修改设置(当前连接件);
    });
}
