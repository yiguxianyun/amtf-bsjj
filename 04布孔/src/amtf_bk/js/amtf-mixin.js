// 列出所有的键，接着遍历数组
function 对象值是否相等(object1, object2) {
    var o1keys = Object.keys(object1);
    var o2keys = Object.keys(object2);
    if (o2keys.length !== o1keys.length) return false;
    for (let i = 0; i <= o1keys.length - 1; i++) {
        let key = o1keys[i];
        if (!o2keys.includes(key)) return false;
        if (typeof object1[key] == "object" && typeof object2[key] == "object") {
            对象值是否相等(object1[key], object2[key]);
        } else {
            if (object2[key] !== object1[key]) return false;
        }
    }
    // console.log("相等");
    return true;
}
//函数深拷贝
const 深拷贝 = (obj = {}) => {
    //变量先置空
    let newobj = null;
    //判断是否需要继续进行递归
    if (typeof obj == "object" && obj !== null) {
        newobj = obj instanceof Array ? [] : {}; //进行下一层递归克隆
        for (var i in obj) {
            newobj[i] = 深拷贝(obj[i]);
        } //如果不是对象直接赋值
    } else newobj = obj;
    return newobj;
};

// //模拟对象
// let obj = {
//     numberParams: 1, functionParams: () => {
//         console.log('阿弥陀佛');
//     }, objParams: {
//         a: 1, b: 2
//     }
// }
// const newObj = 深拷贝(obj); //这样就完成了一个对象的递归拷贝obj.numberParams = 100;  //更改第一个对象的指console.log(newObj.numberParams); //输出依然是1 不会跟随obj去改变

String.prototype.format = function (args) {
    var result = this;
    if (arguments.length > 0) {
        if (arguments.length == 1 && typeof args == "object") {
            for (var k in args) {
                if (args[k] != undefined) {
                    var reg = new RegExp("({" + k + "})", "g");
                    result = result.replace(reg, args[k]);
                }
            }
        } else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                    //var reg = new RegExp("({[" + i + "]})", "g");//这个在索引大于9时会有问题
                    var reg = new RegExp("({)" + i + "(})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
};

function 转为数字(item) {
    item = parseFloat(item);
    if (isNaN(item)) {
        item = 0;
    }
    return item;
}

function 展示计算结果(e) {
    // value = exp_result(e.value);
    // console.log(value);
    // console.log($("#计算结果")[0].value);
    $("#计算结果")[0].value = exp_result(e.value);
}
function 同步表达式(e) {
    $("#算数表达式")[0].value = e.value;
    $($("#算数表达式")[0]).trigger("oninput");
}
