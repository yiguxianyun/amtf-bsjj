/*global SyntaxHighlighter*/
SyntaxHighlighter.config.tagName = 'code';

if (window.$) {
    $(document).ready(function () {
        var dt110 = $.fn.dataTable && $.fn.dataTable.Api ? true : false;

        // Work around for WebKit bug 55740
        var info = $('div.info');

        if (info.height() < 115) {
            info.css('min-height', '8em');
        }

        var escapeHtml = function (str) {
            return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        };

        // css
        var cssContainer = $('div.tabs div.css');
        if (cssContainer.find('code').text().trim() === '') {
            cssContainer.find('p').eq(0).css('display', 'none');
            cssContainer.find('code, div').css('display', 'none');
        }

        // init html
        var demoHtml = '';

        if ($('div.demo-html').length) {
            demoHtml = $('div.demo-html').html().trim();

            if (demoHtml) {
                demoHtml = demoHtml + '\n\n';
            }
        }

        $('div.tabs div.table').append('<code class="multiline language-html">\t\t\t\t' + escapeHtml(demoHtml) + '</code>');

        // This can really slow things down
        setTimeout(function () {
            SyntaxHighlighter.highlight({}, $('div.table code')[0]);
        }, 1000);

        // Allow the demo code to run if DT 1.9 is used
        if (dt110) {
            // json
            var ajaxTab = $('ul.tabs li').eq(3).css('display', 'none');

            $(document).on('init.dt', function (e, settings) {
                if (e.namespace !== 'dt') {
                    return;
                }

                var api = new $.fn.dataTable.Api(settings);

                var show = function (str) {
                    ajaxTab.css('display', 'block');
                    $('div.tabs div.ajax code').remove();
                    $('div.tabs div.ajax div.syntaxhighlighter').remove();

                    // Old IE :-|
                    try {
                        str = JSON.stringify(str, null, 2);
                    } catch (e) {}

                    var strArr = str.split('\n');

                    if (strArr.length > 1000) {
                        var first = strArr.splice(0, 500);
                        var second = strArr.splice(strArr.length - 499, 499);
                        first.push(
                            "\n\n... Truncated for brevity - look at your browser's network inspector to see the full source ...\n\n"
                        );
                        str = first.concat(second).join('\n');
                    }

                    $('div.tabs div.ajax').append($('<code class="multiline language-js"/>').text(str));

                    // This can be really slow for large builds
                    setTimeout(function () {
                        SyntaxHighlighter.highlight({}, $('div.tabs div.ajax code')[0]);
                    }, 500);
                };

                // First draw
                var json = api.ajax.json();
                if (json) {
                    show(json);
                }

                // Subsequent draws
                api.on('xhr.dt', function (e, settings, json) {
                    show(json);
                });
            });

            // php
            var phpTab = $('ul.tabs li').eq(4).css('display', 'none');

            $(document).on('init.dt.demoSSP', function (e, settings) {
                if (e.namespace !== 'dt') {
                    return;
                }

                if (settings.oFeatures.bServerSide) {
                    if (typeof settings.ajax === 'function') {
                        return;
                    }
                    $.ajax({
                        url: '../resources/examples.php',
                        data: {
                            src: settings.sAjaxSource || settings.ajax.url || settings.ajax
                        },
                        dataType: 'text',
                        type: 'post',
                        success: function (txt) {
                            phpTab.css('display', 'block');
                            $('div.tabs div.php').append('<code class="multiline language-php">' + txt + '</code>');
                            SyntaxHighlighter.highlight({}, $('div.tabs div.php code')[0]);
                        }
                    });
                }
            });
        } else {
            $('ul.tabs li').eq(3).css('display', 'none');
            $('ul.tabs li').eq(4).css('display', 'none');
        }

        // Tabs
        $('ul.tabs').on('click', 'li', function () {
            $('ul.tabs li.active').removeClass('active');
            $(this).addClass('active');

            $('div.tabs>div').css('display', 'none').eq($(this).index()).css('display', 'block');
        });
        $('ul.tabs li.active').trigger('click');
    });
}

// <!-- 调用ruby👇 -->
$(document).ready(function () {
    sketchup.读取设置();
    $('#穿透选择并隐藏').on('click', function () {
        sketchup.穿透选择并隐藏();
    });
    $('#穿透显示').on('click', function () {
        sketchup.穿透显示();
    });
    $('#布尔').on('click', function () {
        sketchup.布尔();
    });
    $('#躺平到原点导出dxf').on('click', function () {
        sketchup.躺平到原点导出dxf();
    });
    $('#开启amtf_exe').on('click', function () {
        amtf_exe路径 = $('#amtf_exe路径')[0].value;
        sketchup.开启amtf_exe(amtf_exe路径);
    });
});

window.onbeforeunload = function (ev) {
    // return true;
    // alert('即将关闭');
    amtf_exe路径 = $('#amtf_exe路径')[0].value;
    sketchup.保存设置({ amtf_exe路径 });
};

// <!-- 调用ruby👇 -->
function 不同板厚放不同标记层() {
    // alert("amtf");
    sketchup.不同板厚放不同标记层();
}
function 不同材质放不同标记层() {
    // alert("amtf");
    sketchup.不同材质放不同标记层();
}
function 返回json() {
    // sketchup.返回json();
    var table = $('#table_id').DataTable({
        ajax: '../mobanbiao.json'
    });
    table.ajax.reload(null, false);
    // alert("params");
}
function 导入model() {
    // var fileOpenDlg = new ActiveXObject("MSComDlg.CommonDialog");
    sketchup.导入model();
}
function 尺寸标注放到指定图层() {
    // arg = "你好，世界"
    // alert(arg)
    // window.location = 'skp:测试@' + arg
    sketchup.尺寸标注放到指定图层(指定图层.value);
    // 发送调试模式();
}

function 选择同材质组件() {
    // arg = document.getElementById("选择同材质组件").value
    // window.location = 'skp:选择同材质组件@' + arg
    sketchup.选择同材质组件();
    // alert(window.location)
}
function 移除组件材质() {
    // window.location = 'skp:移除组件and子级材质@' + arg
    sketchup.移除组件材质();
    // alert(window.location)
}
function 移除组件and子级材质() {
    // window.location = 'skp:移除组件and子级材质@' + arg
    sketchup.移除组件and子级材质();
    // alert(window.location)
}
function 更改背板厚度() {
    arg = document.getElementById('背板厚度').value;
    // window.location = 'skp:更改背板厚度@' + arg
    sketchup.更改背板厚度(arg);
    // alert(window.location)
}

function 清除未使用() {
    arg = '清除未使用';
    // window.location = 'skp:清除未使用@' + arg
    sketchup.清除未使用(arg);
}

function 对齐() {
    // window.location = 'skp:对齐@' + arg
    sketchup.对齐(arg);
}

function 多余组件处理() {
    // window.location = 'skp:多余组件处理@' + arg
    sketchup.多余组件处理(arg);
}

function 另存为排版模型() {
    // window.location = 'skp:另存为排版模型@' + arg
    sketchup.另存为排版模型(arg);

    // alert(window.location)
}

function 删除指定图层() {
    // window.location = 'skp:删除指定图层@' + arg
    sketchup.删除指定图层(arg);
}

function 删除隐藏项目() {
    window.location = 'skp:删除隐藏项目@' + arg;
}

function 删除or炸开无板字组件() {
    window.location = 'skp:删除or炸开无板字组件@' + arg;
}

function 炸开所有子组件() {
    // window.location = 'skp:炸开所有子组件@' + arg
    sketchup.炸开所有子组件();

    // alert(window.location)
}

function 组件改名() {
    // window.location = 'skp:组件改名@' + arg
    sketchup.组件改名(arg);
}

function 组件转群组() {
    // window.location = 'skp:组件转群组@' + arg
    sketchup.组件转群组(arg);
}

function 干涉检查() {
    // window.location = 'skp:干涉检查@' + arg
    sketchup.干涉检查();
}

function 延伸背板() {
    arg = document.getElementById('延伸值').value;
    // alert(arg)
    // window.location = 'skp:延伸背板@' + arg
    sketchup.延伸背板(arg);
}

function 文件改名() {
    // window.location = 'skp:文件改名@' + arg
    sketchup.文件改名(arg);
}

function 画柜子(arg) {
    // alert(arg)
    // window.location = 'skp:画柜子@' + arg
    sketchup.画柜子(arg);
}
// #region 供ruby调用👇
function 展示设置(params) {
    // alert(params);
    // const result = JSON.parse(params);
    $('#amtf_exe路径')[0].value = params['amtf_exe路径'];
}
// #endregion
