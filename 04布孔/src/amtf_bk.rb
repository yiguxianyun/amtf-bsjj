# encoding: UTF-8
require 'sketchup.rb'
require 'extensions.rb'
module AMTF
	module BUKONG
		path = __FILE__
		path.force_encoding('UTF-8') if path.respond_to?(:force_encoding)
		PLUGIN_DIR = File.join(File.dirname(path), File.basename(path, '.*'))
		# UI.messagebox "PLUGIN_DIR👉 #{PLUGIN_DIR}"
		REQUIRED_SU_VERSION = 14
		unless file_loaded?(__FILE__)
			EXTENSION =
				SketchupExtension.new(
					'布孔',
					File.join(PLUGIN_DIR, 'main')
				)
			EXTENSION.creator = '老鱼'
			EXTENSION.description = 'amtf'
			EXTENSION.version = '0.0.1'
			EXTENSION.copyright = "amtf, #{EXTENSION.creator}"
			Sketchup.register_extension(EXTENSION, true)
			file_loaded(__FILE__)
		end
	end # module bk
end # module AMTF
#
