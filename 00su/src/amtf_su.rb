require 'sketchup.rb'
require 'extensions.rb'
module AMTF
  def self.reload
    puts "reload：阿弥陀佛"

    original_verbose = $VERBOSE
    $VERBOSE = nil
    pattern = File.join(__dir__, 'amtf_su/buer/*.rb')
    puts "load file    pattern222:#{pattern}"
    Dir.glob(pattern).each { |file|
      # Cannot use `Sketchup.load` because its an alias for `Sketchup.require`.
      puts "load file: "+file
      load file
    }.size
    pattern = File.join(__dir__, 'amtf_su/*.rb')
    puts "load file   pattern111:#{pattern}"
    Dir.glob(pattern).each { |file|
      # Cannot use `Sketchup.load` because its an alias for `Sketchup.require`.
      puts "load file: "+file
      load file
    }.size
    puts "~"*108
    ensure
      $VERBOSE = original_verbose
  end



  unless file_loaded?(__FILE__)
    ex = SketchupExtension.new('amtf', 'amtf_su/amtf')
    ex.description = '……'
    ex.version     = '1.0.0'
    ex.copyright   = '阿弥陀佛'
    ex.creator     = '老鱼'
    Sketchup.register_extension(ex, true)
    file_loaded(__FILE__)
  end
end # module AMTF
