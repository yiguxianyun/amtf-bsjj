# require File.join(__FILE__, '../AMTF_mixin.rb')
# require File.join(__FILE__, '../A布尔select_tool_mixin.rb')
require 'sketchup.rb'
module AMTF
  class A布尔select_tool_基类
    include AMTF_mixin
    include AMTF::A布尔select_tool_mixin
    #overwritten for each boolean tool
    def initialize()
      @first = nil  #the first operand
      @second = nil #the second operand
      @state = 0
      @sel = Sketchup.active_model.selection
      icon = File.join(PATH, "Icons", "wait.png")
      @wait_cursor = UI.create_cursor(icon, 12, 12)
    end

    def perform_boolean(first, second, recurse)
      UI.set_cursor(@wait_cursor)
      box = get_bounds_of_instances([first, second])

      begin
        start_operation(@bool_operation_string)
        changed_instances = boolean_function(first, second, recurse).changed_instances
        # changed_instances.each{ |i|
        #   puts "i.name:#{i.definition.name}"
        #   puts "i.deleted?:#{i.deleted?}"
        #   puts "i.visible?:#{i.visible?}"
        #   # defin.set_attribute(MODIFIED_DICTIONARY, MODIFIED_DIC_KEY, true)
        # }
        #todo set attributes on the instances definition
        # set_bool_tools_modified_attribute(changed_instances)#不知道要干嘛，先取消
      rescue Exception => e
        p "布尔工具操作错误:"
        p e.message
        p e.backtrace
      ensure
        # 下面是干啥？先取消
        # lic_status = Licensing::License.new.get_state
        # if lic_status == Licensing::LICENSED
        # if 1 == 1
          puts "走commit_operation"
          Sketchup.active_model.commit_operation #前面start了，这里没commit，目录树表现为没有更新!!!!
        # else
        #   puts "走previewer"
        #   previewer = BTPreviewer.new
        #   previewer.set_bounds(box)
        #   changed_instances.each { |inst| previewer.add_preview_mesh(inst) }
        #   Sketchup.active_model.abort_operation
        #   Sketchup.active_model.select_tool(previewer)
        # end
      end
    end
    #overwritten for each boolean tool

    def boolean_function(first, second, recurse) end
    def activate
     reset
    end
    def onSetCursor
      if ((@state == 1) && @cursor2)
        UI.set_cursor(@cursor2)
      else
        UI.set_cursor(@cursor)
      end
    end
    def deactivate(view)
      @first = nil
      @second = nil
      @state = 0
      @sel.clear
      Sketchup.set_status_text
    end

    def onLButtonDown(flags, x, y, view)
      if (@state == 0)  #we are getting the first operand
        ph = view.pick_helper
        ph.do_pick(x, y)
        best = ph.best_picked
        if ((best.class == Sketchup::Group) || (best.class == Sketchup::ComponentInstance))
          @sel.add(best)
          @first = best
          @state = 1
          Sketchup.set_status_text("请选择第二个组或组件。")
        end
      elsif (@state == 1) #we are getting the second operand
        ph = view.pick_helper
        ph.do_pick(x, y)
        best = ph.best_picked
        if ((best.class == Sketchup::Group) || (best.class==Sketchup::ComponentInstance))
          # p best
          # p @first
          # p best == @first
          unless (best == @first)
            @sel.add(best)
            @second = best
            @state = 2
            result = validate_operands(@first, @second)
            recurse = true #todo change this as bool tool default for now
            perform_boolean(@first, @second, recurse) if (result)
            self.reset
          end
        end
      end
    end  #end onLbuttondown

    def onMouseMove(flags, x, y, view)
      ph = view.pick_helper
      ph.do_pick(x, y)
      best = ph.best_picked
      if ((best.class == Sketchup::Group) || (best.class == Sketchup::ComponentInstance))
        @sel.clear
        @sel.add(best)
        @sel.add(@first) if (@state == 1)
      else
        @sel.clear
        @sel.add(@first) if (@state == 1)
      end
    end

    def reset
      onSetCursor
      @first = nil  #the first operand
      @second = nil #the second operand
      @state = 0
      @sel = Sketchup.active_model.selection
      @sel.clear
      Sketchup.set_status_text("请先选择第一个组或组件。")
    end

  end  #end class
end

