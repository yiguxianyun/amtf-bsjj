require 'sketchup.rb'
require 'json'

module AMTF
  class A组件属性
    include AMTF_mixin
    @@xyzarr = ["x", "y", "z"]
    # @@不认识字符 = ["└--▶"]

    def initialize
      puts "A组件属性 初始化"
    end

    def 识别选中组件()
      设置常用对象
      动态属性h=Hash[]
      @选中组件=@selection[0]
      主组件动态属性h=遍历组件属性(@选中组件)
      动态属性h[:主组件]=主组件动态属性h
      # puts 1
      父组件=@选中组件.parent
      if 父组件.kind_of? Sketchup::Model
        父组件动态属性h=Hash["组件定义名"=>"顶层组件"]
        # 父组件动态属性h=["组件定义名"="顶层组件"]
      else
        父组件动态属性h=遍历组件属性(父组件.instances[0])
      end
      # puts 父组件.definition.name
      动态属性h[:父组件]=父组件动态属性h

      子组件arr=获取子组件(@选中组件)

      if 父组件.kind_of? Sketchup::Model
        最终父组件=父组件
      else
        最终父组件=父组件.instances[0]
      end
      兄弟组件arr=获取子组件(最终父组件)
      arr=子组件arr.concat(兄弟组件arr)
      arr属性=[]
      for e in arr do
        # puts e
        属性h=遍历组件属性(e)
        arr属性.push 属性h
      end
      动态属性h[:相关组件]=arr属性
      # puts arr属性.to_json
      puts 动态属性h.to_json
      return 动态属性h
    end

    def 修改组件属性(动态属性h)
      puts "修改组件属性"
      组件定义名=@选中组件.definition.name
      # puts "之前组件定义名 = #{组件定义名}"
      # puts 动态属性h["其他属性"]
      组件定义动态属性dic = @选中组件.definition.attribute_dictionaries["dynamic_attributes"]
      组件实例动态属性dic = @选中组件.attribute_dictionaries["dynamic_attributes"]

      # 旧属性公式值=组件定义动态属性dic["_x_formula"]
      # puts "旧属性公式值"
      # puts 旧属性公式值
      # UI.messagebox "#{动态属性h}"

      arr=[]
      动态属性h["其他属性"].each { | obj |
        # e_def.set_attribute 'dynamic_attributes', 'lenx', 59
        # puts obj
        # puts obj["key"]
        if @@xyzarr.include?(obj["key"])
          最终对象=@选中组件
          最终动态属性s=组件实例动态属性dic
        else
          最终对象=@选中组件.definition
          最终动态属性s=组件定义动态属性dic
        end

        formula="_#{obj["key"]}_formula"
        puts "formula"
        puts formula
        旧属性公式值=最终动态属性s[formula]
        puts "旧属性公式值"
        puts 旧属性公式值
        puts obj["key"]
        puts obj["formula"].to_s
        if obj["formula"]==nil and 旧属性公式值!=nil
          # obj["formula"]=""#没办法删除或者置空公式值么？
        end
        # puts obj["formula"]!=nil and 旧属性公式值!=obj["formula"]
        if obj["formula"]!=nil and 旧属性公式值!=obj["formula"]
        # if 旧属性公式值!=obj["formula"]
          最终对象.set_attribute 'dynamic_attributes', "_#{obj["key"]}_formula", obj["formula"]
          最终对象.set_attribute 'dynamic_attributes', "_#{obj["key"]}_formulaunits", "CENTIMETERS"
          formlabelkk=最终动态属性s["_#{obj["key"]}_formlabel"]
          if formlabelkk!=nil
            if !formlabelkk.include? '不认识！'
              最终对象.set_attribute 'dynamic_attributes', "_#{obj["key"]}_formlabel", obj["formlabel"]
              最终对象.set_attribute 'dynamic_attributes', "_#{obj["key"]}_access", "TEXTBOX"
              最终对象.set_attribute 'dynamic_attributes', "_#{obj["key"]}_units", "MILLIMETERS"
            end
          end
          新属性值=最终动态属性s[formula]
          if 新属性值!=obj["formula"]
            arr.push obj["key"]
          end
        end
      }
      dcs = $dc_observers.get_latest_class
      dcs.redraw_with_undo(@选中组件)
      修改组件属性结果=Hash[]
      修改组件属性结果[:修改组件属性结果]=arr
      return 修改组件属性结果
    end

    def 遍历组件属性(e)
      排除属性arr = ["description", "dialogheight", "dialogwidth",
        "imageurl","itemcode","name","scaletool","summary","onclick"
      ]
      xyzlarr = ["x", "y", "z",
        "lenx","leny","lenz"
      ]

      if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
        组件定义名=e.definition.name
        puts 组件定义名
        组件实例名=e.name
        # 是不是definition差别很大！
        # e=e.definition
        动态属性h=Hash[]
        组件定义动态属性dic=Hash[]
        组件定义动态属性dic["dynamic_attributes"]=Hash[]
        if e.definition.attribute_dictionaries != nil
          if e.definition.attribute_dictionaries["dynamic_attributes"]!= nil
            组件定义动态属性dic = e.definition.attribute_dictionaries["dynamic_attributes"]
          end
        end

        组件实例动态属性dic=Hash[]
        组件实例动态属性dic["dynamic_attributes"]=Hash[]
        if e.attribute_dictionaries != nil
          if e.attribute_dictionaries["dynamic_attributes"]!= nil
            组件实例动态属性dic = e.attribute_dictionaries["dynamic_attributes"]
          end
        end

        # 旧属性公式值=组件定义动态属性dic["_x_formula"]
        # puts "旧属性公式值"
        # puts 旧属性公式值

        动态属性h["组件定义名"]=组件定义名
        动态组件名=组件定义动态属性dic["_name"]
        动态属性h["动态组件名"]=动态组件名

        组件定义动态属性dic[:x]=""
        组件定义动态属性dic[:y]=""
        组件定义动态属性dic[:z]=""
        组件定义动态属性dic[:lenx]=""
        组件定义动态属性dic[:leny]=""
        组件定义动态属性dic[:lenz]=""

        其他属性id初始值=6
        动态属性arr=[]
        组件定义动态属性dic.each { | key, value |
          关键词位置= key=~ /^_/
          if 关键词位置!=nil
            next
          end
          if 排除属性arr.include?(key)
            next
          end

          if xyzlarr.include?(key)
            case key
            when "x"
              id=0
            when "y"
              id=1
            when "z"
              id=2
            when "lenx"
              id=3
            when "leny"
              id=4
            when "lenz"
              id=5
            end
          else
            id=其他属性id初始值
            其他属性id初始值=其他属性id初始值+1
          end

          if @@xyzarr.include?(key)
            最终动态属性s=组件实例动态属性dic
          else
            最终动态属性s=组件定义动态属性dic
          end

          hash=Hash[]

          hash[:id]=id
          hash[:key]=key
          hash[:value]=value
          label="_#{key}_formlabel"
          # puts "#{label} = #{组件定义动态属性dic[label]}"
          labelkk=最终动态属性s[label]
          if labelkk!=nil
            # labelkk.gsub!(/[\f\n\r\t\v]|└--▶/, "不认识！")
            labelkk.gsub!(/└--▶/, "不认识！")
            labelkk.gsub!(/	/, "")
          end
          hash[:formlabel]=labelkk

          if @@xyzarr.include?(key)
            最终动态属性s=组件实例动态属性dic
          else
            最终动态属性s=组件定义动态属性dic
          end

          formula="_#{key}_formula"
          # puts "#{label} = #{组件定义动态属性dic[label]}"
          hash[:formula]=最终动态属性s[formula]
          error="_#{key}_error"
          hash[:error]=最终动态属性s[error]
          动态属性arr.push hash
        }
        # puts 动态属性arr.to_json
        # puts "xxx= #{组件定义动态属性dic["_x_formula"]}"
        # 旧属性公式值=组件定义动态属性dic["_x_formula"]
        # puts "旧属性公式值"
        # puts 旧属性公式值
        动态属性h[:其他属性]=动态属性arr
        # puts 动态属性h.to_json
        return 动态属性h
      end
    end #遍历组件属性

  end #class
end # module amtf_su
