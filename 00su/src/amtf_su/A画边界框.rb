require 'sketchup.rb'
# require File.join(__FILE__, '../AF.rb')
require 'json'

module AMTF
  class A画边界框
    include AMTF_mixin
    def initialize(view,box)
      @view=view
      @box=box

    end
    def activate
      @画完边框 = false
      @选择点 = Sketchup::InputPoint.new
      self.reset(nil)
    end
    def resume(view)
    end
    def deactivate(view)
      view.invalidate if @画完边框
    end
    def onCancel(flag, view)
      self.reset(view)
    end
    def reset(view)
      @选择点.clear
      @message = "选择面 以识别内空"
      @pts = []
      Sketchup.set_status_text( @message )
      if( view )
        view.tooltip = nil
        view.invalidate if @画完边框
      end
      @画完边框 = false
    end

    def draw(view)
      # Sketchup.set_status_text( @message )
      puts "开始绘图"
      Sketchup.set_status_text( "开始绘图" )
#       - 0 = [0, 0, 0] (left front bottom)
# - 1 = [1, 0, 0] (right front bottom)
# - 2 = [0, 1, 0] (left back bottom)
# - 3 = [1, 1, 0] (right back bottom)
# - 4 = [0, 0, 1] (left front top)
# - 5 = [1, 0, 1] (right front top)
# - 6 = [0, 1, 1] (left back top)
# - 7 = [1, 1, 1] (right back top)
      # if @pts != []
      Sketchup.set_status_text( @box.corner(0) )
        @view.draw_points(@box.corner(0),12, 5, "red")

        @view.line_width = 4
        @view.drawing_color="blue"
        @view.draw_polyline(@box.corner(0),@box.corner(1))
        # @selection.clear
      # end
      @画完边框 = true
    end

  end #class
end # module amtf_su
