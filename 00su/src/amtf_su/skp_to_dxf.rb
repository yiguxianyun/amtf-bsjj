# SketchUp to DXF STL Converter
# download from http://http://www.guitar-list.com/download-software/convert-sketchup-skp-files-dxf-or-stl
# Original authors: Nathan Bromham, Konrad Shroeder
# with extra code from git-hub sketchup-stl project
# License: Apache License, Version 2.0
module AMTF
  class Askp_to_dxf
    include AMTF_mixin
    def file方式创建dxf
      export_ents=选中或全部对象()
      model = Sketchup.active_model
      ss = model.selection
      $stl_conv = 1.0
      $group_count = 0
      $component_count = 0
      $face_count = 0
      $line_count = 0
      @rad_to_deg=180/Math::PI
      @已经处理过的曲线=[]
      @属于已处理曲线的边s=[]
      others=[]
      entities = model.entities
      if (Sketchup.version_number < 7)
        model.start_operation("导出dxf ing")
      else
        model.start_operation("导出dxf ing",true)
      end
      路径,文件短名,无后缀名,后缀=解析文件名(@当前模型全名)
      导出dxf全名="#{路径}/#{无后缀名}.dxf"
      # @导出文件 = File.new( 导出dxf全名 , "w" )
      # @导出文件 = File.new( 导出dxf全名 , "w:gb2312" )
      @导出文件 = File.new( 导出dxf全名 , "w:GB2312" )
      # @导出文件 = File.new( 导出dxf全名 , "w:UTF-8" )
      model_name = 文件短名.split(".")[0]

      检测模型单位
      dxf_option="polylines"
      # dxf_option="lines"

      创建文本样式(@导出文件)
      @导出文件.puts( " 0\nSECTION\n 2\nENTITIES")
      # Recursively export faces and edges, exploding groups as we go, counting any entities we can't export
      others = 遍历模型(0, export_ents, Geom::Transformation.new(), dxf_option)
      # 写入文字
      @导出文件.puts( " 0\nENDSEC\n 0\nEOF")

      @导出文件.close
      UI.messagebox( $face_count.to_s + " faces exported " + $line_count.to_s + " lines exported\n" + others.to_s + " objects ignored" )
      model.commit_operation
      return 导出dxf全名
    end

    def 创建文本样式(file)
      # 写入DXF表格部分
      file.puts "0"
      file.puts "SECTION"
      file.puts "2"
      file.puts "TABLES"

      # 创建一个文本样式
      file.puts "0"
      file.puts "TABLE"
      file.puts "2"
      file.puts "STYLE"
      file.puts "0"
      file.puts "STYLE"
      file.puts "2"
      file.puts "字体样式kk"
      file.puts "70"
      file.puts "0"
      file.puts "40"
      file.puts "1.0"
      file.puts "41"
      file.puts "1.0"
      file.puts "50"
      file.puts "0.0"
      file.puts "71"
      file.puts "0"
      file.puts "42"
      file.puts "2.5"
      file.puts "3"
      file.puts "simkai.ttf"
      file.puts "4"
      file.puts ""

      # 结束表格部分
      file.puts "0"
      file.puts "ENDTAB"
      file.puts "0"
      file.puts "ENDSEC"
    end

    def 写入文字
      # @导出文件.puts("0\nTEXT\n 5\n BEB\n 330\n 1F\n 100\n AcDbEntity\n 8\n TextToNest\n100\nAcDbText")
      @导出文件.puts("0")
      @导出文件.puts("TEXT")
      @导出文件.puts("8")
      @导出文件.puts("TextToNest")
      @导出文件.puts("10\n0.0")
      @导出文件.puts("20\n0.0")
      @导出文件.puts("30\n0.0")
      @导出文件.puts("40\n25.0")
      @导出文件.puts("1")
      # text = "上柜-_右侧板#3_1_1_00_1_-1___默认板材(未指定)"
      # encoded_text = text.encode("gb2312")
      # @导出文件.puts(encoded_text)
      @导出文件.puts("上柜-_右侧板#3_1_1_00_1_-1___默认板材(未指定)")
      # @导出文件.puts("\\FKaiTi|b0|i0|c0|p34;你好，DXF！")
      # @导出文件.puts("{\\S|0.0|Arial|你好，DXF！}")
      @导出文件.puts("7\n字体样式kk")
      # @导出文件.puts("100\nAcDbText")
    end

    def 遍历模型(others, entities, tform,dxf_option)
      面s=entities.grep(Sketchup::Face)
      面s.each{|f|
        loops = f.loops
        loops.each {|loop|
          if loop.outer?
            edges = loop.edges
            edges.each{|ee|
              写入边线(ee, tform)
            }
          else
            # puts "Loop is not an outer loop."
          end
        }
      }
      puts "面s 👉 #{面s}"
      if 面s.count==0
        边s=entities.grep(Sketchup::Edge)
        边s.each{|b|
          写入边线(b, tform)
        }
      end
      grep集合中组件s(entities).each{|g|
        puts "定义或实例名 👉 #{定义或实例名(g)}"
        others = 遍历模型(others, 组或群entities(g),tform * g.transformation,dxf_option)
      }
    end

    def dxf_transform_edge(edge, tform)
      points=[]
      points.push(转换点坐标(edge.start, tform))
      points.push(转换点坐标(edge.end, tform))
      points
    end

    def 转换点坐标(e, tform)
      # 点或者位置
      if e.class==Sketchup::Vertex
        p=e.position
      else
        p=e
      end
      point = Geom::Point3d.new(p.x, p.y, p.z)
      point = Geom::Point3d.new(p.x, p.y, p.z)
      point.transform! tform
      point
    end

    def 处理图层名_颜色_厚度(原图层名)
      if 原图层名=="0" || 原图层名=="1"
        @导出文件.puts( "62")
        @导出文件.puts( "1") #1=红色
        return
      end
      # 封边信息=
      封边左=1
      封边右=2
      封边上=3
      封边下=4
      if ["2","3","4","5"].index(原图层名)
        case 原图层名
        when "2"
          封边厚度=封边左
        when "3"
          封边厚度=封边右
        when "4"
          封边厚度=封边下
        when "5"
          封边厚度=封边上
        end
        @导出文件.puts( "39")
        @导出文件.puts( 封边厚度)
      end

    end

    def 提取厚度(s)
      match=/-sd([-\d.]+)/.match("399-amtf-zhengyuan-ABF-DSIDE-8-sd34")
      puts "match 👉 #{match[1]}"
      # match开头数字=/^\d+/.match(s)
      return match
    end

    def 提取开头的数字(s)
      # match开头数字=/^\d+/.match("399-amtf-zhengyuan-ABF-DSIDE-8-sd34")
      match开头数字=/^\d+/.match(s)
      return match开头数字
    end

    def 写入边线(edge, tform)
      原图层名=edge.layer.name
      if 原图层名=~ /临时层|ABF_Label/
        puts "转换后图层名=~ /临时层/ 👉 #{原图层名}"
        return
      end
      # 转换后图层名=提取开头的数字(原图层名)
      #原样输出算了，还是用原来的C#方式处理颜色等等，因为这里只能是导出简化版的dxf，云熙无法识别
      转换后图层名=原图层名
      类型=识别边线类型(edge)
      if 类型=="直线"
        puts "类型 👉 #{类型}"
        points = dxf_transform_edge(edge, tform)
        @导出文件.puts( "  0\nLINE\n 8\n"+转换后图层名)
        # 处理图层名_颜色_厚度(原图层名)
        for j in 0..1 do
          @导出文件.puts((10+j).to_s+"\n"+(points[j].x.to_f * $stl_conv).to_s)#x
          @导出文件.puts((20+j).to_s+"\n"+(points[j].y.to_f * $stl_conv).to_s)#y
          @导出文件.puts((30+j).to_s+"\n"+(points[j].z.to_f * $stl_conv).to_s)#z
        end
        $line_count+=1
        return
      end

      c=edge.curve
      if @已经处理过的曲线.index(c)
        return
      else
        puts "curve 👉 #{c}"
        @已经处理过的曲线 << c
      end
      puts "类型 👉 #{类型}"
      case 类型
      when "整圆"
        puts "CIRCLE 转换后图层名 👉 #{转换后图层名}"
        # puts "c.center 👉 #{c.center}"
        # c.transform! tform
        # puts "c.center 👉 #{c.center}"
        圆心 = 转换点坐标(c.center,tform)
        @导出文件.puts( "  0\nCIRCLE\n 8\n"+转换后图层名)
        # 处理图层名_颜色_厚度(原图层名)
        @导出文件.puts((10).to_s+"\n"+(圆心.x.to_f * $stl_conv).to_s)#x
        @导出文件.puts((20).to_s+"\n"+(圆心.y.to_f * $stl_conv).to_s)#y
        @导出文件.puts((30).to_s+"\n"+(圆心.z.to_f * $stl_conv).to_s)#z
        @导出文件.puts((40).to_s+"\n"+(c.radius.to_f * $stl_conv).to_s)#z
      when "圆弧"
        puts "ARC 转换后图层名 👉 #{转换后图层名}"
        dir=c.normal.z
        if dir == 1
          _start=c.first_edge.vertices[0].position
          _end=c.last_edge.vertices[1].position
        elsif dir == -1
          _start=c.last_edge.vertices[1].position
          _end=c.first_edge.vertices[0].position
        end

        start_angle=@rad_to_deg*Geom::Vector3d.new(1,0,0).angle_between(Geom::Vector3d.new(_start.x-c.center.x,_start.y-c.center.y,_start.z-c.center.z))
        start_angle = 360-start_angle if c.center.y > _start.y
        # start_angle = 0-start_angle if c.center.y > _start.y # for - values
        end_angle = @rad_to_deg*c.end_angle

        @导出文件.puts( "  0\nARC\n 8\n"+转换后图层名)
        # 处理图层名_颜色_厚度(原图层名)
        @导出文件.puts((10).to_s+"\n"+(c.center.x.to_f * $stl_conv).to_s)#x
        @导出文件.puts((20).to_s+"\n"+(c.center.y.to_f * $stl_conv).to_s)#y
        @导出文件.puts((30).to_s+"\n"+(c.center.z.to_f * $stl_conv).to_s)#z
        @导出文件.puts((40).to_s+"\n"+(c.radius.to_f * $stl_conv).to_s)#z
        @导出文件.puts((50).to_s+"\n"+(start_angle.to_f ).to_s)#z
        @导出文件.puts((51).to_s+"\n"+((start_angle+end_angle).to_f ).to_s)#z
      when "曲线"
        puts "POLYLINE 转换后图层名 👉 #{转换后图层名}"
        @导出文件.puts("  0\nPOLYLINE\n 8\n"+转换后图层名)
        @导出文件.puts(" 66\n     1\n70\n    8\n 10\n0.0\n 20\n 0.0\n 30\n0.0")
        for j in 0..c.vertices.length do
          if (j==c.vertices.length)
            count = 0
          else
            count = j
          end
          point = 转换点坐标(c.vertices[count],tform)
          @导出文件.puts( "  0\nVERTEX\n  8\n"+转换后图层名)
          # 处理图层名_颜色_厚度(原图层名)
          @导出文件.puts("10\n"+(point.x.to_f * $stl_conv).to_s)
          @导出文件.puts("20\n"+(point.y.to_f * $stl_conv).to_s)
          @导出文件.puts("30\n"+(point.z.to_f * $stl_conv).to_s)
          @导出文件.puts( " 70\n     32")
        end
        if (c.vertices.length > 0)
          @导出文件.puts( "  0\nSEQEND")
        end
      end

    end

    def 识别边线类型(e)
      if @属于已处理曲线的边s.index(e)
        # puts "属于已处理曲线的边s!"
        return
      end
      if e.curve # is curve?
        edges=e.curve.edges
        @属于已处理曲线的边s=@属于已处理曲线的边s.concat(edges)
        if e.curve.typename != "ArcCurve" # is no arc? -> is polyline
          return 类型="曲线"
        else
          return 类型=曲线是整圆还是圆弧(e.curve)
          # puts "e.curve.end_angle 👉 #{e.curve.end_angle}"
          # if e.curve.end_angle==Math::PI*2
          #   return 类型="整圆"
          # else
          #   return 类型="圆弧"
          # end
        end
      else
        return 类型="直线"
      end
    end



  end#class
end # module amtf_su
