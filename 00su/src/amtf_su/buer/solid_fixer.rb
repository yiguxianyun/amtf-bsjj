# require File.join(__FILE__, '../A布尔操作_mixin.rb')

module AMTF
require 'sketchup.rb'
module Boolean
  class SolidFixer
    include A布尔操作_mixin

    # @param [Group/Component] instance, the instance that should be a solid but failed the solid checks
    def initialize(instance)
      @instance = instance #the instance to be made a solid if possible
      @entities = get_entities(instance) # the instances entities
      @scaler = InstanceScaler.new(instance)
      @base_edge = nil #most of the functions rely on the current base edge
    end
    # all edges in a solid should have 2 faces!
    def repair_missing_faces
      remove_edges = [] # make sure this is empty at the start of this function
      bad_edges = get_edges_with_less_than_2_faces(remove_edges) #edges with less than 2 faces

      # p "initial bad edges: #{bad_edges}" #todo remove

      if (bad_edges.first != nil)

        @base_edge = nil #make sure base_edge is reset

        loop do #instead of begin end while, using loop do break end

          @base_edge = bad_edges.collect{ |e| e if(e.faces.length == 1) }.compact.first #start from an edge that has 1 face if possible

          # p "base edge that has 1 face: #{@base_edge}" #todo remove

          if (@base_edge.nil?) #if no bad edge with a face is left use any edge

            @base_edge = bad_edges.first

            # p "no bad edge has a face: #{@base_edge}" #todo remove

          end

          face_removed = remove_face_that_should_be_broken_by_edges #try to remove bad faces first!

          # p "face removed: #{face_removed}" #todo remove

          if (face_removed)

            bad_edges = get_edges_with_less_than_2_faces(remove_edges) #reset the bad edges

            # p "face removed. updated bad edges: #{bad_edges}" #todo remove

          end

          # p "creating face" #todo remove

          face = (@base_edge.nil? || !@base_edge.valid?) ? nil : create_face_using_edge_and_open_vertices(bad_edges) #attempt to create a face

          if (face.nil?)

            # p "no face was created!" #todo remove

            remove_edges.push(@base_edge) # mark this edge as one that can't make a face

            @base_edge = nil

            bad_edges = bad_edges - remove_edges

            # p "base edge should be removed from bad edges: #{bad_edges}" #todo remove

          else

            # p "a face was made: #{face}" #todo remove

            # face.material = 'red' #todo remove

            @base_edge = nil

            bad_edges = bad_edges.clone.collect{ |edge| (edge.valid? && (edge.faces.length < 2)) ? edge : nil }.compact - remove_edges

            # p "bad edges should be changed since a face was made unless all edges had 0 faces: #{bad_edges}" # todo remove

          end

          # p "bad edges left: #{bad_edges.length}" #todo remove

          break unless (!bad_edges.empty?)

        end

      end

    end



    #moves along a curve till it finds a vertex that is used to make a face or the curve ends (no more edges / splits into multiple new curves)

    # @param [Boolean] start_is_base, true if extending the start vertex; false if extending the end vertex

    def find_curves_end_vertex_faces(start_is_base)

      faces = []



      if (start_is_base)

        current_vertex = @base_edge.start

        prev_vertex = @base_edge.end

      else

        current_vertex = @base_edge.end

        prev_vertex = @base_edge.start

      end



      edge = @base_edge



      loop do

        edges = current_vertex.edges

        edges = edges - [edge, @base_edge]

        if (edges.length == 1) #this vertex has 2 edges (current edge and one more)

          vertices = edges.first.vertices #collect{ |e| e.vertices }

          temp_vertex = vertices - [current_vertex, prev_vertex] #this should get the new vertex

          if (temp_vertex.empty?)

            # p 'no vertex left!' #todo remove

            break

          else

            prev_vertex = current_vertex

            current_vertex = temp_vertex.first #should only have the one entity

            edge = edges.first #prev_vertex.common_edge(current_vertex)

            faces = current_vertex.faces #attempt to fill array with faces

          end

        else

          break #multiple edges or no edges

        end

        break unless (faces.empty?)

      end

      return faces

    end



    # removes a face if it should have been broken by this edge

    # @returns [Boolean] true if face was remove

    def remove_face_that_should_be_broken_by_edges

      #check what faces this edge does make

      #check what faces use this edges first vertex

      #if no face uses this vertex see if only 1 edge is attached and use that vertex

      #check what faces use this edges second vertex

      #if no face uses this vertex see if only 1 edge is attached and use that vertex



      # any face that uses both vertex of this edge should be in the edges faces list

      # otherwise the face is bad and should be removed



      removed_a_face = false



      if (@base_edge.nil? || !@base_edge.valid?) #todo this might not be needed anymore

        # p "base edge was no longer valid, so no face removed!" #todo remove this

        return removed_a_face

      end



      edges_faces = @base_edge.faces #find the edges faces



      start_vertex_faces = @base_edge.start.faces

      start_is_base = true

      start_vertex_faces = find_curves_end_vertex_faces(start_is_base) if (start_vertex_faces.empty?) #follows a single line till it joins with multiple or has faces



      start_is_base = false

      end_vertex_faces = @base_edge.end.faces

      end_vertex_faces = find_curves_end_vertex_faces(start_is_base) if (end_vertex_faces.empty?)



      bad_faces = start_vertex_faces & end_vertex_faces #potential bad faces are those that use both ends on this edge

      bad_faces = bad_faces - edges_faces #if a face uses both vertices but not the edge itself it is a bad face!



      if (bad_faces.empty?)

        # p "no bad faces found for edge: #{@base_edge}"

      else

        bad_faces.each{ |face|

          # make edges hard so they dont disappear

          face.edges.each{ |e|

            e.soft = false

            # e.smooth = false

          }

          face.erase!

          removed_a_face = true

        }

      end



      return removed_a_face # if a face was removed return true

    end



    # makes sure the base vertex is not the same as the start vertex so the base edge can't be returned

    # then returns a valid edge if it exists to close the face or nil if no edge exists

    # @param [Vertex] start_vertex, first vertex used to make the face

    # @param [Vertex] end_vertex, last vertex to be used to make the face (connected to start vertex by the base edge)

    # @param [Vertex] base_vertex, the last vertex added to the possible face points

    # @returns [Edge] or [nil]

    def find_edge_to_close_the_face(start_vertex, end_vertex, base_vertex)

      next_edge = nil

      if (base_vertex != start_vertex) #check to make sure we are past the first vertex

        #check to see if we can close the face

        next_edge = base_vertex.common_edge(end_vertex)

        next_edge = nil if (next_edge && next_edge.faces.length > 1) #todo maybe remove this condition

      end

      return next_edge

    end



    # finds all open vertices connected to the current vertex by an edge with less than 2 faces

    # @param [Array] path_array, the current path_array that we want to extend to a new vertex

    # @param [Array] open_vertices, the vertices that do not currently make 2 or more faces

    def extend_possible_paths_for_base_vertex(path_array, open_vertices, base_vertex, new_possible_paths)

      possible_next_vertices = base_vertex.edges.collect{ |e| e.other_vertex(base_vertex) if (e.faces.length < 2) }.compact

      possible_next_vertices = [] if (possible_next_vertices.nil?)

      possible_next_vertices = possible_next_vertices - @base_edge.vertices #removes used vertices

      possible_next_vertices.each{ |vertex|

        if (!path_array.include?(vertex)) #prevent the face from using a vertex more than once

          if (valid_next_vertex?(vertex, base_vertex))

            temp_path = path_array.clone

            temp_path.push(vertex)

            new_possible_paths.push(temp_path) #todo remove this instance variable?

          end

        end

      }

    end



    # checks that the vertex is connected to the base vertex by an edge that is considered open (currently creates less than 2 faces)

    # @param [Vertex] vertex, might be added to possible face points

    # @param [Vertex] base_vertex, current vertex joined to the possible points to make the face

    # @returns true if vertices are joined by an edge that has less than 2 faces

    def valid_next_vertex?(vertex, base_vertex)

      next_edge = base_vertex.common_edge(vertex)

      if (next_edge) #edge must exist

        if (next_edge.faces.length < 2) # ensure the edge is not already creating 2 faces

          return true

        end

      end

      return false

    end



    # returns an array of edges that the face vertices correspond to

    def get_edges_based_on_face_vertices(face_vertices)

      edges = []

      prev_v = face_vertices.last

      face_vertices.each{ |v|

        edges.push(v.common_edge(prev_v))

        prev_v = v

      }



      return edges

    end



    # @returns [Face] that was made or nil if face could not be created

    def create_face_from_vertices(face_vertices)

      begin

        # p "attempting to make a face!" #todo remove

        #todo check that the face that was made uses the same edges that the vertices used

        # if it does not then remove any edges that were not used!

        edges = get_edges_based_on_face_vertices(face_vertices)

        faces = @base_edge.faces

        face = @entities.add_face(face_vertices) #this will error out if not planar

        # p "face didn't use the edges it was suppose to!!!" if (!face.nil? && !(edges - face.edges).empty? ) #todo find a way that this doesn't happen or is handled

        face = (face.nil? || (faces.include?(face))) ? nil : face #if it made a face that already exists return nil so it know a new face was not made!

        return face

      rescue ArgumentError => e

        # this path is failed continue without adding this path to the new path array

        # p "vertices are not planar: #{face_vertices}"

        # p "#{e.message}"

        return nil

      end

    end



    # creates a new faces using the least amount of vertices starting with the base edge and then any other open edge/vertex

    # @param [Array] bad_edges, edges used to make less than 2 faces

    # @returns [Face] that was made or [nil] if face could not be made using specified edge and vertices

    def create_face_using_edge_and_open_vertices(bad_edges)

      open_vertices = get_vertices_from(bad_edges) #array of vertex elements that can be used to make a face

      start_vertex = @base_edge.start

      end_vertex = @base_edge.end

      open_vertices = open_vertices - [start_vertex, end_vertex] # remove these vertices from the list of open vertices

      possible_paths = [[start_vertex]]

      face = nil

      loop do

        new_possible_paths = []

        possible_paths.each{ |path_array|

          base_vertex = path_array.last

          next_edge = find_edge_to_close_the_face(start_vertex, end_vertex, base_vertex)

          if (next_edge.nil?)

            #can't close the face yet keep adding to the edge array

            #if 3 points are in the path only keep it if points are planar

            if ((path_array.length < 3) || are_vertices_planar?(path_array))

              extend_possible_paths_for_base_vertex(path_array, open_vertices, base_vertex, new_possible_paths)

            end

          else

            #create the face

            face_vertices = path_array.clone

            face_vertices.push(end_vertex)

            orientate_face_vertices(face_vertices) #make sure face vertices are in the correct order so face normal is correct

            if (are_vertices_planar?(face_vertices))

              trans = @scaler.scale_to_avoid_tiny_face_bug(face_vertices) #true if the entities were scaled

              face = create_face_from_vertices(face_vertices)

              # p "vertices used: #{face_vertices}" if (!face.nil?)

              @scaler.transform_entities(trans.inverse) unless (trans.nil?)

            else

              face = nil

            end

            break if (!face.nil?) #face was created time to exit

          end

        }

        # p "possible paths: #{new_possible_paths}"

        possible_paths = new_possible_paths

        break unless ((!possible_paths.empty?) && (face.nil?))

      end

      return face

    end



    # reverses the order of the face vertices if needed so the face is orientated properly

    def orientate_face_vertices(face_vertices)

      #find an edge with a face

      prev_vertex = face_vertices.last

      reverse_vertices = false

      face_vertices.each{ |vertex|

        edge = vertex.common_edge(prev_vertex)

        if (edge && (edge.faces.length == 1))

          #this edge has a face that's the only one we need

          reverse_vertices = is_edge_use_the_same?(edge, [prev_vertex, vertex])

          break

        end

      }



      face_vertices.reverse! if (reverse_vertices)

    end



    # @param [Edge] edge, an existing edge that makes a face

    # @param [Array] vertex_array, vertices that make the edge sent but in the order that they are being used to create a new face

    # this order should be the opposite that edge is used.

    def is_edge_use_the_same?(edge, vertex_array)

      reverse = false

      face = edge.faces.first #should only have 1 face

      loops = face.loops

      #find the loop that has this edge

      loops.each{ |loop|

        if (loop.edges.include?(edge))

          #this loop uses this edge

          edgeuse = loop.edgeuses.collect{ |e| e if (e.edge == edge) }.compact.first #return an element not an array

          if (!edgeuse.nil?)

            vertex_order_reversed = edgeuse.reversed?

            face_vertex_array = [edge.start, edge.end]

            face_vertex_array.reverse! if (vertex_order_reversed)

            #is this an inner or outer loop

            if (loop.outer?)

              #the vertices use must be opposite

              reverse = (face_vertex_array == vertex_array) #if these match need to reverse else its fine

            else

              #must be an inner: vertices use must be the same

              reverse = (face_vertex_array != vertex_array) #if they are the same dont reverse

            end

          end

          break #only need to use one edge

        end

      }



      return reverse

    end



    # finds all edges within entities that have less than 2 faces

    # @returns [Array] of edges

    def get_edges_with_less_than_2_faces(remove_edges)

      @entities = get_entities(@instance) #reseting the entities

      edges = @entities.grep(Sketchup::Edge)

      bad_edges = edges.collect{ |edge| (edge.valid? && (edge.faces.length < 2)) ? edge : nil }.compact

      return bad_edges - remove_edges # do not include edges that have been removed

    end



    # creates an array of all the vertices used to create the array of edges

    # @returns [Array] array with unique vertex elements representing all the vertices used by edges sent

    def get_vertices_from(edges)

      vertices = edges.collect{ |e| e.vertices }

      vertices.flatten!

      vertices.uniq!

      return vertices

    end



  end



end

end

