module AMTF
require 'sketchup.rb'
module Boolean
  # @param [Array] instances, array of instances to make any groups unique
  def self.make_groups_uniq(instances)

    instances.each{ |instance|

      instance.make_unique if(self.group?(instance))

    }

  end

  def self.group?(instance)

    if (instance.class == Sketchup::Group)

      return true

    else

      return false

    end

  end

module A布尔操作_mixin2
  PLANAR_PRECISION = 6 #decimal places
  def get_entities(instance)
    return (instance.class == Sketchup::Group) ? instance.entities : instance.definition.entities
  end
  # finds all edges with less than 2 faces and attempts to find faces for them

  # @param [Group/Component] instance, group to try and make into a solid

  def add_faces_to_complete_solid(instance)

    solid_repair = SolidFixer.new(instance)

    solid_repair.repair_missing_faces

  end

  #returns true if the instance has faces and edges, false otherwise
  def instance_has_surface?(instance)
    entities = get_entities(instance)
    has_faces = false
    has_edges = false
    entities.grep(Sketchup::Face){ |f| has_faces = true; break }
    entities.grep(Sketchup::Face){ |e| has_edges = true; break }
    if (has_faces && has_edges)
      return true
    else
      return false
    end
  end

  # @param [Group] instance, group or component to be duplicated
  # @param [Boolean] remove_inners, true if inner instance should be removed when copied, false to keep them
  def copy_instance(instance, remove_inners = true)
    parent = instance.parent
    entities = parent.entities
    copied_inst = create_instance_copy_in_entities(entities, instance, remove_inners)
    # amtf.nil
    第几次=1
    puts "copy_instance 第几次 #{第几次}"
    第几次=第几次+1
    return copied_inst
  end

  # copies the instance into the active_instances parent
  # @param [Instance] instance, the instance to copy
  # @param [Instance] other_instance, the instance that will be at the same level as the copy (other_instances parent will contain the copy)
  # @param [Boolean] put_in_same_level, true if the copy will be at the same level as active
  def copy_remove_nested_instance(instance, other_instance, put_in_same_level = true)
    if (put_in_same_level)
      parent = other_instance.parent
      entities = parent.entities
    else
      entities = get_entities(other_instance)
    end
    remove_inners = true
    copied_inst = create_instance_copy_in_entities(entities, instance, remove_inners)
    return copied_inst
  end

  # creates a copy with same material, layer and attributes
  # @param [Boolean] remove_inners, false to keep inner instances or true to let them be removed!
  def create_instance_copy_in_entities(entities, instance, remove_inners)
    copied_inst = 添加实例对象(entities, instance)
    remove_text_and_dimensions(copied_inst)
    remove_inner_instances(copied_inst) if (remove_inners)
    copied_inst.material = instance.material #material
    copied_inst.layer = instance.layer #layer
    #attribute dictionaries for instance and definition
    copy_attribute_dictionaries_from(instance, copied_inst)
    puts "create_instance_copy_in_entities"
    # amtf.nil
    return copied_inst
  end

  def remove_text_and_dimensions(instance)
    entities = get_entities(instance)
    entities.grep(Sketchup::Text){ |t| t.erase! }
    entities.grep(Sketchup::Dimension){ |d| d.erase! }
  end

  # adds a unique instance of instance to entities
  def 添加实例对象(entities, instance)
    # 添加的实例对象=nil
    if (instance.class == Sketchup::Group)
      添加实例对象 = entities.add_instance(instance.entities.parent, instance.transformation)
      添加实例对象.make_unique
    elsif (instance.class == Sketchup::ComponentInstance)
      puts "添加个实例对象"
      添加实例对象 = entities.add_instance(instance.definition, instance.transformation)
      添加实例对象.make_unique
    end
    return 添加实例对象
  end

  # @param [Group/Component] instance, group that is suppose to be a solid
  def solid?(instance)

    #TODO is it worth using "manifold?" instead? need to research what manifold? all checks

    entities = get_entities(instance)

    #Solid should have 2 faces for every edge

    entities.grep(Sketchup::Edge) { |edge|

      faces = edge.faces

      if (faces.length != 2)

        return false

      end

    }



    return true

  end

  #deletes any edge not used to create a face
  def remove_free_edges(instance)

    entities = get_entities(instance)

    delete_these_edges = []



    entities.grep(Sketchup::Edge){ |edge|

      faces = edge.faces

      if (faces.empty?)

        delete_these_edges.push(edge)

      end

    }



    entities.erase_entities(delete_these_edges)

  end

  # removes edges that have 2 faces both with identical normals
  # returns true if edges were removed!
  def delete_coplanar_edges(instance)

    entities = get_entities(instance)



    delete_edges = []

    entities.grep(Sketchup::Edge) { |e|

      faces = e.faces

      if ((faces.length > 1) && are_faces_material_the_same(faces) && are_faces_coplanar(faces))

        delete_edges.push(e)

      end

    }



    if (delete_edges.empty?)

      return false

    else

      entities.erase_entities(delete_edges) #remove the coplanar edges

      return true

    end

  end

  # @param [Array] faces, faces array should contain 2 faces that share an edge
  # @returns [Boolean] true if faces are determined to be on the same plane
  def are_faces_coplanar(faces) #todo using point_on_plane method means shouldn't need to scale before this

    #TODO check for and remove any duplicate faces first



    #make a plane from one of the faces

    plane = nil

    vertices = []

    faces.each{ |face|

      vertices.push(face.vertices)

    }

    vertices.flatten!

    vertices.uniq!

    #check if all the points lie on this plane

    result = are_vertices_planar?(vertices)



    return result

  end

  # if all faces have the same material return true
  def are_faces_material_the_same(faces)

    if (!faces.first.nil?)

      material = faces.first.material

      faces.each{ |face|

        return false if (face.material != material)

      }

    end

    return true

  end

  #creates a plane from the vertices points then checks all points are on that plane

  def are_vertices_planar?(vertices)

    points = vertices.collect{ |v| v.position }

    plane = Geom.fit_plane_to_points(points)

    points.each{ |p|

      if (!point_on_plane?(p, plane))

        return false

      end

    }

    return true

  end



  #more accurate check of point.on_plane? which only goes to 0.001"

  def point_on_plane?(point, plane)

    distance = point.distance_to_plane(plane).round(PLANAR_PRECISION) #todo change magic number 6 to a constant

    if (distance != 0.0)

      return false

    else

      return true

    end

  end



  # @param [Entity] entity, usually expect this to be an instance or definition

  # @param [Entity] target, usually expect this to be a copy of the entity

  def copy_attribute_dictionaries(entity, target)

    attribute_dictionaries = entity.attribute_dictionaries

    if (!attribute_dictionaries.nil?)

      attribute_dictionaries.each{ |dictionary|

        dict_name =  dictionary.name #name of the dictionary to be recreated

        if (dict_name != "GSU_ContributorsInfo")

          dictionary.each{ |key, value| #key and value for this dictionary

            target.set_attribute(dict_name, key, value) #todo can throw RuntimeError: Not allowed to create Contributors attribute.

          }

        end

      }

    end

  end



  # copies attribute dictionaries from inst1 to inst2

  def copy_attribute_dictionaries_from(inst1, inst2)

    original_definition = (inst1.class == Sketchup::Group) ? inst1.entities.parent : inst1.definition

    definition = (inst2.class == Sketchup::Group) ? inst2.entities.parent : inst2.definition



    copy_attribute_dictionaries(inst1, inst2)

    copy_attribute_dictionaries(original_definition, definition)

  end



  def remove_inner_instances(instance)

    entities = get_entities(instance)

    entities.grep(Sketchup::Group){ |g| g.make_unique.erase! }

    entities.grep(Sketchup::ComponentInstance){ |c| c.erase! }

  end



  def get_next_intersection(ray)

    model = Sketchup.active_model

    entity = model.raytest(ray)

    return entity

  end



  #returns an array of the meshes (groups/components)

  def inmesh?(point, vector = Geom::Vector3d.new(0,0,1))

    p1 = point

    # Sketchup.active_model.entities.add_cpoint(p1) #TODO remove this



    intersect = true

    num = 0

    inmesh = {}

    while (intersect)

      intersect = get_next_intersection([p1, vector])

      if (intersect)

        p1 = intersect[0]

        # Sketchup.active_model.entities.add_cpoint(p1) #TODO remove this

        # Sketchup.active_model.entities.add_line(point, p1) #TODO remove this

        inst_path = intersect[1]

        instance = inst_path[-2]   #get the instance or group that contains the face that intersected

        inmesh[instance] = [] if (!inmesh[instance]) #if this is the first time this group/component was hit start its array

        inmesh[instance].push(inst_path.last)  #keep track of the found face

        num += 1

      end

      intersect = false if (num > 500)  #prevents infinite loops

    end



    inmeshes = []

    inmesh.each { |group, faces|

      # p "groups hit: #{group}" #TODO remove this

      if ((faces.length % 2) == 1)  #odd number of intersections and therefore the the point is inside the mesh

        inmeshes.push(group)

      end

    }



    return inmeshes

  end



  # @param [Group/component] instance, this instance has only the edges made from an intersection between 2 instances

  # @returns [Array] containing all vertices used only once

  def open_vertices_of_intersection(instance)

    entities = get_entities(instance)



    #check that the intersection lines are all connected

    #each vertex should have an even number of edges attached

    new_edges = entities.grep(Sketchup::Edge)

    odd_vertices = []

    vertices = Hash.new(0)



    new_edges.each { |e|

      #collect vertices in a hash to see how many times they are used

      # should be an even number

      vertices[e.start] += 1

      vertices[e.end] += 1

    }



    #if there is odd number of use it needs to be connected to another vertex

    odd_vertices = vertices.map{ |k, v| (v == 1) ? k : nil }.compact



    return odd_vertices

  end



  # if there are an even number of open vertices then connect them with new edges

  # @param [Group/Component] instance, the group that the connections will be made in

  def repair_open_intersection_edges(instance)

    odd_vertices = open_vertices_of_intersection(instance)

    entities = get_entities(instance)

    #if vertices have an odd number the edges are not closed should be equal number of odd vertex

    # determine which vertex to connect (closest together?)



    # p "open vertices = #{odd_vertices}"



    #connecting open vertices based on distance from each other

    loop do

      odd_vertices.each{ |base_vertex|

        odd_vertices.delete(base_vertex)

        distance = -1

        connect_to = nil

        odd_vertices.each{ |open_vertex|

          temp_distance = base_vertex.position.distance(open_vertex.position)

          # p "distance: #{temp_distance}"

          if (distance < 0)

            distance = temp_distance

            connect_to = open_vertex

          elsif (temp_distance < distance)

            distance = temp_distance

            connect_to = open_vertex

          end

        }

        odd_vertices.delete(connect_to)

        #TODO check if a line should be drawn or vertices merged based on distance

        # p "add line between: #{base_vertex} and #{connect_to}"

        entities.add_line(base_vertex, connect_to)

        # p "the intersection lines were modified!"

        break

      }

      break unless (odd_vertices.length > 1)

    end

  end



  # return a loop of points without reference to the bad vetex and using the remaining on instead if the bad vertex was referenced

  def corrected_loop_points(loop, bad_vertex, remaining_vertex)

    prev_vertex = loop.last

    new_loop = []

    loop.each{ |v|

      if (v == bad_vertex)

        if (remaining_vertex != prev_vertex)

          prev_vertex = remaining_vertex

          new_loop.push(remaining_vertex)

        end

      else

        if (v != prev_vertex)

          prev_vertex = v

          new_loop.push(v)

        end

      end

    }

    loop_points = new_loop.collect{ |v| v.position }

    return loop_points

  end



  # transforms a vertex to a point

  def transform_vertex_to_point(entities, vertex, point)

    vector = vertex.position.vector_to(point)

    vertex_trans = Geom::Transformation.new(vector)

    entities.transform_entities(vertex_trans, vertex)

  end



  # takes 2 objects as parameters then makes any groups unique to prevent them acting like components

  # scales both object up so really small edges can be created

  # intersects the first group with the second placing intersection edges in a empty temp group

  # then intersects the second group with the first placing the edges in the same temp group

  # runs a attempted fix on the intersection edges (the function could be improved but was needed in some cases where there are breaks in the intersection edges)

  # explodes the intersection lines onto both objects

  # returns both objects to the original scale

  # repairs solids if the explode made the object no longer a solid

  # @param [Instance] inst1, the instance that will be modified

  # @param [Instance] inst2, the instance that is used to modify the other instance

  # @param [Instance] base_inst, if inst1 is not the same as base_inst and is modified make it unique.

  # used to prevent all nested inner components from changing based on boolean operation.

  def intersect_meshes(inst1, inst2, base_inst)
    inst1.make_unique if (inst1.class == Sketchup::Group)
    scaler = InstancesScaler.new([inst1, inst2])
    scale_array = scaler.scale_instances_based_on_small_edge #todo for consistency maybe dont only scale up but always scale this is the smallest edge
    delete_coplanar_edges(inst1)
    delete_coplanar_edges(inst2)
    triangulate([inst1, inst2]) #only triangulating non planar faces
    fix_solids([inst1, inst2])
    #TODO use duplicates of the instances without inner instances to make the intersection lines
    remove_inners = true
    inst1_dup = copy_instance(inst1, remove_inners)
    inst2_dup = copy_instance(inst2, remove_inners)
    ents1_dup = get_entities(inst1_dup)
    ents2_dup = get_entities(inst2_dup)
    intersection_group = Sketchup.active_model.entities.add_group
    intersection_entities = intersection_group.entities

    #TODO get these transforms so the entities are viewed in respect to the model!
    t = intersection_group.transformation
    t1 = inst1.transformation
    t2 = inst2.transformation
    #create the intersection in a new group so we don't have to perform the intersection twice
    #WARNING: the placement of ents2 and inst1 matter so we need to do this twice to be safe
    ents2_dup.intersect_with(false, t2, intersection_entities, t, true, inst1_dup)   #intersect the second group/component with the first - intersection lines created on the first object
    ents1_dup.intersect_with(false, t1, intersection_entities, t, true, inst2_dup)

    if ((inst1 != base_inst) && !intersection_entities.grep(Sketchup::Edge).empty?)

      unless (is_instance_uniq?(inst1)) #inner component is about to be changed and is not uniq

        scaler.reverse_scale_instances(scale_array)

        inst1.make_unique

        scaler = InstancesScaler.new([inst1, inst2])

        scale_array = scaler.scale_instances_based_on_small_edge

      end

    end

    #TODO how to improve these functions?
    repair_open_intersection_edges(intersection_group)
    #explode intersection lines into inst1 and inst2
    explode_intersection_lines_into_instance(intersection_group, inst1)
    explode_intersection_lines_into_instance(intersection_group, inst2)
    remove_component_instance_and_definition(inst1_dup)
    remove_component_instance_and_definition(inst2_dup)
    #WARNING: explode can remove faces!
    fix_solids([inst1, inst2])
    scaler.reverse_scale_instances(scale_array)
    intersection_group.erase! if (intersection_group.valid?) #this is a group can be erase!
    return

  end #end of intersect method



  # if only 1 or 0 instance of this exists it is unique

  def is_instance_uniq?(instance)

    definition = instance.definition

    instances = definition.instances

    if (instances.length < 2)

      return true

    else

      return false

    end

  end



  # run all the current solid fixing operations until the instance is a solid or all have been run

  def fix_solids(instances)

    instances.each{ |instance|

      if (!solid?(instance) && instance_has_surface?(instance))

        # p "#{instance} no longer solid after exploding intersection lines"

        explode_edges_that_dont_have_2_faces(instance)

        explode_faces_if_edges_only_face(instance)

        add_faces_to_complete_solid(instance)

        remove_free_edges(instance)

        # p "#{instance} still not a solid" if (!solid?(instance))

      end

    }

  end



  #creates a temp group and draws edges that dont have 2 face in it

  # then explodes the temp group to hopefully break the existing face so the edges have 2 faces

  def explode_edges_that_dont_have_2_faces(instance)

    entities = get_entities(instance)

    edges = entities.grep(Sketchup::Edge){ |e| e if (e.faces.length != 2) }.compact

    edges = [] if (edges.nil?)

    temp_group = entities.add_group

    edges.each{ |edge|

      begin

        temp_group.entities.add_line(edge.vertices)

      rescue ArgumentError => e



      end

    }

    make_instance_layer_layer0(temp_group)

    temp_group.explode

    # p "exploded #{edges} lines" unless (edges.empty?)

  end



  #creates a temp group and draws edges that have 1 face in it

  # then explodes the temp group to hopefully break the existing face so the edges have 2 faces

  def explode_faces_if_edges_only_face(instance)

    entities = get_entities(instance)

    face_loops = entities.grep(Sketchup::Edge){ |e| e.faces[0].outer_loop if (e.faces.length == 1) }.compact

    face_loops = [] if (face_loops.nil?)

    face_loops.uniq!

    temp_group = entities.add_group

    face_loops.each{ |loop|

      begin

      temp_group.entities.add_face(loop.vertices)

      rescue ArgumentError => e

        #Points are not planar

      end

    }

    explode_instance(temp_group, instance)

    # p "exploded #{face_loops} faces" unless (face_loops.empty?)

  end



  # explodes an instance of the intersection group into the desired instance.

  # transformation is applied to the inner entities to make the explode work like the instance is the active entities.

  def explode_intersection_lines_into_instance(intersection_group, instance)

    trans = instance.transformation

    entities = get_entities(instance)

    temp_group = entities.add_instance(intersection_group.entities.parent, trans.inverse) #add the intersection lines to the current instance

    explode_instance(temp_group, instance)

  end



  # explodes the group but first scales it based on its parents transformation

  # WARNING: if the parent_instance is somehow the active entities the scaling probably shouldn't be applied!

  def explode_instance(explode_group, parent_instance)

    trans = parent_instance.transformation

    scaler = InstanceScaler.new(parent_instance)

    scaler.transform_entities(trans) #apply the instance outer transform to its inner entities making the explode work as if the instance was the active entities

    make_instance_layer_layer0(explode_group)

    explode_group.explode

    scaler.transform_entities(trans.inverse) #undo the scaling that we did on the inner entities

  end



  #assumes that the api call is within a start/commit operation so these definitions are removed when the operation is commited
  #您考虑的太周到了👍🐂👆
  def remove_component_instance_and_definition(instance)
    puts "remove_component_instance_and_definition  #{instance.name} #{instance.definition.name} 开始啦"
    puts "instance.deleted?:#{instance.deleted?}"
    if (instance.valid?)
      definition = instance.definition
      if (definition.group?)
        instance.erase! #just erase the group dont create an empty group
      else
        remove_component_definition(definition)
      end
    end
    # puts "remove_component_instance_and_definition  完成啦"
  end

  def remove_component_definition(definition)
    if (definition.valid?)
      if (definition.group?)
        definition.instances.each{ |group| group.erase! if (group.valid?) }
      else
        definition.entities.to_a.each{ |e| e.erase! if (e.valid?) }
      end
    end
  end

  # changes the instance layer to layer0.
  # standard in sketchup is to create everything on layer0
  def make_instance_layer_layer0(instance)
    model = Sketchup.active_model
    layers = model.layers
    layer = layers.add('Layer0')
    instance.layer = layer
  end

  # triangulates all faces of an instance, this should make all faces have the correct plane
  def triangulate(instances)

    instances.each{ |instance|

      new_faces = []

      delete_face = []

      entities = get_entities(instance)



      entities.grep(Sketchup::Face){ |f|

        if (!are_faces_coplanar([f]))

          polymesh = triangulate_face(f)

          if (!polymesh.nil?)

            new_faces.push([polymesh, f.material, f.back_material])

            delete_face.push(f)

          end

        end

      }



      delete_face.each{ |face|

        face.edges.each{ |edge|

          edge.soft = false #so they dont disappear

        }

        face.erase!

      }



      new_faces.each { |f_array|

        entities.add_faces_from_mesh(f_array[0], Geom::PolygonMesh::SOFTEN_BASED_ON_INDEX, f_array[1], f_array[2])

      }

    }

  end

  # returns the polymesh of the face or nil
  def triangulate_face(face)

    loops = face.loops



    if (loops.length == 1 and loops[0].convex?)

      verts = face.vertices

      num_verts = verts.length

      if (num_verts == 3)

        polymesh = nil

      elsif (num_verts == 4)

        polymesh = face.mesh

      else

        polymesh = face.mesh

      end

    else

      polymesh = face.mesh

    end



    return polymesh

  end

  # creates a new unique layer
  def create_unique_layer

    unique_layer_name = Sketchup.active_model.layers.unique_name

    unique_layer = Sketchup.active_model.layers.add(unique_layer_name)

    unique_layer

  end

  # creates a new unique layer that the copy of the group will be assigned

  # this copy of the group will be in the model entities

  # @param [Group] instance, that will be copied

  # @returns [Group] copy of the group sent

  def create_copy_of_group_on_unique_layer(instance)

    definition = (instance.class == Sketchup::Group) ? instance.entities.parent : instance.definition

    model = Sketchup.active_model

    trans = instance.transformation

    #todo maybe make a copy without inner instance?

    group_copy = model.entities.add_instance(definition, trans).make_unique

    unique_layer = create_unique_layer

    group_copy.layer = unique_layer

    entities = get_entities(group_copy)

    #TODO only the faces are ment to be hit

    entities.grep(Sketchup::Face){ |face| face.layer = unique_layer }

    group_copy

  end
  # changes all but the sent layers visibility to false

  # @param [String] visible_layer_name, name of the layer that will remain visible

  # @returns [Hash] a hash indexed by layer names containing all the layers original visibility

  def hide_all_but_layer(visible_layer_name)

    layers_backup_visibility = Hash.new

    layers = Sketchup.active_model.layers

    layers.each { |layer|

      layers_backup_visibility[layer.name] = layer.visible?

      layer.visible = false

    }

    layers[visible_layer_name].visible = true

    return layers_backup_visibility

  end

  # sets layer visibilities based on hash

  # @param [Hash] layers_backup_visibility, is a hash with layer names as indexes and visibility (true/false) as value

  def reset_layer_visibility(layers_backup_visibility)

    layers = Sketchup.active_model.layers

    layers.each { |layer|

      layer.visible = layers_backup_visibility[layer.name]

    }

  end

  # checks if instance 1 entities are inside instance 2
  # @param [Boolean] keep_inside, true if the inside faces will be kept, false otherwise
  # this is needed to determine if we should follow the face normal (keep outside) or the reverse (keep inside)
  # because which way we raytest will affect shared faces classification
  def is_instance_entity_inside_other_instance(instance1, instance2, keep_inside = false)

    entities1 = get_entities(instance1)



    #todo attempt to prevent clipping by removing the outer scaling that might be on instance 2

    # this is done by temporarily applying its inverse transformation to both instances

    #todo instead of just using instance 2 transformation should we just extract the scaling part of it?

    t2 = instance2.transformation #used to temporarily remove outer scaling from instance 2

    instance2.transform!(t2.inverse) #todo remove outer scaling inst2

    instance1.transform!(t2.inverse) #todo make inst1 still positioned in regards to inst 2



    #loop through all faces and edges in the first entities list to determine

    # if they are inside the second entities object

    # since we are checking against the second object make sure it is the only thing that raytest will hit

    model = Sketchup.active_model

    visible_instance = create_copy_of_group_on_unique_layer(instance2)

    unique_layer = visible_instance.layer

    old_active_layer = model.active_layer

    model.active_layer = unique_layer

    layers_backup_visibility = hide_all_but_layer(unique_layer.name)



    inside_instance2 = []  #will keep track of all faces and edges from first instance that are inside the second instance

    t1 = instance1.transformation

    entities1.grep(Sketchup::Face) { |face|

      vec = face.normal

      vec.reverse! if (keep_inside)

      vec.transform!(t1)   #this will be the ray direction that we will cast to test for boundaries

      mesh = face.mesh

      mesh.transform!(t1)

      polys = mesh.polygons



      #use the largest poly

      poly = nil

      polys.each{ |p|

        if (poly.nil?)

          poly = p

        elsif (area_of_triangle(mesh, p) > area_of_triangle(mesh, poly))

          poly = p

        end

      }



      p1 = mesh.point_at(poly[0])

      p2 = mesh.point_at(poly[1])

      p3 = mesh.point_at(poly[2])

      cent = centroid([p1, p2, p3])



      inmeshes = inmesh?(cent, vec)  #returns an array of component instances that this point is inside

      if (inmeshes.include?(visible_instance))

        inside_instance2.push(face)

      end

    }

    model.active_layer = old_active_layer

    reset_layer_visibility(layers_backup_visibility)

    remove_component_instance_and_definition(visible_instance)

    model.layers.remove(unique_layer)



    instance2.transform!(t2) #todo re-apply outer scaling inst2

    instance1.transform!(t2) #todo remove inst2 transform from inst1





    return inside_instance2

  end

  # @param [Mesh] mesh, that the poly is referring to
  # @param [Array] poly, poly made of 3 points that make a triangle
  def area_of_triangle(mesh, poly)

    p1 = mesh.point_at(poly[0])

    p2 = mesh.point_at(poly[1])

    p3 = mesh.point_at(poly[2])



    #find the length of each side of the triangle

    s1 = p1.distance(p2)

    s2 = p2.distance(p3)

    s3 = p3.distance(p1)



    hp = (s1 + s2 + s3) / 2.0 #half perimeter



    begin

      area = Math.sqrt(hp * (hp - s1) * (hp - s2) * (hp - s3))

    rescue

      area = -1

    end



    return area

  end
  # method to determine if a given face or edge for one mesh is inside another mesh
  # @param [Array] reverse_normal, if array has 1 value then use for both otherwise should be 2 item array
  def classify(inst1, inst2, reverse_normal = [false])

    #scale so that faces are hopefully far enough apart that raytest works as expected!

    scaler = InstancesScaler.new([inst1, inst2])

    scale_array = scaler.scale_instances_based_on_theoretical_SU_limit



    #now we have to test if edges and faces are inside either of the groups or components

    inst1_inside_inst2 = is_instance_entity_inside_other_instance(inst1, inst2, reverse_normal.first)

    inst2_inside_inst1 = is_instance_entity_inside_other_instance(inst2, inst1, reverse_normal.last) #todo enable this



    scaler.reverse_scale_instances(scale_array) #sends ratio used and then point used



    return [inst1_inside_inst2, inst2_inside_inst1]  #return the classification arrays

  end
  # explodes the second instance into the first combining the 2 objects
  # objects are first scaled up to reduce amount of small geometry lost during the explode
  # reverses the faces depending on operation (end result should have all faces pointing outward)
  def combine(inst1, inst2, reverse_faces)

    scaler = InstancesScaler.new([inst1, inst2])

    scale_array = scaler.scale_instances_based_on_small_edge #todo prevents small geometry from being removed during the explode



    t1 = inst1.transformation



    #now add a new copy of the second instance and reverse all of the faces

    copied_inst = copy_remove_nested_instance(inst2, inst1, false) #add an instance of the second group/component so that we can explode it to create the new faces

    copied_inst.transform!(t1.inverse)

    c_ents = get_entities(copied_inst)

    c_ents.grep(Sketchup::Face) { |e| e.reverse! } if (reverse_faces)



    c_definition = copied_inst.definition

    explode_instance(copied_inst, inst1) #todo might not be needed? (is needed for intersection method though)

    remove_component_definition(c_definition)



    scaler.reverse_scale_instances(scale_array) #todo is this needed

  end

  def centroid(pts)  #pts is an array of point3d objects

    num = 0

    sumx = 0.0

    sumy = 0.0

    sumz = 0.0

    pts.each { |p|

      sumx += p.x

      sumy += p.y

      sumz += p.z

      num += 1

    }

    num = num.to_f

    return Geom::Point3d.new((sumx / num), (sumy / num), (sumz / num))

  end

end

end

end

