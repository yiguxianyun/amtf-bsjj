module AMTF
require 'sketchup.rb'
module Boolean
  class UnionOperation < A布尔操作_基类
    def initialize(first, second, recurse_nested)
      if (recurse_nested)
        UnionSingleInstanceOperation.new(first) # this should make instance 1 not nested
      end
      super
    end

    def set_operation_specific_variables

      @remove_second_instance = true

      @reverse_faces = false

      @remove_inside_faces = [true, true]

    end



    def bool_operation(first, second)

      if (instance_has_surface?(second))

        bool_tools_operation(first, second) #removes the second entity from the first

      end

    end



    # do not reverse the normals

    def get_reverse_normal_array

      return [false, false]

    end



  end



end





end

