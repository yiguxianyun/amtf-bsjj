module AMTF
require 'sketchup.rb'
module Boolean
  # creates new intersection object
  # then subtracts it from object 1 and object 2
  class SplitOperation
    include A布尔操作_mixin
    attr_reader :changed_instances
    def initialize(first, second, recurse)
      #do not alter the originals yet

      remove_inner_instances = false

      copied1 = copy_instance(first, remove_inner_instances)

      copied2 = copy_instance(second, remove_inner_instances)

      copied2_def = copied2.definition

      #create new object based on copied1

      IntersectionOperation.new(copied1, copied2, recurse)

      #copied1 should now be the intersection of the 2 solids

      remove_component_definition(copied2_def) #remove duplicate definition



      #subtract the intersection from object 1 but do not remove the intersection (trim)

      temp_copy_1 = copy_instance(first, remove_inner_instances)

      temp_copy_2 = copy_instance(second, remove_inner_instances)

      temp_copy_2_def = temp_copy_2.definition

      DifferenceOperation.new(first, temp_copy_2, recurse)

      remove_component_definition(temp_copy_2_def) #remove duplicate definition

      UnionSingleInstanceOperation.new(temp_copy_1) if (recurse)# this should make instance 1 not nested

      temp_copy_1_def = temp_copy_1.definition

      DifferenceOperation.new(second, temp_copy_1, recurse)

      remove_component_definition(temp_copy_1_def)  #remove duplicate definition



      #everything should be done at this point!

      @changed_instances =  [copied1, first, second] #array of instance that were modified from the operation!

      # p "changed instances: #{@changed_instances}"

    end



  end



end

end

