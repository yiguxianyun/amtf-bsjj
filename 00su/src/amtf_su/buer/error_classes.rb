
module AMTF

module Boolean



  class BooleanSolidException < RuntimeError; end



  class FirstSolidException < RuntimeError; end

  class SecondSolidException < RuntimeError; end

  class FirstNestedException < RuntimeError; end

  class SecondNestedException < RuntimeError; end



end

end

