
module AMTF

require 'sketchup.rb'

module Boolean



  class IntersectionOperation < A布尔操作_基类

    def set_operation_specific_variables

      @remove_second_instance = true

      @reverse_faces = false

      @remove_inside_faces = [false, false]

    end



    def bool_operation(first, second)

      if (instance_has_surface?(first) && instance_has_surface?(second))

        bool_tools_operation(first, second) #removes the second entity from the first

      else

        #the result should be nothing

        first.erase! if (first.valid?)

        second.erase! if (second.valid?)

      end

    end



    # reverse normal for both instances classification

    def get_reverse_normal_array

      return [true, true]

    end



  end



end

end

