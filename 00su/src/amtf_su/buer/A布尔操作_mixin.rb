# require File.join(__FILE__, '../A布尔操作_mixin2.rb')

module AMTF
module Boolean
  # MODULE = self
module A布尔操作_mixin
  T_D_POSITION_DICTIONARY = 'AF Boolean Position'
  T_D_POSITION_KEYS = {
      text: 'text_position',
      linear: 'linear_position',
      radial: 'radial_position'
  }
  SOFTEN_ANGLE = 30.0.degrees
  # include MODULE::Mixin
  include Boolean::A布尔操作_mixin2
  #softens edges of the object based on the angle (degrees)
  def soften(instance)
    ents = get_entities(instance)
    angle = SOFTEN_ANGLE
    edges = ents.grep(Sketchup::Edge)   #get all the edges inside the instance
    edges.each{ |edge|

      faces = edge.faces

      if (faces.length == 2)

        n1 = faces[0].normal

        n2 = faces[1].normal

        angle2 = n1.angle_between(n2)

        if (angle2 <= angle)

          edge.soft = true

          edge.smooth = true

        elsif (angle2 >= angle)

          edge.soft = false

          edge.smooth = false

        end

      else

        edge.soft = false

        edge.smooth = false

      end

    }

    return nil

  end

  #sets an attribute containing needed position information on each text/dimension
  def get_text_and_dimension_position
    Sketchup.active_model.entities.grep(Sketchup::Text){ |t| set_text_position_attribute(t) }

    Sketchup.active_model.entities.grep(Sketchup::DimensionLinear){ |l| set_linear_position_attribute(l) }

    Sketchup.active_model.entities.grep(Sketchup::DimensionRadial){ |r|  set_radial_position_attribute(r) }



    Sketchup.active_model.definitions.each{ |d|

      instance = d.instances.first

      if (!instance.nil? && instance.class != Sketchup::Image)

        ents = get_entities(d.instances.first)

        ents.grep(Sketchup::Text){ |t| set_text_position_attribute(t) }

        ents.grep(Sketchup::DimensionLinear){ |l| set_linear_position_attribute(l) }

        ents.grep(Sketchup::DimensionRadial){ |r| set_radial_position_attribute(r) }

      end

    }

  end

  # if our dictionary exists reset the position
  def reset_text_and_dimension_position
    Sketchup.active_model.entities.grep(Sketchup::Text){ |t| set_text_position_based_on_attribute(t) }
    Sketchup.active_model.entities.grep(Sketchup::DimensionLinear){ |l| set_linear_position_based_on_attribute(l) }
    Sketchup.active_model.entities.grep(Sketchup::DimensionRadial){ |r|  set_radial_position_based_on_attribute(r) }
    Sketchup.active_model.definitions.each{ |d|
      instance = d.instances.first
      if (!instance.nil? && instance.class != Sketchup::Image)
        ents = get_entities(d.instances.first)
        ents.grep(Sketchup::Text){ |t| set_text_position_based_on_attribute(t) }
        ents.grep(Sketchup::DimensionLinear){ |l| set_linear_position_based_on_attribute(l) }
        ents.grep(Sketchup::DimensionRadial){ |r| set_radial_position_based_on_attribute(r) }
      end
    }
  end

  def set_text_position_attribute(text)
    text.set_attribute(T_D_POSITION_DICTIONARY, T_D_POSITION_KEYS[:text], text.point.to_a)
  end

  def set_linear_position_attribute(linear)
    linear.set_attribute(T_D_POSITION_DICTIONARY, T_D_POSITION_KEYS[:linear], [linear.start[1].to_a, linear.end[1].to_a])
  end

  def set_radial_position_attribute(radial)
    radial.set_attribute(T_D_POSITION_DICTIONARY, T_D_POSITION_KEYS[:radial], radial.leader_break_point.to_a)
  end

  def set_text_position_based_on_attribute(text)
    point_array = text.get_attribute(T_D_POSITION_DICTIONARY, T_D_POSITION_KEYS[:text])
    if (point_array && point_array.length == 3)
      point = Geom::Point3d.new(point_array)
      text.point = point
    end
  end

  def set_linear_position_based_on_attribute(linear)
    position_array = linear.get_attribute(T_D_POSITION_DICTIONARY, T_D_POSITION_KEYS[:linear])
    if (position_array)
      start_point = Geom::Point3d.new(position_array.first)
      end_point = Geom::Point3d.new(position_array.last)
      linear.start = start_point
      linear.end = end_point
    end
  end

  def set_radial_position_based_on_attribute(radial)
    reference = radial.arc_curve
    if (reference.nil?)
      radial.erase!
    end
  end
end
end
end

