# require File.join(__FILE__, '../A布尔操作_基类.rb')

module AMTF
require 'sketchup.rb'
module Boolean
  class DifferenceOperation < A布尔操作_基类
    def set_operation_specific_variables
      @remove_second_instance = true
      @reverse_faces = true
      @remove_inside_faces = [true, false] #remove inst1 inner faces, and inst2 outer faces
    end

    def bool_operation(first, second)
      if (instance_has_surface?(first) && instance_has_surface?(second))
        bool_tools_operation(first, second) #removes the second entity from the first
      end
    end

    # reverse normal for instance 1 but not instance 2 classification
    def get_reverse_normal_array
      return [true, false]
    end
  end  #end class

  class A修剪操作 < DifferenceOperation
    # def initialize(first, second, recurse_nested)
    #   return super
    # end

    def set_operation_specific_variables
      super
      @remove_second_instance = false #overwritten so the second instance is not removed
      @隐藏工具对象=true
    end

    #perform subtraction but do not delete or alter the second solid
    def bool_operation(first, second)
      copied2 = copy_instance(second)
      super(first, copied2)
      remove_component_instance_and_definition(copied2)
    end
  end
end
end

