require File.join(__FILE__, '../A布尔操作_mixin.rb')
module AMTF
module Boolean
  include A布尔操作_mixin
  extend self
  RECURSE_DEFAULT = false
#TODO remove any object scaling
  # performs the union operation
  # @param [Boolean] recurse, true if objects should be recurse
  def self.union(first, second, recurse = RECURSE_DEFAULT)

    self.make_groups_uniq([first, second])

    UnionOperation.new(first, second, recurse)

  end
  # merges the instance into a single level (no inner instances)
  def self.self_union(instance)

    self.make_groups_uniq([instance])

    UnionSingleInstanceOperation.new(instance)

  end
  # performs the intersection operation
  # @param [Boolean] recurse, true if objects should be recurse
  def self.intersection(first, second, recurse = RECURSE_DEFAULT)

    self.make_groups_uniq([first, second])

    return IntersectionOperation.new(first, second, recurse)

  end
  # performs the difference operation
  # @param [Boolean] recurse, true if objects should be recurse
  def self.difference(first, second, recurse = RECURSE_DEFAULT)

    self.make_groups_uniq([first, second])

    DifferenceOperation.new(first, second, recurse)

  end

  # performs the trim operation
  # @param [Boolean] recurse, true if objects should be recurse
  def self.trim(first, second, recurse = RECURSE_DEFAULT)
    self.make_groups_uniq([first, second])
    puts "self.make_groups_uniq([first, second]) 执行完了！"
    # amtf.nil
    kk= A修剪操作.new(first, second, recurse)
    puts "A修剪操作.new(first, second, recurse) 执行完了！"
    puts "kk:#{kk}"
    return kk
    # amtf.nil
  end

  # performs the split operation
  # creates a new group for intersection, then modifies group 1 and group 2 using subtraction
  # @param [Boolean] recurse, true if objects should be recurse
  def self.split(first, second, recurse = RECURSE_DEFAULT)
    self.make_groups_uniq([first, second])
    SplitOperation.new(first, second, recurse)
  end


  #for now only checking if an instance has an edge smaller than SU_EDGE_CLEAN_UP_SIZE
  # returns false if result has small edges, true if no small edge detected!
  def result_is_valid?(instances)
    instances.each{ |instance|
      if (instance.valid?)
        entities = (instance.class == Sketchup::Group) ? instance.entities : instance.definition.entities
        entities.grep(Sketchup::Edge){ |e|
          if (e.length.to_f < SU_EDGE_CLEAN_UP_SIZE) #only want the actual edge length no visible edge length
            return false
          end
        }
        entities.grep(Sketchup::Face){ |f| #check for really small faces

          if (f.area.to_f < SU_FACE_CLEAN_UP_SIZE)

            return false

          end

        }

        entities.grep(Sketchup::Group){ |instance|

          result = result_is_valid?([instance])

          return result if (result == false)

        }

        entities.grep(Sketchup::ComponentInstance){ |instance|

          result = result_is_valid?([instance])

          return result if (result == false)

        }

      end

    }

    return true

  end



  # uses scaling to prevent sketchup from removing small edges

  def avoid_tiny_edges(instances)

    instances.each{ |instance|

      #check for inner instances

      entities = get_entities(instance)

      entities.grep(Sketchup::Group){ |inner_instance| avoid_tiny_edges(inner_instance) }

      entities.grep(Sketchup::ComponentInstance){ |inner_instance| avoid_tiny_edges(inner_instance) }



      #need to apply to all instances of a component!

      if (instance.class == Sketchup::ComponentInstance)

        #get all instances of this component

        instances = instance.definition.instances

        scale_values = nil

        instances.each{ |comp_instance|

          scaler = InstanceScaler.new(comp_instance)

          if (scale_values.nil?)

            scale_values = scaler.scale_entities_up_and_scale_down_instance

          else #inner entities are scaled up so scale down outer instance

            point = scaler.get_center_point

            scaler.scale_down_modified_instance(scale_values[0], point) #scale down the instance

          end

        }

      else

        scaler = InstanceScaler.new(instance)

        scaler.scale_entities_up_and_scale_down_instance

      end

    }

  end



  # return true if instance are solid (second can be nil)

  # otherwise returns string combination of warning messages

  def self.validate_operands(first, second = nil)
    warning = nil
    solid1 = self.validate_mesh(first)
    unless (solid1 == true)
      string = '第一个选定对象或其内部对象之一不是有效的实体。'
      warning = (warning.nil?) ? string : warning + "\n#{string}"
    end
    if (!second.nil?)
      solid2 = self.validate_mesh(second)
      unless (solid2 == true)
        string = '第二个选定对象或其内部对象之一不是有效的实体。'
        warning = (warning.nil?) ? string : warning + "\n#{string}"
      end
    end
    if (warning.nil?)
      return true
    else
      raise(BooleanSolidException, warning)
    end
  end

  # mesh is valid if it is solid and all of its nested groups are solid. (solid meaning all edges have 2 faces)
  # @param [Group] instance, group/component that needs to be validated
  # @returns [Boolean] true if each level of instance is a solid (each edge has 2 faces)
  def self.validate_mesh(instance)
    entities = (instance.class == Sketchup::Group) ? instance.entities : instance.definition.entities
    solid = true
    entities.grep(Sketchup::Edge) { |e|
      num_faces = e.faces.length
      #todo cannot use manifold since nested groups return false
      if (num_faces != 2) #this would return true for a flat face if it is duplicated not ideal!!
        solid = false
        return solid
      end
    }
    entities.grep(Sketchup::Group){ |g|
      solid = self.validate_mesh(g)
      return solid unless (solid)
    }
    entities.grep(Sketchup::ComponentInstance){ |c|

      solid = self.validate_mesh(c)

      return solid unless (solid)

    }



    return solid

  end



end

end

