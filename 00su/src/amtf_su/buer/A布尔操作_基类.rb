require File.join(__FILE__, '../A布尔操作_mixin.rb')
module AMTF
module Boolean
  class A布尔操作_基类
    include A布尔操作_mixin
    attr_reader :changed_instances
    TIME_FUNCTIONS = true
    # performs boolean operation using 2 instances sent
    # @param [Boolean] recurse_nested, true by default nested instance will be considered for first; false only consider the top level
    def initialize(first, second, recurse_nested)
      @remove_second_instance = false #this gets overwritten
      @reverse_faces = [] #this gets overwritten depending on operation type
      @remove_inside_faces = [] #This gets overwritten depenging on operation type
      @changed_instances = []
      set_operation_specific_variables
      get_text_and_dimension_position
      @base_instance = first
      # puts "initialize  到这了"
      # amtf.nil
      if (recurse_nested)
        puts "recurse_nested  #{recurse_nested}"
        copied2 = copy_instance(second, false) #make copy but keep inner instances
        UnionSingleInstanceOperation.new(copied2) # this should make instance 2 not nested
        # amtf.nil
        base_trans = Geom::Transformation.new
        next_trans = first.transformation
        perform_boolean_for_nested(first, copied2, base_trans, next_trans)
        # amtf.nil
        remove_component_instance_and_definition(copied2)
        second.erase! if (@remove_second_instance)

      else
        puts "recurse_nested  #{recurse_nested}"
        # put "perform_boolean(first, second)  开始"
        perform_boolean(first, second)
        # put "perform_boolean(first, second)  结束"
        # amtf.nil
        changed_instances.push(first)
      end
      reset_text_and_dimension_position
      @changed_instances.push(first)
      @changed_instances.push(second) if (second.valid?)
      second.visible=false if (second.valid? && @隐藏工具对象)

      puts "return @changed_instances:#{@changed_instances}"
      return @changed_instances #array of instance that were modified by the operation
    end
    #perform the boolean for each internal visible instance

    def perform_boolean_for_nested(instance, second, trans, next_trans)
      instance.make_unique if (instance.class == Sketchup::Group) #prevent sketchup from treating groups like components
      # 防止sketchup将组视为组件👆
      if (instance.visible?)
        ents1 = (instance.class == Sketchup::Group) ? instance.entities : instance.definition.entities
        temp_trans = trans * next_trans
        groups = ents1.grep(Sketchup::Group)
        # p "there are #{groups.length} nested groups"
        groups.each{ |group|
          temp_next_trans = group.transformation
          perform_boolean_for_nested(group, second, temp_trans, temp_next_trans)
        }
        # amtf.nil
        comps = ents1.grep(Sketchup::ComponentInstance)
        # p "there are #{comps.length} nested components"
        comps.each{ |instance|
          temp_next_trans = instance.transformation
          perform_boolean_for_nested(instance, second, temp_trans, temp_next_trans)
        }
        # amtf.nil

        unless (ents1.grep(Sketchup::Face).empty?)
          copied2 = copy_remove_nested_instance(second, instance)
          copied2_def = copied2.definition
          copied2.transform!(trans.inverse)
          # instance.material = 'red'
          perform_boolean(instance, copied2)
          # amtf.nil

          if (copied2.valid?)
            remove_component_instance_and_definition(copied2)
          else
            remove_component_definition(copied2_def)
          end
        else
          # p "#{instance} is empty!!!!!!!!!!!!!!!!!!!!!!"
        end
        # amtf.nil

      end
    end

    #this function is overwritten for each specific boolean function

    def set_operation_specific_variables() end
    #to be overwritten for different boolean operations

    def bool_operation(first, second) end
    #performs any of the boolean operations
    # copy was added to prevent bug splat when undoing boolean operation after second was removed
    # 添加副本是为了防止在删除第二个布尔运算后撤消布尔运算时出现错误飞溅
    def perform_boolean(first, second)
      puts "copy_instance(second)开始"
      copy = copy_instance(second) #prevent bugsplat on undo
      puts "copy_instance(second)结束"
      # amtf.nil
      bool_operation(first, copy)
      # amtf.nil
      puts "copy.valid?:#{copy.valid?}"
      remove_component_instance_and_definition(copy) if (copy.valid?)
      # amtf.nil
      puts "@remove_second_instance:#{@remove_second_instance}"
      second.erase! if (second.valid? && @remove_second_instance)
      puts "@隐藏工具对象:#{@隐藏工具对象}"
      puts "copy.valid?:#{copy.valid?}"
      puts "copy.deleted?:#{copy.deleted?}"
      # second.visible=false if (@隐藏工具对象)
      # copy.visible=false if (@隐藏工具对象)
      # puts "你咋不更新模型树呢！"因为没有commit_operation
    end

    #this will be overwritten for each tool
    def get_reverse_normal_array() end
    #this will be overwritten for each tool
    # @param [Group] inst1 group or component being intersected
    # @param [Array] inst1_inner_faces is all the inst1 faces that are inside inst2
    # @param [Group] inst2 group or component being intersected
    # @param [Array] inst2_inner_faces is all the inst2 faces that are inside inst1

    # @param [Instance] base_inst, inst1 should normally be equal to this. However when it is not then if inst1 will be changed we need to make it unique.

    # this prevents unwanted behaviour for nested component instances.

    def delete_unwanted_faces(inst1, inst1_inner_faces, inst2, inst2_inner_faces, base_inst)

      ents1 = (inst1.class == Sketchup::Group) ? inst1.entities : inst1.definition.entities

      ents2 = (inst2.class == Sketchup::Group) ? inst2.entities : inst2.definition.entities



      delete1 = []

      delete2 = []



      # remove faces inside or outside based on instance variable array

      loop do

        restart_with_new_entities = false

        ents1.grep(Sketchup::Face) { |face|

          if (inst1_inner_faces.include?(face) == @remove_inside_faces.first)

            if ((inst1 != base_inst) && !is_instance_uniq?(inst1))

              inst1.make_unique #inner component is about to be changed and is not uniq

              ents1 = get_entities(inst1)

              restart_with_new_entities = true

              break

            end

            # make edges hard so they dont disappear

            face.edges.each{ |e|

              e.soft = false

              e.smooth = false

            }

            # face.material = 'red' #todo remove this

            delete1.push(face) #todo enable this

          end

        }

        break unless (restart_with_new_entities) #redo loop if inst1 was made uniq

      end



      ents2.grep(Sketchup::Face) { |face|

        if (inst2_inner_faces.include?(face) == @remove_inside_faces.last)

          # make edges hard so they dont disappear

          face.edges.each{ |e|

            e.soft = false

            e.smooth = false

          }

          delete2.push(face) #todo enable this

          # face.material = 'red' #todo remove this

        end

      }



      ents1.erase_entities(delete1)  #now erase all of the entities that were found inside the other mesh

      ents2.erase_entities(delete2)  #now erase all of the entities that were found inside the other mesh



      #now clean up edges without any faces

      clean_up_edges_without_faces(ents1)

      clean_up_edges_without_faces(ents2)

    end



    #removes any edges found without a face

    def clean_up_edges_without_faces(entities)

      edges = entities.grep(Sketchup::Edge)

      edges = edges.collect { |e| e if (e.valid? && (e.faces.length == 0)) }.compact

      entities.erase_entities(edges) unless (edges.nil?)

    end

    # @param [Group] first, primary solid entity
    # @param [Group] second, the solid entity to remove from the primary
    # covers the main steps of any boolean operation
    # 涵盖了任何布尔运算的主要步骤
    def bool_tools_operation(first, second)
      #WARNING Sketchup will remove edges smaller than .001 inches
      # to avoid this we need to do it first
      # start_time = Time.now if (TIME_FUNCTIONS)
      intersect_meshes(first, second, @base_instance) #the first item can be made uniq so we need to update the reference
      # end_time = Time.now if (TIME_FUNCTIONS)
      # p "Intersecting the objects took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)
      reverse_normal = get_reverse_normal_array #tells raytest which way to project for classifying the faces
      # start_time = Time.now if (TIME_FUNCTIONS)
      args = classify(first, second, reverse_normal) #needed function
      # end_time = Time.now if (TIME_FUNCTIONS)
      # p "Classifying faces took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)
      # start_time = Time.now if (TIME_FUNCTIONS)
      delete_unwanted_faces(first, args[0], second, args[1], @base_instance) #the first item can be made uniq so we need to update the reference
      # end_time = Time.now if (TIME_FUNCTIONS)
      # p "Deleting extra faces took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)

      # start_time = Time.now if (TIME_FUNCTIONS)
      combine(first, second, @reverse_faces) #needed function
      # end_time = Time.now if (TIME_FUNCTIONS)
      # p "Combining objects took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)
      if (!solid?(first))
        # p "object is not solid!" #todo remove
        # start_time = Time.now if (TIME_FUNCTIONS)
        add_faces_to_complete_solid(first) #needed function
        # end_time = Time.now if (TIME_FUNCTIONS)
        # p "Adding faces to complete solid took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)
      end
      # start_time = Time.now if (TIME_FUNCTIONS)
      remove_free_edges(first) #needed function
      # end_time = Time.now if (TIME_FUNCTIONS)
      # p "Removing free edges took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)
      # start_time = Time.now if (TIME_FUNCTIONS)
      result = delete_coplanar_edges(first) #needed function
      # end_time = Time.now if (TIME_FUNCTIONS)
      # p "Removing coplanar edges took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)
      if (result && !solid?(first)) #removing an edge might remove a face!
        # p "object is not solid!" #todo remove
        # start_time = Time.now if (TIME_FUNCTIONS)
        add_faces_to_complete_solid(first) #needed function
        # end_time = Time.now if (TIME_FUNCTIONS)
        # p "Adding faces to complete solid took #{end_time - start_time} seconds." if (TIME_FUNCTIONS)
      end
      soften(first)
    end

  end  #end class



end

end

