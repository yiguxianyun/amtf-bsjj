
module AMTF
require 'sketchup.rb'
module Boolean
  class UnionSingleInstanceOperation
    include A布尔操作_mixin
    attr_reader :changed_instances
    #Unions the selected object (instance) with any inner instances it might have so when complete a single instance should exist with a solid and no nested instances
    #将选定的对象（实例）与它可能具有的任何内部实例联合，因此当完成时，单个实例应该存在一个实体实例，而不存在嵌套实例
    def initialize(instance)
      @new_component_definitions = []

      @old_to_new_definition = Hash.new

      get_text_and_dimension_position

      #Union all inner instances with the base instance

      instance.make_unique if (instance.class == Sketchup::Group)

      @base_instance = instance

      @base_trans = @base_instance.transformation

      make_inner_instances_uniq(instance)

      perform_union_for_nested

      remove_new_definitions

      remove_inner_instances_from_base

      reset_text_and_dimension_position

      soften(instance)

      #everything should be done at this point!

      @changed_instances = [@base_instance] #should be only modified instance

    end



    def make_inner_instances_uniq(instance)

      make_inners_unique(instance)

      if (instance.visible?) #instance has to be visible

        ents1 = get_entities(instance)



        groups = ents1.grep(Sketchup::Group)

        groups.each{ |group| make_inner_instances_uniq(group) }



        comps = ents1.grep(Sketchup::ComponentInstance)

        comps.each{ |instance| make_inner_instances_uniq(instance) }

      end

    end



    #any internal instance needs to be unique since ruby treats them like components some times

    def make_inners_unique(instance)

      unless ((instance == @base_instance) || @new_component_definitions.include?(instance.definition))

        old_def = instance.definition

        if (@old_to_new_definition.has_key?(old_def) && !old_def.group?) #has a new definition been made

          instance.definition = @old_to_new_definition[old_def] #set to new definition

        else

          instance.make_unique

          new_def = instance.definition

          @new_component_definitions.push(new_def) #list of definitions that need to be cleared

          @old_to_new_definition[old_def] = new_def

        end

      end

    end



    def perform_union_for_nested

      base_ents = get_entities(@base_instance)

      base_ents.grep(Sketchup::Group){ |g|

        if (g.visible?) #instance has to be visible

          UnionSingleInstanceOperation.new(g)

          union_nested_instance_with_outer(g)

        end

      }

      base_ents.grep(Sketchup::ComponentInstance){ |c|

        if (c.visible?) #instance has to be visible

          UnionSingleInstanceOperation.new(c)

          union_nested_instance_with_outer(c)

        end

      }

    end



    #nested instances should be a single level and not also include nested instances

    def union_nested_instance_with_outer(instance)

      copied = copy_remove_nested_instance(instance, @base_instance)

      copied.transform!(@base_instance.transformation)

      copied_def = copied.definition

      recurse = false

      UnionOperation.new(@base_instance, copied, recurse) #adds the instance to the base instance

      remove_component_definition(copied_def) #remove duplicate definition

    end



    # removes all internal groups and components

    def remove_inner_instances_from_base

      base_entities = get_entities(@base_instance)

      base_entities.grep(Sketchup::Group){ |g| g.erase! } #remove any internal groups

      base_entities.grep(Sketchup::ComponentInstance){ |c| c.erase! } #remove any internal components but not their definitions

    end



    # removes all internal groups and components

    def remove_new_definitions

      @new_component_definitions.each{ |definition|

        remove_component_definition(definition)

      }

    end



  end
end
end
