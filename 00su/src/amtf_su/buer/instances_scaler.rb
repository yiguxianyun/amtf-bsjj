
module AMTF

require 'sketchup.rb'

module Boolean



  # Takes an array of instances that will be able to be scaled and un-scaled

  class InstancesScaler



    #TODO can be increased probably more. Might want to push to a max or do heavy testing to find a max

    # todo if an accepted max is found should probably apply it as a cap to all scaling functions

    SU_BOUNDING_BOX_DIMENSION_LIMIT = 50.mile.to_f #setting theoretical cap at approx 16 miles



    def initialize(instance_array)

      @instances = instance_array

    end



    # @returns transformation used or nil

    def scale_instances_based_on_small_edge #todo need to account for group scaling since explode will use combination

      scale = small_edge_scale_ratio

      area_scale = face_area_scale_ratio

      if (!area_scale.nil?)

        scale = area_scale if (scale.nil? || (area_scale > scale))

      end

      point = scale_instance_if_ratio_set(scale)



      return [scale, point]

    end



    #used for classification

    def scale_instances_based_on_theoretical_SU_limit

      scale = get_scale_based_on_bounds

      point = scale_instance_if_ratio_set(scale)



      return [scale, point]

    end



    # @param [Number] ratio, the scale ratio used for the original scaling

    # @param [Point3d] point, the center point used for the original scaling

    def reverse_scale_instances(scale_array)#ratio, point)

      ratio = scale_array[0]

      point = scale_array[1]

      if (!ratio.nil?) #need to scale down if not nil

        @instances.each{ |instance|

          if (instance.valid?)

            scaler = InstanceScaler.new(instance)

            #todo attempting to see if capping scaling at 0.001 and 1000 stops face unclickable glitch

            #todo if it works push those numbers as large/small as works

            max_scale = 100000.to_f #tested on revolve model and this works

            remaining_ratio = ratio.to_f

            loop do

              temp_ratio = (remaining_ratio < max_scale) ? remaining_ratio : max_scale

              reverse_trans = Geom::Transformation.scaling(point.clone.transform!(instance.transformation.inverse), temp_ratio, temp_ratio, temp_ratio).inverse

              scaler.transform_entities(reverse_trans)

              remaining_ratio = (remaining_ratio / temp_ratio)

              break unless (remaining_ratio != 1.0)

            end

          end

        }

      end

    end



    ##### private functions ###########



    # @param [Array] instance_array, array of instances with edges

    # @returns [Number] the smallest edge length or SU_EDGE_CLEAN_UP_SIZE constant (which ever is smaller)

    def length_of_smallest_edge

      smallest_length = nil

      @instances.each{ |instance|

        scaler = InstanceScaler.new(instance)

        temp_length = scaler.length_of_smallest_edge

        smallest_length = temp_length if (smallest_length.nil? || (!temp_length.nil? && (temp_length < smallest_length)))

      }

      return smallest_length

    end



    # gets the total bounds then find the largest of its dimensions

    # scale is the ratio that makes the largest dimension equal to our theoretical cap

    def get_scale_based_on_bounds

      bounds = Geom::BoundingBox.new

      @instances.each{ |instance|

        bounds.add(instance.bounds)

      }

      #find the largest dimension

      largest_dimension = bounds.depth.to_f

      largest_dimension = bounds.height.to_f if (bounds.height.to_f > largest_dimension)

      largest_dimension = bounds.width.to_f if (bounds.width.to_f > largest_dimension)



      scale = SU_BOUNDING_BOX_DIMENSION_LIMIT / largest_dimension



      return scale

    end



    def get_average_face_area

      average_face_area = 0

      @instances.each{ |instance|

        scaler = InstanceScaler.new(instance)

        average_face_area += scaler.get_average_face_area

      }

      return (average_face_area / @instances.length.to_f)

    end



    def get_smallest_face_area

      smallest_face_area = nil

      @instances.each{ |instance|

        scaler = InstanceScaler.new(instance)

        temp_area = scaler.get_smallest_face_area

        smallest_face_area = temp_area if (smallest_face_area.nil? || (temp_area < smallest_face_area))

      }

      return smallest_face_area

    end



    # @returns ratio to scale or nil if no need to scale

    def small_edge_scale_ratio #todo used by intersection and combine currently

      #that means explode (can remove small edges!) and intersection (can create small edges?)

      length = length_of_smallest_edge.to_f

      if ((EXPLODE_MIN_EDGE_SIZE > length) && length != 0.0)

        scale = EXPLODE_MIN_EDGE_SIZE / length.to_f

      else

        scale = nil

      end

      return scale

    end



    # @returns nil or scale ratio

    def face_area_scale_ratio #used for combine and intersection (needs to be larger than SU limits since explode might make smaller edges/faces)

      area = get_smallest_face_area.to_f #new method using smallest

      if ((MIN_FACE_AREA > area) && (area != 0.0))

        scale = MIN_FACE_AREA / area

      else

        scale = nil

      end

      return scale

    end



    #returns the center point so scaling keeps the object near the origin

    def get_center_point

      total_bounds = Geom::BoundingBox.new

      @instances.each{ |instance|

        total_bounds.add(instance.bounds)

      }

      point = total_bounds.center #center of all instances



      return point #center of the instances bounds to keep the scaled object near the origin

    end



    # @param [Number] scale, the ratio to scale the instance or nil if no need to scale

    # @returns [Point3d] point used for the scaling or nil if no scaling done

    def scale_instance_if_ratio_set(scale)

      point = nil



      if (!scale.nil?) #need to scale up if not nil

        point = get_center_point #center of all instances

        @instances.each{ |instance|

          scaler = InstanceScaler.new(instance)

          temp_point = point.clone.transform!(instance.transformation.inverse)

          scaler.transform_entities_capped(temp_point, scale) #todo working and might have benefits!

          # trans = Geom::Transformation.scaling(temp_point, scale, scale, scale)

          # scaler.transform_entities(trans)

        }

      end



      return point

    end



  end



end

end

