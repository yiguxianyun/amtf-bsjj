
module AMTF

require 'sketchup.rb'

module Boolean



  MIN_FACE_AREA = 0.01 #min area for scaling functions



  REPAIR_MIN_EDGE_SIZE = 0.005.inch.to_f # need to scale larger than 0.001 for duplicate points and larger still to avoid not planar message



  EXPLODE_MIN_EDGE_SIZE = 10.inch.to_f #any edge existing or created by explode below 0.001 will be removed!!



  SU_EDGE_CLEAN_UP_SIZE = 0.0011.inch.to_f #should only need a value slightly larger than 0.001

  SU_FACE_CLEAN_UP_SIZE = 0.0000011.inch.to_f



  # Takes an entities object that will be able to be scaled and un-scaled

  class InstanceScaler

    def initialize(instance)

      @instance = instance

      @entities = (instance.class == Sketchup::Group) ? instance.entities : instance.definition.entities

      @inst_trans = instance.transformation #TODO if the parent is not the model, might would we need all the transformation till the parent is the model?

    end



    #MAIN FUNCTION TO CALL



    # Transforms all the entities of an entities object.

    # WARNING: then a cpoint is added and removed so the model updates the entities properly.

    def transform_entities(transformation)

      @entities.transform_entities(transformation, @entities.to_a) #todo @entities.to_a can throw TypeError reference to deleted Entities

      erase = @entities.add_cpoint(Geom::Point3d.new(0, 0, 0))

      erase.erase!

    end



    # same as transforming something down scaling up will also be capped

    def transform_entities_capped(point, scale)

      max_scale = 10000.to_f #todo 1000 or 10,000 seem like a safe number

      remaining_ratio = scale.to_f

      loop do

        temp_ratio = (remaining_ratio < max_scale) ? remaining_ratio : max_scale

        trans = Geom::Transformation.scaling(point, temp_ratio, temp_ratio, temp_ratio)

        transform_entities(trans)

        remaining_ratio = (remaining_ratio / temp_ratio)

        break unless (remaining_ratio != 1.0)

      end

    end



    #returns the transform used or nil if not transformed

    # def scale_based_on_smallest_face_area

    #   smallest_area = get_smallest_face_area

    #   transformation = nil

    #   #check if there is a small face that needs scaling

    #   if (smallest_area < MIN_FACE_AREA)

    #     transformation = scale_entities_based_on_min_face_area(smallest_area)

    #   end

    #

    #   return transformation

    # end



    # Scales up all entities to prevent bug preventing small edges from creating a face

    # @param [Array] face_vertices, vertices that will be used to make a face

    # returns [Transformation] Transformation object or nil if not transformed

    def scale_to_avoid_tiny_face_bug(face_vertices)

      #todo edges must be larger than 0.001" and face area must be larger than 0.000001"sq

      #todo in order to calculate face area from points need to transform them into xy plane (dont actually transform the vertices)

      smallest_distance = find_smallest_distance(face_vertices)

      transformation = nil

      ratio = nil

      if (smallest_distance < REPAIR_MIN_EDGE_SIZE) # if min distance is smaller need to scale up

        ratio = REPAIR_MIN_EDGE_SIZE / smallest_distance #lets scale so smallest edge is min edge

      end

      #find area of this new face based on points

      area_ratio = get_scale_ratio_based_on_area_of_face_vertices(face_vertices)

      #set ratio to the larger between edge length and face area

      if (ratio.nil?)

        ratio = area_ratio

      elsif (!area_ratio.nil?)

        ratio = (ratio >= area_ratio) ? ratio : area_ratio

      end

      if (!ratio.nil?)

        point = get_center_point

        transformation = Geom::Transformation.scaling(point.clone.transform!(@instance.transformation), ratio)

        transform_entities(transformation)

      end



      return transformation

    end



    # scales the entities up so the smallest edge is SU_EDGE_CLEAN_UP_SIZE inches

    # then scales down the instance so the visible size has not changed

    def scale_entities_up_and_scale_down_instance

      scale_values = scale_instance_to_avoid_su_clean_up #scale up the inners



      scale_down_modified_instance(scale_values[0], scale_values[1]) #scale down the instance

      return scale_values

    end



    ####### Called by instances_scaler #############



    #finds the average of all the faces area for the instance

    def get_average_face_area

      average_face_area = 0

      faces = @entities.grep(Sketchup::Face)

      faces.each{ |face|

        average_face_area += face.area.to_f #do not care about outer scaling

      }

      average_face_area = (average_face_area / faces.length.to_f)



      return average_face_area

    end



    ##### private functions ###########



    #returns the center point so scaling keeps the object near the origin

    def get_center_point

      return @instance.bounds.center #center of the instance bounds to keep the scaled object near the origin

    end



    # returns area or MIN_FACE_AREA which ever is smaller

    def get_scale_ratio_based_on_area_of_face_vertices(face_vertices) #used when creating a face

      face_points = face_vertices.collect{ |v| v.position }

      area = get_face_area_from_points(face_points)

      area = MIN_FACE_AREA if (area.nil?)

      area_ratio = (area < MIN_FACE_AREA) ? MIN_FACE_AREA / area : nil

      return area_ratio

    end



    # transforms the points into a 2d plane then calculates the area

    def get_face_area_from_points(points)

      #find 2 vectors from the face that are not collinear

      vector1 = points[0].vector_to(points[1]).normalize!

      prev_point = points.last

      vector2 = nil

      points.each{ |point|

        temp_vector = prev_point.vector_to(point).normalize!

        if ((vector2.nil?) || (temp_vector != vector1)) #find a vector not in-line with vector1

          vector2 = temp_vector

          break

        end

      }



      #find the normal of the face

      # p "v1: #{vector1} --- v2: #{vector2}" #todo remove

      normal = vector1.cross(vector2)

      # p "normal: #{normal}" #todo remove



      #find the rotation transformation

      origin = Geom::Point3d.new(0, 0, 0)

      if (normal.length.to_f == 0.0)

        # p "something went wrong with getting points normal!"

        return nil

      else

        trans = Geom::Transformation.new(origin, normal)

        trans.invert!



        #find the points seen in xy plane

        points_xy = points.collect{ |p| p.transform(trans) }

        #find the area using xy points

        area = polygonArea(points_xy)



        return area

      end

    end



    # finds the area of a polygon based on 2d points

    def polygonArea(points_xy)

      area = 0.0 # Accumulates area in the loop

      prev_point = points_xy.last

      points_xy.each{ |point|

        area += (prev_point.x.to_f * point.y.to_f) - (prev_point.y.to_f * point.x.to_f)

        prev_point = point

      }

      return (area / 2.0).abs

    end



    # returns the min face area or area of the smallest face

    def get_smallest_face_area

      smallest_area = MIN_FACE_AREA

      @entities.grep(Sketchup::Face) { |face|

        area = face.area(@inst_trans).to_f #only used for classification want the model to see it as big as possible

        area = face.area.to_f if (face.area.to_f < area)

        if (area < smallest_area)

          smallest_area = area

        end

      }

      return smallest_area

    end



    # @returns [Number] the smallest edge length or SU_EDGE_CLEAN_UP_SIZE constant (which ever is smaller)

    def length_of_smallest_edge

      smallest_length = nil

      edges = @entities.grep(Sketchup::Edge)

      edges.each { |e|

        temp_length = e.length(@inst_trans).to_f

        if (e.length.to_f < temp_length)

          # p "using scaled length"

          temp_length = e.length.to_f

        end

        smallest_length = temp_length if (smallest_length.nil? || (temp_length != 0.0 && (temp_length < smallest_length))) #todo trying smallest of transformed and non transformed

      }

      return smallest_length

    end



    # @param [Array] face_vertices, vertices order matters as these represent edges if they are all connected in a loop

    # @returns the smallest distance between connected vertices or the REPAIR_MIN_EDGE_SIZE constant (which ever is smaller)

    def find_smallest_distance(face_vertices)

      prev_vertex = face_vertices.last

      smallest_distance = REPAIR_MIN_EDGE_SIZE

      face_vertices.each{ |v|

        distance = v.position.distance(prev_vertex.position).to_f

        if (distance < smallest_distance)

          smallest_distance = distance

        end

        prev_vertex = v

      }

      return smallest_distance

    end



    def scale_instance_to_avoid_su_clean_up

      scale = tiny_edge_scale_ratio

      area_scale = tiny_face_scale_ratio

      if (!area_scale.nil?)

        scale = area_scale if (scale.nil? || (area_scale > scale))

      end

      if (!scale.nil?) #need to scale up if not nil

        point = get_center_point

        scale_up_trans = Geom::Transformation.scaling(point.clone.transform!(@inst_trans.inverse), scale, scale, scale)

        transform_entities(scale_up_trans)

      end

      return [scale, point]

    end



    # @returns ratio to scale or nil if no need to scale

    def tiny_edge_scale_ratio

      length = length_of_smallest_edge_no_transform

      if ((SU_EDGE_CLEAN_UP_SIZE.to_f > length) && (length != 0.0))

        scale = SU_EDGE_CLEAN_UP_SIZE / length.to_f

      else

        scale = nil

      end

      return scale

    end



    # @returns ratio to scale or nil if no need to scale

    def tiny_face_scale_ratio

      area = area_of_smallest_face_no_transform

      if ((SU_FACE_CLEAN_UP_SIZE.to_f > area) && (area != 0.0))

        scale = SU_FACE_CLEAN_UP_SIZE / area.to_f

      else

        scale = nil

      end

      return scale

    end



    # @returns [Number] the smallest edge length or SU_EDGE_CLEAN_UP_SIZE constant (which ever is smaller)

    def length_of_smallest_edge_no_transform

      smallest_length = nil

      edges = @entities.grep(Sketchup::Edge)

      edges.each { |e|

        temp_length = e.length.to_f

        smallest_length = temp_length if (smallest_length.nil? || (temp_length != 0.0 && (temp_length < smallest_length)))

      }

      return smallest_length.to_f

    end



    def area_of_smallest_face_no_transform

      smallest_area = nil

      faces = @entities.grep(Sketchup::Face)

      faces.each { |f|

        temp_area = f.area.to_f

        smallest_area = temp_area if (smallest_area.nil? || (temp_area != 0.0 && (temp_area < smallest_area)))

      }

      return smallest_area.to_f

    end



    # scales down the instance but not its inner elements

    # @param [number] scale, the scale ratio used for the original scaling

    def scale_down_modified_instance(scale, point)

      if (!scale.nil?) #need to scale down if not nil

        if (@instance.valid?)

          max_scale = 100000.to_f #tested on revolve model and this works

          remaining_ratio = scale.to_f

          loop do

            temp_ratio = (remaining_ratio < max_scale) ? remaining_ratio : max_scale

            reverse_trans = Geom::Transformation.scaling(point, temp_ratio, temp_ratio, temp_ratio).inverse

            @instance.transform!(reverse_trans)

            remaining_ratio = (remaining_ratio / temp_ratio)

            break unless (remaining_ratio != 1.0)

          end

        end

      end

    end



  end



end

end

