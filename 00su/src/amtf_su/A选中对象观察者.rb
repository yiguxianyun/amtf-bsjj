require 'sketchup.rb'
# require File.join(__FILE__, '../AF.rb')
require 'json'

module AMTF
  class A选中对象观察者 < Sketchup::SelectionObserver
    include AMTF_mixin
    def initialize(窗口)
      @窗口对象=窗口
      # @A画边界框m=Sketchup.active_model.select_tool A画边界框.new
    end
    def onSelectionCleared(selection)
      @窗口对象.显示对象信息(nil)
    end
    def onSelectionBulkChange(selection)
      # puts "onSelectionBulkChange: #{selection}"
      @窗口对象.显示对象信息(selection[0])
    end
  end #class
end # module amtf_su
