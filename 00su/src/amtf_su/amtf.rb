require 'sketchup.rb'
require 'open3'
require 'json'

# require File.join(__dir__, 'amtf_mixin.rb')
# require File.join(__dir__, '另存排版.rb') #不引入的话，htmldialog需要reload后才会响应
# require File.join(__dir__, 'A组件属性.rb')

this_dir=File.dirname(__FILE__)
Sketchup.require(File.join(this_dir, "buer/loader.rb"))

# 先加载基层
files=%w(
  AMTF_mixin
  AMTF_mixin2
  amtf_公用
  A布尔select_tool_mixin
  A布尔select_tool_基类
)
files.each { |f|	Sketchup.require(File.join(this_dir, f)) }
# 再加载全部
pattern = File.join(__dir__, '*.rb')
puts "require  pattern111:#{pattern}"
Dir.glob(pattern).each { |file|
  # puts "require file: "+file
  Sketchup.require file
}


# pattern = File.join(__dir__, '布尔/loader.rb')
# puts "require  pattern222:#{pattern}"
# Dir.glob(pattern).each { |file|
#   # puts "require file: "+file
#   Sketchup.require file
# }

module AMTF
  this_dir = File.dirname(__FILE__)

  PATH = this_dir
  # Sketchup.require(File.join(__dir__, '另存排版.rb'))
  # include AMTF_mixin

  unless file_loaded?(__FILE__)
    dialog_amtf_su=nil
    dialog_设置=nil
    @dialog=nil
    @DLG_尺寸与位置=nil
    @observer_选中对象=nil
    @observer_选中对象_动态组件=nil
    @DLG_其他=nil
    @DLG_动态组件=nil
    @@path_to_resources = File.join(File.dirname(__FILE__), 'images')

    # menu = UI.menu('Plugins').add_submenu('amtf_su')
    # menu.add_item('amtf_su') {
    #   dialog_amtf_su.close if !dialog_amtf_su.nil?
    #   dialog_amtf_su=AMTF窗口.new()
    # }
    menu = UI.menu('Plugins').add_submenu('amtf')
    tb = UI::Toolbar.new('amtf')
    command = UI::Command.new("尺寸与位置"){ 打开DLG_尺寸与位置}
    command.small_icon = File.join(@@path_to_resources,"笑脸1.svg")
    command.large_icon = File.join(@@path_to_resources,"笑脸1.svg")
    command.tooltip = ("尺寸与位置")
    command.status_bar_text = ("尺寸与位置")
    tb.add_item(command)
    menu.add_item(command)

    command = UI::Command.new("动态组件"){ 打开DLG_动态组件}
    command.small_icon = File.join(@@path_to_resources,"笑脸3.jpeg")
    command.large_icon = File.join(@@path_to_resources,"笑脸3.jpeg")
    command.tooltip = ("动态组件")
    command.status_bar_text = ("动态组件")
    tb.add_item(command)
    menu.add_item(command)

    command = UI::Command.new("其他"){ 打开DLG_其他}
    command.small_icon = File.join(@@path_to_resources,"笑脸2.svg")
    command.large_icon = File.join(@@path_to_resources,"笑脸2.svg")
    command.tooltip = ("其他")
    command.status_bar_text = ("其他")
    tb.add_item(command)
    menu.add_item(command)

    command = UI::Command.new("连接amtf"){ 重启浏览器窗口(true)}
    command.small_icon = File.join(@@path_to_resources,"amtf-ml.ico")
    command.large_icon = File.join(@@path_to_resources,"amtf-ml.ico")
    command.tooltip = ("连接amtf")
    command.status_bar_text = ("连接amtf")
    tb.add_item(command)
    menu.add_item(command)

    command = UI::Command.new("刷新选中动态组件和群组"){ A其他.new().刷新选中动态组件和群组}
    command.small_icon = File.join(@@path_to_resources,"刷新.png")
    command.large_icon = File.join(@@path_to_resources,"刷新.png")
    command.tooltip = ("刷新选中动态组件和群组")
    command.status_bar_text = ("刷新选中动态组件和群组")
    tb.add_item(command)
    menu.add_item(command)

    command = UI::Command.new("刷新amtf"){ AMTF::reload}
    command.small_icon = File.join(@@path_to_resources,"amtf-无我偈2.png")
    command.large_icon = File.join(@@path_to_resources,"amtf-无我偈2.png")
    command.tooltip = ("刷新amtf")
    command.status_bar_text = ("刷新amtf")
    tb.add_item(command)
    menu.add_item(command)

    command = UI::Command.new("关于amtf"){ AMTF窗口about.new()}
    command.small_icon = File.join(@@path_to_resources,"amtf-无量寿.png")
    command.large_icon = File.join(@@path_to_resources,"amtf-无量寿.png")
    command.tooltip = ("关于amtf")
    command.status_bar_text = ("关于amtf")
    tb.add_item(command)
    menu.add_item(command)

    file_loaded(__FILE__)
  end
  def self.接收参数(arg)
    @observer_选中对象=arg
  end
  def self.接收参数_动态组件(arg)
    @observer_选中对象_动态组件=arg
  end

  def self.打开DLG_其他()
    @DLG_其他.close if !@DLG_其他.nil?
    @DLG_其他=DLG_其他.new()
  end

  def self.打开DLG_动态组件()
    if !@DLG_动态组件.nil?
      puts "@observer_选中对象,已经存在👇"
      puts @observer_选中对象_动态组件
      @DLG_动态组件.close
      selection = Sketchup.active_model.selection
      status = selection.remove_observer @observer_选中对象_动态组件
      puts "移除 status"
      puts status
    end
    @DLG_动态组件=DLG_动态组件.new()
  end

  def self.打开DLG_尺寸与位置()
    if !@DLG_尺寸与位置.nil?
      puts "@observer_选中对象,已经存在👇"
      puts @observer_选中对象
      @DLG_尺寸与位置.close
      selection = Sketchup.active_model.selection
      status = selection.remove_observer @observer_选中对象
      puts "移除 status"
      puts status
    end
    @DLG_尺寸与位置=DLG_尺寸与位置.new()

    # @DLG_尺寸与位置.close if !@DLG_尺寸与位置.nil?
    # @DLG_尺寸与位置=DLG_尺寸与位置.new()
  end

  def self.重启浏览器窗口(显示)
    之前已经打开了窗口=false
    if !@dialog.nil? && @dialog.visible?
      之前已经打开了窗口=true
      # left, top = @dialog.get_position#为什么提示没有这个方法？
    end
    puts "!@dialog.nil?"
    puts !@dialog.nil?
    @dialog.close if !@dialog.nil?
    @dialog=AMTF窗口.new()
    # @dialog.set_size(100, 50)
    # @dialog.set_position(0, 0)
    @dialog.center

    puts "之前已经打开了窗口"
    puts 之前已经打开了窗口
    # if 之前已经打开了窗口
    #   @dialog.center
    #   # @dialog.set_position(100, 50)#为什么提示没有这个方法？
    # end
    # puts 显示
    UI.start_timer(2, false) {
      隐藏本窗口
    }
    # AMTF::隐藏本窗口
    # puts "上面的执行了吗？"
    # if 显示
    #   if RUBY_PLATFORM.index "darwin"
    #     @dialog.show_modal
    #   else
    #     @dialog.show
    #   end
    # else #不显示

    # end
  end

  def self.隐藏本窗口()
    # UI.start_timer(5, false) {
      @dialog.set_position(500, 2000)
      puts "@dialog.set_position(500, 2000)"
    # }
  end

  # class AMTF窗口about < UI::HtmlDialog
  class AMTF窗口about
    def initialize
      view=Sketchup.active_model.active_view
      宽度=view.vpwidth-100
      高度=view.vpheight-100
      options = {
        dialog_title: "关于amtf😄",
        preferences_key: "关于amtf😄",
        width: 宽度,
        height: 高度,
        left: 300,
        top: 100,
        min_width: 50,
        min_height: 150,
        style: UI::HtmlDialog::STYLE_DIALOG
      }
      dialog = UI::HtmlDialog.new(options)
      path = File.join(__dir__, 'html/关于amtf.html')
      dialog.set_file path
      # Display the dialog box
      if RUBY_PLATFORM.index "darwin"
        dialog.show_modal
      else
        dialog.show
      end
      observer=nil
      dialog.center
    end
  end

  class AMTF窗口 < UI::HtmlDialog
    include AMTF_mixin
    def initialize
      # Dialog parameters
      # super "amtf", false, "amtf", 350, 600, 50, 100, true
      # puts File.join(__dir__, '另存排版.rb')
      options = {
        dialog_title: "amtf",
        preferences_key: "amtf",
        # scrollable: false,
        # resizable: false,
        width: 400,
        height: 160,
        left: 0,
        top: 100,
        # min_width: 50,
        # min_height: 150,
        max_width: 1000,
        max_height: 1000,
        # style: UI::HtmlDialog::STYLE_DIALOG
        style: UI::HtmlDialog::STYLE_UTILITY
      }
      super(options)
      path = File.join(__dir__, '/html/amtf.html')
      set_file path
      # set_url('https://www.baidu.com/')
      # set_url("http://127.0.0.1:5501")
      observer=nil
      # Display the dialog box
      if RUBY_PLATFORM.index "darwin"
        show_modal
      else
        show
      end
      Sketchup::set_status_text "amtf有感必有应"
      # AMTF::隐藏本窗口
      @A组件属性obj=A组件属性.new()
      @导出obj=A导出.new(self)

      add_action_callback("读取设置") {|dialog, arg|
        设置=读取设置()
        展示设置(设置)
        发送提示信息("方法完成")
      }
      add_action_callback("获取当前模型全名") {|dialog, arg|
        # puts "收到的arg👉 #{arg}"
        设置常用对象
        当前模型全名=@当前模型全名
        当前模型全名 = 当前模型全名.gsub!(/\\/, "/")
        puts "当前模型全名 #{当前模型全名}"
        execute_script("选择su文件('#{当前模型全名}')")
      }
      add_action_callback("批量导入su") {|dialog, arg|
        puts "收到的arg👉 #{arg}" #网页传参到ruby可以直接传，不需要转json
        # arg = JSON.parse(arg)
        # puts "收到的arg👉 #{arg}"
        # 选择的文件s=arg["选择的文件s"]
        导入数量=A导入.new().批量导入su(arg)
      }
      add_action_callback("缩放边界") {|dialog, arg|
        # puts "收到的组件名："+arg
        # UI.messagebox "#{arg}"
        A其他.new().缩放边界(arg)
      }
      add_action_callback("导入stl") {|dialog, arg|
        puts "收到的组件名："+arg
        arg = arg.gsub!(/\\/, "/")
        导入数量=A导入.new().导入stl(arg)
        execute_script("导入stl结果('#{导入数量}')")
      }
      add_action_callback("识别选中组件") {|dialog, arg|
        # puts "收到的组件名："+arg
        动态属性h=@A组件属性obj.识别选中组件()
        # puts "aaa"
        # puts 动态属性h.to_json
        execute_script("接收识别选中组件('#{动态属性h.to_json}')")
      }
      add_action_callback("修改组件属性") {|dialog, arg|
        # puts arg
        # UI.messagebox "#{arg}"
        修改组件属性结果=@A组件属性obj.修改组件属性(arg)
        execute_script("接收修改组件属性('#{修改组件属性结果.to_json}')")
      }

      # Set the WebDialog's callback
      add_action_callback("开启显示边界框尺寸") {|dialog, arg|
        # puts "收到的arg："+arg
        if arg==true
          if observer==nil
            puts "创建观察者"
            observer=A选中对象观察者.new(self)
          else
            puts "已经创建过了观察者"
          end
          Sketchup.active_model.selection.add_observer(observer)
        else
          if observer!=nil
            selection = Sketchup.active_model.selection
            status = selection.remove_observer observer
            puts "移除观察者"
            puts status
          else
            puts "本来就没有观察者"
          end
        end
      }

      add_action_callback("拼合组件名") {|dialog, arg|
        puts "收到的组件名："+arg
        @导出obj.拼合组件名(arg)
      }

      add_action_callback("解读组件名") {|dialog, arg|
        # 对象=返回第一个选择对象
        # puts "对象.name"+对象.name
        解读后的组件名=@导出obj.解读组件名
        # 发送解读后的组件名(解读后的组件名)
        self.execute_script("接收解读后的组件名('#{解读后的组件名}')")
      }
      add_action_callback("发送调试模式") {|dialog, arg|
        # amtf_dxf文件='D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe'
        if arg!="false"
          puts "是调式模式"
          @调式模式=true
        else
          puts "非调式模式"
          @调式模式=false
        end
      }
      add_action_callback("隐藏本窗口") {|dialog, arg|
        AMTF::隐藏本窗口
        # dialog.set_position(500, -500)
      }
      add_action_callback("重新连接amtf") {|dialog, arg|
        AMTF::重启浏览器窗口(true)
      }
      add_action_callback("开启amtf_exe") {|dialog, arg|
        结果=开启amtf_exe(arg)
      }
      add_action_callback("开启amtf_dxf") {|dialog, arg|
        @导出obj.显示不能隐藏的层()

        判断是否调试模式
        if @调式模式
          amtf_dxf文件='D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe'
        else
          # amtf_dxf文件='../../amtf_dxf/amtf_dxf.exe'
          # amtf_dxf文件=File.join(__dir__, '/amtf_dxf/amtf_dxf.exe')
          amtf_dxf文件='amtf_dxf.exe'
        end
        # exec 'D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe'
        # system(amtf_dxf文件)
        # `D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe`
        kk=Open3.popen3(amtf_dxf文件)
        puts "调用外部命令结果："
        puts kk
      }
      # add_action_callback("开启amtf_dxf") {|dialog, arg|
      #   调用amtf_dxf('D:\amtf\00南腾家居-项目\导出20221016_202614/吊柜-顶板#18__1_24_00_1_-1___amtf_板厚292.dxf')
      # }
      add_action_callback("关于amtf") {|dialog, arg|
        puts "AMTF::AMTF窗口about.newkk"
        # AMTF窗口about.new
        # AMTF::AMTF窗口about.new
        link=File.join(__dir__, '关于amtf.html')
        # system("start #{link}")
        kk=Open3.popen3("start #{link}")
      }
      # add_action_callback("导出cad") {|dialog, arg|
      #   puts arg
      #   arg_sp=arg.split("|")
      #   # puts arg_sp
      # }

      add_action_callback("导出cad") {|dialog, arg|
        puts arg
        @导出cadarg=arg
        当前模型全名,@拟导出组s=@导出obj.导出前检查
        if 当前模型全名=="出错了"
          return
        else
          当前模型全名 = 当前模型全名.gsub!(/\\/, "/")
          self.execute_script("生成唯一文件夹('#{当前模型全名}')")
        end
        @导出时打开的su文件=当前模型全名
      }

      add_action_callback("创建dxf") {|dialog, arg|
        puts arg
        @创建dxf_arg=arg
        当前模型全名,@拟导出组s=@导出obj.导出前检查
        if 当前模型全名=="出错了"
          return
        else
          当前模型全名 = 当前模型全名.gsub!(/\\/, "/")
          self.execute_script("生成唯一文件夹_创建dxf用('#{当前模型全名}')")
        end
        @导出时打开的su文件=当前模型全名
      }

      add_action_callback("创建dxf_调用nodejs后继续") {|dialog, arg|
        puts arg
        # amtf.nil
        唯一文件夹=arg["唯一文件夹"]
        # kk.生产, kk.炸开圆弧
        生产=@创建dxf_arg["生产"]
        arg_sp=生产.split("_板数")
        @封边信息=arg_sp[0]
        puts "@封边信息"
        puts @封边信息
        每块板数=arg_sp[1]
        炸开圆弧=@创建dxf_arg["炸开圆弧"]
        自动保存摆正文件=@创建dxf_arg["自动保存摆正文件"]
        puts "自动保存摆正文件 👉 #{自动保存摆正文件}"
        # amtf.nil

        @导出obj.另存skp放平_识别模型(每块板数,唯一文件夹,@拟导出组s,@封边信息,@导出时打开的su文件,炸开圆弧,自动保存摆正文件)
      }
      add_action_callback("导出cad_调用nodejs后继续") {|dialog, arg|
        puts arg
        # amtf.nil
        唯一文件夹=arg["唯一文件夹"]

        arg_sp=@导出cadarg.split("_板数")
        @封边信息=arg_sp[0]
        puts "@封边信息"
        puts @封边信息
        # amtf.nil
        每块板数=arg_sp[1]

        # 生产 = JSON.parse(arg)
        # 每块板数=生产[:每块板数]

        # Sketchup.active_model.save
        # 导出=A导出.new(self)
        # 发送提示信息("导出组件到单独文件中，稍安勿躁~~" )
        @导出obj.另存skp放平炸开重组组件_导出组件定义到单独文件(每块板数,唯一文件夹,@拟导出组s,@封边信息,@导出时打开的su文件)

        # 拟导出全名hash,导出路径,@当前模型全名=导出.另存skp放平炸开重组组件_导出组件定义到单独文件(每块板数,唯一文件夹,@拟导出组s)
        # 拟导出组件名数组,导出路径=导出.另存skp放平炸开重组组件()
        # 计数=1
        # 拟导出全名hash.each {|k,v|
        #   导出全名=k
        #   原缩放大小=v
        #   导出dxf全名,板厚mm=导出.逐个导出dxfV2(导出全名)
        #   puts "导出.逐个导出dxfV2 完成👆"
        #   发送提示信息("转amtf_dxf处理，注意看其进度……" )
        #   # amtf.nil
        #   # kk=调用amtf_dxf(导出dxf全名+"|"+计数.to_s+"|"+@封边信息)
        #   # kk=调用amtf_dxf("#{导出dxf全名}|#{板厚mm}|#{计数}|#{@封边信息}")

        #   导出dxf全名="#{导出dxf全名}|#{板厚mm}|#{计数}|#{@封边信息}"
        #   导出dxf全名 = 导出dxf全名.gsub!(/\\/, "/")
        #   puts "导出dxf全名"
        #   puts 导出dxf全名
        #   # amtf.nil
        #   # 生产[:导出dxf全名]=导出dxf全名
        #   # 生产[:板厚mm]=板厚mm
        #   # 生产[:计数]=计数
        #   # puts "调用amtf_dxf2('#{生产.to_json}')"
        #   self.execute_script("调用amtf_dxf2('#{导出dxf全名}')")
        #   计数 += 1
        #   # puts "调用amtf_dxf(导出dxf全名):#{kk}"
        # }
        # result = Sketchup.open_file(@导出时打开的su文件)
        # # model = Sketchup.active_model
        # # puts "model.save"
        # # puts model.save
        # # result = Sketchup.open_file(@当前模型全名,true)
        #   # amtf.nil

        # result = UI.messagebox('导出完成，是否 需要 打开导出 文件夹？', MB_YESNO)
        # if result == IDYES
        #   # system('D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe')
        #   UI.openURL(导出路径)
        #   # Open3.popen3("CopyClip.exe '#{导出路径}'")
        # end
      }

      add_action_callback("孤立") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().孤立()
      }
      add_action_callback("选择同材质组件") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().选择同材质组件()
      }
      add_action_callback("移除组件and子级材质") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().移除组件and子级材质()
      }
      add_action_callback("更改背板厚度") {|dialog, arg|
        puts arg
        AF另存排版.new().更改背板厚度(arg)
      }

      add_action_callback("测试") {|dialog, arg|
        # puts arg
          # 联系amtf
          # 测试连接
          对象=返回第一个选择对象()
          A导入.new().炸开组件判断是否有孤立面(对象)


          # @导出obj.删除参考线(对象)

          # 设置常用对象
          # @导出obj.内部边界转子群组(@model)

          # @导出obj.焊接组件边线(对象)
          # @导出obj.导出v2
          # A组件属性.new().遍历组件属性
          # AMTF窗口组件属性.new
          # reload其他插件
          # A导入.new().A导入('D:\\桌面\\新建文件夹\\导出stl-标准柜20230208-1335-29')
          # A导入.new().A导入('D:/桌面/新建文件夹/导出stl-标准柜20230208-1335-29')
          # 设置常用对象
          # 对象=@entities[0].parent
          # A导入.new().识别孔槽(对象)

          # 新边界尺寸=Hash[]
          # 新边界尺寸["x"]=6567.0
          # 新边界尺寸["y"]=240.0
          # 新边界尺寸["z"]=1000
          # A其他.new().缩放边界(新边界尺寸)

          发送提示信息("方法完成")
      }

      add_action_callback("刷新amtf") {|dialog, arg|
        AMTF::reload
        # puts arg+" 完成！"
        Sketchup::set_status_text arg+" 完成！"
        # return arg+" 完成！"
      }
      add_action_callback("匹配尺寸") {|dialog, arg|
        Sketchup.active_model.select_tool A匹配尺寸.new
      }
      add_action_callback("对齐") {|dialog, arg|
        # Sketchup.active_model.select_tool( AF对齐.new )
        Sketchup.active_model.select_tool AF对齐.new
      }
      add_action_callback("多余组件处理") {|dialog, arg|
        AF另存排版.new().多余组件处理
      }
      add_action_callback("画柜子") {|dialog, arg|
        # p "hello"
        Sketchup.active_model.select_tool AF画柜子.new(arg+".skp")
      }
      add_action_callback("另存为排版模型") {|dialog, arg|
        AF另存排版.new().另存为排版模型
      }
      add_action_callback("删除指定图层") {|dialog, arg|
         Sketchup::set_status_text __method__.to_s()+" ing"
        AF另存排版.new().删除指定图层
      }
      add_action_callback("删除隐藏项目") {|dialog, arg|
        AF另存排版.new().删除隐藏项目不传参数
      }
      add_action_callback("删除or炸开无板字组件") {|dialog, arg|
        AF另存排版.new().删除or炸开无板字组件
      }
      add_action_callback("炸开所有子组件") {|dialog, arg|
        AF另存排版.new().炸开所有子组件
      }
      add_action_callback("组件改名") {|dialog, arg|
        AF另存排版.new().组件改名
      }
      add_action_callback("组件转群组") {|dialog, arg|
        AF另存排版.new().组件转群组
      }
      add_action_callback("干涉检查") {|dialog, arg|
        # UI.messagebox("我执行了！")
        AF另存排版.new().干涉检查
      }
      add_action_callback("一键排版预处理") {|dialog, arg|
        @一键排版预处理arg=arg
        设置常用对象
        file=@model.path
        file = file.gsub!(/\\/, "/")
        # puts file
        self.execute_script("生成唯一文件名('#{file}')")
      }
      add_action_callback("一键排版预处理_正式执行") {|dialog, arg|
        唯一文件名=arg
        处理结果=AF另存排版.new().一键排版预处理(@一键排版预处理arg,唯一文件名)
        self.execute_script("返回一键排版预处理结果('#{处理结果}')")
      }
      add_action_callback("检查板厚") {|dialog, arg|
        返回结果=@导出obj.检查板厚()
        self.execute_script("返回_返回结果('#{返回结果}')")
        # self.execute_script("返回_检查板厚_处理结果('#{处理结果.to_json}')")

      }
      add_action_callback("检查材质") {|dialog, arg|
        返回结果=@导出obj.检查材质()
        self.execute_script("返回_返回结果('#{返回结果}')")
      }
      add_action_callback("延伸背板") {|dialog, arg|
        AF另存排版.new().延伸背板(arg)
      }

      add_action_callback("文件改名") {|dialog, arg|
        文件改名("kk")
      }
      add_action_callback("返回值给ruby") {|dialog, arg|
        return arg
      }
      add_action_callback("清除未使用") {|dialog, arg|
        清除未使用
      }
      add_action_callback("重载ABF") {|dialog, arg|
        Sketchup::set_status_text "重载ABF"+" ing"

        path = Sketchup.find_support_file "abf_solutions/startup.rb", "Plugins"
        puts "path=" + path.to_s
        load path.to_s
        puts "重新加载ABFing……稍安勿躁……"
        Sketchup::set_status_text "重载ABF"+" 完成"
      }
      # Display the dialog box
      # if RUBY_PLATFORM.index "darwin"
      #   show_modal
      # else
      #   show
      # end

    end #initialize

    def reload其他插件
      puts "reload其他插件ing"
      original_verbose = $VERBOSE
      $VERBOSE = nil
      # pattern = File.join('D:/amtf/amtf_suoc/', 'oneclickcabinet/**/*.rb')
      pattern = File.join('D:/amtf/amtf_suoc/', '*.rb')
      puts pattern
      Dir.glob(pattern).each { |file|
        # Cannot use `Sketchup.load` because its an alias for `Sketchup.require`.
        # puts file
        load file
      }.size
      ensure
        $VERBOSE = original_verbose
    end

    # 执行网页js👇
    def 调用amtf_dxf(导出dxf全名)
      # phone = %q|D:\amtf\00南腾家居-项目\导出20221016_202614/吊柜-顶板#18__1_24_00_1_-1___amtf_板厚292.dxf|
      # # puts "电话号码 : #{phone}"

      # # 移除数字以外的其他字符
      # phone = phone.gsub!(/\\/, %q|\\\\\\\\|)
      # puts "电话号码 : #{phone}"

      # 导出dxf全名 = 导出dxf全名.gsub!(/\\/, "\\\\\\\\")
      导出dxf全名 = 导出dxf全名.gsub!(/\\/, "/")
      puts "调用amtf_dxf('#{导出dxf全名}')"

      self.execute_script("调用amtf_dxf('#{导出dxf全名}')")
      # self.execute_script("调用amtf_dxf('D:amtf00南腾家居-项目')")
      # D:\amtf\00南腾家居-项目\导出20221016_202614/吊柜-顶板#18__1_24_00_1_-1___amtf_板厚292.dxf
    end

    def 判断是否调试模式()
      self.execute_script("发送调试模式()")
    end
    # def 发送解读后的组件名(解读后的组件名)
    #   self.execute_script("接收解读后的组件名('#{解读后的组件名}')")
    # end

    def 显示边界框尺寸(边界框尺寸)
      # puts "边界框尺寸"
      # puts 边界框尺寸
      self.execute_script("显示边界框尺寸('#{边界框尺寸}')")
    end

  end #AMTF窗口


end # module amtf_su
