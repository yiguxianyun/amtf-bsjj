# require  'find'

module AMTF
  class A导入
    include AMTF_mixin
    def 批量导入su(su文件s)
      设置常用对象
      # box = @model.bounds
      # lenx=box.width
      su文件s.each{|e|
        puts "su文件名👉"+e
        definitions = @model.definitions
        definition = definitions.load(e)
        解析文件名V2(e)
        definition.name=@无后缀名
        加入前box右下角=@model.bounds.corner(1)
        puts "加入前box右下角"
        puts 加入前box右下角
        lenx = @model.bounds.width
        puts "加入前lenx"
        puts lenx
        偏距=1000.mm
        if lenx==0
          # lenx=lenx+1000.mm
          加入前box右下角=ORIGIN
          puts "加入前box右下角"
          puts 加入前box右下角
          偏距=0
        end
        transform=Geom::Transformation.new(Geom::Point3d.new 0,0,0)
        instance = @entities.add_instance definition, transform

        当前组件左下角=instance.bounds.corner(0)
        puts "当前组件左下角"
        puts 当前组件左下角
        vector = 当前组件左下角.vector_to 加入前box右下角
        tr = Geom::Transformation.translation(vector)
        instance.transform! tr
        vector1 = Geom::Vector3d.new(偏距,0,0)
        tr = Geom::Transformation.translation(vector1)
        instance.transform! tr
      }

      Sketchup.active_model.active_view.invalidate
      result = Sketchup.send_action("viewIso:")
      result = Sketchup.send_action("viewZoomExtents:")
    end


    def 导入stl(主路径)
      设置常用对象
      puts "主路径："+主路径
      # Dir.foreach(主路径) do |item|
      #   # next if item == '.' or item == '..'
      #   # do work on real items
      #   puts item#打印不出文件名！
      # end
      # Find.find(主路径) do |filename|
      #   p filename
      # end
      原对象=Hash[]
      @entities.each {|e|
        原对象[e]=""
      }

      options = { :units => "mm",
        :merge_coplanar_faces => true,
        :preserve_origin  => true,
        :swap_yz  => false }

      # Dir.glob("#{主路径}/*.STL") do |file|
      #   # File.dirname("D:/桌面/导出stl-中重载型(200kg)万向型脚轮[CAE150-PB]20230501-2309-19")
      # # Dir.glob("D:/桌面/导出stl-中重载型(200kg)万向型脚轮[CAE150-PB]20230501-2309-19/*.stl")
      # # Dir.glob("#{主路径}/*") do |file|
      #   puts file
      #   status = @model.import(file, options)
      #   # if /stl\b/i.match(file)
      #   #   status = @model.import(file, options)
      #   # end
      # end
      文件s=Dir.glob("#{主路径}/*.STL")
      if !文件s.count==0
        文件s.each{|e|
          puts e
          status = @model.import(e, options)
        }
      else #含有特殊符合的路径不认识？比如下面这个
        # 主路径="D:/桌面/导出stl-中重载型(200kg)万向型脚轮[CAE150-PB]20230501-2309-19"
        主路径的父亲=File.dirname(主路径)
        主路径短名=File.basename(主路径)
        # 主路径短名="导出stl"
        # /#{关键词}/.match(e.name)
        文件s=Dir.glob("#{主路径的父亲}/*/*").grep(/stl$/i)
        # 文件s=Dir.glob("#{主路径的父亲}/*")
        文件s.each{|e|
          if e.include?(主路径短名)
            puts "e👉#{e}"
            status = @model.import(e, options)
          end
        }
      end
# amtf.nil
      @selection.clear
      导入进来的组件=[]
      需转群组的组件=[]
      @entities.each {|e|#@entities会包含新导入进来的对象
        if !原对象.has_key?(e)
          if e.is_a?( Sketchup::ComponentInstance )
            puts "这是导入进来的"
            组件名=e.definition.name
            p 组件名
            组件名= 组件名.gsub(/.STL\S*/i, "")
            组件名= 组件名.gsub(/ - /i, "_")
            p 组件名
            e.definition.name=组件名

            关键词位置= 组件名=~ /Hettich|Rastex|Dowels/i
            # UI.messagebox "组件名:#{组件名}  关键词位置==nil#{关键词位置==nil}"
            if 关键词位置==nil
              需转群组的组件.push(e)
            else
              导入进来的组件.push(e)
            end
          end #如果是组件
        end#!原对象.has_key?(e)
      }

      需转群组的组件.each {|e|
        组件名=e.definition.name
        eee=e.explode
        孤立面s=[]
        fs=[]
        eee.to_a.each {|e|
          if e.is_a?( Sketchup::Face ) #or e.is_a?( Sketchup::Edge )
            fs.push e
          # elsif
            # 导入之前就应该隐藏sw的孤立面，不然处理不了
            # e.is_a?( Sketchup::Edge )
            # if e.faces.count==1
            #   孤立面=e.faces[0]
            #   if !孤立面s.include?(孤立面)
            #     孤立面s.push e.faces[0]
            #   end
            #   # e.erase!
            # end
          end
        }

        fs=fs-孤立面s
        entities = Sketchup.active_model.entities
        group = entities.add_group(fs)
        # if 孤立面s.count >0
        #   孤立面sedgs=[]
        #   孤立面s.each{|孤立面|
        #     孤立面sedgs.push 孤立面.edges
        #   }
        #   entities.erase_entities(*孤立面s)
        #   entities.erase_entities(*孤立面sedgs)
        # end


        # 组件名= 组件名.gsub!(/.STL\S*/i, "")
        # 组件名= 组件名.gsub!(/ - /i, "_")
        group.name=组件名
        # amtf.nil
        识别孔槽(group)

        导入进来的组件.push(group)
      }

      @selection.add(导入进来的组件)
      result = Sketchup.send_action("viewIso:")
      result = Sketchup.send_action("viewZoomToSelection:")
      return 导入进来的组件.count
    end

def 炸开组件判断是否有孤立面(e)
  组件名=e.definition.name
  eee=e.explode
  孤立面s=[]
  fs=[]
  eee.to_a.each {|e|
    if e.is_a?( Sketchup::Face ) #or e.is_a?( Sketchup::Edge )
      fs.push e
    elsif
      e.is_a?( Sketchup::Edge )
      if e.faces.count==1
        孤立面=e.faces[0]
        if !孤立面s.include?(孤立面)
          孤立面s.push e.faces[0]
        end
        # e.erase!
      end
    end
  }

  需要的面s=fs-孤立面s
  entities = Sketchup.active_model.entities
  group = entities.add_group(需要的面s)
  if 孤立面s.count >0
    孤立面sedgs=[]
    孤立面s.each{|孤立面|
      孤立面sedgs.push 孤立面.edges
    }
    entities.erase_entities(*孤立面s)
    entities.erase_entities(*孤立面sedgs)
  end


  # 组件名= 组件名.gsub!(/.STL\S*/i, "")
  # 组件名= 组件名.gsub!(/ - /i, "_")
  group.name=组件名
  # amtf.nil
  识别孔槽(group)
end

    #boundbox区域最大的面判定为顶底面，当开槽面积很大，并且把顶底面完全分割开了的时候，判断会错误，碰到再说
    def 识别孔槽(群组)
      设置常用对象
      # 名称=群组.name
      entities = 群组.entities
      # entities = @model.active_entities
      面h=Hash[]
      entities.each {|ee|
        if ee.is_a?( Sketchup::Face )
          面=ee
          bb面积=获取面BoundingBox面积(面)#如果是开了个很大的槽，槽面积可能大于板顶底面面积
          # 面积=面.area
          面h[面]=bb面积
        end
      }

      面h= Hash[面h.sort_by {|key,value| value}.reverse!]#按面积排序
      # 面h.each { |面,v|
      #   @selection.add(面)
      #   UI.messagebox "#{v}"
      # }
      plane = 面h.keys[0].plane
      point = 面h.keys[1].vertices[0].position
      板厚 = point.distance_to_plane(plane).to_mm.round(2)
      # 大面

      拟删除通孔边s=[]#如果过早删除会影响另外一面通孔的判断
      # @selection.clear
      (0..1).each { |i|
        面=面h.keys[i]
        # @selection.add(面)

        loops = 面.loops
        normal = 面.normal
        loops.each {|loop|
          edges = loop.edges
          if loop.outer?
            # puts "Loop is an outer loop."
            # @selection.add(edges)
            # if i==0
            #   最大面外环边数=edges.count
            # else
            平行面s对应多余边s=找edges平行边面(edges,面,板厚)
            平行面s对应多余边s.each{|平行面,value|
              # @selection.add(平行面)
              # UI.messagebox "#{平行面}"

              # 深度=平行面s对应多余边s[平行面][1]
              深度=value[1]
              status = 平行面.pushpull(深度.mm)
              用平面组件代替face(平行面,群组,深度)

              多余边s=value[0]
              # @selection.add(多余边s)#push后已经无效了
              # @selection.add(面)
              共面面s=Hash[]
              共面面s[:计数]=0
              共面线s=Hash[]
              已找到的共面线s=Hash[]
              已找到的共面线s[面]=""
              找共面面s及共面线(面,共面面s,共面线s,已找到的共面线s)

              # 共面面s.each{|face,value|
              #   用平面组件代替face(face,群组,深度)
              # }
              # 用平面组件代替face(共面线s.keys,群组,深度)
              entities.erase_entities(共面线s.keys)
            }

            # UI.messagebox "#{edges.count}"
          else#内部边界
            平行边,深度,平行面=找edges平行边面_找到一个就收工(edges,面)
            if 平行边==nil
              @selection.add(edges)
              UI.messagebox "请注意，没找到这个边界的平行边，无法识别深度！"
            end
            if 深度==板厚#通孔槽，如果是圆的需要处理，其他形状不处理
              if 是否为圆形(edges)
                if i==0
                  #通孔，用平面组件代替
                  用平面组件代替內部edges(edges,群组,面,-1)
                  拟删除通孔边s.push(edges)#如果过早删除会影响另外一面通孔的判断
                else
                  entities.erase_entities(edges)#删除另一面的圆孔边线，已经用组件代替了
                end
              else
                next#通槽，不处理
              end
            else#非通孔槽
              用平面组件代替內部edges(edges,群组,面,深度)
              entities.erase_entities(edges)
            end
            # UI.messagebox "#{平行边},#{深度}"
            # UI.messagebox "#{bb.depth},#{bb.depth},#{bb.width} center:#{bb.center}"
            # UI.messagebox "#{边长}"
          end
        }
      }

      拟删除通孔边s.each {|e|
        entities.erase_entities(e)#删除圆孔边线，已经用组件代替了
      }
    end

    def 找共面面s及共面线(face,共面面s,共面线s,已找到的共面线s)
      # 共面面s=Hash[]
      # 共面线s=Hash[]
      共面面s[:计数]=共面面s[:计数]+1
      # UI.messagebox "#{共面面s[:计数]}"
      face.edges.each {|e|
        其他faces = e.faces
        其他faces.each {|其他face|
          if !已找到的共面线s.has_key?(其他face) and face.normal.parallel?(其他face.normal)
            共面面s[其他face]=""
            已找到的共面线s[其他face]=""
            找共面面s及共面线(其他face,共面面s,共面线s,已找到的共面线s)
          end
        }
        if((e.faces.length==0) || (e.faces.length==2 && e.faces[0].material==e.faces[1].material && e.faces[0].normal.parallel?(e.faces[1].normal)))
          # erasee << e
          共面线s[e]=""
       end

      }

      # return 共面面s,共面线s
    end



    def 用平面组件代替內部edges(edges,群组,面,标记深度)
      entities = 群组.entities
      bb,边长=获取entities边界大小(edges)
      边长mm=边长.to_mm

      if 是否为圆形(edges)
        子group = entities.add_group()
        entities子=子group.entities
        # @selection.add(面)
        # UI.messagebox "(边长/2).round(1)#{(边长/2).round(1)}"
        # UI.messagebox "(边长/2)#{(边长/2)}"
        半径inch=((边长mm/2).round(1))/25.4
        puts "(半径inch)===#{半径inch}"
        edges子 = entities子.add_circle bb.center, 面.normal, 半径inch
        # 3.49.round(0.5)
        # 3.49.round(1)
        # (4.999207/2).round(1)
        子group.name="ABF_圆孔"
      else
        子group = entities.add_group(edges)
        子group.name="ABF_槽"
        子group=把组件转移到某组件_mx(子group,群组)

      end
      layers = Sketchup.active_model.layers
      new_layer = layers.add("kk-sd#{标记深度}")
      子group.entities.each {|e|
        e.layer = new_layer
      }

    end

    def 用平面组件代替face(face,群组,标记深度)
      entities = 群组.entities
      # @selection.clear
      子group = entities.add_group(face.edges)
      # UI.messagebox "添加面到子群组"
      # amtf.nil
      # UI.messagebox "移动面到原父群组"
      子group.name="ABF_破边槽"

      layers = Sketchup.active_model.layers
      new_layer = layers.add("kk-sd#{标记深度}")
      子group.entities.each {|e|
        e.layer = new_layer
      }

      把组件转移到某组件_mx(子group,群组)

    end

    def 是否为圆形(edges)
      if edges.count < 20
        return false
      end

      上个边线夹角=nil
      vector上一个=nil
      上个edge=nil
      边长=edges[0].length.to_mm.round(2)
      edges.each {|edge|
        # 通过夹角判断
        line = edge.line
        if (line)
          vector1=line[1]
          if vector上一个!=nil
            边线夹角 =( vector1.angle_between vector上一个).radians.round(1)
              # puts "边线夹角"
              # puts 边线夹角
            if 上个边线夹角!=nil
              # puts "上个边线夹角"
              # puts 上个边线夹角
              # puts "上个边线夹角差距"
              # puts 上个边线夹角-边线夹角
              夹角差距=(上个边线夹角-边线夹角).abs
              # if 上个边线夹角!=边线夹角
              if 夹角差距>4
                # @selection.add(edge)
                # @selection.add(上个edge)
                # amtf.nil
                return false
              end
            end
            上个边线夹角=边线夹角
          end
          vector上一个=vector1
          上个edge=edge
        end

        # 通过长度 不行，圆孔导入过来边可能有三角面
        # if edge.length.to_mm.round(2) != 边长
        #   return false
        # end
      }
      return true
    end

    def cleanupline  #删共面线
      # Sketchup.active_model.start_operation("删共面线", true)
      ss = Sketchup.active_model.selection
      erasee = []
      ss.each {|e|
        if e.is_a?(Sketchup::Edge)
          if((e.faces.length==0) || (e.faces.length==2 && e.faces[0].material==e.faces[1].material && e.faces[0].normal.parallel?(e.faces[1].normal)))
             erasee << e
          end
        end
      }
      erasee.each {|e|
        e.erase! if(e.deleted? == false)
      }
      # Sketchup.active_model.commit_operation
      # UI.beep
      # Sketchup.set_status_text '蛤蜊提示：共删除了 ' + erasee.length.to_s + ' 根线！', SB_VCB_LABEL
    end

    def 找edges平行边面(源edges,源面,板厚)
      平行面s对应多余边s=Hash[]
      源edges.each {|edge|
        faces = edge.faces
        faces.each {|face|
          # 平行大面=(face.normal).parallel?(源面.normal)
          if face!=源面
            平行边,深度,平行面=找单个edge最远平行边(edge,face)
            if 深度<板厚
              # @selection.clear
              # @selection.add(edge)
              # UI.messagebox "edge"
              # @selection.add(平行边)
              # UI.messagebox "深度#{深度}"
              # @selection.add(平行面)
              if !平行面s对应多余边s.has_key?(平行面)
                平行面s对应多余边s[平行面]=[]
                平行面s对应多余边s[平行面][0]=[]
              end
              平行面s对应多余边s[平行面][0].push(edge)
              平行面s对应多余边s[平行面][1]=深度
            end
          end
        }
      }
      return 平行面s对应多余边s
    end

    def 找单个edge最远平行边(源edge,搜索面)
      最远平行面=nil
      最远平行边=nil
      最远平行边深度=0

      line1 = 源edge.line
      if (line1)
        源edge向量=line1[1]
      else
        # UI.messagebox "Failure"
      end

      # @selection.add(搜索面)
      # puts "找到搜索面"
      搜索面edges = 搜索面.edges
      搜索面edges.each{|搜索面edge|
        搜索面line = 搜索面edge.line
        if (搜索面line)
          搜索面edge向量=搜索面line[1]
        else
          # UI.messagebox "Failure"
        end

        #待续，开了露边槽的外围面上可能会多个平行边，需要识别
        if 搜索面edge向量.parallel?(源edge向量) and 搜索面edge!=源edge
            # @selection.clear
            # @selection.add(源edge)
            # UI.messagebox "源edge"

            深度 = 源edge.end.position.distance_to_line(搜索面line).to_mm.round(1)
            # @selection.add(搜索面edge)
            # UI.messagebox "搜索面edge"
            # UI.messagebox "深度#{深度}====最远平行边深度#{最远平行边深度}"
            if 深度>最远平行边深度
              最远平行边深度=深度

              最远平行边=搜索面edge
              faces = 搜索面edge.faces
              faces.each {|face|
                if face!=搜索面
                  最远平行面=face
                  break
                end
              }
            end
            # return 平行边,深度,平行面#如果是 找到一个就收工
        end
      }
      # @selection.clear
      # @selection.add(平行边)
      # @selection.add(源edge)
      # UI.messagebox "深度#{深度}====最远平行边深度#{最远平行边深度}"
      return 最远平行边,最远平行边深度,最远平行面
    end


    def 找edges平行边面_找到一个就收工(源edges,源面)
      平行边=nil
      平行面=nil
      深度=0

      源edges.each {|edge|
        faces = edge.faces
        faces.each {|face|
          faces = edge.faces
          # 找到对应孔槽边=false
          if face!=源面
            平行边,深度,平行面=找单个edge平行边_找到一个就收工(edge,face)
            if 平行边!=nil
              return 平行边,深度,平行面
            end
            # if 平行边!=nil
            #   return
            # end
          end
        }
      }
      return 平行边,深度,平行面
    end

    def 找单个edge平行边_找到一个就收工(源edge,搜索面)
      平行边=nil
      平行面=nil
      深度=0
      line1 = 源edge.line
      if (line1)
        源edge向量=line1[1]
      else
        # UI.messagebox "Failure"
      end

      # @selection.add(搜索面)
      # puts "找到搜索面"
      搜索面edges = 搜索面.edges
      搜索面edges.each{|搜索面edge|
        搜索面line = 搜索面edge.line
        if (搜索面line)
          搜索面edge向量=搜索面line[1]
        else
          # UI.messagebox "Failure"
        end
        if 搜索面edge向量.parallel?(源edge向量) and 搜索面edge!=源edge
            # @selection.add(源edge)
            # @selection.add(搜索面edge)
            深度 = 源edge.end.position.distance_to_line(搜索面line).to_mm.round(1)
            puts "深度:#{深度}"
            # UI.messagebox distance*25.4
            平行边=搜索面edge
            faces = 搜索面edge.faces
            faces.each {|face|
              if face!=搜索面
                平行面=face
                break
              end
            }
            return 平行边,深度,平行面
        end
      }
      return 平行边,深度,平行面
    end



  end#class
end # module amtf_su
