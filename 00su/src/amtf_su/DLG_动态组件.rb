module AMTF
  class DLG_动态组件 < UI::HtmlDialog
    # include AMTF_mixin
    include AMTF_mixin2
    def initialize
      # Dialog parameters
      # super "amtf", false, "amtf", 350, 600, 50, 100, true
      # puts File.join(__dir__, '另存排版.rb')
      options = {
        dialog_title: "动态组件😄",
        preferences_key: "动态组件😄",
        # scrollable: false,
        # resizable: false,
        width: 400,
        height: 160,
        left: 0,
        top: 100,
        # min_width: 50,
        # min_height: 150,
        max_width: 1000,
        max_height: 1000,
        style: UI::HtmlDialog::STYLE_DIALOG
      }
      super(options)
      path = File.join(__dir__, '/html/动态组件.html')
      set_file path
      @observer_选中对象_动态组件=nil
      # Display the dialog box
      if RUBY_PLATFORM.index "darwin"
        show_modal
      else
        show
      end
      add_action_callback("组件唯一_含子级") {|dialog, arg|
        组件唯一_含子级
      }
      add_action_callback("添加xyz属性_含子级") {|dialog, arg|
        添加xyz属性_含子级
      }
      add_action_callback("设置当前公式值") {|dialog, arg|
        #xyz的位置值，如果没有设置公式，没办法通过读取常规值的方式获取………该方法废了
        puts "接收到的arg👉#{arg}"
        k=arg["k"]
        v=arg["v"]
        e=@选中对象
        puts "e.make_unique"
        e.make_unique
        设置公式值_单个(e,k,v)
        $dc_observers.get_class_by_version(e).redraw_with_undo(e)
      }
      add_action_callback("监控选中") {|dialog, arg|
        puts "收到的arg："
        puts arg
        puts arg==true
        设置常用对象
        if arg==true
          @observer_选中对象_动态组件=A选中对象观察者.new(self)
          AMTF::接收参数_动态组件(@observer_选中对象_动态组件)
          status =@selection.add_observer(@observer_选中对象_动态组件)
          puts "创建观察者?"
          puts status
        else
          model = Sketchup.active_model
          status = @selection.remove_observer @observer_选中对象_动态组件
          puts "移除观察者:status"
          puts status
        end
      }
      add_action_callback("修改属性") {|dialog, arg|
        puts "接收到的arg👉#{arg}"
        h=arg
        k=h["k"]
        # 公式值=arg["公式值"]
        e=@选中对象
        h["_lengthunits"]="CENTIMETERS"
        puts "e.make_unique"
        e.make_unique
        h.each{|k,v|
          # 改属性_单个_k值确定v值可能要转换(e,k,v)
          改属性_单个_传字典(e,k,v)
        }
        $dc_observers.get_class_by_version(e).redraw_with_undo(e)
        显示对象信息(e,true)
      }

      add_action_callback("导出json") {|dialog, arg|
        puts "接收到的arg👉#{arg}"
        导出json(arg)
      }
      add_action_callback("导入json") {|dialog, arg|
        puts "接收到的arg👉#{arg}"
        导入json(arg)
      }
      add_action_callback("清除公式") {|dialog, arg|
        puts "接收到的arg👉#{arg}"
        清除公式(arg)
      }
      add_action_callback("生成装配件") {|dialog, arg|
        # puts "arg"
        # puts arg
        e=生成装配件(arg)
        if arg["跟随鼠标"]==true
          Sketchup.active_model.select_tool SLCT_移动到鼠标位置.new(e)
        end
      }
      add_action_callback("链接父级属性") {|dialog, arg|
        链接父级属性()
      }
      add_action_callback("生成单组件") {|dialog, arg|
        # arg = JSON.parse(arg)
        puts "arg"
        puts arg
        e=生成单组件(arg)
        if arg["跟随鼠标"]==true
          Sketchup.active_model.select_tool SLCT_移动到鼠标位置.new(e)
        end
      }

      add_action_callback("替代或重叠另一个对象") {|dialog, arg|#接收到的就是json对象，不需要解析
        puts "arg[替代]"
        puts arg["替代"]
        @替代=(arg["替代"])
        @加公式=(arg["加公式"])

        @去替代对象=返回第一个选择对象("请提前选中一个 去替代  的对象 ！")
        # puts "e=返回第一个选择对象"
        # puts e
        if @去替代对象==nil
          return
        end
        Sketchup.active_model.select_tool SLCT_替代或重叠另一个对象.new(@去替代对象,self)
        # 替代或重叠另一个对象()
      }
      set_on_closed() {
        status = Sketchup.active_model.selection.remove_observer @observer_选中对象_动态组件
        puts "移除 status 👉 #{status}"
      }

    end #initialize

    def 导入json(arg)
      e=返回第一个选择对象
      return if e.nil?
      定义名=e.definition.name
      路径,文件短名,无后缀名,后缀=解析文件名(@当前模型全名)
      json文件名="#{路径}\\#{无后缀名}-#{定义名}.json"

      json = File.read(json文件名)
      h = JSON.parse(json)
      puts "h👉#{h}"

      转mm=false
      旧属性h=读取全部动态属性h(e,转mm)
      puts "旧属性h👉#{旧属性h}"


      h.each{|k,v|
        if 旧属性h.has_key?(k)
          puts "原来存在的键 👉 #{k}"
          # 不覆盖旧属性
          if arg=="新增且修改"
            改属性_单个_传字典(e,k,v)
          end
        else
          puts "原来不存在的键 👉 #{k}"
          改属性_单个_传字典(e,k,v)
        end
      }

      if e.is_a? Sketchup::Group
        e.make_unique
      end

      $dc_observers.get_class_by_version(e).redraw_with_undo(e)

      发送提示信息("方法完成")
      puts "发送提示信息(\"方法完成\")  已经执行👆"
    end

    def 导出json(e)
      e=返回第一个选择对象
      return if e.nil?
      定义名=e.definition.name
      路径,文件短名,无后缀名,后缀=解析文件名(@当前模型全名)
      json文件名="#{路径}\\#{无后缀名}-#{定义名}.json"
      转mm=false
      h=读取全部动态属性h(e,转mm)
      File.write(json文件名, JSON.dump(h))
      puts "json文件名👉#{json文件名}"
      开启amtf_exe=Open3.popen3(`explorer #{json文件名}` )
      # 开启amtf_exe=Open3.popen3(`explorer /e,/select,#{json文件名}` )
      发送提示信息("方法完成")
    end

    def 显示对象信息(e,强制刷新=false)
      # puts "显示对象信息  触发了👇…………………………………………"
      # puts "选中的对象是 #{e}"
      # puts "A全部xyz简称👉  #{A全部xyz简称}"
      # puts "强制刷新 👉 #{强制刷新}"
      选中了其他对象=false
      if e.nil?
        # puts "没有选中对象"
        # h=Hash[]
        # @选中对象=nil
      else
        if @选中对象!=e || 强制刷新
          # puts "强制刷新 👉 #{强制刷新}"
          选中了其他对象=true
          h=读取全部动态属性h(e)
          h["定义名"]=e.definition.name
          @选中对象=e
        else
          # 选中对象没有改变=true
          # puts "选中对象没有改变"
        end
      end

      if 选中了其他对象
        # puts "展示信息👇"
        信息= h.to_json
        self.execute_script("显示信息('#{信息}')")
      end
      # puts "A全部xyz简称👉  #{A全部xyz简称}"
    end #遍历组件属性

    def 设置到同父级一样大(e)
      父组件属性dic = 根据元素确定dic(e.parent)
      puts "父组件属性h👇"
      puts 父组件属性dic
      _name=父组件属性dic["_name"]
      h=Hash[]
      ["lenx","leny","lenz"].each{|k|
        父属性="#{_name}!#{k}"#父属性没有这个值呢？再说
        puts "父属性 #{父属性}"
        h[k]=Hash["公式值"=>父属性]
      }
      h["x"]=Hash["公式值"=>0]
      h["y"]=Hash["公式值"=>0]
      h["z"]=Hash["公式值"=>0]
      h.each{|k,v|
        # 改属性_单个_k值确定v值可能要转换(e,k,v)
        改属性_单个_传字典(e,k,v)
      }

    end

    def 链接父级属性
      e=返回第一个选择对象
      if e==nil
        return
      end
      父组件属性dic = e.parent.attribute_dictionaries["dynamic_attributes"]
      puts "父组件属性h👇"
      puts 父组件属性dic
      排除属性arr = ["description", "dialogheight", "dialogwidth",
        "imageurl","itemcode","name","scaletool","summary","onclick"
      ] + Axyz
      _name=父组件属性dic["_name"]
      父组件属性dic.each{|k,v|
        # puts k
        if 排除属性arr.include?(k)
          next
        end
        是否_开头=/^_/.match(k)
        if 是否_开头
          next
        end
        puts "#{k}👉#{v}"
        父属性="#{_name}!#{k}"#父属性没有这个值呢？再说
        公式k="_#{k}_formula"
        e.set_attribute 'dynamic_attributes', 公式k, 父属性
      }
      $dc_observers.get_class_by_version(e).redraw_with_undo(e)
      设置到原点(e)
    end

    def 设置到原点(e)
      属性h=Hash[
        "x"=>0,
        "y"=>0,
        "z"=>0,
      ]
      属性h.each{|k,v|
        # 公式k="_#{k}_formula"
        设置公式值_单个(e,k,v)
      }
      $dc_observers.get_class_by_version(e).redraw_with_undo(e)
    end

    def 清除公式(arg)
      e=返回第一个选择对象
      if e==nil
        return
      end
      属性a=arg["哪个"]
      属性a.each{|a|
        # 原属性值=读取动态属性单个(e,a)
        # puts "原属性值👉#{原属性值}"
        清除动态属性单个(e,"_#{a}_formula")
        # 不设置回去的话，组件有可能跑了，碰到再处理
        # 设置公式值_单个(e,a,原属性值)#非公式的方式
      }

      # 属性h=Hash[]
      # 属性h["_x_formula"]=""
      # 属性h["_y_formula"]=""
      # 属性h["_z_formula"]=""
      # 属性h["_lenx_formula"]=""
      # 属性h["_leny_formula"]=""
      # 属性h["_lenz_formula"]=""
      # 属性h.each{|k,v|
      # }
      # 属性h=Hash[]
      # 属性h["y"]=0
      # 属性h.each{|k,v|
      # }
      # # 设置公式值_单个(e,"_lengthunits","CENTIMETERS")

      $dc_observers.get_class_by_version(e).redraw_with_undo(e)

      显示对象信息(e,true)

      发送提示信息("方法完成")
    end

    def 生成装配件(arg)
      基准盒组件=生成单组件(Hash["组件名" => "基准盒"])
      # amtf.nil
      组件=生成空组件定义(arg["组件名"])
      entities=组件.definition.entities
      基准盒 = entities.add_instance 基准盒组件.definition, Geom::Transformation.new(Geom::Point3d.new 0,0,0)
      puts 基准盒
      设置基准盒样式(基准盒)
      # amtf.nil

      设置新柜子动态属性(组件,arg)
      设置到同父级一样大(基准盒)
      基准盒组件.erase!
      return 组件
      发送提示信息("方法完成")
    end

    def 设置基准盒样式(e)
      layer = @layers["0_1 参考"]
      if layer.nil?
        layer = @layers.add("0_1 参考")
        # Sketchup.active_model.line_styles.names
        line_style = Sketchup.active_model.line_styles["Dash dot"]
        layer.line_style = line_style
      end
      e.layer=layer

      material = @materials['透明红']
      if material.nil?
        material = @materials.add('透明红')
        material.alpha = 0.12
        material.color = 'red'
      end
      e.material=material
    end

    def 生成单组件(arg)
      组件名=arg["组件名"]
      组件=生成空组件定义(组件名)
      entities=组件.definition.entities
      lenx=(arg["lenx"]||1000).mm
      leny=(arg["leny"]||1000).mm
      lenz=(arg["lenz"]||1000).mm
      x=(arg["x"]||0).mm
      y=(arg["y"]||0).mm
      z=(arg["z"]||0).mm
      puts "lenz"
      puts lenx
      puts leny
      puts lenz

      pts = []
      pts[0] = [0, 0, 0]
      # puts "pts[0]"
      # puts pts[0]
      pts[1] = [lenx, 0, 0]
      pts[2] = [lenx, leny, 0]
      pts[3] = [0, leny, 0]
      # pts[0] = [0, 0, 0].transform(@编辑中tr)
      # puts "pts[0]"
      # puts pts[0]
      # pts[1] = [lenz, 0, 0].transform(@编辑中tr)
      # pts[2] = [lenz, leny, 0].transform(@编辑中tr)
      # pts[3] = [0, leny, 0].transform(@编辑中tr)
      # Add the face to the entities in the model
      face = entities.add_face(pts)
      # amtf.nil
      # status = face.reverse!
      # puts "面翻转结果👉 #{status}"
      status = face.pushpull(-lenz)
      # status = face.pushpull(-lenz, true)
      # amtf.nil

      if 组件名=="基准盒"
        puts "组件名==基准盒"
        entities.grep(Sketchup::Face) {|e|
          面法向朝向=判断面的法向(e)
          if 面法向朝向=="前"
            e.visible=false
          end
        }

        设置基准盒样式(组件)
      end

      组件原点 = Geom::Point3d.new(0, 0, 0)
      指定点 = Geom::Point3d.new(x, y, z)
      vector = 组件原点.vector_to 指定点
      vector.transform!(@编辑中tr)
      tr = Geom::Transformation.translation(vector)
      组件.transform! tr

      设置新板件动态属性(组件,arg)
      发送提示信息("方法完成")
      return 组件
    end

    def 组件唯一_含子级()
      选择对象=返回选择对象
      if 选择对象.nil?
        return
      end
      grep集合中组件s(选择对象).each{|e|
        组件唯一_含子级_单个(e)
      }
      发送提示信息("方法完成")
    end
    def 添加xyz属性_含子级()
      选择对象=返回选择对象
      if 选择对象.nil?
        return
      end
      grep集合中组件s(选择对象).each{|e|
        添加xyz属性_单个(e)
        $dc_observers.get_class_by_version(e).redraw_with_undo(e)
      }
      发送提示信息("方法完成")
    end

    def 组件唯一_含子级_单个(e)
      puts "e.make_unique 👇"
      puts e.make_unique
      grep组件的子组件s(e).each{|ee|
        组件唯一_含子级_单个(ee)
      }
    end
    def 添加xyz属性_单个(e)
      # e=e.definition
      e.set_attribute 'dynamic_attributes', "x", ""
      e.set_attribute 'dynamic_attributes', "y", ""
      e.set_attribute 'dynamic_attributes', "z", ""
      e.set_attribute 'dynamic_attributes', "lenx", ""
      e.set_attribute 'dynamic_attributes', "leny", ""
      e.set_attribute 'dynamic_attributes', "lenz", ""
      e.set_attribute 'dynamic_attributes', "rotx", ""
      e.set_attribute 'dynamic_attributes', "roty", ""
      e.set_attribute 'dynamic_attributes', "rotz", ""
      e.set_attribute 'dynamic_attributes', "_lengthunits", "CENTIMETERS"
      grep组件的子组件s(e).each{|ee|
        添加xyz属性_单个(ee)
      }
    end

    def 设置新柜子动态属性(e,arg)
      h=Hash[]
      h["_name"]="a#{e.definition.name}"
      h["_lengthunits"]="CENTIMETERS"
      # h["scaletool"]=Hash["常规值"=>120]
      h["scaletool"]=120

      h.each{|k,v|
        改属性_单个_传字典(e,k,v)
      }
      $dc_observers.get_class_by_version(e).redraw_with_undo(e)
    end

    def 设置新板件动态属性(e,arg)
      h=Hash[]
      h["_name"]="a#{e.definition.name}"
      h["_lengthunits"]="CENTIMETERS"
      # h["scaletool"]=Hash["常规值"=>120]
      h["scaletool"]=120


      组件名=arg["组件名"]
      case 组件名
        when "左侧板","右侧板"
          h["lenx"]=Hash["公式值"=>arg["lenx"]]
        when "面板","背板"
          # h[公式k("leny")]=arg["leny"]
          h["leny"]=Hash["公式值"=>arg["leny"]]
        when "底板","顶板"
          # h[公式k("lenz")]=arg["lenz"]
          h["lenz"]=Hash["公式值"=>arg["lenz"]]
      end

      h.each{|k,v|
        # 改属性_单个_k值确定v值可能要转换(e,k,v)
        改属性_单个_传字典(e,k,v)
      }
      # 设置公式值_单个(e,"_lengthunits","CENTIMETERS")

      $dc_observers.get_class_by_version(e).redraw_with_undo(e)
    end

    def 生成空组件定义(组件名,transform=Geom::Transformation.new(Geom::Point3d.new 0,0,0))
      设置常用对象
      transform=transform* @编辑中tr
      definitions = @model.definitions
      componentdefinition = definitions.add 组件名
      instance = @entities.add_instance componentdefinition, transform
      Sketchup.active_model.active_view.invalidate
      return instance
    end


  def 替代或重叠另一个对象(被替代对象)
    设置常用对象
    puts "替代或重叠另一个对象👇………………………………………………………………………………"
    边界h=获取边界h(被替代对象)

    原点=边界h["box"].corner(0).transform(被替代对象.transformation) #直接到了全局坐标系
    原点=原点.transform(@model.edit_transform.inverse)
    # 原点=边界h["box"].corner(0)

    被替代对象位置与尺寸h=Hash[
      "x"=>原点[0],
      "y"=>原点[1],
      "z"=>原点[2],
      "lenx"=>边界h["lenx"],
      "leny"=>边界h["leny"],
      "lenz"=>边界h["lenz"]
    ]
    puts 被替代对象位置与尺寸h
    被替代对象位置与尺寸h=转mm可选取整(被替代对象位置与尺寸h)
    puts 被替代对象位置与尺寸h

    e=@去替代对象.make_unique
    if @加公式
       # 全部xyz公式全称=返回全部xyz公式全称
      # 被替代对象xyz公式h=读取全部xyz动态属性h(被替代对象)
      被替代对象xyz公式h=Hash[]
      A全部xyz简称.each{|k|
        # 原属性值=读属性_单个(被替代对象,k,false)
        转mm=false
        属性值字典=读属性_单个_字典形式V2(被替代对象,k,转mm)
        被替代对象xyz公式h[k]=属性值字典["公式值"]
      }
      # amtf.nil

      属性h=Hash[]
      属性h["scaletool"]=120
      属性h["_lengthunits"]="CENTIMETERS"

      被替代对象xyz公式h.each{|k,v|
        puts "被替代对象 原来的xyz公式h 值  #{v}"
        公式k="_#{k}_formula"
        # puts v
        if v==nil
          v=被替代对象位置与尺寸h[k]
        end
        属性h[公式k]=v
      }

      # puts e
      # puts "e=去替代对象"
      属性h.each{|k,v|
        puts "改属性_单个_k值确定v值可能要转换 之前 k👉#{k}  v👉#{v}"
        改属性_单个_k值确定v值可能要转换(e,k,v)
      }
      puts "改属性_单个_传字典  完成了"
      $dc_observers.get_class_by_version(e).redraw_with_undo(e)
    else #使用缩放的方式重叠
      缩放移动对象_基层(@去替代对象,边界h,0,0)
    end

    @去替代对象.visible=true
    if @替代
      被替代对象.erase!
    end
    puts "替代或重叠另一个对象  完成👆…………………………………………………………"
    # amtf.nil
    发送提示信息("方法完成")
  end


  end #AMTF窗口

  class SLCT_移动到鼠标位置
    include AMTF_mixin
    def initialize(拟移动对象)
      设置常用对象
      @拟移动对象=拟移动对象
      @选择点 = Sketchup::InputPoint.new
    end

    def onMouseMove(flags, x, y, view)
      @选择点.pick( view, x, y )
      指定点 = @选择点.position
      puts "指定点👉#{指定点}"

      边界h=获取边界h(@拟移动对象)
      组件原点=边界h["box"].corner(0).transform(@拟移动对象.transformation)

      vector = 组件原点.vector_to 指定点
      puts "vector#{vector}"
      vector.transform!(@编辑中tr)
      puts "vector#{vector}"
      tr = Geom::Transformation.translation(vector)
      @拟移动对象.transform! tr
    end

    def deactivate(view)
      # @去替代对象.visible=true
    end

    def onCancel(flag, view)
      # @去替代对象.visible=true

    end

    def onLButtonDown flags,x,y,view
      # puts "就放在这里"
      # @选择点.pick( view, x, y )
      # 指定点 = @选择点.position
      # puts "指定点👉#{指定点}"

      # 组件原点 = Geom::Point3d.new(0, 0, 0)
      # vector = 组件原点.vector_to 指定点
      # puts "vector#{vector}"
      # vector.transform!(@编辑中tr)
      # puts "vector#{vector}"
      # tr = Geom::Transformation.translation(vector)
      # @拟移动对象.transform! tr

      view.model.select_tool(nil)
    end
  end

  class SLCT_替代或重叠另一个对象
    include AMTF_mixin
    def initialize(去替代对象,dlg)
      @sel = Sketchup.active_model.selection
      @去替代对象=去替代对象
      @去替代对象.visible=false
      @dlg=dlg
    end

    def onMouseMove(flags, x, y, view)
      ph = view.pick_helper
      ph.do_pick(x, y)
      best = ph.best_picked
      if ((best.class == Sketchup::Group) || (best.class == Sketchup::ComponentInstance))
        @sel.clear
        @sel.add(best)
        @被替代对象=best
      else
        @被替代对象=nil
        @sel.clear
      end
    end

    def deactivate(view)
      @去替代对象.visible=true
    end

    def onCancel(flag, view)
      @去替代对象.visible=true

    end

    def onLButtonDown flags,x,y,view
      puts "@被替代对象"
      puts @被替代对象
      if @被替代对象!=nil
        puts "@替代"
        puts @替代
        @dlg.替代或重叠另一个对象(@被替代对象)
        view.model.select_tool(nil)
      end
    end
  end

end # module amtf_su
