require 'sketchup.rb'
module AMTF
class AF画柜子
  include AMTF_mixin
  def initialize(组件名)
    @ip  = Sketchup::InputPoint.new
    @ip1 = Sketchup::InputPoint.new
    @ip2 = Sketchup::InputPoint.new
    @cline = nil
    @start_line = []
    @组件全名 = File.join(get_dir,组件名).freeze
  end

  # def get_dir
  #   file = __FILE__.dup
  #   file.force_encoding("UTF-8")
  #   if file.respond_to?(:force_encoding)
  #     f = File.basename(file, '.*').freeze
  #     p "File.basename="+f
  #     r = File.dirname(file).freeze+"/组件模板"
  #     p "File.dirname="+r
  #     return r
  #     # p "File.join(r,f)="+File.join(r,f)
  #     # return File.join(r,f)
  #   end
  # end

  def self.options
	  if $dc_observers.get_latest_class.configure_dialog_is_visible
	    $dc_observers.get_latest_class.close_configure_dialog
	  else
	    $dc_observers.get_latest_class.show_configure_dialog
	  end
	  Sketchup.send_action("selectSelectionTool:")
  end

  def reset(view = nil)
    @points = []
    @height = 0
    @state = -1
    @ip.clear
    @ip1.clear
    @ip2.clear
    @start_line = []
    @face_normal = Z_AXIS
    if @cline&&@cline.valid?
      @cline.erase!
    end
    @cline = nil
    view.invalidate if view
    Sketchup::set_status_text "", SB_VCB_LABEL
    Sketchup::set_status_text "", SB_VCB_VALUE
    Sketchup::set_status_text "点击参考点"
  end

  def activate
    self.reset
  end
  def deactivate(view)
    self.reset(view)
  end
  def onMouseMove(flags, x, y, view)
    self.set_current_point(x, y, view)
  end
  def onLButtonDown(flags, x, y, view)
    self.set_current_point(x, y, view)
    self.increment_state(view)
  end
  def onKeyDown(key, rpt, flags, view)
    if( key == CONSTRAIN_MODIFIER_KEY && rpt == 1 )
      @shift_down_time = Time.now
      if( view.inference_locked? )
        view.lock_inference
      elsif( @state == 0 )
        view.lock_inference @ip, @ip2
      elsif( @state == 1 )
        view.lock_inference @ip, @ip1
      end
    end
  end
  def onKeyUp(key, rpt, flags, view)
    if(key == CONSTRAIN_MODIFIER_KEY && view.inference_locked? && (Time.now - @shift_down_time) > 0.5 )
      view.lock_inference
    end
  end
  def onCancel(flag, view)
    self.reset(view)
  end
  def onUserText(text, view)
    begin
      value = text.to_l
    rescue
      UI.beep
      value = nil
      Sketchup::set_status_text "", SB_VCB_VALUE
    end
    return if !value
    case @state
    when 0
      vec = @ip.position-@ip2.position
      if( vec.length > 0.0 )
        vec.length = value
        @ip1 = Sketchup::InputPoint.new(@ip2.position.offset(vec))
        self.increment_state(view)
        view.invalidate
        @input = true
      end
    when 1
      vec = @points[1] - @points[0]
      if( vec.length > 0.0 )
        vec.length = value
        @width = value
        @points[1] = @points[0].offset(vec)
        view.invalidate
        self.increment_state(view)
      end
    when 2
      vec = @points[3] - @points[0]
      if( vec.length > 0.0 )
        vec.length = value
        @depth = value
        @points[2] = @points[1].offset(vec)
        @points[3] = @points[0].offset(vec)
        self.increment_state(view)
      end
    when 3
      @height = value
      self.increment_state(view)
    end
    view.invalidate
  end

  def set_current_point(x, y, view)
    case @state
    when -1
      @ip.pick view, x, y
      @ip2.copy! @ip
    when 0
      @ip.pick view, x, y,@ip2
      @ip1.copy! @ip
      if @ip1.valid?
        @points[0] = @ip1.position
        @points[4] = @points[0]
      end
      width = @ip2.position.distance @ip.position
      Sketchup::set_status_text width.to_s, SB_VCB_VALUE
    when 1
      @points[0] = @ip1.position
      @points[4] = @points[0]
      return nil if( !@ip.pick(view, x, y, @ip1) )
      if @ip.valid?
        @points[1] = @ip.position
        @width = @points[0].distance @points[1]
        Sketchup::set_status_text @width.to_s, SB_VCB_VALUE
      end
    when 2
      return nil if(!@ip.pick(view, x, y,@ip1))
      if @ip.valid?
        return nil if @ip==@ip1
        pt1 = @ip.position
        pt2 = pt1.project_to_line @points
        vec = pt1 - pt2
        @depth = vec.length
        if( @depth > 0 )
          square_point = pt2.offset(vec, @width)
          if( view.pick_helper.test_point(square_point, x, y) )
            @depth = @width
            @points[2] = @points[1].offset(vec, @depth)
            @points[3] = @points[0].offset(vec, @depth)
            view.tooltip = "Square"
          else
            @points[2] = @points[1].offset(vec)
            @points[3] = @points[0].offset(vec)
          end
        else
          @points[2] = @points[1]
          @points[3] = @points[0]
        end
        Sketchup::set_status_text @depth.to_mm.round(2).to_s, SB_VCB_VALUE
      end
    when 3
      @ip.pick view, x, y,@ip1
      if @ip.valid?
        v1 = @points[1].vector_to(@points[0])
        v2 = @points[1].vector_to(@points[2])
        @face_normal = v1*v2.normalize
        @cline ||= Sketchup.active_model.entities.add_cline(@points[2],@face_normal)
        plane = [@points[0],@face_normal]
        pt1 = @ip.position
        length = pt1.distance_to_plane(plane)
        pt2  = pt1.project_to_plane plane
        vec = pt2.vector_to(pt1).normalize
        @face_normal = vec  if vec.valid?
        @height = length
        Sketchup::set_status_text @height.to_mm.round, SB_VCB_VALUE
      end
    end
    if @ip.valid?
      view.invalidate
      view.tooltip = @ip.tooltip
    end
  end

  def createGeometry(view)
    if @height>0
      model = Sketchup.active_model
      model.start_operation("Draw panel")
      ents = model.active_entities
      if File.exist? @组件全名
        组件定义 = model.definitions.load @组件全名
        b = 组件定义.bounds
        边界宽 = b.width
        边界高 = b.height
        边界深 = b.depth
        sr = Geom::Transformation.scaling(@points[0],@width/边界宽,@depth/边界高,@height/边界深)
        x = @points[0].vector_to @points[1]
        y = @face_normal*x
        tr = Geom::Transformation.new(x,y,@face_normal,@points[0])
        c = ents.add_instance 组件定义,Geom::Transformation.new
        c.make_unique
        c.transform! sr
        p "kk:"+$dc_observers.to_s
        $dc_observers.get_class_by_version(c).redraw_with_undo(c)
        c.transformation = tr
      end
      self.reset(view)
    else
      self.reset(view)
    end
  end

  def increment_state(view)
    @state += 1
    case @state
    when 0
      @ip1.copy! @ip
      @start_line[0] = @ip2.position
      Sketchup::set_status_text "点击起点"
      Sketchup::set_status_text "长度", SB_VCB_LABEL
      Sketchup::set_status_text "", SB_VCB_VALUE
    when 1
      @start_line[1] = @ip1.position
      Sketchup::set_status_text "点击右/宽度"
      Sketchup::set_status_text "长度", SB_VCB_LABEL
      Sketchup::set_status_text "", SB_VCB_VALUE
    when 2
      Sketchup::set_status_text "点击后/深度"
      Sketchup::set_status_text "长度", SB_VCB_LABEL
      Sketchup::set_status_text "", SB_VCB_VALUE
    when 3
      Sketchup::set_status_text "点击上/高度"
      Sketchup::set_status_text "高度", SB_VCB_LABEL
      Sketchup::set_status_text "", SB_VCB_VALUE
    when 4
      self.createGeometry(view)
    end
  end

  def draw(view)
    if( @ip.valid? && @ip.display? )
      @ip.draw(view)
    end
    self.drawGeometry(view)
  end

  def drawGeometry(view)
    view.line_width = 2
    case @state
    when 0
      view.set_color_from_line(@ip2, @ip)
      inference_locked = view.inference_locked?
      view.line_width = 4 if inference_locked
      view.line_stipple = "-"
      view.draw_line(@ip2.position,@ip1.position)
    when 1
      view.line_stipple = ""
      view.set_color_from_line(@ip1, @ip)
      inference_locked = view.inference_locked?
      view.line_width = 4 if inference_locked
      view.draw(GL_LINE_STRIP, @points[0], @points[1])
      view.line_stipple = "-"
      view.draw_line(@start_line)
    when 2
      view.line_stipple = ""
      view.drawing_color = "red"
      view.draw(GL_LINE_STRIP, @points)
      view.line_stipple = "-"
      view.draw_line(@start_line)
    when 3
      view.drawing_color = "red"
       view.line_stipple = ""
      view.draw(GL_LINE_STRIP, @points)
      points = @points.map{|pt| pt.offset(@face_normal,@height)}
      view.draw(GL_LINE_STRIP, points)
      points.each_with_index do |pt,i|
        view.draw(GL_LINE_STRIP, pt,@points[i])
      end
      view.line_stipple = "-"
      view.draw_line(@start_line)
    end
  end

  def getExtents
    bb = Geom::BoundingBox.new
    case @state
    when -1,0
      if( @ip.valid? && @ip.display? )
        bb.add @ip.position
      end
    else
      bb.add @points.compact
      bb.add @ip.position
    end
    bb
  end


end# Class
end # module
