require 'sketchup.rb'
require 'json'
module AMTF_mixin2
  include AMTF_mixin

  def 缩放移动对象_基层(对象,新边界h,缩放前基准点box角点序号,缩放后基准点box角点序号)
    设置常用对象
    @model.start_operation("缩放移动对象")
      ents = @model.active_entities
      # 边界大小=获取边界大小_加入原点box(对象)
      旧边界h=获取边界h_只认边和面(对象,true,true)

      xscale=新边界h["lenx"]/旧边界h["lenx"]
      yscale=新边界h["leny"]/旧边界h["leny"]
      zscale=新边界h["lenz"]/旧边界h["lenz"]
      # puts "xscale=="
      # puts xscale
      # puts yscale
      # puts zscale
      # box=边界大小[3]
      box=新边界h["box"]
      if 缩放前基准点box角点序号==999 #基准在中心=true
        缩放前基准点=box.center
      else
        缩放前基准点=box.corner(缩放前基准点box角点序号)
      end
      缩放前基准点=缩放前基准点.transform(新边界h["tr"]) #转换到了全局坐标
      puts "缩放前基准点"
      puts 缩放前基准点

      tr原来=对象.transformation
      对象trx=tr原来.xaxis
      对象try=tr原来.yaxis
      对象trz=tr原来.zaxis
      puts "tr原来 x y z"
      puts 对象trx
      puts 对象try
      puts 对象trz

      puts "tr原来.origin:"
      puts tr原来.origin

      #对齐到世界组件坐标系，↓
      旋转组件轴到全局坐标系(对象)
      # x = Geom::Vector3d.new(1, 0, 0)
      # y = Geom::Vector3d.new(0, 1, 0)
      # z = Geom::Vector3d.new(0, 0, 1)
      # tr = Geom::Transformation.new(x,y,z,box原点)
      # 对象.transformation = tr
      # amtf.nil
      sr = Geom::Transformation.scaling(缩放前基准点,xscale,yscale,zscale)
      #就得先旋转，再缩放↓
      对象.transform! sr
      # amtf.nil

      # 不这样来一下，旋转回去前 后面缩放的操作又退回去了！！！
      # $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)
      # 我现在猜，其实是因为下面的transformation，把scale重新变成1了！就不能只是选中吗？
      # 还是这样来一下，可以更新纹理！！！
      $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)

      # amtf.nil
      #对齐到局部组件坐标系，↓
      旋转组件轴到原坐标系(对象,tr原来)
      # x = 对象trx.transform(@编辑中tr)
      # y = 对象try.transform(@编辑中tr)
      # z = 对象trz.transform(@编辑中tr)
      # puts "对象trx.transform(@编辑中tr)"
      # puts x
      # puts y
      # puts z
      # # x = Geom::Vector3d.new(1, 0, 0).transform(@编辑中tr)
      # # y = Geom::Vector3d.new(0, 1, 0).transform(@编辑中tr)
      # # z = Geom::Vector3d.new(0, 0, 1).transform(@编辑中tr)
      # tr = Geom::Transformation.new(x,y,z,box原点)
      # 对象.transformation = tr

      # amtf.nil
      # 边界大小2=获取边界大小_加入原点box(对象)
      边界h2=获取边界h_只认边和面(对象,true,true)
      # box2=边界大小2[3]
      box2=边界h2["box"]

      if 缩放后基准点box角点序号==999 #基准在中心=true
        缩放后基准点=box.center
      else
        缩放后基准点=box.corner(缩放后基准点box角点序号)
      end
      缩放后基准点=缩放后基准点.transform(对象.transformation) #转换到了全局坐标
      vector = 缩放后基准点.vector_to 缩放前基准点
      tr = Geom::Transformation.translation(vector)
      对象.transform! tr
    @model.commit_operation
  end

  def 缩放移动对象(对象,新边界尺寸)
    设置常用对象
    @model.start_operation("缩放移动对象")
      ents = @model.active_entities
      # 边界大小=获取边界大小_加入原点box(对象)
      旧边界h=获取边界h_只认边和面(对象,true,true)

      旧边界h["lenx"] = 旧边界h["lenx"].to_mm
      旧边界h["leny"] = 旧边界h["leny"].to_mm
      旧边界h["lenz"] = 旧边界h["lenz"].to_mm
      # 旧边界h["lenx"] = 边界大小[0].to_mm
      # 旧边界h["leny"] = 边界大小[1].to_mm
      # 旧边界h["lenz"] = 边界大小[2].to_mm
      xscale=1
      yscale=1
      zscale=1
      if 新边界h["lenx"]!=0
        xscale=新边界h["lenx"]/旧边界h["lenx"]
      end
      if 新边界h["leny"]!=0
        yscale=新边界h["leny"]/旧边界h["leny"]
      end
      if 新边界h["lenz"]!=0
        zscale=新边界h["lenz"]/旧边界h["lenz"]
      end
      # puts "xscale=="
      # puts xscale
      # puts yscale
      # puts zscale
      # box=边界大小[3]
      box=旧边界h["box"]
      box原点=box.corner(0)
      box原点=box原点.transform(对象.transformation) #转换到了全局坐标

      缩放后基准点box角点序号=0
      x基准=新边界尺寸[3]
      y基准=新边界尺寸[4]
      z基准=新边界尺寸[5]
      if x基准==1 && y基准==0 && z基准==0
        缩放后基准点box角点序号=1
      end
      if x基准==0 && y基准==1 && z基准==0
        缩放后基准点box角点序号=2
      end
      if x基准==1 && y基准==1 && z基准==0
        缩放后基准点box角点序号=3
      end
      if x基准==0 && y基准==0 && z基准==1
        缩放后基准点box角点序号=4
      end
      if x基准==1 && y基准==0 && z基准==1
        缩放后基准点box角点序号=5
      end
      if x基准==0 && y基准==1 && z基准==1
        缩放后基准点box角点序号=6
      end
      if x基准==1 && y基准==1 && z基准==1
        缩放后基准点box角点序号=7
      end
      缩放前基准点=box.corner(缩放后基准点box角点序号)
      基准在中心=新边界尺寸[6]
      if 基准在中心
        缩放前基准点=box.center
      end

      # puts "缩放前基准点"
      # puts 缩放前基准点
      对象tr=对象.transformation
      对象trx=对象tr.xaxis
      对象try=对象tr.yaxis
      对象trz=对象tr.zaxis
      # puts "对象trx y z"
      # puts 对象trx
      # puts 对象try
      # puts 对象trz

      缩放前基准点=缩放前基准点.transform(对象.transformation) #转换到了全局坐标
      # puts "缩放前基准点"
      # puts 缩放前基准点
      # 缩放前基准点=缩放前基准点.transform(@编辑中tr)
      # puts 缩放前基准点

      #对齐到世界组件坐标系，↓
      tr原来 = 对象.transformation
      旋转组件轴到全局坐标系(对象)
      # x = Geom::Vector3d.new(1, 0, 0)
      # y = Geom::Vector3d.new(0, 1, 0)
      # z = Geom::Vector3d.new(0, 0, 1)
      # tr = Geom::Transformation.new(x,y,z,box原点)
      # 对象.transformation = tr
      # amtf.nil
      sr = Geom::Transformation.scaling(缩放前基准点,xscale,yscale,zscale)
      #就得先旋转，再缩放↓
      对象.transform! sr
      # amtf.nil

      # 不这样来一下，旋转回去前 后面缩放的操作又退回去了！！！
      # $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)
      # 我现在猜，其实是因为下面的transformation，把scale重新变成1了！就不能只是选中吗？
      # 还是这样来一下，可以更新纹理！！！
      $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)


      # amtf.nil
      #对齐到局部组件坐标系，↓
      旋转组件轴到原坐标系(对象,tr原来)
      # x = 对象trx.transform(@编辑中tr)
      # y = 对象try.transform(@编辑中tr)
      # z = 对象trz.transform(@编辑中tr)
      # puts "对象trx.transform(@编辑中tr)"
      # puts x
      # puts y
      # puts z
      # # x = Geom::Vector3d.new(1, 0, 0).transform(@编辑中tr)
      # # y = Geom::Vector3d.new(0, 1, 0).transform(@编辑中tr)
      # # z = Geom::Vector3d.new(0, 0, 1).transform(@编辑中tr)
      # tr = Geom::Transformation.new(x,y,z,box原点)
      # 对象.transformation = tr

      # amtf.nil
      # 边界大小2=获取边界大小_加入原点box(对象)
      边界h2=获取边界h_只认边和面(对象,true,true)
      # box2=边界大小2[3]
      box2=边界h2["box"]
      缩放后基准点=box2.corner(缩放后基准点box角点序号)
      if 基准在中心
        缩放后基准点=box2.center
      end
      缩放后基准点=缩放后基准点.transform(对象.transformation) #转换到了全局坐标
      vector = 缩放后基准点.vector_to 缩放前基准点
      tr = Geom::Transformation.translation(vector)
      对象.transform! tr
    @model.commit_operation
  end


end #md
