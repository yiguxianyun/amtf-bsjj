module AMTF::A布尔select_tool_mixin
  MODIFIED_DICTIONARY = 'AF BoolTool Modified Definition'
  MODIFIED_DIC_KEY = 'Modified'

  #start operation based on instance variable bool_operation_string
  def start_operation(string)
    Sketchup.active_model.start_operation(string, true)
  end

  def validate_operands(first, second = nil)

    begin

      AMTF::Boolean.validate_operands(first, second)

    rescue Exception => e

      res = UI.messagebox("警告:\n#{e.message}\n\n结果可能不如预期。是否要继续？", MB_YESNO)

      return false if (res == 7)

    end



    return true

  end
  # returns the bounding box that encloses the array of groups of components

  # @param [Array] instances an array that includes only groups or component instances

  # @return [BoundingBox]

  def get_bounds_of_instances(instances)

    box = Geom::BoundingBox.new

    instances.each { |inst| box.add(inst.bounds) }

    box

  end

  def onCancel(reason, view)

    reset

  end

  def set_bool_tools_modified_attribute(instances)
    instances.each{ |i|
      # puts "MODIFIED_DICTIONARY:#{MODIFIED_DICTIONARY}"
      puts "i.deleted?:#{i.deleted?}"
      # defin.set_attribute(MODIFIED_DICTIONARY, MODIFIED_DIC_KEY, true)
    }
    # definitions = instances.collect{ |i| i.definition }.uniq
    # definitions.each{ |defin|
    #   puts "MODIFIED_DICTIONARY:#{MODIFIED_DICTIONARY}"
    #   puts "defin.deleted?:#{defin.deleted?}"
    #   defin.set_attribute(MODIFIED_DICTIONARY, MODIFIED_DIC_KEY, true)
    # }
  end
end
