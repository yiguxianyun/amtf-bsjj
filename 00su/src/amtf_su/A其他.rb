require 'sketchup.rb'
require File.join(__FILE__, '../amtf_mixin.rb')
require 'json'
require "win32ole"

# require 'open3'
# require 'win32ole'
module AMTF
  class A其他
    include AMTF_mixin
    def 布尔
      Sketchup.active_model.select_tool(A布尔select_tool_修剪.new)
      # Sketchup.active_model.select_tool(BoolTrimTool.new)
      # arg=Hash[]
      # icon=File.join(PATH, "Icons", "trim_cursor_1.png")
      # icon2=File.join(PATH, "Icons", "trim_cursor_2.png")
      # arg["@cursor"]=UI.create_cursor(icon, 12, 12)
      # arg["@cursor2"]=UI.create_cursor(icon2, 12, 12)
      # arg["@bool_operation_string"]="修剪"
      # Sketchup.active_model.select_tool(A布尔select_tool.new(arg))

      # 发送提示信息("方法完成")
    end

    def 视图缩放到选择对象
      model = Sketchup.active_model
      view = model.active_view
      view = view.zoom Sketchup.active_model.selection
    end

    def 不同板厚放不同标记层
      板厚hash=获取板厚()
      # 发送提示信息(板厚hash)
      model = Sketchup.active_model
      layers = model.layers
      板厚hash.each{|k,v|
        发送提示信息(k)
        puts k
        图层对象 = layers.add("板厚__#{k}")
        图层对象.visible=true
        v.each{|e|
          # 发送提示信息(提示信息)
          e.layer=图层对象

        }
      }
      num_layers_removed = layers.purge_unused
      发送提示信息("方法完成")
    end

    def 不同材质放不同标记层
      设置常用对象
      材质hash=获取entities材质(@顶层entities)
      # 发送提示信息(板厚hash)
      model = Sketchup.active_model
      layers = model.layers

      材质hash.each{|k,v|
        发送提示信息(k)
        puts k
        图层对象 = layers.add("材质__#{k}")
        图层对象.visible=true
        v.each{|e|
          # 发送提示信息(提示信息)
          e.layer=图层对象
        }
      }
      num_layers_removed = layers.purge_unused
      puts "num_layers_removed"
      puts num_layers_removed
      发送提示信息("方法完成")
    end

    def 返回json
      json = File.read(path = File.join(__dir__, 'mobanbiao.json'))
      return @模板表 = json
    end

    def 组件属性导出到json
      model = Sketchup.active_model
      e=model.selection[0]
      动态属性h=遍历全部组件属性(e)
      组件实例__定义名="#{e.name}__#{e.definition.name}"
      文件名 = File.join(__dir__, "#{组件实例__定义名}.json")
      File.write(文件名, 动态属性h.to_json)
    end

    def 尺寸标注放到指定图层(指定图层)
      model = Sketchup.active_model
      layers = model.layers
      图层对象 = layers.add(指定图层)
      # text.layer = new_layer
      尺寸标注放到指定图层_递归(model,图层对象)
      发送提示信息("方法完成")
    end

    def 尺寸标注放到指定图层_递归(e,图层对象)
      if e.is_a?( Sketchup::ComponentInstance )
        entities=e.definition.entities
      else
        entities = e.entities
      end
      entities.grep(Sketchup::Dimension) {|e|
        puts e.layer.name
        if e.layer.name!=图层对象.name
          e.layer=图层对象
        end
      }
      entities.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          尺寸标注放到指定图层_递归(e,图层对象)
        end
      }
    end

    def 刷新选中动态组件和群组
      selection = Sketchup.active_model.selection
        unless selection.empty?
          selection.grep(Sketchup::ComponentInstance) do |e|
              $dc_observers.get_class_by_version(e).redraw_with_undo(e)
        end
        selection.grep(Sketchup::Group) do |e|
            $dc_observers.get_class_by_version(e).redraw_with_undo(e)
        end
      end
    end


    def 遍历全部组件属性(e)
      # model = Sketchup.active_model
      # e=model.selection[0]
      组件实例__定义名="#{e.name}__#{e.definition.name}"
      puts 组件实例__定义名
      组件实例名=e.name
      动态属性h=Hash[]
      源动态属性dic=Hash[]
      if e.is_a? Sketchup::ComponentInstance
        定义或实例对象=e.definition
      else
        定义或实例对象=e
      end
      # 源动态属性dic["dynamic_attributes"]=Hash[]
      if 定义或实例对象.attribute_dictionaries != nil
        if 定义或实例对象.attribute_dictionaries["dynamic_attributes"]!= nil
          源动态属性dic = 定义或实例对象.attribute_dictionaries["dynamic_attributes"]
        end
      end
      源动态属性dic.each { | key, value |
        属性值=源动态属性dic[key]
        动态属性h[key]=属性值
      }
      puts 动态属性h.to_json
    end #遍历组件属性

    def 导入model
      require 'win32ole'#调用win32ole
# excel=WIN32OLE.new('excel.application')
# excel.Visible=true
# excel.WorkBooks.Add

# excel.Range("a1").value=3
# excel.Range('a2').value=2
# excel.Range('a3').value=1
# excel.Range('b1').value="win32ole操作excel栗子"
# excel.Range('a1:a3').select

# excel_chart=excel.charts.add
# excel_chart.type=-4100
# excel.ActiveWorkbook.SaveAs("c:\\test.xls")
# excel.ActiveWorkbook.Close(0)
# excel.Quit

      require 'win32ole'#调用win32ole
      cd = WIN32OLE.new('MSComDlg.CommonDialog')
      cd.filter = "All Files(*.*)|*.*" +"|Ruby Files(*.rb)|*.rb"
      cd.filterIndex = 2
      cd.maxFileSize = 128    # Set MaxFileSize
      cd.showOpen()
      file = cd.fileName      # Retrieve file, path
      if not file or file==""
        puts "No filename entered."
      else
        puts "The user selected: #{file}\n"
      end
    end

  end#class
end # module amtf_su
