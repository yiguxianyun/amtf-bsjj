require 'sketchup.rb'
# require File.join(__FILE__, '../amtf_mixin.rb')
# require 'win32ole'
module AMTF
  class AF另存排版
    include AMTF_mixin
    def 孤立()
      设置常用对象
      选择的ents=[]
      if @selection.count == 0
        UI.messagebox "请选择一个 组件 或者 群组 ！"
        return false
      else
        entities=@entities
        entities.each {|e|
          p @selection.include?(e)
          if !@selection.include?(e)
            e.visible=false
          end
        }
      end
    end

    def 移除组件and子级材质
      entities=返回全部或选择对象
      for e in entities do
        遍历对象材质(e)
      end
      发送提示信息("方法完成")
    end

    def 移除组件材质
      entities=返回全部或选择对象
      for e in entities do
        遍历对象材质(e,false)
      end
      发送提示信息("方法完成")
    end

    def 遍历对象材质(e,递归子级=true)
      puts e
      # puts e.material
      if !e.material.nil?
        puts "e.material.name"
        puts e.material.name
        e.material=nil
      end
      if e.is_a?( Sketchup::Face )
        if !e.back_material.nil?
          puts "e.back_material"
          puts e.back_material.name
          e.back_material=nil
        end
      end
      if 递归子级
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          entities=组或群entities(e)
          entities.each {|ee|
            遍历对象材质(ee)
          }
        end
      end
    end

    def 选择同材质组件()
      设置常用对象
      选择的ents=[]
      if @selection.count == 0
        UI.messagebox "请选择一个 组件 或者 群组 ！"
        return false
      else
        群组=@selection[0]
        # 当前材质=获取组件第一个面的材质名称(群组)
        当前材质=获取组件or最大面材质名称(群组)


        # m=群组.material
        # 当前材质=(m != nil ? m.name : "未指定")
        # puts 当前材质
        提示信息="当前材质: #{当前材质}"
        发送提示信息(提示信息)
      end

      材质hash=获取entities材质(@顶层entities)
      材质hash.each{|k,v|
        if k==当前材质
          v.each{|e|
            @selection.add(e)
          }
        end
      }
    end


    def 遍历组件获取材质(e,拟匹配材质)
      if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
        组件定义名=e.definition.name
        组件实例名=e.name
        组件加实例名=组件定义名+"+"+组件实例名
        # p 组件加实例名
        m=e.material
        群组的材质=(m != nil ? m.name : "未指定")
        if 群组的材质==拟匹配材质
          # 不干啥
          提示信息="找到 "+组件加实例名+"  对应材质:"+拟匹配材质.to_s
          # 提示信息="找到 #{组件加实例名} 对应材质: #{拟匹配材质.to_s}  显示名：#{拟匹配材质.display_name}"

          p 提示信息
          发送提示信息(提示信息)
          @selection.add(e)
        end
        # 遍历子组件
        # e.definition.entities.each{|ee|
        #   遍历组件获取材质(ee,拟匹配材质)
        # }
      end
    end

    def 获取组件第一个面的材质名称(群组)
      entities = 群组.entities
      材质2=群组.material
      puts "材质名称=#{材质2.name}"
      entities.each {|ee|
        材质=ee.material

        if 材质 !=nil
          puts "材质名称=#{材质.name}"
          # puts "#{材质.display_name} ==== #{材质.display_name}"
        else
          puts "材质名称=nil"
        end
        # if ee.is_a?( Sketchup::Face )
        #   材质=ee.material
        #   puts "材质名称=#{材质.name}"
        #   # puts "#{材质.display_name} ==== #{材质.display_name}"
        #   return 材质
        # end
      }
    end


    def 更改背板厚度(背板厚度)
      设置常用对象
      puts "背板厚度:#{背板厚度}"
      背板厚度=背板厚度.to_f/25.4
      puts "背板厚度:#{背板厚度}"
      entities=@entities
      entities.each {|e|
        更改背板厚度主体(e,背板厚度)
      }
      selection = Sketchup.active_model.selection
      发送提示信息("方法完成")
    end

    def 更改背板厚度主体(e,背板厚度)
      背板代号s=[]
      #改三节抽屉抽屉背板：
      背板代号s.push "zmb31"
      #通用背板：
      背板代号s.push "bh1"
      #吊柜背板：
      背板代号s.push "tjjh1"
      #地柜背板：
      背板代号s.push "tjgd00"
      if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
        组件定义名=e.definition.name
        组件实例名=e.name
        组件加实例名=组件定义名+"+"+组件实例名
        # puts 组件加实例名
        关键词位置= 组件加实例名=~ /三节抽屉|通用背板|吊柜|地柜|薄背板/
        # p 关键词位置
        if 关键词位置!=nil
          puts 组件加实例名
          # 不干啥
          dictionaries = e.attribute_dictionaries
          dictionaries.each { |dictionary|
            # puts dictionary.name
            # k='lenz'
            # v=dictionary[k]
            # puts "#{k} = #{v}"
            dictionary.each { | key, value |
              # puts "#{key} = #{value}"
            }
           }

          for 背板代号 in 背板代号s do
            当前背板厚=(e.get_attribute "dynamic_attributes", 背板代号 ).to_f*25.4

            if 当前背板厚 < 10
              # value = e.set_attribute "dynamic_attributes", "tjjh1", 背板厚度
              value = e.set_attribute "dynamic_attributes", 背板代号, 背板厚度
              提示信息="更改 #{组件加实例名} 的背板厚度，从 #{当前背板厚} 到 #{(背板厚度*25.4)}"
              puts "提示信息:#{提示信息}"
              发送提示信息(提示信息)
              # amtf.nil
            end
          end
          # #改三节抽屉抽屉背板：
          # value = e.set_attribute "dynamic_attributes", "zmb31", 背板厚度
          # #改通用背板：
          # value = e.set_attribute "dynamic_attributes", "bh1", 背板厚度
          # #改吊柜背板：
          # 当前背板厚=(e.get_attribute "dynamic_attributes", "tjjh1" ).to_f*25.4
          # if 当前背板厚 < 10
          #   value = e.set_attribute "dynamic_attributes", "tjjh1", 背板厚度
          #   提示信息="更改 #{组件加实例名} 的背板厚度，从 #{当前背板厚} 到 #{(背板厚度*25.4)}"
          #   发送提示信息(提示信息)
          #   # amtf.nil
          # end
          # #改地柜背板：
          # value = e.set_attribute "dynamic_attributes", "tjgd00", 背板厚度
          $dc_observers.get_class_by_version(e).redraw_with_undo(e)
        else
          e.definition.entities.each{|ee|
            更改背板厚度主体(ee,背板厚度)
          }
        end
      end
    end

    def 导入多个模型
      设置常用对象
      model = Sketchup.active_model
      com_path = UI.openpanel("请选择拟导入的skp文件 可多选", '', 'SketchUp File|*.skp||')
      com_def = model.definitions.load com_path
      model.place_component(com_def)

    end

    def 另存为排版模型
      Sketchup::set_status_text __method__.to_s()+" ing"
      设置常用对象
      file=@model.path
      # # p @model.path
      # 路径 = File.dirname(file).freeze
      # 文件名 = File.basename(file, '.*')
      # today = Time.new;
      # 时间后缀= today.strftime("%Y%m%d_%H%M%S");
      # 另存文件名=文件名
      # if 另存文件名.include? '_排版'
      #   另存文件名=另存文件名.gsub!(/_排版\S*/, "")
      # end
      # 另存文件名=另存文件名+'_排版'+时间后缀
      # 另存全名=路径+'/'+另存文件名+'.skp'
      另存全名=生成唯一文件名(file,"排版")
      status = @model.save(另存全名)
      Sketchup::set_status_text __method__.to_s()+另存全名+" 完成！"
    end
    def cs删除指定图层
      ie = WIN32OLE.new('internetExplorer.Application')
      ie.visible = true

      # cd = WIN32OLE.new("MSComDlg.CommonDialog")

      # cd.filter = "All Files(*.*)|*.*" +"|Ruby Files(*.rb)|*.rb"
      # cd.filterIndex = 2

      # cd.maxFileSize = 128    # Set MaxFileSize

      # cd.showOpen()

      # file = cd.fileName      # Retrieve file, path

      # if not file or file==""
      #   puts "No filename entered."
      # else
      #   puts "The user selected: #{file}\\n"
      # end
    end
    def 删除指定图层_显示不该隐藏的层(排版预处理关键词)
      Sketchup::set_status_text __method__.to_s()+" ing"
      设置常用对象
      层s=@model.layers
      拟删除层=[]
      拟删除层名称=[]
      p 层s.size
      排版预处理关键词
      关键词=排版预处理关键词["拟删除图层"]

      # ruby端附加
      # 拟删除图层="|0_1 参考|0_1 硬装|0_1 硬装_屋顶|0_9 外部参考"
      # # puts "关键词"
      # # puts 关键词
      # 关键词=关键词 + 拟删除图层
      # puts 关键词
      层s.each {|e|
        # p e
        # 关键词="afu-参考|afu-五金|afu-尺寸标注|afu-开向|afu-拉手|afu-明装拉手|a-参考|a-房子"
        # !/#{关键词}/.match("afu-参考参考  afu-五金")
        if /#{关键词}/.match(e.name)
        # if e.name=~ /五金/
          # 层s.remove(e,true)
          # puts "拟删除层: #{e.name}"
          拟删除层.push e
          拟删除层名称.push e.name
        end
        if e.name=~ /切除工具实体/
          e.visible=true
        end
      }
      # amtf.nil
      拟删除层.each {|e|
          层s.remove(e,true)
      }
      # amtf.nil
      # puts "到底怎么啦？"
      提示信息="已删除图层："+拟删除层名称.to_s
      Sketchup::set_status_text 提示信息
    end

    def 删除隐藏项目不传参数
      设置常用对象
      删除隐藏项目(@entities)
    end
    def 删除隐藏项目(entities)
      原有的子组件s=@entities.grep(Sketchup::Group)+@entities.grep(Sketchup::ComponentInstance)
      原有的子组件s.to_a.each {|e|
        删除隐藏项目主体(e)
      }
    end
    def 删除隐藏项目主体(e)
      if e.hidden?
          e.erase!
      else
        entities=组或群entities(e)
        原有的子组件s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)
        原有的子组件s.to_a.each {|ee|
          删除隐藏项目主体(ee)
        }
      end
    end

    def 炸开所有子组件()
      选择对象=返回选择对象
      选择对象.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          # 当前层级=0
          if e.is_a?( Sketchup::ComponentInstance )
            entities=e.definition.entities
          else
            entities=e.entities
          end
          entities.each {|ee|
            if ( ee.is_a?( Sketchup::ComponentInstance ) || ee.is_a?( Sketchup::Group ))
              # 组件定义名=ee.definition.name
              # 组件实例名=ee.name
              # 组件加实例名=组件定义名+"+"+组件实例名
              # puts 组件加实例名
              炸开末级子组件(ee)
            end
          }
        end
      }
      发送提示信息("方法完成")
    end
    def 干涉检查
      # UI.messagebox("我执行了！")
      设置常用对象
      组件s=[]
      含子组件的组件s=[]
      @entities.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          组件名=e.definition.name
          # if !含子组件?(e)
            组件s.push e
          # else
          #   含子组件的组件s.push e
          # end
        end
      }

      materials = @model.materials
      干涉的组件s=[]
      相交出来的组件s=[]
      相交出来的空组件s=[]
      组件s2=组件s
      组件s.each {|e1|
        组件s2.delete(e1)
        组件s2.each {|e2|
          boundingbox1=e1.bounds
          boundingbox2=e2.bounds
          boundingbox = boundingbox1.intersect(boundingbox2)
          干涉体积=boundingbox.width*boundingbox.height*boundingbox.depth
          # puts 干涉体积>0.1
          if 干涉体积>0.1
            # 上面发现边界框存在干涉，但是
            # 边界框可能是歪的，进一步用实体布尔的方式检查干涉
            if 含子组件?(e1) or 含子组件?(e2)
              含子组件的组件s.push e1
              含子组件的组件s.push e2
            else
              group1 = e1.copy
              group2 = e2.copy
              result = group1.intersect(group2)
              # result.material="red"
              for v in result.entities do
                v.material="red"
              end

              # color = Sketchup::Color.new "Red"
              # # color.alpha = 50
              # alpha = color.alpha = 50
              # puts alpha
              # result.material=color

              volume = result.volume
              puts volume>0
              if volume>0#真干涉
                相交出来的组件s.push result
                if !干涉的组件s.include?(e1)
                  干涉的组件s.push e1
                end
                if !干涉的组件s.include?(e2)
                  干涉的组件s.push e2
                end
              else#假干涉，相交出来的假组件需要处理吗？
                # 啥也不干？
                相交出来的空组件s.push result
              end
            end
          end
        }
      }

      for e in 相交出来的空组件s do
        e.erase!
      end

      提示信息=""
      if 含子组件的组件s.count == 0 and 干涉的组件s.count == 0
        提示信息="没发现干涉 ！"
      else
        selection = @model.selection
        selection.clear
        if 含子组件的组件s.count > 0
          selection.add(含子组件的组件s)
          提示信息="有#{含子组件的组件s.count}个组件可能有干涉，但是因为其含有子组件，没法进行实体干涉检查 ！"
        end
        if 干涉的组件s.count > 0
          selection.add(干涉的组件s)
          提示信息=提示信息+ "\n"+"选中的组件存在干涉，名称为 “相交” 的组件，是其相交的结果！"
        end
      end
      UI.messagebox 提示信息
      发送提示信息("方法完成")
    end

    def 炸开末级子组件(e)
      组件定义名=e.definition.name
      组件实例名=e.name
      组件加实例名=组件定义名+"+"+组件实例名
      # puts 组件加实例名
      # puts "e.layer:"
      # puts e.layer.name
      if !含子组件?(e)
        # puts "e.layer:"
        # puts e.layer.name
        if e.layer.name=="afu-参考"
          # puts "e.layer:"
          # puts e.layer.name
          e.erase!
        else
          e.explode
        end
      else
        if e.is_a?( Sketchup::ComponentInstance )
          entities=e.definition.entities
        else
          entities=e.entities
        end
        entities.each {|ee|
          if ( ee.is_a?( Sketchup::ComponentInstance ) || ee.is_a?( Sketchup::Group ))
            炸开末级子组件(ee)
          end
        }
        炸开末级子组件(e)
      end
    end

    def 删除or炸开无板字组件(排版预处理关键词)
      # if @当前顶级组件==nil
      #   @当前顶级组件=Sketchup.active_model
      # end
      # # entities=组或群entities(@当前顶级组件)
      # if @当前顶级组件.kind_of? Sketchup::Model
      #   设置常用对象
      #   entities=@entities
      # else
      #   entities=@当前顶级组件.definition.entities
      # end

      @含板字组件s=[]
      原有的子组件s=grep集合中组件s(@entities)
      原有的子组件s.to_a.each {|e|
        if !e.deleted?
          删除or炸开无板字组件主体(e,排版预处理关键词)
        end
      }

      发送提示信息("方法完成")
      return @含板字组件s
    end

    def 删除or炸开无板字组件主体(e,排版预处理关键词)
      # if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
        @炸开了组件=false
        组件定义名=e.definition.name
        组件实例名=e.name
        组件加实例名=组件定义名+"+"+组件实例名
        puts "组件加实例名: #{组件加实例名}"

        不处理关键词=排版预处理关键词["不处理关键词"]
        含有第一项关键词=排版预处理关键词["含有第一项关键词"]
        不含有第二项关键词=排版预处理关键词["不含有第二项关键词"]
        # 不处理关键词="铰链立板条"
        # 含有第一项关键词="板|切口"
        # 不含有第二项关键词="均分|铰链|智能|参数|侧封板|均分|切口组件"

        # 列外关键词位置= 组件加实例名=~ /均分|铰链|智能|参数|侧封板|均分|切口组件/
        # 不往下遍历的组件关键词位置=组件加实例名=~ /板|切口/

        if /#{不处理关键词}/.match(组件加实例名)
          # p "含特批关键词,不再遍历其子组件"
        elsif /#{含有第一项关键词}/.match(组件加实例名) and !/#{不含有第二项关键词}/.match(组件加实例名)
          p "是含板字组件，不再遍历其子组件"
          @含板字组件s.push(e)
          # 从外向内炸开子组件(e)

        # elsif 组件加实例名.include? '参考'
        elsif 组件加实例名=~ /辅助条|不开料/
          提示信息="删除:"+组件加实例名
          p 提示信息
          发送提示信息(提示信息)
          e.erase!
        # elsif !(@当前顶级组件.kind_of? Sketchup::Model) and 需要忽略的关键词位置 != nil
        #   p "忽略这个组件"
        else
          if !含子组件?(e)
            提示信息="删除:"+组件加实例名
            p 提示信息
            发送提示信息(提示信息)
            e.erase!
          else
            提示信息="炸开:"+组件加实例名
            p 提示信息
            发送提示信息(提示信息)
            e.explode
            删除or炸开无板字组件(排版预处理关键词)
          end
        end
      # end
    end

    def 炸开所有子组件一键排版用
      设置常用对象
      @层级h=Hash[]
      ents=[]
      if @selection.count > 0
        @selection.each {|e|
          if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
            ents.push e
          end
        }
      end
      p ents.count == 0
      # p "选择的组件名:"+ents[0].definition.name
      # kk.nil
      ents=@entities if ents.count == 0
      ents.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          当前层级=0
          炸开所有子组件主体(e,当前层级)
        end
      }
      发送提示信息("方法完成")
    end

    def 炸开所有子组件一键排版用V2(含板字组件s)
      设置常用对象
      @层级h=Hash[]
      含板字组件s.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          当前层级=0
          炸开所有子组件主体(e,当前层级)
        end
      }
      发送提示信息("方法完成")
    end

    def 组件改名(含板字组件s)
      提示信息a=[]
      @含板字组件s.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          组件名=e.definition.name
          # 组件名= 组件名.gsub!(/\S*通用|智能|参数/, "")
          组件名= 组件名.gsub!(/通用|智能|参数/, "")
          if 组件名!=nil
            e.definition.name=组件名
            提示信息a.push 组件名
          end
        end
      }
      发送提示信息("方法完成")
    end

    def 组件转群组(含板字组件s)
      提示信息a=[]
      @含板字组件s.each {|e|
        puts "@含板字组件s 👉 #{定义名加实例名(e)}"
        if e.deleted?
          next
        end
        if e.is_a?( Sketchup::ComponentInstance )
          组件名=e.definition.name
          # p 组件名
          begin
            原图层=e.layer
            eee=e.explode
            # p eee
            # group = @entities.add_group()
            # group.name=组件名
            # entities2 = group.entities
            # entities2.add_face(*eee)
            fs=[]
            拟删除s=[]
            eee.each {|e|
              if e.is_a?( Sketchup::Face )
                if e.visible?
                  fs.push e
                else
                  拟删除s.push e
                end
              end
            }
            if 拟删除s.count>0
              @entities.erase_entities(*拟删除s)
            end
            # entities2.add_face(fs)
            group = @entities.add_group(fs)
            group.name=组件名
            group.layer=原图层
          rescue Exception => e
            puts "出错了！#{e}"
          end
        end
      }
      发送提示信息("方法完成")
    end

    def 每个子组件加上柜子名(c,柜子名)
      grep组件的子组件s(c).each {|e|
        e.make_unique
        成品柜名,单元柜名,板名,订单,客户,封边s,编号=解读单个组件名(柜子名,true)

        拟改名="#{成品柜名}_#{单元柜名}_#{定义或实例名(e)}"
        puts "定义或实例名(e)"
        puts 定义或实例名(e)
        puts "拟改名👇"
        puts 拟改名
        改定义或实例名(e,拟改名)
        if 是组件或群组?(e)
          # 每个子组件加上柜子名
          每个子组件加上柜子名(e,柜子名)
        end
      }
    end

    def 一键排版预处理(排版预处理关键词,唯一文件名)
      设置常用对象
      # 另存为排版模型
      status = @model.save(唯一文件名)
      拟删除es=[]
      只挑选顶层含柜字组件=排版预处理关键词["只挑选顶层含柜字组件"]
      puts 只挑选顶层含柜字组件

      if 只挑选顶层含柜字组件
        puts "只挑选顶层含柜字组件"
        # amtf.nil
        原有的子组件s=@entities.grep(Sketchup::Group)+@entities.grep(Sketchup::ComponentInstance)
        原有的子组件s.each {|e|
          顶层组件layer=e.layer.name
          顶层组件layer含定制=顶层组件layer=~/定制/i

          if 定义名加实例名(e)=~/柜|定制|书桌/i
            # 每个子组件加上柜子名
            柜子名=定义或实例名(e)
            每个子组件加上柜子名(e,柜子名)
          else
            if !顶层组件layer含定制
              拟删除es.push e
            end
          end
        }
      end
      if 拟删除es.count>0
        @顶层entities.erase_entities(*拟删除es)
      end

      # amtf.nil
      删除指定图层_显示不该隐藏的层(排版预处理关键词)
      # amtf.nil

      删除隐藏项目(@entities)
      # 清除未使用
      # amtf.nil

      @含板字组件s=删除or炸开无板字组件(排版预处理关键词)
      删除零散元素(@entities)

      @含板字组件s.each{|e|
        从外向内炸开子组件(e)
      }
      # amtf.nil
      # 炸开所有子组件一键排版用V2(@含板字组件s)
      组件改名(@含板字组件s)
      # amtf.nil
      组件转群组(@含板字组件s)


      删除零散元素(@entities)

      清除未使用
      显示全部标记

      发送提示信息("方法完成")
      return "完成！"
    end

    def 删除零散元素(entities)
      原有的子组件s=grep集合中组件s(entities)
      零散es=entities.to_a-原有的子组件s
      if 零散es.count>0
        entities.erase_entities(*零散es)
      end
    end

    def 显示全部标记
      层s=Sketchup.active_model.layers
      层s.each {|e|
        e.visible = true
      }
    end

    def 延伸背板(延伸值)
      设置常用对象
      if @selection.count == 0
        UI.messagebox "请选择一个背板"
      end
      @model.start_operation('延伸背板')
        @selection.each {|e|
          if e.is_a?( Sketchup::Group )
            群组=e
          elsif e.is_a?( Sketchup::Face )
            群组=e.parent
          end

          延伸背板主体(群组,延伸值)
        }
      @model.commit_operation
      发送提示信息("方法完成")
    end

    def 延伸背板主体(群组,延伸值)
      名称=群组.name
      p 名称
      entities = 群组.entities
      面h=Hash[]
      entities.each {|ee|
        if ee.is_a?( Sketchup::Face )
          面=ee
          面积=面.area
          面h[面]=面积
        end
      }

      延伸值=延伸值.to_f
      延伸值=延伸值.mm
      面h= Hash[面h.sort_by {|key,value| value}]#按面积排序
      倒数第三=-3
      # @selection.clear
      (0..3).each { |i|
        面=面h.keys[倒数第三-i]
        # @selection.add(面)
        status = 面.pushpull(延伸值)
      }
    end

    def 删除多余元素(entities,拟删除元素)
      for e in entities.to_a
        if !(e.is_a?( Sketchup::Face ) || e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          if e.is_a?( Sketchup::Edge )
            faces = e.faces
            # p faces.size
            if faces.size<1
                # e.erase!
                拟删除元素.concat(拟删除元素)
            end
          else
            # e.erase!
            拟删除元素.concat(拟删除元素)
          end
        end
      end
    end
  end#class

end # module amtf_su
