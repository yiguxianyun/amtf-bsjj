require 'sketchup.rb'

module AMTF
  class A孤立选中对象
    include AMTF_mixin
    def initialize()
      @初始显示状态h=Hash[]
      对象=返回第一个选择对象
      puts 对象==nil
      if 对象==nil
        puts "对象==nil"
        return
      end
      # model = Sketchup.active_model
      if @model
        # 保存初始显示状态

        获取组件显示状态V2(@model)
        # @顶层entities.each{|e|
        #   获取组件显示状态V2(e)
        # }
        # 获取组件显示状态(对象)
        # 获取兄弟组件显示状态()
        # puts "父组件名称:"
        # puts 定义名加实例名(@父组件)
        # puts "@爷爷组件名称:"
        # puts 定义名加实例名(@爷爷组件)
        # # amtf.nil
        # 获取父亲兄弟组件的显示状态() unless @爷爷组件==nil
        其他组件s=@初始显示状态h.keys-@selection.to_a
        其他组件s.each{|e|
          e.visible=false
        }
        @model.active_view.invalidate
      end
    end

    # def initialize()
    #   @初始显示状态h=Hash[]
    #   对象=返回第一个选择对象
    #   puts 对象==nil
    #   if 对象==nil
    #     puts "对象==nil"
    #     return
    #   end

    #   model = Sketchup.active_model
    #   if model
    #     # 保存初始显示状态
    #     @父组件=nil
    #     @爷爷组件=nil
    #     获取组件显示状态(对象)
    #     获取兄弟组件显示状态()
    #     puts "父组件名称:"
    #     puts 定义名加实例名(@父组件)
    #     puts "@爷爷组件名称:"
    #     puts 定义名加实例名(@爷爷组件)
    #     # amtf.nil
    #     获取父亲兄弟组件的显示状态() unless @爷爷组件==nil
    #     其他组件s=@初始显示状态h.keys-@selection.to_a
    #     其他组件s.each{|e|
    #       e.visible=false
    #     }
    #     model.active_view.invalidate
    #   end
    # end

    def 获取组件显示状态V2(搜索范围)
      找到选中组件了=false
      entities=组或群entities(搜索范围)
      entities.each{|e|
        if e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group )
          # puts e
          # puts 定义名加实例名(e)
          # puts @selection.contains?(e)
          # puts @selection
          if @selection.contains?(e)#找到选中组件了，开始返回
            找到选中组件了=true
            puts "找到选中组件了"
            puts 定义名加实例名(e)
            上层组件=搜索范围
            获取兄弟组件的显示状态(上层组件,e)
            return 找到选中组件了,e
          else
            # puts "你进来了吗？"
            找到选中组件了,找到的选中组件=获取组件显示状态V2(e)
            # puts "找到选中组件了"
            # puts 找到选中组件了
            if 找到选中组件了
              上层组件=搜索范围
              获取兄弟组件的显示状态(上层组件,e)
              return 找到选中组件了
            end
          end
        else #其他元素
          # @初始显示状态h[e]=e.visible?
        end
      }
      return 找到选中组件了
    end

    def 获取组件显示状态(选中组件)
      设置常用对象
      puts "选中的组件名:"
      puts 定义名加实例名(选中组件)
      # amtf.nil
      @顶层entities.each{|e|
        if e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group )
          if e==选中组件 #本身是顶层组件
            @父组件=@model
            return
          else
            是否包含=子级是否包含某组件(e,选中组件)
            # puts e.name
            # puts e.definition.name
            # puts "是否包含"
            # puts 是否包含
            if !是否包含 #e是爷爷的兄弟姐妹，只记录顶层组件的显示状态
              @初始显示状态h[e]=e.visible?
            else#e是爷爷或者父亲组件
              if @爷爷组件==nil
                @爷爷组件=@model
              end
            end
          end
        end
      }
    end

    def 子级是否包含某组件(搜索范围,选中组件)
      是否包含=false
      # if 搜索范围==选中组件
      #   puts "搜索范围==选中组件"
      #   # puts e
      #   是否包含=true
      #   return 是否包含
      # end
      entities=组或群entities(搜索范围)
      entities.each{|e|
        if e==选中组件
          puts "e==选中组件"
          puts e
          是否包含=true
          @父组件=搜索范围
          puts "父组件名称:"
          puts 定义名加实例名(@父组件)

          return 是否包含
        end
        if e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group )
          # entities2=组或群entities(e)
          是否包含=子级是否包含某组件(e,选中组件)
          if 是否包含==true
            # @父组件=e
            if @爷爷组件==nil
              @爷爷组件=搜索范围
            end
            # @爷爷组件=搜索范围
            # puts "@爷爷组件名称222:"
            # puts 定义名加实例名(@爷爷组件)
            return 是否包含
          end
        end
      }
      # puts "还往下执行？"
      # puts "是否包含"
      # puts 是否包含
      # if 是否包含==true
      #   return 是否包含
      # end
      # puts "是否包含"
      # puts 是否包含
      return 是否包含
    end

    def 获取兄弟组件的显示状态(上层组件,自己)
      entities=组或群entities(上层组件)
      if !entities.nil?
        entities.each{|e|
          if e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group )
            if e!=自己
              puts "上层组件的兄弟组件"
              puts 定义名加实例名(e)
              @初始显示状态h[e]=e.visible?
            end
          end
        }
      end
    end

    def 获取父亲兄弟组件的显示状态()
      entities=组或群entities(@爷爷组件)
      if !entities.nil?
        entities.each{|e|
          if e!=@父组件
            puts "父亲的兄弟组件"
            puts 定义名加实例名(e)
            # puts e.definition.name
            @初始显示状态h[e]=e.visible?
          end
        }
      end
    end
    # def 获取兄弟组件显示状态()
    #   # 父组件=组件.parent
    #   # puts "父组件"
    #   # puts 父组件.name
    #   # puts 父组件
    #   # # puts 父组件.definition.name
    #   entities=组或群entities(@父组件)
    #   if !entities.nil?
    #     entities.each{|e|
    #       puts "自己的兄弟组件"
    #       puts 定义名加实例名(e)
    #       # puts e.definition.name
    #       @初始显示状态h[e]=e.visible?
    #     }
    #   end
    #   # if !父组件.kind_of? Sketchup::Model
    #   #   获取兄弟组件显示状态(父组件)
    #   # end
    # end

    def 恢复原显示状态()
      unless @初始显示状态h.nil?
        @初始显示状态h.each{|e,v|
          e.visible=v
        }
        Sketchup.active_model.active_view.invalidate
      else

      end
    end
  end

end
