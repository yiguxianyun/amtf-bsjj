require 'sketchup.rb'
module AMTF
class AF对齐
	def initialize
		if Sketchup.active_model.selection.count == 0
			UI.messagebox "请选择实体"
			deactivate
		end
	end
	def activate
		@画完边框 = false
		@ip = Sketchup::InputPoint.new
		self.reset(nil)
	end
	def resume(view)
	end
	def deactivate(view)
		view.invalidate if @画完边框
	end
	def onCancel(flag, view)
		self.reset(view)
	end
	def reset(view)
		@ip.clear
		@message = "选择要靠齐的面"
		@pts = []
		Sketchup.set_status_text( @message )
		if( view )
			view.tooltip = nil
			view.invalidate if @画完边框
		end
		@画完边框 = false
	end
	def draw(view)
		Sketchup.set_status_text( @message )
		if @pts != []
			view.line_width = 4
			view.drawing_color="red"
      @pts.push @pts[0] if @pts[0] != @pts[-1]
      # p "开始画边框"
			view.draw_polyline(@pts)
		end
		@画完边框 = true
	end
	def onMouseMove(flags, x, y, view)
		@ip.pick( view, x, y )
		if @ip.face and @ip.face.visible?
      tr = @ip.transformation
      # p " @ip.transformation"+tr.to_a.to_s
      @pts = @ip.face.vertices.map{|vt|
        # p " vt.position"+vt.position.to_s
        vt.position.transform( tr )
        # p " vt.position.transform( tr )"+vt.position.transform( tr ).to_s
      }
			view.invalidate# if @画完边框
		else
			@pts = []
		end
	end
	def onLButtonDown(flags, x, y, view)
		@ip.pick( view, x, y )
		if @ip.face
			tr = @ip.transformation
			后选面 = [ @ip.face.vertices[0].position.transform( tr ) , @ip.face.normal.transform( tr ) ]
			start_throwing_plane( 后选面 , tr )
			self.reset(view)
		end
	end
	def start_throwing_plane( 后选面 , tr )
		后选面向量 = 后选面[1]
		model = Sketchup.active_model
		ents = model.active_entities
		向量s = []
		实体s = []
		Sketchup.active_model.start_operation "对齐" , true#Sketchup.active_model.commit_operation
		model.selection.each{|sel|
			pts = []
			pts = get_all_vt_pts( sel )
      先选对象点s = pts.uniq.compact #去除数组中的重复项目和nil项目
      # p " 先选对象点s"+先选对象点s.to_s
      dist = model.bounds.diagonal
      p "dist:="+dist.to_s
			向量 = Geom::Vector3d.new(0,0,0)
			先选对象点s.each{|先选对象点|
				ray = [ 先选对象点 , 后选面向量 ]
				ipt = Geom.intersect_line_plane(ray, 后选面)
				if ipt
					if 先选对象点.distance( ipt ) < dist
						dist = 先选对象点.distance( ipt )
            向量 = 先选对象点.vector_to( ipt )
            # line = ents.add_line 先选对象点,ipt
					end
				end
			}
      if 向量.valid?
        p "sel:" + sel.to_s
				实体s.push sel
				向量s.push 向量
			end
		}
		if 实体s != []
			ents.transform_by_vectors 实体s, 向量s
		end
		Sketchup.active_model.commit_operation
	end
	def get_all_vt_pts( e , gtr = Geom::Transformation.new )
		if e.kind_of? Sketchup::Group
			tr = e.transformation
			pts = []
			e.entities.each{|e2|
				pts.push get_all_vt_pts( e2 , gtr * tr )
			}
			return pts.flatten
		elsif e.kind_of? Sketchup::ComponentInstance
			tr = e.transformation
			pts = []
			e.definition.entities.each{|e2|
				pts.push get_all_vt_pts( e2 , gtr * tr )
			}
			return pts.flatten
		elsif e.visible? and ( e.kind_of? Sketchup::Face or e.kind_of? Sketchup::Edge )
			return e.vertices.map{|vt| vt.position.transform( gtr ) }
		end
	end
end # Class
end
