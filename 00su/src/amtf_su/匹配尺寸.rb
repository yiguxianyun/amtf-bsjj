require 'sketchup.rb'
# require File.join(__FILE__, '../AF.rb')
module AMTF
  class A匹配尺寸
    include AMTF_mixin

    def initialize()
      设置常用对象
      if @selection.count == 0
        # UI.messagebox "请先选择一个拟匹配到空间的对象(组件)"
      else
        @拟匹配对象=@selection[0]
        # 选择点偏移=ip_p.transform(transform)
        # @拟匹配对象.transform! @编辑中tr
        # p "@model.path="+@拟匹配对象.path
        @拟匹配对象.visible=false
        @selection.clear
      end
      @选择点 = Sketchup::InputPoint.new
    end

    def onMouseMove(flags, x, y, view)
      @pts = []
      @pts选择点在面上放射击中点 = []
      @pts选择点偏移放射击中面上点 = []
      @选择点.pick( view, x, y )
      if @选择点.face and @选择点.face.visible?
        ip_p = @选择点.position
        @pts.push ip_p
        face=@选择点.face
        # @selection.add(face)
        edges = face.edges
        # @selection.add(edges[0])
        # @selection.add(edges[1])
        line0 = edges[0].line
        line0向量=line0[1]
        line1 = edges[1].line
        line1向量=line1[1]

        选择面局部法向 = @选择点.face.normal#是的，返回的法向可能并不垂直于面！
        # p "选择面局部法向="+选择面局部法向.to_s
        选择点tr = @选择点.transformation
        选择面法向 = 选择面局部法向.transform( 选择点tr )
        # p "选择面法向="+选择面法向.to_s
        选择面法向normalize = 选择面法向.normalize
        # p "选择面法向normalize="+选择面法向normalize.to_s

        选择面法向normalize.length = 1/25.4*10
        # p "选择面法向normalize/25.4*10="+选择面法向normalize.to_s
        transform = Geom::Transformation.new(选择面法向normalize)
        选择点偏移=ip_p.transform(transform)

        选择点反向偏移=ip_p.transform(transform.inverse)
        # p "法向偏移前选择点坐标："+选择点偏移.to_s
        # line = @entities.add_line @原点,选择点偏移
        # @选择点=""
        @左面x=nil
        @右面x=nil
        @后面y=nil
        @前面y=nil
        @上面z=nil
        @下面z=nil

        @左h=Hash[]
        @右h=Hash[]
        @上h=Hash[]
        @下h=Hash[]
        @后h=Hash[]
        @前h=Hash[]
        @选择面上_四边击中点h=Hash[]
        选择点偏移前_放射击中面h=Hash[]
        @自动识别面上_选中点对应点h=Hash[]
        @方向h=Hash[]
        @方向h['左']=Geom::Vector3d.new(-1, 0, 0).transform(@编辑中tr)
        # @方向h['左']=Geom::Vector3d.new(-1, 0, 0)
        @方向h['右']=@方向h['左'].reverse

        @方向h['后']=Geom::Vector3d.new(0, 1, 0).transform(@编辑中tr)
        # @方向h['后']=Geom::Vector3d.new(0, 1, 0)
        @方向h['前']=@方向h['后'].reverse

        @方向h['上']=Geom::Vector3d.new(0, 0, 1).transform(@编辑中tr)
        # @方向h['上']=Geom::Vector3d.new(0, 0, 1)
        @方向h['下']=@方向h['上'].reverse

        选择面法向朝向=""
        @方向h.each{|k,v|
          angle = 选择面法向normalize.angle_between(v).radians
          # degrees = angle.radians
          # p "angle="+angle.to_s
          if angle<0.1
            # p "选择面法向朝向："+k
            选择面法向朝向=k
          end
        }

        @方向h.each{|k,v|
          投影测试=@model.raytest([@选择点, v], true)
          if 投影测试!=nil
            击中点=投影测试[0]
            @选择面上_四边击中点h[k]=击中点
            # @pts选择点在面上放射击中点.push 击中点
          end
          投影测试=@model.raytest([选择点偏移, v], true)
          if 投影测试!=nil
            击中面 = 投影测试[1][-1]
            选择点偏移前_放射击中面h[k]=击中面
            @自动识别面上_选中点对应点h[k]=投影测试[0]
            # @pts选择点偏移放射击中面上点.push 投影测试[0]
          end
        }
        # puts 选择点偏移前_放射击中面h.count
        # 选中的面可能是反面
        if 选择点偏移前_放射击中面h.count==1
          @方向h.each{|k,v|
            投影测试=@model.raytest([选择点反向偏移, v], true)
            if 投影测试!=nil
              击中面 = 投影测试[1][-1]
              选择点偏移前_放射击中面h[k]=击中面
              @自动识别面上_选中点对应点h[k]=投影测试[0]
              # @pts选择点偏移放射击中面上点.push 投影测试[0]
            end
          }
          case 选择面法向朝向
          when "左"
            选择面法向朝向="右"
          when "右"
            选择面法向朝向="左"
          when "上"
            选择面法向朝向="下"
          when "下"
            选择面法向朝向="上"
          when "后"
            选择面法向朝向="前"
          when "前"
            选择面法向朝向="后"
          end
        end
        # plane = 选择点偏移前_放射击中面h['下'].plane

        # 换算成局部坐标
        # @选择面上_四边击中点h.each{|k,v|
        #   @选择面上_四边击中点h[k]=v.transform(@编辑中tr.inverse)
        # }
        # @自动识别面上_选中点对应点h.each{|k,v|
        #   @自动识别面上_选中点对应点h[k]=v.transform(@编辑中tr.inverse)
        # }

        @自动识别面局部x=Hash[]
        @自动识别面局部y=Hash[]
        @自动识别面局部z=Hash[]
        自动识别面上_选中点对应点h=@自动识别面上_选中点对应点h['左']
        if 自动识别面上_选中点对应点h !=nil
          局部坐标点=Geom::Point3d.new(自动识别面上_选中点对应点h).transform(@编辑中tr.inverse)
          @自动识别面局部x['左']=局部坐标点[0]
        end
        自动识别面上_选中点对应点h=@自动识别面上_选中点对应点h['右']
        if 自动识别面上_选中点对应点h !=nil
          局部坐标点=Geom::Point3d.new(自动识别面上_选中点对应点h).transform(@编辑中tr.inverse)
          @自动识别面局部x['右']=局部坐标点[0]
        end
        自动识别面上_选中点对应点h=@自动识别面上_选中点对应点h['上']
        if 自动识别面上_选中点对应点h !=nil
          局部坐标点=Geom::Point3d.new(自动识别面上_选中点对应点h).transform(@编辑中tr.inverse)
          @自动识别面局部z['上']=局部坐标点[2]
        end
        自动识别面上_选中点对应点h=@自动识别面上_选中点对应点h['下']
        if 自动识别面上_选中点对应点h !=nil
          局部坐标点=Geom::Point3d.new(自动识别面上_选中点对应点h).transform(@编辑中tr.inverse)
          @自动识别面局部z['下']=局部坐标点[2]
        end
        自动识别面上_选中点对应点h=@自动识别面上_选中点对应点h['后']
        if 自动识别面上_选中点对应点h !=nil
          局部坐标点=Geom::Point3d.new(自动识别面上_选中点对应点h).transform(@编辑中tr.inverse)
          @自动识别面局部y['后']=局部坐标点[1]
        end
        自动识别面上_选中点对应点h=@自动识别面上_选中点对应点h['前']
        if 自动识别面上_选中点对应点h !=nil
          局部坐标点=Geom::Point3d.new(自动识别面上_选中点对应点h).transform(@编辑中tr.inverse)
          @自动识别面局部y['前']=局部坐标点[1]
        end

        puts 选择面法向朝向
        case 选择面法向朝向
        when "右"
          往什么方向找边='前'
          往什么方向找角点='下'
          找左下前点(往什么方向找边,往什么方向找角点)
          找右下前点("替换x值")
          找左下后点
          找左上前点()
        # when "右"
        #   point = @选择面上_四边击中点h['前']
        #   @pts.push point
        #   投影测试=@model.raytest([point, @方向h['下']], true)
        #   选择了左面_其前下角点 = 投影测试[0]
        #   @左下前点p = 选择了左面_其前下角点
        #   @pts.push @左下前点p

        #   point = @左下前点p
        #   投影测试=@model.raytest([point, @方向h['右']], true)
        #   @右下前点p = 投影测试[0]
        #   @pts.push @右下前点p

        #   # point = @右下前点p
        #   # 投影测试=@model.raytest([point, @方向h['后']], true)
        #   # @右下后点p = 投影测试[0]
        #   # # @pts.push @右下前点p

        #   point = @左下前点p
        #   投影测试=@model.raytest([point, @方向h['后']], true)
        #   @左下后点p = 投影测试[0]
        #   @pts.push @左下后点p

        #   找左上前点(@方向h,@选择面上_四边击中点h)
        when "上"
          往什么方向找边='前'
          往什么方向找角点='左'
          找左下前点(往什么方向找边,往什么方向找角点)
          找右下前点
          找左下后点
          找左上前点("替换z值")
        when nil
          puts "#{ai} is else"
        end

        # puts "点数组大小：#{@pts.size.to_s}"
        @width = @左下前点p.distance @右下前点p
        # puts "@width:#{@width.to_s}"
        @depth = @左下前点p.distance @左下后点p
        # puts "@depth:#{@depth.to_s}"
        @height = @左下前点p.distance @左上前点p
        # puts "@height:#{@height.to_s}"

        view.invalidate# if @画完边框
      else
        @pts = []
      end
    end

    def 找左下前点(往什么方向找边,往什么方向找角点)
      point = @选择面上_四边击中点h[往什么方向找边]
      # @pts.push point
      投影测试=@model.raytest([point, @方向h[往什么方向找角点]], true)
      @左下前点p = 投影测试[0]
      # puts "@左下前点p = 投影测试[0]"
      puts @自动识别面局部x['左']
      # puts "@自动识别面局部x['左'] != nil"
      # puts @自动识别面局部x['左'] != nil
      # 如果有自动识别面，优先她的坐标
      if @自动识别面局部x['左']
        局部坐标=Geom::Point3d.new(@左下前点p).transform(@编辑中tr.inverse)
        局部坐标[0]=@自动识别面局部x['左']
        @左下前点p=Geom::Point3d.new(局部坐标).transform(@编辑中tr)
      end
    end

    def 找右下前点(方式="放射方式")
        if 方式=="放射方式"
          point = @左下前点p
          投影测试=@model.raytest([point, @方向h['右']], true)
          @右下前点p = 投影测试[0]
          # puts "@右下前点p"
          # puts @右下前点p
          # if @自动识别面局部x['右']
          #   局部坐标=Geom::Point3d.new(@右下前点p).transform(@编辑中tr.inverse)
          #   局部坐标[0]=@自动识别面局部x['右']
          #   @右下前点p=Geom::Point3d.new(局部坐标).transform(@编辑中tr)
          # end
          # puts @右下前点p
        end
        if @自动识别面局部x['右']
          局部坐标=Geom::Point3d.new(@左下前点p).transform(@编辑中tr.inverse)
          局部坐标[0]=@自动识别面局部x['右']
          @右下前点p=Geom::Point3d.new(局部坐标).transform(@编辑中tr)
        end
        # puts "@左上前点p"
        # puts @左上前点p
    end
    def 找左下后点(方式="放射方式")
      if 方式=="放射方式"
        point = @左下前点p
        投影测试=@model.raytest([point, @方向h['后']], true)
        @左下后点p = 投影测试[0]
        if @自动识别面局部y['后']
          局部坐标=Geom::Point3d.new(@左下后点p).transform(@编辑中tr.inverse)
          局部坐标[1]=@自动识别面局部y['后']
          @左下后点p=Geom::Point3d.new(局部坐标).transform(@编辑中tr)
        end
      end
    end
    def 找左上前点(方式="放射方式")
      if 方式=="放射方式"
        point = @左下前点p
        投影测试=@model.raytest([point, @方向h['上']], true)
        @左上前点p = 投影测试[0]
      end
      if @自动识别面局部z['上']
        局部坐标=Geom::Point3d.new(@左下前点p).transform(@编辑中tr.inverse)
        局部坐标[2]=@自动识别面局部z['上']
        @左上前点p=Geom::Point3d.new(局部坐标).transform(@编辑中tr)
      end
      # puts "@左上前点p"
      # puts @左上前点p
    end
    # def 找左上前点(@方向h,@选择面上_四边击中点h)
    #   point = @左下前点p
    #   投影测试=@model.raytest([point, @方向h['上']], wysiwyg_flag = true)
    #   # 投影测试=@model.raytest([point, @方向h['上']], false)
    #   point上 = @选择面上_四边击中点h['上']
    #   # @左上前点p = 投影测试[0]
    #   选择了左面_其上前角点 = 投影测试[0]

    #   选择点放射击中点z值=@选择面上_四边击中点h['上'][2]
    #   puts "选择点放射击中点z值"
    #   puts 选择点放射击中点z值
    #   # 其他面板子有缩进的情况下，直接用选择面角点去放射会不正确，需要进一步比较判断坐标值
    #   if 选择点放射击中点z值<选择了左面_其上前角点[2]
    #     选择了左面_其上前角点[2]=选择点放射击中点z值
    #   end
    #   @左上前点p=选择了左面_其上前角点

    #   @pts.push @左上前点p
    # end

    def activate
      @画完边框 = false
      @选择点 = Sketchup::InputPoint.new
      self.reset(nil)
    end
    def resume(view)
    end
    def deactivate(view)
      view.invalidate if @画完边框
    end
    def onCancel(flag, view)
      self.reset(view)
    end
    def reset(view)
      @选择点.clear
      @message = "选择面 以识别内空"
      @pts = []
      Sketchup.set_status_text( @message )
      if( view )
        view.tooltip = nil
        view.invalidate if @画完边框
      end
      @画完边框 = false
    end
    def draw(view)
      Sketchup.set_status_text( @message )
      if @pts != []
        # view.draw_points(@pts,18, 5, "red")
        # view.draw_points(@pts选择点在面上放射击中点,12, 5, "blue")
        view.draw_points(@选择面上_四边击中点h.values,12, 5, "red")
        # view.draw_points(@pts选择点偏移放射击中面上点,16, 5, "DarkOrange")
        view.draw_points(@自动识别面上_选中点对应点h.values,18, 5, "DarkOrange")
        view.draw_points(@左下前点p,26, 5, "blue")
        view.draw_points(@右下前点p,13, 5, "blue")
        view.draw_points(@左下后点p,13, 5, "blue")
        view.draw_points(@左上前点p,13, 5, "blue")
        view.line_width = 4
        view.drawing_color="blue"
        view.draw_polyline(@左下前点p,@右下前点p,@左下后点p,@左下前点p)
        # @selection.clear
      end
      @画完边框 = true
    end

    def onLButtonDown flags,x,y,view
      # 设置常用对象
      # 断点.nil
      缩放移动对象
      @拟匹配对象.visible=true
      view.model.select_tool(nil)
    end

    def 缩放移动对象()
      新边界尺寸=[]
      新边界尺寸[0]=@width.to_mm
      新边界尺寸[1]=@depth.to_mm
      新边界尺寸[2]=@height.to_mm
      缩放移动对象_主体(@拟匹配对象,新边界尺寸,@左下前点p)
    end

    def 缩放移动对象_主体(对象,新边界尺寸,指定缩放后对齐点=nil)
      设置常用对象
      @model.start_operation("缩放移动对象")
        ents = @model.active_entities
        # 边界大小=获取边界大小_加入原点box(对象)
        # 旧边界x大小 = 边界大小[0].to_mm
        # 旧边界y大小 = 边界大小[1].to_mm
        # 旧边界z大小 = 边界大小[2].to_mm

        边界h=获取边界h_只认边和面(对象,true,true)
        旧边界x大小 = 边界h["lenx"].to_mm
        旧边界y大小 = 边界h["leny"].to_mm
        旧边界z大小 = 边界h["lenz"].to_mm
        puts "旧边界x大小=="
        puts 旧边界x大小
        puts 旧边界y大小
        puts 旧边界z大小
        xscale=1
        yscale=1
        zscale=1

        if 新边界尺寸[0]!=0
          xscale=新边界尺寸[0]/旧边界x大小
        end
        if 新边界尺寸[1]!=0
          yscale=新边界尺寸[1]/旧边界y大小
        end
        if 新边界尺寸[2]!=0
          zscale=新边界尺寸[2]/旧边界z大小
        end
        puts "xscale=="
        puts xscale
        puts yscale
        puts zscale
        # box=边界大小[3]
        box=边界h["box"]
        box原点=box.corner(0)
        box原点=box原点.transform(对象.transformation) #转换到了全局坐标
        缩放原点box角点序号=0
        缩放原点=box.corner(缩放原点box角点序号)

        puts "缩放原点"
        puts 缩放原点
        对象tr=对象.transformation
        对象trx=对象tr.xaxis
        对象try=对象tr.yaxis
        对象trz=对象tr.zaxis
        # puts "对象trx y z"
        # puts 对象trx
        # puts 对象try
        # puts 对象trz

        缩放原点=缩放原点.transform(对象.transformation) #转换到了全局坐标

        #对齐到世界组件坐标系，↓
        tr原来 = 对象.transformation
        旋转组件轴到全局坐标系(对象)

        sr = Geom::Transformation.scaling(缩放原点,xscale,yscale,zscale)
        #就得先旋转，再缩放↓
        对象.transform! sr
        # amtf.nil

        # 不这样来一下，旋转回去前 后面缩放的操作又退回去了！！！
        # $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)
        # 我现在猜，其实是因为下面的transformation，把scale重新变成1了！就不能只是选中吗？
        # 还是这样来一下，可以更新纹理！！！
        $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)

        # amtf.nil
        #对齐到局部组件坐标系，↓
        旋转组件轴到原坐标系(对象,tr原来)

        # amtf.nil
        # 边界大小2=获取边界大小_加入原点box(对象)
        # box2=边界大小2[3]
        边界h2=获取边界h_只认边和面(对象,true,true)
        box2=边界h["box"]

        缩放原点2=box2.corner(缩放原点box角点序号)
        缩放原点2=缩放原点2.transform(对象.transformation) #转换到了全局坐标
        puts "缩放原点2"
        puts 缩放原点2
        if 指定缩放后对齐点
          缩放后对齐点=指定缩放后对齐点
        else
          缩放后对齐点=缩放原点
        end
        vector = 缩放原点2.vector_to 缩放后对齐点
        tr = Geom::Transformation.translation(vector)
        对象.transform! tr

      @model.commit_operation
      # 显示边界框尺寸(对象)
    end

    def 识别投影面(投影测试,h)
      选择点是击中点=false
      if 投影测试 != nil
        #     投影对象 = 投影测试[1][-1]
        #     @selection.add(投影对象)
        击中点=投影测试[0]
        p "击中点全局="+击中点.to_s
        ip_p = @选择点.position
        p "ip_p="+ip_p.to_s
        p "ip_p==击中点？"+(ip_p==击中点).to_s
        选择点是击中点=ip_p==击中点
        h['是被点面']=ip_p==击中点

        @击中面 = 投影测试[1][-1]
        @层级转换 = Geom::Transformation.new
        # p "投影测试[1]:"+ 投影测试[1].to_s
        距离编辑中组件=-1
        到达编辑中组件=false
        # @层级转换= @层级转换 * 投影测试[1][-2].transformation
        # @层级转换= @层级转换 * 投影测试[1][-3].transformation
        # @层级转换= @层级转换 * 投影测试[1][-4].transformation
        # @层级转换= @层级转换 * 投影测试[1][-5].transformation
        投影测试[1].each{|e|
          if e.kind_of? Sketchup::Group or e.kind_of? Sketchup::ComponentInstance
            # p "e:"+ e.to_s
            # p "e:"+ e.name.to_s
            # p "e:"+ e.definition.name.to_s
            # if 距离编辑中组件>0
              # p "变换了这个坐标："+e.definition.name.to_s
              tr = e.transformation
              # p "e.transformation=?"+(tr==@编辑中tr).to_s
              # p "e.transformation=?"+tr.to_a.to_s
              # p "@编辑中tr=?"+@编辑中tr.to_a.to_s
              @层级转换= @层级转换 * tr
            # end
            if 到达编辑中组件
              距离编辑中组件+=1
            end
            if e.definition.name==@编辑中组件名称
              到达编辑中组件=true
            end
          end
        }
      end
    end

    def 判断空间坐标()
      if @左h['投影面'] != nil
        零点坐标转换(@左h)
        @左面x=@左h['0点'][0]
        p "@左面x:"+@左面x.to_s
      end
      if @右h['投影面'] != nil
        零点坐标转换(@右h)
        @右面x=@右h['0点'][0]
        p "@右面x:"+@右面x.to_s
      end
      if @上h['投影面'] != nil
        零点坐标转换(@上h)
        @上面z=@上h['0点'][2]
      end
      if @下h['投影面'] != nil
        零点坐标转换(@下h)
        @下面z=@下h['0点'][2]
      end
      # if @下h['投影面'] != nil
      #   点坐标转换(@下h)
      #   @下面z=@下h['0点'][0]
      # end
    end

    def 零点坐标转换(h)
      点=h['投影面'].vertices[0]
      h['0点']=点坐标转换(点,h)
    end

    def 点坐标转换(点,h)
      vtp=点.position#返回的是局部坐标！！
      p "vtp局部："+vtp.to_s
      vtp=vtp.transform( h['层级转换'] )
      p "vtp层级转换："+vtp.to_s
      return vtp
    end

    def 识别选择面边界值(h)
      最小x=nil
      最大x=nil
      最小y=nil
      最大y=nil
      最小z=nil
      最大z=nil
      # @selection.add(面)
      面=h["投影面"]
      h["点组"]=[]
      面.vertices.each{|vt|
        vtp=点坐标转换(vt,h)
        h["点组"].push vtp
        # line = @entities.add_line  @原点, vtp
        # p "编辑中局部原点:"+@编辑中局部原点.to_s
        # vtp=@编辑中局部原点.vector_to(vtp)
        # p "vtp编辑中局部："+vtp.to_s
        x值=vtp[0]
        y值=vtp[1]
        z值=vtp[2]

        最小x=vtp[0] if 最小x.nil?
        最大x=vtp[0] if 最大x.nil?
        最小y=vtp[1] if 最小y.nil?
        最大y=vtp[1] if 最大y.nil?
        最小z=vtp[2] if 最小z.nil?
        最大z=vtp[2] if 最大z.nil?

        if x值<最小x
          最小x=x值
        end
        if x值>最大x
          最大x=x值
        end
        if y值<最小y
          最小y=y值
        end
        if y值>最大y
          最大y=y值
        end
        if z值<最小z
          最小z=z值
        end
        if z值>最大z
          最大z=z值
        end
      }

      # p "@左面x:"+@左面x.to_s
      @左面x=最小x if @左面x.nil?
      @右面x=最大x if @右面x.nil?
      @后面y=最大y if @后面y.nil?
      @前面y=最小y if @前面y.nil?
      puts "激光测出来的z值"
      puts @上面z
      puts @上面z.nil?
      @上面z=最大z if @上面z.nil?
      @下面z=最小z if @下面z.nil?
    end
  end

end # module amtf_su
