module AMTF
  class A穿透选择
    include AMTF_mixin
    def initialize()
      设置常用对象
      @sel = Sketchup.active_model.selection
    end

    def 穿透显示()
      puts "穿透显示() 开始"
      穿透显示_递归(@model)

      # 需要激活手动编辑一次组件，不然组件里面的子组件不会识别到
      # ObjectSpace.each_object(Sketchup::ComponentInstance){ |e|
      #   显示指定对象(e)
      # }
      # ObjectSpace.each_object(Sketchup::Group){ |e|
      #   显示指定对象(e)
      # }

      @model.active_view.invalidate
      # UI.messagebox "方法完成"
      puts "穿透显示() 完成"

      发送提示信息("方法完成")
    end

    def 显示指定对象(e)
      定义名加实例名=定义名加实例名(e)
      puts 定义名加实例名
      if 定义名加实例名 =~ /墙/
        # puts e.visible?
        e.visible=true
      end
    end

    def 穿透显示_递归(e)
      entities=组或群entities(e)
      entities.grep(Sketchup::ComponentInstance) {|e|
        显示指定对象(e)
      }
      entities.grep(Sketchup::Group) {|e|
        显示指定对象(e)
      }
      entities.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          穿透显示_递归(e)
        end
      }
    end

    def onMouseMove(flags, x, y, view)
      ph = view.pick_helper
      ph.do_pick(x, y)
      ph.count.times { |pick_path_index|
        obj=ph.path_at(pick_path_index)
        obj.each{|e|
          名称=定义名加实例名(e)
          puts 名称
          if 名称 =~ /墙/
            @sel.clear
            @sel.add(e)
          end
        }
      }
    end

    def deactivate(view)
      # @拟匹配对象.visible=true
    end

    def onCancel(flag, view)
      # @拟匹配对象.visible=true
    end

    def onLButtonDown flags,x,y,view
      if @sel.count>0
        @sel[0].visible=false
        @sel.clear
        view.invalidate
      else
        puts "可以退出了"
        # deactivate(view)
        view.model.select_tool(nil)
      end
    end
  end

end # module amtf_su
