var 普通话 = {
    emptyTable: '当前无数据',
    info: '显示 _START_ 到 _END_ (共 _TOTAL_ 条)',
    infoEmpty: '当前无数据',
    infoFiltered: '(从 _MAX_ 条数据中筛选得到)',
    infoPostFix: '',
    thousands: ',',
    lengthMenu: '显示 _MENU_ 条数据',
    loadingRecords: '正在载入数据...',
    processing: '正在处理...',
    search: '查:',
    zeroRecords: '未找到匹配的结果',
    paginate: {
        first: '首页',
        last: '尾页',
        next: '下页',
        previous: '上页'
    },
    aria: {
        sortAscending: ':从小到大',
        sortDescending: ':从大到小'
    },
    buttons: { create: '加', edit: '改', remove: '删' }
};
