# require File.join(__FILE__, '../A布尔select_tool_基类.rb')
require 'sketchup.rb'
module AMTF
  class BoolDifferenceTool < A布尔select_tool_基类
    def initialize
      super
      icon = File.join(PATH, "Icons", "subtract_cursor_1.png")
      icon2 = File.join(PATH, "Icons", "subtract_cursor_2.png")
      @cursor = UI.create_cursor(icon, 12, 12)
      @cursor2 = UI.create_cursor(icon2, 12, 12)
      @bool_operation_string = "Difference"
    end
    def boolean_function(first, second, recurse)
      Boolean.difference(first, second, recurse)
    end
  end  #end class

  class A布尔select_tool_修剪 < BoolDifferenceTool
    def initialize
      super
      icon = File.join(PATH, "Icons", "trim_cursor_1.png")
      icon2 = File.join(PATH, "Icons", "trim_cursor_2.png")
      @cursor = UI.create_cursor(icon, 12, 12)
      @cursor2 = UI.create_cursor(icon2, 12, 12)
      @bool_operation_string = "布尔修剪中……"
    end

    def boolean_function(first, second, recurse)
      # Boolean.trim(first, second, recurse)
      kk= Boolean.trim(first, second, recurse)
      # puts "Boolean.trim(first, second, recurse)==#{kk.changed_instances}"
      return kk
      # Sketchup.active_model.active_view.invalidate
      # $dc_observers.get_class_by_version(first).redraw_with_undo(first)
      # $dc_observers.get_class_by_version(second).redraw_with_undo(second)

      # 发送提示信息("方法完成")
      # 发送提示信息("aaa")
    end
  end



end

