// <!-- 调用ruby👇 -->
$(document).ready(function () {
    监控选中($('#监控选中')[0]);
    切换其他属性显示($('#显示其他属性')[0]);
    绑定事件();
    window.由程序刷新页面 = false;
    $('#添加属性').on('click', function () {
        let 新行 = $('#其他属性模板行').html().format({ k: '新属性', 属性值: 0 });
        $('#其他属性').append(新行);
        绑定事件();
    });
    $('#导出json').on('click', function () {
        sketchup.导出json();
    });
    $('.导入json').on('click', function () {
        // console.log(this.innerText);

        sketchup.导入json(this.innerText);
    });
    $('#监控选中').on('click', function () {
        监控选中(this);
    });
    $('#显示其他属性').on('click', function () {
        切换其他属性显示(this);
        // if (this.checked) {
        //       $('#其他属性容器').show();
        //   } else {
        //       $('#其他属性容器').hide();
        //   }
    });
    $('#组件唯一_含子级').on('click', function () {
        sketchup.组件唯一_含子级();
    });
    $('#添加xyz属性_含子级').on('click', function () {
        sketchup.添加xyz属性_含子级();
    });
    $('#链接父级属性').on('click', function () {
        sketchup.链接父级属性();
    });
    $('#替代另一个对象').on('click', function () {
        sketchup.替代或重叠另一个对象({ 替代: true });
    });
    $('#重叠另一个对象').on('click', function () {
        sketchup.替代或重叠另一个对象({ 替代: false });
    });
    $('#重叠另一个对象_加公式').on('click', function () {
        sketchup.替代或重叠另一个对象({ 加公式: true });
    });

    $('.清除这个').on('click', function () {
        // alert(this.value);
        sketchup.清除公式({ 哪个: [this.value] });
    });
    $('.设置当前公式值').on('click', function () {
        let e = $(this).parent().find('input.属性值')[0];
        // console.log(`el👉 ${el}`);
        // alert(el.id);
        sketchup.设置当前公式值({ k: e.id, v: 转为数字(e.value) });
    });
    $('#清除xyz').on('click', function () {
        sketchup.清除公式({ 哪个: ['x', 'y', 'z'] });
    });
    $('#清除lenxyz').on('click', function () {
        sketchup.清除公式({ 哪个: ['lenx', 'leny', 'lenz'] });
    });
    $('#生成基准盒').on('click', function () {
        // alert("段落被点击了。");
        // console.log(this.id);
        生成单组件({ 组件名: this.value });
    });
    // $("#生成左侧板,#生成右侧板").on("click", function () {
    //   sketchup.生成单组件({ 组件名: this.value, lenz: 18 });
    // });

    $('#生成左侧板').on('click', function () {
        生成单组件({ 组件名: this.value, lenx: 18 });
    });
    $('#生成右侧板').on('click', function () {
        生成单组件({ 组件名: this.value, lenx: 18, x: 1000 });
    });
    $('#生成顶板').on('click', function () {
        生成单组件({ 组件名: this.value, lenz: 18, z: 1000 });
    });
    $('#生成底板').on('click', function () {
        生成单组件({ 组件名: this.value, lenz: 18 });
    });
    $('#生成面板').on('click', function () {
        生成单组件({ 组件名: this.value, leny: 18 });
    });
    $('生成背板').on('click', function () {
        生成单组件({ 组件名: this.value, leny: 18, y: 1000 });
    });
    // $("#生成右侧板").on("click", function () {
    //   sketchup.生成单组件([this.value, 18]);
    // });

    $('.生成装配件').on('click', function () {
        // sketchup.生成装配件(e.value);
        // sketchup.生成装配件({ 组件名: this.value });
        生成装配件({ 组件名: this.value });
    });
});

function 生成装配件(params) {
    params.跟随鼠标 = $('#跟随鼠标')[0].checked;
    sketchup.生成装配件(params);
}
function 生成单组件(params) {
    params.跟随鼠标 = $('#跟随鼠标')[0].checked;
    sketchup.生成单组件(params);
}
function 监控选中(e) {
    console.log(e);
    arg = e.checked;
    sketchup.监控选中(arg);
}
function 切换其他属性显示(e) {
    if (e.checked) {
        $('#其他属性容器').show();
    } else {
        $('#其他属性容器').hide();
    }
}
function 绑定事件() {
    上一个按键 = '';
    $('.属性值').keydown(function (e) {
        // console.log(this);
        if (e.keyCode === 13) {
            console.log(this);
            按回车后判断是否要修改属性(this);
            // this.blur();
            // 修改属性(this);
        }
        if (e.keyCode === 27) {
            上一个按键 = 'esc';
        } else {
            上一个按键 = e.keyCode;
        }
    });

    $('.属性值').change(function (e) {
        // console.log(上一个按键);
        // alert('属性值变化了！');
        if (上一个按键 != 'esc') {
            // this.blur();
            if ($('#监控选中')[0].checked) {
                修改属性(this);
            }
        }
    });

    $('.属性值').on('input', function (e) {
        同步表达式(this);
    });

    $('.清除其他属性').on('click', function () {
        console.log($(this).parent());
        $(this).parent().remove();
    });
}
function 按回车后判断是否要修改属性(e) {
    // let k = e.className;
    // alert(e.className);
    由程序刷新页面 = false;
    e.value = exp_result(e.value);

    e.blur();
    if (e.className.includes('有公式值')) {
        // alert('原来有公式值');
    } else {
        // alert('原先没有公式值');
        修改属性(e);
    }
}

function 修改属性(e) {
    // console.log(e);
    let k = e.id;
    if (!k) {
        k = $(e).prev()[0].value;
    }
    // console.log(k);
    let arg = {};
    arg[k] = { 公式值: 转为数字(e.value) };
    // arg = { k: { 公式值: 转为数字(e.value) } };
    // console.log(arg);
    if ($('#监控选中')[0].checked) {
        // alert(由程序刷新页面);
        // if (!由程序刷新页面) {
        // alert('修改属性去了');
        sketchup.修改属性(arg);
        // }
    }
}
// <!-- 供ruby调用V2👇 -->
function 显示信息(params) {
    // alert(params);
    const result = JSON.parse(params);
    // 由程序刷新页面 = true;
    console.log(result);
    $('#动态组件名')[0].value = result._name;
    $('#组件定义名')[0].value = result.定义名;

    $('#其他属性').html('');
    $.each(result, function (k, v) {
        if (['定义名', '_name'].includes(k)) {
            return true; // jQuery包装方法里面 不能用 continue
        }
        公式值 = v.公式值;
        有公式值 = false;
        if (公式值 != '空') {
            属性值 = 公式值;
            有公式值 = true;
        } else {
            属性值 = v.常规值;
        }
        if (['x', 'y', 'z', 'lenx', 'leny', 'lenz'].includes(k)) {
            $('#' + k)[0].value = 属性值;
            $('#' + k).removeClass('有公式值');
            if (有公式值) {
                // $('#' + k)[0].addClass('有公式值');
                $('#' + k).addClass('有公式值'); //赋值的时候需要加[0]，执行函数的时候不需要！又学废了
            } else {
                $('#' + k).removeClass('有公式值'); //赋值的时候需要加[0]，执行函数的时候不需要！又学废了
            }
        } else {
            // console.log(`有公式值? ${有公式值}`);
            if (属性值 != '空') {
                let 新行html = $('#其他属性模板行').html().format({ k, 属性值 });
                // $('#其他属性').append(新行);
                let 新行 = $(新行html).appendTo('#其他属性');
                // console.log(新行);
                // $(新行).children().addClass('有公式值');
                if (有公式值) {
                    let k = 新行.find('input.属性值');
                    // console.log(k);
                    k.addClass('有公式值');
                    // 新行.find('input.属性值').eq(0).addClass('有公式值');
                }
            }
        }
    });
    // console.log($("#展示内容").html());
    if (!$('#其他属性').html()) {
        let plItem = $('<div class="plItem">' + '没有其他属性' + '</div>');
        $('#其他属性').append(plItem);
        return;
    }
    // 绑定事件(); //不能笼统的绑定事件，会多次触发
}
