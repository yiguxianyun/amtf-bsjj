String.prototype.format = function (args) {
    var result = this;
    if (arguments.length > 0) {
        if (arguments.length == 1 && typeof args == 'object') {
            for (var k in args) {
                if (args[k] != undefined) {
                    var reg = new RegExp('({' + k + '})', 'g');
                    result = result.replace(reg, args[k]);
                }
            }
        } else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                    //var reg = new RegExp("({[" + i + "]})", "g");//这个在索引大于9时会有问题
                    var reg = new RegExp('({)' + i + '(})', 'g');
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
};

function 转为数字(item) {
    item = parseFloat(item);
    if (isNaN(item)) {
        item = 0;
    }
    return item;
}

function 展示计算结果(e) {
    // value = exp_result(e.value);
    // console.log(value);
    // console.log($("#计算结果")[0].value);
    $('#计算结果')[0].value = exp_result(e.value);
}
function 同步表达式(e) {
    $('#算数表达式')[0].value = e.value;
    $($('#算数表达式')[0]).trigger('oninput');
}
