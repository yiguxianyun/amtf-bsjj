// <!-- 调用ruby👇 -->
$(document).ready(function () {
    window.门信息 = {};
    门信息.缝隙宽度 = 转为数字($('#缝隙宽度')[0].value);
    sketchup.读取设置();
    $('#测试1').on('click', function () {
        sketchup.测试1();
    });
    $('#测试2').on('click', function () {
        sketchup.测试2();
    });
    $('#识别选中门板').on('click', function () {
        sketchup.识别选中门板();
    });
    $('#穿透选择并隐藏').on('click', function () {
        sketchup.穿透选择并隐藏();
    });
    $('#穿透显示').on('click', function () {
        sketchup.穿透显示();
    });
    $('#布尔').on('click', function () {
        sketchup.布尔();
    });
    $('#躺平到原点导出dxf').on('click', function () {
        sketchup.躺平到原点导出dxf();
    });
    $('#生成标准标记层').on('click', function () {
        sketchup.生成标准标记层();
    });
    $('#开启amtf_exe').on('click', function () {
        amtf_exe路径 = $('#amtf_exe路径')[0].value;
        sketchup.开启amtf_exe(amtf_exe路径);
    });

    上一个按键 = '';
    $('.回车执行').keydown(function (e) {
        // console.log(this);
        if (e.keyCode === 13) {
            console.log(this);
            // this.blur();
            // this.change();
            this.dispatchEvent(new Event('input'));
        }
        if (e.keyCode === 27) {
            上一个按键 = 'esc';
        } else {
            上一个按键 = e.keyCode;
        }
    });
    $('#门板张数').change(function (e) {
        门信息.门板张数 = 转为数字(e.target.value);
        // $('#门缝个数')[0].value = 门信息.门板张数 + 1;
        门板张数改变();
    });
    $('#门板张数').on('input', function (e) {
        门信息.门板张数 = 转为数字(e.target.value);
        门板张数改变();
    });
    $('#门缝个数').on('input', function (e) {
        门信息.门缝个数 = 转为数字(e.target.value);
        门缝个数改变();
    });
    $('#缝隙宽度').on('input', function (e) {
        门信息.缝隙宽度 = 转为数字(e.target.value);
        缝隙宽度改变();
    });
    $('#门板单边扣尺').on('input', function (e) {
        门信息.门板单边扣尺 = 转为数字(e.target.value);
        门板单边扣尺改变();
    });
    $('#修改动态组件缝隙').on('click', function () {
        门信息.上缝隙宽度 = 转为数字($('#上缝隙宽度')[0].value);
        门信息.下缝隙宽度 = 转为数字($('#下缝隙宽度')[0].value);
        sketchup.修改动态组件缝隙(门信息);
    });
});

window.onbeforeunload = function (ev) {
    // return true;
    // alert('即将关闭');
    amtf_exe路径 = $('#amtf_exe路径')[0].value;
    sketchup.保存设置({ amtf_exe路径 });
};

// <!-- 调用ruby👇 -->
function 不同板厚放不同标记层() {
    // alert("amtf");
    sketchup.不同板厚放不同标记层();
}
function 不同材质放不同标记层() {
    // alert("amtf");
    sketchup.不同材质放不同标记层();
}
function 返回json() {
    // sketchup.返回json();
    var table = $('#table_id').DataTable({
        ajax: '../mobanbiao.json'
    });
    table.ajax.reload(null, false);
    // alert("params");
}
function 导入model() {
    // var fileOpenDlg = new ActiveXObject("MSComDlg.CommonDialog");
    sketchup.导入model();
}
function 尺寸标注放到指定图层() {
    // arg = "你好，世界"
    // alert(arg)
    // window.location = 'skp:测试@' + arg
    sketchup.尺寸标注放到指定图层(指定图层.value);
    // 发送调试模式();
}

function 选择同材质组件() {
    // arg = document.getElementById("选择同材质组件").value
    // window.location = 'skp:选择同材质组件@' + arg
    sketchup.选择同材质组件();
    // alert(window.location)
}
function 移除组件材质() {
    // window.location = 'skp:移除组件and子级材质@' + arg
    sketchup.移除组件材质();
    // alert(window.location)
}
function 移除组件and子级材质() {
    // window.location = 'skp:移除组件and子级材质@' + arg
    sketchup.移除组件and子级材质();
    // alert(window.location)
}
function 更改背板厚度() {
    arg = document.getElementById('背板厚度').value;
    // window.location = 'skp:更改背板厚度@' + arg
    sketchup.更改背板厚度(arg);
    // alert(window.location)
}

function 清除未使用() {
    arg = '清除未使用';
    // window.location = 'skp:清除未使用@' + arg
    sketchup.清除未使用(arg);
}

function 对齐() {
    // window.location = 'skp:对齐@' + arg
    sketchup.对齐(arg);
}

function 多余组件处理() {
    // window.location = 'skp:多余组件处理@' + arg
    sketchup.多余组件处理(arg);
}

function 另存为排版模型() {
    // window.location = 'skp:另存为排版模型@' + arg
    sketchup.另存为排版模型(arg);

    // alert(window.location)
}

function 删除指定图层() {
    // window.location = 'skp:删除指定图层@' + arg
    sketchup.删除指定图层(arg);
}

function 删除隐藏项目() {
    window.location = 'skp:删除隐藏项目@' + arg;
}

function 删除or炸开无板字组件() {
    window.location = 'skp:删除or炸开无板字组件@' + arg;
}

function 炸开所有子组件() {
    // window.location = 'skp:炸开所有子组件@' + arg
    sketchup.炸开所有子组件();

    // alert(window.location)
}

function 组件改名() {
    // window.location = 'skp:组件改名@' + arg
    sketchup.组件改名(arg);
}

function 组件转群组() {
    // window.location = 'skp:组件转群组@' + arg
    sketchup.组件转群组(arg);
}

function 干涉检查() {
    // window.location = 'skp:干涉检查@' + arg
    sketchup.干涉检查();
}

function 延伸背板(e) {
    arg = document.getElementById('延伸值').value;
    // alert(arg)
    // window.location = 'skp:延伸背板@' + arg
    sketchup.延伸背板(arg);
    e.blur();
    // document.getElementById('辅助聚焦').focus();
    // document.getElementById('延伸按钮').blur();
    // document.getElementById('辅助').focus();
}

function 文件改名() {
    // window.location = 'skp:文件改名@' + arg
    sketchup.文件改名(arg);
}

function 画柜子(arg) {
    // alert(arg)
    // window.location = 'skp:画柜子@' + arg
    sketchup.画柜子(arg);
}
// #region 供ruby调用👇
function 展示设置(params) {
    // alert(params);
    // const result = JSON.parse(params);
    $('#amtf_exe路径')[0].value = params['amtf_exe路径'];
}
识别原缝隙ing = false;
function 展示门板信息(params) {
    // alert(params);
    console.log(params);
    // 门信息 = params;
    门信息 = Object.assign({}, 门信息, params);
    // const result = JSON.parse(params);
    // $('#左缝隙宽度')[0].value = params.左缝隙宽度;
    for (let [k, v] of Object.entries(门信息)) {
        console.log(k);
        console.log(v);
        $('#' + k)[0].value = v;
    } // 返回键值对组成的数组，如：['key', 'value']
    识别原缝隙ing = true;
    门板张数改变();
    识别原缝隙ing = false;
}
function 门板张数改变() {
    $('#门缝个数')[0].value = 门信息.门缝个数 = 门信息.门板张数 + 1;
    门缝个数改变();
}
function 门缝个数改变() {
    计算单边扣尺();
}
function 缝隙宽度改变() {
    计算单边扣尺();
}
function 计算单边扣尺() {
    let 门板单边扣尺 = (门信息.门缝个数 * 门信息.缝隙宽度) / 门信息.门板张数 / 2;
    console.log(门板单边扣尺);
    $('#门板单边扣尺')[0].value = 门信息.门板单边扣尺 = 门板单边扣尺;
    if (!识别原缝隙ing) {
        门板单边扣尺改变();
    }
}
function 门板单边扣尺改变() {
    $('#左缝隙宽度')[0].value = 门信息.左缝隙宽度 = 门信息.门板单边扣尺;
    $('#右缝隙宽度')[0].value = 门信息.右缝隙宽度 = 门信息.门板单边扣尺;
    $('#中缝隙宽度')[0].value = 门信息.中缝隙宽度 = 门信息.门板单边扣尺 * 2;
}
// #endregion
