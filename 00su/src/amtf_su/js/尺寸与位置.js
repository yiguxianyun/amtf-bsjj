var start = function () {
    window.开启显示信息el = document.getElementById('开启显示信息');
    window.x尺寸el = document.getElementById('x尺寸');
    window.y尺寸el = document.getElementById('y尺寸');
    window.z尺寸el = document.getElementById('z尺寸');
    开启显示信息(开启显示信息el);
};

var 上一个按键 = '';
window.onload = start;
$(document).ready(function () {
    $('.回车执行').keydown(function (e) {
        // console.log(this);
        if (e.keyCode === 13) {
            // this.blur();
            缩放移动对象(this);
        }
        if (e.keyCode === 27) {
            上一个按键 = 'esc';
        } else {
            上一个按键 = e.keyCode;
        }
    });

    $('.回车执行').change(function (e) {
        console.log(上一个按键);
        if (上一个按键 != 'esc') {
            // this.blur();
            缩放移动对象(this);
        }
    });
});

window.onbeforeunload = function (ev) {
    localStorage.setItem('指定图层', 指定图层.value);
};

// <!-- 调用ruby👇 -->
function 缩放移动对象(e) {
    e.value = exp_result(e.value);

    边界框尺寸 = [x尺寸el.value, y尺寸el.value, z尺寸el.value];
    边界框尺寸.push($("input[name='x基准']:checked").val());
    边界框尺寸.push($("input[name='y基准']:checked").val());
    边界框尺寸.push($("input[name='z基准']:checked").val());
    边界框尺寸 = 边界框尺寸.map(function (item) {
        item = parseFloat(item);
        if (isNaN(item)) {
            item = 0;
        }
        return item;
    });

    边界框尺寸.push($('#基准在中心')[0].checked);
    sketchup.缩放移动对象(JSON.stringify(边界框尺寸));
}
function 开启显示信息(e) {
    // 调式模式 = document.getElementById('调式模式');
    arg = e.checked;
    // alert(arg)
    // window.location = 'skp:开启显示信息@' + arg
    sketchup.开启显示信息(arg);
}

function 孤立选中对象() {
    // window.location = 'skp:孤立@' + arg
    sketchup.孤立选中对象();
}
function 还原原显示状态() {
    // window.location = 'skp:孤立@' + arg
    sketchup.还原原显示状态();
}

function 匹配尺寸() {
    arg = '匹配尺寸';
    // window.location = 'skp:匹配尺寸@' + arg
    sketchup.匹配尺寸(arg);
}

// <!-- 供ruby调用V2👇 -->
function 显示信息(params) {
    // alert(params);
    const result = JSON.parse(params);
    x尺寸el.value = result['x'];
    y尺寸el.value = result['y'];
    z尺寸el.value = result['z'];
    console.log(result);
    $('#组件定义名')[0].value = result['组件定义名'];
    $('#组件实例名')[0].value = result['组件实例名'];
}
