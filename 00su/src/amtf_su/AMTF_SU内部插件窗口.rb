module AMTF
  class AMTF_SU内部插件窗口 < UI::HtmlDialog
    include AMTF_mixin
    def initialize
      # Dialog parameters
      # super "amtf", false, "amtf", 350, 600, 50, 100, true
      # puts File.join(__dir__, '另存排版.rb')
      options = {
        dialog_title: "amtf_su😄",
        preferences_key: "amtf_su内部插件😄",
        # scrollable: false,
        # resizable: false,
        width: 400,
        height: 160,
        left: 0,
        top: 100,
        # min_width: 50,
        # min_height: 150,
        max_width: 1000,
        max_height: 1000,
        style: UI::HtmlDialog::STYLE_DIALOG
      }
      super(options)
      path = File.join(__dir__, '/amtf_su内部插件.html')
      set_file path
      # set_url('https://www.baidu.com/')
      # set_url("http://127.0.0.1:5501")
      observer=nil
      # Display the dialog box
      if RUBY_PLATFORM.index "darwin"
        show_modal
      else
        show
      end
      add_action_callback("尺寸标注放到指定图层") {|dialog, arg|
        A其他.new().尺寸标注放到指定图层(arg)
        # puts arg+" 完成！"
        # 发送提示信息("方法完成")
      }

      add_action_callback("缩放移动对象") {|dialog, arg|
        puts "缩放移动对象"
        puts arg
        # UI.messagebox "#{arg}"
        对象=返回第一个选择对象
        A其他.new().缩放移动对象(对象,JSON.parse(arg))
      }
      add_action_callback("导入stl") {|dialog, arg|
        puts "收到的组件名："+arg
        arg = arg.gsub!(/\\/, "/")
        导入数量=A导入.new().A导入(arg)
        execute_script("导入stl结果('#{导入数量}')")
      }
      add_action_callback("识别选中组件") {|dialog, arg|
        # puts "收到的组件名："+arg
        动态属性h=@A组件属性obj.识别选中组件()
        # puts "aaa"
        # puts 动态属性h.to_json
        execute_script("接收识别选中组件('#{动态属性h.to_json}')")
      }
      add_action_callback("修改组件属性") {|dialog, arg|
        # puts arg
        # UI.messagebox "#{arg}"
        修改组件属性结果=@A组件属性obj.修改组件属性(arg)
        execute_script("接收修改组件属性('#{修改组件属性结果.to_json}')")
      }

      # Set the WebDialog's callback
      add_action_callback("开启显示边界框尺寸") {|dialog, arg|
        # puts "收到的arg："+arg
        if arg==true
          if observer==nil
            puts "创建观察者"
            observer=A选中对象观察者.new(self)
          else
            puts "已经创建过了观察者"
          end
          Sketchup.active_model.selection.add_observer(observer)
        else
          if observer!=nil
            selection = Sketchup.active_model.selection
            status = selection.remove_observer observer
            puts "移除观察者"
            puts status
          else
            puts "本来就没有观察者"
          end
        end
      }

      add_action_callback("拼合组件名") {|dialog, arg|
        puts "收到的组件名："+arg
        A导出.new().拼合组件名(arg)
      }

      add_action_callback("解读组件名") {|dialog, arg|
        # 对象=返回第一个选择对象
        # puts "对象.name"+对象.name
        解读后的组件名=A导出.new().解读组件名
        # 发送解读后的组件名(解读后的组件名)
        self.execute_script("接收解读后的组件名('#{解读后的组件名}')")
      }
      add_action_callback("发送调试模式") {|dialog, arg|
        # amtf_dxf文件='D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe'
        if arg!="false"
          puts "是调式模式"
          @调式模式=true
        else
          puts "非调式模式"
          @调式模式=false
        end
      }

      add_action_callback("重新连接amtf") {|dialog, arg|
        AMTF::重启浏览器窗口(true)
      }

      # add_action_callback("开启amtf_dxf") {|dialog, arg|
      #   调用amtf_dxf('D:\amtf\00南腾家居-项目\导出20221016_202614/吊柜-顶板#18__1_24_00_1_-1___amtf_板厚292.dxf')
      # }
      add_action_callback("关于amtf") {|dialog, arg|
        puts "AMTF::AMTF窗口about.newkk"
        # AMTF窗口about.new
        # AMTF::AMTF窗口about.new
        link=File.join(__dir__, '关于amtf.html')
        # system("start #{link}")
        kk=Open3.popen3("start #{link}")
      }

      @a孤立选中对象=nil
      add_action_callback("孤立选中对象") {|dialog, arg|
        # puts "amtf"
        # AF另存排版.new().孤立()
        @a孤立选中对象=A孤立选中对象.new()
      }
      add_action_callback("还原原显示状态") {|dialog, arg|
        @a孤立选中对象.恢复原显示状态()
      }

      add_action_callback("选择同材质组件") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().选择同材质组件()
      }
      add_action_callback("移除组件and子级材质") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().移除组件and子级材质()
      }
      add_action_callback("更改背板厚度") {|dialog, arg|
        puts arg
        AF另存排版.new().更改背板厚度(arg)
      }

      add_action_callback("测试") {|dialog, arg|
        # puts arg
          # 联系amtf
          # 测试连接
          对象=返回第一个选择对象()
          A导入.new().炸开组件判断是否有孤立面(对象)


          # A导出.new().删除参考线(对象)

          # 设置常用对象
          # A导出.new().内部边界转子群组(@model)

          # A导出.new().焊接组件边线(对象)
          # A导出.new().导出v2
          # A组件属性.new().遍历组件属性
          # AMTF窗口组件属性.new
          # reload其他插件
          # A导入.new().A导入('D:\\桌面\\新建文件夹\\导出stl-标准柜20230208-1335-29')
          # A导入.new().A导入('D:/桌面/新建文件夹/导出stl-标准柜20230208-1335-29')
          # 设置常用对象
          # 对象=@entities[0].parent
          # A导入.new().识别孔槽(对象)

          # 新边界尺寸=Hash[]
          # 新边界尺寸["x"]=6567.0
          # 新边界尺寸["y"]=240.0
          # 新边界尺寸["z"]=1000
          # A其他.new().缩放边界(新边界尺寸)

          发送提示信息("方法完成")
      }

      add_action_callback("刷新amtf") {|dialog, arg|
        AMTF::reload
        # puts arg+" 完成！"
        Sketchup::set_status_text arg+" 完成！"
        # return arg+" 完成！"
      }

      add_action_callback("匹配尺寸") {|dialog, arg|
        Sketchup.active_model.select_tool A匹配尺寸.new
      }
      add_action_callback("对齐") {|dialog, arg|
        # Sketchup.active_model.select_tool( AF对齐.new )
        Sketchup.active_model.select_tool AF对齐.new
      }
      add_action_callback("多余组件处理") {|dialog, arg|
        AF另存排版.new().多余组件处理
      }
      add_action_callback("画柜子") {|dialog, arg|
        # p "hello"
        Sketchup.active_model.select_tool AF画柜子.new(arg+".skp")
      }
      add_action_callback("另存为排版模型") {|dialog, arg|
        AF另存排版.new().另存为排版模型
      }
      add_action_callback("删除指定图层") {|dialog, arg|
         Sketchup::set_status_text __method__.to_s()+" ing"
        AF另存排版.new().删除指定图层
      }
      add_action_callback("删除隐藏项目") {|dialog, arg|
        AF另存排版.new().删除隐藏项目不传参数
      }
      add_action_callback("删除or炸开无板字组件") {|dialog, arg|
        AF另存排版.new().删除or炸开无板字组件
      }
      add_action_callback("炸开所有子组件") {|dialog, arg|
        AF另存排版.new().炸开所有子组件
      }
      add_action_callback("组件改名") {|dialog, arg|
        AF另存排版.new().组件改名
      }
      add_action_callback("组件转群组") {|dialog, arg|
        AF另存排版.new().组件转群组
      }
      add_action_callback("干涉检查") {|dialog, arg|
        # UI.messagebox("我执行了！")
        AF另存排版.new().干涉检查
      }
      add_action_callback("一键排版预处理") {|dialog, arg|
        处理结果=AF另存排版.new().一键排版预处理(arg)
        self.execute_script("返回一键排版预处理结果('#{处理结果}')")

      }
      add_action_callback("检查板厚") {|dialog, arg|
        返回结果=A导出.new().检查板厚()
        self.execute_script("返回_返回结果('#{返回结果}')")
        # self.execute_script("返回_检查板厚_处理结果('#{处理结果.to_json}')")

      }
      add_action_callback("检查材质") {|dialog, arg|
        返回结果=A导出.new().检查材质()
        self.execute_script("返回_返回结果('#{返回结果}')")
      }
      add_action_callback("延伸背板") {|dialog, arg|
        AF另存排版.new().延伸背板(arg)
      }

      add_action_callback("文件改名") {|dialog, arg|
        文件改名("kk")
      }
      add_action_callback("返回值给ruby") {|dialog, arg|
        return arg
      }
      add_action_callback("清除未使用") {|dialog, arg|
        清除未使用
      }
      add_action_callback("重载ABF") {|dialog, arg|
        Sketchup::set_status_text "重载ABF"+" ing"

        path = Sketchup.find_support_file "abf_solutions/startup.rb", "Plugins"
        puts "path=" + path.to_s
        load path.to_s
        puts "重新加载ABFing……稍安勿躁……"
        Sketchup::set_status_text "重载ABF"+" 完成"
      }
      # Display the dialog box
      # if RUBY_PLATFORM.index "darwin"
      #   show_modal
      # else
      #   show
      # end

    end #initialize

    def reload其他插件
      puts "reload其他插件ing"
      original_verbose = $VERBOSE
      $VERBOSE = nil
      # pattern = File.join('D:/amtf/amtf_suoc/', 'oneclickcabinet/**/*.rb')
      pattern = File.join('D:/amtf/amtf_suoc/', '*.rb')
      puts pattern
      Dir.glob(pattern).each { |file|
        # Cannot use `Sketchup.load` because its an alias for `Sketchup.require`.
        # puts file
        load file
      }.size
      ensure
        $VERBOSE = original_verbose
    end


    # 执行网页js👇
    def 调用amtf_dxf(导出dxf全名)
      # phone = %q|D:\amtf\00南腾家居-项目\导出20221016_202614/吊柜-顶板#18__1_24_00_1_-1___amtf_板厚292.dxf|
      # # puts "电话号码 : #{phone}"

      # # 移除数字以外的其他字符
      # phone = phone.gsub!(/\\/, %q|\\\\\\\\|)
      # puts "电话号码 : #{phone}"

      # 导出dxf全名 = 导出dxf全名.gsub!(/\\/, "\\\\\\\\")
      导出dxf全名 = 导出dxf全名.gsub!(/\\/, "/")
      puts "调用amtf_dxf('#{导出dxf全名}')"

      self.execute_script("调用amtf_dxf('#{导出dxf全名}')")
      # self.execute_script("调用amtf_dxf('D:amtf00南腾家居-项目')")
      # D:\amtf\00南腾家居-项目\导出20221016_202614/吊柜-顶板#18__1_24_00_1_-1___amtf_板厚292.dxf
    end

    def 判断是否调试模式()
      self.execute_script("发送调试模式()")
    end
    # def 发送解读后的组件名(解读后的组件名)
    #   self.execute_script("接收解读后的组件名('#{解读后的组件名}')")
    # end

    def 显示边界框尺寸(边界框尺寸)
      # puts "边界框尺寸"
      # puts 边界框尺寸
      self.execute_script("显示边界框尺寸('#{边界框尺寸}')")
    end

  end #AMTF窗口
end # module amtf_su
