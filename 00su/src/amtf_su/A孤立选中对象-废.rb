require 'sketchup.rb'

module AMTF
  # require_relative '../gl/gl_button'
  class A孤立选中对象
    include AMTF_mixin
    COLOR_FACE = Sketchup::Color.new(255, 0, 0, 128).freeze
    COLOR_TEXT = Sketchup::Color.new(0, 0, 0, 255).freeze
    FONT_TEXT = ''
    def initialize()
      @初始显示状态h=Hash[]

      @line_1_text = "孤立状态中"
      # Define text options
      @line_1_text_options = {
          color: COLOR_TEXT,
          font: FONT_TEXT,
          size: 20,
          align: TextAlignCenter
      }
      button_text_options = {
        color: COLOR_TEXT,
        font: FONT_TEXT,
        size: 12,
        align: TextAlignCenter
    }
    end

    def activate
      对象=返回第一个选择对象
      puts 对象==nil
      if 对象==nil
        puts "对象==nil"
        # desactivate(@model.active_view)
        @model.active_view.model.select_tool(nil)
        return
      end
      model = Sketchup.active_model
      if model
        # 保存初始显示状态
        获取兄弟组件显示状态(对象)
        其他组件s=@初始显示状态h.keys-@selection.to_a
        其他组件s.each{|e|
          e.visible=false
        }
        model.active_view.invalidate
      end
    end
    def suspend(view)
      # UI.messagebox "suspend(view) 执行了 ！"
    end
    def 获取兄弟组件显示状态(组件)
      父组件=组件.parent
      puts "父组件"
      puts 父组件
      父组件.entities.each{|e|
        @初始显示状态h[e]=e.visible?
      }
      if !父组件.kind_of? Sketchup::Model
        获取兄弟组件显示状态(父组件)
      end
    end

    def deactivate(view)
      # UI.messagebox "desactivate(view) 执行了 ！"
      unless @初始显示状态h.nil?
        @初始显示状态h.each{|e,v|
          e.visible=v
        }
      end
      # puts "@初始显示状态h.nil?"
      # puts @初始显示状态h.nil?
      view.invalidate
    end

    def onCancel(flag, view)
      # UI.messagebox "onCancel(flag, view) 执行了 ！"
    end

    def resume(view)
      view.invalidate
    end

    def draw(view)
      view.drawing_color = COLOR_FACE
      # view.draw(GL_TRIANGLES, @face_triangles_cache)
      if Sketchup.version_number >= 16000000
        unless @line_1_text.nil?
          view.draw_text(Geom::Point3d.new(view.vpwidth-100, 25 , 0), @line_1_text, @line_1_text_options)
        end
      end
    end
    # -- Events --

    # def onLButtonDown(flags, x, y, view)
    #   @buttons.each { |button|
    #     if button.onLButtonDown(flags, x, y, view)
    #       return
    #     end
    #   }
    # end

    # def onLButtonUp(flags, x, y, view)
    #   @buttons.each { |button|
    #     if button.onLButtonUp(flags, x, y, view)
    #       return
    #     end
    #   }
    #   view.model.rendering_options["ModelTransparency"] = @initial_model_transparency
    #   view.model.select_tool(nil)  # Desactivate the tool on click
    #   view.invalidate
    # end

    # def onMouseMove(flags, x, y, view)
    #   @buttons.each { |button|
    #     if button.onMouseMove(flags, x, y, view)
    #       return
    #     end
    #   }
    # end
    private

  end

end
