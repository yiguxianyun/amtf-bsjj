module AMTF
  class DLG_尺寸与位置 < UI::HtmlDialog
    include AMTF_mixin
    def initialize
      # Dialog parameters
      # super "amtf", false, "amtf", 350, 600, 50, 100, true
      # puts File.join(__dir__, '另存排版.rb')
      options = {
        dialog_title: "尺寸与位置😄",
        preferences_key: "尺寸与位置😄",
        # scrollable: false,
        # resizable: false,
        width: 400,
        height: 160,
        left: 0,
        top: 100,
        # min_width: 50,
        # min_height: 150,
        max_width: 1000,
        max_height: 1000,
        style: UI::HtmlDialog::STYLE_DIALOG
      }
      super(options)
      path = File.join(__dir__, '/html/尺寸与位置.html')
      set_file path
      @observer_选中对象=nil
      # Display the dialog box
      if RUBY_PLATFORM.index "darwin"
        show_modal
      else
        show
      end
      add_action_callback("缩放移动对象") {|dialog, arg|
        puts "缩放移动对象"
        puts arg
        # UI.messagebox "#{arg}"
        对象=返回第一个选择对象
        缩放移动对象(对象,JSON.parse(arg))
        显示对象信息(对象)
      }
      # Set the WebDialog's callback
      add_action_callback("开启显示信息") {|dialog, arg|
        puts "收到的arg："
        puts arg
        puts arg==true
        model = Sketchup.active_model
        selection = Sketchup.active_model.selection
        if arg==true
          # if @observer_选中对象==nil
          #   puts "创建观察者"
          #   @observer_选中对象=A选中对象观察者.new(self)
          #   puts "创建观察者完成了！"
          # else
          #   puts "已经创建过了观察者"
          # end
          @observer_选中对象=A选中对象观察者.new(self)
          AMTF::接收参数(@observer_选中对象)
          # status = selection.remove_observer @observer_选中对象
          status =selection.add_observer(@observer_选中对象)
          # model.add_observer(@observer_选中对象)
          puts "创建观察者?"
          puts status
        else
          model = Sketchup.active_model
          # status = model.remove_observer(@observer_选中对象)
          status = selection.remove_observer @observer_选中对象
          puts "移除观察者:status"
          puts status
          # if @observer_选中对象!=nil
          #   status = selection.remove_observer @observer_选中对象
          #   puts "移除观察者"
          #   puts status
          # else
          #   puts "本来就没有观察者"
          # end
        end
      }

      @a孤立选中对象=nil
      add_action_callback("孤立选中对象") {|dialog, arg|
        # puts "amtf"
        # AF另存排版.new().孤立()
        @a孤立选中对象=A孤立选中对象.new()
      }
      add_action_callback("还原原显示状态") {|dialog, arg|
        @a孤立选中对象.恢复原显示状态()
      }
      add_action_callback("匹配尺寸") {|dialog, arg|
        Sketchup.active_model.select_tool A匹配尺寸.new
      }
      set_on_closed() {
        status = Sketchup.active_model.selection.remove_observer @observer_选中对象
        puts "移除 status 👉 #{status}"

      }
    end #initialize

    def 显示对象信息(对象)
      if @上次选中对象==对象
        return
      end
      对象信息h=Hash[]
      if !对象.nil?
        # 边界=获取边界大小V2(c)
        # 边界=获取边界大小_加入原点box(对象)
        边界h=获取边界h_只认边和面(对象,true,true)

        # AMTF::AMTF窗口.new
        # AMTF窗口.new().显示对象信息(边界)
        # AMTF窗口.显示对象信息(边界)
        对象信息h["x"]=边界h["lenx"].to_mm.round(1)
        对象信息h["y"]=边界h["leny"].to_mm.round(1)
        对象信息h["z"]=边界h["lenz"].to_mm.round(1)
        if 是组件或群组?(对象)
          对象信息h["组件定义名"]=对象.definition.name
          对象信息h["组件实例名"]=对象.name
        else
          对象信息h["组件定义名"]=对象.class
          对象信息h["组件实例名"]=对象.to_s
        end

      else
        对象信息h["x"]="空"
        对象信息h["y"]="空"
        对象信息h["z"]="空"
        对象信息h["组件定义名"]=""
        对象信息h["组件实例名"]=""
      end
      边界js=对象信息h.to_json
      # puts 边界js
      self.execute_script("显示信息('#{边界js}')")

      @上次选中对象=对象
    end

    def 缩放移动对象(对象,新边界尺寸)
      设置常用对象
      @model.start_operation("缩放移动对象")
        ents = @model.active_entities
        # 边界大小=获取边界大小_加入原点box(对象)
        边界h=获取边界h_只认边和面(对象,true,true)

        旧边界x大小 = 边界h["lenx"].to_mm
        旧边界y大小 = 边界h["leny"].to_mm
        旧边界z大小 = 边界h["lenz"].to_mm
        # 旧边界x大小 = 边界大小[0].to_mm
        # 旧边界y大小 = 边界大小[1].to_mm
        # 旧边界z大小 = 边界大小[2].to_mm
        xscale=1
        yscale=1
        zscale=1
        if 新边界尺寸[0]!=0
          xscale=新边界尺寸[0]/旧边界x大小
        end
        if 新边界尺寸[1]!=0
          yscale=新边界尺寸[1]/旧边界y大小
        end
        if 新边界尺寸[2]!=0
          zscale=新边界尺寸[2]/旧边界z大小
        end
        # puts "xscale=="
        # puts xscale
        # puts yscale
        # puts zscale
        # box=边界大小[3]
        box=边界h["box"]
        box原点=box.corner(0)
        box原点=box原点.transform(对象.transformation) #转换到了全局坐标

        缩放后box角点序号=0
        x基准=新边界尺寸[3]
        y基准=新边界尺寸[4]
        z基准=新边界尺寸[5]
        if x基准==1 && y基准==0 && z基准==0
          缩放后box角点序号=1
        end
        if x基准==0 && y基准==1 && z基准==0
          缩放后box角点序号=2
        end
        if x基准==1 && y基准==1 && z基准==0
          缩放后box角点序号=3
        end
        if x基准==0 && y基准==0 && z基准==1
          缩放后box角点序号=4
        end
        if x基准==1 && y基准==0 && z基准==1
          缩放后box角点序号=5
        end
        if x基准==0 && y基准==1 && z基准==1
          缩放后box角点序号=6
        end
        if x基准==1 && y基准==1 && z基准==1
          缩放后box角点序号=7
        end
        缩放前基准点=box.corner(缩放后box角点序号)
        基准在中心=新边界尺寸[6]
        if 基准在中心
          缩放前基准点=box.center
        end

        # puts "缩放前基准点"
        # puts 缩放前基准点
        对象tr=对象.transformation
        对象trx=对象tr.xaxis
        对象try=对象tr.yaxis
        对象trz=对象tr.zaxis
        # puts "对象trx y z"
        # puts 对象trx
        # puts 对象try
        # puts 对象trz

        缩放前基准点=缩放前基准点.transform(对象.transformation) #转换到了全局坐标
        # puts "缩放前基准点"
        # puts 缩放前基准点
        # 缩放前基准点=缩放前基准点.transform(@编辑中tr)
        # puts 缩放前基准点

        #对齐到世界组件坐标系，↓
        tr原来 = 对象.transformation
        旋转组件轴到全局坐标系(对象)
        # x = Geom::Vector3d.new(1, 0, 0)
        # y = Geom::Vector3d.new(0, 1, 0)
        # z = Geom::Vector3d.new(0, 0, 1)
        # tr = Geom::Transformation.new(x,y,z,box原点)
        # 对象.transformation = tr
        # amtf.nil
        sr = Geom::Transformation.scaling(缩放前基准点,xscale,yscale,zscale)
        #就得先旋转，再缩放↓
        对象.transform! sr
        # amtf.nil

        # 不这样来一下，旋转回去前 后面缩放的操作又退回去了！！！
        # $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)
        # 我现在猜，其实是因为下面的transformation，把scale重新变成1了！就不能只是选中吗？
        # 还是这样来一下，可以更新纹理！！！
        $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)


        # amtf.nil
        #对齐到局部组件坐标系，↓
        旋转组件轴到原坐标系(对象,tr原来)
        # x = 对象trx.transform(@编辑中tr)
        # y = 对象try.transform(@编辑中tr)
        # z = 对象trz.transform(@编辑中tr)
        # puts "对象trx.transform(@编辑中tr)"
        # puts x
        # puts y
        # puts z
        # # x = Geom::Vector3d.new(1, 0, 0).transform(@编辑中tr)
        # # y = Geom::Vector3d.new(0, 1, 0).transform(@编辑中tr)
        # # z = Geom::Vector3d.new(0, 0, 1).transform(@编辑中tr)
        # tr = Geom::Transformation.new(x,y,z,box原点)
        # 对象.transformation = tr

        # amtf.nil
        # 边界大小2=获取边界大小_加入原点box(对象)
        边界h2=获取边界h_只认边和面(对象,true,true)
        # box2=边界大小2[3]
        box2=边界h2["box"]
        缩放后基准点=box2.corner(缩放后box角点序号)
        if 基准在中心
          缩放后基准点=box2.center
        end
        缩放后基准点=缩放后基准点.transform(对象.transformation) #转换到了全局坐标
        vector = 缩放后基准点.vector_to 缩放前基准点
        tr = Geom::Transformation.translation(vector)
        对象.transform! tr
      @model.commit_operation
    end

    # def 缩放移动对象f(对象,新边界尺寸)
    #   设置常用对象
    #   @model.start_operation("缩放移动对象")
    #   ents = @model.active_entities
    #   边界大小=获取边界大小_加入原点box(对象)
    #   旧边界x大小 = 边界大小[0].to_mm.round(2)
    #   旧边界y大小 = 边界大小[1].to_mm.round(2)
    #   旧边界z大小 = 边界大小[2].to_mm.round(2)
    #   xscale=1
    #   yscale=1
    #   zscale=1
    #   if 新边界尺寸[0]!=0
    #     xscale=新边界尺寸[0]/旧边界x大小
    #   end
    #   if 新边界尺寸[1]!=0
    #     yscale=新边界尺寸[1]/旧边界y大小
    #   end
    #   if 新边界尺寸[2]!=0
    #     zscale=新边界尺寸[2]/旧边界z大小
    #   end
    #   puts xscale
    #   puts yscale
    #   puts zscale
    #   box=边界大小[3]
    #   box原点=box.corner(0)
    #   box原点=box原点.transform(对象.transformation) #转换到了全局坐标

    #   缩放原点box角点序号=0
    #   x基准=新边界尺寸[3]
    #   y基准=新边界尺寸[4]
    #   z基准=新边界尺寸[5]
    #   if x基准==1 && y基准==0 && z基准==0
    #     缩放原点box角点序号=1
    #   end
    #   if x基准==0 && y基准==1 && z基准==0
    #     缩放原点box角点序号=2
    #   end
    #   if x基准==1 && y基准==1 && z基准==0
    #     缩放原点box角点序号=3
    #   end
    #   if x基准==0 && y基准==0 && z基准==1
    #     缩放原点box角点序号=4
    #   end
    #   if x基准==1 && y基准==0 && z基准==1
    #     缩放原点box角点序号=5
    #   end
    #   if x基准==0 && y基准==1 && z基准==1
    #     缩放原点box角点序号=6
    #   end
    #   if x基准==1 && y基准==1 && z基准==1
    #     缩放原点box角点序号=7
    #   end
    #   缩放原点=box.corner(缩放原点box角点序号)

    #   # puts "缩放原点"
    #   # puts 缩放原点
    #   对象tr=对象.transformation
    #   对象trx=对象tr.xaxis
    #   对象try=对象tr.yaxis
    #   对象trz=对象tr.zaxis
    #   puts "对象trx y z"
    #   puts 对象trx
    #   puts 对象try
    #   puts 对象trz

    #   缩放原点=缩放原点.transform(对象.transformation) #转换到了全局坐标
    #   # puts "缩放原点"
    #   # puts 缩放原点
    #   # 缩放原点=缩放原点.transform(@编辑中tr)
    #   # puts 缩放原点

    #   #对齐到世界组件坐标系，↓
    #   x = Geom::Vector3d.new(1, 0, 0)
    #   y = Geom::Vector3d.new(0, 1, 0)
    #   z = Geom::Vector3d.new(0, 0, 1)
    #   tr = Geom::Transformation.new(x,y,z,box原点)
    #   对象.transformation = tr
    #   # amtf.nil
    #   sr = Geom::Transformation.scaling(缩放原点,xscale,yscale,zscale)
    #   #就得先旋转，再缩放↓
    #   对象.transform! sr
    #   # amtf.nil

    #   # 不这样来一下，旋转回去前 后面缩放的操作又退回去了！！！
    #   # $dc_observers.get_class_by_version(对象).redraw_with_undo(对象)
    #   # 我现在猜，其实是因为下面的transformation，把scale重新变成1了！就不能只是选中吗？

    #   # amtf.nil
    #   #对齐到局部组件坐标系，↓
    #   x = 对象trx.transform(@编辑中tr)
    #   y = 对象try.transform(@编辑中tr)
    #   z = 对象trz.transform(@编辑中tr)
    #   puts "对象trx.transform(@编辑中tr)"
    #   puts x
    #   puts y
    #   puts z
    #   # x = Geom::Vector3d.new(1, 0, 0).transform(@编辑中tr)
    #   # y = Geom::Vector3d.new(0, 1, 0).transform(@编辑中tr)
    #   # z = Geom::Vector3d.new(0, 0, 1).transform(@编辑中tr)
    #   tr = Geom::Transformation.new(x,y,z,box原点)
    #   对象.transformation = tr

    #   # amtf.nil
    #   边界大小2=获取边界大小_加入原点box(对象)
    #   box2=边界大小2[3]
    #   缩放原点2=box2.corner(缩放原点box角点序号)
    #   缩放原点2=缩放原点2.transform(对象.transformation) #转换到了全局坐标
    #   vector = 缩放原点2.vector_to 缩放原点
    #   tr = Geom::Transformation.translation(vector)
    #   对象.transform! tr

    # end



  end #AMTF窗口
end # module amtf_su
