require 'sketchup.rb'
require File.join(__FILE__, '../amtf_mixin.rb')
require 'json'
require "win32ole"

# require 'open3'
# require 'win32ole'
module AMTF
  class A门缝
    include AMTF_mixin
    def 识别选中门板
        选择对象=返回选择对象
        if 选择对象.nil?
          return
        end
        门板张数=0
        h=Hash[]
        转mm=true
        子组件s=grep集合中组件s(选择对象)
        子组件s.to_a.each {|e|
          puts 定义名加实例名(e)
          拟读取属性s=["bjqfx11","bjqfx12","bjqfx13","bjqfx14","bjqfx2"]
          if 定义名加实例名(e)=~/对开门/i
            门板张数=门板张数+2
          elsif 定义名加实例名(e)=~/左开门|右开门|/i
            # 每个子组件加上柜子名
            门板张数=门板张数+1
          elsif 定义名加实例名(e)=~/抽屉|/i
            # 每个子组件加上柜子名
            门板张数=门板张数+1
          end
          拟读取属性s.each{|k|
            # h[k]=Hash[]
            # 属性值字典=读属性_单个_字典形式(e,k,转mm)
            属性值=get动态属性(e,k,转mm=true)
            h[k]=属性值
          }
        }
        h2=Hash[]
        h2["上缝隙宽度"]=h["bjqfx11"]
        h2["下缝隙宽度"]=h["bjqfx12"]
        h2["左缝隙宽度"]=h["bjqfx13"]
        h2["右缝隙宽度"]=h["bjqfx14"]
        h2["中缝隙宽度"]=h["bjqfx2"]
        h2["门板张数"]=门板张数
        # puts h2
        发送提示信息("方法完成")
        return h2
    end


    def 修改动态组件缝隙(h2)
      选择对象=返回选择对象
      if 选择对象.nil?
        return
      end
      h=Hash[]
      h["bjqfx11"]=h2["上缝隙宽度"]
      h["bjqfx12"]=h2["下缝隙宽度"]
      h["bjqfx13"]=h2["左缝隙宽度"]
      h["bjqfx14"]=h2["右缝隙宽度"]
      h["bjqfx2"]=h2["中缝隙宽度"]
      子组件s=grep集合中组件s(选择对象)
      子组件s.to_a.each {|e|
        puts 定义名加实例名(e)
        h.each{|k,v|
          puts k
          puts v
          # value = e.set_attribute "dynamic_attributes", k, v #只能是英寸？
          set动态属性(e,k,v)
        }
        $dc_observers.get_class_by_version(e).redraw_with_undo(e)
      }
      发送提示信息("方法完成")
    end

  end #class
end
