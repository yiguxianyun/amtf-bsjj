require 'sketchup.rb'
require 'json'
module AMTF_mixin
  def 找中点(p1, p2)
    return unless p1 && p2
    x = (p2.x + p1.x) / 2
    y = (p2.y + p1.y) / 2
    z = (p2.z + p1.z) / 2
    return Geom::Point3d.new x, y, z
  end

  def 提取深度(s)
    # match=/-sd([-\d.]+)/.match("399-amtf-zhengyuan-ABF-DSIDE-8-sd34")
    # match=/-sd([-\d.]+)/.match("399-amtf-zhengyuan-ABF-DSIDE-8-sd-1")
    # if match[1]=="-1"
    #   puts "match[1] 👉 #{match[1]}"
    # end
    match=/-sd([-\d.]+)/i.match(s)
    if match
      # puts "match 👉 #{match[1]}"
      if match[1]=="-1"
        return @板厚mm
      else
        return match[1].to_f
      end
    else
      return 0
    end
  end

  def 识别边线类型(e)
    if @属于已处理曲线的边s.index(e)
      # puts "属于已处理曲线的边s!"
      return 类型="属于已处理曲线的边s"
    end
    if e.curve # is curve?
      edges=e.curve.edges
      @属于已处理曲线的边s=@属于已处理曲线的边s.concat(edges)
      if e.curve.typename != "ArcCurve" # is no arc? -> is polyline
        return 类型="曲线"
      else
        return 类型=曲线是整圆还是圆弧(e.curve)
        # puts "e.curve.end_angle 👉 #{e.curve.end_angle}"
        # if e.curve.end_angle==Math::PI*2
        #   return 类型="整圆"
        # else
        #   return 类型="圆弧"
        # end
      end
    else
      return 类型="直线"
    end
  end

  def 转换点坐标(e, tform,面方位)
    # 点或者位置
    if e.class==Sketchup::Vertex
      p=e.position
    else
      p=e
    end
    # point = Geom::Point3d.new(p.x, p.y, p.z)
    point = Geom::Point3d.new(p.x, p.y, p.z)
    # 转换到父级坐标系
    point.transform! tform
    point=视图转换(point,面方位)

    point = Geom::Point3d.new(point.x* $stl_conv, point.y* $stl_conv, point.z* $stl_conv)
    return point.to_a
    # 这里to_a并没有进行单位转换，但是把to_a的结果转换成字符串或者打印的时候，会显示成mm单位(可能是文档设置的单位)👆
  end

  def 视图转换(point,面方位)
    tr  = Geom::Transformation.new(Geom::Point3d.new(0,0,-@板厚))
    point.transform! tr
    视图间距=@导出["视图间距"]
    板宽=@导出["板宽"]
    板高=@导出["板高"]
    # puts "面方位 👉 #{面方位}"
    case 面方位
    when "顶"
      # # tr  = Geom::Transformation.new(Geom::Point3d.new(0,0,-@板厚.to_mm))
      # tr  = Geom::Transformation.new(Geom::Point3d.new(0,0,-@板厚))
      # point.transform! tr
    when "底"
      point2 = Geom::Point3d.new(板宽/2, 0, -@板厚/2)
      rot = Geom::Transformation.rotation(point2,Y_AXIS,180.degrees)
      point.transform! rot
      vector = Geom::Vector3d.new(板宽+视图间距+@板厚+视图间距, 0, 0)
      tr  = Geom::Transformation.new(vector)
      point.transform! tr
    end
    return point
  end

  def 检测模型单位
    cu=Sketchup.active_model.options['UnitsOptions']["LengthUnit"]
    # cu=Sketchup.active_model.options[0]["LengthUnit"]
    case cu
    when 4
        current_unit= "Meters"
    when 3
        current_unit= "Centimeters"
    when 2
        current_unit= "Millimeters"
    when 1
        current_unit= "Feet"
    when 0
        current_unit= "Inches"
    end

    case current_unit
    when "Meters"
        $stl_conv=0.0254
    when "Centimeters"
        $stl_conv=2.54
    when "Millimeters"
        $stl_conv=25.4
    when "Feet"
        $stl_conv=0.0833333333333333
    when "Inches"
        $stl_conv=1
    end
  end

  def 曲线是整圆还是圆弧(curve)
    start_angle = curve.start_angle
    end_angle = curve.end_angle
    # puts "start_angle"
    # puts start_angle.radians
    # puts end_angle.radians
    角度差=(end_angle-start_angle).abs.radians
    # puts "角度差"
    # puts 角度差
    if 角度差==360.0
      return "整圆"
    else
      return "圆弧"
    end
  end

  def 组件及子元素放到指定图层(c,指定图层)
    设置常用对象
    图层对象 = @layers.add(指定图层)
    c.layer=图层对象
    组或群entities(c).each{|e|
      e.layer=图层对象
    }
    grep组件的子组件s(c).each{|e|
      组件及子元素放到指定图层(e,指定图层)
    }
    # 发送提示信息("方法完成")
  end

  def 解读编号(解读名称)
    编号=""
    编号match=/__(\d+)./.match(解读名称)
    if 编号match
      编号=编号match[1]
    end
    return 编号
  end
  def 解读单个组件名(解读名称,可能只有柜子名=false)
    成品柜名=""
    单元柜名=""
    板名=""
    订单=""
    客户=""
    封边s=""
    编号=解读编号(解读名称)
      # 解读名称=e.name
      # puts "解读名称:"+解读名称
      去除编号=解读名称.sub(/__(\d+). /, "")
      去除编号sp=去除编号.split("_")
      # 拆分封边sp=去除编号sp.split("_封边")
      # 封边match=/_封边\[(\S+)\]/.match("组件实例名_封边[0_0_]")
      # 封边s= 封边match[1]
      封边match=/_封边\[(\S+)\]/.match(解读名称)
      if 封边match
        封边s= 封边match[1]
      end

      柜子sp=去除编号.split("_")
      # puts 柜子sp.count
      # 柜子sp.each{|e|
      #   puts "分割后每个项目👇"
      #   puts e
      # }


      if 柜子sp.count==1 && !可能只有柜子名
        板名=柜子sp[0]
        # puts "板名:"+板名
      else
        if 成品柜名 == ""
          成品柜名=柜子sp[0]
        end
        if 单元柜名 == ""
          单元柜名=柜子sp[1]
        end
        if 板名 == ""
          板名=柜子sp[2]
        end
        if 订单 == ""
          订单=柜子sp[3]
        end
        if 客户 == ""
          客户=柜子sp[4]
        end

      end
    return 成品柜名,单元柜名,板名,订单,客户,封边s,编号
  end

  def grep集合中组件s(entities)
    return 原有的子组件s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)
  end
  def grep组件的子组件s(c)
    entities=组或群entities(c)
    return grep集合中组件s(entities)
  end
  def grep组件的面边s(c)
    entities=组或群entities(c)
    return 面边s=entities.grep(Sketchup::Face)+entities.grep(Sketchup::Edge)

  end
  def grep组件的边s(c)
    entities=组或群entities(c)
    return 边s=entities.grep(Sketchup::Edge)
  end

  def 删除软化边(c)
    边s=grep组件的边s(c)
    拟删除es=[]
    边s.each{|e|
      if e.soft?
        拟删除es.push e
      end
    }
    if 拟删除es.count>0
      entities=组或群entities(c)
      entities.erase_entities(*拟删除es)
    end
  end

  def grep组件的独立边s(c)
    entities=组或群entities(c)
    边s=entities.grep(Sketchup::Edge)
    面s=entities.grep(Sketchup::Face)
    return 独立边s=边s+entities.grep(Sketchup::Edge)
    return 独立边s

  end
  def 获取组件or最大面材质名称(群组)
    puts 定义名加实例名(群组)
    材质=群组.material
    if 材质 !=nil
      # UI.messagebox 材质.name
      return @材质名称=材质.name
    end

    面h=面积排序(群组)
    最大面=面h.keys[0]
    if 最大面.nil?
      result = UI.messagebox("#{定义名加实例名(群组)} 没有顶层面，含有下级组件，先炸开吧")
    end
    材质=最大面.material
    if 材质 !=nil
      return @材质名称=材质.name
    end
    @材质名称="未指定"
    return @材质名称

  end

  def 获取entities材质(entities)
    材质hash=Hash[]
    entities.each {|e|
      if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
        材质名称=获取组件or最大面材质名称(e)
        if !材质hash.has_key?(材质名称)
          材质hash[材质名称]=[]
        end
        材质hash[材质名称].push e
      end
    }
    return 材质hash
  end

  def 删除所有面
      # 删除所有面
    # entities = Sketchup.active_model.entities
    entities = Sketchup.active_model.active_entities
    entities.grep(Sketchup::Face) {|e|
      # entities.erase_entities(*拟删除es)
      e.erase!
      # puts e
    }
  end

  def 从外向内炸开组件(组件)
    array = 组件.explode
    array.each{|e|
      if e.is_a?( Sketchup::Group ) or e.is_a?( Sketchup::ComponentInstance )
        # e.explode
        从外向内炸开组件(e)
      end
    }
  end

  def 从外向内炸开子组件(组件)
    entities=组或群entities(组件)
    组件s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)
    if 组件s.count >0
      组件s.each{|e|
        从外向内炸开组件(e)
      }
    end
  end

  def 选中或全部对象()
    设置常用对象
    if @selection.count == 0
      entities = @model.active_entities
    else
      entities=@selection
    end
    return entities
  end

  def 生成唯一文件夹(拟添加中文后缀)
    file=@model.path
    # p @model.path
    路径 = File.dirname(file).freeze
    文件名 = File.basename(file, '.*')
    today = Time.new;
    时间后缀= today.strftime("%Y%m%d");
    唯一名=""
    数字序号=1
    导出路径="#{路径}/导出dxf"
    Dir.mkdir(导出路径)
    loop do
      唯一名="#{导出路径}/#{文件名}_#{拟添加中文后缀}a#{时间后缀}_#{数字序号}"
      puts "唯一名===#{唯一名}"
      if File.directory?(唯一名)
        数字序号=数字序号+1
      else
        Dir.mkdir(唯一名)
        break
      end
    end
    return  唯一名
  end

  def 解析文件名(文件全名)
    文件短名 = File.basename 文件全名
    后缀 = File.extname  文件全名
    无后缀名 = File.basename 文件全名, 后缀
    路径 = File.dirname  文件全名
    puts "路径"
    puts 路径
		路径.force_encoding('UTF-8') if 路径.respond_to?(:force_encoding)
    puts 路径

    return 路径,文件短名,无后缀名,后缀
  end

  def 解析文件名V2(文件全名)
    @文件短名 = File.basename 文件全名
    @后缀 = File.extname  文件全名
    @无后缀名 = File.basename 文件全名, @后缀
    @路径 = File.dirname  文件全名
    # puts "路径"
    # puts 路径
		# 路径.force_encoding('UTF-8') if 路径.respond_to?(:force_encoding)
    # puts 路径

  end

  # 路径中有特殊字符遍历不了，改成由nodejs完成
  def 生成唯一文件名(全名,拟添加中文后缀)
    # 拟添加中文后缀="排版"
    # 文件短名 = File.basename 全名
    # 后缀 = File.extname  全名
    # 无后缀名 = File.basename 全名, 后缀
    # 路径 = File.dirname  全名
    路径,文件短名,无后缀名,后缀=解析文件名(全名)
    pattern = File.join(路径, "#{无后缀名}_#{拟添加中文后缀}*.skp")

    # pattern = "#{路径}/#{无后缀名}*.skp"
    # pattern = "#{路径}/#{无后缀名}_#{拟添加中文后缀}*.skp"
    puts pattern
    Dir.glob(pattern).each { |file|
      puts "找到的文件👉 "+file
    }

    amtf.nil

    today = Time.new;
    时间后缀= today.strftime("%Y%m%d");
    数字序号=1
    唯一名=""
    loop do
      唯一名="#{路径}/#{无后缀名}_#{拟添加中文后缀}a#{时间后缀}_#{数字序号}#{后缀}"
      if File::exists?(唯一名)
        数字序号=数字序号+1
      else
        puts 唯一名
        break
      end
    end
    puts "没到这里来吗？"
    puts 唯一名
    return 唯一名
  end

  def 生成唯一文件名fff(全名,拟添加中文后缀)
    # 拟添加中文后缀="排版"
    # 文件短名 = File.basename 全名
    # 后缀 = File.extname  全名
    # 无后缀名 = File.basename 全名, 后缀
    # 路径 = File.dirname  全名
    路径,文件短名,无后缀名,后缀=解析文件名(全名)

    today = Time.new;
    时间后缀= today.strftime("%Y%m%d");
    数字序号=1
    唯一名=""
    loop do
      唯一名="#{路径}/#{无后缀名}_#{拟添加中文后缀}a#{时间后缀}_#{数字序号}#{后缀}"
      if File::exists?(唯一名)
        数字序号=数字序号+1
      else
        puts 唯一名
        break
      end
    end
    puts "没到这里来吗？"
    puts 唯一名
    return 唯一名
  end

  def 读取全部动态属性h(e,转mm=true)
    puts "读取全部动态属性h 👇"
    # puts "A全部xyz简称👉  #{A全部xyz简称}"
    h=Hash[]
    dic=根据元素确定dic(e)
    h["_name"]=dic["_name"]
    # 拟读取属性s=A全部xyz简称 #后面会把 A全部xyz简称 给改掉，又学废了
    拟读取属性s=[]+A全部xyz简称

    # A全部xyz简称.each{|a|
    #   h[a]=Hash[]
    # }

    排除属性arr = ["description", "dialogheight", "dialogwidth",
      "imageurl","itemcode","name","scaletool","summary","onclick"
    ]+A全部xyz简称
    # 获取其他属性排除掉xyzlenxyz
    dic.each { | k, v |
      if 排除属性arr.include?(k)
        next
      end
      是否_开头=/^_/.match(k)
      if 是否_开头
        next
      end
      # puts "#{k}👉#{v}"
      拟读取属性s.push(k)
    }

    # 上面指定要读的属性👆
    拟读取属性s.each{|k|
      h[k]=Hash[]
      属性值字典=读属性_单个_字典形式(e,k,转mm)
      h[k]=属性值字典
    }
    # puts "A全部xyz简称👉  #{A全部xyz简称}"
    puts "读取全部动态属性h 👆"
    # amtf.nil
    return h #带参数的方法必须retrun？
  end

  def get动态属性(e,k,转mm=true)
    # 常规值=dic[k] #用这种方式设置属性值时，出现了奇怪的问题！
    属性值=(e.get_attribute "dynamic_attributes", k )
    puts "属性值:#{属性值} #{属性值.class}"
    if 转mm
      属性值=属性值.to_f.to_mm.round(3)
    end
    return 属性值
  end

  def set动态属性(e,k,v)
    puts v.class
    # 传过来的可能是float也可能是整数
    # if v.class=="Float"
    #   v=v.to_f/25.4
    # end
    v=v.to_f/25.4
    value = e.set_attribute "dynamic_attributes", k, v #只能是英寸？
    # dic[k]=属性值 #用这种方式设置属性值时，出现了奇怪的问题！

    return value
  end

  def 读属性_单个_字典形式V2(e,k,转mm=true)
    # 得空改成 get_attribute 方式试试看
    dic=根据k确定dic(e,k)
    公式k="_#{k}_formula"
    公式值=dic[公式k]
    # puts "转换前  #{公式k} 公式值 👉#{公式值}"
    if 公式值!=nil#优先读公式值
      # 如果不需要转成mm，就原样输出,导出json时不需要转换
      if 转mm
        公式值=(公式值.to_f*10)#只是加了个等号的表达式可以这么转mm
        # 复杂的表达式还不知道怎么转换……
        puts "转换后  #{公式k} 公式值 👉#{公式值}"
      end
    end
    公式单位=dic["_#{k}_formulaunits"]
    if 转mm
      if 常规值.class==String #传入的是 公式表达式
        #不需要转换
        if 公式单位=="CENTIMETERS"
          puts "公式单位==CENTIMETERS  转换前的值  #{常规值}" #
          # 常规值=(常规值.to_f*25.4).round(1)#英寸转mm
          常规值=(常规值.to_f*25.4)#英寸转mm
          # puts "公式单位==CENTIMETERS  转换后的值  #{属性值}"
        else
          # 没有设置单位，默认为文本
          常规值=(常规值.to_f*10)#文本cm转mm
        end
      else
        # 属性值="#{属性值*1.0/10}"
        常规值=常规值.to_f.to_mm
      end
      puts "#{k}转换后的  常规值👉 #{常规值}"
    end

    h=Hash[]
    # h["常规值"]=常规值 #这玩意没用
    h["公式值"]=公式值
    h["公式单位"]=公式单位
    return h
  end

  def 读属性_单个_字典形式(e,k,转mm=true,常规值圆整=true)
    # puts "读属性_单个_字典形式 转mm=#{转mm}"
    dic=根据k确定dic(e,k)
    公式k="_#{k}_formula"
    公式值=dic[公式k]
    # puts "转换前  #{公式k} 公式值 👉#{公式值}"
    if 公式值!=nil#优先读公式值
      # 如果不需要转成mm，就原样输出,导出json时不需要转换
      if 转mm
        公式值=(公式值.to_f*10)#只是加了个等号的表达式可以这么转mm
        # 复杂的表达式还不知道怎么转换……
        puts "转换后  #{公式k} 公式值 👉#{公式值}"
      end
    else
      公式值="空"
    end

    if ["lenx","leny","lenz"].include?(k)
      常规值=dic["_#{k}_nominal"]
    else
      常规值=dic[k]
    end
    puts "#{k}转换前的  常规值👉 #{常规值}"
    公式单位=dic["_#{k}_formulaunits"]

    if 转mm
      if 常规值.class==String #传入的是 公式表达式
        #不需要转换
        if 公式单位=="CENTIMETERS"
          puts "公式单位==CENTIMETERS  转换前的值  #{常规值}" #
          # 常规值=(常规值.to_f*25.4).round(1)#英寸转mm
          常规值=(常规值.to_f*25.4)#英寸转mm
          # puts "公式单位==CENTIMETERS  转换后的值  #{属性值}"
        else
          # 没有设置单位，默认为文本
          常规值=(常规值.to_f*10)#文本cm转mm
        end
      else
        # 属性值="#{属性值*1.0/10}"
        常规值=常规值.to_f.to_mm
        if 常规值圆整
          常规值=常规值.round(2)
        end
      end
      puts "#{k}转换后的  常规值👉 #{常规值}"
    end

    h=Hash[]
    h["常规值"]=常规值
    h["公式值"]=公式值
    h["公式单位"]=公式单位
    return h
  end

  def 读属性_单个(e,k,转mm=true)
    puts "读属性_单个 转mm=#{转mm}"
    dic=根据k确定dic(e,k)
    公式k="_#{k}_formula"

    属性值=dic[公式k]
    puts "#{公式k} 公式值 👉#{属性值}"
    if 属性值!=nil#优先读公式值
      # 如果不需要转成mm，就原样输出
      if 转mm
        属性值=(属性值.to_f*10).round(0)#只是加了个等号的表达式可以这么转mm
        # 复杂的表达式还不知道怎么转换……
      end
    else
      if ["lenx","leny","lenz"].include?(k)
        属性值=dic["_#{k}_nominal"] #这种读取方式好像没意义啊！
      else
        属性值=dic[k]
      end
      # puts "没有填公式时的  属性值👉 #{属性值}"
      if 转mm
        if 属性值.class==String #传入的是 公式表达式
          #不需要转换
          公式单位=dic["_#{k}_formulaunits"]
          if 公式单位=="CENTIMETERS"
            # puts "公式单位==CENTIMETERS  转换前的值  #{属性值}" #
            属性值=(属性值.to_f*25.4).round(0)#英寸转mm
            # puts "公式单位==CENTIMETERS  转换后的值  #{属性值}"
          else
            # 没有设置单位，默认为文本
            属性值=(属性值.to_f*10).round(0)#文本cm转mm
          end
        else
          # 属性值="#{属性值*1.0/10}"
          属性值=属性值.to_f.to_mm.round(0)
        end
        puts "#{公式k} 转换后属性值 👉#{属性值}"
      end
    end
    return 属性值
  end

  def 改属性_单个_k值确定v值可能要转换(e,k,v)
    puts "改属性_单个_k值确定v值可能要转换……………………………………"
    dic=根据k确定dic(e,k)
    属性值=v
    puts "原来的k👉#{k}"
    puts "原来的属性值👉#{属性值}  属性值.class👉#{属性值.class}"
    if k=~/_formula/
      if !属性值.nil? #有可能有 公式这个k,但是v值没有设置
        if 属性值.class==Float #通过box识别过来的尺寸单位是mm,这里转换成 cm单位的 字符串
          属性值="#{属性值*1.0/10}" #通过box识别过来的尺寸单位是mm,这里转换成cm
          puts "转换后属性值👉#{属性值}  属性值.class👉#{属性值.class}"
        else
          # 传入的是从另外一个组件读过来的 公式表达式  为字符串 不需要转换
        end
      end
    end
    dic[k]=属性值
    # 最终对象.set_attribute 'dynamic_attributes', k, v
  end

  def 设置公式值_单个(e,k,v)
    dic=根据k确定dic(e,k)
    k="_#{k}_formula"
    puts "转换前的v👉#{v}"

    # if 属性值.class==String
    #   #不需要转换
    # else

      v="#{v*1.0/10}"#通过box识别过来的尺寸单位是mm,这里转换成cm
      # 属性值=属性值.to_f.to_mm.round(0)
    # end
    puts "转换后v👉#{v}"
    dic[k]=v
    # 最终对象.set_attribute 'dynamic_attributes', k, v
  end

  def 改属性_单个_传字典(e,k,v)
    # puts "改属性_单个_传字典"
    puts "原来的k👉#{k}  原来的v👉#{v}"

    dic=根据k确定dic(e,k)
    if ["_name" ,"_lengthunits","scaletool"].include?(k)
      dic[k]=v
      return
    end

    常规值=v["常规值"]
    puts "原来的常规值👉#{常规值}"

    公式单位=v["公式单位"]
    dic["_#{k}_formulaunits"]=公式单位
    # 公式单位=dic["_#{k}_formulaunits"]
    puts "公式单位#{公式单位}"
    if 公式单位=="CENTIMETERS"
      puts "公式单位==CENTIMETERS  转换前的值  #{常规值}" #
      puts "导出json时写入的是英寸单位，现在读回来也是英寸，不需要转换" #

      # puts "公式单位==CENTIMETERS  转换后的值  #{属性值}"
    else
      puts "没有设置单位，默认为文本"
      # 常规值=(常规值.to_f*25.4/10)  #英寸转cm
      常规值=(常规值.to_f*10)#文本cm转mm
    end
    puts "转换后的常规值👉#{常规值}"

    dic[k]=常规值#不设置这个，直接设置公式值的话，显示不出来
    label="_#{k}_label"
    dic[label]=k

    公式值=v["公式值"]
    formula="_#{k}_formula"
    if 公式值 !="空" && !公式值.nil?
      if 公式值.class==String
        #不需要转换
      else
        公式值="#{公式值*1.0/10}"#通过box识别过来的尺寸单位是mm,这里转换成cm
        # 属性值=属性值.to_f.to_mm.round(0)
      end
      puts "转换后公式值👉#{公式值}"
      dic[formula]=公式值
    end
    # 最终对象.set_attribute 'dynamic_attributes', k, v
  end

  def 根据元素确定dic(e)
    if  e.is_a?( Sketchup::Group)
      dic=获取or添加属性dic(e)
    elsif e.is_a?( Sketchup::ComponentDefinition)
      dic=获取or添加属性dic(e)
    else
      dic=获取or添加属性dic(e.definition)
    end
    return dic
  end

  #大写的为模块常量
  # A全部xyz公式 = ["_x_formula", "_y_formula", "_z_formula","_lenx_formula", "_leny_formula", "_lenz_formula"]
  A全部xyz简称=["x","y","z","lenx","leny","lenz"]
  Axyz=["x","y","z"]

  def 返回全部xyz公式全称
    return A全部xyz简称.map { |a| "_#{a}_formula"}
  end

  def 转mm可选取整(h,取整=true)
    h.each{|k,v|
      # puts "v"
      # puts v
      # if v != nil
      h[k]=h[k].to_mm
      if 取整
        h[k]=h[k].round(2)
      end
      # end
    }
    return h
  end

  def 展示设置(arg)
    self.execute_script("展示设置(#{arg.to_json})")
  end
  def 开启amtf_exe(arg)
    amtf_dxf文件=arg
    # exists1 = File::exists?(amtf_dxf文件)
    exists1 = File.directory?(arg)
    if exists1
      # 开启amtf_exe=Open3.popen3(`explorer #{amtf_dxf文件} /select,#{amtf_dxf文件}/amtf.exe`)
      全名="#{amtf_dxf文件}\\amtf.exe"
      puts 全名
      开启amtf_exe=Open3.popen3(`explorer /e,/select,#{全名}` )
      # 开启amtf_exe=Open3.popen3(amtf_dxf文件)
      # 开启amtf_exe= exec amtf_dxf文件
      # 开启amtf_exe= system(amtf_dxf文件)
      # 开启amtf_exe= system(amtf_dxf文件)
      puts "调用外部命令结果："
      puts 开启amtf_exe
    else
      # result = UI.messagebox("文件不存在！\n#{amtf_dxf文件}")
      result = UI.messagebox("文件路径  不存在！\n#{amtf_dxf文件}")
    end
  end

  A设置文件=File.join(__dir__, '设置.json')#大写的为模块常量

  def 读取设置
    json = File.read(A设置文件)
    读取设置 = JSON.parse(json)
  end

  def 保存设置(arg)
    原设置=读取设置
    puts 原设置["amtf_exe路径"]
    原设置["amtf_exe路径"]=arg["amtf_exe路径"]
    puts 原设置["amtf_exe路径"]
    File.write(A设置文件, JSON.dump(原设置))
  end

  def 公式k(k)
    公式k="_#{k}_formula"
  end

  def 获取or添加属性dic(e)
    dic存在=true
    if e.attribute_dictionaries.nil?
      puts "#没有任何属性字典，dic是nil的"
      dic存在=false
    else
      if e.attribute_dictionaries["dynamic_attributes"].nil?
        puts "#没有有属性字典，但是没有动态属性字典"
        dic存在=false
      end
    end
    if !dic存在
      e.set_attribute 'dynamic_attributes', "amtf", "amtf"
      dic = e.attribute_dictionaries["dynamic_attributes"]
      dic.delete_key("amtf")
    end
    dic = e.attribute_dictionaries["dynamic_attributes"]
    return dic
  end

  # def 获取或新建属性dic(e)
  #   if e.attribute_dictionaries.nil?
  #     e.set_attribute 'dynamic_attributes', "amtf", "amtf"
  #   else
  #     if e.attribute_dictionaries["dynamic_attributes"].nil?
  #       e.set_attribute 'dynamic_attributes', "amtf", "amtf"
  #     end
  #   end
  #   dic = e.attribute_dictionaries["dynamic_attributes"]
  #   # if dic.nil?#如果没有手动添加属性，dic是nil的
  #   #   e.set_attribute 'dynamic_attributes', "amtf", "amtf"
  #   #   dic = e.attribute_dictionaries["dynamic_attributes"]
  #   #   # dic=e.attribute_dictionaries["dynamic_attributes"]=Sketchup::AttributeDictionary.new()
  #   # end
  #   return dic
  # end

  def 根据k确定dic(e,k)
    xyzarr = ["x", "y", "z","_x_formula", "_y_formula", "_z_formula"]
    if  e.is_a?( Sketchup::Group)
      dic=获取or添加属性dic(e)
    elsif xyzarr.include?(k)
      dic=获取or添加属性dic(e)
    else
      dic=获取or添加属性dic(e.definition)
    end
    return dic
  end

  def 清除动态属性单个(e,k)
    dic=根据k确定dic(e,k)
    dic.delete_key(k)
  end

  def 读取动态属性单个(e,k)
    最终对象=根据k确定dic(e,k)
    读取动态属性单个=最终对象.get_attribute "dynamic_attributes", k
  end

  def 获取板厚
    entities=Sketchup.active_model.entities
    板厚hash=Hash[]
    entities.each {|e|
      if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
        边界=获取边界大小_只识别面(e)
        板厚=边界[0]
        (0...3).each{|i|
          if 边界[i]<板厚
            板厚=边界[i]
          end
        }
        板厚=板厚.to_mm.round(1)
        if !板厚hash.has_key?(板厚)
          板厚hash[板厚]=[]
        end
        板厚hash[板厚].push e
      end
    }
    # 板厚hash=Hash[]
    # 板厚hash[5]=[1,2,3]
    # 板厚hash[6]=[1,2,3,4,5]
    # 板厚数量hash=Hash[]
    # 板厚hash.each{|k,v|
    #   # puts v.count
    #   # puts v
    #   # 板厚数量hash["板厚#{k.to_mm.round(1)}"]="数量#{v.count}"
    #   板厚数量hash["板厚#{k.to_mm}"]="数量#{v.count}<br/>"
    # }
    # puts 板厚数量hash
    return 板厚hash
  end

  def 旋转组件轴到指定轴fff(对象,需对齐轴,垂直轴,被对齐轴)
    tr = 对象.transformation
    # puts "tr.origin:"
    # puts tr.origin
    # vector = tr.origin.vector_to ORIGIN
    # tr = Geom::Transformation.translation(vector)

    轴夹角 = 被对齐轴.angle_between 需对齐轴
    # puts "轴夹角:"
    # puts 轴夹角.radians
    # if 轴夹角!=0.0
    if !是否重合(轴夹角)
      两轴平行 = 被对齐轴.parallel?(需对齐轴)
      if !两轴平行
        旋转轴 = 需对齐轴 * 被对齐轴
      else
        旋转轴 = 垂直轴
      end
      旋转角度=-轴夹角
      rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
      对象.transform! rot
      # amtf.nil

      tr = 对象.transformation
      # 需对齐轴=需对齐轴
      轴夹角2 = 被对齐轴.angle_between 需对齐轴
      # puts "轴夹角2:"
      # puts 轴夹角2.radians
      # if z_anglez2!=0.0
      if !是否重合(轴夹角2)
        旋转角度=2*轴夹角
        # puts "旋转角度:"
        # puts 旋转角度.radians
        rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
        对象.transform! rot
        # amtf.nil
        tr = 对象.transformation
        # 需对齐轴=需对齐轴
        轴夹角3 = 被对齐轴.angle_between 需对齐轴
        # puts "转动后轴夹角3:"
        # puts 轴夹角3
        # puts 轴夹角3.radians
        # if 轴夹角3!=0.0
      if !是否重合(轴夹角3)
          UI.messagebox "试图对齐对象轴：#{需对齐轴}到 被对齐轴:#{被对齐轴}，没成功 ！"
          return
        end
      end
    end# 以上对齐z轴👆
  end

  def 旋转组件轴到全局坐标系(对象)
    tr = 对象.transformation
    puts "旋转前 tr.origin: xyz"
    puts tr.origin
    puts tr.xaxis
    puts tr.yaxis
    puts tr.zaxis
    # vector = tr.origin.vector_to ORIGIN
    # tr = Geom::Transformation.translation(vector)

    z_anglez = Z_AXIS.angle_between tr.zaxis
    puts "z_anglez:"
    puts z_anglez
    # if z_anglez!=0.0
    if !是否重合(z_anglez)
      对象平行z轴 = Z_AXIS.parallel?(tr.zaxis)
      if !对象平行z轴
        旋转轴 = tr.zaxis * Z_AXIS
      else
        旋转轴 = X_AXIS
      end
      旋转角度=-z_anglez
      rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
      对象.transform! rot
      # amtf.nil

      tr = 对象.transformation
      # tr.zaxis=tr.zaxis
      z_anglez2 = Z_AXIS.angle_between tr.zaxis
      # puts "z_anglez2:"
      # puts z_anglez2
      # if z_anglez2!=0.0
      if !是否重合(z_anglez2)
        旋转角度=2*z_anglez
        # puts "旋转角度:"
        # puts 旋转角度.radians
        rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
        对象.transform! rot
        # amtf.nil
        tr = 对象.transformation
        # tr.zaxis=tr.zaxis
        z_anglez3 = Z_AXIS.angle_between tr.zaxis
        # puts "转动后z_anglez3:"
        # puts z_anglez3
        # puts z_anglez3.radians
        # if z_anglez3!=0.0
      if !是否重合(z_anglez3)
          UI.messagebox "试图对齐对象z轴到全局坐标系，没成功 ！"
          return
        end
      end
    end# 以上对齐z轴👆

    # tr = 对象.transformation*e.transformation
    # tr.xaxis=tr.xaxis
    旋转轴 = Z_AXIS
    # 原来你的夹角，是不会区分逆时针和顺时针的，不会有负值，也不会超过180°
    x_anglex = tr.xaxis.angle_between X_AXIS
    # puts "x_anglex:"
    # puts x_anglex
    # puts x_anglex.radians
    # if x_anglex!=0.0
    if !是否重合(x_anglex)
      旋转角度=-x_anglex
      # puts "旋转角度:"
      # puts 旋转角度.radians
      rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
      对象.transform! rot

      tr = 对象.transformation
      # tr.xaxis=tr.xaxis
      x_anglex2 = tr.xaxis.angle_between X_AXIS
      # puts "转动后x_anglex2:"
      # puts x_anglex2
      # puts x_anglex2.radians
      # if x_anglex2!=0.0
      if !是否重合(x_anglex2)
        旋转角度=2*x_anglex
        # puts "旋转角度:"
        # puts 旋转角度.radians
        rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
        对象.transform! rot

        tr = 对象.transformation
        # tr.xaxis=tr.xaxis
        x_anglex3 = tr.xaxis.angle_between X_AXIS
        # puts "转动后x_anglex3:"
        # puts x_anglex3
        # puts x_anglex3.radians
        # if x_anglex3!=0.0
        if !是否重合(x_anglex3)
          UI.messagebox "试图对齐对象x轴到全局坐标系，没成功 ！ ！"
          amtf.nil
        end
      end
    end# 以上对齐x轴👆

    tr = 对象.transformation
    puts "旋转后 tr.origin: xyz"
    puts tr.origin
    puts tr.xaxis
    puts tr.yaxis
    puts tr.zaxis
  end

  def 旋转组件轴到原坐标系(对象,tr原来)
    tr = 对象.transformation
    puts "tr.origin:"
    puts tr.origin

    z_anglez = tr原来.zaxis.angle_between tr.zaxis
    puts "z_anglez:"
    puts z_anglez
    # if z_anglez!=0.0
    if !是否重合(z_anglez)
      对象平行z轴 = tr原来.zaxis.parallel?(tr.zaxis)
      if !对象平行z轴
        旋转轴 = tr.zaxis * tr原来.zaxis
      else
        旋转轴 = tr原来.xaxis
      end
      旋转角度=-z_anglez
      rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
      对象.transform! rot
      # amtf.nil

      tr = 对象.transformation
      # tr.zaxis=tr.zaxis
      z_anglez2 = tr原来.zaxis.angle_between tr.zaxis
      puts "z_anglez2:"
      puts z_anglez2
      # if z_anglez2!=0.0
      if !是否重合(z_anglez2)
        旋转角度=2*z_anglez
        puts "旋转角度:"
        puts 旋转角度.radians
        rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
        对象.transform! rot
        # amtf.nil
        tr = 对象.transformation
        # tr.zaxis=tr.zaxis
        z_anglez3 = tr原来.zaxis.angle_between tr.zaxis
        puts "转动后z_anglez3:"
        puts z_anglez3
        puts z_anglez3.radians
        # if z_anglez3!=0.0
      if !是否重合(z_anglez3)
          UI.messagebox "试图对齐对象z轴到全局坐标系，没成功 ！"
          return
        end
      end
    end# 以上对齐z轴👆

    # tr = 对象.transformation*e.transformation
    # tr.xaxis=tr.xaxis
    旋转轴 = tr原来.zaxis
    # 原来你的夹角，是不会区分逆时针和顺时针的，不会有负值，也不会超过180°
    x_anglex = tr.xaxis.angle_between tr原来.xaxis
    puts "x_anglex:"
    puts x_anglex
    puts x_anglex.radians
    # if x_anglex!=0.0
    if !是否重合(x_anglex)
      旋转角度=-x_anglex
      puts "旋转角度:"
      puts 旋转角度.radians
      rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
      对象.transform! rot

      tr = 对象.transformation
      # tr.xaxis=tr.xaxis
      x_anglex2 = tr.xaxis.angle_between tr原来.xaxis
      puts "转动后x_anglex2:"
      puts x_anglex2
      puts x_anglex2.radians
      # if x_anglex2!=0.0
      if !是否重合(x_anglex2)
        旋转角度=2*x_anglex
        puts "旋转角度:"
        puts 旋转角度.radians
        rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
        对象.transform! rot

        tr = 对象.transformation
        # tr.xaxis=tr.xaxis
        x_anglex3 = tr.xaxis.angle_between tr原来.xaxis
        puts "转动后x_anglex3:"
        puts x_anglex3
        puts x_anglex3.radians
        # if x_anglex3!=0.0
        if !是否重合(x_anglex3)
          UI.messagebox "试图对齐对象x轴到全局坐标系，没成功 ！ ！"
          amtf.nil
        end
      end
    end# 以上对齐x轴👆
  end

  def 判断面的法向(face)
    @方向h=Hash[]
    @方向h['左']=Geom::Vector3d.new(-1, 0, 0)
    # @方向h['左']=Geom::Vector3d.new(-1, 0, 0).transform(@编辑中tr)
    # @方向h['左']=Geom::Vector3d.new(-1, 0, 0)
    @方向h['右']=@方向h['左'].reverse

    @方向h['后']=Geom::Vector3d.new(0, 1, 0)
    # @方向h['后']=Geom::Vector3d.new(0, 1, 0).transform(@编辑中tr)
    # @方向h['后']=Geom::Vector3d.new(0, 1, 0)
    @方向h['前']=@方向h['后'].reverse

    @方向h['上']=Geom::Vector3d.new(0, 0, 1)
    # @方向h['上']=Geom::Vector3d.new(0, 0, 1).transform(@编辑中tr)
    # @方向h['上']=Geom::Vector3d.new(0, 0, 1)
    @方向h['下']=@方向h['上'].reverse

    面法向朝向=""
    @方向h.each{|k,v|
      # face.normal返回的是局部坐标系
      angle = face.normal.angle_between(v).radians
      degrees = angle.radians
      # p "angle="+angle.to_s
      if angle<0.1
        # p "选择面法向朝向："+k
        面法向朝向=k
      end
    }
    return 面法向朝向
  end

  def 组或群entities(e)
    if e.is_a?( Sketchup::ComponentInstance )
      组或群entities=e.definition.entities
    # elsif e.is_a?( Sketchup::Group )
    else
      组或群entities=e.entities
    end
  end

  def 是组件或群组?(e)
    if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
      return true
    else
      return false
    end
  end
  def 改定义或实例名(e,拟改名)
    if e.is_a?( Sketchup::ComponentInstance )
      e.definition.name=拟改名
    else
      e.name=拟改名
    end
  end
  def 定义或实例名(e)
    名称=""
    if e.is_a?( Sketchup::ComponentInstance )
      名称=e.definition.name
    else
      名称=e.name
    end
    return 名称
  end
  def 定义名加实例名(e)
    if e.deleted?
      return 定义名加实例名="改元素被删除了！"
    end
    if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
      # entities=e.definition.entities
      定义名加实例名="#{e.definition.name}&&#{e.name}"
    else
      定义名加实例名="没name&&#{e.to_s}"
    end
  end

  def 是否重合(角度)
    角度rd=角度.round(0.01)
    if 角度rd==0.00 or 角度rd==3.14
      return true
    end
    return false
  end

  def 把组件转移到某组件_mx(c,某组件)
    旧组件名=c.name
    # puts "某组件"
    # puts 某组件
    trr = c.transformation
    # amtf.nil

    c另一个=某组件.entities.add_instance(c.definition,trr)
    # c包裹 = 某组件.add_group(c)
    # amtf.nil
    c另一个.name=旧组件名
    c.erase!
    c=c另一个
    # amtf.nil
    return c
  end

  def 返回第一个选择对象(错误信息="请先选择一个 对象")
    设置常用对象
    if @selection.count == 0
      UI.messagebox 错误信息
      return nil
    else
      return 对象=@selection[0]
    end
  end

  def 返回全部或选择对象
    model = Sketchup.active_model
    selection = Sketchup.active_model.selection
    if selection.count == 0
      entities = model.active_entities
    else
      entities=selection
    end
    return entities
  end

  def 返回选择对象
    设置常用对象
    if @selection.count == 0
      UI.messagebox "没有选中 任何 对象 ！"
      return nil
    else
      return @selection
    end
  end

  def 设置常用对象
    @model = Sketchup.active_model
    @entities = @model.active_entities
    @顶层entities = @model.entities
    @selection = @model.selection
    @layers = @model.layers
    @materials = @model.materials

    @当前模型全名=@model.nil? ?  "" : @model.path
    if @entities[0]==nil
      #su中没有可见对象
    else
      if @entities[0].parent.kind_of? Sketchup::Model
        @编辑中组件名称=nil
      else
        @编辑中组件=@entities[0].parent
        @编辑中组件名称=@编辑中组件.name
      end
    end

    @编辑中tr=@model.edit_transform
    @原点 = Geom::Point3d.new(0,0,0)
    @编辑中局部原点=@原点.transform(@编辑中tr)
    @definitions = @model.definitions

  end

  def get_dir
    p "hello AF!"
    file = __FILE__.dup
    file.force_encoding("UTF-8")
    if file.respond_to?(:force_encoding)
      f = File.basename(file, '.*').freeze
      p "File.basename="+f
      r = File.dirname(file).freeze+"/组件模板"
      p "File.dirname="+r
      return r
      # p "File.join(r,f)="+File.join(r,f)
      # return File.join(r,f)
    end
  end
  def 清除未使用
    # p "hello 清除未使用!"
    设置常用对象
    # @model = Sketchup.active_model
    之前数量=@model.definitions.length
    p  "现有definitions数量："+@model.definitions.length.to_s
    @model.definitions.purge_unused
    之后数量=@model.definitions.length
    清除组件数量=之前数量-之后数量
    p  "清除未使用后，剩下definitions数量：" + @model.definitions.length.to_s

    之前数量=@model.materials.length
    @model.materials.purge_unused
    之后数量=@model.materials.length
    清除材料数量=之前数量-之后数量
    Sketchup::set_status_text "清除组件:"+清除组件数量.to_s+"  清除材质："+清除材料数量.to_s


  end

  def 含子组件?(e)
    entities=e.definition.entities
    entities.each {|e|
      if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
        return true
      end
    }
    return false
  end
  def 获取子组件(e)
    子组件arr=[]
    if e.kind_of? Sketchup::Model
      entities=e.entities
    else
      entities=e.definition.entities
    end

    entities.each {|e|
      if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
        子组件arr.push e
      end
    }
    return 子组件arr
  end

  def 炸开所有子组件主体(e,当前层级)
    @层级h[e]=当前层级
    entities=e.definition.entities
    entities.each {|e|
      if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
        当前层级+=1
        炸开所有子组件主体(e,当前层级)
      end
    }
    #递归到底，开始返回
    if @层级h[e]>0
      提示信息="炸开:"+e.definition.name
      p 提示信息
      发送提示信息(提示信息)
      e.explode
    end
  end
  def 发送提示信息(提示信息)
    # p 提示信息=="方法完成"
    if 提示信息=="方法完成"
      上个被调用方法名=caller_locations(1,1)[0].label
      # p 上个被调用方法名
      提示信息=上个被调用方法名+"  完成!"
    else
      提示信息=提示信息
    end
    # puts 提示信息
    Sketchup::set_status_text 提示信息
  end
  def 文件改名(替换字符)
    # chosen_folder = UI.select_directory(title: "选文件夹",directory: "d:")
    chosen_folder = UI.select_directory(title: "请选择 拟改名文件所在 文件夹")
  end

  def 处理scale(e)
    scale_x = 1
    scale_y = 1
    scale_z = 1
    # puts e
    if (e.is_a? Sketchup::Group) || (e.is_a? Sketchup::ComponentInstance)
      # puts "e.is_a? Sketchup::Group || (e.is_a? Sketchup::ComponentInstance)"
      scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
      scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
      scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    end
    return scale_x,scale_y,scale_z
  end

  def 获取边界h_只认边和面_创建dxf用(e)
    h={}
    scale_x,scale_y,scale_z=处理scale(e)
    box = Geom::BoundingBox.new
    grep组件的面边s(e).each{|ee|
      box.add(ee.bounds)
    }
    h["lenx"]=(box.width  * scale_x)
    h["leny"]=(box.height  * scale_y)
    h["lenz"]=(box.depth  * scale_z)
    h["box"]=box

    # 获取的是局部坐标👇
    # puts "h[lenx] 👉 #{h["lenx"]}"
    # puts "h[leny] 👉 #{h["leny"]}"
    # puts "h[lenz] 👉 #{h["lenz"]}"
    # puts "box.corner(0) 👉 #{box.corner(0)}"
    return h
  end

  def 获取边界h_只认face(e,直接传入entities=false)
    h={}
    scale_x = 1
    scale_y = 1
    scale_z = 1
    # puts e
    if (e.is_a? Sketchup::Group) || (e.is_a? Sketchup::ComponentInstance)
      # puts "e.is_a? Sketchup::Group || (e.is_a? Sketchup::ComponentInstance)"
      scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
      scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
      scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    end
    box = Geom::BoundingBox.new
    if 直接传入entities
      entities=e
    else
      entities=组或群entities(e)
    end
    entities.grep(Sketchup::Face){|face|
      box.add(face.bounds)
    }

    h["lenx"]=(box.width  * scale_x)
    h["leny"]=(box.height  * scale_y)
    h["lenz"]=(box.depth  * scale_z)
    h["box"]=box

    # 获取的是局部坐标👇
    # puts "h[lenx] 👉 #{h["lenx"]}"
    # puts "h[leny] 👉 #{h["leny"]}"
    # puts "h[lenz] 👉 #{h["lenz"]}"
    # puts "box.corner(0) 👉 #{box.corner(0)}"
    return h
  end

  # def 递归获取边界h_只认face_主体(c,box)
  #   grep组件的面边s(c).each{|e|
  #     puts "#{定义名加实例名(c)}  加上面边box"
  #     puts "box.width == #{box.width}"
  #     box.add(e.bounds)
  #     puts "box.width == #{box.width}"
  #   }
  #   grep组件的子组件s(c).each{|子组件|
  #     # box2 = Geom::BoundingBox.new
  #     # box2=递归获取边界h_只认face_主体(子组件,box2)
  #     # box.add(box2)
  #     box.add(子组件.bounds)
  #   }
  #   return box
  # end

  def 获取边界h_只认边和面(e,排除隐藏对象=false,加入原点box=true,排除超界组件=true)
    # return unless e.respond_to?(:transformation)
    获取边界h=Hash[]
    scale_x = 1
    scale_y = 1
    scale_z = 1
    box = Geom::BoundingBox.new
    # puts e
    if 是组件或群组?(e)
      # puts "e.is_a? Sketchup::Group || (e.is_a? Sketchup::ComponentInstance)"
      scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
      scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
      scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
      grep组件的面边s(e).each{|ee|
        box.add(ee.bounds)
      }
      grep组件的子组件s(e).each{|ee|
        跳过这个=false
        if 排除隐藏对象 && !ee.visible?
          跳过这个=true
        end
        if 排除超界组件 && 定义名加实例名(ee)=~ /超界/i
          跳过这个=true
        end
        if !跳过这个
          box.add(ee.bounds)
        end
      }
      if 加入原点box
        box.add(ORIGIN)
      end
    else #如果是其他元素
      box.add(e.bounds)
    end

    if box
      获取边界h["lenx"]=(box.width  * scale_x)
      获取边界h["leny"]=(box.height  * scale_y)
      获取边界h["lenz"]=(box.depth  * scale_z)
      获取边界h["box"]=box
    end
    puts "交出去的是什么？"
    return 获取边界h
  end

  def 获取边界h(e,排除隐藏对象=false,加入原点box=true)
    # return unless e.respond_to?(:transformation)
    获取边界h=Hash[]
    scale_x = 1
    scale_y = 1
    scale_z = 1
    tr=e.transformation
    获取边界h["tr"]=tr
    # puts e
    if (e.is_a? Sketchup::Group) || (e.is_a? Sketchup::ComponentInstance)
      # puts "e.is_a? Sketchup::Group || (e.is_a? Sketchup::ComponentInstance)"
      scale_x = ((Geom::Vector3d.new 1,0,0).transform! tr).length
      scale_y = ((Geom::Vector3d.new 0,1,0).transform! tr).length
      scale_z = ((Geom::Vector3d.new 0,0,1).transform! tr).length
    end
    box = Geom::BoundingBox.new
    跳过这个=false
    if 排除隐藏对象 && !e.visible?
      跳过这个=true
    end
    if !跳过这个
      if e.is_a? Sketchup::Group
          e.entities.each {|en| box.add(en.bounds) }
      elsif e.is_a? Sketchup::ComponentInstance
          box = e.definition.bounds
      else
        box = e.bounds
      end
    end
    # point1 = Geom::Point3d.new(0, 0, 0)
    if 加入原点box
      box.add(ORIGIN)
    end
    if box
      获取边界h["lenx"]=(box.width  * scale_x)
      获取边界h["leny"]=(box.height  * scale_y)
      获取边界h["lenz"]=(box.depth  * scale_z)
      获取边界h["box"]=box
    end
    # puts "交出去的是什么？"
    return 获取边界h
  end

  def 获取边界大小_加入原点box(e,排除隐藏对象=false,加入原点box=true)
    # return unless e.respond_to?(:transformation)
    scale_x = 1
    scale_y = 1
    scale_z = 1
    # puts e
    if (e.is_a? Sketchup::Group) || (e.is_a? Sketchup::ComponentInstance)
      # puts "e.is_a? Sketchup::Group || (e.is_a? Sketchup::ComponentInstance)"
      scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
      scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
      scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    end
    box = Geom::BoundingBox.new
    跳过这个=false
    if 排除隐藏对象 && !e.visible?
      跳过这个=true
    end
    if !跳过这个
      if e.is_a? Sketchup::Group
          e.entities.each {|en| box.add(en.bounds) }
      elsif e.is_a? Sketchup::ComponentInstance
          box = e.definition.bounds
      else
        box = e.bounds
      end
    end
    # point1 = Geom::Point3d.new(0, 0, 0)
    if 加入原点box
      box.add(ORIGIN)
    end
    if box
        dims = [
            width  = (box.width  * scale_x),
            height = (box.height * scale_y),
            depth  = (box.depth  * scale_z),
            # width  = (box.width  * scale_x).to_mm.round(2),
            # height = (box.height * scale_y).to_mm.round(2),
            # depth  = (box.depth  * scale_z).to_mm.round(2),
            box,
            [scale_x,scale_y,scale_z]
        ]
        # puts "scale_x:\t#{scale_x}"
        # puts "scale_y:\t#{scale_y}"
        # puts "scale_z:\t#{scale_z}"
        return dims
    end
  end

  def 获取边界大小(e)
    # return unless e.respond_to?(:transformation)
    scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
    scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
    scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    box = nil
    if e.is_a? Sketchup::Group
        box = Geom::BoundingBox.new
        e.entities.each {|en| box.add(en.bounds) }
    elsif e.is_a? Sketchup::ComponentInstance
        box = e.definition.bounds
    end

    if box
        dims = [
            width  = (box.width  * scale_x),
            height = (box.height * scale_y),
            depth  = (box.depth  * scale_z),
            # width  = (box.width  * scale_x).to_mm.round(2),
            # height = (box.height * scale_y).to_mm.round(2),
            # depth  = (box.depth  * scale_z).to_mm.round(2),
            box,
            [scale_x,scale_y,scale_z]
        ]
        # puts "scale_x:\t#{scale_x}"
        # puts "scale_y:\t#{scale_y}"
        # puts "scale_z:\t#{scale_z}"
        return dims
    end
  end
  def 获取边界大小V2(e)
    # return unless e.respond_to?(:transformation)
    box = nil
    if e.is_a? Sketchup::Group
        box = Geom::BoundingBox.new
        e.entities.each {|en| box.add(en.bounds) }
    elsif e.is_a? Sketchup::ComponentInstance
        box = e.definition.bounds
    else
      box = e.bounds
    end

    scale_x = 1
    scale_y = 1
    scale_z = 1
    if e.is_a? Sketchup::Group or e.is_a? Sketchup::ComponentInstance
      scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
      scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
      scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    end
    if box
        dims = [
            width  = (box.width  * scale_x),
            height = (box.height * scale_y),
            depth  = (box.depth  * scale_z),
            # width  = (box.width  * scale_x).to_mm.round(2),
            # height = (box.height * scale_y).to_mm.round(2),
            # depth  = (box.depth  * scale_z).to_mm.round(2),
            box,
            [scale_x,scale_y,scale_z]
        ]
        # puts "scale_x:\t#{scale_x}"
        # puts "scale_y:\t#{scale_y}"
        # puts "scale_z:\t#{scale_z}"
        return dims
    end
  end

  def 获取边界大小排除部分组件(e,排除项目)
    # return unless e.respond_to?(:transformation)
    scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
    scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
    scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    box = nil
    if e.is_a? Sketchup::Group or e.is_a? Sketchup::ComponentInstance
      box = Geom::BoundingBox.new
      e.entities.each {|en|
        if en.is_a? Sketchup::Face
          box.add(en.bounds)
        end
      }
    end
    if box
        dims = [
            width  = box.width  * scale_x,
            height = box.height * scale_y,
            depth  = box.depth  * scale_z,
            box
        ]
        # puts "x尺寸:\t#{dims[0].to_l}\ny尺寸:\t#{dims[1].to_l}\nz尺寸:\t#{dims[2].to_l}"
        return dims
    end
  end
  def 面积排序(群组)
    entities = 群组.entities
    # entities = @model.active_entities
    面h=Hash[]
    entities.each {|ee|
      if ee.is_a?( Sketchup::Face )
        面=ee
        bb面积=获取面BoundingBox面积(面)#如果是开了个很大的槽，槽面积可能大于板顶底面面积
        # 面积=面.area
        面h[面]=bb面积
      end
    }

    面h= Hash[面h.sort_by {|key,value| value}.reverse!]#按面积排序
    return 面h
  end

  def 计算组件缩放比例(e)
    scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
    scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
    scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    return 组件缩放比例[scale_x,scale_y,scale_z]
  end

  def 获取边界大小_只识别面(e)
    scale_x = ((Geom::Vector3d.new 1,0,0).transform! e.transformation).length
    scale_y = ((Geom::Vector3d.new 0,1,0).transform! e.transformation).length
    scale_z = ((Geom::Vector3d.new 0,0,1).transform! e.transformation).length
    box = nil
    if e.is_a? Sketchup::Group or e.is_a? Sketchup::ComponentInstance
      box = Geom::BoundingBox.new
      # entities=
      组或群entities(e).each {|en|
        if en.is_a? Sketchup::Face
          box.add(en.bounds)
        end
      }
    end
    if box
        dims = [
            width  = box.width  * scale_x,
            height = box.height * scale_y,
            depth  = box.depth  * scale_z,
            box
        ]
        # puts "x尺寸:\t#{dims[0].to_l}\ny尺寸:\t#{dims[1].to_l}\nz尺寸:\t#{dims[2].to_l}"
        return dims
    end
  end

  def 获取entities边界大小(entities)
    box = Geom::BoundingBox.new
    边长=0
    entities.each {|en|
      box.add(en.bounds)
    }

    [box.width,box.height,box.depth].each{|e|
      puts e
      if e>0
        边长=e
        break
      end
    }
    return box,边长
  end

  def 获取面BoundingBox面积(face)
    box=face.bounds
    bb面积=1
    [box.width,box.height,box.depth].each{|e|
      if e!=0
        bb面积=e*bb面积
      end
    }
    return bb面积
  end
end
