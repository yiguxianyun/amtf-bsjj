module AMTF
  class DLG_其他 < UI::HtmlDialog
    include AMTF_mixin
    def initialize
      # Dialog parameters
      # super "amtf", false, "amtf", 350, 600, 50, 100, true
      # puts File.join(__dir__, '另存排版.rb')
      options = {
        dialog_title: "其他😄",
        preferences_key: "其他😄",
        # scrollable: false,
        # resizable: false,
        width: 400,
        height: 160,
        left: 0,
        top: 100,
        # min_width: 50,
        # min_height: 150,
        max_width: 1000,
        max_height: 1000,
        style: UI::HtmlDialog::STYLE_DIALOG
      }
      super(options)
      path = File.join(__dir__, '/html/其他.html')
      set_file path
      # set_url("http://localhost:12112")

      @observer_选中对象=nil
      # Display the dialog box
      if RUBY_PLATFORM.index "darwin"
        show_modal
      else
        show
      end
      add_action_callback("测试1") {|dialog, arg|
        puts arg
        对象=返回第一个选择对象
        边界=获取边界h_只认face(对象)

      }
      add_action_callback("测试2") {|dialog, arg|
        puts arg
        Askp_to_dxf.new().file方式创建dxf
      }
      add_action_callback("修改动态组件缝隙") {|dialog, arg|
        puts arg
        A门缝.new().修改动态组件缝隙(arg)
        # self.execute_script("展示门板信息(#{h.to_json})")
        # puts h
      }
      add_action_callback("识别选中门板") {|dialog, arg|
        puts arg
        h=A门缝.new().识别选中门板
        self.execute_script("展示门板信息(#{h.to_json})")
        # puts h
      }
      add_action_callback("生成标准标记层") {|dialog, arg|
        puts arg
        A标记.new().生成标准标记层
      }
      add_action_callback("穿透显示") {|dialog, arg|
        puts "穿透显示"
        puts arg
        # Sketchup.active_model.select_tool A穿透选择.new().穿透显示
        A穿透选择.new().穿透显示
      }
      add_action_callback("躺平到原点导出dxf") {|dialog, arg|
        A导出.new(self).躺平到原点导出dxf()
      }
      add_action_callback("穿透选择并隐藏") {|dialog, arg|
        Sketchup.active_model.select_tool A穿透选择.new()
      }
      add_action_callback("保存设置") {|dialog, arg|
        结果=保存设置(arg)
      }
      add_action_callback("开启amtf_exe") {|dialog, arg|
        结果=开启amtf_exe(arg)
      }
      add_action_callback("匹配尺寸") {|dialog, arg|
        Sketchup.active_model.select_tool A匹配尺寸.new
      }
      add_action_callback("读取设置") {|dialog, arg|
        设置=读取设置()
        展示设置(设置)
        发送提示信息("方法完成")
      }
      add_action_callback("布尔") {|dialog, arg|
        A其他.new().布尔()
      }
      add_action_callback("不同板厚放不同标记层") {|dialog, arg|
        # 发送提示信息("不同板厚放不同标记层")
        A其他.new().不同板厚放不同标记层()
        # return json
      }
      add_action_callback("不同材质放不同标记层") {|dialog, arg|
        # 发送提示信息("不同板厚放不同标记层")
        A其他.new().不同材质放不同标记层()
        # return json
      }
      add_action_callback("返回json") {|dialog, arg|
        json=A其他.new().返回json()
        puts json
        self.execute_script("展示json(#{json})")
        发送提示信息("方法完成")
        # return json
      }
      add_action_callback("导入model") {|dialog, arg|
        A其他.new().导入model()
        # puts arg+" 完成！"
        发送提示信息("方法完成")
      }
      add_action_callback("尺寸标注放到指定图层") {|dialog, arg|
        A其他.new().尺寸标注放到指定图层(arg)
        # puts arg+" 完成！"
        # 发送提示信息("方法完成")
      }

      add_action_callback("拼合组件名") {|dialog, arg|
        puts "收到的组件名："+arg
        A导出.new().拼合组件名(arg)
      }

      add_action_callback("解读组件名") {|dialog, arg|
        # 对象=返回第一个选择对象
        # puts "对象.name"+对象.name
        解读后的组件名=A导出.new().解读组件名
        # 发送解读后的组件名(解读后的组件名)
        self.execute_script("接收解读后的组件名('#{解读后的组件名}')")
      }


      add_action_callback("选择同材质组件") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().选择同材质组件()
      }
      add_action_callback("移除组件材质") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().移除组件材质()
      }
      add_action_callback("移除组件and子级材质") {|dialog, arg|
        # puts "amtf"
        AF另存排版.new().移除组件and子级材质()
      }
      add_action_callback("更改背板厚度") {|dialog, arg|
        puts arg
        AF另存排版.new().更改背板厚度(arg)
      }

      add_action_callback("测试") {|dialog, arg|
        # puts arg
          # 联系amtf
          # 测试连接
          对象=返回第一个选择对象()
          A导入.new().炸开组件判断是否有孤立面(对象)


          # A导出.new().删除参考线(对象)

          # 设置常用对象
          # A导出.new().内部边界转子群组(@model)

          # A导出.new().焊接组件边线(对象)
          # A导出.new().导出v2
          # A组件属性.new().遍历组件属性
          # AMTF窗口组件属性.new
          # reload其他插件
          # A导入.new().A导入('D:\\桌面\\新建文件夹\\导出stl-标准柜20230208-1335-29')
          # A导入.new().A导入('D:/桌面/新建文件夹/导出stl-标准柜20230208-1335-29')
          # 设置常用对象
          # 对象=@entities[0].parent
          # A导入.new().识别孔槽(对象)

          # 新边界尺寸=Hash[]
          # 新边界尺寸["x"]=6567.0
          # 新边界尺寸["y"]=240.0
          # 新边界尺寸["z"]=1000
          # A其他.new().缩放边界(新边界尺寸)

          发送提示信息("方法完成")
      }

      add_action_callback("对齐") {|dialog, arg|
        # Sketchup.active_model.select_tool( AF对齐.new )
        Sketchup.active_model.select_tool AF对齐.new
      }
      add_action_callback("多余组件处理") {|dialog, arg|
        AF另存排版.new().多余组件处理
      }
      add_action_callback("画柜子") {|dialog, arg|
        # p "hello"
        Sketchup.active_model.select_tool AF画柜子.new(arg+".skp")
      }
      add_action_callback("另存为排版模型") {|dialog, arg|
        AF另存排版.new().另存为排版模型
      }
      add_action_callback("删除指定图层") {|dialog, arg|
         Sketchup::set_status_text __method__.to_s()+" ing"
        AF另存排版.new().删除指定图层
      }
      add_action_callback("删除隐藏项目") {|dialog, arg|
        AF另存排版.new().删除隐藏项目不传参数
      }
      add_action_callback("删除or炸开无板字组件") {|dialog, arg|
        AF另存排版.new().删除or炸开无板字组件
      }
      add_action_callback("炸开所有子组件") {|dialog, arg|
        AF另存排版.new().炸开所有子组件
      }
      add_action_callback("组件改名") {|dialog, arg|
        AF另存排版.new().组件改名
      }
      add_action_callback("组件转群组") {|dialog, arg|
        AF另存排版.new().组件转群组
      }
      add_action_callback("干涉检查") {|dialog, arg|
        # UI.messagebox("我执行了！")
        AF另存排版.new().干涉检查
      }
      add_action_callback("一键排版预处理") {|dialog, arg|
        处理结果=AF另存排版.new().一键排版预处理(arg)
        self.execute_script("返回一键排版预处理结果('#{处理结果}')")

      }
      add_action_callback("检查板厚") {|dialog, arg|
        返回结果=A导出.new().检查板厚()
        self.execute_script("返回_返回结果('#{返回结果}')")
        # self.execute_script("返回_检查板厚_处理结果('#{处理结果.to_json}')")

      }
      add_action_callback("检查材质") {|dialog, arg|
        返回结果=A导出.new().检查材质()
        self.execute_script("返回_返回结果('#{返回结果}')")
      }
      add_action_callback("延伸背板") {|dialog, arg|
        AF另存排版.new().延伸背板(arg)
      }

      add_action_callback("文件改名") {|dialog, arg|
        文件改名("kk")
      }
      add_action_callback("返回值给ruby") {|dialog, arg|
        return arg
      }
      add_action_callback("清除未使用") {|dialog, arg|
        清除未使用
      }

    end #initialize

  end #AMTF窗口
end # module amtf_su
