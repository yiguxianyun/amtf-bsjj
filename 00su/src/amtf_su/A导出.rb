require 'sketchup.rb'
require File.join(__FILE__, '../amtf_mixin.rb')
require 'json'
# require 'open3'
# require 'win32ole'
# 此版本是使用导出到单独su再导出dxf的方式，在尝试改成创建dxf的方式
module AMTF
  class A导出
    include AMTF_mixin
    def initialize(窗口)
      @窗口对象=窗口
      puts "@窗口对象"
      puts @窗口对象
      @各个面对应图层={
        "顶"=>"0",
        "底"=>"1",
        "左"=>"2",
        "右"=>"3",
        "上"=>"5",
        "下"=>"4",
      }
      @封边厚度={
        "左"=>"0",
        "右"=>"0",
        "上"=>"0",
        "下"=>"0",
      }
      @各个面对应颜色={
        "顶"=>"red",
        "底"=>"red",
        "左"=>"white",
        "右"=>"white",
        "上"=>"white",
        "下"=>"white",
      }
      # puts @各个面对应图层["下"]
      @标签关键词="标签|_ABF_Label|HT_Label"
      @创建dxf拟排除关键词=@标签关键词+"|sideDrillDepth"
      @沿线加工关键词="online|沿线"
      @导出={}
      @rad_to_deg=180/Math::PI
      # @A画边界框m=Sketchup.active_model.select_tool A画边界框.new
      @容差=0.1
    end

    def 导出前检查()
      entities=选中或全部对象()
      组件s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)

      拟导出组件名数组=[]
      拟导出全名hash=Hash[]
      @拟导出组s=[]
      @处理完成组s=[]
      拟删除es=[]

      隐藏了的图层=获取隐藏了的图层()
      if 隐藏了的图层.count >0
        res = UI.messagebox("有隐藏的图层,可能是忘记显示了。是否要  自动打开并继续？", MB_YESNOCANCEL)
        if res == IDCANCEL
          return "出错了"
        elsif res == IDYES
          隐藏了的图层.each{|e|
            e.visible=true
          }
        else
          # 选择了否，正常往下走
        end
      end
      隐藏了的顶层组件=获取隐藏了的顶层组件(组件s)
      if 隐藏了的顶层组件.count >0
        res = UI.messagebox("有隐藏的顶层组件,可能是忘记显示了。是否要  自动显示并继续？", MB_YESNOCANCEL)
        if res == IDCANCEL
          return "出错了"
        elsif res == IDYES
          隐藏了的顶层组件.each{|e|
            e.visible=true
          }
        else
          # 选择了否，正常往下走
        end
      end


      #先判断有没有重名组件
      发送提示信息("正在判断 有没有重名组件……" )
      # entities.each {|e|
      组件s.each {|e|
          拟导出组名=e.name
          关键词位置= 拟导出组名=~ /参考|布局盒子/i
          # puts "关键词位置:"
          # puts 关键词位置
          if 关键词位置!=nil
            puts "拟排除群组"
            puts 拟导出组名
            next
            puts "看到我说明上面的next没有奏效！"
          end

          if 拟导出组件名数组.include?(拟导出组名)
            UI.messagebox "有 重名 的组件: #{拟导出组名}   !  \n 请修改！"
            return
          else
            匹配HT编号=/(\d+)-/.match(拟导出组名)
            匹配amtf或者ABF编号=/__(\d+)./.match(拟导出组名)
            if 匹配HT编号==nil && 匹配amtf或者ABF编号==nil
              puts "匹配HT编号==nil"
              # puts e
            else
              # puts "e.visible?"
              # puts e.visible?
              if e.visible? #排除隐藏组件
                # puts "e.layer.visible?"
                # puts e.layer.visible?
                if e.layer.visible?
                  拟导出组件名数组.push 拟导出组名
                  # e.visible=false
                  @拟导出组s.push e
                  # puts "拟导出组名👇"
                  # puts 拟导出组名
                end
              end
            end
          end
      }
      # amtf.nil
      if 拟导出组件名数组.count==0
        UI.messagebox "没有符合要求的群组可以导出!  \n 是不是，全部都没有添加AMTF 或者 HT 或者 ABF标签？"
        return "出错了"
      end

      显示不能隐藏的底层层()

      @model.save
      # 👇开始对当前模型进行了修改、清理、另存为，另存为的时候，可能会提示是否要保存？
      # 显示不能隐藏的底层层()
      return @当前模型全名,@拟导出组s
    end#def

    def 调用node创建dxf(创建dxf全名,板厚mm,计数,总数)
      封边信息=@封边信息 #应该称为  封边配置信息  板厚小于多少不封边等等
      # 导出时打开的su文件=@窗口对象.导出时打开的su文件
      导出时打开的su文件=@导出时打开的su文件

      # 导出全名=k
      # 原缩放大小=v
      # 创建dxf全名,板厚mm=逐个导出dxfV2(导出全名)
      计数信息="#{计数}/#{总数}"
      puts "计数信息👇"
      puts 计数信息
      发送提示信息(计数信息 )  #为什么不显示？
      # amtf.nil
      # kk=调用amtf_dxf(创建dxf全名+"|"+计数.to_s+"|"+封边信息)
      # kk=调用amtf_dxf("#{创建dxf全名}|#{板厚mm}|#{计数}|#{封边信息}")

      # 创建dxf全名加上计数和封边配置信息
      创建dxf全名="#{创建dxf全名}|#{板厚mm}|#{计数}|#{封边信息}|#{总数}"
      替换特殊字符 = 创建dxf全名.gsub(/\\/, "/")
      if !替换特殊字符.nil?
        创建dxf全名=替换特殊字符
      end
      # puts "创建dxf全名 👉 #{创建dxf全名}"
      # amtf.nil
      生产={}
      生产[:创建dxf全名]=创建dxf全名
      生产[:模型边s]=@模型边s
      # 生产[:计数]=计数
      # puts "调用amtf_dxf2('#{生产.to_json}')"
      @窗口对象.execute_script("调用amtf_dxf2_创建dxf('#{生产.to_json}')")

    end

    def 导出dxf(拟导出全名hash)
      封边信息=@封边信息
      # 导出时打开的su文件=@窗口对象.导出时打开的su文件
      导出时打开的su文件=@导出时打开的su文件
      计数=1

      # 异步执行，一团糟
      # 拟导出全名s=拟导出全名hash.keys
      # i=0
      # pb = ProgressBar.new(拟导出全名s.size, "导出dxf ing")
      # timer =UI.start_timer(0.000005, true) {
      #   if i == 拟导出全名hash.size
      #     UI.stop_timer(timer)
      #     Sketchup.set_status_text("导出dxf 完成")
      #     result = Sketchup.open_file(导出时打开的su文件)
      #     result = UI.messagebox('导出完成，是否 需要 打开导出 文件夹？', MB_YESNO)
      #     if result == IDYES
      #       # system('D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe')
      #       UI.openURL(@导出路径)
      #       # Open3.popen3("CopyClip.exe '#{导出路径}'")
      #     end
      #   end
      #   if i < 拟导出全名s.size
      #     begin
      #       导出全名=拟导出全名s[i]
      #       导出dxf全名,板厚mm=逐个导出dxfV2(导出全名)
      #       # amtf.nil
      #       信息="导出dxf  第#{i}  个 共 #{拟导出全名hash.count}  个}"
      #       puts 信息
      #       发送提示信息(信息 )  #为什么不显示？
      #       # amtf.nil
      #       # kk=调用amtf_dxf(导出dxf全名+"|"+计数.to_s+"|"+封边信息)
      #       # kk=调用amtf_dxf("#{导出dxf全名}|#{板厚mm}|#{计数}|#{封边信息}")
      #       导出dxf全名="#{导出dxf全名}|#{板厚mm}|#{i}|#{封边信息}"
      #       导出dxf全名 = 导出dxf全名.gsub!(/\\/, "/")
      #       puts "导出dxf全名"
      #       puts 导出dxf全名
      #       # amtf.nil
      #       # 生产[:导出dxf全名]=导出dxf全名
      #       # 生产[:板厚mm]=板厚mm
      #       # 生产[:计数]=计数
      #       # puts "调用amtf_dxf2('#{生产.to_json}')"
      #       @窗口对象.execute_script("调用amtf_dxf2('#{导出dxf全名}')")
      #     rescue => exception
      #       puts exception
      #     end
      #   end
      #   pb.update(i)
      #   i += 1
      # }

      拟导出全名hash.each {|k,v|
          导出全名=k
          原缩放大小=v
          导出dxf全名,板厚mm=逐个导出dxfV2(导出全名)
          计数信息="#{计数}/#{拟导出全名hash.count}"
          puts 计数信息
          发送提示信息(计数信息 )  #为什么不显示？
          # amtf.nil
          # kk=调用amtf_dxf(导出dxf全名+"|"+计数.to_s+"|"+封边信息)
          # kk=调用amtf_dxf("#{导出dxf全名}|#{板厚mm}|#{计数}|#{封边信息}")

          导出dxf全名="#{导出dxf全名}|#{板厚mm}|#{计数}|#{封边信息}|#{拟导出全名hash.count}"
          导出dxf全名 = 导出dxf全名.gsub!(/\\/, "/")
          puts "导出dxf全名"
          puts 导出dxf全名
          # amtf.nil
          # 生产[:导出dxf全名]=导出dxf全名
          # 生产[:板厚mm]=板厚mm
          # 生产[:计数]=计数
          # puts "调用amtf_dxf2('#{生产.to_json}')"
          @窗口对象.execute_script("调用amtf_dxf2('#{导出dxf全名}')")
          计数 += 1
          # puts "调用amtf_dxf(导出dxf全名):#{kk}"
        }

          Sketchup.set_status_text("导出dxf 完成")
          result = Sketchup.open_file(导出时打开的su文件)
          result = UI.messagebox('导出完成，是否 需要 打开导出 文件夹？', MB_YESNO)
          if result == IDYES
            # system('D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe')
            UI.openURL(@导出路径)
            # Open3.popen3("CopyClip.exe '#{导出路径}'")
          end

    end

    def 另存skp(每块板数,唯一文件夹,拟导出组s,封边信息,导出时打开的su文件)
      # amtf.nil
      @导出路径=唯一文件夹
      @拟导出组s=拟导出组s
      puts "收到的封边信息  配置信息  形如 true-1_12_板数1"
      puts 封边信息
      @封边信息=封边信息
      @导出时打开的su文件=导出时打开的su文件
      # model = Sketchup.active_model
      设置常用对象
      # @当前模型全名=model.path
      文件名 = File.basename(@当前模型全名, '.*')
      另存文件名=文件名
      另存文件名=另存文件名+'_另存文件_摆正'
      @总另存全名=@导出路径+'/'+另存文件名+'.skp'
      puts "@总另存全名"
      puts @总另存全名
      status = @model.save(@总另存全名)
      if !status
        UI.messagebox "另存为 #{@总另存全名} 出错，找老鱼！"
        return
      end
      发送提示信息("另存为文件：#{@总另存全名}" )
      #👆

      设置常用对象#另存为后真的不用设置了吗？那怎么有时候把原文件给躺平了？
      层s=@model.layers
      层s.to_a.each {|e|
        if /参考|布局|刻线/.match(e.name)
          提示信息="删除图层👉#{e.name}"
          层s.remove(e,true)
        end
      }

      拟删除es=@顶层entities.to_a-@拟导出组s
      if 拟删除es.count>0
        @顶层entities.erase_entities(*拟删除es)
      end
      # amtf.nil

      # 删除以后要保存一下，不然后面会报错？
      @model.save
    end

    def 收尾处理
      if @自动保存摆正文件
        设置常用对象
        @definitions.purge_unused
        @model.save
      end

      Sketchup.set_status_text("导出dxf 完成")
      result = Sketchup.open_file(@导出时打开的su文件)
      result = UI.messagebox('导出完成，是否 需要 打开导出 文件夹？', MB_YESNO)
      if result == IDYES
        # system('D:\amtf\amtf_csharp\amtf_dxf\bin\Debug\netcoreapp3.1/amtf_dxf.exe')
        UI.openURL(@导出路径)
        # Open3.popen3("CopyClip.exe '#{导出路径}'")
      end
    end

    def 导出组件到单独文件时的主名称(拟导出对象,每块板数)
      @材质名称=获取组件or最大面材质名称(拟导出对象)
      # 产品-柜名_成品名称_纹理(1=横纹)_柜号1_柜号2_数量_-1(不知道什么含义)_订单_客户_材质
      组件实例名=拟导出对象.name
      puts "组件实例名 👉 #{组件实例名}"
      成品柜名,单元柜名,板名,订单,客户,封边s,编号=解读单个组件名(组件实例名)

      纹理=1#(1=横纹)
      柜号1=编号
      柜号2="00"
      数量=每块板数
      # 订单=去除编号sp[1]
      # 客户=去除编号sp[2]
      材质=@材质名称
      关键词位置= 材质=~ /\([\s\S]+\)/i
      if 关键词位置==nil
        puts "材质:"
        材质="默认板材(#{材质})"
        puts 材质
      end
      @开料主信息="#{成品柜名}-#{单元柜名}_#{板名}_#{纹理}_#{柜号1}_#{柜号2}_#{数量}_-1_#{订单}_#{客户}_#{材质}_封边[#{封边s}]"
      # 主名称=@导出路径+'/'+@开料主信息+'.skp'
      主名称=@导出路径+'/'+@开料主信息
      puts "主名称 👉 #{主名称}"
      return 主名称
    end

    def 识别模型(c)
      # 上面躺平的时候，已经炸开组件重新设置了原点，局部坐标系和全局是一致的
      # result = Sketchup.send_action("viewIso:")
      # result = Sketchup.send_action("viewZoomExtents:")
      原组件名=c.name
      @已经处理过的曲线=[]
      边界h=获取边界h_只认face(c)
      @板厚=边界h["lenz"]
      @板厚mm=@板厚.to_mm.round(1)
      视图间距 ||= 40.mm
      板宽=边界h["lenx"]
      板高=边界h["leny"]
      @属于已处理曲线的边s=[]
      @导出["视图间距"]=视图间距
      @导出["板宽"]=板宽
      @导出["板高"]=板高
      # 下移一个板厚
      # puts "@板厚 👉 #{@板厚}"  #英寸数值 39.37007874015748
      # puts "@板厚.to_mm 👉 #{@板厚.to_mm}"  #毫米数值 1000
      # # tr  = Geom::Transformation.new(Geom::Point3d.new(0,0,-@板厚.to_mm))
      # tr  = Geom::Transformation.new(Geom::Point3d.new(0,0,-@板厚))
      # c.transform! tr
      # # amtf.nil

      # tr=c.transformation
      entities=组或群entities(c)
      @已经在loop中找到过的边s=[]
      顶底面h={}
      entities.grep(Sketchup::Face){|e|
        bb = e.bounds
        pt=bb.center
        if Z_AXIS.parallel?(e.normal)
          # puts "pt.z 到底是什么单位？"  #是mm
          # puts pt.z
          # 单独打印pt.z 显示的mm,但是……下面不to_mm的话，后面转换成  标记 字符串 时 会是inch
          # puts "pt.z.to_mm 👉 #{pt.z.to_mm}"
          离顶距离=(pt.z.to_mm-@板厚mm).abs
          离底距离=(pt.z.to_mm-0).abs
          # puts "离顶距离 👉 #{离顶距离}"
          if 离顶距离  < @容差
            顶底面h[0]=[e,"顶"]
            # 炸开面外部轮廓的弧线(e) if @炸开圆弧
            # 识别顶层级别的顶底面(c,e,"顶")
          elsif 离底距离 < @容差
            顶底面h[1]=[e,"底"]
            # 炸开面外部轮廓的弧线(e) if @炸开圆弧
            # 识别顶层级别的顶底面(c,e,"底")
          end
        end
      }

      顶底面h= Hash[顶底面h.sort_by {|k,v| k}]
      顶底面h.each{|k,v|
        面=v[0]
        面方位=v[1]
        炸开面外部轮廓的弧线(面) if @炸开圆弧
        识别顶层级别的顶底面(c,面,面方位)
      }

      原有的子组件s=grep组件的子组件s(c)
      重组后的子组件s=包裹其他元素_炸开重组子组件(c,原有的子组件s)
      # amtf.nil
      重组后的子组件s.each{|e|
        识别子组件(e)
      }

      # amtf.nil

      加入上下左右轮廓线坐标
      # amtf.nil
      return @板厚mm
    end

    def 识别子组件(c)
      tr=c.transformation
      entities=组或群entities(c)

      边界h=获取边界h_只认边和面_创建dxf用(c)
      # 边界h=获取边界h_只认边和面(c,false,false)
      宽=边界h["lenx"].to_mm.round(1)
      深=边界h["leny"].to_mm.round(1)
      厚度=边界h["lenz"].to_mm.round(1)
      # puts "宽 👉 #{宽}"
      # puts "深 👉 #{深}"
      # puts "厚度 👉 #{厚度}"
      体积=宽*深*厚度
      box=边界h["box"]
      # puts "左底前点 👉 #{box.corner(0)}"
      # puts "左顶前点 👉 #{box.corner(4)}"
      左底前点=box.corner(0).transform!(c.transformation)
      左顶前点=box.corner(4).transform!(c.transformation)
      # puts "左底前点 👉 #{左底前点}"
      # puts "左顶前点 👉 #{左顶前点}"


      # puts 定义名加实例名(c)
      # puts "厚度👇"
      # puts 厚度
      # amtf.nil
      面方位=""
      # puts "左顶前点.z 👉 #{左顶前点.z}" #打印出来是毫米，比较的时候是英寸！！！！！！！
      puts "左顶前点.z 👉 #{左顶前点.z}"
      puts "左顶前点.z.to_mm 👉 #{左顶前点.z.to_mm}"
      puts "@板厚mm-@容差 👉 #{@板厚mm-@容差}"
      # puts "左顶前点.z >= (@板厚mm-@容差) 👉 #{左顶前点.z.to_mm >= (@板厚mm-@容差)}"
      if 左顶前点.z.to_mm >= (@板厚mm-@容差)
        面方位="顶"
        比较基准z=@板厚mm
        比较基准点=Geom::Point3d.new 0, 0, 比较基准z.mm
      elsif 左底前点.z.to_mm <= @容差
        面方位="底"
        比较基准z=0
        比较基准点=Geom::Point3d.new 0, 0, 比较基准z.mm
      end
      puts "面方位 👉 #{面方位}"
      return if 面方位==""

      @需要焊接的散线s=[]
      @需要焊接的散线h={}

      # 孔槽是用实体子组件表示的情况👇
      if 体积>0.0
        平行且靠近基准的面={}
        平行且远离基准的面={}
        entities.grep(Sketchup::Face).each{|e|
          if Z_AXIS.parallel?(e.normal)
            bb = e.bounds
            pt=bb.center
            puts "pt转换前 👉 #{pt}"
            pt.transform! tr
            puts "pt转换后 👉 #{pt}"
            x=pt.x.to_mm.round(1)
            y=pt.y.to_mm.round(1)
            标记="#{x}_#{y}"
            puts "标记 👉 #{标记}"
            离基准距离=(pt.z.to_mm-比较基准z).abs
            puts "离基准距离 👉 #{离基准距离}"
            if 离基准距离 <0.1
              平行且靠近基准的面[标记]=e
            else
              平行且远离基准的面[标记]=e
            end
          end
        }

        平行且靠近基准的面.each{|k,v|
          edges=v.edges
          远离基准的面=平行且远离基准的面[k]
          # puts "比较基准点 👉 #{比较基准点}"
          深度=比较基准点.distance_to_plane(远离基准的面.plane).to_mm
          # puts "深度 👉 #{深度}"
          edges.each{|ee|
            识别边线(ee,c,面方位,深度,非板件外轮廓需要焊接散线=true)
          }
        }

      else #扁平边线的方式表示孔槽👇
        # if 面方位="顶"
          entities.grep(Sketchup::Edge).each{|ee|
            h=识别边线(ee,c,面方位,深度=0,非板件外轮廓需要焊接散线=true)
            # puts "h[颜色] 👉"
            # puts h["颜色"]
          }
          # end
      end

      # 一个子组件识别完成，进行后处理
      焊接散线(c)
    end

    def 添加进模型边并检查是否要复制到底面(h,c)
      # puts " 添加进去了 👉 #{h["类型"]}"
      @模型边s.push h
      # 复制通孔槽到底面
      if h["深度"] >= @板厚mm
        # tr=c.transformation #已经换算到了全局坐标系，复制到底面时，不需要再换算了
        h2 = {} #直接用h的话会把原对象修改掉！
        h.each{|k,v|
          h2[k]=v
        }
        面方位="底"
        dxf图层名=@各个面对应图层[面方位]
        h2["图层"]=dxf图层名

        if h["类型"]=="整圆"
          # e=h["圆心"]
          # point= Geom::Point3d.new(e[0]/ $stl_conv, e[1]/ $stl_conv, e[2]/ $stl_conv)
          # point=转换点坐标(point, IDENTITY,面方位)
          # h2["圆心"]=point
          # @模型边s.push h2
        else
          点s=h2["点s"]
          points = []
          点s.each{|e|
            points << Geom::Point3d.new(e[0]/ $stl_conv, e[1]/ $stl_conv, e[2]/ $stl_conv)
          }
          点s2 = []
          points.each{|point|
            点s2.push(转换点坐标(point, IDENTITY,面方位))
          }
          h2["点s"]=点s2
          @模型边s.push h2
        end
      end
    end

    def 进行逆时针排序点s(点s)
      points = []
      点s.each{|e|
        points << Geom::Point3d.new(e[0], e[1], e[2])
      }
      # 计算中心点
      center = Geom::Point3d.new(0, 0, 0)
      average_point = Geom::Point3d.new(points.sum { |point| point.x } / points.length.to_f, points.sum { |point| point.y } / points.length.to_f, 0)

      # 按逆时针排序点数组
      sorted_points = points.sort_by { |point| Math.atan2(point.y - average_point.y, point.x - average_point.x) }

      # 输出排序后的点数组
      返回点s=[]
      sorted_points.each_with_index do |point, index|
        # puts "Point #{index + 1}: #{point}"
        # puts "point.to_a 👉 #{point.to_a}"
        返回点s.push(point.to_a)
      end
      # puts "返回点s 👉 #{返回点s}"
      return 返回点s
      # amtf.nil
    end

    def 焊接散线(c)
      if @需要焊接的散线s.count>0
        @需要焊接的散线s.uniq!
        points=进行逆时针排序点s(@需要焊接的散线s)
        h=@需要焊接的散线h
        h["类型"]="多段线"
        h["点s"]=points
        # @模型边s.push h
        添加进模型边并检查是否要复制到底面(h,c)
      end
    end

    def 包裹其他元素_炸开重组子组件(c,子组件s)
      # box只能获取局部坐标系的xyz大小，不炸开重组的话，可能因为坐标系没对齐获取不正确
      entities=组或群entities(c)

      # 先把顶层元素临时包裹起来，防止自动合并……把要炸开的组件先包裹起来就不用这样了吧
      # 拟临时成组元素s=entities.to_a-拟处理群组s

      # 临时成组 = entities.add_group(拟临时成组元素s.to_a)
      # 临时成组 = entities.add_group(拟临时成组元素s)
      # 临时成组.name="临时群组"

      重组后的子组件s=[]
      子组件s.each {|e|
        if /#{@创建dxf拟排除关键词}/i.match(定义名加实例名(e))
          # 别动
        else
          重组后的子组件=组件炸开重设原点_非顶层子组件(c,e)
          重组后的子组件s.push 重组后的子组件
        end
      }
      # amtf.nil
      # 临时成组.explode
      return 重组后的子组件s
    end

    def 组件炸开重设原点_非顶层子组件(parent,c)
      旧组件名=c.name
      转移到顶层的组件=@entities.add_instance(c.definition,(parent.transformation)*c.transformation)
      c.erase!
      改定义或实例名(转移到顶层的组件,旧组件名)
      c=组件炸开重设原点(转移到顶层的组件)
      c=把组件转移到某组件(c,parent)
      return c
    end

    def 加入上下左右轮廓线坐标
      # puts "@板厚 👉 #{@板厚}"
      # puts "@板厚.to_mm 👉 #{@板厚.to_mm}"
      # puts "@板厚mm 👉 #{@板厚mm}"
      板厚=@板厚mm.mm
      # puts "板厚 👉 #{板厚}"
      视图间距=@导出["视图间距"]
      板宽=@导出["板宽"]
      板高=@导出["板高"]
      @封边厚度["左"]=5
      @封边厚度["右"]=6
      @封边厚度["上"]=7
      @封边厚度["下"]=8

      面方位="左"
      左视图角点=[]
      左视图角点[0] = Geom::Point3d.new(-视图间距-板厚, 0, 0)
      左视图角点[1] = Geom::Point3d.new(-视图间距, 0, 0)
      左视图角点[2] = Geom::Point3d.new(-视图间距, 板高, 0)
      左视图角点[3] = Geom::Point3d.new(-视图间距-板厚, 板高, 0)
      加入上下左右轮廓线坐标_转换坐标点(左视图角点,面方位)

      面方位="右"
      右视图角点=[]
      右视图角点[0] = Geom::Point3d.new(视图间距+板宽, 0, 0)
      右视图角点[1] = Geom::Point3d.new(视图间距+板宽+板厚, 0, 0)
      右视图角点[2] = Geom::Point3d.new(视图间距+板宽+板厚, 板高, 0)
      右视图角点[3] = Geom::Point3d.new(视图间距+板宽, 板高, 0)
      加入上下左右轮廓线坐标_转换坐标点(右视图角点,面方位)

      面方位="上"
      上视图角点=[]
      上视图角点[0] = Geom::Point3d.new(0, 视图间距+板高, 0)
      上视图角点[1] = Geom::Point3d.new(板宽, 视图间距+板高, 0)
      上视图角点[2] = Geom::Point3d.new(板宽, 视图间距+板高+板厚, 0)
      上视图角点[3] = Geom::Point3d.new(0, 视图间距+板高+板厚, 0)
      加入上下左右轮廓线坐标_转换坐标点(上视图角点,面方位)

      面方位="下"
      下视图角点=[]
      下视图角点[0] = Geom::Point3d.new(0, -视图间距-板厚, 0)
      下视图角点[1] = Geom::Point3d.new(板宽, -视图间距-板厚, 0)
      下视图角点[2] = Geom::Point3d.new(板宽, -视图间距, 0)
      下视图角点[3] = Geom::Point3d.new(0, -视图间距, 0)
      加入上下左右轮廓线坐标_转换坐标点(下视图角点,面方位)
    end

    def 加入上下左右轮廓线坐标_转换坐标点(角点s,面方位)
      h={}
      points=[]
      h["图层"]=@各个面对应图层[面方位]
      h["类型"]="上下左右轮廓线"
      h["面方位"]=面方位
      points.push(转换点坐标(角点s[0], IDENTITY,"不用转视图"))
      points.push(转换点坐标(角点s[1], IDENTITY,"不用转视图"))
      points.push(转换点坐标(角点s[2], IDENTITY,"不用转视图"))
      points.push(转换点坐标(角点s[3], IDENTITY,"不用转视图"))
      h["点s"]=points
      @模型边s.push h
    end

    def 识别顶层级别的顶底面(c,e,面方位)
      tr=c.transformation
      loops = e.loops
      loops.each {|loop|
        @需要焊接的散线s=[]
        @需要焊接的散线h={}
        if loop.outer?
          edges = loop.edges
          edges.each{|ee|
            识别边线(ee, c,面方位)
          }
        else
          # puts "Loop is not an outer loop."
          edges = loop.edges
          if  (@已经在loop中找到过的边s & edges).any?
            next
          end
          平行边,深度,平行面=找edges平行边面_找到一个就收工(edges,e)
          if 平行边==nil
            # 找不到平行边，无法识别深度，跳过
            next
          else
            @已经在loop中找到过的边s.push(平行边)
          end
          edges.each{|ee|
            # puts "识别顶层级别的顶底面 识别边线 深度 👉 #{深度}"
            # puts "面方位 👉 #{面方位}"
            识别边线(ee,c,面方位,深度,非板件外轮廓需要焊接散线=true)
          }
          # 内边界环识别完成，进行后处理
          焊接散线(c)
        end
      }
    end

    def 找edges平行边面_找到一个就收工(源edges,源面)
      平行边=nil
      平行面=nil
      深度=0

      源edges.each {|edge|
        faces = edge.faces
        faces.each {|face|
          faces = edge.faces
          # 找到对应孔槽边=false
          if face!=源面
            平行边,深度,平行面=找单个edge平行边_找到一个就收工(edge,face)
            if 平行边!=nil
              return 平行边,深度,平行面
            end
            # if 平行边!=nil
            #   return
            # end
          end
        }
      }
      return 平行边,深度,平行面
    end

    def 找单个edge平行边_找到一个就收工(源edge,搜索面)
      平行边=nil
      平行面=nil
      深度=0
      line1 = 源edge.line
      if (line1)
        源edge向量=line1[1]
      else
        # UI.messagebox "Failure"
      end

      # @selection.add(搜索面)
      # puts "找到搜索面"
      搜索面edges = 搜索面.edges
      搜索面edges.each{|搜索面edge|
        搜索面line = 搜索面edge.line
        if (搜索面line)
          搜索面edge向量=搜索面line[1]
        else
          # UI.messagebox "Failure"
        end
        if 搜索面edge向量.parallel?(源edge向量) and 搜索面edge!=源edge
            # @selection.add(源edge)
            # @selection.add(搜索面edge)
            深度 = 源edge.end.position.distance_to_line(搜索面line).to_mm.round(1)
            puts "深度:#{深度}"
            # UI.messagebox distance*25.4
            平行边=搜索面edge
            faces = 搜索面edge.faces
            faces.each {|face|
              if face!=搜索面
                平行面=face
                break
              end
            }
            return 平行边,深度,平行面
        end
      }
      return 平行边,深度,平行面
    end

    def 圆弧顺逆时针处理(c,面方位)
      centerpoint = c.center
      puts "圆心 👉 #{centerpoint}"
      normal=c.normal.normalize!
      # puts normal
      # puts "normal 👉 #{normal}"
      radius = c.radius
      # # puts radius
      xaxis = c.xaxis
      # puts "xaxis 👉 #{xaxis}"
      # # puts xaxis.normalize!
      start_angle = c.start_angle.radians
      end_angle = c.end_angle.radians
      角度差=(start_angle-end_angle).abs

      # puts "转换前 start_angle"
      # puts start_angle
      # puts end_angle
      与X正夹角=Geom::Vector3d.new(1,0,0).angle_between(c.xaxis).radians
      puts "与X正夹角 👉 #{与X正夹角}"
      _start=c.first_edge.vertices[0].position
      # 90°附件无法区分c.xaxis是在顺时针还是逆时针方向？
      # if 与X正夹角==90
      if (与X正夹角-90).abs<0.1
        if c.center.y > _start.y
          与X正夹角=-90
        end
      end
      start_angle=与X正夹角+start_angle
      end_angle = start_angle+角度差

      # dir=c.normal.z
      # if dir == 1
      #   _start=c.first_edge.vertices[0].position
      #   _end=c.last_edge.vertices[1].position
      # elsif dir == -1
      #   _start=c.last_edge.vertices[1].position
      #   _end=c.first_edge.vertices[0].position
      # end
      # puts "_start.x 👉 #{_start.x}"
      # puts "c.center.x 👉 #{c.center.x}"

      # start_angle=Geom::Vector3d.new(1,0,0).angle_between(Geom::Vector3d.new(_start.x-c.center.x,_start.y-c.center.y,_start.z-c.center.z)).radians
      # puts "重新计算与X+的夹角后 start_angle"
      # puts start_angle
      # puts "c.center.y 👉 #{c.center.y}"
      # puts "_start.y 👉 #{_start.y}"
      # start_angle = 360-start_angle if c.center.y > _start.y
      # # start_angle = 0-start_angle if c.center.y > _start.y # for - values
      # # end_angle = c.end_angle.radians


      if normal[2]==-1
        puts "谁的法向反了？👇"
        puts normal
        # vector = Geom::Vector3d.new 0,0,1
        # normal = vector.normalize!
        end_angle =start_angle
        start_angle =end_angle-角度差
        # end_angle2 =360 - start_angle
        # start_angle =0.degrees
        # end_angle =30.degrees
        # edges = entities.add_arc centerpoint, xaxis, normal, radius, start_angle2, end_angle2
      else

      end


      # puts "转换后  start_angle"
      puts start_angle
      end_angle = start_angle+角度差

      if 面方位=="底"
        puts "以y轴镜像处理,起始角度镜像后，给结束角度 👇"
        旧start_angle=start_angle
        旧end_angle=end_angle
        end_angle=90+90-旧start_angle
        start_angle=end_angle-角度差
        puts "转换后  start_angle"
        puts start_angle
        puts end_angle
      end

      # puts "转换后  start_angle"
      # puts start_angle
      # puts end_angle
      return centerpoint, radius, normal, start_angle, end_angle
    end

    def 识别边线(edge, c,面方位,深度=0,非板件外轮廓需要焊接散线=false)
      # 尽早跳过以处理的过的曲线
      curve=edge.curve
      if curve
        if @已经处理过的曲线.index(curve)
          return
        else
          # puts "curve 👉 #{curve}"
          @已经处理过的曲线 << curve
        end
      end

      tr=c.transformation
      h={}
      points=[]
      dxf图层名=@各个面对应图层[面方位]
      h["图层"]=dxf图层名
      原图层名=edge.layer.name
      # puts "原图层名 👉 #{原图层名}"
      if 原图层名=~ /临时层/ || /#{@标签关键词}/i.match(原图层名)
        # puts "转换后图层名=~ /临时层/ 👉 #{原图层名}"
        return
      end

      if 非板件外轮廓需要焊接散线
          # puts "深度 👉 #{深度}"
        if 深度==0
          深度=提取深度(原图层名)
        end
        # puts "深度 👉 #{深度}"

        if 深度==0
          # 无深度的孔跳过
          return
        end

        h["深度"]=深度
        h["颜色"]=""
        if 深度 >= (@板厚mm-@容差)
          # puts "深度 👉 #{深度}"
          h["颜色"]="蓝"
        else
          # puts "h[颜色]"
          # puts h["颜色"]
        end
        if /#{@沿线加工关键词}/i.match(原图层名)
          h["颜色"]="洋红"
        end
      else
        h["颜色"]="红"
      end

      类型=识别边线类型(edge)
      h["类型"]=类型
      # puts "类型 👉 #{类型}"
      # puts "edge 👉 #{edge}"
      # puts "edge.nil? 👉 #{edge.nil?}"
      # puts "edge.deleted?? 👉 #{edge.deleted?}"
      case 类型
      when "直线"
        points.push(转换点坐标(edge.start, tr,面方位))
        points.push(转换点坐标(edge.end, tr,面方位))
        h["点s"]=points
        if !非板件外轮廓需要焊接散线
          @模型边s.push h
        else
          @需要焊接的散线s.concat(h["点s"])
          @需要焊接的散线h=h
        end
      when "整圆"
        # puts "curve.center 👉 #{curve.center}"
        # curve.transform! tr
        # puts "curve.center 👉 #{curve.center}"
        圆心 = 转换点坐标(curve.center,tr,面方位)
        h["圆心"]=圆心
        h["半径"]=curve.radius.to_f * $stl_conv
        # puts "h[颜色] 👉 #{h["颜色"]}"
        if h["颜色"]=="蓝"
          if h["半径"]<=15
            # puts "把圆通孔的的颜色改掉 👉 "
            h["颜色"]=""
            # 如果通孔用蓝色，就算有可以钻孔刀具，云溪和云熙也都不会用钻孔的方式加工，而是用扩孔的方式
            # 但是如果不用蓝色，云熙没办法识别为通孔，会用铣槽的方式加工，所以大直径的孔还是用蓝色！
          end
        end
        添加进模型边并检查是否要复制到底面(h,c)
      when "圆弧"
        centerpoint, radius, normal, start_angle, end_angle=圆弧顺逆时针处理(curve,面方位)
        # puts "start_angle2 👉 #{start_angle2}"
        圆心 = 转换点坐标(centerpoint,tr,面方位)
        h["圆心"]=圆心
        h["半径"]=radius.to_f * $stl_conv
        # h["法向"]=normal.to_a
        h["开始角度"]=start_angle
        h["结束角度"]=end_angle
        if !非板件外轮廓需要焊接散线
          @模型边s.push h
        else
          puts "内部圆弧也要焊接"
          for j in 0...curve.vertices.length do
            点=curve.vertices[j]
            puts "点 👉 #{点}"
            points.push(转换点坐标(点, tr,面方位))
          end
          @需要焊接的散线s.concat(points)
          @需要焊接的散线h=h
        end
      when "曲线"
        #如果是曲线形式的圆，根据标记名称进行判断处理
        if !curve.is_polygon?
          # 原图层名 = "ggD12-SD15.5"
          str = 原图层名
          d_numbers = str.scan(/d(\d+(?:\.\d+)?)-sd(\d+(?:\.\d+)?)/i)
          if !d_numbers.empty?
            h["类型"]="整圆"
            puts "原图层名 👉 #{原图层名}"
            圆心 = 转换点坐标(curve.bounds.center,tr,面方位)
            h["圆心"]=圆心
            h["半径"]=d_numbers[0][0].to_f/2
            puts " #{h["圆心"]}"
            puts " #{h["半径"]}"
            添加进模型边并检查是否要复制到底面(h,c)
            return
          end
        end
        h["类型"]="多段线"
        # @导出文件.puts("  0\nPOLYLINE\n 8\n"+转换后图层名)
        # @导出文件.puts(" 66\n     1\n70\n    8\n 10\n0.0\n 20\n 0.0\n 30\n0.0")
        for j in 0...curve.vertices.length do
          点=curve.vertices[j]
          # puts "点 👉 #{点}"
          points.push(转换点坐标(点, tr,面方位))
        end
        h["点s"]=points
        if !非板件外轮廓需要焊接散线 #顶层
          @模型边s.push h
        else
          # @需要焊接的散线s.concat(h["点s"])
          # @需要焊接的散线h=h
          添加进模型边并检查是否要复制到底面(h,c) #不好判断曲线是否闭合，干脆统一不进行焊接，客户可以先行自行焊接
        end
          # # 判断曲线是否闭合
          # edges=curve.edges
          # start_point = edges[0].start.position
          # end_point = edges[-1].end.position
          # puts "start_point 👉 #{start_point}"
          # puts "end_point 👉 #{end_point}"
          # @selection.add(edges[0])
          # @selection.add(edges[-1])
          # amtf.nil
          # if start_point == end_point
          #   puts "曲线是闭合的。"
          #   h["点s"]=points
          #   @模型边s.push h
          #   return h
          # else
          #   puts "曲线不是闭合的。"
          #   @需要焊接的散线s.concat(points)
          # end
      end
    end

    def 另存skp放平_识别模型(每块板数,唯一文件夹,拟导出组s,封边信息,导出时打开的su文件,炸开圆弧=true,自动保存摆正文件=true)
      @炸开圆弧=炸开圆弧
      @自动保存摆正文件=自动保存摆正文件
      # @炸开圆弧=false
      puts "炸开圆弧 👉 #{炸开圆弧}"
      # amtf.nil
      另存skp(每块板数,唯一文件夹,拟导出组s,封边信息,导出时打开的su文件)
      检测模型单位
      # 拟导出全名hash=Hash[]
      总数=@拟导出组s.size
      #start region 异步执行？报错不好调试
        # i=0
        # pb = ProgressBar.new(总数, "放平组件读取信息 ing")
        # timer =
        #   UI.start_timer(0.000005, true) do
        #     if i == 总数
        #       UI.stop_timer(timer)
        #       Sketchup.set_status_text("识别组件 完成,还要等dxf创建完成")
        #       # 等node返回完成的信息后，打开原文件，提示打开导出文件夹
        #       收尾处理
        #     end
        #     if i < 总数
        #       begin
        #         俯视group1=@拟导出组s[i]
        #         计数=i+1

        #         @模型边s=[]
        #         puts "拟处理组件"
        #         puts 俯视group1.name
        #         原组件名=俯视group1.name
        #         # 删除参考线(俯视group1)
        #         俯视group1=板材躺平(俯视group1)
        #         # amtf.nil
        #         主名称=导出组件到单独文件时的主名称(俯视group1,每块板数)
        #         创建dxf全名=主名称+'.dxf'
        #         # 创建dxf全名=识别模型(俯视group1,每块板数)
        #         板厚mm=识别模型(俯视group1)
        #         调用node创建dxf(创建dxf全名,板厚mm,计数,总数)

        #       rescue => exception
        #         puts exception
        #       end
        #     end
        #     pb.update(i)
        #     i += 1
        #   end
      # end region

      计数=1
      @拟导出组s.each{|俯视group1|
        @模型边s=[]
        puts "拟处理组件"
        puts 俯视group1.name
        原组件名=俯视group1.name
        # 删除参考线(俯视group1)
        俯视group1=板材躺平(俯视group1)
        # amtf.nil
        主名称=导出组件到单独文件时的主名称(俯视group1,每块板数)
        创建dxf全名=主名称+'.dxf'
        # 创建dxf全名=识别模型(俯视group1,每块板数)
        板厚mm=识别模型(俯视group1)
        调用node创建dxf(创建dxf全名,板厚mm,计数,总数)
        计数 += 1
      }
      收尾处理
    end#def

    def 另存skp放平炸开重组组件_导出组件定义到单独文件(每块板数,唯一文件夹,拟导出组s,封边信息,导出时打开的su文件)
      # amtf.nil
      @导出路径=唯一文件夹
      @拟导出组s=拟导出组s
      puts "收到的封边信息  配置信息  形如 true-1_12_板数1"
      puts 封边信息
      @封边信息=封边信息
      @导出时打开的su文件=导出时打开的su文件
      # model = Sketchup.active_model
      设置常用对象
      # @当前模型全名=model.path
      文件名 = File.basename(@当前模型全名, '.*')
      另存文件名=文件名
      另存文件名=另存文件名+'_另存文件_摆正'
      @总另存全名=@导出路径+'/'+另存文件名+'.skp'
      puts "@总另存全名"
      puts @总另存全名
      status = @model.save(@总另存全名)
      if !status
        UI.messagebox "另存为 #{@总另存全名} 出错，找老鱼！"
        return
      end
      发送提示信息("另存为文件：#{@总另存全名}" )
      #👆

      设置常用对象#另存为后真的不用设置了吗？那怎么有时候把原文件给躺平了？
      层s=@model.layers
      层s.to_a.each {|e|
        if /参考|布局|刻线/.match(e.name)
          提示信息="删除图层👉#{e.name}"
          层s.remove(e,true)
        end
      }

      拟删除es=@顶层entities.to_a-@拟导出组s
      if 拟删除es.count>0
        @顶层entities.erase_entities(*拟删除es)
      end
      # amtf.nil

      # 删除以后要保存一下，不然后面会报错？
      @model.save

      拟导出全名hash=Hash[]
      i=0
      pb = ProgressBar.new(@拟导出组s.size, "导出组件定义到单独文件ing")
      timer =
        UI.start_timer(0.000005, true) do
          if i == @拟导出组s.size
            UI.stop_timer(timer)
            Sketchup.set_status_text("导出组件定义到单独文件 完成")
            # 进行导出组件定义后也要保存一下？
            # 设置常用对象
            definitions = Sketchup.active_model.definitions
            definitions.purge_unused
            @model.save
            导出dxf(拟导出全名hash)
          end
          if i < @拟导出组s.size
            begin
              俯视group1=@拟导出组s[i]
              puts "拟处理组件"
              puts 俯视group1.name
              原组件名=俯视group1.name
              # 删除参考线(俯视group1)
              俯视group1=板材躺平(俯视group1)
              导出全名=导出组件定义到单独文件(俯视group1,每块板数)
              拟导出全名hash[导出全名]=""
            rescue => exception
              puts exception
            end
          end
          pb.update(i)
          i += 1
        end

      # @拟导出组s.each {|俯视group1|
      #   puts "拟处理组件"
      #   puts 俯视group1.name
      #   原组件名=俯视group1.name
      #   # 删除参考线(俯视group1)

      #   俯视group1=板材躺平(俯视group1)
      #   # amtf.nil
      #   信息="正在导出组件 #{原组件名} 到单独su文件……"
      #   puts 信息
      #   发送提示信息(信息)
      #   导出全名=导出组件定义到单独文件(俯视group1,每块板数)
      #   拟导出全名hash[导出全名]=""
      #   i=i+1
      #   信息="处理完第 #{i} 个 共 #{@拟导出组s.count} 个"
      #   puts 信息
      #   发送提示信息(信息)
      # }

      # 发送提示信息("正在保存……")
      # # model = Sketchup.active_model
      # # 进行导出组件定义后也要保存一下？
      # @model.save
      # # amtf.nil
      # return 拟导出全名hash,@导出路径,@当前模型全名
    end#def

    def 清除未使用
      # entities = Sketchup.active_model.entities
      # if entities.count>0
      #   entities.erase_entities(*entities)
      # end
      materials = Sketchup.active_model.materials
      materials.purge_unused
      # styles = Sketchup.active_model.styles
      # styles.purge_unused
      layers = Sketchup.active_model.layers
      num_layers_removed = layers.purge_unused
      Sketchup.active_model.definitions.purge_unused
    end

    def 导出组件定义到单独文件(拟导出对象,每块板数)
      @材质名称=获取组件or最大面材质名称(拟导出对象)
      # 产品-柜名_成品名称_纹理(1=横纹)_柜号1_柜号2_数量_-1(不知道什么含义)_订单_客户_材质
      组件实例名=拟导出对象.name
      成品柜名,单元柜名,板名,订单,客户,封边s,编号=解读单个组件名(组件实例名)

      纹理=1#(1=横纹)
      柜号1=编号
      柜号2="00"
      数量=每块板数
      # 订单=去除编号sp[1]
      # 客户=去除编号sp[2]
      材质=@材质名称
      关键词位置= 材质=~ /\([\s\S]+\)/i
      if 关键词位置==nil
        puts "材质:"
        材质="默认板材(#{材质})"
        puts 材质
      end

      @开料主信息="#{成品柜名}-#{单元柜名}_#{板名}_#{纹理}_#{柜号1}_#{柜号2}_#{数量}_-1_#{订单}_#{客户}_#{材质}_封边[#{封边s}]"

      导出全名=@导出路径+'/'+@开料主信息+'.skp'

      definition = 拟导出对象.definition

      # amtf.nil
      # 发送提示信息("正在 另存组件：#{组件实例名}" )
      # 发送提示信息("正在导出组件 #{组件实例名} 到单独su文件……")
      # kk=definition.save_as(导出全名)
      kk=definition.save_copy(导出全名)
      # 发送提示信息("导出结果: #{kk}")
      return 导出全名
    end

    def 拼合组件名(收到的组件名)
      设置常用对象
      if @selection.count == 0
        entities = @model.active_entities
      else
        entities=@selection
      end
      # 收到的组件名sp=收到的组件名.split("_")
      puts "收到的组件名"
      puts 收到的组件名

      成品柜名,单元柜名,板名,订单,客户,封边s,编号=解读单个组件名(收到的组件名)
      puts "收到的组件名"
      puts 收到的组件名

      entities.each {|e|
        if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
          组件实例名=e.name

          # 不要,不要,板名,不要,不要,不要,编号=解读单个组件名(e.name)
          原有_成品柜名,原有_单元柜名,原有_板名,原有_订单,原有_客户,原有_封边s,原有_编号=解读单个组件名(组件实例名)
          puts 成品柜名
          puts 单元柜名
          puts 订单
          puts 客户
          puts 成品柜名==""
          if 成品柜名==""
            成品柜名=原有_成品柜名
          end
          if 单元柜名==""
            单元柜名=原有_单元柜名
          end
          if 单元柜名==""
            单元柜名=原有_单元柜名
          end
          板名=原有_板名
          if 订单==""
            订单=原有_订单
          end
          if 客户==""
            客户=原有_客户
          end
          if 封边s==""
            封边s=原有_封边s
          end
          编号=原有_编号

          解读后的组件名="#{成品柜名}_#{单元柜名}_#{板名}_#{订单}_#{客户}_封边[#{封边s}]"
          # 新组件名="__(\d+)."编号+解读后的组件名
          新组件名="__#{编号}. #{解读后的组件名}"
          puts 新组件名
          e.name=新组件名
        end
      }
      # @model.active_view.invalidate
      # refreshed_view = @model.active_view.refresh
      # puts "更新视图222"
    end

    def 解读组件名
      设置常用对象
      if @selection.count == 0
        entities = @model.active_entities
      else
        entities=@selection
      end
      成品柜名=""
      单元柜名=""
      板名=""
      订单=""
      客户=""
      封边s=""

      entities.each {|e|
        if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
          组件实例名=e.name
          成品柜名,单元柜名,板名,订单,客户,封边s,编号=解读单个组件名(组件实例名)
          # puts "组件实例名:"+组件实例名
          # # __88. 衣柜-层板#150
          # # kk=/__(\d+)./.match(组件实例名)
          # # 编号=kk[1]
          # 去除编号=组件实例名.sub!(/__(\d+). /, "")
          # 去除编号sp=去除编号.split("_")
          # 拆分封边sp=去除编号sp.split("_封边")
          # 柜子sp=拆分封边sp[0].split("_")

          # puts 柜子sp.count
          # puts "柜子sp[2]:"
          # puts 柜子sp[2]
          # puts 柜子sp[2]==nil
          # if 柜子sp.count==1
          #   板名=柜子sp[0]
          #   puts "板名:"+板名
          # else
          #   if 成品柜名 == ""
          #     成品柜名=柜子sp[0]
          #   end
          #   if 单元柜名 == ""
          #     单元柜名=柜子sp[1]
          #   end
          #   if 板名 == ""
          #     板名=柜子sp[2]
          #   end
          #   if 订单 == ""
          #     订单=柜子sp[3]
          #   end
          #   if 客户 == ""
          #     客户=柜子sp[4]
          #   end
            if 成品柜名 != "" and 单元柜名 != "" and 板名 != "" and 订单 != "" and 客户 != ""
              break
            end
          # end
          # if 拆分封边sp.count>1
          #   封边s=拆分封边sp[1]
          #   # puts "板名:"+板名
          # end
        end
      }
      # if 封边s==""
      #   解读后的组件名="#{成品柜名}_#{单元柜名}_#{板名}_#{订单}_#{客户}"
      # else
        解读后的组件名="#{成品柜名}_#{单元柜名}_#{板名}_#{订单}_#{客户}_封边[#{封边s}]"
      # end
      puts "解读后的组件名:"+解读后的组件名
      return 解读后的组件名
    end

    def 获取隐藏了的顶层组件(组件s)
      隐藏了的顶层组件=[]
      组件s.each{|e|
        if !e.visible?
            # puts e.name
            隐藏了的顶层组件.push e
        end
      }
      return 隐藏了的顶层组件
    end
    def 获取隐藏了的图层
      model = Sketchup.active_model
      layers = model.layers
      隐藏了的图层=[]
      layers.each{|e|
        if !e.visible?
          # puts e.name
          if !(e.name =~ /afu-门板/i)
            # puts e.name
            隐藏了的图层.push e
          end
        end
      }
      return 隐藏了的图层
    end

    def 显示不能隐藏的底层层
      # 门板层不能隐藏，不然后面会有问题
      model = Sketchup.active_model
      layers = model.layers
      layer_by_name = layers["afu-门板"]
      if layer_by_name != nil
        if !layer_by_name.visible?
          layer_by_name.visible=true
          model.save
        end
      end

    end

    def 逐个导出dxfV2(导出全名)
      puts "打开导出文件: #{导出全名}"
      # 拟导出对象=打开导出文件躺平处理(导出全名,原缩放大小)

      result = Sketchup.open_file(导出全名)
      # puts "打开导出文件result:#{result}"
      # 发送提示信息("正在 打开导出的组件：#{导出全名}" )

      result = Sketchup.send_action("viewIso:")
      result = Sketchup.send_action("viewZoomExtents:")
      entities1 = Sketchup.active_model.active_entities
      俯视group1 = entities1.add_group(entities1.to_a)

      # amtf.nil
      # model=生成视图(俯视group1)
      生成视图V2(俯视group1)
      puts "生成视图V2 完成👆"


      # 删除所有面  #这样干的话，导出后云溪不识别多个外轮廓槽

      # amtf.nil

      导出dxf全名=逐个导出dxf_主体V2()
      板厚mm=(@板厚*25.4).round(1)
      # 板厚mm=@板厚.round(1)#英寸还是毫米傻傻分不清了！
      # amtf.nil
      model = Sketchup.active_model
      return 导出dxf全名,板厚mm
      # 拟导出全名_dxfhash[导出全名]=导出dxf全名
    end

    def 逐个导出dxf_主体V2()
      model = Sketchup.active_model
      definitions = Sketchup.active_model.definitions

      definitions.purge_unused
      puts "model.save"
      puts model.save
      # model.save
      file=model.path
      路径 = File.dirname(file).freeze
      文件名 = File.basename(file, '.*')
      # today = Time.new;
      # 时间后缀= today.strftime("%Y%m%d_%H%M%S");

      # # @开料主信息=文件名
      # # @开料主信息=@开料主信息+时间后缀

      # @导出路径="#{路径}"
      # # 导出全名=@导出路径+'/'+@开料主信息+'.dxf'
      # 板厚mm=(@板厚*25.4).round(0)
      导出dxf全名="#{路径}/#{文件名}.dxf"
      puts "export 之前"
      # 发送提示信息("终于要开始导出dxf了……" )
      # options_hash = {:acad_version=> "acad_2007",
      options_hash = {:acad_version=> "acad_2000",
        :edges => true,
        :faces_flag  => false,
        :construction_geometry  => false,
        :dimensions   => false,
        :texture_maps => false,#手册上没有，应该是不起作用
        :text => false}

      begin
        status = model.export(导出dxf全名, options_hash)
      rescue Exception => e
        puts "导出错误！"
        puts e.message
        puts e.backtrace
      ensure
      end
      puts "export 之后"
      # 发送提示信息("导出执行完了……" ) #可能会直接戛然而止！
      puts status
      model = Sketchup.active_model
      definitions.purge_unused
      puts "model.save"
      puts model.save
      if !status
        # 发送提示信息("导出DXF出错了……" )
      else
        # 发送提示信息("顺利导出文件 ：#{导出dxf全名}……" )
        # 拟导出全名_dxfhash[file]=导出dxf全名
      end
      return 导出dxf全名
    end

    def 文字测试()
            # text=拟导出对象
      # puts 拟导出对象.has_leader?
      # puts text.display_leader?
      # puts text.leader_type
    end

    def 生成视图V2(c)
      c,边界h=炸开后保留需要对象_删除多余对象(c)
      # 上面只能识别正反面实体孔，还需要补充识别侧孔的
      清除未使用

      视图间距 ||= 40.mm
      # prompts = ["输入视图间距"]

      板宽=边界h["lenx"]
      板高=边界h["leny"]
      # @板厚=边界h[2]
      # puts "板宽:\t#{板宽}"
      # puts "板宽.to_mm:\t#{板宽.to_mm}"
      # puts "板高:\t#{板高}"
      # puts "板高.to_mm:\t#{板高.to_mm}"
      # puts "@板厚:\t#{@板厚}"
      # puts "@板厚.to_mm:\t#{@板厚.to_mm}"
      # 保留顶底面及组部件_删除多余边线(c)
      # amtf.nil
      # 左前上角 = Geom::Point3d.new(0,0,@板厚)
      左前上角 =ORIGIN

      trr = c.transformation
      parent = c.parent
      左视组件=parent.entities.add_instance(c.definition,trr)
      # # amtf.nil
      # # 左视组件=Sketchup.active_model.entities.add_instance(c.definition,trr)
      左视组件.make_unique
      # # point = Geom::Point3d.new(trr.origin.x, 0, 0)
      point = 左前上角
      rot = Geom::Transformation.rotation(point,Y_AXIS,90.degrees)
      左视组件.transform! rot
      # vector = Geom::Vector3d.new(视图间距+板宽+@板厚, 0, 0)
      # vector = Geom::Vector3d.new(-视图间距-板宽-@板厚, 0, 0)
      vector = Geom::Vector3d.new(-视图间距, 0, 0)
      tr  = Geom::Transformation.new(vector)
      左视组件.transform! tr

      # amtf.nil
      左视图角点=[]
      # 左视图角点[0] = Geom::Point3d.new(视图间距+板宽, 0, 0)
      # 左视图角点[1] = Geom::Point3d.new(视图间距+板宽+@板厚, 0, 0)
      # 左视图角点[2] = Geom::Point3d.new(视图间距+板宽+@板厚, 板高, 0)
      # 左视图角点[3] = Geom::Point3d.new(视图间距+板宽, 板高, 0)
      左视图角点[0] = Geom::Point3d.new(-视图间距-@板厚, 0, 0)
      左视图角点[1] = Geom::Point3d.new(-视图间距, 0, 0)
      左视图角点[2] = Geom::Point3d.new(-视图间距, 板高, 0)
      左视图角点[3] = Geom::Point3d.new(-视图间距-@板厚, 板高, 0)

      右视组件=parent.entities.add_instance(c.definition,trr)
      # # 右视组件=Sketchup.active_model.entities.add_instance(c.definition,trr)
      右视组件.make_unique
      point = Geom::Point3d.new(板宽, 0, 0)
      rot = Geom::Transformation.rotation(point,Y_AXIS,-90.degrees)
      右视组件.transform! rot
      # vector = Geom::Vector3d.new(-视图间距-板宽-@板厚, 0, 0)
      # vector = Geom::Vector3d.new(视图间距+板宽+@板厚, 0, 0)
      vector = Geom::Vector3d.new(视图间距, 0, 0)
      tr  = Geom::Transformation.new(vector)
      右视组件.transform! tr
      # amtf.nil
      右视图角点=[]
      # 右视图角点[0] = Geom::Point3d.new(-视图间距-@板厚, 0, 0)
      # 右视图角点[1] = Geom::Point3d.new(-视图间距, 0, 0)
      # 右视图角点[2] = Geom::Point3d.new(-视图间距, 板高, 0)
      # 右视图角点[3] = Geom::Point3d.new(-视图间距-@板厚, 板高, 0)
      右视图角点[0] = Geom::Point3d.new(视图间距+板宽, 0, 0)
      右视图角点[1] = Geom::Point3d.new(视图间距+板宽+@板厚, 0, 0)
      右视图角点[2] = Geom::Point3d.new(视图间距+板宽+@板厚, 板高, 0)
      右视图角点[3] = Geom::Point3d.new(视图间距+板宽, 板高, 0)

      后视组件=parent.entities.add_instance(c.definition,trr)
      # 后视组件=Sketchup.active_model.entities.add_instance(c.definition,trr)
      后视组件.make_unique
      point = Geom::Point3d.new(板宽/2, 0, -@板厚/2)
      rot = Geom::Transformation.rotation(point,Y_AXIS,180.degrees)
      后视组件.transform! rot
      vector = Geom::Vector3d.new(板宽+视图间距+@板厚+视图间距, 0, 0)
      tr  = Geom::Transformation.new(vector)
      后视组件.transform! tr

      上视组件=parent.entities.add_instance(c.definition,trr)
      # 上视组件=Sketchup.active_model.entities.add_instance(c.definition,trr)
      上视组件.make_unique
      point = Geom::Point3d.new(0, 板高, 0)
      rot = Geom::Transformation.rotation(point,X_AXIS,90.degrees)
      上视组件.transform! rot
      # vector = Geom::Vector3d.new(0, -板高-视图间距-@板厚, 0)
      # vector = Geom::Vector3d.new(0, 板高+视图间距+@板厚, 0)
      vector = Geom::Vector3d.new(0, 视图间距, 0)
      tr  = Geom::Transformation.new(vector)
      上视组件.transform! tr
      上视图角点=[]
      # 上视图角点[0] = Geom::Point3d.new(0, -视图间距-@板厚, 0)
      # 上视图角点[1] = Geom::Point3d.new(板宽, -视图间距-@板厚, 0)
      # 上视图角点[2] = Geom::Point3d.new(板宽, -视图间距, 0)
      # 上视图角点[3] = Geom::Point3d.new(0, -视图间距, 0)
      上视图角点[0] = Geom::Point3d.new(0, 视图间距+板高, 0)
      上视图角点[1] = Geom::Point3d.new(板宽, 视图间距+板高, 0)
      上视图角点[2] = Geom::Point3d.new(板宽, 视图间距+板高+@板厚, 0)
      上视图角点[3] = Geom::Point3d.new(0, 视图间距+板高+@板厚, 0)

      下视组件=parent.entities.add_instance(c.definition,trr)
      # 下视组件=Sketchup.active_model.entities.add_instance(c.definition,trr)
      下视组件.make_unique
      point =左前上角
      rot = Geom::Transformation.rotation(point,X_AXIS,-90.degrees)
      下视组件.transform! rot
      # vector = Geom::Vector3d.new(0, 板高+视图间距+@板厚, 0)
      # vector = Geom::Vector3d.new(0, -板高-视图间距-@板厚, 0)
      vector = Geom::Vector3d.new(0, -视图间距, 0)
      tr  = Geom::Transformation.new(vector)
      下视组件.transform! tr
      下视图角点=[]
      # 下视图角点[0] = Geom::Point3d.new(0, 视图间距+板高, 0)
      # 下视图角点[1] = Geom::Point3d.new(板宽, 视图间距+板高, 0)
      # 下视图角点[2] = Geom::Point3d.new(板宽, 视图间距+板高+@板厚, 0)
      # 下视图角点[3] = Geom::Point3d.new(0, 视图间距+板高+@板厚, 0)
      下视图角点[0] = Geom::Point3d.new(0, -视图间距-@板厚, 0)
      下视图角点[1] = Geom::Point3d.new(板宽, -视图间距-@板厚, 0)
      下视图角点[2] = Geom::Point3d.new(板宽, -视图间距, 0)
      下视图角点[3] = Geom::Point3d.new(0, -视图间距, 0)
      # 删除非顶面群组(c,true)

      # 如果是通圆孔，在另一面如果保留，云熙会多要求翻面加工一次孔槽，云溪会多钻一次孔
      # 如果是通槽，在另一面如果不保留，云熙会直接不加工
      # 所以需要区分 圆孔 和 槽 分别进行保留和不保留，得空再搞
      保留通槽组=true
      # 保留通槽组=false
      删除多余对象_处理图层_炸开组件(c,保留通槽组,'0',"red")
      # 删除非顶面群组(c,true)
      # 处理图层_并炸开组件(c,'0',"red")
      # amtf.nil

      # 删除非顶面群组(后视组件,true)
      # 处理图层_并炸开组件(后视组件,'1',"red")
      保留通槽组=true
      # 保留通槽组=false
      删除多余对象_处理图层_炸开组件(后视组件,保留通槽组,'1',"red")

      entities = Sketchup.active_model.entities
      faces=entities.grep(Sketchup::Face)
      # entities.to_a.each {|e|
      # amtf.nil
      faces.each {|e|
        面内部边界处理(e)
      }
      # amtf.nil

      删除多余对象_处理图层_炸开组件_侧面用(左视组件,false,'2',"white")
      添加轮廓面V2(左视图角点,'2',"white")
      # amtf.nil

      删除多余对象_处理图层_炸开组件_侧面用(右视组件,false,'3',"white")
      添加轮廓面V2(右视图角点,'3',"white")
      # amtf.nil

      删除多余对象_处理图层_炸开组件_侧面用(上视组件,false,'5',"white")
      添加轮廓面V2(上视图角点,'5',"white")

      删除多余对象_处理图层_炸开组件_侧面用(下视组件,false,'4',"white")
      添加轮廓面V2(下视图角点,'4',"white")
      # amtf.nil
    end

    def 添加轮廓面V2(pts,新图层名,颜色)
      model = Sketchup.active_model
      entities=model.entities
      face = entities.add_face(pts)
      面s=[]
      组件s=[]
      面s.push(face)
      处理图层V2(面s,组件s,新图层名,颜色)
    end
    def 添加轮廓面_删除其他边(c,一,二,三,四)
      model = Sketchup.active_model
      selection = model.selection
      entities=c.entities
      # 边界=获取边界大小(c)
      边界=获取边界大小排除部分组件(c,/_ABF_Intersect|_ABF_Label/i)
      entities.each {|e|
        if e.is_a?( Sketchup::Edge )
          e.erase!
        end
      }
      bounds=边界[3]
      pts = []
      pts[0] = bounds.corner(一)
      pts[1] = bounds.corner(二)
      pts[2] = bounds.corner(三)
      pts[3] = bounds.corner(四)
      face = entities.add_face(pts)
      # puts "face:#{face}"
      # selection.add face
      # puts "pts:#{pts}"
      # amtf.nil
    end

    def 检查板厚
      entities=Sketchup.active_model.entities
      板厚hash=Hash[]
      entities.each {|e|
        if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
          边界=获取边界大小_只识别面(e)
          板厚=边界[0]
          (0...3).each{|i|
            if 边界[i]<板厚
              板厚=边界[i]
            end
          }
          if !板厚hash.has_key?(板厚)
            板厚hash[板厚]=[]
          end
          板厚hash[板厚].push e
        end
      }
      # 板厚hash=Hash[]
      # 板厚hash[5]=[1,2,3]
      # 板厚hash[6]=[1,2,3,4,5]
      板厚数量hash=Hash[]
      板厚hash.each{|k,v|
        # puts v.count
        # puts v
        # 板厚数量hash["板厚#{k.to_mm.round(1)}"]="数量#{v.count}"
        板厚数量hash["板厚#{k.to_mm}"]="数量#{v.count}<br/>"
      }
      puts 板厚数量hash
      return 板厚数量hash
    end

    def 检查材质
      entities=Sketchup.active_model.entities
      材质hash=Hash[]
      entities.each {|e|
        if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
          材质名称=获取组件or最大面材质名称(e)
          if !材质hash.has_key?(材质名称)
            材质hash[材质名称]=[]
          end
          材质hash[材质名称].push e
        end
      }

      板厚数量hash=Hash[]
      材质hash.each{|k,v|
        # puts v.count
        # puts v
        # 板厚数量hash["板厚#{k.to_mm.round(1)}"]="数量#{v.count}"
        板厚数量hash["材质名称:#{k}"]="数量#{v.count}<br/>"
      }
      puts 板厚数量hash
      return 板厚数量hash
    end

    def 识别组件z向面高度(c)
      z向面高度h={}
      entities=组或群entities(c)
      entities.grep(Sketchup::Face){|e|
        if Z_AXIS.parallel?(e.normal)
          bb = e.bounds
          pt=bb.center
          # puts "pt.z 到底是什么单位？"  #是mm
          # puts pt.z
          # 单独打印pt.z 显示的mm,但是……下面不to_mm的话，后面转换成  标记 字符串 时 会是inch
          z=(pt.z.to_mm).round(1)
          # puts "z"
          # puts z
          if !z向面高度h.has_key?(z)
            z向面高度h[z]=[]
          end
          z向面高度h[z].push e
        end
      }
      z向面高度h2= Hash[z向面高度h.sort_by {|k,v| k}.reverse!]#高度排序
      顶面z = z向面高度h2.keys[0]
      顶面s =z向面高度h2[顶面z]

      底面z = z向面高度h2.keys[z向面高度h2.count-1]
      底面s =z向面高度h2[底面z]
      return z向面高度h2,顶面s,底面s,顶面z,底面z
    end

    def 保留顶底面和他们的边删除多余边_会保留圆弧(c,顶面s,底面s)
      entities=组或群entities(c)
      需保留的边s=[]
      顶面s.each{|e|
        需保留的边s=需保留的边s+e.edges
      }
      底面s.each{|e|
        需保留的边s=需保留的边s+e.edges
      }
      全部边s=entities.grep(Sketchup::Edge)
      多余的边s=全部边s-需保留的边s

      # 拟删除es=@顶层entities.to_a-@拟导出组s
      if 多余的边s.count>0
        提示信息= "有多余的边，正在删除"
        # puts 提示信息
        # 发送提示信息(提示信息)
        entities.erase_entities(*多余的边s)
      end
    end

    def 炸开面外部轮廓的弧线(face)
      # puts "炸开面外部轮廓的弧线"
      拟炸开弧线h={}
      loops = face.loops
      loops.each {|loop|
        if loop.outer?
          edges = loop.edges
          edges.each{|e|
            curve = e.curve
            if (curve)
              # puts "curve.typename"
              # puts curve.typename
              #就算是圆弧，也要炸开成边线，不然云熙有时候识别不了
              # if curve.typename!="ArcCurve"
                拟炸开弧线h[curve]=e
              # end
            end
          }
        end
      }
      # puts "loops循环完成"

      拟炸开弧线h.each{|k,v|
        curve =v.explode_curve
        # puts "炸开面外部轮廓的弧线 👉#{curve}"
      }
    end

    def 获取面组的边s(faces)
      边s=[]
      faces.each {|face|
          edges = face.edges
          边s=边s+edges
      }
      return 边s
    end

    def 炸开后保留需要对象_删除多余对象(c)
      # 发送提示信息("正在删除 #{c.name} 的多余对象……" )

      c.explode
      model = Sketchup.active_model
      entities=Sketchup.active_model.entities

      # 把顶底面和子组件临时成组，删除其他，破坏边界的圆弧，使其成为小线段，云熙才能识别边界……
      # 难道一开始我就是故意这么做的吗？
      # 然后如果把内部边界的圆弧给做掉了，云溪就把圆孔认成方的了！……差评
      # z向面高度h2,顶面s,底面s,@板子顶面z,@板子底面z=识别组件z向面高度(model)
      # 拟保留元素s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)
      # 拟保留元素s=拟保留元素s+顶面s
      # 拟保留元素s=拟保留元素s+底面s
      # 临时成组 = entities.add_group(拟保留元素s.to_a)
      # 临时成组.name="临时群组"
      # entities数组=entities.to_a
      # entities数组.delete(临时成组)
      # puts "entities数组"
      # puts entities数组
      # if entities数组.count>0
      #   entities.erase_entities(*entities数组)
      # end
      # 临时成组.explode


      z向面高度h2,顶面s,底面s,@板子顶面z,@板子底面z=识别组件z向面高度(model)
      # 上面只能识别正反面实体孔，还需要补充识别侧孔的
      保留顶底面和他们的边删除多余边_会保留圆弧(model,顶面s,底面s)

      拟炸开弧形面s=顶面s+底面s
      拟炸开弧形面s.each{|e|
        炸开面外部轮廓的弧线(e)
      }
      # amtf.nil

      边界h=获取边界h_只认face(model)
      @板厚=边界h["lenz"]
      @板厚mm=@板厚.to_mm.round(1)
      原有的子组件s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)

      包裹其他元素_炸开子组件_焊接边线_废(原有的子组件s)
      # amtf.nil
      临时成组 = entities.add_group(entities.to_a)
      # 下移一个板厚
      tr  = Geom::Transformation.new(Geom::Point3d.new(0,0,-@板厚))
      临时成组.transform! tr
      临时成组.name = "主视图"
      # amtf.nil
      return 临时成组,边界h
    end

    def 删除多余对象_处理图层_炸开组件_侧面用(c,保留通槽组,新图层名,颜色)
      # 发送提示信息("处理图层……")
      trr = c.transformation
      entities=c.entities
      拟处理面s=[]
      拟处组件s=[]
      拟删除对象s=[]

      原有的子组件s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)
      原有的子组件s.each {|e|
        if 是否为需保留组件(c,e,保留通槽组)
          拟处组件s.push(e)
        end
      }

      拟删除对象s=entities.to_a-拟处组件s
      # puts 拟删除对象s.count
      if 拟删除对象s.count >0
        entities.erase_entities(*拟删除对象s)
      end

      处理图层V2(拟处理面s,拟处组件s,新图层名,颜色)
      c.explode
      # amtf.nil
    end
    def 删除多余对象_处理图层_炸开组件(c,保留通槽组,新图层名,颜色)
      # 发送提示信息("处理图层……")
      trr = c.transformation
      entities=c.entities
      拟处理面s=[]
      属于顶面的边s=[]
      拟处组件s=[]
      拟删除对象s=[]
      需保留的对象s=Hash[]

      faces=entities.grep(Sketchup::Face)
      # entities.to_a.each {|e|
      faces.each {|e|
          bb = e.bounds
          pt=bb.center
          pt.transform! trr
          if pt.z > -0.2.mm
            需保留的对象s[e]=""
            拟处理面s.push(e)
            e.edges.each {|edge|
              需保留的对象s[edge]=""
            }
            属于顶面的边s=e.edges
          end
      }

      原有的子组件s=entities.grep(Sketchup::Group)+entities.grep(Sketchup::ComponentInstance)
      原有的子组件s.each {|e|
        if 定义名加实例名(e)=~/标签/i
          从外向内炸开子组件(e)
        end
        if 是否为需保留组件(c,e,保留通槽组)
          需保留的对象s[e]=""
          拟处组件s.push(e)
        end
      }

      # puts entities.to_a.count
      拟删除对象s=entities.to_a-需保留的对象s.keys
      # puts 拟删除对象s.count
      if 拟删除对象s.count >0
        entities.erase_entities(*拟删除对象s)
      end

      处理图层V2(拟处理面s,拟处组件s,新图层名,颜色)
      c.explode
      # amtf.nil
    end

    def 是否为需保留组件(c,e,保留通槽组)
      trr = c.transformation
      # entities=c.entities
      # 拟删除群组=[]

      组件实例名=e.name
      # puts 组件实例名
      组件tr = trr*e.transformation
      # puts "#{组件实例名}:组件tr.origin:#{组件tr.origin}"
      bb = Geom::BoundingBox.new
      e.entities.each {|en| bb.add(en.bounds) }
      pt=bb.center
      pt.transform! 组件tr
      # puts "组件实例名--#{组件实例名}:bounds.center:#{pt}"
      # amtf.nil
      if pt.z < -0.2.mm
        # e.erase!
        第一个子e=e.entities[0]
        层名=第一个子e.layer.name
        通槽孔关键词位置= 层名=~ /-sd-1/i #是通槽或者通孔
        整圆关键词位置= 层名=~ /zhengyuan/i #是整圆
        if 通槽孔关键词位置!=nil and 整圆关键词位置!=nil
          # 整圆不保留
          return false
        elsif 通槽孔关键词位置!=nil and 保留通槽组
          return true
        else
          # 拟删除群组.push(e)
          return false
        end
      else
        return true
      end
    end
    def 删除非顶面群组(c,保留通槽组)
      trr = c.transformation
      entities=c.entities
      拟删除群组=[]

      entities.each {|e|
        if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
          组件实例名=e.name
          # puts 组件实例名
          组件tr = trr*e.transformation
          # puts "#{组件实例名}:组件tr.origin:#{组件tr.origin}"
          bb = Geom::BoundingBox.new
          e.entities.each {|en| bb.add(en.bounds) }
          pt=bb.center
          pt.transform! 组件tr
          # puts "组件实例名--#{组件实例名}:bounds.center:#{pt}"
          # amtf.nil
          if pt.z < -1.mm
            # e.erase!
            第一个子e=e.entities[0]
            层名=第一个子e.layer.name
            关键词位置= 层名=~ /-sd-1/i #是ABF形式的通槽
            if 关键词位置!=nil and 保留通槽组
              # 不删除
            else
              拟删除群组.push(e)
            end
          end
        end
      }
      拟删除群组.each { |e|
        kk=e.erase!
      }
      # amtf.nil
    end

    def 删除非顶面边(c)
      trr = c.transformation
      entities=c.entities
      拟删除边=[]
      entities.each {|e|
        if e.is_a?( Sketchup::Edge )
          vertices = e.vertices
          vertices.each {|v|
            是顶面边线=true
            vp=v.position
            # puts "vp转换前:#{vp}"
            vp.transform! trr
            if vp.z.to_f < -3.mm
              # puts "vp.z:#{vp.z}"
              是顶面边线=false
              # e.erase!
              # selection.add(e)
              拟删除边.push(e)
              break
            end
          }
        end
      }
      拟删除边.each { |e|
        begin
          kk=e.erase!
        rescue => err
         puts err
       end
      }
      # amtf.nil
    end

    def 把组件转移到某组件(c,某组件)
      旧组件名=c.name
      # puts "某组件"
      # puts 某组件
      trr = c.transformation
      # amtf.nil

      c另一个=某组件.entities.add_instance(c.definition,trr)
      # c包裹 = 某组件.add_group(c)
      # amtf.nil
      c另一个.name=旧组件名
      c.erase!
      c=c另一个
      # amtf.nil
      return c
    end

    def 面内部边界处理(e)
      entities=Sketchup.active_model.entities
      # 生成的子组件s=[]
      loops = e.loops
        loops.each {|loop|
          if loop.outer?
            # 不能焊接，焊接后两个天工都识别不了，虽然具体报错原因不相同
            # 焊接后把面删除再导出，云熙还是可以识别的，但是云溪一样不识别  狗骨头边界
            # edges = loop.edges
            # curves = entities.weld(edges)
            # puts "外轮廓焊接后的曲线s👉#{curves}"
            # puts "Loop is an outer loop."
          else
            # puts "Loop is not an outer loop."
            edges = loop.edges
           子group = entities.add_group(edges)
           是整圆,处理后的群组=炸开子组件_焊接边线(子group)
           原图层名=e.layer.name
            puts "原图层名 ?#{原图层名}"
            puts "是整圆 ?#{是整圆}"
            puts "loop一个完成"
            # 是整圆的通孔，不要保留背面的孔群组
            if 是整圆 && 原图层名=="1"
              处理后的群组.erase!
            end
            #  entities子=子group.entities
           #  curve = edges[0].curve
           #  if (curve)
           #    puts "本身就是曲线了，不需要进一步焊接了"
           #  else
           #    puts "是独立的直线，进行焊接"
           #    curves = entities子.weld(entities子.to_a)
           #  end
          end
        }
        puts "loops循环完成"
        # amtf.nil
    end

    def 包裹其他元素_炸开子组件_焊接边线_废(拟处理群组s)
      # 处理群组里面的子组件,比如ABF槽群组，先把顶层元素临时包裹起来，防止自动合并
      entities=Sketchup.active_model.entities
      # 拟处理群组s=[]
      # 拟临时成组元素s=[]
      拟临时成组元素s=entities.to_a-拟处理群组s
      # [1,2,3]-[1,2]

      # 临时成组 = entities.add_group(拟临时成组元素s.to_a)
      临时成组 = entities.add_group(拟临时成组元素s)
      临时成组.name="临时群组"

      拟处理群组s.each {|e|
        关键词位置= 定义名加实例名(e)=~ /sideDrillDepth/i
        if 关键词位置!=nil
          组件及子元素放到指定图层(e,"临时层")
        elsif 定义名加实例名(e)!=@标签名
          炸开子组件_焊接边线(e)
        end
      }
      # amtf.nil
      临时成组.explode
    end

    def 组件炸开重设原点(c)
      旧组件名=c.name
      # 旧parent=c.parent
      model = Sketchup.active_model
      entities = model.entities
      # 在非顶层组件下去add，会出问题
      c包裹 = entities.add_group(c)
      puts c包裹
      c包裹.name="amtf"
      # c.explode
      # amtf.nil
      从外向内炸开组件(c)
      c包裹.name=旧组件名
      c=c包裹
      # amtf.nil
      # c=把组件转移到某组件(c,旧parent)
      return c
    end

    def 组件炸开_先认顶层面重设原点(组件)
      model = Sketchup.active_model
      entities = model.entities

      旧组件名=组件.name
      entities2=组件.explode
      faces=entities2.grep(Sketchup::Face)
      新组1 = entities.add_group(faces)
      子组件=[]
      entities2.each {|e|
        if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
          子组件.push e
        end
      }
      新组2 = entities.add_group(子组件)
      转移后的组件=新组1.entities.add_instance(新组2.definition,(新组1.transformation.inverse)*新组2.transformation)
      新组2.erase!
      转移后的组件.explode
      新组1.name=旧组件名
      return 新组1
    end

    def 炸开子组件_焊接边线(c)
      是整圆=false
      c=组件炸开重设原点(c)
      entities=c.entities
      model = Sketchup.active_model
      layers = model.layers

      边界h=获取边界h_只认face(c)
      厚度=边界h["lenz"].to_mm.round(1)
      # puts 定义名加实例名(c)
      # puts "厚度👇"
      # puts 厚度
      # amtf.nil
      # region 孔槽是用实体子组件表示的情况👇
        if 厚度>0.0
          # puts 定义名加实例名(c)
          # puts "厚度👇"
          # puts 厚度

          z向面高度h2,顶面s,底面s,顶面z,底面z=识别组件z向面高度(c)
          box=边界h["box"]
          顶点=box.corner(4)
          # puts 顶点
          顶点=顶点.transform(c.transformation) #转换到了全局坐标
          # puts 顶点
          顶面z=顶点.z.to_mm.round(1)

          底点=box.corner(0)
          底点=底点.transform(c.transformation) #转换到了全局坐标
          底面z=底点.z.to_mm.round(1)
          # amtf.nil

          保留顶底面和他们的边删除多余边_会保留圆弧(c,顶面s,底面s)
          # puts "@板子顶面z 👇"
          # puts @板子顶面z
          # puts 顶面z
          # puts @板子底面z
          # puts 底面z
          需改图层的es=[]
          需删除的es=[]
          if 顶面z>=@板子顶面z
            # puts "孔在正面"
            孔槽深度=@板子顶面z-底面z
            if 孔槽深度>=@板厚mm
              # 通孔
              孔槽深度=-1
            end

            顶面s.each{|e|
              需改图层的es=需改图层的es+e.edges
            }
            需改图层的es=需改图层的es+顶面s

            底面s.each{|e|
              需删除的es=需删除的es+e.edges
            }
            需删除的es=需删除的es+底面s

          else
            puts "孔在背面"
            孔槽深度=顶面z-@板子底面z

            底面s.each{|e|
              需改图层的es=需改图层的es+e.edges
            }
            需改图层的es=需改图层的es+底面s

            顶面s.each{|e|
              需删除的es=需删除的es+e.edges
            }
            需删除的es=需删除的es+顶面s
          end


          图层对象 = layers.add("kongcao-sd#{孔槽深度}")
          需改图层的es.each{|e|
            e.layer=图层对象
          }

          if 需删除的es.count>0
            entities.erase_entities(*需删除的es)
          end

        end
      #end region
        # amtf.nil

      # 删除子组件里面的面，为什么？怕 重画法向反了的圆弧 的时候出问题？
      面s=entities.grep(Sketchup::Face)
      if 面s.count>0
        提示信息= "删除所有面ing"
        # puts 提示信息
        # 发送提示信息(提示信息)
        entities.erase_entities(*面s)
      end

      # 重画法向反了的圆弧(c)

      拟焊接曲线s=[]
      entities=c.entities
      entities.grep(Sketchup::Edge) {|e|
        curve = e.curve
        line = e.line
        if (curve)
          # puts "curve.typename"
          # puts curve.typename
          if curve.typename!="ArcCurve"
            # puts "这是个曲线的组件，但不是圆弧或圆，可能本来就是个整条的曲线，后续不需要焊接处理……，"
            # puts "还是焊接一下吧……，"
            拟焊接曲线s.push e
            # return
          else
            start_angle = curve.start_angle
            end_angle = curve.end_angle
            # puts "start_angle"
            # puts start_angle.radians
            # puts end_angle.radians
            角度差=(end_angle-start_angle).abs.radians
            # puts "角度差"
            # puts 角度差
            if 角度差==360.0
              # puts "是整圆,不需要焊接"
              # 圆孔标记加上前缀
              原图层名=e.layer.name
              amtf关键词位置= 原图层名=~ /-AMTF-/i
              if amtf关键词位置!=nil
                新图层名 = 原图层名.gsub(/-AMTF-/i, "-AMTF-zhengyuan-")
              else
                新图层名="zhengyuan-#{原图层名}"
              end
              图层对象 = layers.add(新图层名)
              e.layer=图层对象
              是整圆=true
            else
              # puts "start_angle"
              # puts start_angle.radians
              # puts end_angle.radians
              # puts "角度差"
              # puts 角度差
              拟焊接曲线s.push e
            end
          end
        elsif (line)
          拟焊接曲线s.push e
        else
          # UI.messagebox "Failure"
        end
      }
      curves = entities.weld(拟焊接曲线s)
      # puts "焊接后的曲线s:"+curves.to_s
      # 发送提示信息("焊接后的曲线s:"+curves.to_s)
      # amtf.nil
      return 是整圆,c
    end

    def 重画法向反了的圆弧(c)
      # ees=c.entities
      # puts "ees.count"
      # puts ees.count
      曲线s = []
      entities=c.entities
      entities.grep(Sketchup::Edge) {|e|
        arccurve=e.curve
        if arccurve != nil
          # puts "arccurve"
          # puts arccurve.typename
          if arccurve.typename=="ArcCurve"
            if !曲线s.include?(arccurve)
              # puts "找到一个新曲线"
              曲线s.push arccurve
            end
          end
        end
      }
      # for e in ees do
      #   if e.is_a?( Sketchup::Edge )
      #     arccurve=e.curve
      #     if arccurve != nil
      #       # puts "arccurve"
      #       # puts arccurve.typename
      #       if arccurve.typename=="ArcCurve"
      #         if !曲线s.include?(arccurve)
      #           # puts "找到一个新曲线"
      #           曲线s.push arccurve
      #         end
      #       end
      #     end
      #   end
      # end

      # puts "曲线s.count"
      # puts 曲线s.count
      for e in 曲线s do
        # puts e.normal
        arccurve=e
        # puts "arccurve"
        # puts arccurve
        if arccurve==nil
          next
        end
        centerpoint = arccurve.center
        normal=arccurve.normal.normalize!
        # puts normal
        radius = arccurve.radius
        # puts radius
        xaxis = arccurve.xaxis
        # puts "xaxis"
        # puts xaxis.normalize!
        start_angle = arccurve.start_angle
        end_angle = arccurve.end_angle
        # puts "start_angle"
        # puts start_angle.radians
        # puts end_angle.radians

        if normal[2]==-1
          # puts "谁的法向反了？👇"
          # puts radius

          vector = Geom::Vector3d.new 0,0,1
          normal2 = vector.normalize!
          start_angle2 =-end_angle
          end_angle2 =start_angle
          # start_angle =0.degrees
          # end_angle =30.degrees
          原图层=e.edges[0].layer
          原材质=e.edges[0].material
          for ee in e.edges do
            ee.erase!
          end
          edges = entities.add_arc centerpoint, xaxis, normal2, radius, start_angle2, end_angle2

          for ee in edges do
            ee.layer = 原图层
            ee.material  = 原材质
          end

        end
      end # e in 曲线s do👆
    end

    def 处理图层V2(面s,组件s,新图层名,颜色)
      # c.material  = 颜色
      # entities=c.entities
      model = Sketchup.active_model
      layers = model.layers
      new_layer = layers.add(新图层名)
      # new_layer.color = 图层颜色
      # trr = c.transformation

      # amtf.nil
      面s.each {|e|
          e.layer = new_layer
          if 颜色=="white"
          else
            # Sketchup.active_model.selection.add(e)
            # UI.messagebox "e:#{e}"
            e.material  = 颜色
          end

          # 遍历面的内部边界的边线，如果原来没有指定深度sd，则设置为通孔槽sd-1👇
          loops = e.loops
          loops.each {|loop|
            edges = loop.edges
            edges.each{|edge|
              原层名=edge.layer.name
              if loop.outer?
                edge.layer = new_layer
              else
                关键词位置= 原层名=~ /-sd[-\d]+/i
                # UI.messagebox "原层名:#{原层名}"
                if 关键词位置!=nil
                  new_layer2 = layers.add("#{新图层名}-amtf-#{原层名}")
                else
                  new_layer2 = layers.add("#{新图层名}-amtf-#{原层名}-sd-1")
                end
                edge.layer = new_layer2
              end
              if 颜色=="white"
              else
                edge.material  = 颜色
              end
            }
          }
        }

      组件s.each {|e|
          组件实例名=e.name
          e.make_unique
          # if 组件实例名!="_ABF_Label"
            # new_layerbg = layers.add("bao-guo")
            # e.layer = new_layerbg
            entities2=e.entities
            entities2.each {|e2|
              原层名=e2.layer.name
              # puts "原图层名："
              # puts 原层名
              if 原层名 != "ABF_Label"

              end
              关键词位置= 原层名=~ /DSIde/i
              # p 关键词位置
              # if 关键词位置==nil
                new_layer2 = layers.add("#{新图层名}-amtf-#{原层名}")
                # puts "新图层名："
                # puts new_layer2.name
                e2.layer = new_layer2
              # end
              # amtf.nil
            }
          # end
          if 组件实例名=="_ABF_Label"
            # e.erase!
            # e.name=@开料主信息
          end
      }
    end

    def 处理图层_并炸开组件(c,新图层名,颜色)
      # c.material  = 颜色
      entities=c.entities
      model = Sketchup.active_model
      layers = model.layers
      new_layer = layers.add(新图层名)
      # new_layer.color = 图层颜色
      trr = c.transformation

      entities.each {|e|
        if e.is_a?( Sketchup::Face )
          e.layer = new_layer
          if 颜色=="white"
          else
            e.material  = 颜色
          end

          # 遍历面的内部边界的边线，如果原来没有指定深度sd，则设置为通孔槽sd-1👇
          loops = e.loops
          loops.each {|loop|
            edges = loop.edges
            edges.each{|edge|
              原层名=edge.layer.name
              if loop.outer?
                edge.layer = new_layer
              else
                关键词位置= 原层名=~ /-sd[-\d]+/i
                if 关键词位置!=nil
                  new_layer2 = layers.add("#{新图层名}-amtf-#{原层名}")
                else
                  new_layer2 = layers.add("#{新图层名}-amtf-#{原层名}-sd-1")
                end
                edge.layer = new_layer2
              end
              if 颜色=="white"
              else
                edge.material  = 颜色
              end
            }
          }

        elsif e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
          组件实例名=e.name
          e.make_unique
          # if 组件实例名!="_ABF_Label"
            # new_layerbg = layers.add("bao-guo")
            # e.layer = new_layerbg
            entities2=e.entities
            entities2.each {|e2|
              原层名=e2.layer.name
              puts "原图层名："
              puts 原层名
              if 原层名 != "ABF_Label"

              end
              关键词位置= 原层名=~ /DSIde/i
              # p 关键词位置
              # if 关键词位置==nil
                new_layer2 = layers.add("#{新图层名}-amtf-#{原层名}")
                puts "新图层名："
                puts new_layer2.name
                e2.layer = new_layer2
              # end
              # amtf.nil
            }
          # end
          if 组件实例名=="_ABF_Label"
            # e.erase!
            # e.name=@开料主信息
          end
       end
      }

      # puts "拟删除边.count: #{拟删除边.count}"
      # 拟删除边.each { |e|
      #   kk=e.erase!
      # }
      c.explode
      # amtf.nil
    end

    def 删除参考线(c)
      entities=c.entities
      entities.each {|e|
        if e.is_a?( Sketchup::ConstructionLine ) || e.is_a?( Sketchup::ConstructionPoint )
          e.erase!
        end
      }
    end

    def 获取组件or最大面材质名称(群组)
      材质=群组.material
      if 材质 !=nil
        # UI.messagebox 材质.name
        return @材质名称=材质.name
      end

      面h=面积排序(群组)
      最大面=面h.keys[0]
      材质=最大面.material
      if 材质 !=nil
        return @材质名称=材质.name
      end
      @材质名称="未指定"
      return @材质名称
    end

    def 躺平到原点导出dxf() #给aspire雕刻用
      对象=返回第一个选择对象
      if 对象.nil?
        return
      end
      @拟导出组s=[]
      @拟导出组s.push 对象
      @当前模型全名=@model.path

      @导出路径=生成唯一文件夹("导出单组件")
      # model = Sketchup.active_model
      @当前模型全名=@model.path
      文件名 = File.basename(@当前模型全名, '.*')
      # @总另存全名=@导出路径+'/'+文件名+_"导出单组件"+'.skp'
      @总另存全名="#{@导出路径}/#{文件名}_导出单组件.skp"
      status = @model.save(@总另存全名)
      发送提示信息("另存为文件：#{@总另存全名}" )
      #👆
      # model = Sketchup.active_model
      # entities = model.entities

      拟删除es=@entities.to_a-@拟导出组s
      if 拟删除es.count>0
        @entities.erase_entities(*拟删除es)
      end
      # 删除以后要保存一下，不然后面会报错？
      @model.save
      对象=板材躺平(对象)

      从外向内炸开组件(对象)
      删除所有面
      焊接所有edge
      逐个导出dxf_主体V2()
      result = Sketchup.send_action("viewIso:")
      result = Sketchup.send_action("viewZoomExtents:")
      UI.openURL(@导出路径)
    end
    def 板材躺平(拟导出对象,炸开重组=true)
      删除软化边(拟导出对象)
      # 发送提示信息("正在把板材躺平……")
      原点=Geom::Point3d.new(0, 0, 0)
      xaxis=Geom::Vector3d.new 1,0,0
      yaxis=Geom::Vector3d.new 0,1,0
      zaxis=Geom::Vector3d.new 0,0,1

      # 先把标签z轴对齐世界z轴👇遇到过板子是斜的，组件坐标系平行世界坐标系的情况
      entities=拟导出对象.definition.entities
      entities.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          组件实例名=e.name
          # puts 组件实例名
          # if 组件实例名=="_ABF_Label"###把 _ABF_Label 的原点设置到 世界坐标系原点
          # if 定义名加实例名(e)=~/标签|_ABF_Label|HT_Label/i
          if /#{@标签关键词}/i.match(定义名加实例名(e))
            @标签名=定义名加实例名(e)
            puts "找到定位用的标签了："+@标签名
            tr = 拟导出对象.transformation*e.transformation
            # puts "tr.origin1:"
            # puts tr.origin
            vector = tr.origin.vector_to ORIGIN
            tr = Geom::Transformation.translation(vector)

            tr = 拟导出对象.transformation*e.transformation
            标签zaxis=tr.zaxis
            z_anglez = Z_AXIS.angle_between 标签zaxis
            # puts "z_anglez:"
            # puts z_anglez
            # if z_anglez!=0.0
            if !是否重合(z_anglez)
              标签平行z轴 = Z_AXIS.parallel?(标签zaxis)
              if !标签平行z轴
                旋转轴 = 标签zaxis * Z_AXIS
              else
                旋转轴 = X_AXIS
              end
              旋转角度=-z_anglez
              rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
              拟导出对象.transform! rot
              # amtf.nil

              tr = 拟导出对象.transformation*e.transformation
              标签zaxis=tr.zaxis
              z_anglez2 = Z_AXIS.angle_between 标签zaxis
              puts "z_anglez2:"
              puts z_anglez2
              # if z_anglez2!=0.0
              if !是否重合(z_anglez2)
                旋转角度=2*z_anglez
                puts "旋转角度:"
                puts 旋转角度.radians
                rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
                拟导出对象.transform! rot
                # amtf.nil

                tr = 拟导出对象.transformation*e.transformation
                标签zaxis=tr.zaxis
                z_anglez3 = Z_AXIS.angle_between 标签zaxis
                puts "转动后z_anglez3:"
                puts z_anglez3
                puts z_anglez3.radians
                # if z_anglez3!=0.0
              if !是否重合(z_anglez3)
                  UI.messagebox "标签的z轴线没法转到和全局z轴平行，太难了，只能撂挑子了 ！"
                  amtf.nil
                end
              end
            end# 以上对齐z轴👆
            # amtf.nil

            tr = 拟导出对象.transformation*e.transformation
            标签xaxis=tr.xaxis
            旋转轴 = Z_AXIS
            # 原来你的夹角，是不会区分逆时针和顺时针的，不会有负值，也不会超过180°
            x_anglex = 标签xaxis.angle_between X_AXIS
            # puts "x_anglex:"
            # puts x_anglex
            # puts x_anglex.radians
            # if x_anglex!=0.0
            if !是否重合(x_anglex)
              旋转角度=-x_anglex
              rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
              拟导出对象.transform! rot

              tr = 拟导出对象.transformation*e.transformation
              标签xaxis=tr.xaxis
              x_anglex2 = 标签xaxis.angle_between X_AXIS
              # puts "转动后x_anglex2:"
              # puts x_anglex2
              # puts x_anglex2.radians
              # if x_anglex2!=0.0
              if !是否重合(x_anglex2)
                旋转角度=2*x_anglex
                puts "旋转角度:"
                puts 旋转角度.radians
                rot = Geom::Transformation.rotation(tr.origin,旋转轴,旋转角度)
                拟导出对象.transform! rot

                tr = 拟导出对象.transformation*e.transformation
                标签xaxis=tr.xaxis
                x_anglex3 = 标签xaxis.angle_between X_AXIS
                puts "转动后x_anglex3:"
                puts x_anglex3
                puts x_anglex3.radians
                # if x_anglex3!=0.0
                if !是否重合(x_anglex3)
                  UI.messagebox "标签的x轴线没法转到和全局x轴平行，太难了，只能撂挑子了 ！"
                  amtf.nil
                end
              end
            end# 以上对齐x轴👆

            # amtf.nil
            break
            puts "上方的break如果没生效，你就会看到我！"
          end
        end
      }

      # 炸开后重新成组，让组的轴平行世界坐标轴
      # 原组件名=拟导出对象.name
      # group新 = entities.add_group(拟导出对象)
      # 拟导出对象.explode

      # group新.name=原组件名
      # 拟导出对象 = group新
      # 为什么要  重设原点？子组件如果超出边界，重新设置的原点会不正确……组件炸开重设原点V2考虑了这种情况！
      #是因为有的板子原点原本不在左下角！！！，并且识别边界box得到是局部坐标，不能判断左下角是不是模型的左下角
      # 拟导出对象=组件炸开重设原点(拟导出对象)
      puts "已经摆正了"
      # amtf.nil
      # if 炸开重组
        拟导出对象=组件炸开_先认顶层面重设原点(拟导出对象)
      # end
      # 边界=获取边界h_只认face(拟导出对象)
      # 边界=获取边界大小_只识别面(拟导出对象)
      # bb=边界["box"]
      # 左下前角点=bb.corner(0)
      # 左下前角点=左下前角点.transform 拟导出对象.transformation
      # vector = 左下前角点.vector_to ORIGIN
      # tr2 = Geom::Transformation.translation(vector)
      # 拟导出对象.transform! tr2

      x = Geom::Vector3d.new(1, 0, 0)
      y = Geom::Vector3d.new(0, 1, 0)
      z = Geom::Vector3d.new(0, 0, 1)
      tr2 = Geom::Transformation.new(x,y,z,ORIGIN)

      拟导出对象.transformation = tr2
      # amtf.nil

      return 拟导出对象

      # 判断标签方向👇，上面如果已经能够成功对齐标签，下面就不需要折腾了
      entities=拟导出对象.definition.entities
      entities.each {|e|
        if ( e.is_a?( Sketchup::ComponentInstance ) || e.is_a?( Sketchup::Group ))
          组件实例名=e.name
          # puts 组件实例名
          if 定义名加实例名(e)=~/标签|_ABF_Label|HT_Label/i
          # if (组件实例名=="标签")  || (组件实例名=="_ABF_Label") ###把 _ABF_Label 的原点设置到 世界坐标系原点
            puts "找到定位用的标签了："+组件实例名
            tr = e.transformation
            tr2 = tr.inverse
            拟导出对象.transformation = tr2
            边界大小2=获取边界大小(拟导出对象)
            # amtf.nil
            x缩放=边界大小1[0]/边界大小2[0]
            puts "x缩放:#{x缩放}"
            y缩放=边界大小1[1]/边界大小2[1]
            puts "y缩放:#{y缩放}"
            z缩放=边界大小1[2]/边界大小2[2]
            puts "z缩放:#{z缩放}"
            # tr3 = Geom::Transformation.scaling(x缩放,y缩放,z缩放)

            标签xaxis=tr2.xaxis
            x_anglex = xaxis.angle_between 标签xaxis
            puts "标签xaxis 与 x轴夹角:#{x_anglex}==#{x_anglex.radians}"
            x_平行x轴 = xaxis.parallel?(标签xaxis)
            puts "x_平行x轴？:#{x_平行x轴}"
            x_angley = yaxis.angle_between 标签xaxis
            puts "标签xaxis 与 y轴夹角:#{x_angley}==#{x_angley.radians}"
            x_平行y轴 = yaxis.parallel?(标签xaxis)
            puts "x_平行y轴？:#{x_平行y轴}"
            x_anglez = zaxis.angle_between 标签xaxis
            puts "标签xaxis 与 z轴夹角:#{x_anglez}==#{x_anglez.radians}"
            x_平行z轴 = zaxis.parallel?(标签xaxis)
            puts "x_平行z轴:#{x_平行z轴}"
            # amtf.nil

            拟导出对象yaxis=tr2.yaxis
            y_angley = yaxis.angle_between 拟导出对象yaxis
            # puts "拟导出对象yaxis 与 y轴夹角:#{y_angley}==#{y_angley.radians}"
            y_平行x轴 = yaxis.parallel?(拟导出对象yaxis)
            # puts "y_平行x轴？:#{y_平行x轴}"
            y_angley = yaxis.angle_between 拟导出对象yaxis
            # puts "拟导出对象yaxis 与 y轴夹角:#{y_angley}==#{y_angley.radians}"
            y_平行y轴 = yaxis.parallel?(拟导出对象yaxis)
            # puts "y_平行y轴？:#{y_平行y轴}"
            y_anglez = zaxis.angle_between 拟导出对象yaxis
            # puts "拟导出对象yaxis 与 z轴夹角:#{y_anglez}==#{y_anglez.radians}"
            y_平行z轴 = zaxis.parallel?(拟导出对象yaxis)
            # puts "y_平行z轴:#{y_平行z轴}"
            # amtf.nil
           if x_平行x轴
            绝对x缩放=x缩放
            if y_平行y轴
              绝对y缩放=y缩放
              绝对z缩放=z缩放
            else
              绝对y缩放=z缩放
              绝对z缩放=y缩放
            end
           elsif x_平行y轴
            绝对y缩放=x缩放
            if y_平行z轴
              绝对x缩放=z缩放
              绝对z缩放=y缩放
            else
              绝对x缩放=y缩放
              绝对z缩放=z缩放
            end
           else #x_平行z轴
            绝对z缩放=x缩放
            if y_平行x轴
              绝对x缩放=y缩放
              绝对y缩放=z缩放
            else
              绝对x缩放=z缩放
              绝对y缩放=y缩放
            end
           end
            # tr3 = Geom::Transformation.scaling(1.968503937007874,1.968503937007874,1.0000000000000004)
            puts "绝对x缩放:#{绝对x缩放}"
            puts "绝对y缩放:#{绝对y缩放}"
            puts "绝对z缩放:#{绝对z缩放}"
            tr3 = Geom::Transformation.scaling(绝对x缩放,绝对y缩放,绝对z缩放)
            拟导出对象.transform! tr3
            # amtf.nil
            return 拟导出对象
            # break
          end
        end#如果是组件👆
      }
    end

    def 焊接所有edge
      entities = Sketchup.active_model.active_entities
      拟焊接曲线s=entities.grep(Sketchup::Edge).to_a
      curves = entities.weld(拟焊接曲线s)
    end
    def 是否重合(角度)
      # puts "原角度"
      # puts 角度
      # puts "弧度转角度"
      # puts 角度.radians
      角度rd=角度.round(4)
      # 角度rd=角度.round(0.01)
      # puts "圆整后角度"
      # puts 角度rd
      # amtf.nil
      if 角度rd==0.00 or 角度rd==3.14
        puts "重合！"
        return true
      end
      return false

    end

    def 获取转换信息()
      设置常用对象
      信息h=Hash[]

      选择的ents=[]
      if @selection.count == 0
        UI.messagebox "请选择一个 组件 或者 群组 ！"
        return false
      else
        # entities=@entities
        entities=@selection
        entities.each {|e|
          if e.is_a? Sketchup::ComponentInstance or e.is_a? Sketchup::Group
            组件定义名=e.definition.name
            组件实例名=e.name
            组件加实例名=组件定义名+"+"+组件实例名
            puts 组件加实例名
            id=组件加实例名
            信息h[id]=Hash[]
            # puts e.transformation.to_a.to_s
            tr=e.transformation
            str4x4 = tr.to_a.each_slice(4).inject { |str, row| "#{str}\r\n#{row}" }
            puts str4x4
            信息h[id]["transformation"]=str4x4

            point2 = tr.origin
            puts "origin: #{point2.to_s}"
            信息h[id]["原点"]=point2.to_s
            x = tr.xaxis
            puts "xaxis: #{x.to_s}"
            信息h[id]["x轴"]=x.to_s
            y = tr.yaxis
            puts "yaxis: #{y.to_s}"
            信息h[id]["y轴"]=y.to_s

            z = tr.zaxis
            puts "zaxis: #{z.to_s}"
            信息h[id]["z轴"]=z.to_s

            边界大小=获取边界大小(e)
            信息h[id]["x尺寸"]=边界大小[0].to_l
            信息h[id]["y尺寸"]=边界大小[1].to_l
            信息h[id]["z尺寸"]=边界大小[2].to_l
          end
        }
      end

      # puts 信息h.to_a.to_s
      puts 信息h.to_json
      # 信息h.each{|k,v|
      #   angle = vector2.angle_between(v).radians
      #   puts "组件id: #{k}"

      #   v.each{|kk,vv|
      #     单项="#{k}: #{vv}"
      #     angle = vector2.angle_between(v).radians
      #     puts "组件id: #{k}"

      #   }
      # }
    end

    def gp测试
      设置常用对象
      if @selection.count == 0
        UI.messagebox "请先选择一个 拟导出对象 ！"
      else
        对象=@selection[0]
      end
      entities = 对象.entities
      if (entities)
        UI.messagebox entities
      else
        UI.messagebox "Failure"
      end
    end
  end#class
end # module amtf_su
