require 'sketchup.rb'
# require File.join(__FILE__, '../AF.rb')
module AMTF
  class A匹配尺寸
    def initialize()
      设置常用对象
      if @selection.count == 0
        # UI.messagebox "请先选择一个拟匹配到空间的对象(组件)"
      else
        @拟匹配对象=@selection[0]
        # 选择点偏移=ip_p.transform(transform)
        # @拟匹配对象.transform! @编辑中tr
        # p "@model.path="+@拟匹配对象.path
      end
    end

    def 设置常用对象
      @model = Sketchup.active_model
      @entities = @model.active_entities
      @selection = @model.selection

      if @entities[0].parent.kind_of? Sketchup::Model
        @编辑中组件名称=nil
      else
        @编辑中组件=@entities[0].parent
        @编辑中组件名称=@编辑中组件.name
      end

      @编辑中tr=@model.edit_transform
      @原点 = Geom::Point3d.new(0,0,0)
      @编辑中局部原点=@原点.transform(@编辑中tr)
    end

    def onLButtonDown flags,x,y,view
      # 设置常用对象
      @ip = Sketchup::InputPoint.new
      @ip.pick view,x,y
      ip_p = @ip.position
      选择面局部法向 = @ip.face.normal
      # p "选择面局部法向="+选择面局部法向.to_s
      @选择点tr = @ip.transformation
      选择面法向 = 选择面局部法向.transform( @选择点tr )
      # p "选择面法向="+选择面法向.to_s
      vector2 = 选择面法向.normalize
      p "vector2="+vector2.to_s
      vector2.length = 1/25.4*10
      p "vector2/25.4*10="+vector2.to_s
      transform = Geom::Transformation.new(vector2)
      选择点偏移=ip_p.transform(transform)
      p "法向偏移后选择点坐标："+选择点偏移.to_s
      # line = @entities.add_line @原点,选择点偏移
      # 选择点=""
      @左面x=nil
      @右面x=nil
      @前面y=nil
      @后面y=nil
      @上面z=nil
      @下面z=nil

      @左h=Hash[]
      @右h=Hash[]
      @上h=Hash[]
      @下h=Hash[]
      @前h=Hash[]
      @后h=Hash[]

      投影测试左=@model.raytest([选择点偏移, Geom::Vector3d.new(-1, 0, 0).transform(@编辑中tr)], true)
      识别投影面(投影测试左,@左h)
      @左h['层级转换']=@层级转换
      @左h['投影面']=@击中面

      投影测试右=@model.raytest([选择点偏移, Geom::Vector3d.new(1, 0, 0).transform(@编辑中tr)], true)
      识别投影面(投影测试右)
      @右h['层级转换']=@层级转换
      @右h['投影面']=@击中面
      # @selection.add @击中面
      # 断点.nil

      投影测试上=@model.raytest([选择点偏移, Geom::Vector3d.new(0, 0, 1).transform(@编辑中tr)], true)
      识别投影面(投影测试上)
      @上h['层级转换']=@层级转换
      @上h['投影面']=@击中面

      投影测试下=@model.raytest([选择点偏移, Geom::Vector3d.new(0, 0, -1).transform(@编辑中tr)], true)
      识别投影面(投影测试下)
      @下h['层级转换']=@层级转换
      @下h['投影面']=@击中面

      投影测试前=@model.raytest([选择点偏移, Geom::Vector3d.new(0, 1, 0).transform(@编辑中tr)], true)
      识别投影面(投影测试前)
      @前h['层级转换']=@层级转换
      @前h['投影面']=@击中面
      投影测试后=@model.raytest([选择点偏移, Geom::Vector3d.new(0, -1, 0).transform(@编辑中tr)], true)

      # 投影测试ar=[投影测试左,投影测试右]
      # 判断空间坐标()
      识别选择面边界值(@左h)
      # @entities.add_cpoint (@左h['点组'][0])
      plane = @下h['投影面'].plane
      point = @左h['点组'][0]
      projected_point = point.project_to_plane(plane)
      @entities.add_cpoint projected_point

      p "右面↓"
      # 识别选择面边界值(@右h)
      # @entities.add_face(@右h['点组'])
      # @entities.add_cpoint (@右h['点组'][0])
      断点.nil

      缩放移动对象
    end

    def 缩放移动对象()
      @model.start_operation("缩放移动对象")
      ents = @model.active_entities
      # 组件名="背板"+".skp"
      # @组件全名 = File.join(AF.get_dir,组件名).freeze
      # if File.exist? @组件全名
      #   组件定义 = @model.definitions.load @组件全名
      #   b = 组件定义.bounds
      # end
      # b = @拟匹配对象.definitions.bounds
      # b = @拟匹配对象.bounds
      # 边界宽 = b.width
      # 边界高 = b.height
      # 边界深 = b.depth
      # sr = Geom::Transformation.scaling(@左下后点p,@width/边界宽,@depth/边界高,@height/边界深)
      # @拟匹配对象.transform! sr

      x = @左下后点p.vector_to @右下后点p
      # x.transform!( @编辑中tr )
      y = @左下后点p.vector_to @左下前点p
      # y.transform!( @编辑中tr )
      z = @左下后点p.vector_to @左上后点p
      # z.transform!( @编辑中tr )

      # entities = Sketchup.active_@model.active_entities
      # line = @entities.add_line  @原点, @左下后点p
      tr = Geom::Transformation.new(x,y,z,@左下后点p)
      @拟匹配对象.transformation = tr

      # c.transformation = tr

      # c = ents.add_instance 组件定义,Geom::Transformation.new
      # c.make_unique
      # c.transform! sr

      # p "kk:"+$dc_observers.to_s
      # $dc_observers.get_class_by_version(@拟匹配对象).redraw_with_undo(@拟匹配对象)
    end

    def 识别投影面(投影测试,h)
      @选择点是击中点=false
      if 投影测试 != nil
        #     投影对象 = 投影测试[1][-1]
        #     @selection.add(投影对象)
        击中点=投影测试[0]
        p "击中点全局="+击中点.to_s
        ip_p = @ip.position
        p "ip_p="+ip_p.to_s
        p "ip_p==击中点？"+(ip_p==击中点).to_s
        @选择点是击中点=ip_p==击中点
        h['是被点面']=ip_p==击中点

        @击中面 = 投影测试[1][-1]
        @层级转换 = Geom::Transformation.new
        # p "投影测试[1]:"+ 投影测试[1].to_s
        距离编辑中组件=-1
        到达编辑中组件=false
        # @层级转换= @层级转换 * 投影测试[1][-2].transformation
        # @层级转换= @层级转换 * 投影测试[1][-3].transformation
        # @层级转换= @层级转换 * 投影测试[1][-4].transformation
        # @层级转换= @层级转换 * 投影测试[1][-5].transformation
        投影测试[1].each{|e|
          if e.kind_of? Sketchup::Group or e.kind_of? Sketchup::ComponentInstance
            # p "e:"+ e.to_s
            # p "e:"+ e.name.to_s
            # p "e:"+ e.definition.name.to_s
            # if 距离编辑中组件>0
              # p "变换了这个坐标："+e.definition.name.to_s
              tr = e.transformation
              # p "e.transformation=?"+(tr==@编辑中tr).to_s
              # p "e.transformation=?"+tr.to_a.to_s
              # p "@编辑中tr=?"+@编辑中tr.to_a.to_s
              @层级转换= @层级转换 * tr
            # end
            if 到达编辑中组件
              距离编辑中组件+=1
            end
            if e.definition.name==@编辑中组件名称
              到达编辑中组件=true
            end
          end
        }
      end
    end

    def 判断空间坐标()
      if @左h['投影面'] != nil
        零点坐标转换(@左h)
        @左面x=@左h['0点'][0]
        p "@左面x:"+@左面x.to_s
      end
      if @右h['投影面'] != nil
        零点坐标转换(@右h)
        @右面x=@右h['0点'][0]
        p "@右面x:"+@右面x.to_s
      end
      if @上h['投影面'] != nil
        零点坐标转换(@上h)
        @上面z=@上h['0点'][2]
      end
      if @下h['投影面'] != nil
        零点坐标转换(@下h)
        @下面z=@下h['0点'][2]
      end
      # if @下h['投影面'] != nil
      #   点坐标转换(@下h)
      #   @下面z=@下h['0点'][0]
      # end
    end

    def 零点坐标转换(h)
      点=h['投影面'].vertices[0]
      h['0点']=点坐标转换(点,h)
    end

    def 点坐标转换(点,h)
      vtp=点.position#返回的是局部坐标！！
      p "vtp局部："+vtp.to_s
      vtp=vtp.transform( h['层级转换'] )
      p "vtp层级转换："+vtp.to_s
      return vtp
    end

    def 识别选择面边界值(h)
      最小x=nil
      最大x=nil
      最小y=nil
      最大y=nil
      最小z=nil
      最大z=nil
      # @selection.add(面)
      面=h["投影面"]
      h["点组"]=[]
      面.vertices.each{|vt|
        vtp=点坐标转换(vt,h)
        h["点组"].push vtp
        # line = @entities.add_line  @原点, vtp
        # p "编辑中局部原点:"+@编辑中局部原点.to_s
        # vtp=@编辑中局部原点.vector_to(vtp)
        # p "vtp编辑中局部："+vtp.to_s
        x值=vtp[0]
        y值=vtp[1]
        z值=vtp[2]

        最小x=vtp[0] if 最小x.nil?
        最大x=vtp[0] if 最大x.nil?
        最小y=vtp[1] if 最小y.nil?
        最大y=vtp[1] if 最大y.nil?
        最小z=vtp[2] if 最小z.nil?
        最大z=vtp[2] if 最大z.nil?

        if x值<最小x
          最小x=x值
        end
        if x值>最大x
          最大x=x值
        end
        if y值<最小y
          最小y=y值
        end
        if y值>最大y
          最大y=y值
        end
        if z值<最小z
          最小z=z值
        end
        if z值>最大z
          最大z=z值
        end
      }

      # p "@左面x:"+@左面x.to_s
      @左面x=最小x if @左面x.nil?
      @右面x=最大x if @右面x.nil?
      @前面y=最大y if @前面y.nil?
      @后面y=最小y if @后面y.nil?
      @上面z=最大z if @上面z.nil?
      @下面z=最小z if @下面z.nil?
    end
  end

end # module amtf_su
