=begin

  # Copyright 2011+, J�rg Bergmann

  # Permission to use this software for any purpose and without fee is hereby granted
  # Distribution of this software for commercial purpose is subject to:
  #  - the expressed, written consent of the author
  #  - the inclusion of the present copyright notice in all copies.

  # THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
  # IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  # WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  
  #----------------------------------------------------------------------------
  # Name        : dxf_export.rb
  # Version     : 0.0.4
  # Date        : 2011-10-28
  # Type        : Ruby Script
  # Description : Exports polygons|polylines|cirles|arcs|lines to .DXF
  # Usage       : Just try ... ;)
  #               Tools->Export to DXF 
  #----------------------------------------------------------------------------
  
  # TODO
  # tbt:2d orientation of arcs
  #   - if sth. selected: not all_connected!
  #   - remove explode_all (do it another way)
  #   - look for components and batch-export
  # tbt:3d rotated circles and arcs as lines
  #   - faces?
  
  # History
  # *** 2011-10-28 .0.0.4 ***
  #   - fixed arc orientation (finally?)
  #   - model won't be exploded anymore
  #   - 3d rotated circles & arcs export to lines
  
  # *** 2011-10-24 v0.0.3 ***
  #   - fixed arc orientation (again)
  #   - removed nomal-'direction' for arcs and circles
  #   - '3d' circles and arcs to lines
  
  # *** 2011-10-22 v0.0.2 ***
  #   - fixed arc orientation
  #   - improved @objects assignment ( faster)
  #   - corrected calculations ( x/2 returned int and not float as needed   )
  #   - automatic unit detection (to be tested   )

=end

## apply component transformation to each entity
# tr = ent.transformation
# ent.definition.entities.transform_entities(tr, ent.definition.entities.to_a)

require 'sketchup.rb'

module Dxf_export

  # global variables
  @save_name = ""
  @processed = Array.new() # Collector for all @processed edges
  @objects = Array.new() # shapes of connected lines (edges only)
        
  @rad_to_deg=180/Math::PI
  @undo=false
	
  def Dxf_export.main(debug=nil)
  
    puts("\n--- DXF_export.rb debug active ----------------------------------") if debug
    
    # reset global variables
    @save_name = ""
    @processed.clear() # Collector for all @processed edges
    @objects.clear() # shapes of connected lines (edges only)
    
    # get the active model
    model = Sketchup.active_model
   
    shapes = Array.new # @objects with the edges assigned to polygons|polylines|cirles|arcs|lines
    
    # define the filename
    model_name = "Untitled" if (model_name = File.basename(model.path, ".skp")) == ""
    
    # get selected/all entities of the active model
    if model.selection.empty?
      return nil if UI.messagebox("Nothing selected!\nContinue with all entities?", MB_YESNO) == 7
      ents = model.entities
    else
      ents = model.selection
    end
    
    if ents.length == 0
      UI.messagebox("Drawing is empty!\nNothing to do!")
      return nil
    end
      
    if Sketchup.version[0,1].to_i >= 7
      model.start_operation("DXF_export", true)
    else
      model.start_operation "DXF_export"
    end
   
    Sketchup.status_text="Export started ..."
    
    collect_objects(ents)
    
    Sketchup.status_text="Parsing objects ..."
    
    # parse all edges in each 'object'
    @objects.each { |obj|
    
      lines = Array.new
      polylines = Array.new
      polygons = Array.new
      arcs = Array.new
      circles = Array.new
    
      obj.each { |ent|
        if ent.curve # is curve?
          if ent.curve.typename != "ArcCurve" # is no arc? -> is polyline or line!
            polylines << ent.curve if not polylines.index(ent.curve)
          elsif ent.curve.is_polygon? # is polygon?
            polygons << ent.curve if not polygons.index(ent.curve)
          elsif ent.curve.normal.z != 1 && ent.curve.normal.z != -1 # circle & arc to polyline if not 2d
            polylines << ent.curve if not polylines.index(ent.curve)
          else
            if ent.curve.end_angle==Math::PI*2
              circles << ent.curve if not circles.index(ent.curve)
            else
              arcs << ent.curve if not arcs.index(ent.curve)
            end
          end
        else
            lines << (ent)
        end
      }
      shapes.push([polylines, polygons, circles, arcs, lines])
    }

    UI.messagebox("#{shapes.length} shapes found")
    
    Sketchup.set_status_text()
    
    @save_name = UI.savepanel( "Save DXF file ...", File.dirname(model.path) , "#{model_name}.dxf" )

    save(shapes)
    
    model.commit_operation
    Sketchup.undo if @undo

    model.selection.clear()

    Sketchup.set_status_text("#{@save_name} saved ...") 

  end
  
  def Dxf_export.get_unit_factor
    # detect units
    str_unit = ""
    str_formatedLength = Sketchup.format_length(1)
    str_units=["\"", "'", "mm", "cm", "m"].each { |u|
      if not str_formatedLength.index(u) == nil
        str_unit = u
        break u
      end
    }
    
    case str_unit
      when "\""
        return 1
      when "'"
        return 1.0/12.0
      when "mm"
        return 25.4
      when "cm"
        return 2.54
      when "m"
        return 0.254
      else
        return -1
    end
  end
  
  def Dxf_export.collect_objects(ents=nil)
    ents.each { |ent| 
      if not @processed.index(ent) && !ent.deleted?
        if ent.is_a? Sketchup::Group
          # apply group transformation to each entity
          # will be undone at the end of this script
          tr = ent.transformation
          ent.entities.transform_entities(tr, ent.entities.to_a)
          @undo = true
          collect_objects(ent.entities)
        elsif ent.is_a? Sketchup::ComponentInstance
          # apply component transformation to each entity
          # will be undone at the end of this script
          tr = ent.transformation
          ent.definition.entities.transform_entities(tr, ent.definition.entities.to_a)
          @undo = true
          collect_objects(ent.definition.entities)
        else
          tmp = Array.new
          if ent.typename == "Edge"
            ent.all_connected.each { |e|
              if e.typename == "Edge" # 'all_connected' returns faces too
                tmp << e
                @processed << e
              end
            }
          end
          @objects << tmp if tmp.length > 0
        end
      end
    }
  end
  
  def Dxf_export.save(shapes)
  
  	# get the unit-factor
    unit_factor=get_unit_factor()
    if unit_factor == -1
      return nil if UI.messagebox("Unable to detect the actual unit setting ...\nGenerate Output in inch?", MB_YESNO) == 7
    end
   
    out_file = File.new( @save_name , "w" )
    
    out_file.puts("0\nSECTION\n2\nENTITIES\n")
  
    id = 0
    shapes.each { |shape| 
      shape[0].each { |p| # polyline (currently polylines output to lines)
        p.each_edge { |l|
          out_file.puts("0\nLINE\n8\n#{id}\n10\n#{(l.start.position.x*unit_factor).to_f12}\n20\n#{(l.start.position.y*unit_factor).to_f12}\n30\n#{(l.start.position.z*unit_factor).to_f12}\n11\n#{(l.end.position.x*unit_factor).to_f12}\n21\n#{(l.end.position.y*unit_factor).to_f12}\n31\n#{(l.end.position.z*unit_factor).to_f12}\n")
        }
      }
      shape[1].each { |p| # polygone (currently polygone output to lines)
        id+=1
        p.each_edge { |l|
          out_file.puts("0\nLINE\n8\n#{id}\n10\n#{(l.start.position.x*unit_factor).to_f12}\n20\n#{(l.start.position.y*unit_factor).to_f12}\n30\n#{(l.start.position.z*unit_factor).to_f12}\n11\n#{(l.end.position.x*unit_factor).to_f12}\n21\n#{(l.end.position.y*unit_factor).to_f12}\n31\n#{(l.end.position.z*unit_factor).to_f12}\n")
        }
      }
      shape[2].each { |c| # circle
        out_file.puts("0\nCIRCLE\n8\n#{id}\n10\n#{(c.center.x*unit_factor).to_f12}\n20\n#{(c.center.y*unit_factor).to_f12}\n30\n#{(c.center.z*unit_factor).to_f12}\n40\n#{(c.radius*unit_factor).to_f12}\n")
      }
      shape[3].each { |a| # arc
      
        # find orientation of arc
        dir=a.normal.z	
        
        if dir == 1
          _start=a.first_edge.vertices[0].position
          _end=a.last_edge.vertices[1].position
        elsif dir == -1
          _start=a.last_edge.vertices[1].position
          _end=a.first_edge.vertices[0].position
        end
        
        start_angle=@rad_to_deg*Geom::Vector3d.new(1,0,0).angle_between(Geom::Vector3d.new(_start.x-a.center.x,_start.y-a.center.y,_start.z-a.center.z))
        start_angle = 360-start_angle if a.center.y > _start.y
        # start_angle = 0-start_angle if a.center.y > _start.y # for - values
        end_angle = @rad_to_deg*a.end_angle
        
        out_file.puts("0\nARC\n8\n#{id}\n10\n#{(a.center.x*unit_factor).to_f12}\n20\n#{(a.center.y*unit_factor).to_f12}\n30\n#{(a.center.z*unit_factor).to_f12}\n40\n#{(a.radius*unit_factor).to_f12}\n50\n#{start_angle.to_f12}\n51\n#{(start_angle+end_angle).to_f12}\n")
      }
      shape[4].each { |l| # line
        out_file.puts("0\nLINE\n8\n#{id}\n10\n#{(l.start.position.x*unit_factor).to_f12}\n20\n#{(l.start.position.y*unit_factor).to_f12}\n30\n#{(l.start.position.z*unit_factor).to_f12}\n11\n#{(l.end.position.x*unit_factor).to_f12}\n21\n#{(l.end.position.y*unit_factor).to_f12}\n31\n#{(l.end.position.z*unit_factor).to_f12}\n")
      }
      id += 1
    }
    
    out_file.puts("0\nENDSEC\n0\nEOF\n")
    out_file.close      
  end
  
  def Dxf_export.reload
    load("dxf_export.rb")
    puts("'dxf_export.rb' reloaded")
  end
  
end

class Float
  def to_f12
    return format("%0.12f", self).to_f
  end
end

if( not file_loaded?("dxf_export.rb") )
   add_separator_to_menu("Tools")
   # UI.menu("Tools").add_item("reload(Export to DXF)") { Dxf_export.reload }
   UI.menu("Tools").add_item("Export to DXF") { Dxf_export.main }
end

file_loaded("dxf_export.rb")
