paths = [
  "D:/amtf/amtf_板式家具/00su/src", #你自己代码的路径,把这个文件放到su的插件目录，然后可以在自己想要的路径下放代码文件
  # Add more as needed here...
]

# C:\Users\Administrator\AppData\Roaming\SketchUp\SketchUp 2020\SketchUp\Plugins

# SKETCHUP_CONSOLE.show  # 显示ruby控制台

paths.each { |path|
  $LOAD_PATH << path
  Dir.glob("#{path}/*.rb") { |file|
    Sketchup.require(file)
  }
}
