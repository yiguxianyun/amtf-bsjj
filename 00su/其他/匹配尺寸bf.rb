require 'sketchup.rb'
# require File.join(__FILE__, '../AF.rb')
module AMTF
  class A匹配尺寸
    def initialize()
      model = Sketchup.active_model
      @编辑中组件tr=Sketchup.active_model.edit_transform
      # p "@编辑中组件tr: #{@编辑中组件tr.class} (#{@编辑中组件tr.to_s})"
      # p "model.path="+model.path
      selection = model.selection
      if selection.count == 0
        # UI.messagebox "请先选择一个拟匹配到空间的对象(组件)"
      else
        @拟匹配对象=selection[0]
        # 偏移点=ip_p.transform(transform)
        # @拟匹配对象.transform! @编辑中组件tr
        # p "model.path="+@拟匹配对象.path
      end
    end

    def 设置常用对象
      @model = Sketchup.active_model
      @entities = @model.active_entities
      @selection = @model.selection
    end

    def onLButtonDown flags,x,y,view
      设置常用对象
      model = Sketchup.active_model
      # @entities = model.active_entities
      selection = model.selection

      @ip = Sketchup::InputPoint.new
      @ip.pick view,x,y
      ip_p = @ip.position
      # instance_path = @ip.instance_path
      # p "instance_path="+instance_path.to_a.to_s
      @原点 = Geom::Point3d.new(0,0,0)
      # Sketchup.status_text = "选择点坐标："+@ip.to_s
      # p "选择点坐标："+ip_p.to_s
      # line = @entities.add_line @原点,ip_p
      # p  @ip.face.visible?
      # selection.add(@ip.face)
      选择面局部法向 = @ip.face.normal
      p "选择面局部法向="+选择面局部法向.to_s
      选择点tr = @ip.transformation
      选择面法向 = 选择面局部法向.transform( 选择点tr )
      p "选择面法向="+选择面法向.to_s
      vector2 = 选择面法向.normalize
      p "vector2="+vector2.to_s
      vector2.length = 1/25.4*10
      p "vector2/25.4*10="+vector2.to_s
      tip = @ip.vertex
      p tip
      transform = Geom::Transformation.new(vector2)
      偏移点=ip_p.transform(transform)
      p "法向偏移后选择点坐标："+偏移点.to_s
      # line = @entities.add_line @原点,偏移点
      # 选择点=""
      @右面x=nil
      @左面x=nil
      @前面y=nil
      @后面y=nil
      @上面z=nil
      @下面z=nil
      ar左=model.raytest([偏移点, Geom::Vector3d.new(-1, 0, 0).transform(@编辑中组件tr)], true)
      ar右=model.raytest([偏移点, Geom::Vector3d.new(1, 0, 0).transform(@编辑中组件tr)], true)
      ar上=model.raytest([偏移点, Geom::Vector3d.new(0, 0, 1).transform(@编辑中组件tr)], true)
      ar下=model.raytest([偏移点, Geom::Vector3d.new(0, 0, -1).transform(@编辑中组件tr)], true)
      ar前=model.raytest([偏移点, Geom::Vector3d.new(0, 1, 0).transform(@编辑中组件tr)], true)
      ar后=model.raytest([偏移点, Geom::Vector3d.new(0, -1, 0).transform(@编辑中组件tr)], true)
      if ar左 != nil and ar下 != nil
        击中点=ar左[0]#本身就是全局坐标，不需要变换
        # # line = @entities.add_line  Geom::Point3d.new(0,0,0), 击中点
        # @左面x=击中点[0]
        # p "@左面x:"+@左面x.to_s
        击中面左 = ar左[1][-1]
        击中面左pl = 击中面左.plane
        p "击中面左pl:"+击中面左pl.to_s

        击中面左nr=击中面左.normal
        p "击中面左nr:"+击中面左nr.to_s
        击中面左nr1=击中面左nr.transform(@编辑中组件tr)
        p "击中面左nr1:"+击中面左nr1.to_s
        击中面左nr2=击中面左nr.transform(tr)
        p "击中面左nr2:"+击中面左nr2.to_s

        击中面下 = ar下[1][-1]
        击中面下nr=击中面下.normal
        p "击中面下nr:"+击中面下nr.to_s
        击中面下nr1=击中面下nr.transform(@编辑中组件tr)
        p "击中面下nr1:"+击中面下nr1.to_s
        击中面下nr2=击中面下nr.transform(选择点tr)
        p "击中面下nr2:"+击中面下nr2.to_s



        相交线 = Geom.intersect_plane_plane([ar左[0],击中面左nr], [ar下[0],击中面下nr])
        line = @entities.add_line  Geom::Point3d.new(0,0,0), 相交线[0]
      end
      if ar右 != nil
        击中点=ar右[0]
        @右面x=击中点[0]
      end
      if ar上 != nil
        击中点=ar上[0]
        @上面z=击中点[2]
      end
      if ar下 != nil
        击中点=ar下[0]
        @下面z=击中点[2]
      end
      if ar前 != nil
        击中点=ar前[0]
        @前面y=击中点[1]
      end
      if ar后 != nil
        击中点=ar后[0]
        @后面y=击中点[1]
      end
      击中arar=[ar左,ar左]
      识别击中ar(ar左)
      # if @选择点是击中点
      #   选择点="ar左"
      # end
      # 识别面对角点(@ip.face,@编辑中组件tr)

      @左下后点p=Geom::Point3d.new(@左面x, @后面y, @下面z)
      p "@左下后点p="+@左下后点p.to_s

      @右下后点p=Geom::Point3d.new(@右面x, @后面y, @下面z)
      p "@右下后点p="+@右下后点p.to_s
      @width = @左下后点p.distance @右下后点p

      @左下前点p=Geom::Point3d.new(@左面x, @前面y, @下面z)
      p "@左下前点p="+@左下前点p.to_s
      @depth = @左下后点p.distance @左下前点p

      @左上后点p=Geom::Point3d.new(@左面x, @后面y, @上面z)
      p "@左上后点p="+@左上后点p.to_s
      @height = @左下后点p.distance @左上后点p

      缩放移动对象
    end

    def 缩放移动对象()
      model = Sketchup.active_model
      model.start_operation("缩放移动对象")
      ents = model.active_entities
      # 组件名="背板"+".skp"
      # @组件全名 = File.join(AF.get_dir,组件名).freeze
      # if File.exist? @组件全名
      #   组件定义 = model.definitions.load @组件全名
      #   b = 组件定义.bounds
      # end
      # b = @拟匹配对象.definitions.bounds
      # b = @拟匹配对象.bounds
      # 边界宽 = b.width
      # 边界高 = b.height
      # 边界深 = b.depth
      # sr = Geom::Transformation.scaling(@左下后点p,@width/边界宽,@depth/边界高,@height/边界深)
      # @拟匹配对象.transform! sr

      x = @左下后点p.vector_to @右下后点p
      x.transform!( @编辑中组件tr )
      y = @左下后点p.vector_to @左下前点p
      y.transform!( @编辑中组件tr )
      z = @左下后点p.vector_to @左上后点p
      z.transform!( @编辑中组件tr )

      # entities = Sketchup.active_model.active_entities
      line = @entities.add_line  Geom::Point3d.new(0,0,0), @左下后点p
      tr = Geom::Transformation.new(x,y,z,@左下后点p)
      @拟匹配对象.transformation = tr

      # c.transformation = tr

      # c = ents.add_instance 组件定义,Geom::Transformation.new
      # c.make_unique
      # c.transform! sr

      # p "kk:"+$dc_observers.to_s
      # $dc_observers.get_class_by_version(@拟匹配对象).redraw_with_undo(@拟匹配对象)
    end

    def 识别击中ar(击中ar)
      @选择点是击中点=false
      if 击中ar != nil
        击中点=击中ar[0]
        ip_p = @ip.position
        p "击中点="+击中点.to_s
        # p "ip_p="+ip_p.to_s
        p "ip_p==击中点？"+(ip_p==击中点).to_s
        @选择点是击中点=ip_p==击中点
        # if ip_p==击中点 #选中内空左面
        击中面 = 击中ar[1][-1]
        if @选择点是击中点
          识别面对角点(击中面)
        end
      end
    end

    def 识别面对角点(面)
      vt0p=面.vertices[0].position
      p "vt0p"+vt0p.to_s
      vt0p=面.vertices[0].position.transform( @编辑中组件tr )
      p "vt0p*@kktr:"+vt0p.to_s
      编辑中组件原点=Geom::Point3d.new(0,0,0)
      # p "编辑中组件原点:"+编辑中组件原点.to_s
      编辑中组件原点=编辑中组件原点.transform(@编辑中组件tr)
      # p "@编辑中组件tr:"+@编辑中组件tr.to_a.to_s
      # p "编辑中组件原点:"+编辑中组件原点.to_s

      距离原点最小值 = 编辑中组件原点.distance vt0p
      距离原点最大值 = 编辑中组件原点.distance vt0p
      最近角点=vt0p
      最远角点=vt0p
      面.vertices.each{|vt|
        vtp=vt.position
        # p "vtp转换前："+vtp.to_s
        vtp=vtp.transform( @编辑中组件tr )
          p "vtp转换后："+vtp.to_s
        距离原点vt = 编辑中组件原点.distance vtp
          p "距离原点vt:"+距离原点vt.to_s
        if 距离原点vt<距离原点最小值
          最近角点=vtp
          距离原点最小值=距离原点vt
        end

        if 距离原点vt>距离原点最大值
          最远角点=vtp
          距离原点最大值=距离原点vt
        end
      }
      p "最近角点:"+最近角点.to_s
      p "最远角点:"+最远角点.to_s
      p "@前面y.nil?:"+@前面y.nil?.to_s
      p "@左面x.nil?:"+@左面x.nil?.to_s
      # p "@左面x:"+@左面x.to_s
      @左面x=最近角点[0] if @左面x.nil?
      # p "@左面x:"+@左面x.to_s
      @右面x=最远角点[0] if @右面x.nil?
      @前面y=最远角点[1] if @前面y.nil?
      @后面y=最近角点[1] if @后面y.nil?
      @上面z=最远角点[2] if @上面z.nil?
      @下面z=最近角点[2] if @下面z.nil?
    end
  end

end # module amtf_su
