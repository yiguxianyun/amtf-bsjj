﻿using Microsoft.SqlServer.Server;
using netDxf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using netDxf.Blocks;
using netDxf.Collections;
using netDxf.Entities;
using GTE = netDxf.GTE;
using netDxf.Header;
using netDxf.Objects;
using netDxf.Tables;
using netDxf.Units;
using Attribute = netDxf.Entities.Attribute;
using FontStyle = netDxf.Tables.FontStyle;
using Image = netDxf.Entities.Image;
using Point = netDxf.Entities.Point;
using Trace = netDxf.Entities.Trace;
using Vector2 = netDxf.Vector2;
using Vector3 = netDxf.Vector3;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace amtf_dxf2
{
    public class Acjdxf
    {
        static string 有online的图层 = "";
        static DxfDocument dxf;
        static float 板厚;
        static string dxf全名;
        static string 开料主信息;
        static string 封边信息;
        static string 去掉封边的文件全名;

        public static Dictionary<string, double> 封边厚度dic = new Dictionary<string, double>()
        {
            {"左", 0 },
            {"右", 0 },
            {"上", 0 },
            {"下", 0 }
        }; public static Dictionary<string, AciColor> 颜色dic = new Dictionary<string, AciColor>()
        {
            {"洋红", AciColor.Magenta },
            {"红", AciColor.Red },
            {"蓝", AciColor.Blue },
        };

        public async Task<object> A创建dxf(string arg)
        {
            //return input;
            Console.WriteLine(arg);
            //形如👇
            //"创建dxf全名":"D:/桌面/新建文件夹/导出dxf_2023-9-17/无标题_导出49/-_面板_1_1_00_1_-1___默认板材(未指定)_封边[].dxf|18.0|1|true-1_12|1"

            JToken jsobj = (JToken)JsonConvert.DeserializeObject(arg);
            //Console.WriteLine(jsobj["name"]);
            //string 创建dxf全名 = (string)jsobj["name"];
            string 创建dxf全名 = (string)jsobj["创建dxf全名"];
            JToken 模型边s = jsobj["模型边s"];

            //string 类型 = (string)模型边s[0]["类型"];
            //var 点s = 模型边s[0]["点s"];
            ////string 点s1 = (string)点s[0][0];
            //double 点s1 = double.Parse((string)点s[0][0]);
            //double 点s2 = Math.Round(点s1, 2);

            识别传入的文件短名(创建dxf全名);
            创建dxf(模型边s);

            //return await Task.FromResult(创建dxf全名 + "A创建dxf完成！" + 类型 + 点s1 + "===" + 点s2);
            return await Task.FromResult("A创建dxf完成！");
            //return dxf全名;
        }

        static void 识别传入的文件短名(string 创建dxf全名)
        {
            string[] ss = 创建dxf全名.Split('|');
            dxf全名 = ss[0];
            板厚 = float.Parse(ss[1]);
            //string 计数 = ss[2];
            封边信息 = ss[3];
            string 文件短名 = Path.GetFileNameWithoutExtension(dxf全名);
            string 文件路径 = Path.GetDirectoryName(dxf全名);
            string[] 拆分封边sp = Regex.Split(文件短名, "_封边", RegexOptions.IgnoreCase);
            开料主信息 = 拆分封边sp[0];
            去掉封边的文件全名 = Path.Combine(文件路径, 开料主信息 + ".dxf");
            封边信息识别(文件短名);
        }


        public string A创建dxf同步版(string arg)
        {
            //return input;
            Console.WriteLine(arg);
            //形如👇
            //"创建dxf全名":"D:/桌面/新建文件夹/导出dxf_2023-9-17/无标题_导出49/-_面板_1_1_00_1_-1___默认板材(未指定)_封边[].dxf|18.0|1|true-1_12|1"

            JToken jsobj = (JToken)JsonConvert.DeserializeObject(arg);
            //Console.WriteLine(jsobj["name"]);
            //string 创建dxf全名 = (string)jsobj["name"];
            string 创建dxf全名 = (string)jsobj["创建dxf全名"];
            JToken 模型边s = jsobj["模型边s"];

            识别传入的文件短名(创建dxf全名);
            创建dxf(模型边s);
            return dxf全名;
        }

        public static void 创建dxf(JToken 模型边s)
        {
            //dxf = 创建dxf(文件全名);
            DxfDocument dxf = new DxfDocument();
            //Layer layer = new Layer("TextToNest") 
            Layer layer = dxf.Layers.Add(new Layer("TextToNest") { Color = AciColor.Cyan });

            dxf.Layers.Add(new Layer("0"));
            dxf.Layers.Add(new Layer("1"));
            dxf.Layers.Add(new Layer("2"));
            dxf.Layers.Add(new Layer("3"));
            dxf.Layers.Add(new Layer("4"));
            dxf.Layers.Add(new Layer("5"));
            //string 封边s = "0_0_0_0";

            foreach (var h in 模型边s)
            {
                //Console.WriteLine("var h in 模型边s");
                //Console.WriteLine(h);
                string 图层 = "0";
                string 面方位 = "";
                double 厚度 = 0;
                double 深度 = 0;
                AciColor 颜色 = AciColor.ByLayer;

                if (h["颜色"] != null)
                {
                    string 颜色名 = (string)h["颜色"];
                    if (颜色名 != null && 颜色名 != "")
                    {
                        AciColor 输入颜色 = 颜色dic[颜色名];
                        颜色 = 输入颜色 ?? 颜色;
                    }
                }
                if (h["图层"] != null)
                {
                    string 输入图层 = (string)h["图层"];
                    图层 = 输入图层 ?? 图层;
                }
                if (h["面方位"] != null)
                {
                    面方位 = (string)h["面方位"];
                    厚度 = 封边厚度dic[面方位];
                }
                if (h["深度"] != null)
                {
                    深度 = (double)h["深度"];
                }

                switch ((string)h["类型"])
                {
                    case "直线":
                        Vector3 首点 = 字符串转矢量点(h["点s"][0]);
                        Vector3 尾点 = 字符串转矢量点(h["点s"][1]);
                        Line line = new Line(首点, 尾点);
                        line.Color = AciColor.Red;
                        line.Layer = dxf.Layers[图层];
                        dxf.Entities.Add(line);
                        break;
                    //上下左右轮廓线👇
                    case "上下左右轮廓线":
                        Polyline2D poly = new Polyline2D();
                        poly.Vertexes.Add(字符串转polyline矢量点(h["点s"][0]));
                        poly.Vertexes.Add(字符串转polyline矢量点(h["点s"][1]));
                        poly.Vertexes.Add(字符串转polyline矢量点(h["点s"][2]));
                        poly.Vertexes.Add(字符串转polyline矢量点(h["点s"][3]));
                        poly.Thickness = 厚度;

                        //poly.Vertexes[2].Bulge = 1;
                        poly.IsClosed = true;
                        poly.Layer = dxf.Layers[图层];
                        //云熙只认直线所以要炸开下 👇
                        dxf.Entities.Add(poly.Explode());
                        break;
                    case "多段线":
                        poly = new Polyline2D();
                        foreach (var 点 in h["点s"])
                        {
                            poly.Vertexes.Add(字符串转polyline矢量点(点));
                        }
                        poly.Thickness = -深度;
                        //poly.Vertexes[2].Bulge = 1;
                        poly.IsClosed = true;
                        poly.Color = 颜色;
                        poly.Layer = dxf.Layers[图层];
                        //double z= double.Parse((string)h["点s"][0][2]);
                        //Console.WriteLine("z 👇");
                        //Console.WriteLine(z);
                        //Console.WriteLine(poly.Elevation);
                        //poly.Elevation = z;
                        //Console.WriteLine(poly.Elevation);
                        poly = 多段线法向纠正(poly);
                        dxf.Entities.Add(poly);
                        break;
                    case "整圆":
                        if (h["圆心"] == null)
                        {
                            Console.WriteLine(h);
                        }
                        Vector3 圆心 = 字符串转矢量点(h["圆心"]);
                        double 半径 = double.Parse((string)h["半径"]); 
                        dynamic e = new Circle { Center = 圆心, Radius = 半径 };
                        e.Thickness = -深度;

                        //如果不设置z，云熙加工圆形通孔的深度不对
                        Vector3 center = e.Center;
                        center.Z = -深度;
                        e.Center = center;

                        e.Color = 颜色;

                        e.Layer = dxf.Layers[图层];
                        dxf.Entities.Add(e);
                        break;
                    case "圆弧":
                        圆心 = 字符串转矢量点(h["圆心"]);
                        半径 = double.Parse((string)h["半径"]);
                        double 开始角度 = double.Parse((string)h["开始角度"]);
                        double 结束角度 = double.Parse((string)h["结束角度"]);
                        e = new Arc(圆心, 半径, 开始角度, 结束角度);
                        //circle.Thickness = -深度;
                        e.Layer = dxf.Layers[图层];
                        e.Color = 颜色;
                        dxf.Entities.Add(e);
                        break;
                    default:
                        break;
                }

            }

            TextStyle style = new TextStyle("True type font", "simkai.ttf");
            //Vector3 v = new Vector3(1, 0, 0);
            Text text = new Text
            {
                //Attribute properties
                Layer = layer,
                Value = 开料主信息,
                Height = 25.0f,
                Style = style,
                Position = Vector3.Zero
            };

            dxf.Entities.Add(text);

            dxf.DrawingVariables.AcadVer = DxfVersion.AutoCad2007;
            dxf.Save(去掉封边的文件全名);

        }

        private static Vector3 字符串转矢量点(JToken 点)
        {
            //Line line = new Line(new Vector3(0, 0, 0), new Vector3(100, 100, 0));
            double 点0 = double.Parse((string)点[0]);
            double 点1 = double.Parse((string)点[1]);
            double 点2 = double.Parse((string)点[2]);
            return new Vector3(点0, 点1, 点2);
        }

        private static void 封边信息识别(string 文件短名)
        {
            Match 封边match = Regex.Match(文件短名, @"_封边\[(\S+)\]", RegexOptions.IgnoreCase);
            string 封边s = "";
            if (封边match.Groups.Count > 0)
            {
                封边s = 封边match.Groups[1].ToString();
            }

            bool 文件短名中不包含封边值 = true;
            if (封边s != "")
            {
                文件短名中不包含封边值 = false;
            }
            string[] 封边信息sp = Regex.Split(封边信息, "_", RegexOptions.IgnoreCase);
            string[] 启用封边sp = Regex.Split(封边信息sp[0], "-", RegexOptions.IgnoreCase);
            string 启用封边 = 启用封边sp[0];
            float 四面同厚 = float.Parse(启用封边sp[1]);
            float 小于多少毫米 = float.Parse(封边信息sp[1]);

            float 封边厚度 = 0;
            if (启用封边 == "false")
            {
                //不用干啥
            }
            else
            {
                if (板厚 >= 小于多少毫米)
                {
                    if (文件短名中不包含封边值)
                    {
                        封边厚度 = 四面同厚;
                        foreach (var key in 封边厚度dic.Keys.ToList())
                        {
                            封边厚度dic[key] = 封边厚度;
                        }
                    }
                    else
                    {
                        string[] 封边厚度s = Regex.Split(封边s, "_", RegexOptions.IgnoreCase);
                        float 封边上 = float.Parse(封边厚度s[0]);
                        float 封边下 = float.Parse(封边厚度s[1]);
                        float 封边左 = float.Parse(封边厚度s[2]);
                        float 封边右 = float.Parse(封边厚度s[3]);
                        封边厚度dic["上"] = 封边上;
                        封边厚度dic["下"] = 封边下;
                        封边厚度dic["左"] = 封边左;
                        封边厚度dic["右"] = 封边右;
                    }

                }
            }
        }

        static bool 匹配其中一个(string 拟匹配字符串, string pattern)
        {
            //string pattern = @"ABF_Label|deside";
            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            //string sentence = "Who writes these notes and uses our paper?";
            Match match = rgx.Match(拟匹配字符串);
            return match.Success;
        }

        private static Polyline2DVertex 字符串转polyline矢量点(JToken 点)
        {
            //Line line = new Line(new Vector3(0, 0, 0), new Vector3(100, 100, 0));
            double 点0 = double.Parse((string)点[0]);
            double 点1 = double.Parse((string)点[1]);
            //double 点2 = double.Parse((string)点[2]);
            return new Polyline2DVertex(点0, 点1);
        }

        static Polyline2D 多段线法向纠正(Polyline2D p2d)
        {
            //2d多段线的法向可能是反的，给了厚度以后往z正方向跑，云熙会不识别，还得如下处理……
            Vector3 v3 = p2d.Normal;
            if (v3.Z == -1)
            {
                v3.Z = 1;
                p2d.Normal = v3;
                //dxf.Entities.Add(line);

                //Polyline2D reflection = (Polyline2D)p2d.Clone();
                //reflection.Color = AciColor.Red;

                // reflection matrix of a mirror plane given its normal and a point on the plane
                Matrix4 reflectionMatrix = Matrix4.Reflection(new Vector3(1, 0, 0), new Vector3(0, 0, 0));

                // for a mirror plane that passes through the origin, you can also use
                //Matrix3 reflectionMatrix = Matrix3.Reflection(mirrorNormal);
                p2d.TransformBy(reflectionMatrix);
            }
            return p2d;
        }
    }
}
