import { useAmtfStore } from 'src/stores/amtf-store';
import { storeToRefs } from 'pinia';

import { ref, reactive, onMounted, onBeforeMount, markRaw } from 'vue'
import stores from 'src/stores';
import { useQuasar } from "quasar";

export default function (ws) {
  const $q = useQuasar();

  onBeforeMount(() => {
    // console.log("我被执行了吗？");
    // window.ws = new WebSocket("ws://localhost:12112");
  })

  // window.ws = new WebSocket("ws://localhost:12112");

  // console.log(ws);
  const store = useAmtfStore();
  const { 排序 } = store;

  function 检查su连接() {
    if (store.su连接状态 == 0) {
      // alert("请开启 amtf_dxf 服务程序先！");
      $q.notify({
        message: "请开启su插件 amtf 先！",
        color: "negative",
        timeout: 1500,
        position: "center"
      });
      return false;
    }
    return true;
  }
  let 边界框尺寸 = reactive({})
  // let 边界框尺寸 = reactive({
  // 	x:0,
  // 	y:0,
  // 	z:0
  // })

  ws.onopen = function () {
    ws.send(JSON.stringify({ 'ui连接状态': 'ui已连接√' }));
  };
  ws.onmessage = function (evt) {
    // alert(evt.data)
    let kk = JSON.parse(evt.data)
    let params = null
    // console.log("收到的源数据", kk);
    switch (true) {
      case "检查板厚_处理结果" in kk:
        // store.检查板厚_处理结果 = kk.检查板厚_处理结果;
        store.$patch({
          检查板厚_处理结果: kk.检查板厚_处理结果,
        })
        break;
      case "返回结果" in kk:
        store.$patch({
          返回结果: kk.返回结果,
        })
        break;
      case "收到边界框尺寸" in kk:
        // console.log(JSON.parse(kk.收到边界框尺寸));
        let 边界框尺寸2 = JSON.parse(kk.收到边界框尺寸)
        // {边界框尺寸.x,边界框尺寸.y,边界框尺寸.z}=JSON.parse(kk.收到边界框尺寸)
        边界框尺寸.x = 边界框尺寸2.x
        边界框尺寸.y = 边界框尺寸2.y
        边界框尺寸.z = 边界框尺寸2.z
        // 边界框尺寸=reactive(边界框尺寸2)
        console.log(边界框尺寸);
        // alert(边界框尺寸);
        // alert(边界框尺寸['x']);
        break;
      case "提醒用户" in kk:
        // alert(kk.提醒用户);
        let color = "negative"
        let position = "center"
        let 提醒内容 = kk.提醒用户
        if (/^呃……/.test(提醒内容)) {
          // color="negative"
          // position="center"
        } else {
          color = "info"
        }
        $q.notify({
          message: 提醒内容,
          color,
          timeout: 3000,
          position
        });
        break;
      case 'su连接状态' in kk:
        store.su连接状态 = kk.su连接状态;
        break;
      case '主组件' in kk:
        // alert(kk)
        // console.log(kk);
        store.主组件属性 = kk.主组件;
        // console.log("store.主组件属性",store.主组件属性);
        store.父组件属性 = markRaw(kk.父组件);
        // console.log("markRaw(kk.父组件)",store.父组件属性);
        store.相关组件属性 = markRaw(kk.相关组件);
        // console.log("store.相关组件属性", store.相关组件属性);
        // console.log("store", store);
        排序();
        break;
      case 'sw边界尺寸' in kk:
        // alert(kk)
        // console.log(kk);
        store.sw边界尺寸 = kk.sw边界尺寸;
        break;
      case '修改组件属性结果' in kk:
        break;
      case '调用amtf_dxf2结果' in kk:
        params = kk.调用amtf_dxf2结果
        if (params == "清空旧值") {
          // store.调用amtf_dxf2结果 = [];
          store.$patch({
            调用amtf_dxf2结果: [],
          })
        } else {
          let 新结果 = store.调用amtf_dxf2结果
          新结果.push(params)
          // console.log("store.调用amtf_dxf2结果===",store.调用amtf_dxf2结果);
          // store.调用amtf_dxf2结果.push(kk.调用amtf_dxf2结果);
          store.$patch({
            调用amtf_dxf2结果: 新结果,
          })
          // store.$patch((state)=>{
          //   state.调用amtf_dxf2结果.push(params)
          // })
        }
        break;
      case '接收解读后的组件名' in kk:
        params = kk.接收解读后的组件名
        console.log(params);
        let 封边match = params.match(/_封边\[(\S+)\]/)
        // alert(封边match)
        let 封边s = ""
        console.log(封边match);
        if (封边match) {
          封边s = 封边match[1]
        }
        // let 拆分封边sp=params.split("_封边")
        let 柜子sp = params.split("_")
        store.生产.成品柜名 = 柜子sp[0]
        store.生产.单元柜名 = 柜子sp[1]
        store.生产.订单 = 柜子sp[3]
        store.生产.客户 = 柜子sp[4]
        if (封边s != "") {
          store.生产.封边厚度 = 封边s.split("_")
        }
        break;
      default:
        break;
    }
  };

  return { store, storeToRefs, 检查su连接, 边界框尺寸 }
}
