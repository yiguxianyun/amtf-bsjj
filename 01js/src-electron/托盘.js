import path from 'path'
import { Menu, Tray, BrowserWindow, app } from 'electron'

// if (process.env.NODE_ENV !== 'development') {
//   global.__public = require('path').join(__dirname, '/public').replace(/\\/g, '\\\\')
// }
// 创建拖盘
// var iconTray = new Tray(path.resolve(__dirname, 'icons/icon.png'));
var iconTray = new Tray(path.join(__主路径, "image/amtf-ml.png"));

// 鼠标悬停托盘提示
iconTray.setToolTip('amtf');

// 配置右键菜单
var trayMenu = Menu.buildFromTemplate([
  // {
  //     label: '设置',
  //     click: function () {
  //         console.log('setting')
  //     }
  // },

  {
    label: '恢复窗口',
    click: function () {
      // win.setFullScreen(false)
      win.show();
    }
  },
  {
    label: '退出',
    click: function () {
      if (process.platform !== 'darwin') {
        app.quit();
      }
    }
  }
]);
// 绑定右键菜单到托盘
iconTray.setContextMenu(trayMenu);
let win = BrowserWindow.getFocusedWindow();
setTimeout(function () {

  // 点击关闭按钮让应用保存在托盘
  win.on('close', (e) => {
    if (!win.isFocused()) {
      win = null;
    } else {
      // 阻止窗口的关闭事件
      e.preventDefault();
      win.hide();
      托盘闪烁();
    }
  });
})

// 任务栏图标双击托盘打开应用
iconTray.on('double-click', function () {
  win.show();
});

// 消息提示
function 托盘闪烁() {
  var count = 0;
  var timer = setInterval(function () {
    count++;
    if (count % 2 == 0) {
      iconTray.setImage(path.join(__主路径, "image/amtf-ml.png"));
    } else {
      // iconTray.setImage(path.resolve(__dirname, 'icons/amtf-透明.png'));
      iconTray.setImage(path.join(__主路径, "image/amtf-无我偈1.png"));
    }
  }, 300);

  var timer2 = setInterval(function () {
    clearInterval(timer)
    iconTray.setImage(path.join(__主路径, "image/amtf-ml.png"));

  }, 1500)
}


