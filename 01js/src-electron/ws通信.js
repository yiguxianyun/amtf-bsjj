// import WebSocket from 'ws'
import WebSocket, { WebSocketServer } from 'ws';
import { shell, ipcMain, Notification, globalShortcut, dialog } from 'electron'
import path from 'path'
import { 生成唯一文件名, 生成唯一文件夹, 选择su文件 } from './文件'
import { log } from 'console';
const ncp = require('copy-paste')

// require引用模块的时候，会自动执行模块的代码。
require('./公用');
// 文件 = require('./文件');

// const { Notification } = require("electron").remote;

// const { exec } = require('child_process')
// import edge from 'edgelectron-edge-jse'

let wss
let ws
export default {
  // constructor(mainWindow) {
  constructor() {
    // const wss = new WebSocketServer({ port: 12111 });
    // globalShortcut.register("ctrl+G", () => {
    wss = new WebSocketServer({ port: 12111 });

    globalShortcut.register("esc", () => {
      console.log("OK");
      // 调用amtf_sw("AMTF", "恢复光标", "", 调用amtf_sw回调);
      // 调用amtf_swcs("KK", 调用amtf_sw回调);
    });

    wss.on('connection', (ws) => {
      // 有客户端连接时, 打印一条日志
      // console.log(`${client} client connected`);
      // console.log(`ws client connected`);
      // 并且创建'message'监听
      // ws.on('message', (message) => {
      ws.on('message', function message(data) {
        // 直接将消息打印出来
        // console.log(data);
        data转字符串 = data.toString()
        let data转json = JSON.parse(data转字符串)
        let arg = ""

        // console.log(data转json);
        switch (true) {
          case '关于amtf' in data转json:
            // 打开网页
            // shell.openExternal('https://github.com');
            // data转json='d:data转json.txt'

            if (process.env.DEBUGGING) {
              主路径 = process.cwd()//nodejs进程的当前工作目录
              // console.log(主路径);

            } else {//发布环境
              主路径 = process.resourcesPath//资源文件夹路径路径
              // console.log(主路径);
            }
            路径 = path.join(主路径, '/其他/html', '关于amtf.html')
            shell.openPath(路径);
            // shell.openExternal('file://' + data转json);
            break;
          case '调用amtf_dxf2_创建dxf' in data转json:
            // 这里得到是字符串，并非json对象，虽然有上面的parse
            arg = data转json.调用amtf_dxf2_创建dxf
            edge.func({
              assemblyFile: path.join(__主路径, 'amtf_dxf2.dll'), // assemblyFile为dll路径
              typeName: "amtf_dxf2.Acjdxf",
              methodName: "A创建dxf",
            })(arg, function (error, result) {
              console.log('调用amtf_dxf2_创建dxf👇');
              console.log(arg);
              let arg2 = JSON.parse(arg)
              // console.log(arg2);
              // console.log(arg2["创建dxf全名"]);

              let 创建dxf全名 = arg2.创建dxf全名
              let 模型边s = arg2.模型边s
              let ss = 创建dxf全名.split('|');
              dxf全名 = ss[0];
              计数 = ss[2];
              总数 = ss[4];
              if (计数 == 1) {
                // console.log(计数);
                路径 = path.dirname(dxf全名)
                // console.log(路径);
                路径 = 路径.replace(/\//g, "\\");
                // exec('clip').stdin.end(路径)
                ncp.copy(路径, function () {
                  // complete...
                })
                let 消息 = JSON.stringify({ '调用amtf_dxf2结果': "清空旧值" })
                广播消息(消息)
              }
              console.log("amtf_dxf2传回的信息👉", result);

              let 消息 = JSON.stringify({ '调用amtf_dxf2结果': `${计数} / ${总数} 完成！ ` })
              广播消息(消息)
            });
            break;
          case '调用amtf_dxf2' in data转json:
            arg = data转json.调用amtf_dxf2
            // console.log("调用amtf_dxf2  arg", arg);
            调用amtf_dxf2(arg, function (error, result) {
              let ss = arg.split('|');
              dxf全名 = ss[0];
              计数 = ss[2];
              总数 = ss[4];
              if (计数 == 1) {
                // console.log(计数);
                路径 = path.dirname(dxf全名)
                // console.log(路径);
                路径 = 路径.replace(/\//g, "\\");
                // exec('clip').stdin.end(路径)
                ncp.copy(路径, function () {
                  // complete...
                })
                let 消息 = JSON.stringify({ '调用amtf_dxf2结果': "清空旧值" })
                广播消息(消息)
              }
              // console.log(result);

              let 消息 = JSON.stringify({ '调用amtf_dxf2结果': `${计数} / ${总数} 完成！ ` })
              广播消息(消息)
            });
            break;
          case '调用sw' in data转json:
            arg = data转json.调用sw
            调用sw(arg, 调用sw回调);
            break;
          case '调用amtf_swff' in data转json:
            arg = data转json.调用amtf_sw
            console.log(arg);
            调用amtf_sw(arg, 调用amtf_sw回调);
            break;
          case '调用amtf_sw' in data转json:
            调用amtf_sw("amtf_sw.匹配内空等等", "监控选中对象", 广播消息cs用, 调用amtf_sw回调);
            break;
          case 'sketchup' in data转json:
            arg = data转json.sketchup
            console.log(arg);
            广播消息(data转字符串)
            break;
          case '调用electron' in data转json:
            arg = data转json.调用electron
            方法名 = data转json.调用electron
            console.log(方法名);
            switch (方法名) {
              case "选择su文件":
                全名 = data转json.params
                console.log(全名);
                选择su文件(全名, ws)
                // .then(result => {
                //   // console.log(result.canceled)
                //   // console.log(result.filePaths)
                //   if (!result.canceled) {
                //     选择的文件s = result.filePaths
                //     ws.send(JSON.stringify({ sketchup: '批量导入su', 选择的文件s }));
                //   }
                //   // console.log(选择的文件s);
                //   return 选择的文件s
                // }).catch(err => {
                //   console.log(err)
                // })

                // console.log(选择的文件s);
                // ws.send(JSON.stringify({ sketchup: '批量导入su', 选择的文件s }));

                break;
              default:
                break;
            }
            break;
          case '调用nodejs' in data转json:
            方法名 = data转json.调用nodejs
            console.log(方法名);
            let 唯一文件夹
            switch (方法名) {
              case "生成唯一文件名":
                全名 = data转json.params
                console.log(全名);
                // let 唯一文件名 = 生成唯一文件名("D:/amtf/00南腾家居-项目/20230410 江山帝景雅典六期 F4-2706/6#  阳台柜.skp", "预")
                let 唯一文件名 = 生成唯一文件名(全名, "预")
                console.log("唯一文件名", 唯一文件名);
                ws.send(JSON.stringify({ sketchup: '一键排版预处理_正式执行', 唯一文件名: 唯一文件名 }));
                break;
              case "生成唯一文件夹_创建dxf用":
                全名 = data转json.params
                console.log(全名);
                唯一文件夹 = 生成唯一文件夹(全名, "导出")
                console.log("唯一文件夹", 唯一文件夹);
                ws.send(JSON.stringify({ sketchup: '创建dxf_调用nodejs后继续', 唯一文件夹: 唯一文件夹 }));
                break;
              case "生成唯一文件夹":
                全名 = data转json.params
                console.log(全名);
                唯一文件夹 = 生成唯一文件夹(全名, "导出")
                console.log("唯一文件夹", 唯一文件夹);
                ws.send(JSON.stringify({ sketchup: '导出cad_调用nodejs后继续', 唯一文件夹: 唯一文件夹 }));
                break;

              default:
                break;
            }
            // if (arg == "生成唯一文件名") {
            //   全名 = data转json.params
            //   console.log(全名);
            //   // let 唯一文件名 = 生成唯一文件名("D:/amtf/00南腾家居-项目/20230410 江山帝景雅典六期 F4-2706/6#  阳台柜.skp", "预")
            //   let 唯一文件名 = 生成唯一文件名(全名, "预")
            //   console.log("唯一文件名", 唯一文件名);
            //   ws.send(JSON.stringify({ sketchup: '一键排版预处理_正式执行', 唯一文件名: 唯一文件名 }));
            // }
            break;
          default:
            广播消息(data转字符串)
            break;
        }
      });
      ws.on('close', function close() {
        // console.log('disconnected……');
      });
    })
  }
}

function 广播消息(params) {
  wss.clients.forEach(function each(client) {
    if (client !== ws && client.readyState === WebSocket.OPEN) {
      client.send(params);
    }
  });
}
function 广播消息cs用(data, callback) {
  消息 = JSON.stringify({ 提醒用户: data })
  广播消息(消息)
  callback(null, data);
}

var edge = require("electron-edge-js");

调用amtf_dxf2 = edge.func({
  // assemblyFile: 'D:/amtf/amtf_csharp/amtf_dxf2/bin/Debug/amtf_dxf2.dll', // assemblyFile为dll路径
  assemblyFile: path.join(__主路径, 'amtf_dxf2.dll'), // assemblyFile为dll路径
});
调用sw = edge.func({
  assemblyFile: path.join(__主路径, 'sw.dll'), // assemblyFile为dll路径
});
调用amtf_swff = edge.func({
  assemblyFile: path.join(__主路径, 'netstandard2.0/amtf_sw.dll'), // assemblyFile为dll路径
});

function 调用amtf_sw(类名, 方法名, payload, 回调函数) {
  edge.func({
    assemblyFile: path.join(__主路径, 'netstandard2.0/amtf_sw.dll'), // assemblyFile为dll路径
    typeName: 类名,
    methodName: 方法名,
  })(
    payload
    , 回调函数)
}

// let subtract = edge.func({
//   assemblyFile: path.resolve("resources/dp2.dll"),
//   typeName: "dp2.ClassDp",
//   methodName: "Subtract",
// });

function 调用sw回调(error, result) {
  // new Notification({
  //   title:"您收到新的消息",
  //   body: result,
  //   icon:path.join(__主路径, "image/amtf-ml.png")
  // }).show();
  let 消息
  if (/^呃……/.test(result)) {
    // new Notification({ title: result }).show();
    消息 = JSON.stringify({ 提醒用户: result })
  } else {
    消息 = JSON.stringify({ sketchup: "导入stl", stl路径: result })
  }
  广播消息(消息)
}
function 调用amtf_sw回调(error, result) {
  // new Notification({ title: error }).show();
  消息 = JSON.stringify({ 提醒用户: result })
  广播消息(消息)
}


// var addAndMultiplyBy2 = edge.func(function () {/*
//     async (dynamic input) => {
//         var add = (Func<object, Task<object>>)input.add;
//         var twoNumbers = new { a = (int)input.a, b = (int)input.b };
//         var addResult = (int)await add(twoNumbers);
//         return addResult * 2;
//     }
// */});

// var payload = {
//     a: 2,
//     b: 3,
//     add: function (data, callback) {
//         callback(null, data.a + data.b);
//     }
// };

// addAndMultiplyBy2(payload, function (error, result) {
//     if (error) throw error;
//     console.log(result);
// });

// export default 通信
// module.exports = 通信;
