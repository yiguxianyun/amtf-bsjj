import { app, BrowserWindow, nativeTheme, ipcMain, Notification, globalShortcut } from 'electron'
import path from 'path'
// import os from 'os'
import ws通信 from './ws通信'
import windowStateKeeper from 'electron-window-state'

// process.chdir(__dirname);

// needed in case process is undefined under Linux
const platform = process.platform || os.platform()

try {
  if (platform === 'win32' && nativeTheme.shouldUseDarkColors === true) {
    require('fs').unlinkSync(path.join(app.getPath('userData'), 'DevTools Extensions'))
  }
} catch (_) { }
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
// console.log(process.env.NODE_ENV);
if (process.env.NODE_ENV !== 'development') {
  global.__public = path.join(__dirname, '../../public').replace(/\\/g, '\\\\')
} else {
  global.__public = path.resolve(__dirname, '../../public')

}
require('./公用');

let mainWindow
// let appTray = null; // 在外面创建 tray ，防止被自动删除，导致图标消失


const additionalData = { amtf: 'amtf运行中' }
const gotTheLock = app.requestSingleInstanceLock(additionalData)
if (!gotTheLock) {
  app.quit()
} else {
  app.on('second-instance', (event, commandLine, workingDirectory, additionalData) => {
    //输入从第二个实例中接收到的数据
    console.log(additionalData)
    //有人试图运行第二个实例，我们应该关注我们的窗口
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.show();
      mainWindow.focus()
    }
  })
  //创建mainWindow，加载应用的其余部分，etc...
  // app.whenReady().then(() => {
  //   mainWindow = createWindow()
  // })
}

function createWindow() {
  /**
   * Initial window options
   */
  let winstate = windowStateKeeper({
    defaultWidth: 300, defaultHeight: 600//,x:50,y:100
  })
  mainWindow = new BrowserWindow({
    icon: path.join(__主路径, "image/amtf-ml.png"), // tray icon
    // width: 300,
    // height: 400,
    width: 300,
    height: 600,
    // x: winstate.x,
    // y: winstate.y,
    x: 50,
    y: 150,
    // minHeight: 200,
    // maxHeight: 200,
    // minWidth: 100,
    menuBarVisible: false,
    useContentSize: true,
    autoHideMenuBar: true,
    // alwaysOnTop: true,
    webPreferences: {
      contextIsolation: true,
      // //渲染进程是否启用remote模块
      // More info: https://v2.quasar.dev/quasar-cli-vite/developing-electron-apps/electron-preload-script
      // preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD),

      // nodeIntegration: true,//nodeIntegration设为false页面就不能使用nodejs和Electron APIs

      //   enableRemoteModule: true,//在渲染进程中使用主进程中的模块方法时，可以使用Electron Remote解决在渲染和主进程间的通讯,这么看 remote 模块可以说是非常好用啦，渲染进程就不用显式通过 ipcRenderer / ipcMain 与主进程通信。除此之外，可以通过 remote.getGlobal 获取主进程中 的全局变量, 通过 remote.process 拿到主进程的 process 对象信息
      // preload: path.join(__dirname, 'preload.js')
    },
  })

  mainWindow.setAlwaysOnTop(true)
  // mainWindow.setAlwaysOnTop(true, "floating", 1);

  mainWindow.loadURL(process.env.APP_URL)
  // console.log(process.env.APP_URL);
  // mainWindow.loadFile(process.env.APP_URL)

  // mainWindow.setMenu(null);// 永久隐藏菜单栏

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    // mainWindow.webContents.openDevTools()
  } else {
    // we're on production; no access to devtools pls
    // mainWindow.webContents.on('devtools-opened', () => {
    //   mainWindow.webContents.closeDevTools()
    // })
  }

  winstate.manage(mainWindow)//记录窗口信息
  mainWindow.on('closed', () => {
    mainWindow = null
  })

  // // ws通信 = require('./ws通信');
  // ws通信.constructor();
  // // new ws通信();

  // 自定义底部拖盘
  require('./托盘');

}

app.whenReady().then(
  () => {
    createWindow()
    // ws通信 = require('./ws通信');
    setTimeout(function () {
      ws通信.constructor();
      // 开启http服务
      // require("./expressapp");
    }, 1000);

  })

app.on('will-quit', () => {
  // 注销快捷键
  // globalShortcut.unregister('CommandOrControl+X')

  // 注销所有快捷键
  globalShortcut.unregisterAll()
})

app.on('window-all-closed', () => {
  if (platform !== 'darwin') {

    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

// new 通信();
// ws通信.constructor(mainWindow);

ipcMain.on('setTitle', (event) => {
  const webContents = event.sender
  const win = BrowserWindow.fromWebContents(webContents)
  win.setTitle("amtfkk")
})

// async function 检查su连接() {
//   return "等下再检查" // 返回给渲染进程
// }

// ipcMain.handle('检查su连接', 检查su连接)
// ws通信 = require('./ws通信');
// ws通信.constructor();
// require('./ws通信');






