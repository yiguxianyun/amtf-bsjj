// require('./公用');
let controllers = require("./controllers/index");
let bodyParser = require("body-parser");
let express = require("express");

let bb = require("express-busboy");
require("source-map-support").install();
let db = require("./db");

// exports.init = init;
// function init() {
let app = express();
app.set("trust proxy", true);

//设置跨域访问（设置在所有的请求前面即可）
app.all("*", function (req, res, next) {
  //设置允许跨域的域名，*代表允许任意域名跨域
  res.header("Access-Control-Allow-Origin", "*");
  //允许的header类型
  res.header("Access-Control-Allow-Headers", "content-type");
  //跨域允许的请求方式
  res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS");
  if (req.method == 'OPTIONS')
    res.sendStatus(200); //让options尝试请求快速结束
  else
    next();
});


// Show error information
process.on("unhandledRejection", (reason, p) => {
  console.log("Unhandled promise error:  " + p + reason);
  console.log("stack: " + reason.stack);
});

// Allow file uploads
app.use(bodyParser.urlencoded({ extended: true }));
bb.extend(app, {
  upload: true,
});
app.use(controllers);

// Static files for the demo. Use nginx or similar for real deploys
// app.use(express.static("src-electron/public"));
app.use(express.static(__前台));
// app.use("/examples", express.static("src-electron/examples"));
// app.use("/examples", express.static("examples文件夹名称"));

// 500 Error
app.use(function (err, req, res, next) {
  res.status(500).send("Something broke!");
});

// 404 Error
app.use(function (req, res) {
  res.status(404).send("Sorry cant find that!");
});

// Listening
app.listen(12112, "0.0.0.0", function () {
  console.log("DataTables Editor demo - navigate to http://localhost:12112/");
});
// }



// Test the database connection on startup by getting a list of table names in the db
// This can safely be removed if you are hexpressappy with your db connection configuration.
// It is used purely to show a console error if the connection is not available.
let query;
let bindings;

switch (db.client.constructor.name) {
  case "Client_MSSQL":
    (query =
      "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_catalog = ?"),
      (bindings = [db.client.database()]);
    break;
  case "Client_MySQL":
  case "Client_MySQL2":
    query = "SHOW TABLES";
    break;
  case "Client_PG":
    query =
      "SELECT table_name FROM information_schema.tables WHERE table_schema = current_schema() AND table_catalog = ?";
    bindings = [db.client.database()];
    break;
  case "Client_SQLite3":
    query = "SELECT name AS table_name FROM sqlite_master WHERE type='table'";
    break;
  case "Client_Oracledb":
    query = "SELECT owner, table_name FROM all_tables";
    break;
}

db.raw(query, bindings)
  .then(function () {
    // noop
  })
  .catch(function (err) {
    console.error(err.toString());
  });



