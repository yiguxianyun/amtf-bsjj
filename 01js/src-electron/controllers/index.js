// let router = require('express').Router();
// router.use(require('./staff'));
// router.use(require('./staff-html'));
// router.use(require('./staff-view'));

let db = require('../db');
let router = require('express').Router();
let {
  Editor,
  Field,
  Validate,
  Format,
  Options
} = require("datatables.net-editor-server");

router.all('/api/biaoji', async function (req, res) {
  let editor = new Editor(db, '标记').fields(
    new Field('简称').validator(Validate.notEmpty()),
    new Field('全名').validator(Validate.notEmpty()),
  );

  await editor.process(req.body);
  res.json(editor.data());
});
module.exports = router;

