import { func } from 'electron-edge-js';
import { dialog } from 'electron'
import { log } from 'console';

var fs = require('fs');
var path = require('path');

//解析需要遍历的文件夹，我这以E盘根目录为例
// var filePath = path.resolve('E:');

// //调用文件遍历方法
// fileDisplay(filePath);
// let 路径
let 路径, 文件短名, 无后缀名, 后缀, 反斜杠路径

function 解析文件(全名) {
  路径 = path.dirname(全名);
  文件短名 = path.basename(全名);
  后缀 = path.extname(全名);
  无后缀名 = path.basename(全名, 后缀);
  反斜杠路径 = 路径.replace(/\//g, "\\");
}


//封装日期函数
function 今天的日期() {
  var date = new Date(); //创建日期对象
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var dates = date.getDate();
  arr = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六',]
  var day = date.getDay();
  // return '今天是:' + year + '年' + month + '月' + dates + '号,' + arr[day];
  return `${year}-${month}-${dates}`;  // 2023-07-24
}


export function 选择su文件(全名, ws) {
  let 选择的文件s = []
  解析文件(全名)
  dialog.showOpenDialog({
    title: "选择需导入的su",
    defaultPath: 反斜杠路径,
    filters: [
      { name: 'skp', extensions: ['skp'] },
    ],
    properties: [
      "multiSelections"
    ]
  })
    .then(result => {
      // console.log(result.canceled)
      // console.log(result.filePaths)
      if (!result.canceled) {
        选择的文件s = result.filePaths
      }
      console.log(选择的文件s);
      ws.send(JSON.stringify({ sketchup: '批量导入su', 选择的文件s: 选择的文件s }));
      // return 选择的文件s
    }).catch(err => {
      console.log(err)
    })
  return 选择的文件s
}

export function 生成唯一文件夹(全名, 关键词) {
  //根据文件路径读取文件，返回文件列表
  解析文件(全名)
  let 数字序号 = 1

  路径 = `${路径}/导出dxf_${今天的日期()}`
  try {
    fs.mkdirSync(路径);
  } catch (err) {
    console.error(err);
  }

  var pa = fs.readdirSync(路径);
  pa.forEach(function (短名称, index) {
    全名 = 路径 + "/" + 短名称
    var 全名stat = fs.statSync(全名)
    if (全名stat.isDirectory()) {
      // console.log(短名称);
      var reg = new RegExp(`${无后缀名}_${关键词}(\\d+)`, "i");
      let match = reg.exec(全名)
      // let match = reg.match(全名)
      // let match = 全名.match(`/${关键词}(\d+)/`)
      if (match) {
        // console.log(全名);
        // console.log("match", match);
        let 序号 = parseInt(match[1])
        if (序号 >= 数字序号) {
          数字序号 = 序号 + 1
        }
      }
    }
  })

  唯一名 = `${路径}/${无后缀名}_${关键词}${数字序号}`
  try {
    fs.mkdirSync(唯一名);
  } catch (err) {
    console.error(err);
  }
  return 唯一名
}

export function 生成唯一文件名(全名, 关键词) {
  //根据文件路径读取文件，返回文件列表
  解析文件(全名)
  let 数字序号 = 1

  var pa = fs.readdirSync(路径);
  pa.forEach(function (短名称, index) {
    全名 = 路径 + "/" + 短名称
    var 全名stat = fs.statSync(全名)
    if (全名stat.isFile()) {
      // console.log(短名称);
      var reg = new RegExp(`${无后缀名}_${关键词}(\\d+)`, "i");
      let match = reg.exec(全名)
      // let match = reg.match(全名)
      // let match = 全名.match(`/${关键词}(\d+)/`)
      if (match) {
        // console.log(全名);
        // console.log("match", match);
        let 序号 = parseInt(match[1])
        if (序号 >= 数字序号) {
          数字序号 = 序号 + 1
        }
      }
    }
  })

  唯一名 = `${路径}/${无后缀名}_${关键词}${数字序号}${后缀}`
  return 唯一名
}

export function 获取文件异步(filePath, 关键词) {
  //根据文件路径读取文件，返回文件列表
  let 文件s = []
  fs.readdir(filePath, function (err, files) {
    if (err) {
      console.warn(err)
    } else {
      //遍历读取到的文件列表
      files.forEach(function (filename) {
        //获取当前文件的绝对路径
        var 全名 = path.join(filePath, filename);
        //根据文件路径获取文件信息，返回一个fs.Stats对象
        fs.stat(全名, function (eror, stats) {
          if (eror) {
            console.warn('获取文件stats失败');
          } else {
            var isFile = stats.isFile();//是文件
            if (isFile) {
              // console.log(全名);

              // var reg = new RegExp(`/${关键词}(\d+)/`);
              var reg = new RegExp(`${关键词}(\\d+)`, "i");
              let match = reg.exec(全名)
              // let match = reg.match(全名)
              // let match = 全名.match(`/${关键词}(\d+)/`)
              if (match) {
                console.log(全名);
                console.log("match", match);
                文件s.push(全名)
              }
            }
          }
        })
      });
    }
  });
  console.log("文件s", 文件s);
  return 文件s
}

/**
 * 文件遍历方法
 * @param filePath 需要遍历的文件路径
 */

function fileDisplay(filePath) {
  //根据文件路径读取文件，返回文件列表
  fs.readdir(filePath, function (err, files) {
    if (err) {
      console.warn(err)
    } else {
      //遍历读取到的文件列表
      files.forEach(function (filename) {
        //获取当前文件的绝对路径
        var 全名 = path.join(filePath, filename);
        //根据文件路径获取文件信息，返回一个fs.Stats对象
        fs.stat(全名, function (eror, stats) {
          if (eror) {
            console.warn('获取文件stats失败');
          } else {
            var isFile = stats.isFile();//是文件
            var isDir = stats.isDirectory();//是文件夹
            if (isFile) {
              console.log(全名);
            }
            if (isDir) {
              fileDisplay(全名);//递归，如果是文件夹，就继续遍历该文件夹下面的文件
            }
          }
        })
      });
    }
  });
}


export default fileDisplay;
// exports.fileDisplay = fileDisplay
