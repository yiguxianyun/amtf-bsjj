<!DOCTYPE html>
<html lang="zh">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>关于amtf</title>
  <link rel="shortcut icon" href="image/amtf-ml.ico">
</head>
<style>
  body
  {background-image: url(image/amtf-透明.png) ;
  background-repeat: no-repeat;
  /* background-position: bottom right; */
  background-position: center center;
  background-size: contain;
  /* background-size: 1000px 1654px; */
  }
</style>
<body>
  <h4>本程序 开源、免费</h4>
  <h4>赞助者：</h4>
  <p>
    <li>
      湖南南腾家居有限公司;
    </li>
    <li>
      某越南网友;
    </li>
  <h4>主要功能：</h4>
  <p>
    <li>
      改造动态组件属性编辑界面;
    </li>
    <li>
      把 afu321 设计的组件，转换成适合 ABF 排版的群组形式;
    </li>
    <li>
      把 ABF 排好孔的文件，导出成 天工 用的dxf文件;
    </li>

  </p>
  <p>
  <h4>使用说明：</h4>
  <p>
    <li>
      先给板件添加ABF标签
    </li>
    <li>
      提前开启 amtf_dxf 程序<br>
      (如果没提前开启，关闭amtf_su界面后，重新打开，直到界面左下角出现“amtf_dxf 已连接”)
    </li>
    <li>
      预选板件后点“导出cad”按钮(什么都不选，则导出全部板件)
      <br>(自动排除含有关键词 参考、布局盒子 的板件)
    </li>
    <li>
      孔槽深度怎么识别？：其边线元素所在的图层名(标记)以"-sd深度值"结束
    </li>
  </p>
  <h4>获取程序：</h4>
    <li>
      amtf_su 源码地址：<a href="https://gitee.com/yiguxianyun/amtf_su" target="_blank">https://gitee.com/yiguxianyun/amtf_su</a>
      <br>
      amtf_dxf 源码地址：<a href="https://gitee.com/yiguxianyun/amtf_dxf.git" target="_blank">https://gitee.com/yiguxianyun/amtf_dxf.git</a>
    </li>
    <li>
      视频教程：<a href="https://www.bilibili.com/video/BV17e4y1U7yJ/" target="_blank">https://www.bilibili.com/video/BV17e4y1U7yJ/</a>
    </li>
    <li>
      qq交流群：667675330
    </li>
  </p>
  <h4>广告：</h4>
  <p>
    <li>
      人生难得，佛法难闻，百千万劫难遭遇：<a href="https://www.amtb.cn/#/home" target="_blank">https://www.amtb.cn/#/home</a>;
    </li>
    <li>
      板式家具定制：湖南南腾家居有限公司 <br>
    </li>
  </p>

</body>

</html>
