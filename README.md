### 本程序开源、免费

#### 赞助者

-   湖南南腾家居有限公司;
-   某越南网友;

#### 主要功能

-   把solidworks模型导入到sketchup(能识别简单的孔槽);
-   改造动态组件属性编辑界面;
-   把 afu321 设计的组件，转换成适合 ABF 排版的群组形式;
-   把 ABF 排好孔的文件，导出成 天工 用的 dxf 文件;

#### 安装

-   把 "\00su\src" 里面的文件复制到 su 插件目录
-   把 "\01js\dist\electron\Packaged\amtf-win32-x64" 文件夹放到任意位置（可以给里面的 exe 文件发送给快捷方式）

#### 使用说明

-   详见程序界面的"关于amtf"

#### 广告

-   人生难得，fo法难闻，百千万劫难遭遇：<a href="https://www.amtb.cn/#/home" target="_blank">https://www.amtb.cn/#/home</a>;
-   板式家具定制：湖南南腾家居有限公司
