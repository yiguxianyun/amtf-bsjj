﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using SolidWorks.Interop.swcommands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace amtf_sw
{
    public class 匹配内空等等
    {
        public Mouse msMouse;
        public SW sw;
        public SketchPoint 对象;
        public Component2 拟操作组件;
        public bool 由编程选中;
        public bool 监控匹配内空中;
        public bool 监控选中对象中;
        public bool 监控选择基准面中;
        public bool 已识别内空;
        public bool status;
        public bool boolstatus;
        public int intstatus;
        public int inta;
        public int mateError;
        public double 选中点偏移x;
        public double 选中点偏移y;
        public double 选中点偏移z;
        public Vertex 前参考点 = null;
        public Dictionary<string, Face2> 参考面s = new Dictionary<string, Face2>();
        public dynamic 广播消息;

        public 匹配内空等等()
        {
            //bool kk = sw.sw是否已经打开();
            if (!SW.sw是否已经打开())
            {
                sw = null;
                return;
            }
            sw = new SW();
            ModelView swModelView = (ModelView)sw.swModel.GetFirstModelView();
            msMouse = swModelView.GetMouse();
            //bool status = sw.swModel.Extension.SelectByID2("Point1", "SKETCHPOINT", 0, 0, 0, false, 0, null, 0);
            //对象 = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
        }

        public async Task<object> 监控选中对象(dynamic input = null)
        {
            if (sw == null) { return await SW.提醒打开sw(); }
            // 广播消息 = (Func<object, Task<object>>)input.add;
            // var twoNumbers = new { a = (int)input.a, b = (int)input.b };
            // var addResult = (int)await add(twoNumbers);
            // return addResult * 2;

            广播消息 = (Func<object, Task<object>>)input;
            监控选中对象中 = true;
            添加监控();
            return await AMTF.提醒方法完成(new StackTrace(new StackFrame(3)), sw);
        }
        public void 监控选中对象_后续动作()
        {
            拟操作组件 = sw.获取第一个选中组件对象();
            if (拟操作组件 == null) { return; }
            string 拟操作组件名 = 拟操作组件.GetPathName();
            Debug.Print(拟操作组件名);
            sw.获取对象尺寸状态(sw.获取组件model(拟操作组件));
            SW.尺寸ob["拟操作组件名"] = 拟操作组件名;
            string kk = JsonConvert.SerializeObject(SW.尺寸ob);
            Debug.Print(kk);
            // await add(twoNumbers);
            广播消息(kk);
        }
        public async Task<object> 匹配内空()
        {
            if (sw == null) { return await SW.提醒打开sw(); }
            var 选择的对象s = sw.识别选择的对象();
            if (选择的对象s == null)
            {
                inta = sw.iSwApp.SendMsgToUser2("请选中一个组件，先!", (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                return await Task.FromResult($"出错了!");
            }
            try { 拟操作组件 = 选择的对象s["组件"][0]; }
            catch (System.Exception)
            {
                inta = sw.iSwApp.SendMsgToUser2("请选中一个组件，先!", (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                return await Task.FromResult($"出错了!");
            }
            sw.swModel.ClearSelection2(true);
            拟操作组件.Visible = 0;
            监控匹配内空中 = true;
            添加监控();
            AMTF.设置光标();
            sw.提示用户("请选中一个内空中的面，程序会识别其他面……");
            return await AMTF.提醒方法完成(new StackTrace(new StackFrame(3)), sw);
        }
        public async Task<object> 选择基准面()
        {
            if (sw == null) { return await SW.提醒打开sw(); }
            拟操作组件 = sw.获取选中组件();
            if (拟操作组件 == null) { return await SW.提醒选中组件(); }

            sw.swModel.ClearSelection2(true);
            // 拟操作组件.Visible = 0;
            监控选择基准面中 = true;
            添加监控();
            AMTF.设置光标();
            sw.提示用户("请选中一个基准面……");
            return await AMTF.提醒方法完成(new StackTrace(new StackFrame(3)), sw);
        }

        public void 测试2()
        {
            sw.selMgr.SuspendSelectionList();
            boolstatus = sw.swDocExt.SelectByID2("Right", "PLANE", 0, 0, 0, false, 0, null, 0);
            // boolstatus = sw.swDocExt.SelectByID2("Right@新的板材_3_展示柜-1@展示柜", "PLANE", 0, 0, 0, false, 0, null, 0);
            var swFeature = (Feature)sw.selMgr.GetSelectedObject6(1, -1);

            // 拟操作组件 = (Component2)((Entity)swFeature).GetComponent();
            // boolstatus = sw.swDocExt.SelectByRay(1.5670851616834796, 0.72418654635720259, 0.3958443601418935,
            // -0.28346532418712805, 0.9210543453725567, -0.26703240038208392, 0.012844745898173105, 2, true, 0, 0);
            boolstatus = sw.swDocExt.SelectByID2("基准面3", "PLANE", 0, 0, 0, true, 0, null, 0);
            var 自动识别对象 = sw.selMgr.GetSelectedObject6(2, -1);

            var swRefPlaneFeatureData = (RefPlaneFeatureData)swFeature.GetDefinition();
            // 装配体里面编辑组件的时候，非要下面这句不可👇
            // status = swRefPlaneFeatureData.AccessSelections(sw.swModel, 拟操作组件);
            object[] kk = (object[])swRefPlaneFeatureData.Selections;
            Array.Clear((object[])swRefPlaneFeatureData.Selections, 0, ((object[])swRefPlaneFeatureData.Selections).Length);
            // object[] yy = new object[] { 自动识别对象 };
            swRefPlaneFeatureData.Selections = (Object)自动识别对象;
            swRefPlaneFeatureData.ISetSelections(1, ref 自动识别对象);
            ((object[])swRefPlaneFeatureData.Selections)[1] = null;
            // swRefPlaneFeatureData.Reference[0] = null;
            // swRefPlaneFeatureData.Reference[1] = null;
            var kk1 = swRefPlaneFeatureData.Reference[0];
            boolstatus = sw.selMgr.AddSelectionListObject(swRefPlaneFeatureData.Reference[0], sw.selData);
            sw.selMgr.SuspendSelectionList();

            var kk2 = swRefPlaneFeatureData.Reference[1];
            var kk3 = swRefPlaneFeatureData.Reference[2];
            swRefPlaneFeatureData.Constraint[0] = (int)swRefPlaneReferenceConstraints_e.swRefPlaneReferenceConstraint_Coincident;
            swRefPlaneFeatureData.Reference[0] = 自动识别对象;
            boolstatus = sw.selMgr.AddSelectionListObject(swRefPlaneFeatureData.Reference[0], sw.selData);
            sw.selMgr.SuspendSelectionList();
            status = swFeature.IModifyDefinition2(swRefPlaneFeatureData, sw.swModel, null);
            // status = swFeature.IModifyDefinition2(swRefPlaneFeatureData, sw.swModel, 拟操作组件);
            // sw.assembly.EditAssembly();
        }
        // public void 更新单个基准面(Feature 拟更新面, dynamic 自动识别对象)

        public void 添加配合测试()
        {
            var 选择的对象s = sw.识别选择的对象();
            拟操作组件 = 选择的对象s["组件"][0];
            // sw.swModel.ClearSelection2(true);
            // 拟操作组件.Visible = 0;
            Face2 面 = 选择的对象s["面"][0];
            double[] 选中点坐标 = new double[3]{
                0.8670851616834909,
                0.7231859831155134,
                0.40497469867767677
            };
            sw.识别参考实体面点_射线探测(面, 选中点坐标);
            匹配内空_后续动作();
            // 猜基准面名添加配合(new string[] { "Left", "左", "右视基准面" }, 面, "左重合");
            // 猜基准面名添加配合();
            // 拟操作组件.Visible = 1;

        }

        public void 匹配内空_后续动作()
        {
            拟操作组件.Visible = 1;
            // 取消监听();
            // sw.猜基准面名添加配合("左", 参考面s["左"], "左重合", 拟操作组件);
            // sw.猜基准面名添加配合("下", 参考面s["下"], "下重合", 拟操作组件);
            // sw.猜基准面名添加配合("前", 前参考点, "前重合", 拟操作组件);
            sw.更新或添加板件配合(拟操作组件);

            sw.更新板件基准面外部参考(拟操作组件);
            // boolstatus = sw.swDocExt.SelectByID2("Right@新的板材_3_展示柜-1@展示柜", "PLANE", 0, 0, 0, false, 0, null, 0);
            // sw.猜基准面名添加外部参考("右", 参考面s["右"], 拟操作组件);
            // sw.猜基准面名添加外部参考("后", 参考面s["后"], 拟操作组件);
            // sw.猜基准面名添加外部参考("上", 参考面s["上"], 拟操作组件);
            sw.assembly.EditAssembly();
            AMTF.恢复光标();
        }
        public void 选择基准面_后续动作()
        {
            取消监听();
            AMTF.恢复光标();
        }

        public async Task<object> 识别内空()
        {
            //Task<object> kk = sw.sw是否已经打开();
            if (sw == null) { return await Task.FromResult("呃……，请自行打开solidworks，先"); }
            //sw = new SW();
            // ModelView swModelView = (ModelView)sw.swModel.GetFirstModelView();
            // msMouse = swModelView.GetMouse();
            监控匹配内空中 = true;

            添加监控();
            string 方法名 = System.Reflection.MethodBase.GetCurrentMethod().Name;
            return await Task.FromResult($"{方法名}  完成!");
        }

        public void 添加监控()
        {
            msMouse.MouseSelectNotify += this.ms_MouseSelectNotify;
            msMouse.MouseLBtnDownNotify += this.ms_MouseLBtnDownNotify;
            msMouse.MouseRBtnDownNotify += this.ms_MouseRBtnDownNotify;
            msMouse.MouseMoveNotify += this.ms_MouseMoveNotify;
            // swAssemblyNewSelectionNotify
            sw.assembly.NewSelectionNotify += this.swAssemblyNewSelectionNotify;

            //msMouse. += this.ms_MouseMoveNotify;

        }
        public void 取消监听()
        {
            msMouse.MouseSelectNotify -= this.ms_MouseSelectNotify;
            msMouse.MouseLBtnDownNotify -= this.ms_MouseLBtnDownNotify;
            msMouse.MouseRBtnDownNotify += this.ms_MouseRBtnDownNotify;
            msMouse.MouseMoveNotify -= this.ms_MouseMoveNotify;
        }

        public int swAssemblyNewSelectionNotify()
        {
            Debug.Print("选择列表变了！");
            if (监控选中对象中)
            {
                Debug.Print("监控选中对象中ing");
                监控选中对象_后续动作();
            }
            return 1;
        }

        private int ms_MouseSelectNotify(int Ix, int Iy, double x, double y, double z)
        {
            if (监控选择基准面中)
            {
                Debug.Print("监控选择基准面中ing");
                选择基准面_后续动作();
            }

            if ((!由编程选中) && 监控匹配内空中)
            {
                double[] 选中点坐标 = new double[3];
                选中点坐标[0] = x;
                选中点坐标[1] = y;
                选中点坐标[2] = z;
                // 0.8670851616834909
                // 0.7231859831155134
                // 0.40497469867767677

                Face2 选中面 = null;
                try
                {
                    选中面 = (Face2)sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
                }
                catch (Exception)
                {
                    //throw;
                    return 0;
                }

                取消监听();
                // 识别参考实体面点_射线探测(选中面, 选中点坐标);
                由编程选中 = true;
                var 参考面点s = sw.识别参考实体面点_射线探测(选中面, 选中点坐标);
                参考面点s = sw.找面的靠前点(选中面);
                匹配内空_后续动作();
                已识别内空 = true;
                由编程选中 = false;
            }
            return 1;
        }

        private int ms_MouseLBtnDownNotify(int x, int y, int WParam)
        {
            Debug.Print("Left-mouse button pressed.");
            //取消监听();
            return 1;
        }
        private int ms_MouseRBtnDownNotify(int x, int y, int WParam)
        {
            Debug.Print("右键 button pressed.");
            //取消监听();
            if (已识别内空)
            {
                匹配内空_后续动作();
            }
            已识别内空 = false;
            return 1;
        }
        private int ms_MouseMoveNotify(int x, int y, int WParam)
        {
            //Debug.Print("ms_MouseMoveNotify.");
            double[] 点坐标 = new double[3];
            点坐标[0] = x;
            点坐标[1] = y;
            点坐标[2] = 0;
            //Debug.Print("转换前");
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());

            屏幕坐标to模型坐标(ref 点坐标);
            //bool status = sw.swModel.Extension.SelectByID2("Point1", "SKETCHPOINT", 0, 0, 0, false, 0, null, 0);
            //SketchPoint 对象 = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
            //对象.X = 点坐标[0];
            //对象.Y = 点坐标[1];
            //对象.Z = 点坐标[2];
            //Debug.Print("转换后");
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());

            //double radius = 1.0;
            //ModelView swModelView = sw.swModel.ActiveView;
            //dynamic 法向 = swModelView.Translation3.ArrayData;
            //bool status = sw.swDocExt.SelectByRay(点坐标[0], 点坐标[1], 点坐标[2], 法向.ArrayData[0], 法向.ArrayData[1], 法向.ArrayData[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            //Debug.Print("Selection status: " + status);
            //if (status)
            //{
            //MathTransform o3 = swModelView.Orientation3;
            //dynamic 法向 = swModelView.GetEyePoint();

            //MathUtility mathUtil = (MathUtility)sw.iSwApp.GetMathUtility();
            ////MathTransform swXform = swModelView.Transform;
            //MathTransform swXform = swModelView.Orientation3;

            //swXform = swXform.Inverse();
            //double[] vPnt = new double[3];
            //vPnt[0] = 0.0;
            //vPnt[1] = 0;
            //vPnt[2] = -1;
            //MathPoint swMathPt = mathUtil.CreatePoint(vPnt);
            //swMathPt = swMathPt.MultiplyTransform(swXform);
            //dynamic 法向 = swMathPt.ArrayData;

            ////}
            ////对象.X = 法向[0];
            ////对象.Y = 法向[1];
            ////对象.Z = 法向[2];
            ////bool status = sw.swDocExt.SelectByRay(点坐标[0], 点坐标[1], 点坐标[2], 法向[0], 法向[1], 法向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            //bool status = sw.swDocExt.SelectByRay(点坐标[0], 点坐标[1], 点坐标[2], 法向[0], 法向[1], 法向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionExtensive);
            //Debug.Print("Selection status: " + status);

            return 1;

        }
        public void 屏幕坐标to模型坐标(ref double[] 点坐标)
        {
            //MathUtility mathUtil = sldworks.MathUtility;
            MathUtility mathUtil = (MathUtility)sw.iSwApp.GetMathUtility();
            ModelView swModelView = (ModelView)sw.swModel.ActiveView;
            MathTransform swXform = swModelView.Transform;
            swXform = (MathTransform)swXform.Inverse();
            MathPoint swMathPt = (MathPoint)mathUtil.CreatePoint((点坐标));
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swXform);
            double[] swMathPtar = (double[])swMathPt.ArrayData;
            点坐标[0] = swMathPtar[0];
            点坐标[1] = swMathPtar[1];
            点坐标[2] = swMathPtar[2];

        }

    }
}
