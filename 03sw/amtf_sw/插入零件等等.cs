using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using SolidWorks.Interop.swcommands;
using System.Diagnostics;

namespace amtf_sw
{
    public class 插入零件等等
    {
        public SW sw;
        public string 模板全名;
        public string 目标全名;
        public Component2 拟操作组件;
        public Component2 基准盒组件;
        public string 资源路径;
        public int status;
        public int errors;
        public int warnings;
        public bool boolstatus;
        public Vertex 前参考点 = null;
        public Dictionary<string, Face2> 参考面s = new Dictionary<string, Face2>();
        private string 组件主名称;

        public 插入零件等等()
        {
            if (!SW.sw是否已经打开())
            {
                sw = null;
                return;
            }
            sw = new SW();
            资源路径 = AMTF.获取资源路径();
        }
        public async Task<object> 插入基准盒()
        {
            if (sw == null) { return await SW.提醒打开sw(); }
            组件主名称 = "基准盒";
            基准盒组件 = sw.根据名称找组件($"{组件主名称}_{sw.FilenameWHZ}");
            if (基准盒组件 != null)
            {
                sw.更新或添加原点配合(基准盒组件, "原点重合");
            }
            else
            {
                目标全名 = AMTF.复制sw文件(sw, 组件主名称, 组件主名称);
                // 模板全名 = $"{资源路径}\\sw模板文件\\{组件主名称}.PRTDOT";
                // 目标全名 = $"{sw.FilePath}\\{组件主名称}_{sw.FilenameWHZ}.SLDPRT";
                // if (!File.Exists(目标全名))
                // {
                //     FileInfo file = new FileInfo(模板全名);
                //     file.CopyTo(目标全名, true);
                // }
                基准盒组件 = sw.插入零件(目标全名, true, true, true, false);
            }
            return await AMTF.提醒方法完成(new StackTrace(new StackFrame(3)), sw);
        }
        public async Task<object> 插入左侧板()
        {
            if (sw == null) { return await SW.提醒打开sw(); }
            var 参考面点s = sw.识别参考实体面点_按面法向(基准盒组件);
            // 参考面点s = sw.找面的靠前点(参考面点s["左"]);
            sw.插入板子("左", sw);
            // 组件主名称 = "左侧板";
            // var 板组件 = sw.根据名称找组件($"{组件主名称}_{sw.FilenameWHZ}");
            // if (板组件 == null)
            // {
            //     目标全名 = AMTF.复制sw文件(sw, 组件主名称, "板子");
            //     板组件 = sw.插入零件(目标全名, false, false, false, false);
            // }
            // sw.更新或添加板件配合(板组件);
            // sw.更新板件基准面外部参考(板组件);

            return await AMTF.提醒方法完成(new StackTrace(new StackFrame(3)), sw);
        }
        public async Task<object> 生成柜体()
        {
            DateTime start = DateTime.Now; //获取代码段执行开始时的时间
            await 插入基准盒();
            var 参考面点s = sw.识别参考实体面点_按面法向(基准盒组件);
            // 参考面点s = sw.找面的靠前点(参考面点s["左"]);
            sw.插入板子("左", sw);
            sw.插入板子("右", sw);
            sw.插入板子("上", sw);
            sw.插入板子("下", sw);
            DateTime stop = DateTime.Now; //获取代码段执行结束时的时间
            TimeSpan tspan = stop - start; //求时间差
            string 执行时间 = tspan.TotalSeconds.ToString(); //获取代码段执行时间
            return await AMTF.提醒方法完成(new StackTrace(new StackFrame(3)), sw, $"执行时间:{执行时间}");
            // 14秒
        }

    }
}