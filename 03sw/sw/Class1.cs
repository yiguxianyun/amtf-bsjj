﻿using amtf;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sw
{
    public class Startup
    {
        private ISldWorks swApp;

        public async Task<object> Invoke(string message)
        {
            string[] ss = message.Split('|');
            string 方法名 = ss[0];
            switch (方法名)
            {
                case "sw转su":
                    return await sw转su();
                default:
                    return await Task.FromResult("没明白，按兵不动，先");

            }

            
        }
        private async Task<object> sw转su() {
            Process 当前打开sw进程 = SW.查找并激活打开的进程("sldworks");
            if (当前打开sw进程 == null)
            {
                return await Task.FromResult("呃……，请自行打开solidworks，先");
            }
            else
            {
                SW sw = new SW();
                ModelDoc2 swModel = sw.swModel;
                if (swModel == null)
                {
                    return await Task.FromResult("呃……，请自行打开一个solidworks 模型，先");
                }
                swApp = sw.iSwApp;
                Frame swFrame = swApp.Frame();
                //swApp.SendMsgToUser2("你好，世界！", (int)swMessageBoxIcon_e.swMbInformation, (int)swMessageBoxBtn_e.swMbOk);
                string 时间 = DateTime.Now.ToString("yyyyMMdd-HHmm-ss");
                string 拟导出文件夹 = sw.FilePath + "/导出stl-" + sw.FilenameWHZ + 时间;
                string 拟导出文件 = 拟导出文件夹 + "/" + sw.FilenameWHZ + ".STL";

                Directory.CreateDirectory(拟导出文件夹);
                swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swSTLBinaryFormat, true);
                swApp.SetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swExportStlUnits, (int)swLengthUnit_e.swMM);
                //swApp.SetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swSTLQuality, (int)swSTLQuality_e.swSTLQuality_Fine);
                //double kk =swApp.GetUserPreferenceDoubleValue((int)swUserPreferenceDoubleValue_e.swSTLDeviation);
                //double yy = swApp.GetUserPreferenceDoubleValue((int)swUserPreferenceDoubleValue_e.swSTLAngleTolerance);


                swApp.SetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swSTLQuality, (int)swSTLQuality_e.swSTLQuality_Custom);
                swApp.SetUserPreferenceDoubleValue((int)swUserPreferenceDoubleValue_e.swSTLDeviation, 0.0004);
                swApp.SetUserPreferenceDoubleValue((int)swUserPreferenceDoubleValue_e.swSTLAngleTolerance, 0.1);
                swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swSTLDontTranslateToPositive, false);
                swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swSTLComponentsIntoOneFile, false);

                int intstatus = sw.swModel.SaveAs3(拟导出文件, 0, 2);
                swFrame.SetStatusBarText("拟导出文件结果:" + intstatus);

                SW.查找并激活打开的进程("sketchup");

                return await Task.FromResult(拟导出文件夹);
                //return true;
            }

        }

        void 激活su窗口()
        {
        
        }
    }


}
