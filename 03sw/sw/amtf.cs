﻿using System;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading.Tasks;
//using Scripting;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

namespace amtf
{
    public class SW
    {
        public ISldWorks iSwApp;
        public ModelDoc2 swModel = default(ModelDoc2);
        public SelectionMgr selMgr = default(SelectionMgr);
        public SelectData selData = default(SelectData);
        public SketchManager swSkMgr = default(SketchManager);
        int numAdded;
        public int nRetval;
        public bool boolStatus, status;
        public bool b激活 = true;
        int errors, Warnings;
        public MassProperty swMass = default(MassProperty);
        public ModelDocExtension swDocExt = default(ModelDocExtension);
        ModelView swModelView = null;
        MathTransform swTransform = default(MathTransform);
        MathPoint swMathPt = default(MathPoint);
        MathUtility swMathUtil = default(MathUtility);
        Component2 当前组件 = null;
        double[] arrayData;

        public bool 父级to组件 = false;
        public bool 草to模 = false;
        public Sketch 当前草图 = null;

        public SW()
        {
            连接sw();
            设置sw常用对象();
        }
        public SW(ISldWorks iSwApp)
        {
            设置sw常用对象();
        }
        public SW(string sw全名, bool 显示 = true)
        {
            连接sw();
            获取指定文件(sw全名);
            设置sw常用对象();
            swModel.Visible = 显示;
        }

        public void 连接sw()
        {
            //Process 进程 = 查找当前打开sw进程();
            //if (进程==null)
            //{

            //}
            //iSwApp = new SldWorks();//这样new在别人的机器上会报错！
            iSwApp = (SldWorks)Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application"));
            //SldWorks.Application.22=sw2017
            //    //SldWorks.Application.26 = sw 2018
            //iSwApp = (SldWorks)Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application.26"));
            //设置sw常用对象();

        }
        void 设置sw常用对象()
        {
            swModel = (ModelDoc2)iSwApp.ActiveDoc;
            if (swModel != null)
            {
                设置sw常用对象(swModel);
            }
        }
        void 设置sw常用对象(ModelDoc2 swModel)
        {
            selMgr = (SelectionMgr)swModel.SelectionManager;
            selData = selMgr.CreateSelectData();
            swDocExt = (ModelDocExtension)swModel.Extension;
            swSkMgr = swModel.SketchManager;
            string sw全名 = swModel.GetPathName();
            if (sw全名 != "")
            {
                拆分文件名(sw全名);
            }
            if (b激活)
            {
                激活sw();
            }
        }
        public void 获取指定文件(string sw全名)
        {
            类型判断(sw全名);
            swModel = (ModelDoc2)iSwApp.GetOpenDocumentByName(sw全名);
            if (swModel == null)
            {
                swModel = (ModelDoc2)iSwApp.OpenDoc(sw全名, swFileTYpe);
            }
        }
        public String FilePath, Filename, FilenameWHZ;
        void 拆分文件名(String FilePathName)
        {
            int 反斜杠位置 = FilePathName.LastIndexOf("\\");
            FilePath = FilePathName.Substring(0, 反斜杠位置); //分解路径
            Filename = FilePathName.Substring(反斜杠位置 + 1); //分解文件名
            FilenameWHZ = Filename.Substring(0, Filename.Length - 7);
            //string fileFullPath = @"D:\Temp\aa.txt";
            //string fileName = Path.GetFileName(fileFullPath);  //aa.txt
            //string filePathOnly = Path.GetDirectoryName(fileFullPath);  //D:\Temp
            //string folderName = Path.GetFileName(filePathOnly);  //Temp
        }
        int swFileTYpe;
        void 类型判断(String FilePathName)
        {
            string 末尾三 = FilePathName.Substring(FilePathName.Length - 3).ToUpper();
            string 末尾六 = FilePathName.Substring(FilePathName.Length - 6).ToUpper();
            switch (末尾三)
            {
                case "PRT":
                case "LFP":
                    swFileTYpe = 1;
                    break;
                case "ASM":
                    swFileTYpe = 2;
                    break;
                case "DRW":
                    swFileTYpe = 3;
                    break;
                default:
                    if (末尾六 == "DRWDOT") { swFileTYpe = 3; }
                    break;
            }

        }

        [DllImport("user32.dll")]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);
        public void 激活sw()
        {
            Process localById = Process.GetProcessById(iSwApp.GetProcessID());
            IntPtr handle = localById.MainWindowHandle;
            SwitchToThisWindow(handle, true);    // 激活，显示在最前
        }
        public void 提示用户(string 提示信息)
        {
            Frame swFrame = (Frame)iSwApp.Frame();
            swFrame.SetStatusBarText(提示信息);
        }

        public void 过滤销钉符号()
        {
            swSelectType_e[] filters = new swSelectType_e[1];
            // if (iSwApp.GetSelectionFilters() == null)
            // {
            //     filters = (swSelectType_e[])iSwApp.GetSelectionFilters();
            // }
            iSwApp.SetSelectionFilters(iSwApp.GetSelectionFilters(), false);
            filters[0] = swSelectType_e.swSelDOWELSYMS;
            iSwApp.SetSelectionFilters(filters, true);
        }
        public void 清除过滤()
        {
            swSelectType_e[] filters = new swSelectType_e[0];
            filters = (swSelectType_e[])iSwApp.GetSelectionFilters();
            iSwApp.SetSelectionFilters(filters, false);
        }
        public void 屏幕坐标to草图坐标(ref double x, ref double y)
        {
            double[] 源点坐标 = new double[3];
            // 源点坐标[0] = Convert.ToInt32(x); 源点坐标[1] = Convert.ToInt32(y); 源点坐标[2] = 0;
            源点坐标[0] = x; 源点坐标[1] = y; 源点坐标[2] = 0;
            double[] 新点坐标 = new double[3];
            新点坐标 = 屏幕坐标to模型坐标(源点坐标);
            // '如果是从模型转换到草图，且是在装配体环境下编辑零件，则需要先转换到零件坐标系↓
            if (!swModel.IsEditingSelf())
            {
                当前组件 = ((AssemblyDoc)swModel).GetEditTargetComponent();
                父级to组件 = true;
                新点坐标 = 组件坐标ht父级坐标(新点坐标);
            }
            草to模 = false;
            新点坐标 = 草图坐标ht模型坐标(新点坐标);
            x = 源点坐标[0]; y = 源点坐标[1];
        }

        public double[] 屏幕坐标to模型坐标(double[] 点坐标)
        {
            swMathUtil = (MathUtility)iSwApp.GetMathUtility();
            // 屏幕坐标→模型坐标↓
            swModelView = (ModelView)swModel.ActiveView;
            Debug.Print(swModel.GetPathName());
            swTransform = swModelView.Transform;
            // swTransform.Inverse();//Inverse()返回的仅仅是一个对象，然后没人接收，这里卡了一个下午和晚上，几度怀疑人生~~
            swTransform = (MathTransform)swTransform.Inverse();
            swMathPt = (MathPoint)swMathUtil.CreatePoint(点坐标);
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swTransform);
            arrayData = (double[])swMathPt.ArrayData;
            double[] @params;
            @params = (double[])swMathPt.ArrayData;
            点坐标[0] = arrayData[0]; 点坐标[1] = arrayData[1]; 点坐标[2] = arrayData[2];
            return 点坐标;
        }
        public double[] 组件坐标ht父级坐标(double[] 点坐标)
        {
            swMathUtil = (MathUtility)iSwApp.GetMathUtility();
            // 屏幕坐标→模型坐标↓
            swModelView = (ModelView)swModel.ActiveView;
            swTransform = 当前组件.Transform2;
            if (父级to组件)
            {
                // swTransform.Inverse();
                swTransform = (MathTransform)swTransform.Inverse();
            }
            swMathPt = (MathPoint)swMathUtil.CreatePoint(点坐标);
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swTransform);
            arrayData = (double[])swMathPt.ArrayData;
            点坐标[0] = arrayData[0]; 点坐标[1] = arrayData[1]; 点坐标[2] = arrayData[2];
            return 点坐标;
        }
        public double[] 草图坐标ht模型坐标(double[] 点坐标)
        {
            if (当前草图 == null)
            {
                nRetval = iSwApp.SendMsgToUser2("还没有进入草图模式!", (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                return null;
            }
            swMathUtil = (MathUtility)iSwApp.GetMathUtility();
            swTransform = 当前草图.ModelToSketchTransform;
            if (草to模)
            {
                // swTransform.Inverse();
                swTransform = (MathTransform)swTransform.Inverse();
            }
            swMathPt = (MathPoint)swMathUtil.CreatePoint(点坐标);
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swTransform);
            arrayData = (double[])swMathPt.ArrayData;
            点坐标[0] = arrayData[0]; 点坐标[1] = arrayData[1]; 点坐标[2] = arrayData[2];
            return 点坐标;
        }

        public void 切换主从尺寸()
        {
            int 总数 = selMgr.GetSelectedObjectCount2(-1);
            for (int i = 1; i <= 总数; i++)
            {
                object 选择对象 = selMgr.GetSelectedObject6(i, -1);
                int objType = selMgr.GetSelectedObjectType3(i, -1);
                switch (objType)
                {
                    case (int)swSelectType_e.swSelDIMENSIONS:
                        Dimension 尺寸 = (Dimension)((DisplayDimension)选择对象).GetDimension();
                        int kk = 尺寸.DrivenState;
                        if (kk == 2)
                        {
                            尺寸.DrivenState = 1;
                        }
                        else if (kk == 1)
                        {
                            尺寸.DrivenState = 2;
                        }
                        break;
                }
            }
            swModelView = (ModelView)swModel.ActiveView;
            swModelView.TranslateBy(-0.0001, 0);
            swModel.ClearSelection2(true);
        }
        public void 复制sw文件(string 模板文件, string 需要文件)
        {
            //FileSystemObject fso = new FileSystemObject();
            try
            {
                //File.Copy(模板文件, 需要文件, false);
                //fso.CopyFile(模板文件, 需要文件, false);
                //模板文件 = @"D:\03_dm\dm\00模板_Z45三维模型（东迈）\臂架\下臂-1.SLDPRT";
                //需要文件 = @"D:\03_dm\dm\00模板_Z45三维模型（东迈）\臂架\下臂-188898.SLDPRT";
                string filePathOnly = Path.GetDirectoryName(需要文件);  //D:\Temp
                if (!Directory.Exists(filePathOnly))
                {
                    Directory.CreateDirectory(filePathOnly);
                }
                //fso.CopyFile(模板文件, 需要文件, false);
                File.Copy(模板文件, 需要文件, false);

            }
            catch (System.Exception ex)
            {
                // throw;
                //如果需要文件已经打开，想要覆盖的话↓未完成……
                nRetval = iSwApp.SendMsgToUser2(ex.ToString(), (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                ModelDoc2 kkswModel = (ModelDoc2)iSwApp.ActivateDoc3(需要文件, false, 0, errors);
                nRetval = kkswModel.ForceReleaseLocks();
                //fso.CopyFile(模板文件, 需要文件, false);

                int nRetVal重载 = kkswModel.ReloadOrReplace(false, 需要文件, true);

            }
            finally
            {

            }
        }
        
        public static Process 查找并激活打开的进程(string 进程名称)
        {
            foreach (System.Diagnostics.Process 进程 in System.Diagnostics.Process.GetProcesses())
            {
                //string 进程名 = 进程.MainWindowTitle;
                string 进程名 = 进程.ProcessName;
                Debug.Print(进程名.ToString());
                //if (进程名.Contains("SldWorks.Application"))
                //if ((进程名.ToUpper()).Contains("solidworks".ToUpper()))
                if ((进程名.ToUpper())==(进程名称.ToUpper()))
                    {
                    //thisproc.Kill();
                    Debug.Print(进程名);
                    //ShowWindowAsync(进程.MainWindowHandle); //调用api函数，正常显示窗口
                    //SetForegroundWindow(进程.MainWindowHandle); //将窗口放置最前端
                    SwitchToThisWindow(进程.MainWindowHandle, true);
                    return 进程;

                }
            }
            return null;
        }
        public static bool sw是否已经打开()
        {
            Process 当前打开sw进程 = 查找并激活打开的进程("sldworks");
            if (当前打开sw进程 == null)
            {
                return false;
            }
            return true;

        }
        ////在进程中查找是否已经有实例在运行
        //#region 确保程序只运行一个实例
        //private static Process RunningInstance()
        //{
        //    Process current = Process.GetCurrentProcess();
        //    Process[] processes = Process.GetProcessesByName(current.ProcessName);
        //    //遍历与当前进程名称相同的进程列表
        //    foreach (Process process in processes)
        //    {
        //        //如果实例已经存在则忽略当前进程
        //        if (process.Id != current.Id)
        //        {
        //            //保证要打开的进程同已经存在的进程来自同一文件路径
        //                //if (Assembly.GetExecutingAssembly().Location.Replace("/", "\\") == current.MainModule.FileName)
        //                //{
        //                //返回已经存在的进程
        //            return process;
        //            //}
        //        }
        //    }
        //    return null;
        //}
        //3.已经有了就把它激活，并将其窗口放置最前端
        //private static void HandleRunningInstance(Process instance)
        //{
        //    ShowWindowAsync(instance.MainWindowHandle, 1); //调用api函数，正常显示窗口
        //    SetForegroundWindow(instance.MainWindowHandle); //将窗口放置最前端
        //}
        [DllImport("User32.dll")]
        //private static extern bool ShowWindowAsync(System.IntPtr hWnd, int cmdShow);
        private static extern bool ShowWindowAsync(System.IntPtr hWnd);
        [DllImport("User32.dll")]
        private static extern bool SetForegroundWindow(System.IntPtr hWnd);
//#endregion

    }

}

