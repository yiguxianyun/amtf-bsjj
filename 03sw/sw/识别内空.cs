﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using SolidWorks.Interop.swcommands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using amtf;


namespace sw
{
    public class 识别内空
    {
        public Mouse msMouse;
        public SW sw;
        public SketchPoint 对象;
        public bool 由编程选中;
        public double 选中点偏移x;
        public double 选中点偏移y;
        public double 选中点偏移z;


        public 识别内空()
        {
            //bool kk = SW.sw是否已经打开();
            if (!SW.sw是否已经打开())
            {
                sw = null;
                return;
            }
            sw = new SW();
            //bool status = sw.swModel.Extension.SelectByID2("Point1", "SKETCHPOINT", 0, 0, 0, false, 0, null, 0);
            //对象 = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
        }
        public async Task<object> 主体()
        {
            //Task<object> kk = SW.sw是否已经打开();
            if (sw == null)
            {
                return await Task.FromResult("呃……，请自行打开solidworks，先");
            }
            //sw = new SW();
            ModelView swModelView = (ModelView)sw.swModel.GetFirstModelView();
            msMouse = swModelView.GetMouse();
            AttachEventHandlers();


            return await Task.FromResult("好的，继续");
        }

        public void AttachEventHandlers()
        {
            AttachSWEvents();
        }

        public void AttachSWEvents()
        {
            msMouse.MouseSelectNotify += this.ms_MouseSelectNotify;
            msMouse.MouseLBtnDownNotify += this.ms_MouseLBtnDownNotify;
            msMouse.MouseMoveNotify += this.ms_MouseMoveNotify;
            //msMouse. += this.ms_MouseMoveNotify;

        }
        public void 取消监听()
        {
            msMouse.MouseSelectNotify -= this.ms_MouseSelectNotify;
            msMouse.MouseLBtnDownNotify -= this.ms_MouseLBtnDownNotify;
            msMouse.MouseMoveNotify -= this.ms_MouseMoveNotify;
        }

        private int ms_MouseSelectNotify(int Ix, int Iy, double x, double y, double z)
        {
            //Debug.Print("Selection made:");
            //Debug.Print(" Mouse location:");
            //Debug.Print("   Window space coordinates:");
            //Debug.Print("     " + Ix);
            //Debug.Print("     " + Iy);
            //Debug.Print("   World space coordinates:");
            //Debug.Print("     " + x);
            //Debug.Print("     " + y);
            //Debug.Print("     " + z);

            //bool status = sw.swModel.Extension.SelectByID2("Point1", "SKETCHPOINT", 0, 0, 0, false, 0, null, 0);
            //SketchPoint 对象 = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
            //对象.X = x;
            //对象.Y = y;
            //对象.Z = z;
            if (!由编程选中)
            {
                double[] 选中点坐标 = new double[3];
                选中点坐标[0] = x;
                选中点坐标[1] = y;
                选中点坐标[2] = z;

                Face2 选中面 = null;
                Entity swEntity = null;
                try
                {
                    选中面 = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
                    swEntity = (Entity)sw.selMgr.GetSelectedObject6(1, -1);
                }
                catch (Exception)
                {
                    //throw;
                    return 0;
                }
                MathUtility MathUtil = (MathUtility)sw.iSwApp.GetMathUtility();
                MathTransform swXform;
                MathTransform 组件tsf;

                double[] Xform = new double[16];
                //3轴旋转
                Xform[0] = 1;
                Xform[1] = 0.0;
                Xform[2] = 0.0;
                Xform[3] = 0.0;
                Xform[4] = 1;
                Xform[5] = 0.0;
                Xform[6] = 0.0;
                Xform[7] = 0.0;
                Xform[8] = 1;
                //translation👇
                Xform[9] = 0;
                Xform[10] = 0;
                Xform[11] = 0;
                //缩放
                Xform[12] = 1.0;
                Xform[13] = 0.0;
                Xform[14] = 0.0;
                Xform[15] = 0.0;

                var 面法向 = 选中面.Normal;
                MathVector 面法向向量 = MathUtil.CreateVector(面法向);
                var 长 = 面法向向量.GetLength();
                //面法向向量.Scale(10 / 长);
                面法向向量 = 面法向向量.Normalise();
                面法向向量=面法向向量.Scale(0.001);
                //面法向向量 = 面法向向量.Scale(0.01);
                var 长2 = 面法向向量.GetLength();
                Body2 实体 = 选中面.GetBody();
                //如果是空心面👇
                //实体.Select2(false,null);
                var kk=实体.GetType();
                //1 = Sheet body
                if (kk == 1)
                {
                    Xform[0] = -1;
                    Xform[4] = -1;
                    Xform[8] = -1;
                    swXform = (MathTransform)MathUtil.CreateTransform(Xform);
                    面法向向量 = 面法向向量.MultiplyTransform(swXform);
                }
                Component2 swComponent = (Component2)swEntity.GetComponent();
                组件tsf = swComponent.Transform2;
                面法向向量 = 面法向向量.MultiplyTransform(组件tsf);

                //选中点，往面法向偏移微小距离
                Xform[0] = 1;
                Xform[4] = 1;
                Xform[8] = 1;
                Xform[9] = 面法向向量.ArrayData[0];
                Xform[10] = 面法向向量.ArrayData[1];
                Xform[11] = 面法向向量.ArrayData[2];
                swXform = (MathTransform)MathUtil.CreateTransform(Xform);
                MathPoint swMathPt = MathUtil.CreatePoint(选中点坐标);
                swMathPt = swMathPt.MultiplyTransform(swXform);

                选中点偏移x = swMathPt.ArrayData[0];
                选中点偏移y = swMathPt.ArrayData[1];
                选中点偏移z = swMathPt.ArrayData[2];
                //return 1;

                double[] x正方向 = new double[] { 1, 0, 0 };
                double[] x负方向 = new double[] { -1, 0, 0 };
                double[] y正方向 = new double[] { 0, 1, 0 };
                double[] y负方向 = new double[] { 0, -1, 0 };
                double[] z正方向 = new double[] { 0, 0, 1 };
                double[] z负方向 = new double[] { 0, 0, -1 };
                double radius = 0;
                ModelView swModelView = sw.swModel.ActiveView;
                由编程选中 = true;
                bool status;
                //Face2[] 左右前后上下面 = new Face2[6];
                Dictionary<string, Face2> 参考面s = new Dictionary<string, Face2>();
                Vertex 前参考点 = null;
                //sw.swModel.ClearSelection2(true);
                status = sw.swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, x正方向[0], x正方向[1], x正方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
                参考面s["右"] = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);

                //sw.swModel.ClearSelection2(true);
                status = sw.swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, x负方向[0], x负方向[1], x负方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
                参考面s["左"] = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);

                //sw.swModel.ClearSelection2(true);
                status = sw.swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, y正方向[0], y正方向[1], y正方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
                参考面s["后"] = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);

                //sw.swModel.ClearSelection2(true);
                status = sw.swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, z正方向[0], z正方向[1], z正方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
                参考面s["上"] = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
                //sw.swModel.ClearSelection2(true);
                status = sw.swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, z负方向[0], z负方向[1], z负方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
                参考面s["下"] = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
                //status = sw.swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z,y正方向[0], y正方向[1], y正方向[2], radius, (int)swSelectType_e.swSelFACES, true, 0, (int)swSelectOption_e.swSelectOptionDefault);

                var loops = 选中面.GetLoops();
                foreach (Loop2 loop in loops)
                {
                    if (loop.IsOuter())
                    {
                        object[] vVertices = (object[])loop.GetVertices();
                        Dictionary<Vertex, double[]> 点dict = new Dictionary<Vertex, double[]>();
                        //List<double> y点list = new List<double>();
                        foreach (Vertex vt in vVertices)
                        {
                            //var py=vt.GetPoint()[1];
                            //y点list.Add(vt.GetPoint()[1]);
                            //点dict.Add(vt, vt.GetPoint()[1]);
                            swMathPt = MathUtil.CreatePoint(vt.GetPoint());
                            swMathPt = swMathPt.MultiplyTransform(组件tsf);
                            点dict.Add(vt, swMathPt.ArrayData);

                            //点dict.Add(vt, vt.GetPoint());
                        }
                        var sort点dict = from item in 点dict orderby item.Value[0] ascending select item;
                        sort点dict = from item in sort点dict orderby item.Value[2] ascending select item;
                        sort点dict = from item in sort点dict orderby item.Value[1] ascending select item;

                        //var sort点dict = 点dict.OrderByDescending(item => item.Value[0]);
                        //sort点dict = sort点dict.ThenByDescending(item => item.Value[2]);
                        //sort点dict = sort点dict.ThenByDescending(item => item.Value[1]);
                        //sw.swModel.ClearSelection2(true);
                        status = sw.selMgr.AddSelectionListObject(sort点dict.First().Key, null);
                        前参考点 = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
                    }
                }

                由编程选中 = false;
            }
            return 1;
        }

        private int ms_MouseLBtnDownNotify(int x, int y, int WParam)
        {
            Debug.Print("Left-mouse button pressed.");
            //取消监听();
            return 1;
        }
        private int ms_MouseMoveNotify(int x, int y, int WParam)
        {
            //Debug.Print("ms_MouseMoveNotify.");
            double[] 点坐标 = new double[3];
            点坐标[0] = x;
            点坐标[1] = y;
            点坐标[2] = 0;
            //Debug.Print("转换前");
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());

            屏幕坐标to模型坐标(ref 点坐标);
            //bool status = sw.swModel.Extension.SelectByID2("Point1", "SKETCHPOINT", 0, 0, 0, false, 0, null, 0);
            //SketchPoint 对象 = sw.selMgr.GetSelectedObject6(sw.selMgr.GetSelectedObjectCount2(-1), -1);
            //对象.X = 点坐标[0];
            //对象.Y = 点坐标[1];
            //对象.Z = 点坐标[2];
            //Debug.Print("转换后");
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());
            //Debug.Print(点坐标[0].ToString());

            //double radius = 1.0;
            //ModelView swModelView = sw.swModel.ActiveView;
            //dynamic 法向 = swModelView.Translation3.ArrayData;
            //bool status = sw.swDocExt.SelectByRay(点坐标[0], 点坐标[1], 点坐标[2], 法向.ArrayData[0], 法向.ArrayData[1], 法向.ArrayData[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            //Debug.Print("Selection status: " + status);
            //if (status)
            //{
            //MathTransform o3 = swModelView.Orientation3;
            //dynamic 法向 = swModelView.GetEyePoint();

            //MathUtility MathUtil = (MathUtility)sw.iSwApp.GetMathUtility();
            ////MathTransform swXform = swModelView.Transform;
            //MathTransform swXform = swModelView.Orientation3;

            //swXform = swXform.Inverse();
            //double[] vPnt = new double[3];
            //vPnt[0] = 0.0;
            //vPnt[1] = 0;
            //vPnt[2] = -1;
            //MathPoint swMathPt = MathUtil.CreatePoint(vPnt);
            //swMathPt = swMathPt.MultiplyTransform(swXform);
            //dynamic 法向 = swMathPt.ArrayData;

            ////}
            ////对象.X = 法向[0];
            ////对象.Y = 法向[1];
            ////对象.Z = 法向[2];
            ////bool status = sw.swDocExt.SelectByRay(点坐标[0], 点坐标[1], 点坐标[2], 法向[0], 法向[1], 法向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            //bool status = sw.swDocExt.SelectByRay(点坐标[0], 点坐标[1], 点坐标[2], 法向[0], 法向[1], 法向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionExtensive);
            //Debug.Print("Selection status: " + status);

            return 1;

        }
        public void 屏幕坐标to模型坐标(ref double[] 点坐标)
        {
            //MathUtility swMathUtil = sldworks.MathUtility;
            MathUtility MathUtil = (MathUtility)sw.iSwApp.GetMathUtility();
            ModelView swModelView = sw.swModel.ActiveView;
            MathTransform swXform = swModelView.Transform;
            swXform = swXform.Inverse();
            MathPoint swMathPt = MathUtil.CreatePoint((点坐标));
            swMathPt = swMathPt.MultiplyTransform(swXform);
            点坐标[0] = swMathPt.ArrayData[0];
            点坐标[1] = swMathPt.ArrayData[1];
            点坐标[2] = swMathPt.ArrayData[2];

        }

    }
}
