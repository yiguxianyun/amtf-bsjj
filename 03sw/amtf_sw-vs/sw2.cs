﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;

namespace amtf_sw
{
    public partial class SW
    {
        public static readonly Dictionary<string, string> 配合名dic = new Dictionary<string, string>()
        {
            {"左", "左重合" },
            {"下", "下重合" },
            {"前", "前重合" }
        };
        public static readonly Dictionary<string, string> 板厚方程式名dic = new Dictionary<string, string>()
        {
            {"左", "\"左右尺寸@前后左右\"=\"Thickness\"" },
            {"右", "\"左右尺寸@前后左右\"=\"Thickness\"" },
            {"上", "\"上下尺寸@上下\"=\"Thickness\"" },
            {"下", "\"上下尺寸@上下\"=\"Thickness\"" },
            {"前", "\"前后尺寸@前后左右\"=\"Thickness\"" },
            {"后", "\"前后尺寸@前后左右\"=\"Thickness\"" },
        };
        public static readonly Dictionary<string, string> 外部参考dic = new Dictionary<string, string>()
        {
            {"右", "右参考" },
            {"上", "上参考" },
            {"后", "后参考" }
        };
        public static readonly Dictionary<string, string[]> 基准可能名dic = new Dictionary<string, string[]>()
        {
            {"左", new string[] { "Left", "左", "右视基准面" } },
            {"下", new string[] {"Bottom", "下", "前视基准面" } },
            {"前", new string[] {"Front", "前", "上视基准面" } },
            {"右", new string[] {"Right", "右"} },
            {"后", new string[] {"Back", "后" } },
            {"上", new string[] {"Top", "上" } },
            {"原点", new string[] {"原点", "Origin" } },
        };
        public static Dictionary<string, string> 板子名dic = new Dictionary<string, string>()
        {
            {"左", "左侧板"  },
            {"下", "底板"  },
            {"前", "面板"  },
            {"右", "右侧板"  },
            {"后", "背板"  },
            {"上", "顶板"  },
        };
        public Dictionary<string, dynamic> 参考面点s = new Dictionary<string, dynamic>();
        public double 选中点偏移x;
        public double 选中点偏移y;
        public double 选中点偏移z;

        public Component2 插入板子(string 方向, SW sw, bool 更新配合和参考 = true)
        {
            // var 参考面点s = 识别参考实体面点_按面法向(基准盒组件);
            // 参考面点s = 找面的靠前点(参考面点s["左"]);
            var 板子名 = 板子名dic[方向];
            var 板组件 = 根据名称找组件($"{板子名}_{FilenameWHZ}");
            if (板组件 == null)
            {
                var 目标全名 = AMTF.复制sw文件(sw, 板子名, "板子");
                板组件 = 插入零件(目标全名, false, false, false, false);
            }
            sw全名kk = 板组件.GetPathName();
            swModelkk = (ModelDoc2)iSwApp.GetOpenDocumentByName(sw全名kk);
            设置板件尺寸状态(swModelkk, 方向, 板组件);
            if (更新配合和参考)
            {
                更新或添加板件配合(板组件);
                更新板件基准面外部参考(板组件);
            }
            return 板组件;
        }

        public bool 猜名称获取原点(string[] 基准可能名, string 组件id)
        {
            // var 组件id = 拟匹配组件对象.GetSelectByIDString();
            foreach (var item in 基准可能名)
            {
                var 对象全名 = $"Point1@{item}";
                if (组件id != "")
                {
                    对象全名 = $"{对象全名}@{组件id}";
                }
                bool status = swModel.Extension.SelectByID2(对象全名, "EXTSKETCHPOINT", 0, 0, 0, true, 1, null, 0);
                if (status)
                {
                    return true;
                }
            }
            return false;
        }
        public bool 猜名称获取基准面(string 方向, string 组件id)
        {
            string[] 基准面可能名 = 基准可能名dic[方向];
            foreach (var item in 基准面可能名)
            {
                var 对象全名 = $"{item}@{组件id}";
                bool status = swModel.Extension.SelectByID2(对象全名, "PLANE", 0, 0, 0, false, 1, null, 0);
                if (status)
                {
                    break;
                }
            }
            return false;
        }

        public Feature 猜名称获取基准面对象(string 方向, Component2 组件)
        {
            string[] 基准面可能名 = 基准可能名dic[方向];
            foreach (var 特征名 in 基准面可能名)
            {
                var 特征 = 组件.FeatureByName(特征名);
                if (特征 != null)
                {
                    // 特征.Select2(false, 0);
                    return 特征;
                }

            }
            return null;
        }

        public dynamic 识别参考实体面点_按面法向(Component2 拟操作组件)
        {
            // 参考面点s = new Dictionary<string, Face2>();
            object vBodyInfo = null;
            object[] vBodies = null;
            vBodies = (object[])拟操作组件.GetBodies3((int)swBodyType_e.swAllBodies, out vBodyInfo);
            var faces = (object[])((Body2)vBodies[0]).GetFaces();
            foreach (Face2 face in faces)
            {
                // selMgr.SuspendSelectionList();
                // selMgr.AddSelectionListObject(face, selData);
                double[] arr = (double[])face.Normal;
                int[] arr2 = arr.Select(el => (int)el).ToArray();
                string result = string.Join(",", arr2);
                switch (result)
                {
                    case "1,0,0":
                        参考面点s["右"] = face;
                        break;
                    case "-1,0,0":
                        参考面点s["左"] = face;
                        break;
                    case "0,1,0":
                        参考面点s["后"] = face;
                        break;
                    case "0,0,1":
                        参考面点s["上"] = face;
                        break;
                    case "0,0,-1":
                        参考面点s["下"] = face;
                        break;
                    default:
                        break;
                }
                // inta = iSwApp.SendMsgToUser2(result, (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
            }
            参考面点s = 找面的靠前点(参考面点s["左"]);
            return 参考面点s;
        }
        public dynamic 识别参考实体面点_射线探测(Face2 选中面, double[] 选中点坐标)
        {
            // MathUtility mathUtil = (MathUtility)iSwApp.GetMathUtility();
            MathTransform swXform;
            MathTransform 组件tsf;
            double[] Xform = new double[16]{
                    1,0.0,0.0,
                    0.0,1,0.0,
                    0.0,0.0,1,
                    //translation👇
                    0,0,0,
                    //缩放
                    1.0,
                    0.0,0.0,0.0,
                };
            var 面法向 = 选中面.Normal;
            MathVector 面法向向量 = (MathVector)mathUtil.CreateVector(面法向);
            var 长 = 面法向向量.GetLength();
            //面法向向量.Scale(10 / 长);
            面法向向量 = 面法向向量.Normalise();
            面法向向量 = (MathVector)面法向向量.Scale(0.001);
            //面法向向量 = 面法向向量.Scale(0.01);
            var 长2 = 面法向向量.GetLength();
            Body2 实体 = (Body2)选中面.GetBody();
            //如果是空心面👇
            //实体.Select2(false,null);
            var kk = 实体.GetType();
            //1 = Sheet body
            if (kk == 1)
            {
                Xform[0] = -1;
                Xform[4] = -1;
                Xform[8] = -1;
                swXform = (MathTransform)mathUtil.CreateTransform(Xform);
                面法向向量 = (MathVector)面法向向量.MultiplyTransform(swXform);
            }
            Entity swEntity = null;
            swEntity = (Entity)选中面;
            Component2 swComponent = (Component2)swEntity.GetComponent();
            组件tsf = swComponent.Transform2;
            面法向向量 = (MathVector)面法向向量.MultiplyTransform(组件tsf);

            //选中点，往面法向偏移微小距离
            Double[] 面法向向量ar = (Double[])面法向向量.ArrayData;

            Xform[0] = 1;
            Xform[4] = 1;
            Xform[8] = 1;
            Xform[9] = 面法向向量ar[0];
            Xform[10] = 面法向向量ar[1];
            Xform[11] = 面法向向量ar[2];
            swXform = (MathTransform)mathUtil.CreateTransform(Xform);
            MathPoint swMathPt = (MathPoint)mathUtil.CreatePoint(选中点坐标);
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swXform);

            Double[] swMathPtar = (Double[])swMathPt.ArrayData;

            选中点偏移x = swMathPtar[0];
            选中点偏移y = swMathPtar[1];
            选中点偏移z = swMathPtar[2];
            //return 1;

            double[] x正方向 = new double[] { 1, 0, 0 };
            double[] x负方向 = new double[] { -1, 0, 0 };
            double[] y正方向 = new double[] { 0, 1, 0 };
            double[] y负方向 = new double[] { 0, -1, 0 };
            double[] z正方向 = new double[] { 0, 0, 1 };
            double[] z负方向 = new double[] { 0, 0, -1 };
            double radius = 0;


            ModelView swModelView = (ModelView)swModel.ActiveView;
            bool status;
            //Face2[] 左右前后上下面 = new Face2[6];
            //swModel.ClearSelection2(true);
            status = swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, x正方向[0], x正方向[1], x正方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            参考面点s["右"] = (Face2)selMgr.GetSelectedObject6(selMgr.GetSelectedObjectCount2(-1), -1);

            //swModel.ClearSelection2(true);
            status = swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, x负方向[0], x负方向[1], x负方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            参考面点s["左"] = (Face2)selMgr.GetSelectedObject6(selMgr.GetSelectedObjectCount2(-1), -1);

            //swModel.ClearSelection2(true);
            status = swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, y正方向[0], y正方向[1], y正方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            参考面点s["后"] = (Face2)selMgr.GetSelectedObject6(selMgr.GetSelectedObjectCount2(-1), -1);

            //swModel.ClearSelection2(true);
            status = swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, z正方向[0], z正方向[1], z正方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            参考面点s["上"] = (Face2)selMgr.GetSelectedObject6(selMgr.GetSelectedObjectCount2(-1), -1);
            //swModel.ClearSelection2(true);
            status = swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z, z负方向[0], z负方向[1], z负方向[2], radius, (int)swSelectType_e.swSelFACES, false, 0, (int)swSelectOption_e.swSelectOptionDefault);
            参考面点s["下"] = (Face2)selMgr.GetSelectedObject6(selMgr.GetSelectedObjectCount2(-1), -1);
            //status = swDocExt.SelectByRay(选中点偏移x, 选中点偏移y, 选中点偏移z,y正方向[0], y正方向[1], y正方向[2], radius, (int)swSelectType_e.swSelFACES, true, 0, (int)swSelectOption_e.swSelectOptionDefault);
            // 找面的靠前点(选中面, swComponent, out 前参考点);
            // var 参考面点s = 找面的靠前点(选中面, swComponent);
            参考面点s = 找面的靠前点(选中面);
            return 参考面点s;

        }
        public dynamic 找面的靠前点(Face2 搜索面)
        {
            // MathUtility mathUtil = (MathUtility)iSwApp.GetMathUtility();
            // 前参考点 = null;
            var 拟操作组件 = (Component2)((Entity)搜索面).GetComponent();
            MathTransform 组件tsf;
            组件tsf = 拟操作组件.Transform2;
            var loops = (Object[])搜索面.GetLoops();
            foreach (Loop2 loop in loops)
            {
                if (loop.IsOuter())
                {
                    object[] vVertices = (object[])loop.GetVertices();
                    Dictionary<Vertex, double[]> 点dict = new Dictionary<Vertex, double[]>();
                    //List<double> y点list = new List<double>();
                    foreach (Vertex vt in vVertices)
                    {
                        //var py=vt.GetPoint()[1];
                        //y点list.Add(vt.GetPoint()[1]);
                        //点dict.Add(vt, vt.GetPoint()[1]);
                        swMathPt = (MathPoint)mathUtil.CreatePoint(vt.GetPoint());
                        swMathPt = (MathPoint)swMathPt.MultiplyTransform(组件tsf);
                        点dict.Add(vt, (double[])swMathPt.ArrayData);

                        //点dict.Add(vt, vt.GetPoint());
                    }
                    var sort点dict = from item in 点dict orderby item.Value[0] ascending select item;
                    sort点dict = from item in sort点dict orderby item.Value[2] ascending select item;
                    sort点dict = from item in sort点dict orderby item.Value[1] ascending select item;

                    //var sort点dict = 点dict.OrderByDescending(item => item.Value[0]);
                    //sort点dict = sort点dict.ThenByDescending(item => item.Value[2]);
                    //sort点dict = sort点dict.ThenByDescending(item => item.Value[1]);
                    //swModel.ClearSelection2(true);
                    status = selMgr.AddSelectionListObject(sort点dict.First().Key, null);
                    var 前参考点 = (Vertex)selMgr.GetSelectedObject6(selMgr.GetSelectedObjectCount2(-1), -1);
                    参考面点s["前"] = 前参考点;
                }
            }
            return 参考面点s;
        }
        public Component2 根据名称找组件(string 关键词)
        {
            // assembly.GetComponents()
            var cmps = assembly.GetComponents(true);
            if (cmps == null)
            {
                return null;
            }
            foreach (Component2 cmp in (Object[])cmps)
            {
                string 组件名 = cmp.Name2;
                Match match = Regex.Match(组件名, @$".*{关键词}.*", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    return cmp;
                }
            }
            return null;
        }
        public void 更新或添加原点配合(Component2 拟操作组件, string 配合名称)
        {
            var mates = 拟操作组件.GetMates();
            if (mates != null)
            {
                foreach (Feature mate in (Object[])mates)
                {
                    if (mate.Name == 配合名称)
                    {
                        // CoincidentMateFeatureData CoincMateData = (CoincidentMateFeatureData)mate.GetDefinition();
                        // // CoincMateData.MateAlignment = (int)swMateAlign_e.swMateAlignALIGNED;
                        // // CoincMateData.MateAlignment = (int)swMateAlign_e.swMateAlignANTI_ALIGNED;
                        // CoincMateData.MateAlignment = (int)swMateAlign_e.swMateAlignCLOSEST;
                        // status = mate.IModifyDefinition2(CoincMateData, swModel, 拟操作组件);
                        return;
                    }
                }
            }
            添加原点配合(拟操作组件, 配合名称);
        }
        public void 更新板件基准面外部参考(Component2 拟操作组件)
        {
            foreach (var 方向 in 外部参考dic.Keys)
            {
                更新基准面外部参考_单个(拟操作组件, 方向);
            }
            assembly.EditAssembly();
        }
        public void 更新基准面外部参考_单个(Component2 拟操作组件, string 方向)
        {
            var 组件id = 拟操作组件.GetSelectByIDString();
            boola = 猜名称获取基准面(方向, 组件id);
            Feature 拟更新面 = 获取最后一个选中对象();

            var swRefPlaneFeatureData = (RefPlaneFeatureData)拟更新面.GetDefinition();
            // 装配体里面编辑组件的时候，非要下面这句不可👇
            status = swRefPlaneFeatureData.AccessSelections(swModel, 拟操作组件);
            swRefPlaneFeatureData.Constraint[0] = (int)swRefPlaneReferenceConstraints_e.swRefPlaneReferenceConstraint_Coincident;
            swRefPlaneFeatureData.Reference[0] = 参考面点s[方向];
            status = 拟更新面.IModifyDefinition2(swRefPlaneFeatureData, swModel, 拟操作组件);
            // assembly.EditAssembly();
        }
        public void 更新或添加板件配合(Component2 拟操作组件)
        {
            // var 配合名称ls = new List<string> { "左重合", "下重合", "前重合" };
            var mates = 拟操作组件.GetMates();
            Dictionary<string, Feature> matesdic = new Dictionary<string, Feature>();
            if (mates != null)
            {
                // matesdic = ((Object[])mates).Select(el => (Feature)el).ToDictionary(el => el.Name, el => el);
                foreach (Feature mate in (Object[])mates)
                {
                    var 配合名称 = mate.Name;
                    matesdic[配合名称] = mate;
                }
                foreach (var 方向 in 配合名dic.Keys)
                {
                    var 目标配合名 = 配合名dic[方向];
                    if (matesdic.Keys.Contains(目标配合名))
                    {
                        var mate = matesdic[目标配合名];
                        // CoincidentMateFeatureData CoincMateData = (CoincidentMateFeatureData)mate.GetDefinition();
                        // CoincMateData.MateAlignment = (int)swMateAlign_e.swMateAlignALIGNED;

                        // status = mate.IModifyDefinition2(CoincMateData, swModel, 拟操作组件);
                        // return;
                        更新或添加板件配合_单个(拟操作组件, 方向, mate);
                    }
                    else
                    {
                        更新或添加板件配合_单个(拟操作组件, 方向);
                    }
                }
            }
            else
            {
                foreach (var 方向 in 配合名dic.Keys)
                {
                    更新或添加板件配合_单个(拟操作组件, 方向);
                }
            }
        }
        public void 更新或添加板件配合_单个(Component2 拟操作组件, string 方向, Feature 配合特征 = null)
        {
            string 拟操作组件ID = 拟操作组件.GetSelectByIDString();
            if (配合特征 != null)//没办法进行编辑，只能删除了重新添加配合
            {
                swModel.ClearSelection2(true);
                boolstatus = selMgr.AddSelectionListObject(配合特征, selData);
                swDocExt.DeleteSelection2(1);
            }
            swModel.ClearSelection2(true);
            猜名称获取基准面(方向, 拟操作组件ID);
            selData.Mark = 1;
            boolstatus = selMgr.AddSelectionListObject(参考面点s[方向], selData);
            Feature matefeature = (Feature)assembly.AddMate5((int)swMateType_e.swMateCOINCIDENT, 1, false,
            0, 0, 0, 0, 0, 0, 0, 0, false, false, 0, out inta);
            matefeature.Name = 配合名dic[方向];
            // if (配合特征 != null)
            // {
            //     var coincMateData = (CoincidentMateFeatureData)配合特征.GetDefinition();
            //     object[] entitiesToMate = (object[])coincMateData.EntitiesToMate;
            //     entitiesToMate[0] = null;
            //     entitiesToMate[1] = null;
            //     entitiesToMate[0] = selMgr.GetSelectedObject6(1, -1);
            //     entitiesToMate[1] = selMgr.GetSelectedObject6(2, -1);
            //     coincMateData.MateAlignment = (int)swMateAlign_e.swMateAlignANTI_ALIGNED;
            //     status = 配合特征.ModifyDefinition(coincMateData, swModel, null);
            // }
            // else
            // {
            //     // Feature matefeature = (Feature)assembly.AddMate5((int)swMateType_e.swMateCOINCIDENT, -1, false, 0, 0, 0, 0, 0, 0, 0, 0, false, false, 0, out inta);
            //     var coincMateData = (ICoincidentMateFeatureData)assembly.CreateMateData((int)swMateType_e.swMateCOINCIDENT);
            //     object[] entitiesToMate = new object[]{
            //         selMgr.GetSelectedObject6(1, -1),
            //         selMgr.GetSelectedObject6(2, -1)
            //     };
            //     coincMateData.EntitiesToMate = entitiesToMate;//这种方式没玩会，只能通过预选面的方式
            //     coincMateData.MateAlignment = 1;
            //     // Create the mate
            //     Feature matefeature = (Feature)assembly.CreateMate(coincMateData);
            //     matefeature.Name = 配合名dic[方向];
            // }
        }
        public void 添加原点配合(Component2 拟操作组件, string 配合名称)
        {
            string 拟操作组件ID = 拟操作组件.GetSelectByIDString();
            swModel.ClearSelection2(true);
            boolstatus = 猜名称获取原点(基准可能名dic["原点"], "");
            boolstatus = 猜名称获取原点(基准可能名dic["原点"], 拟操作组件ID);
            // boolstatus = swDocExt.SelectByID2("Point1@原点", "EXTSKETCHPOINT", 0, 0, 0, True, 0, Nothing, 0)
            // boolstatus = swModel.Extension.SelectByID2(原点2, "EXTSKETCHPOINT", 0, 0, 0, True, 0, Nothing, 0)
            Feature matefeature = (Feature)assembly.AddMate5((int)swMateType_e.swMateCOORDINATE, -1, false,
                0, 0, 0, 0, 0, 0, 0, 0, false, false, 0, out inta);
            // Feature matefeature = (Feature)assembly.AddMate5(20, -1, false, 0, 0, 0, 0, 0, 0, 0, 0, false, false, 0, out inta);
            matefeature.Name = 配合名称;
        }
        public Component2 插入零件(string 目标全名, bool 清单排除, bool 封套, bool 重合原点, bool 虚拟件)
        {
            类型判断(目标全名);
            // 模板全名 = @$"{sw.获取资源路径()}\sw模板文件\{组件主名称}.PRTDOT";
            激活sw();
            ModelDoc2 swModelkk = iSwApp.OpenDoc6(目标全名, swFileTYpe, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, "", ref inta, ref warnings);
            swModelkk.Visible = false;
            boolstatus = swModelkk.Save3(1, ref errors, ref warnings); //不保存一下会有什么问题吗？……插入不进来
            var 插入的组件 = assembly.AddComponent5(目标全名, 0, "", false, "", 0, 0, 0);
            Debug.Print(插入的组件.GetSelectByIDString());
            string 拟操作组件ID = 插入的组件.GetSelectByIDString();

            bool ToplevelOnly = true;
            int 组件数量 = assembly.GetComponentCount(ToplevelOnly);
            if (组件数量 == 1)
            {
                boolstatus = swDocExt.SelectByID2(拟操作组件ID, "COMPONENT", 0, 0, 0, false, 0, null, 0);
                assembly.UnfixComponent();
            }
            boolstatus = swDocExt.SelectByID2(拟操作组件ID, "COMPONENT", 0, 0, 0, false, 0, null, 0);
            boolstatus = assembly.CompConfigProperties5(2, 0, true, true, "", 清单排除, 封套);
            if (虚拟件)
            {
                boolstatus = 插入的组件.MakeVirtual2(false);
            }
            // kk = 插入的组件.Name
            // SaveOk = swModel.Save3(1, lErrors, lwarnings)
            if (重合原点)
            {
                // 原点2 = $"Point1@原点@{插入的组件.GetSelectByIDString()}";
                // swModel.ClearSelection2(true);
                // 猜名称获取原点(new string[] { "原点", "Origin" }, "");
                // 猜名称获取原点(new string[] { "原点", "Origin" }, 拟操作组件ID);
                // // boolstatus = swDocExt.SelectByID2("Point1@原点", "EXTSKETCHPOINT", 0, 0, 0, True, 0, Nothing, 0)
                // // boolstatus = swModel.Extension.SelectByID2(原点2, "EXTSKETCHPOINT", 0, 0, 0, True, 0, Nothing, 0)
                // Feature myMate = (Feature)assembly.AddMate5(20, -1, false, 0, 0, 0, 0, 0, 0, 0, 0, false, false, 0, out status);
                // myMate.Name = "原点重合";
                添加原点配合(插入的组件, "原点重合");
            }
            else
            {
                //         kk = "前视基准面" & "@" & 拟操作组件ID
                // boolstatus = swModel.Extension.SelectByID2(kk, "PLANE", 0, 0, 0, False, 0, Nothing, 0)
                // numAdded = SelMgr.AddSelectionListObject(放置面, selData)
                // Set myMate = swModel.AddMate5(0, 0, False, 0, 0, 0, 0, 0, 0, 0, 0, False, False, 0, lstatus)
            }
            // '    swModel.EditRebuild3
            // Set swFeatMgr = swModel.FeatureManager
            ftmgr.UpdateFeatureTree();
            return 插入的组件;
        }
        public void 设置板件尺寸状态cs()
        {
            var sw = new SW();
            var 对象 = sw.获取选中or当前对象()[0];
            // inta = assembly.EditPart2(true, false, ref inta);
            设置板件尺寸状态(对象, "上");
            // 设置板件尺寸状态(对象, "下");
            // 设置板件尺寸状态(对象, "左");
            // 设置板件尺寸状态(对象, "右");
            // 设置板件尺寸状态(对象, "前");
            // 设置板件尺寸状态(对象, "后");
        }
        public void 设置板件尺寸状态(ModelDoc2 swModelkk, string 方向, Component2 拟操作组件 = null)
        {
            // var sw全名 = 组件.GetPathName();
            // ModelDoc2 swModelkk = (ModelDoc2)iSwApp.GetOpenDocumentByName(sw全名);
            // var 尺寸 = (Dimension)swModelkk.Parameter("厚@上下");
            // 尺寸.DrivenState = 2;
            // var cusPropMgr = swDocExt.CustomPropertyManager[""];
            // var 属性名 = "Thickness";
            // string 属性值;
            // string 属性求解值;
            // inta = cusPropMgr.Get6(属性名, false, out 属性值, out 属性求解值, out bool wasResolved, out bool linkToProperty);

            JObject 尺寸状态ob = new JObject();
            尺寸状态ob["上下尺寸@上下"] = 2;
            尺寸状态ob["下偏移@上下"] = 2;
            尺寸状态ob["上偏移@上下"] = 2;
            尺寸状态ob["前后尺寸@前后左右"] = 2;
            尺寸状态ob["左右尺寸@前后左右"] = 2;
            尺寸状态ob["左偏移@前后左右"] = 2;
            尺寸状态ob["前偏移@前后左右"] = 2;
            尺寸状态ob["右偏移@前后左右"] = 2;
            尺寸状态ob["后偏移@前后左右"] = 2;
            尺寸状态ob["前后尺寸@前后左右"] = 2;
            尺寸状态ob["左右尺寸@前后左右"] = 2;
            尺寸状态ob["左偏移@前后左右"] = 2;
            尺寸状态ob["前偏移@前后左右"] = 2;
            尺寸状态ob["右偏移@前后左右"] = 2;
            尺寸状态ob["后偏移@前后左右"] = 2;
            JObject 尺寸值ob = new JObject();
            尺寸值ob["上偏移@上下"] = 0;
            尺寸值ob["下偏移@上下"] = 0;
            尺寸值ob["左偏移@前后左右"] = 0;
            尺寸值ob["右偏移@前后左右"] = 0;
            尺寸值ob["前偏移@前后左右"] = 0;
            尺寸值ob["后偏移@前后左右"] = 0;

            删除厚度方程式(swModelkk);
            switch (方向)
            {
                case "左":
                    尺寸状态ob["上下尺寸@上下"] = 1;
                    尺寸状态ob["右偏移@前后左右"] = 1;
                    尺寸状态ob["前后尺寸@前后左右"] = 1;
                    // 尺寸值ob["上偏移@上下"] = 0;
                    // 尺寸值ob["下偏移@上下"] = 0;
                    // 尺寸值ob["左偏移@前后左右"] = 0;
                    // 尺寸值ob["前偏移@前后左右"] = 0;
                    // 尺寸值ob["后偏移@前后左右"] = 0;
                    break;
                case "右":
                    尺寸状态ob["上下尺寸@上下"] = 1;
                    尺寸状态ob["左偏移@前后左右"] = 1;
                    尺寸状态ob["前后尺寸@前后左右"] = 1;
                    break;
                case "上":
                    尺寸状态ob["下偏移@上下"] = 1;
                    尺寸状态ob["左右尺寸@前后左右"] = 1;
                    尺寸状态ob["前后尺寸@前后左右"] = 1;
                    break;
                case "下":
                    尺寸状态ob["上偏移@上下"] = 1;
                    尺寸状态ob["左右尺寸@前后左右"] = 1;
                    尺寸状态ob["前后尺寸@前后左右"] = 1;
                    break;
                case "前":
                    尺寸状态ob["上下尺寸@上下"] = 1;
                    尺寸状态ob["左右尺寸@前后左右"] = 1;
                    尺寸状态ob["后偏移@前后左右"] = 1;
                    break;
                case "后":
                    尺寸状态ob["上下尺寸@上下"] = 1;
                    尺寸状态ob["左右尺寸@前后左右"] = 1;
                    尺寸状态ob["前偏移@前后左右"] = 1;
                    break;
                default:
                    break;
            }
            foreach (var item in 尺寸状态ob)
            {
                Debug.Print(item.Key);
                var 尺寸 = (Dimension)swModelkk.Parameter(item.Key);
                try
                {
                    尺寸.DrivenState = (int)item.Value;
                }
                catch (System.Exception)
                {
                    throw;
                }
            }
            foreach (var item in 尺寸值ob)
            {
                Debug.Print(item.Key);
                var 尺寸 = (Dimension)swModelkk.Parameter(item.Key);
                // 尺寸.DrivenState = (int)item.Value;
                var lstatus = 尺寸.SetUserValueIn2(swModelkk, (double)item.Value, 0);
            }
            // var 尺寸2 = (Dimension)swModelkk.Parameter("左右尺寸@前后左右");
            // var lstatus = 尺寸2.SetUserValueIn2(swModelkk, 199, 0);

            插入厚度方程式(swModelkk, 方向, 拟操作组件);
            swModelkk.EditRebuild3();
        }

        public void 插入厚度方程式(ModelDoc2 swModelkk, string 方向, Component2 拟操作组件 = null)
        {
            string 方程式 = 板厚方程式名dic[方向];
            var swEquationMgr = swModelkk.GetEquationMgr();
            var 配置数 = swModelkk.GetConfigurationCount();
            // iSwApp.ActivateDoc3(swModelkk.GetPathName(), false, (int)swRebuildOnActivation_e.swDontRebuildActiveDoc, ref inta);
            if (拟操作组件 != null)
            {
                boola = 拟操作组件.Select2(false, 0);
                inta = assembly.EditPart2(true, false, ref inta);
            }

            if (配置数 > 1)
            {
                inta = swEquationMgr.Add3(-1, 方程式, false, 2, null);
            }
            else
            {
                inta = swEquationMgr.Add2(-1, 方程式, false);
            }

            if (拟操作组件 != null)
            {
                assembly.EditAssembly();
            }
            // iSwApp.ActivateDoc3(sw全名, false, (int)swRebuildOnActivation_e.swDontRebuildActiveDoc, ref inta);
        }
        public void 插入方程式(ModelDoc2 swModelkk)
        {
            // string 方程式 = 端距 & "=" & "(" & 总长 & "-(" & 阵列数量 & "-1)*" & 阵列距离 & ")/2";
            string 方程式 = 板厚方程式名dic["左"];
            var swEquationMgr = swModelkk.GetEquationMgr();
            var 配置数 = swModelkk.GetConfigurationCount();
            inta = swEquationMgr.Add3(-1, 方程式, false, 2, null);

            // if (配置数 > 1)
            // {
            //     inta = swEquationMgr.Add3(-1, 方程式, false, 2, null);
            // }
            // else
            // {
            //     inta = swEquationMgr.Add2(-1, 方程式, false);
            // }
        }
        public void 遍历方程式cs()
        {
            var swEquationMgr = swModel.GetEquationMgr();
            inta = swEquationMgr.GetCount();
            for (int i = 0; i < inta; i++)
            {
                Debug.Print("  Equation(" + i + ")  = " + swEquationMgr.get_Equation(i));
                Debug.Print("    Value = " + swEquationMgr.get_Value(i));
                Debug.Print("    Index = " + swEquationMgr.Status);
                Debug.Print("    Global variable? " + swEquationMgr.get_GlobalVariable(i));
            }
        }
        public void 删除厚度方程式(ModelDoc2 swModelkk)
        {
            while (!删除厚度方程式_是否到底(swModelkk))
            {
                删除厚度方程式_是否到底(swModelkk);
            }
        }
        public bool 删除厚度方程式_是否到底(ModelDoc2 swModelkk)
        {
            bool 是否到底 = false;
            var swEquationMgr = swModelkk.GetEquationMgr();
            inta = swEquationMgr.GetCount();
            if (inta == 0)
            {
                return true;
            }
            for (int i = 0; i < inta; i++)
            {
                string 方程式 = swEquationMgr.get_Equation(i);
                Debug.Print("  Equation(" + i + ")  = " + 方程式);
                if (板厚方程式名dic.ContainsValue(方程式))
                {
                    // Debug.Print("含有方程式");
                    swEquationMgr.Delete(i);
                    if (i == (inta - 1))
                    {
                        是否到底 = true;
                    }
                }
            }
            return 是否到底;
        }
    }
}