// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using System.Windows.Forms;

// namespace amtf_sw
// {
//     class HotKey
//     {
//         //如果函数执行成功，返回值不为0。
//         //如果函数执行失败，返回值为0。要得到扩展错误信息，调用GetLastError。
//         [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
//         public static extern bool RegisterHotKey(IntPtr hWnd,                //要定义热键的窗口的句柄
//            int id,                     //定义热键ID（不能与其它ID重复）
//             KeyModifiers fsModifiers,   //标识热键是否在按Alt、Ctrl、Shift、Windows等键时才会生效
//             Keys vk                     //定义热键的内容
//             );
//         [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
//         public static extern bool UnregisterHotKey(
//             IntPtr hWnd,                //要取消热键的窗口的句柄
//             int id                      //要取消热键的ID
//             );
//         //定义了辅助键的名称（将数字转变为字符以便于记忆，也可去除此枚举而直接使用数值）
//         [Flags()]
//         public enum KeyModifiers
//         {
//             None = 0,
//             Alt = 1,
//             Ctrl = 2,
//             Shift = 4,
//             WindowsKey = 8
//         }
//     }

//     // protected override void WndProc(ref Message m)
//     // {
//     //     const int WM_HOTKEY = 0x0312;
//     //     //按快捷键 
//     //     switch (m.Msg)
//     //     {
//     //         case WM_HOTKEY:
//     //             switch (m.WParam.ToInt32())
//     //             {
//     //                 case 100:    //按下的是Shift+S
//     //                              //此处填写快捷键响应代码 
//     //                     break;
//     //                 case 101:
//     //                     {//按下的是Ctrl+B
//     //                      //此处填写快捷键响应代码
//     //                      //MessageBox.Show("hot key");
//     //                         this.Show();
//     //                     }
//     //                     break;
//     //                 case 102:    //按下的是Alt+D
//     //                              //此处填写快捷键响应代码
//     //                     break;
//     //             }
//     //             break;
//     //     }
//     //     base.WndProc(ref m);
//     // }
// }