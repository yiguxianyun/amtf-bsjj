﻿using System;
using System.Collections.Generic;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
//using Scripting;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

namespace amtf_sw
{
    public partial class SW
    {
        public ISldWorks iSwApp;
        public ModelDoc2 swModel = default(ModelDoc2);
        public AssemblyDoc assembly = null;
        public FeatureManager ftmgr = null;
        public SelectionMgr selMgr = default(SelectionMgr);
        public SelectData selData = default(SelectData);
        public SketchManager swSkMgr = default(SketchManager);
        int numAdded;
        public int nRetval, inta, warnings;
        public bool boolStatus, status, boolstatus, boola;
        public bool b激活 = true;
        int errors, Warnings;
        public MassProperty swMass = default(MassProperty);
        public ModelDocExtension swDocExt = default(ModelDocExtension);
        ModelView swModelView = null;
        MathTransform swTransform = default(MathTransform);
        MathPoint swMathPt = default(MathPoint);
        MathUtility mathUtil = default(MathUtility);
        Component2 当前组件 = null;
        double[] arrayData;

        public bool 父级to组件 = false;
        public bool 草to模 = false;
        public Sketch 当前草图 = null;
        public String FilePath, Filename, FilenameWHZ;
        public int swFileTYpe;
        string sw全名, sw全名kk;
        ModelDoc2 swModelkk = null;

        public static readonly Dictionary<string, string> 提醒词dic = new Dictionary<string, string>()
        {
            {"打开sw", "呃……，请自行打开solidworks，先" },
            {"选中组件", "呃……，请选中一个组件，先!" },
        };
        public SW()
        {
            连接sw();
            设置sw常用对象();
        }
        public SW(ISldWorks iSwApp)
        {
            设置sw常用对象();
        }
        public SW(string sw全名, bool 显示 = true)
        {
            连接sw();
            获取指定文件(sw全名);
            设置sw常用对象();
            swModel.Visible = 显示;
        }

        public void 连接sw()
        {
            //Process 进程 = 查找当前打开sw进程();
            //if (进程==null)
            //{

            //}
            //iSwApp = new SldWorks();//这样new在别人的机器上会报错！
            iSwApp = (SldWorks)Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application"));
            //SldWorks.Application.22=sw2017
            //    //SldWorks.Application.26 = sw 2018
            //iSwApp = (SldWorks)Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application.26"));
            //设置sw常用对象();
            mathUtil = (MathUtility)iSwApp.GetMathUtility();
        }
        void 设置sw常用对象()
        {
            swModel = (ModelDoc2)iSwApp.ActiveDoc;
            try
            {
                assembly = (AssemblyDoc)swModel;
            }
            catch (System.Exception)
            {
            }
            if (swModel != null)
            {
                设置sw常用对象(swModel);
            }
        }
        void 设置sw常用对象(ModelDoc2 swModel)
        {
            selMgr = (SelectionMgr)swModel.SelectionManager;
            selData = selMgr.CreateSelectData();
            swDocExt = (ModelDocExtension)swModel.Extension;
            swSkMgr = swModel.SketchManager;
            ftmgr = swModel.FeatureManager;
            string sw全名 = swModel.GetPathName();
            if (sw全名 != "")
            {
                拆分文件名(sw全名);
            }
            if (b激活)
            {
                激活sw();
            }
        }
        public void 获取指定文件(string sw全名)
        {
            类型判断(sw全名);
            swModel = (ModelDoc2)iSwApp.GetOpenDocumentByName(sw全名);
            if (swModel == null)
            {
                swModel = (ModelDoc2)iSwApp.OpenDoc(sw全名, swFileTYpe);
            }
        }
        void 拆分文件名(String FilePathName)
        {
            int 反斜杠位置 = FilePathName.LastIndexOf("\\");
            FilePath = FilePathName.Substring(0, 反斜杠位置); //分解路径
            Filename = FilePathName.Substring(反斜杠位置 + 1); //分解文件名
            FilenameWHZ = Filename.Substring(0, Filename.Length - 7);
            //string fileFullPath = @"D:\Temp\aa.txt";
            //string fileName = Path.GetFileName(fileFullPath);  //aa.txt
            //string filePathOnly = Path.GetDirectoryName(fileFullPath);  //D:\Temp
            //string folderName = Path.GetFileName(filePathOnly);  //Temp
        }
        public void 类型判断(String FilePathName)
        {
            string 末尾三 = FilePathName.Substring(FilePathName.Length - 3).ToUpper();
            string 末尾六 = FilePathName.Substring(FilePathName.Length - 6).ToUpper();
            switch (末尾三)
            {
                case "PRT":
                case "LFP":
                    swFileTYpe = 1;
                    break;
                case "ASM":
                    swFileTYpe = 2;
                    break;
                case "DRW":
                    swFileTYpe = 3;
                    break;
                default:
                    if (末尾六 == "DRWDOT") { swFileTYpe = 3; }
                    break;
            }

        }

        [DllImport("user32.dll")]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);
        public void 激活sw()
        {
            Process localById = Process.GetProcessById(iSwApp.GetProcessID());
            IntPtr handle = localById.MainWindowHandle;
            SwitchToThisWindow(handle, true);    // 激活，显示在最前
        }
        public void 提示用户(string 提示信息)
        {
            Frame swFrame = (Frame)iSwApp.Frame();
            swFrame.SetStatusBarText(提示信息);
        }

        public void 过滤销钉符号()
        {
            swSelectType_e[] filters = new swSelectType_e[1];
            // if (iSwApp.GetSelectionFilters() == null)
            // {
            //     filters = (swSelectType_e[])iSwApp.GetSelectionFilters();
            // }
            iSwApp.SetSelectionFilters(iSwApp.GetSelectionFilters(), false);
            filters[0] = swSelectType_e.swSelDOWELSYMS;
            iSwApp.SetSelectionFilters(filters, true);
        }
        public void 清除过滤()
        {
            swSelectType_e[] filters = new swSelectType_e[0];
            filters = (swSelectType_e[])iSwApp.GetSelectionFilters();
            iSwApp.SetSelectionFilters(filters, false);
        }
        public void 屏幕坐标to草图坐标(ref double x, ref double y)
        {
            double[] 源点坐标 = new double[3];
            // 源点坐标[0] = Convert.ToInt32(x); 源点坐标[1] = Convert.ToInt32(y); 源点坐标[2] = 0;
            源点坐标[0] = x; 源点坐标[1] = y; 源点坐标[2] = 0;
            double[] 新点坐标 = new double[3];
            新点坐标 = 屏幕坐标to模型坐标(源点坐标);
            // '如果是从模型转换到草图，且是在装配体环境下编辑零件，则需要先转换到零件坐标系↓
            if (!swModel.IsEditingSelf())
            {
                当前组件 = ((AssemblyDoc)swModel).GetEditTargetComponent();
                父级to组件 = true;
                新点坐标 = 组件坐标ht父级坐标(新点坐标);
            }
            草to模 = false;
            新点坐标 = 草图坐标ht模型坐标(新点坐标);
            x = 源点坐标[0]; y = 源点坐标[1];
        }

        public double[] 屏幕坐标to模型坐标(double[] 点坐标)
        {
            mathUtil = (MathUtility)iSwApp.GetMathUtility();
            // 屏幕坐标→模型坐标↓
            swModelView = (ModelView)swModel.ActiveView;
            Debug.Print(swModel.GetPathName());
            swTransform = swModelView.Transform;
            // swTransform.Inverse();//Inverse()返回的仅仅是一个对象，然后没人接收，这里卡了一个下午和晚上，几度怀疑人生~~
            swTransform = (MathTransform)swTransform.Inverse();
            swMathPt = (MathPoint)mathUtil.CreatePoint(点坐标);
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swTransform);
            arrayData = (double[])swMathPt.ArrayData;
            double[] @params;
            @params = (double[])swMathPt.ArrayData;
            点坐标[0] = arrayData[0]; 点坐标[1] = arrayData[1]; 点坐标[2] = arrayData[2];
            return 点坐标;
        }
        public double[] 组件坐标ht父级坐标(double[] 点坐标)
        {
            mathUtil = (MathUtility)iSwApp.GetMathUtility();
            // 屏幕坐标→模型坐标↓
            swModelView = (ModelView)swModel.ActiveView;
            swTransform = 当前组件.Transform2;
            if (父级to组件)
            {
                // swTransform.Inverse();
                swTransform = (MathTransform)swTransform.Inverse();
            }
            swMathPt = (MathPoint)mathUtil.CreatePoint(点坐标);
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swTransform);
            arrayData = (double[])swMathPt.ArrayData;
            点坐标[0] = arrayData[0]; 点坐标[1] = arrayData[1]; 点坐标[2] = arrayData[2];
            return 点坐标;
        }
        public double[] 草图坐标ht模型坐标(double[] 点坐标)
        {
            if (当前草图 == null)
            {
                nRetval = iSwApp.SendMsgToUser2("还没有进入草图模式!", (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                return null;
            }
            mathUtil = (MathUtility)iSwApp.GetMathUtility();
            swTransform = 当前草图.ModelToSketchTransform;
            if (草to模)
            {
                // swTransform.Inverse();
                swTransform = (MathTransform)swTransform.Inverse();
            }
            swMathPt = (MathPoint)mathUtil.CreatePoint(点坐标);
            swMathPt = (MathPoint)swMathPt.MultiplyTransform(swTransform);
            arrayData = (double[])swMathPt.ArrayData;
            点坐标[0] = arrayData[0]; 点坐标[1] = arrayData[1]; 点坐标[2] = arrayData[2];
            return 点坐标;
        }

        public void 切换主从尺寸()
        {
            int 总数 = selMgr.GetSelectedObjectCount2(-1);
            for (int i = 1; i <= 总数; i++)
            {
                object 选择对象 = selMgr.GetSelectedObject6(i, -1);
                int objType = selMgr.GetSelectedObjectType3(i, -1);
                switch (objType)
                {
                    case (int)swSelectType_e.swSelDIMENSIONS:
                        Dimension 尺寸 = (Dimension)((DisplayDimension)选择对象).GetDimension();
                        int kk = 尺寸.DrivenState;
                        if (kk == 2)
                        {
                            尺寸.DrivenState = 1;
                        }
                        else if (kk == 1)
                        {
                            尺寸.DrivenState = 2;
                        }
                        break;
                }
            }
            swModelView = (ModelView)swModel.ActiveView;
            swModelView.TranslateBy(-0.0001, 0);
            swModel.ClearSelection2(true);
        }
        public void 复制sw文件(string 模板文件, string 需要文件)
        {
            //FileSystemObject fso = new FileSystemObject();
            try
            {
                //File.Copy(模板文件, 需要文件, false);
                //fso.CopyFile(模板文件, 需要文件, false);
                //模板文件 = @"D:\03_dm\dm\00模板_Z45三维模型（东迈）\臂架\下臂-1.SLDPRT";
                //需要文件 = @"D:\03_dm\dm\00模板_Z45三维模型（东迈）\臂架\下臂-188898.SLDPRT";
                string filePathOnly = Path.GetDirectoryName(需要文件);  //D:\Temp
                if (!Directory.Exists(filePathOnly))
                {
                    Directory.CreateDirectory(filePathOnly);
                }
                //fso.CopyFile(模板文件, 需要文件, false);
                File.Copy(模板文件, 需要文件, false);

            }
            catch (System.Exception ex)
            {
                // throw;
                //如果需要文件已经打开，想要覆盖的话↓未完成……
                nRetval = iSwApp.SendMsgToUser2(ex.ToString(), (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                ModelDoc2 kkswModel = (ModelDoc2)iSwApp.ActivateDoc3(需要文件, false, 0, errors);
                nRetval = kkswModel.ForceReleaseLocks();
                //fso.CopyFile(模板文件, 需要文件, false);

                int nRetVal重载 = kkswModel.ReloadOrReplace(false, 需要文件, true);

            }
            finally
            {

            }
        }

        public static Process 查找并激活打开的进程(string 进程名称)
        {
            foreach (System.Diagnostics.Process 进程 in System.Diagnostics.Process.GetProcesses())
            {
                //string 进程名 = 进程.MainWindowTitle;
                string 进程名 = 进程.ProcessName;
                Debug.Print(进程名.ToString());
                //if (进程名.Contains("SldWorks.Application"))
                //if ((进程名.ToUpper()).Contains("solidworks".ToUpper()))
                if ((进程名.ToUpper()) == (进程名称.ToUpper()))
                {
                    //thisproc.Kill();
                    Debug.Print(进程名);
                    //ShowWindowAsync(进程.MainWindowHandle); //调用api函数，正常显示窗口
                    //SetForegroundWindow(进程.MainWindowHandle); //将窗口放置最前端
                    SwitchToThisWindow(进程.MainWindowHandle, true);
                    return 进程;

                }
            }
            return null;
        }
        public static bool sw是否已经打开()
        {
            Process 当前打开sw进程 = 查找并激活打开的进程("sldworks");
            if (当前打开sw进程 == null)
            {
                return false;
            }
            return true;

        }
        [DllImport("User32.dll")]
        //private static extern bool ShowWindowAsync(System.IntPtr hWnd, int cmdShow);
        private static extern bool ShowWindowAsync(System.IntPtr hWnd);
        // [DllImport("User32.dll")]
        // private static extern bool SetForegroundWindow(System.IntPtr hWnd);

        public Dictionary<string, List<dynamic>> 识别选择的对象()
        {
            // Dictionary<string, dynamic[]>
            Dictionary<string, List<dynamic>> 选择的对象 = new Dictionary<string, List<dynamic>>();
            选择的对象.Add("组件", new List<dynamic>());
            选择的对象.Add("面", new List<dynamic>());
            选择的对象.Add("组件model", new List<dynamic>());
            double 总数 = selMgr.GetSelectedObjectCount2(-1);
            if (总数 == 0)
            {
                return null;
            }
            for (int i = 0; i < 总数; i++)
            {
                var 对象 = selMgr.GetSelectedObject6(i + 1, -1);
                switch (selMgr.GetSelectedObjectType3(i + 1, -1))
                {
                    case 20:
                        选择的对象["组件"].Add(对象);
                        sw全名 = ((Component2)对象).GetPathName();
                        swModelkk = (ModelDoc2)iSwApp.GetOpenDocumentByName(sw全名);
                        选择的对象["组件model"].Add(swModelkk);
                        break;
                    case (int)swSelectType_e.swSelFACES:
                    case 1:
                    case 3:
                        switch (selMgr.GetSelectedObjectType3(i + 1, -1))
                        {
                            case (int)swSelectType_e.swSelFACES:
                                选择的对象["面"].Add((Face2)对象);
                                break;
                        }
                        var swEntity = (Entity)对象;
                        对象 = swEntity.GetComponent();
                        选择的对象["组件"].Add(对象);
                        sw全名 = ((Component2)对象).GetPathName();
                        swModelkk = (ModelDoc2)iSwApp.GetOpenDocumentByName(sw全名);
                        选择的对象["组件model"].Add(swModelkk);
                        break;
                }
            }
            return 选择的对象;
        }

        public dynamic 获取最后一个选中对象()
        {
            int 总数 = selMgr.GetSelectedObjectCount2(-1);
            if (总数 == 0)
            {
                return null;
            }
            var 对象 = selMgr.GetSelectedObject6(总数, -1);
            return 对象;
        }
        public List<ModelDoc2> 获取选中or当前对象()
        {
            var 选中or当前对象 = new List<ModelDoc2>();
            int 总数 = selMgr.GetSelectedObjectCount2(-1);
            if (总数 == 0 || swFileTYpe == 1)
            {
                选中or当前对象.Add(swModel);
            }
            else
            {
                var kk = 识别选择的对象()["组件model"];
                List<ModelDoc2> yy = kk.Select(el => (ModelDoc2)el).ToList();
                选中or当前对象 = 选中or当前对象.Union(yy).ToList<ModelDoc2>(); //剔除重复项
            }
            return 选中or当前对象;
        }
        public void 遍历模型特征(ModelDoc2 模型)
        {
            IDisplayDimension swDispDim;
            string sPadStr = " ";
            Feature swFeat = (Feature)模型.FirstFeature();
            while ((swFeat != null))
            {
                // Debug.Print(sPadStr + swFeat.Name + " [" + swFeat.GetTypeName2() + "]");
                swDispDim = (IDisplayDimension)swFeat.GetFirstDisplayDimension();
                while ((swDispDim != null))
                {
                    var swDim = (Dimension)swDispDim.GetDimension();
                    var 尺寸全名 = swDim.GetNameForSelection();
                    // var 尺寸短名称 = Left(尺寸全名, InStrRev(尺寸全名, "@"));
                    var 尺寸值 = swDim.GetUserValueIn(swModel);
                    var 尺寸状态 = swDim.DrivenState;
                    // Debug.Print($"{尺寸全名}==={尺寸值}==={尺寸状态}");
                    Debug.Print($"尺寸状态o[\"{尺寸全名}\"] = {尺寸状态}");
                    // 尺寸状态o["上下尺寸@上下"] = 2;

                    swDispDim = (IDisplayDimension)swFeat.GetNextDisplayDimension(swDispDim);
                }
                swFeat = (Feature)swFeat.GetNextFeature();
            }
        }

        public void 修改尺寸(Component2 组件)
        {
            var sw全名 = 组件.GetPathName();
            ModelDoc2 swModelkk = (ModelDoc2)iSwApp.GetOpenDocumentByName(sw全名);

            var myDimension = (Dimension)swModelkk.Parameter("厚@上下");
            var lstatus = myDimension.SetUserValueIn2(swModelkk, 199, 0);
            swModel.EditRebuild3();
        }
        public Component2 获取选中组件()
        {
            Component2 选中组件 = null;
            var 选择的对象s = 识别选择的对象();
            if (选择的对象s == null)
            {
                inta = iSwApp.SendMsgToUser2(提醒词dic["选中组件"], (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                return null;
            }
            try { 选中组件 = 选择的对象s["组件"][0]; }
            catch (System.Exception)
            {
                inta = iSwApp.SendMsgToUser2(提醒词dic["选中组件"], (int)swMessageBoxIcon_e.swMbWarning, (int)swMessageBoxBtn_e.swMbOk);
                return null;
            }
            return 选中组件;
        }
        public static Task<string> 提醒打开sw()
        {
            return Task.FromResult(提醒词dic["打开sw"]);
        }
        public static Task<string> 提醒选中组件()
        {
            return Task.FromResult(提醒词dic["选中组件"]);
        }
    }

}

