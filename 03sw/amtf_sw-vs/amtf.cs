﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

namespace amtf_sw
{
    public class AMTF
    {

        [DllImport("user32")]
        private static extern IntPtr LoadCursorFromFile(string fileName);

        [DllImport("User32.DLL")]
        public static extern bool SetSystemCursor(IntPtr hcur, uint id);
        public const uint OCR_NORMAL = 32512;
        public const uint OCR_IBEAM = 32513;

        [DllImport("User32.DLL")]
        public static extern bool SystemParametersInfo(uint uiAction, uint uiParam, IntPtr pvParam, uint fWinIni);

        public const uint SPI_SETCURSORS = 87;
        public const uint SPIF_SENDWININICHANGE = 2;

        public static void 设置光标()
        {
            //设置
            // IntPtr iP = LoadCursorFromFile(@"D:\My Files\图片\cursor.cur");
            // SetSystemCursor(iP, OCR_NORMAL);

            // IntPtr hcur_click = LoadCursorFromFile(@"C:\Windows\Cursors\aero_helpsel_l.cur");
            IntPtr hcur_click = LoadCursorFromFile(@"C:\Windows\Cursors\aero_pin_xl.cur");
            SetSystemCursor(hcur_click, OCR_NORMAL);
            SetSystemCursor(hcur_click, OCR_IBEAM);
            // 设置移动
            //cur = LoadCursorFromFile("my.cur");
            //SetSystemCursor(cur, OCR_SIZEALL);
            // 设置不可用
            //cur = LoadCursorFromFile("my.cur");
            //SetSystemCursor(cur, OCR_NO);
            // 设置超链接
            //cur = LoadCursorFromFile("my.cur");
            //SetSystemCursor(cur, OCR_HAND);

        }

        public static async Task<object> 恢复光标()
        {
            //恢复
            SystemParametersInfo(SPI_SETCURSORS, SPIF_SENDWININICHANGE, IntPtr.Zero, SPIF_SENDWININICHANGE);
            return await Task.FromResult("ok");
        }
        public static string 获取dll执行路径()
        {
            string 路径 = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return 路径;
            // 在某些情况下，DLL在执行前被复制阴影，.Location属性将返回复制的路径。
            // 如果需要原始DLL的路径，请改用Assembly.GetExecutingAssembly().CodeBase属性。
            // 通过 CodeBase 得到一个 URI 格式的路径；
            // 用 UriBuild.UnescapeDataString 去掉前缀 File://；
            // 用 GetDirectoryName 把它变成正常的 windows 格式。

            // string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            // UriBuilder uri = new UriBuilder(codeBase);
            // string path = Uri.UnescapeDataString(uri.Path);
            // return Path.GetDirectoryName(path);
        }
        public static string 获取资源路径()
        {
            string path = @"D:\amtf\amtf_板式家具\01js\其他";
            if (Directory.Exists(path))
            {
                return path;
            }
            string 路径 = 获取dll执行路径();
            路径 = System.IO.Directory.GetParent(路径).FullName;
            return 路径;

        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr zeroOnly, string lpWindowName);

        public static void 置顶窗口(IntPtr handle, int 频率)
        {
            Task task = Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(频率);
                    SetForegroundWindow(handle);
                    // Console.WriteLine(SetForegroundWindow(handle));
                }
            });

            // string 方法名 = System.Reflection.MethodBase.GetCurrentMethod().Name;
            // return await Task.FromResult($"{方法名}  完成!");
        }

        public static Task<string> 提醒方法完成(StackTrace st, SW sw, string 附加消息1 = "")
        {
            // string 方法名 = System.Reflection.MethodBase.GetCurrentMethod().Name;//异步方法不适用
            string 方法名 = st.GetFrame(0).GetMethod().Name;
            string 消息 = $"{方法名}  完成!";
            if (附加消息1 != "")
            {
                消息 = $"{消息}……{附加消息1}";
            }
            sw.提示用户(消息);
            return Task.FromResult(消息);
        }

        public static string 复制sw文件(SW sw, string 组件主名称, string 模板名称)
        {
            var 资源路径 = 获取资源路径();
            var 模板全名 = @$"{资源路径}\sw模板文件\{模板名称}.PRTDOT";
            var 目标全名 = @$"{sw.FilePath}\{组件主名称}_{sw.FilenameWHZ}.SLDPRT";
            if (!File.Exists(目标全名))
            {
                FileInfo file = new FileInfo(模板全名);
                file.CopyTo(目标全名, true);
            }
            return 目标全名;
        }


        // //如果函数执行成功，返回值不为0。
        // //如果函数执行失败，返回值为0。要得到扩展错误信息，调用GetLastError。
        // [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        // public static extern bool RegisterHotKey(IntPtr hWnd,                //要定义热键的窗口的句柄
        //    int id,                     //定义热键ID（不能与其它ID重复）
        //     KeyModifiers fsModifiers,   //标识热键是否在按Alt、Ctrl、Shift、Windows等键时才会生效
        //     Keys vk                     //定义热键的内容
        //     );
        // [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        // public static extern bool UnregisterHotKey(
        //     IntPtr hWnd,                //要取消热键的窗口的句柄
        //     int id                      //要取消热键的ID
        //     );
        // //定义了辅助键的名称（将数字转变为字符以便于记忆，也可去除此枚举而直接使用数值）
        // [Flags()]
        // public enum KeyModifiers
        // {
        //     None = 0,
        //     Alt = 1,
        //     Ctrl = 2,
        //     Shift = 4,
        //     WindowsKey = 8
        // }


        // protected override void WndProc(ref Message m)
        // {

        //     const int WM_HOTKEY = 0x0312;
        //     //按快捷键 
        //     switch (m.Msg)
        //     {
        //         case WM_HOTKEY:
        //             switch (m.WParam.ToInt32())
        //             {
        //                 case 100:    //按下的是Shift+S
        //                              //此处填写快捷键响应代码 
        //                     break;
        //                 case 101:
        //                     {//按下的是Ctrl+B
        //                      //此处填写快捷键响应代码
        //                      //MessageBox.Show("hot key");
        //                         this.Show();
        //                     }
        //                     break;
        //                 case 102:    //按下的是Alt+D
        //                              //此处填写快捷键响应代码
        //                     break;
        //             }
        //             break;
        //     }
        //     base.WndProc(ref m);
        // }

        // private void Form1_Load(object sender, EventArgs e)
        // {
        //     HotKey.RegisterHotKey(Handle, 101, HotKey.KeyModifiers.Ctrl, Keys.B);
        // }
        // private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        // {
        //     HotKey.UnregisterHotKey(Handle, 101);
        // }
    }
}

